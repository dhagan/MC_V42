unique="base_a"
fastyaml="$(pwd)/fast.yaml"

mkdir -p ${OUT_PATH}/helicity/${unique}
mkdir -p ${OUT_PATH}/helicity/${unique}/plots
pushd ${OUT_PATH}/helicity/${unique} >> /dev/null
helicity -i ${fastyaml}
popd >> /dev/null
