#include <funcs.hxx>

const double inv_sqrt_2pi = 1.0/std::sqrt(2*M_PI);

double sag( double * x, double * p ){

  double chi_amplitude = p[0];
  double & chi_centre    = p[1];
  double inv_chi_left_sigma   = 1.0/p[2];
  double inv_chi_right_sigma   = 1.0/p[3];
  double inv_chi_comb_sigma = 2.0/(p[2] + p[3]);
  double & bckg_amplitude = p[4];
  double & bckg_centre    = p[5];
  double inv_bckg_sigma   = 1.0/p[6];

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  if ( x[0] < chi_centre ){
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_left_sigma) 
                                    *((position-chi_centre)*inv_chi_left_sigma) )
                          * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } else {
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_right_sigma) 
                                     *((position-chi_centre)*inv_chi_right_sigma) )
                           * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } 

}

double sag_exp( double * x, double * p ){

  double chi_amplitude = p[0];
  double & chi_centre    = p[1];
  double inv_chi_left_sigma   = 1.0/p[2];
  double inv_chi_right_sigma   = 1.0/p[3];
  double inv_chi_comb_sigma = 2.0/(p[2] + p[3]);

  double & position = x[0];

  double & a = p[4];
  double & b = p[5];
  double & c = p[6];
  double & d = p[7];
  double bpos = ( position - b );
  double bckg = a*std::pow( bpos, c )*exp( -1.0*d*bpos );

  if ( x[0] < chi_centre ){
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_left_sigma) 
                                    *((position-chi_centre)*inv_chi_left_sigma) )
                          * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } else {
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_right_sigma) 
                                     *((position-chi_centre)*inv_chi_right_sigma) )
                           * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } 

}