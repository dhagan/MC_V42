#include <helicity.hxx>

#include <math.h>
#include <memory>

#include <TF1.h>
#include <TMath.h>
#include <TFitResult.h>

#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "RooAbsPdf.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "RooBifurGauss.h"



#include <sys/stat.h>
inline bool file_exists(const std::string& name) {
  struct stat buffer;   
  return ( stat( name.c_str(), &buffer ) == 0 ); 
}

void prepare( YAML::Node & input, const std::string & filepath, variable_set & variables, bool reprocess ){
  
  if ( variables.extra_bounds.empty() ){ std::cout << "ne" << std::endl; }

  std::string snapshot_name = "./snapshot_" + input["unique"].as<std::string>() + ".root";
  std::string small_snapshot_name = "./small_snapshot_" + input["unique"].as<std::string>() + ".root";

  bool process_ntuple = !file_exists( snapshot_name ) || reprocess ;
  if ( process_ntuple ){
    TChain input_chain;
    input_chain.Add( filepath.c_str());
    ROOT::RDataFrame input_frame( input_chain );
    auto frame = input_frame.Filter( "(Photon_Pt.size() != 0)" );
    frame = frame.Define( "minv", "Float_t( MuMuY_M[0]/1e3 )" );
    frame = frame.Define( "mumu_mass",  "Float_t( DiLept_M[0]/1000.0 )" );
    frame = frame.Define( "photon_eta",	"Float_t( Photon_Eta[0] )" );
    frame = frame.Define( "mupos_eta",  "Float_t( MuPlus_Eta[0] )" );
    frame = frame.Define( "muneg_eta",  "Float_t( MuMinus_Eta[0] )" );
    frame = frame.Define( "deltaEta_mu", "abs( MuMinus_Eta[0] - MuPlus_Eta[0] ) "  );
    frame = frame.Define( "deltaPhi_mu", "abs( MuMinus_Phi[0] - MuPlus_Phi[0] )"  );
    frame = frame.Define( "minvres", "Float_t( minv - mumu_mass + 3.097 )"  );
    frame = frame.Define( "photpt", "Photon_Pt[0]/1000.0"  );
    frame = frame.Define( "jpsipt", "DiLept_Pt[0]/1000.0"  );
    frame = frame.Filter( "abs( muneg_eta ) < 2.4" );
    frame = frame.Filter( "abs( mupos_eta ) < 2.4" );
    frame = frame.Filter( "abs( photon_eta ) < 2.4" );
    frame = frame.Filter( "Photon_quality[0] == 0" );
    frame.Snapshot( "tree", snapshot_name.c_str(), "" );
    frame = frame.Filter( "(abs(DiMuonVertex_Tau[0])<0.1)&&(minvres>3.4&&minvres<4.0)"
                          "&&(mumu_mass > 2.8 && mumu_mass < 3.4)" );
    frame = frame.Define( "mmg_phi", "Float_t( MuMuGamma_Phi[0] )" );
    frame.Snapshot( "tree", small_snapshot_name.c_str(), "" );
  }
  
  TFile * input_file = new TFile( snapshot_name.c_str(), "READ" );
  TTree * tree = static_cast<TTree*>( input_file->Get( "tree" ) );
 
  TH1F * mmg_phi_hl = new TH1F( "mmg_phi_hl", "", 50, -M_PI, M_PI ); 
  TH1F * mmg_cos_hl = new TH1F( "mmg_cos_hl", "", 50, -1, 1 ); 
  TH1F * mmg_phi_cs = new TH1F( "mmg_phi_cs", "", 50, -M_PI, M_PI ); 
  TH1F * mmg_cos_cs = new TH1F( "mmg_cos_cs", "", 50, -1, 1 ); 
  TH1F * mm_phi_hl  = new TH1F( "mm_phi_hl", "", 50, -M_PI, M_PI ); 
  TH1F * mm_cos_hl  = new TH1F( "mm_cos_hl", "", 50, -1, 1 ); 
  TH1F * mm_phi_cs  = new TH1F( "mm_phi_cs", "", 50, -M_PI, M_PI ); 
  TH1F * mm_cos_cs  = new TH1F( "mm_cos_cs", "", 50, -1, 1 ); 
  TH1F * minvres    = new TH1F( "mmg_minvres", "", 60, 3.4, 4.0 ); 

  TH2F * mmg_cos_hl_phot = new TH2F( "mmg_cos_hl_phot", "", 50, -1, 1, 20, 5, 25 );

  //std::string cut_weight = input[ "cut_expression" ].as<std::string>();

  std::string cut = "(abs(DiMuonVertex_Tau[0])<0.1)&&(minvres>3.4&&minvres<3.6)"
                    "&&(mumu_mass > 2.8 && mumu_mass < 3.4)";

  tree->Draw( "MuMuGamma_Phi>>mmg_phi_hl",                  cut.c_str(), "e goff" );
  tree->Draw( "MuMuGamma_CosTheta>>mmg_cos_hl",             cut.c_str(), "e goff" );
  tree->Draw( "MuMuGamma_CS_Phi>>mmg_phi_cs",               cut.c_str(), "e goff" );
  tree->Draw( "MuMuGamma_CS_CosTheta>>mmg_cos_cs",          cut.c_str(), "e goff" );
  tree->Draw( "MuMu_Phi>>mm_phi_hl",                        cut.c_str(), "e goff" );
  tree->Draw( "MuMu_CosTheta>>mm_cos_hl",                   cut.c_str(), "e goff" );
  tree->Draw( "MuMu_CS_Phi>>mm_phi_cs",                     cut.c_str(), "e goff" );
  tree->Draw( "MuMu_CS_CosTheta>>mm_cos_cs",                cut.c_str(), "e goff" );
  tree->Draw( "minvres>>mmg_minvres",                       cut.c_str(), "e goff" );
  tree->Draw( "photpt:MuMuGamma_CosTheta>>mmg_cos_hl_phot",  cut.c_str(), "e goff" );

  std::string store_name = "./store_" + input["unique"].as<std::string>() + ".root";
  TFile * output_store = new TFile( store_name.c_str(), "RECREATE" );
  output_store->cd();
  
  mmg_phi_hl->Write();
  mmg_cos_hl->Write();
  mmg_phi_cs->Write();
  mmg_cos_cs->Write();
  mm_phi_hl->Write();
  mm_cos_hl->Write();
  mm_phi_cs->Write();
  mm_cos_cs->Write();
  minvres->Write();
  mmg_cos_hl_phot->Write();
  
  output_store->Close();
  delete mmg_phi_hl; 
  delete mmg_cos_hl;
  delete mmg_phi_cs;
  delete mmg_cos_cs;
  delete mm_phi_hl;
  delete mm_cos_hl;
  delete mm_phi_cs;
  delete mm_cos_cs;
  delete mmg_cos_hl_phot;

} 

void parameterise( YAML::Node & input, TF1 * function, const std::string & function_name ){
  if ( !input[function_name + "_value"] ){ return; }
  std::vector< double > value = input[ function_name + "_value" ].as<std::vector<double>>(); 
  std::vector< double > upper = input[ function_name + "_upper" ].as<std::vector<double>>();
  std::vector< double > lower = input[ function_name + "_lower" ].as<std::vector<double>>();
  for ( int param = 0; param < function->GetNpar(); param++ ){
    function->SetParameter( param, value.at( param ) );
    function->SetParLimits( param, lower.at( param ), upper.at( param ) );
  }
}


void helicity( YAML::Node & input, variable_set & variables ){

  prep_style();

  if ( variables.extra_bounds.empty() ){ std::cout << "ne" << std::endl; }

  std::string store_name = "./store_" + input["unique"].as<std::string>() + ".root";

  TFile * store = new TFile( store_name.c_str(), "READ" );
  store->cd();
  TH1F * mmg_phi_hl   = static_cast<TH1F *>( store->Get( "mmg_phi_hl" ) );
  TH1F * mmg_cos_hl   = static_cast<TH1F *>( store->Get( "mmg_cos_hl" ) );
  TH1F * mmg_phi_cs   = static_cast<TH1F *>( store->Get( "mmg_phi_cs" ) );
  TH1F * mmg_cos_cs   = static_cast<TH1F *>( store->Get( "mmg_cos_cs" ) );
  TH1F * mm_phi_hl    = static_cast<TH1F *>( store->Get( "mm_phi_hl" ) );
  TH1F * mm_cos_hl    = static_cast<TH1F *>( store->Get( "mm_cos_hl" ) );
  TH1F * mm_phi_cs    = static_cast<TH1F *>( store->Get( "mm_phi_cs" ) );
  TH1F * mm_cos_cs    = static_cast<TH1F *>( store->Get( "mm_cos_cs" ) );
  //TH1F * mmg_minvres  = static_cast<TH1F *>( store->Get( "mmg_minvres" ) );
  TH2F * mmg_cos_hl_phot = static_cast< TH2F * >( store->Get( "mmg_cos_hl_phot" ) );

 
  TCanvas * canvas = new TCanvas( "cnv", "", 200, 200, 4000, 2000 );
  canvas->Divide( 4, 2 );
  gStyle->SetOptStat( "ieoumrn" );
  gStyle->SetOptFit( 1 );

  TPad * active_pad = static_cast<TPad *>( canvas->cd( 1 ) );
  mmg_phi_hl->Draw( "HIST E1" );
  hist_prep_axes( mmg_phi_hl );
  add_atlas_decorations( active_pad );
  set_axis_labels( mmg_phi_hl, "#phi_{h}", "Entries" );
  add_pad_title( active_pad, "#mu#mu#gamma" );
  TPaveStats * mmg_phi_hl_stats = make_stats( mmg_phi_hl );
  mmg_phi_hl_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( canvas->cd( 2 ) );
  mmg_cos_hl->Draw( "HIST E1" );
  hist_prep_axes( mmg_cos_hl );
  add_atlas_decorations( active_pad );
  set_axis_labels( mmg_cos_hl, "cos#theta_{h}", "Entries" );
  add_pad_title( active_pad, "#mu#mu#gamma" );
  TPaveStats * mmg_cos_hl_stats = make_stats( mmg_cos_hl );
  mmg_cos_hl_stats->Draw( "SAME" );
  
  active_pad = static_cast<TPad *>( canvas->cd( 3 ) );
  mmg_phi_cs->Draw( "HIST E1" );
  hist_prep_axes( mmg_phi_cs );
  add_atlas_decorations( active_pad );
  set_axis_labels( mmg_phi_cs, "#phi_{cs}", "Entries" );
  add_pad_title( active_pad, "#mu#mu#gamma" );
  TPaveStats * mmg_phi_cs_stats = make_stats( mmg_phi_cs );
  mmg_phi_cs_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( canvas->cd( 4 ) );
  mmg_cos_cs->Draw( "HIST E1" );
  hist_prep_axes( mmg_cos_cs );
  add_atlas_decorations( active_pad );
  set_axis_labels( mmg_cos_cs, "cos#theta_{cs}", "Entries" );
  add_pad_title( active_pad, "#mu#mu#gamma" );
  TPaveStats * mmg_cos_cs_stats = make_stats( mmg_cos_cs );
  mmg_cos_cs_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( canvas->cd( 5 ) );
  mm_phi_hl->Draw( "HIST E1" );
  hist_prep_axes( mm_phi_hl );
  add_atlas_decorations( active_pad );
  set_axis_labels( mm_phi_hl, "#phi_{h}", "Entries" );
  add_pad_title( active_pad, "#mu#mu" );
  TPaveStats * mm_phi_hl_stats = make_stats( mm_phi_hl );
  mm_phi_hl_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( canvas->cd( 6 ) );
  mm_cos_hl->Draw( "HIST E1" );
  hist_prep_axes( mm_cos_hl );
  add_atlas_decorations( active_pad );
  set_axis_labels( mm_cos_hl, "cos#theta_{h}", "Entries" );
  add_pad_title( active_pad, "#mu#mu" );
  TPaveStats * mm_cos_hl_stats = make_stats( mm_cos_hl );
  mm_cos_hl_stats->Draw( "SAME" );


  active_pad = static_cast<TPad *>( canvas->cd( 7 ) );
  mm_phi_cs->Draw( "HIST E1" );
  hist_prep_axes( mm_phi_cs );
  add_atlas_decorations( active_pad );
  set_axis_labels( mm_phi_cs, "#phi_{cs}", "Entries" );
  add_pad_title( active_pad, "#mu#mu" );
  TPaveStats * mm_phi_cs_stats = make_stats( mm_phi_cs );
  mm_phi_cs_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( canvas->cd( 8 ) );
  mm_cos_cs->Draw( "HIST E1" );
  hist_prep_axes( mm_cos_cs );
  add_atlas_decorations( active_pad );
  set_axis_labels( mm_cos_cs, "cos#theta_{cs}", "Entries" );
  add_pad_title( active_pad, "#mu#mu" );
  TPaveStats * mm_cos_cs_stats = make_stats( mm_cos_cs );
  mm_cos_cs_stats->Draw( "SAME" );

  std::string save_name = "./plots/helicity_" + input["unique"].as<std::string>() + ".png";
  canvas->SaveAs( save_name.c_str() );

  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  TCanvas * coscanv = new TCanvas( "coscanv", "", 200, 200, 1000, 1000 );
  coscanv->Divide( 1, 1 );
  active_pad = static_cast<TPad*>( coscanv->cd( 1 ) );
  mmg_cos_hl_phot->SetStats( false );
  mmg_cos_hl_phot->Draw( "COLZ" );
  set_axis_labels( mmg_cos_hl_phot, "cos#theta_{h}", "p_{T,#gamma}" );
  coscanv->SaveAs( "costhetahl_photpt.png" );


  RooWorkspace workspace{"workspace"};

  Double_t mass_lower_limit = 3.4, mass_upper_limit = 4.0;
  RooRealVar minvres( "minvres", "M_{inv,res}", mass_lower_limit, mass_upper_limit, "GeV");
  RooRealVar mmg_phi( "mmg_phi", "#phi", -M_PI, M_PI, "rad");

  // mass model for the signal
  RooRealVar chi_mass( "chi_mass", "chi centre", 3.5, mass_lower_limit, mass_upper_limit );
  RooRealVar chi_sigma_l( "chi_sigma_l", "Left Width of Gaussian", 0.05, 0, 0.5, "GeV" );
  RooRealVar chi_sigma_r( "chi_sigma_r", "Right Width of Gaussian", 0.07, 0, 0.5, "GeV" );
  RooBifurGauss sign_model( "sign_model", "Signal asymmetric gaussian", minvres, chi_mass, chi_sigma_l, chi_sigma_r );

  RooRealVar bckg_mass( "bckg_mass", "Backgroudn centre", 3.9, mass_lower_limit, mass_upper_limit );
  RooRealVar bckg_sigma( "bckg_sigma", "Background width", 0.1, 0.001, 0.3, "GeV" );
  RooGaussian bckg_model( "bckg_model", "Background Model", minvres, bckg_mass, bckg_sigma );
 
  // Yield variables
  RooRealVar sign_yield( "sign_yield", "fitted yield for sign", 6000.0, 0.0, 10000.0 );
  RooRealVar bckg_yield( "bckg_yield", "fitted yield for bckg", 4000.0, 0.0, 10000.0 );
 
  // now make the combined models
  RooAddPdf model("model", "sign+bckg model", {sign_model, bckg_model}, {sign_yield, bckg_yield});
 
  workspace.import( model );

  // import the data
  std::string snapshot_name = "./small_snapshot_" + input["unique"].as<std::string>() + ".root";
  TFile * input_file = new TFile( snapshot_name.c_str(), "READ" );
  TTree * tree = static_cast<TTree*>( input_file->Get( "tree" ) );
  RooDataSet data( "data", "data", RooArgSet( minvres, mmg_phi ), RooFit::Import( *tree ) );
  workspace.import( data );

  // get what we need out of the workspace
  RooAbsPdf *  ws_model        = workspace.pdf( "model" );
  RooRealVar * ws_sign_yield  = workspace.var( "sign_yield" );
  RooRealVar * ws_bckg_yield  = workspace.var( "bckg_yield" );
  RooDataSet & ws_data        = static_cast< RooDataSet& > ( *workspace.data( "data" ) );

  
  ws_data.Print();
  ws_model->fitTo( ws_data );
  RooStats::SPlot splot( "splot_data", "SPlot", ws_data, ws_model, RooArgList( *ws_sign_yield, *ws_bckg_yield ) );
  ws_data.Print();

  workspace.import( ws_data, RooFit::Rename("dataWithSWeights") );

  std::cout << splot.GetYieldFromSWeight("sign_yield") << std::endl;
  std::cout << splot.GetYieldFromSWeight("bckg_yield") << std::endl;
  
  RooAbsPdf * ws_sign_model = workspace.pdf( "sign_model" );
  RooAbsPdf * ws_bckg_model = workspace.pdf( "bckg_model" );
  
  RooDataSet ws_data_sign{ ws_data.GetName(), ws_data.GetTitle(), &ws_data, *ws_data.get(), nullptr, "sign_yield_sw"};
  RooDataSet ws_data_bckg{ ws_data.GetName(), ws_data.GetTitle(), &ws_data, *ws_data.get(), nullptr, "bckg_yield_sw"};
  
  TCanvas * splot_canv = new TCanvas("sPlot", "", 200, 200, 3000, 1000);
  splot_canv->Divide( 3, 1 );

  active_pad = static_cast<TPad *>( splot_canv->cd( 1 ) );
  RooPlot * frame = minvres.frame();
  ws_data.plotOn( frame, RooFit::LineStyle( 1 ), RooFit::LineColor( 1 ) );
  ws_model->plotOn( frame, RooFit::Name( "total" ) );
  ws_model->plotOn( frame, RooFit::Components( *ws_sign_model ), RooFit::LineStyle(kDashed), RooFit::LineColor(kRed + 1 ),   RooFit::Name("sign_model") );
  ws_model->plotOn( frame, RooFit::Components( *ws_bckg_model ), RooFit::LineStyle(kDashed), RooFit::LineColor(kBlue + 1 ),  RooFit::Name("bckg_model") );
  frame->Draw();
  add_atlas_decorations( active_pad );
  add_pad_title( active_pad, "Model fit", false );
  frame->GetYaxis()->SetRangeUser( -100, 600 );
  
  active_pad = static_cast<TPad *>( splot_canv->cd( 2 ) );
  RooPlot * frame2 = minvres.frame();
  ws_data.plotOn( frame2, RooFit::LineStyle( 1 ), RooFit::LineColor( 1 ), RooFit::Name("data_minv")  );
  ws_data_sign.plotOn( frame2, RooFit::DataError(RooAbsData::SumW2), RooFit::MarkerColor( kRed + 1 ),   RooFit::Name("sign_sw_minv") );
  ws_data_bckg.plotOn( frame2, RooFit::DataError(RooAbsData::SumW2), RooFit::MarkerColor( kBlue + 1 ),  RooFit::Name("bckg_sw_minv") );
  frame2->Draw();
  add_atlas_decorations( active_pad );
  add_pad_title( active_pad, "sPlot Model", false );
  frame2->GetYaxis()->SetRangeUser( -100, 600 );


  active_pad = static_cast<TPad *>( splot_canv->cd( 3 ) );
  RooPlot * frame3 = mmg_phi.frame();
  ws_data.plotOn( frame3, RooFit::LineStyle( 1 ), RooFit::LineColor( 1 ), RooFit::Name( "data_phi" ) );
  ws_data_sign.plotOn( frame3, RooFit::DataError(RooAbsData::SumW2), RooFit::MarkerColor( kRed + 1 ),   RooFit::Name("sign_sw_phi") );
  ws_data_bckg.plotOn( frame3, RooFit::DataError(RooAbsData::SumW2), RooFit::MarkerColor( kBlue + 1 ),  RooFit::Name("bckg_sw_phi") );
  //TH1F * data_phi = frame2->getHist( "data_phi" )->GetHistogram();
  //hist_prep_axes( data_phi );
  frame3->Draw();
  add_atlas_decorations( active_pad );
  add_pad_title( active_pad, "sPlot extraction", false );
  frame3->GetYaxis()->SetRangeUser( 0, 200 );


  splot_canv->SaveAs( "splot.png" );

}
