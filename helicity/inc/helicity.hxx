#include <main.hxx>
#include <yaml-cpp/yaml.h>

void prepare( YAML::Node & input, const std::string & filepath, variable_set & variables, bool reprocess=false );
void helicity( YAML::Node & input, variable_set & variables );

