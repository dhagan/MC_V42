# J/psi-gamma h1g/f1g analysis repo

## Introduction

This is a repository for the analysis attempting to extract h1g and f1g TMDs from the momentum distribution of a j/psi + photon system. A. Tee, A. I. Hagan, V. Kartvelishvili.

## Setup process
* After clone, ensure submodule is initialised. 
* Run the following.
```
source ./setup.sh -i
```
This method and option enforces the cmake call. If this is not the first compile run, omit the -i.
The script should still be ran with each new shell opened, it provides environment variables for the framework.

## Outline of the analysis process

1. Create trees for analysis - trees
2. Run the BDT - bdt
3. Perform mass sideband and long lifetime subtraction - subtraction
4. Perform histfactory fits - histfactory\_fit
5. Make individiual plots - plotting

<!--  5. Make efficiency plots - SignalTruth(Matching)/ -->
<!--  6. Make differential plots - Differentials/ -->

## Plotting functions
* single\_systematic
  * produce plots of the baseline and combination with a single systematic
* systematics
  * full systematic application
* migration
  * produce migration plots, needs seeded files from trees.
* tree\_check
  * compares baseline output of tree or bdt stage with that of a variation or change.
* extract
  * Extracts signal and background from hf, corrects signal with efficiency.
* exp\_bg\_fit
  * fits tau background.
* sf\_conversion
  * performs scale factor conversion buisiness

## Notes.
Symbol lookup errors may occur, this is a consequence of sourcing the wrong libanalysisutils. Try symlinking the library to the user libraries 

## Analysis Process
This section is to be updated with the progress of the analysis and modifications to the repo. Use this section as a log of the changes we make. For example, 449->445, or other small configuration changes.
