#include <main.hxx>

void photon_delta( const std::string & input_filepath );
void photon_eff( TFile * efficiency_file );
void photon_splits( TFile * input_file );
