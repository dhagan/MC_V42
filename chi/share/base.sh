unique="base"
yaml="$(pwd)/${unique}.yaml"
fastyaml="$(pwd)/fast.yaml"

mkdir -p ${OUT_PATH}/chi/${unique}
mkdir -p ${OUT_PATH}/chi/${unique}/plots
pushd ${OUT_PATH}/chi/${unique} >> /dev/null
##chi -i ${fastyaml}
chi -i ${yaml}
popd >> /dev/null
