cmake_minimum_required( VERSION 3.14.0 )

## Project details
project( chi )

## Set sources and lib paths.
set( sources src/main.cxx src/chi.cxx src/photon.cxx )

## Find packages
find_package( ROOT CONFIG REQUIRED ROOTDataFrame ) 
find_package( nlohmann_json 3.2.0 REQUIRED ) 
##target_compile_definitions( ROOT::Core INTERFACE ${ROOT_DEFINITIONS} )

## Prepare executable
add_compile_options( -std=c++17 -Wall -Wextra -pedantic -Werror -O -O2 )
add_executable( chi ${sources} )
##rarget_include_directories( chi PUBLIC inc ${CMAKE_SOURCE_DIR}/lib/laurel/inc )
target_include_directories( chi PUBLIC inc ${CMAKE_SOURCE_DIR}/lib/analysis_utils/inc )

## Link libraries
target_link_libraries( chi PRIVATE ROOT::Core ${ROOT_LIBRARIES} nlohmann_json::nlohmann_json yaml-cpp::yaml-cpp )
target_link_libraries( chi PUBLIC analysis_utils )
##target_link_libraries( chi PUBLIC laurel )

## Enable export.
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
