#include <photon.hxx>

#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <fstream>


inline bool file_exists(const std::string& name) {
  struct stat buffer;   
  return ( stat( name.c_str(), &buffer ) == 0 ); 
}

void photon_delta( const std::string & input_filepath ){

  gStyle->SetOptStat( "ormen" );

  bool process_ntuple = !file_exists( "./delta.root" );

  if ( process_ntuple ){
    std::string filepath = std::string( std::getenv( "IN_PATH" ) ) + "/ntuples/" 
                              + input_filepath + "/tree";
    TChain input_chain;
    input_chain.Add( filepath.c_str() );
    ROOT::RDataFrame input_frame( input_chain );
    auto frame = input_frame.Filter( "(Photon_Pt.size() != 0)" );
    frame = frame.Define( "minv", "Float_t( MuMuY_M[0]/1000.0 )" );
    frame = frame.Define( "mumu_mass",  "Float_t( DiLept_M[0]/1000.0 )" );
    frame = frame.Define( "phot_eta",	"Float_t( Photon_Eta[0] )" );
    frame = frame.Define( "mupos_eta",  "Float_t( MuPlus_Eta[0] )" );
    frame = frame.Define( "muneg_eta",  "Float_t( MuMinus_Eta[0] )" );
    frame = frame.Define( "phot_phi",	"Float_t( Photon_Phi[0] )" );
    frame = frame.Define( "mupos_phi",  "Float_t( MuPlus_Phi[0] )" );
    frame = frame.Define( "muneg_phi",  "Float_t( MuMinus_Phi[0] )" );
    frame = frame.Define( "mupos_pt",  "Float_t( MuPlus_Pt[0]/1000.0 )" );
    frame = frame.Define( "muneg_pt",  "Float_t( MuMinus_Pt[0]/1000.0 )" );
    frame = frame.Define( "minvres", "Float_t( minv - mumu_mass + 3.097 )"  );
    frame = frame.Define( "deltaPhi_mu", "abs( MuMinus_Eta[0] - MuPlus_Eta[0] ) "  );
    frame = frame.Define( "deltaEta_mu", "abs( MuMinus_Phi[0] - MuPlus_Phi[0] )"  );
    frame = frame.Define( "photpt", "Photon_Pt[0]/1000.0"  );
    frame = frame.Define( "jpsipt", "DiLept_Pt[0]/1000.0"  );
    frame = frame.Define( "dphi_pos", "abs( phot_phi - mupos_phi )" );
    frame = frame.Define( "dphi_neg", "abs( phot_phi - muneg_phi )" );
    frame = frame.Define( "deta_pos", "abs( phot_eta - mupos_eta )" );
    frame = frame.Define( "deta_neg", "abs( phot_eta - muneg_eta )" );
    frame = frame.Define( "dr_pos", "sqrt( dphi_pos*dphi_pos + deta_pos*deta_pos )" );
    frame = frame.Define( "dr_neg", "sqrt( dphi_neg*dphi_neg + deta_neg*deta_neg )" );
    frame = frame.Define( "dr_min", "min( dr_pos, dr_neg )" ); 
    frame = frame.Define( "pos_nearest", " dr_pos < dr_neg " );
    frame = frame.Define( "pt_nearest", "(pos_nearest) ? mupos_pt : muneg_pt" );
    frame = frame.Define( "mu_diff", "mupos_pt + muneg_pt - jpsipt" );
    frame = frame.Filter( "abs( muneg_eta ) < 2.4" );
    frame = frame.Filter( "abs( mupos_eta ) < 2.4" );
    frame = frame.Filter( "abs( phot_eta ) < 2.4" );
    frame = frame.Filter( "Photon_quality[0] == 0" );
    frame = frame.Filter( "mumu_mass > 2.8 && mumu_mass < 3.4" );
    frame = frame.Filter( "minvres < 4" );
    frame.Snapshot( "tree", "./delta.root", "" );
  }

  TChain input_chain( "tree" );
  input_chain.Add( "delta.root" );
  auto frame = ROOT::RDataFrame( input_chain );

  TH1D min_dr = *( frame.Histo1D( { "min_dr", "min_dr", 100, 0, 1}, "dr_min" ) );
  TH2D pos = *( frame.Histo2D( { "pos", "pos", 100, 0, 1, 100, 0, 1 }, "dphi_pos", "deta_pos" ) );
  TH2D neg = *( frame.Histo2D( { "neg", "neg", 100, 0, 1, 100, 0, 1 }, "dphi_neg", "deta_neg" ) );
  TH1D * pos_dphi = pos.ProjectionX();
  TH1D * pos_deta = pos.ProjectionY();
  TH1D * neg_dphi = pos.ProjectionX();
  TH1D * neg_deta = pos.ProjectionY();
  TH1D pos_dr = *( frame.Histo1D( { "pos_dr", "pos_dr", 100, 0, 1}, "dr_pos" ) );
  TH1D neg_dr = *( frame.Histo1D( { "neg_dr", "neg_dr", 100, 0, 1}, "dr_neg" ) );
  TH2D minnr = *( frame.Histo2D( { "minnr", "minnr", 100, 0, 0.3, 100, 5, 30 }, "dr_min", "pt_nearest" ) );
  TH1D dimu_check = *( frame.Histo1D( { "dimu_check", "dimu_check", 100, -5, 5 }, "mu_diff" ) );


  TH1D pt0_hist = *( frame.Histo1D( { "pt0", "pt0", 50, 3, 4 }, "minvres" ) );

  auto pt1 = frame.Filter( "photpt > 4.0 && photpt < 6.0" );
  TH1D pt1_hist = *( pt1.Histo1D( { "pt1", "pt1", 50, 3, 4 }, "minvres" ) );
  auto pt2 = frame.Filter( "photpt > 6.0 && photpt < 8.0" );
  TH1D pt2_hist = *( pt2.Histo1D( { "pt2", "pt2", 50, 3, 4 }, "minvres" ) );
  auto pt3 = frame.Filter( "photpt > 8.0 && photpt < 10.0" );
  TH1D pt3_hist = *( pt3.Histo1D( { "pt3", "pt3", 50, 3, 4 }, "minvres" ) );
  auto pt4 = frame.Filter( "photpt > 10.0" );
  TH1D pt4_hist = *( pt4.Histo1D( { "pt4", "pt4", 50, 3, 4 }, "minvres" ) );
 
  TFile * output_file = new TFile( "photon_split.root", "RECREATE" );
  pos.Write(); neg.Write();
  pos_dr.Write(); neg_dr.Write();
  pos_dphi->Write(); pos_deta->Write();
  neg_dphi->Write(); neg_deta->Write();
  output_file->Close();

  TCanvas canvas( "", "", 100, 100, 4000, 2000 );
  canvas.Divide( 4, 2 );

  canvas.cd( 1 );
  pos.SetName( "#mu^{+}-#gamma" );
  pos.Draw( "COLZ" );
  set_axis_labels( &pos, "#Delta#phi", "#Delta#eta" );
  TPaveStats * pos_stats = make_stats( &pos );
  pos_stats->Draw();

  canvas.cd( 2 );
  neg.SetName( "#mu^{-}-#gamma" );
  neg.Draw( "COLZ" );
  set_axis_labels( &neg, "#Delta#phi", "#Delta#eta" );
  TPaveStats * neg_stats = make_stats( &neg );
  neg_stats->Draw();

  canvas.cd( 3 );
  pos_dr.SetName( "#mu^{+}-#gamma #Delta R" );
  pos_dr.Draw( "HIST" );
  set_axis_labels( &pos_dr, "#Delta R", "Entries" );
  TPaveStats * pos_dr_stats = make_stats( &pos_dr );
  pos_dr_stats->Draw();

  canvas.cd( 4 );
  neg_dr.SetName( "#mu^{-}-#gamma #Delta R" );
  neg_dr.Draw( "HIST" );
  set_axis_labels( &neg_dr, "#Delta R", "Entries" );
  TPaveStats * neg_dr_stats = make_stats( &neg_dr );
  neg_dr_stats->Draw();

  canvas.cd( 5 );
  pos_dphi->SetName( "#mu^{+}-#gamma #Delta#phi" );
  pos_dphi->Draw( "HIST" );
  set_axis_labels( pos_dphi, "#Delta#phi", "Entries" );
  TPaveStats * pos_dphi_stats = make_stats( pos_dphi );
  pos_dphi_stats->Draw();

  canvas.cd( 6 );
  neg_dphi->SetName( "#mu^{-}-#gamma #Delta#phi" );
  neg_dphi->Draw( "HIST" );
  set_axis_labels( neg_dphi, "#Delta#phi", "Entries" );
  TPaveStats * neg_dphi_stats = make_stats( neg_dphi );
  neg_dphi_stats->Draw();

  canvas.cd( 7 );
  pos_deta->SetName( "#mu^{+}-#gamma #Delta#eta" );
  pos_deta->Draw( "HIST" );
  set_axis_labels( pos_deta, "#Delta#eta", "Entries" );
  TPaveStats * pos_deta_stats = make_stats( pos_deta );
  pos_deta_stats->Draw();

  canvas.cd( 8 );
  neg_deta->SetName( "#mu^{-}-#gamma #Delta#eta" );
  neg_deta->Draw( "HIST" );
  set_axis_labels( neg_deta, "#Delta#eta", "Entries" );
  TPaveStats * neg_deta_stats = make_stats( neg_deta );
  neg_deta_stats->Draw();

  canvas.SaveAs( "plots/photon_delta.png" );

  canvas.Clear();
  canvas.Divide( 4, 2 );

  canvas.cd( 1 );
  min_dr.SetName( "min-#mu-#gamma #Delta R" );
  min_dr.Draw( "HIST" );
  set_axis_labels( &min_dr, "#Delta R", "Entries" );
  TPaveStats * min_dr_stats = make_stats( &min_dr );
  min_dr_stats->Draw();

  canvas.cd( 2 );
  minnr.SetName( "min(#Delta R)-minR(p_{T})" );
  minnr.Draw( "COLZ" );
  set_axis_labels( &minnr, "min(#Delta R)", "min_r(pt)" );
  TPaveStats * minnr_stats = make_stats( &minnr );
  minnr_stats->Draw();

  canvas.cd( 3 );
  dimu_check.SetName( "dimu_check" );
  dimu_check.Draw( "HIST" );
  set_axis_labels( &dimu_check, "p_{T,j/#psi} - p_{T,#mu^{+}} - p_{T,#mu^{-}}", "Entries" );
  TPaveStats * dimu_check_stats = make_stats( &dimu_check );
  dimu_check_stats->Draw();

  canvas.cd( 4 );
  pt0_hist.SetName( "p_{T,#gamma},all" );
  pt0_hist.Draw( "HIST" );
  set_axis_labels( &pt0_hist, "m_{inv,res}", "Entries" );
  TPaveStats * pt0_hist_stats = make_stats( &pt0_hist );
  pt0_hist_stats->Draw();

  canvas.cd( 5 );
  pt1_hist.SetName( "p_{T,#gamma},4-6" );
  pt1_hist.Draw( "HIST" );
  set_axis_labels( &pt1_hist, "m_{inv,res}", "Entries" );
  TPaveStats * pt1_hist_stats = make_stats( &pt1_hist );
  pt1_hist_stats->Draw();

  canvas.cd( 6 );
  pt2_hist.SetName( "p_{T,#gamma},6-8" );
  pt2_hist.Draw( "HIST" );
  set_axis_labels( &pt2_hist, "m_{inv,res}", "Entries" );
  TPaveStats * pt2_hist_stats = make_stats( &pt2_hist );
  pt2_hist_stats->Draw();


  canvas.cd( 7 );
  pt3_hist.SetName( "p_{T,#gamma},8-10" );
  pt3_hist.Draw( "HIST" );
  set_axis_labels( &pt3_hist, "m_{inv,res}", "Entries" );
  TPaveStats * pt3_hist_stats = make_stats( &pt3_hist );
  pt3_hist_stats->Draw();

  canvas.cd( 8 );
  pt4_hist.SetName( "p_{T,#gamma},10+" );
  pt4_hist.Draw( "HIST" );
  set_axis_labels( &pt3_hist, "minv", "Entries" );
  TPaveStats * pt4_hist_stats = make_stats( &pt4_hist );
  pt4_hist_stats->Draw();



  canvas.SaveAs( "plots/min_dr.png" );



}


void photon_eff( TFile * efficiency_file ){

  gStyle->SetOptStat( "ormen" );

  TTree * reco_tree = (TTree *) efficiency_file->Get( "reco_tree" );
  TTree * trth_tree = (TTree *) efficiency_file->Get( "truth_tree" );

  TH1F reco_hist( "reco", "reco", 44, 5, 27 ); 
  TH1F trth_hist( "trth", "trth", 44, 5, 27 );  
  TH1F effc_hist( "effc", "effc", 44, 5, 27 );  

  const char * mass_cut = "Lambda>25&&Lambda<200";

  reco_tree->Draw( "PhotonPt>>reco",  mass_cut, "goff" );
  trth_tree->Draw( "PhotonPt>>trth", mass_cut, "goff" );
  effc_hist.Divide( &reco_hist, &trth_hist, 1.0, 1.0, "B" );

  TCanvas canv( "canv", "", 100, 100, 1000, 1000 );
  canv.Divide( 1 );

  effc_hist.SetName( "Efficiency" );
  effc_hist.Draw( "E1" );
  effc_hist.GetYaxis()->SetRangeUser( 0.0, 1.0 );
  set_axis_labels( &effc_hist, "P_{T,#gamma}", "Entries" );
  TPaveStats * effc_stats = make_stats( &effc_hist );
  effc_stats->Draw();

  canv.SaveAs( "plots/photon_efficiency.png" );

}

void photon_splits( TFile * input_file ){

  prep_style();

  TH1F upper_hist(  "up", "", 100, -10, 20 ); 
  TH1F first_hist(  "fi", "", 100, -10, 20 ); 
  TH1F second_hist( "se", "", 100, -10, 20 ); 
  TH1F third_hist(  "th", "", 100, -10, 20 ); 
  TH1F fourth_hist( "fo", "", 100, -10, 20 ); 
  TH1F fifth_hist(  "fv", "", 100, -10, 20 ); 

  std::string upper   = "PhotonPt > 10.0";
  std::string first   = "PhotonPt > 5.0 && PhotonPt < 6.0";
  std::string second  = "PhotonPt > 6.0 && PhotonPt < 7.0";
  std::string third   = "PhotonPt > 7.0 && PhotonPt < 8.0";
  std::string fourth  = "PhotonPt > 8.0 && PhotonPt < 9.0";
  std::string fifth   = "PhotonPt > 9.0 && PhotonPt < 10.0";

  TTree * tree = (TTree *) input_file->Get( "tree" );

  tree->Draw( "qtA>>up", upper.c_str(),  "goff" );
  tree->Draw( "qtA>>fi", first.c_str(),  "goff" );
  tree->Draw( "qtA>>se", second.c_str(), "goff" );
  tree->Draw( "qtA>>th", third.c_str(),  "goff" );
  tree->Draw( "qtA>>fo", fourth.c_str(), "goff" );
  tree->Draw( "qtA>>fv", fifth.c_str(),  "goff" );

  upper_hist.SetLineColorAlpha( kBlack, 1.0 ); 
  first_hist.SetLineColorAlpha( kBlack, 0.3 );
  first_hist.SetLineStyle( 2 );
  second_hist.SetLineColorAlpha( kGreen+3, 1.0 );
  third_hist.SetLineColorAlpha( kMagenta+2, 0.7 );
  fourth_hist.SetLineColorAlpha( kRed, 0.6 );
  fifth_hist.SetLineColorAlpha( kBlue-3, 1.0 );
  fifth_hist.SetLineStyle( 2 );

  THStack ptstack( "ptstack", "ptstack" );
  ptstack.Add( &upper_hist ); 
  ptstack.Add( &fifth_hist ); 
  ptstack.Add( &fourth_hist ); 
  ptstack.Add( &third_hist ); 
  ptstack.Add( &second_hist ); 
  ptstack.Add( &first_hist ); 



  TCanvas canv( "canv", "", 200, 200, 2000, 1000 );
  canv.Divide( 2, 1 );
  
  TPad * active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  first_hist.SetName( "5-6" );
  first_hist.Draw( "HIST E1" );
  upper_hist.Draw( "HIST E1 SAME" );
  second_hist.Draw( "HIST E1 SAME" );
  third_hist.Draw( "HIST E1 SAME" );
  fourth_hist.Draw( "HIST E1 SAME" );
  fifth_hist.Draw( "HIST E1 SAME" );
  hist_prep_axes( &first_hist, true );
  set_axis_labels( &first_hist, "q_{T}^{A}", "Entries" );
  add_atlas_decorations( active_pad );
  TLegend * legend = create_atlas_legend();
  legend->AddEntry( &first_hist, "5-6", "L");
  legend->AddEntry( &second_hist, "6-7",  "L"  );
  legend->AddEntry( &third_hist,  "7-8",  "L" );
  legend->AddEntry( &fourth_hist, "8-9",  "L"  );
  legend->AddEntry( &fifth_hist,  "9-10", "L"  );
  legend->AddEntry( &upper_hist,  ">10",  "L" );
  
  legend->Draw();

  active_pad = static_cast<TPad*>( canv.cd( 2 ) );
  ptstack.Draw( "E1" );
  hist_prep_axes( ptstack.GetHistogram(), true );
  set_axis_labels( ptstack.GetHistogram(), "q_{T}^{A}", "Entries" );
  ptstack.GetHistogram()->GetYaxis()->SetRangeUser( 0, ptstack.GetMaximum()*1.5 );
  add_atlas_decorations( active_pad );
  //ptstack.GetHists()->At( 0 )
  //set_axis_labels( &ptstack, "P_{T,#Gamma}", "Entries" );


  canv.SaveAs( "plots/split.png" );

  
  delete tree;
  input_file->Close();


}
