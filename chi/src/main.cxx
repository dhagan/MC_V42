#include <main.hxx>
#include <chi.hxx>
#include <photon.hxx>


int main(int argc, char ** argv){

  ROOT::EnableThreadSafety();
  //ROOT::EnableImplicitMT( 6 );
  //laurel::initialise(); 

  int option_index{0},option{0};
  static struct option long_options[] = {
      { "input",          required_argument,    0,      'i'},
      { 0,                0,                    0,      0}
  };

  const char * config_path = "";

  do {
    option = getopt_long( argc, argv, "i:", long_options, &option_index );
    switch ( option ){
      case 'i':
        config_path = optarg; 
        option = -1;
        break;
    }
  } while ( option != -1 );


  if ( strlen( config_path ) == 0 ){ throw std::invalid_argument( "No config file provided" ); }

  YAML::Node config = YAML::LoadFile( config_path );
  std::vector< std::string > run_nodes = config["general"]["runs"].as<std::vector<std::string>>();
  bool multithreaded = config["general"]["multithreaded"].as<bool>();
  if ( multithreaded ){ std::cout << "Multithreaded not implemented" << std::endl; }

  for ( std::string & input : run_nodes ){

    std::cout << "Run: input" << std::endl;

    YAML::Node run = config[input];
    std::string input_filepath = run["input_filepath"].as<std::string>();
    std::cout << input << std::endl;
    std::map< std::string, bool > modes  = run["modes"].as< std::map< std::string, bool > >();

    if ( modes["fastchi"] ){
      std::string filepath = std::string( std::getenv( "IN_PATH" ) ) + "/ntuples/" 
                            + input_filepath + "/tree";
      fastchi( config[input] );
    }

    if ( modes["chi"] ){
      std::cout << "chi mode" << std::endl;
      std::string filepath = std::string( std::getenv( "IN_PATH" ) ) + "/ntuples/" 
                            + input_filepath + "/tree";
      bool reprocess = ( run["reprocess"] ) ? run["reprocess"].as<bool>() : false;
      chi( filepath, reprocess );
    }

    if ( modes["photon_deltas"] ){
      std::cout << "Photon delta mode" << std::endl;
      photon_delta( input_filepath );
    }

    if ( modes["photon_eff"] ){
      std::cout << "Photon Efficiency mode" << std::endl;
      std::string efficiency_filepath = std::string( std::getenv( "OUT_PATH" ) ) + "/" + input_filepath;
      TFile * input_file = new TFile( efficiency_filepath.c_str(), "READ");
      photon_eff( input_file ); 
    }


    if ( modes["photon_splits"] ){
      std::string input_path = std::string( std::getenv( "OUT_PATH" ) ) + "/" + input_filepath;
      TFile * input_file = new TFile( input_path.c_str(), "READ");
      photon_splits( input_file ); 
    }

  }
}
