#include <chi.hxx>
#include <TF1.h>
#include <math.h>
#include <TMath.h>
#include <TFitResult.h>

#include <sys/stat.h>
inline bool file_exists(const std::string& name) {
  struct stat buffer;   
  return ( stat( name.c_str(), &buffer ) == 0 ); 
}

// chi c1 3.511
// chi c2 3.556


const double inv_sqrt_2pi = 1.0/std::sqrt(2*M_PI);

double agaus( double * x, double * p ){

  double & amplitude    = p[0];
  double & centre       = p[1];
  double inv_left_sigma   = 1.0/p[2];
  double inv_right_sigma  = 1.0/p[3];
  double inv_comb_sigma = 2.0/(p[2] + p[3]);
  double & position = x[0];

  if ( x[0] < centre ){
    return amplitude * exp(-0.5*( (position-centre)*inv_left_sigma) * ( (position-centre)*inv_left_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma;
  } else {
    return amplitude * exp(-0.5*( (position-centre)*inv_right_sigma) * ( (position-centre)*inv_right_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma;
    }
}

double agaus_r_chi1( double * x, double * p ){

  double amplitude    = p[0]/(1+p[1]);
  double & centre       = p[2];
  double inv_left_sigma   = 1.0/p[3];
  double inv_right_sigma  = 1.0/p[4];
  double inv_comb_sigma = 2.0/(p[3] + p[4]);

  double & position = x[0];

  if ( x[0] < centre ){
    return amplitude * exp(-0.5*( (position-centre)*inv_left_sigma) * ( (position-centre)*inv_left_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma;
  } else {
    return amplitude * exp(-0.5*( (position-centre)*inv_right_sigma) * ( (position-centre)*inv_right_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma;
    }
}
  
double agaus_r_chi2( double * x, double * p ){

  double amplitude = p[0]/(1+p[1])*p[1];
  double & centre       = p[2];
  double inv_left_sigma   = 1.0/p[3];
  double inv_right_sigma  = 1.0/p[4];
  double inv_comb_sigma = 2.0/(p[3] + p[4]);
  double & position = x[0];

  if ( x[0] < centre ){
    return amplitude * exp(-0.5*( (position-centre)*inv_left_sigma) * ( (position-centre)*inv_left_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma;
  } else {
    return amplitude * exp(-0.5*( (position-centre)*inv_right_sigma) * ( (position-centre)*inv_right_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma;
    }
}

double agaus_bckg( double * x, double * p ){

  double & amplitude    = p[0];
  double & centre       = p[1];
  double inv_left_sigma   = 1.0/p[2];
  double inv_right_sigma  = 1.0/p[3];

  double & bckg_amplitude    = p[4];
  double & bckg_centre       = p[5];
  double inv_bckg_sigma   = 1.0/p[6];

  double inv_comb_sigma = 2.0/(p[2] + p[3]);

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                                    * inv_sqrt_2pi * inv_bckg_sigma;

  if ( x[0] < centre ){
    return amplitude * exp(-0.5*( (position-centre)*inv_left_sigma) * ( (position-centre)*inv_left_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma + bckg;
  } else {
    return amplitude * exp(-0.5*( (position-centre)*inv_right_sigma) * ( (position-centre)*inv_right_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma + bckg;
    }
}

double agaus_2_bckg( double * x, double * p ){

  double & amplitude = p[0];
  double & chi1_centre    = p[1];
  double inv_chi1_sigma   = 1.0/p[2];
  double & chi2_centre         = p[3];
  double inv_chi2_left_sigma   = 1.0/p[4];
  double inv_chi2_right_sigma  = 1.0/p[5];
  double inv_comb_sigma = 2.0/(p[4] + p[5]);
  double & bckg_amplitude = p[6];
  double & bckg_centre    = p[7];
  double inv_bckg_sigma   = 1.0/p[8];
  double & position = x[0];
  double chi1_gauss =  amplitude * exp(-0.5*( (position-chi1_centre)*inv_chi1_sigma) * ( (position-chi1_centre)*inv_chi1_sigma) )
                                      * inv_sqrt_2pi * inv_chi1_sigma;

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  if ( x[0] < chi2_centre ){
    return amplitude * exp(-0.5*( (position-chi2_centre)*inv_chi2_left_sigma) * ( (position-chi2_centre)*inv_chi2_left_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma + bckg + chi1_gauss;
  } else {
    return amplitude * exp(-0.5*( (position-chi2_centre)*inv_chi2_right_sigma) * ( (position-chi2_centre)*inv_chi2_right_sigma) )
            * inv_sqrt_2pi * inv_comb_sigma + bckg + chi1_gauss;
    }
}

double agaus2pin_bckg( double * x, double * p ){

  double & amplitude = p[0];
  double & chi1_centre    = p[1];
  double inv_chi1_sigma   = 1.0/p[2];
  double chi2_centre         = p[1] + 0.045;
  double inv_chi2_left_sigma   = 1.0/p[3];
  double inv_chi2_right_sigma  = 1.0/p[4];
  double inv_comb_sigma = 2.0/(p[3] + p[4]);
  double & bckg_amplitude = p[5];
  double & bckg_centre    = p[6];
  double inv_bckg_sigma   = 1.0/p[7];
  double & position = x[0];

  double chi1_gauss =  amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_sigma) 
                                           *((position-chi1_centre)*inv_chi1_sigma) )
                                 * inv_sqrt_2pi * inv_chi1_sigma;

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  if ( x[0] < chi2_centre ){
    return amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_left_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_left_sigma) )
                                * inv_sqrt_2pi * inv_comb_sigma 
                                + bckg + chi1_gauss;
  } else {
    return amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_right_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_right_sigma) )
                                * inv_sqrt_2pi * inv_comb_sigma 
                                + bckg + chi1_gauss;
    }
}

  //double & amplitude = p[0];
  //double & chi1_centre    = p[1];
  //double inv_chi1_sigma   = 1.0/p[2];
  //double chi2_centre         = p[1] + 0.045;
  //double inv_chi2_left_sigma   = 1.0/p[3];
  //double inv_chi2_right_sigma  = 1.0/p[4];
  //double inv_comb_sigma = 2.0/(p[3] + p[4]);
  //double & bckg_amplitude = p[5];
  //double & bckg_centre    = p[6];
  //double inv_bckg_sigma   = 1.0/p[7];





//double & amplitude    = p[0];
//double & centre       = p[1];
//double inv_left_sigma   = 1.0/p[2];
//double inv_right_sigma  = 1.0/p[3];
//double inv_comb_sigma = 2.0/(p[2] + p[3]);

double dual_agaus_pin_bckg( double * x, double * p ){

  double & chi1_amplitude = p[0];
  double chi2_amplitude = p[0]*p[1];

  double & chi1_centre    = p[2];
  double inv_chi1_left_sigma   = 1.0/p[3];
  double inv_chi1_right_sigma   = 1.0/p[4];
  double inv_chi1_comb_sigma = 2.0/(p[3] + p[4]);

  double chi2_centre         = p[2] + 0.045;
  double inv_chi2_left_sigma   = 1.0/p[5];
  double inv_chi2_right_sigma  = 1.0/p[6];
  double inv_chi2_comb_sigma = 2.0/(p[5] + p[6]);

  double & bckg_amplitude = p[7];
  double & bckg_centre    = p[8];
  double inv_bckg_sigma   = 1.0/p[9];

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  double chi1_gauss = 0;
  if ( x[0] < chi1_centre ){
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_left_sigma) 
                                     *((position-chi1_centre)*inv_chi1_left_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } else {
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_right_sigma) 
                                     *((position-chi1_centre)*inv_chi1_right_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } 

  if ( x[0] < chi2_centre ){
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_left_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_left_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
  } else {
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_right_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_right_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
    }
}

double dag_param( double * x, double * p ){

  double chi1_amplitude = p[0]/(1+p[1]);
  double chi2_amplitude = chi1_amplitude*p[1];

  double & chi1_centre    = p[2];
  double inv_chi1_left_sigma   = 1.0/p[3];
  double inv_chi1_right_sigma   = 1.0/p[4];
  double inv_chi1_comb_sigma = 2.0/(p[3] + p[4]);

  double chi2_centre         = p[2] + 0.045;
  double inv_chi2_left_sigma   = 1.0/p[5];
  double inv_chi2_right_sigma  = 1.0/p[6];
  double inv_chi2_comb_sigma = 2.0/(p[5] + p[6]);

  double & bckg_amplitude = p[7];
  double & bckg_centre    = p[8];
  double inv_bckg_sigma   = 1.0/p[9];

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  double chi1_gauss = 0;
  if ( x[0] < chi1_centre ){
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_left_sigma) 
                                     *((position-chi1_centre)*inv_chi1_left_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } else {
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_right_sigma) 
                                     *((position-chi1_centre)*inv_chi1_right_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } 

  if ( x[0] < chi2_centre ){
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_left_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_left_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
  } else {
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_right_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_right_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
    }
}

// dual asymmetric gaussian common left-right sigma
double dag_clrs( double * x, double * p ){

  double & chi1_amplitude = p[0];
  double chi2_amplitude = p[0]*p[1];
  double & chi1_centre    = p[2];
  double chi2_centre         = p[2] + 0.045;

  double inv_left_sigma   = 1.0/p[3];
  double inv_right_sigma   = 1.0/p[4];
  double inv_comb_sigma = 2.0/(p[3] + p[4]);

  double & bckg_amplitude = p[5];
  double & bckg_centre    = p[6];
  double inv_bckg_sigma   = 1.0/p[7];

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  double chi1_gauss = 0;
  if ( x[0] < chi1_centre ){
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_left_sigma) 
                                          *((position-chi1_centre)*inv_left_sigma) )
                                * inv_sqrt_2pi * inv_comb_sigma;
  } else {
    chi1_gauss = chi2_amplitude * exp(-0.5*((position-chi2_centre)*inv_right_sigma) 
                                          *((position-chi2_centre)*inv_right_sigma) )
                                * inv_sqrt_2pi * inv_comb_sigma;
  } 

  if ( x[0] < chi2_centre ){
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_left_sigma) 
                                * ( (position-chi2_centre)*inv_left_sigma) )
                                * inv_sqrt_2pi * inv_comb_sigma 
                                + bckg + chi1_gauss;
  } else {
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_right_sigma) 
                                * ( (position-chi2_centre)*inv_right_sigma) )
                                * inv_sqrt_2pi * inv_comb_sigma 
                                + bckg + chi1_gauss;
    }
}

double gauss3( double * x, double * p ){

  double & chi1_amplitude = p[0];
  double chi2_amplitude = p[0]*p[1];
  double & chi1_centre    = p[2];
  double chi2_centre         = p[2] + 0.045;
  double inv_chi1_sigma   = 1.0/p[3];
  double inv_chi2_sigma   = 1.0/p[4];

  double & bckg_amplitude = p[5];
  double & bckg_centre    = p[6];
  double inv_bckg_sigma   = 1.0/p[7];

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  double chi1_gauss = chi1_amplitude * exp(-0.5 * ( (position-chi1_centre)*inv_chi2_sigma) 
                                                * ( (position-chi1_centre)*inv_chi2_sigma) )
                                     * inv_sqrt_2pi * inv_chi1_sigma; 
                                    
  double chi2_gauss = chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_sigma) 
                                                * ( (position-chi2_centre)*inv_chi2_sigma) )
                                     * inv_sqrt_2pi * inv_chi2_sigma;

  return chi1_gauss+chi2_gauss+bckg;
}

double lnd( double * x, double * p){
  return  TMath::Landau( x[0], p[0], p[1], false );
}
double dag_landau( double * x, double * p ){

  double & chi1_amplitude = p[0];
  double chi2_amplitude = p[0]*p[1];

  double & chi1_centre    = p[2];
  double inv_chi1_left_sigma   = 1.0/p[3];
  double inv_chi1_right_sigma   = 1.0/p[4];
  double inv_chi1_comb_sigma = 2.0/(p[3] + p[4]);

  double chi2_centre         = p[2] + 0.045;
  double inv_chi2_left_sigma   = 1.0/p[5];
  double inv_chi2_right_sigma  = 1.0/p[6];
  double inv_chi2_comb_sigma = 2.0/(p[5] + p[6]);

  double & position = x[0];

  double bckg = TMath::Landau( position, p[7], p[8], false );
  //double bckg = TMath::Landau( position, p[7], p[8], true );

  double chi1_gauss = 0;
  if ( x[0] < chi1_centre ){
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_left_sigma) 
                                     *((position-chi1_centre)*inv_chi1_left_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } else {
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_right_sigma) 
                                     *((position-chi1_centre)*inv_chi1_right_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } 

  if ( x[0] < chi2_centre ){
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_left_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_left_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
  } else {
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_right_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_right_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
    }
}














double bckg_exp( double * x, double * p ){
  double & position = x[0];
  double & a = p[0];
  double & b = p[1];
  double & c = p[2];
  double & d = p[3];
  double bpos = ( position - b );
  double bckg = a*std::pow( bpos, c )*exp( -1.0*d*bpos );
  return bckg;
}

double dag_exp( double * x, double * p ){

  double chi1_amplitude = p[0]/(1+p[1]);
  double chi2_amplitude = chi1_amplitude*p[1];

  double & chi1_centre    = p[2];
  double inv_chi1_left_sigma   = 1.0/p[3];
  double inv_chi1_right_sigma   = 1.0/p[4];
  double inv_chi1_comb_sigma = 2.0/(p[3] + p[4]);

  double chi2_centre         = p[2] + 0.045;
  double inv_chi2_left_sigma   = 1.0/p[5];
  double inv_chi2_right_sigma  = 1.0/p[6];
  double inv_chi2_comb_sigma = 2.0/(p[5] + p[6]);

  double & position = x[0];

  double & a = p[7];
  double & b = p[8];
  double & c = p[9];
  double & d = p[10];
  double bpos = ( position - b );
  double bckg = a*std::pow( bpos, c )*exp( -1.0*d*bpos );

  double chi1_gauss = 0;
  if ( x[0] < chi1_centre ){
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_left_sigma) 
                                     *((position-chi1_centre)*inv_chi1_left_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } else {
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_right_sigma) 
                                     *((position-chi1_centre)*inv_chi1_right_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } 

  if ( x[0] < chi2_centre ){
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_left_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_left_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
  } else {
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_right_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_right_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
    }
}


double dag_pinlr_param( double * x, double * p ){

  double chi1_amplitude = p[0]/(1+p[1]);
  double chi2_amplitude = chi1_amplitude*p[1];

  double & chi1_centre    = p[2];
  double inv_chi1_left_sigma   = 1.0/p[3];
  double inv_chi1_right_sigma   = 1.0/p[4];
  double inv_chi1_comb_sigma = 2.0/(p[3] + p[4]);

  double chi2_centre         = p[2] + 0.045;
  double inv_chi2_left_sigma   = 1.0/p[3];
  double inv_chi2_right_sigma  = 1.0/p[4];
  double inv_chi2_comb_sigma = 2.0/(p[3] + p[4]);

  double & bckg_amplitude = p[5];
  double & bckg_centre    = p[6];
  double inv_bckg_sigma   = 1.0/p[7];

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  double chi1_gauss = 0;
  if ( x[0] < chi1_centre ){
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_left_sigma) 
                                     *((position-chi1_centre)*inv_chi1_left_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } else {
    chi1_gauss = chi1_amplitude * exp(-0.5*((position-chi1_centre)*inv_chi1_right_sigma) 
                                     *((position-chi1_centre)*inv_chi1_right_sigma) )
                           * inv_sqrt_2pi * inv_chi1_comb_sigma;
  } 

  if ( x[0] < chi2_centre ){
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_left_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_left_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
  } else {
    return chi2_amplitude * exp(-0.5 * ( (position-chi2_centre)*inv_chi2_right_sigma) 
                                * ( (position-chi2_centre)*inv_chi2_right_sigma) )
                                * inv_sqrt_2pi * inv_chi2_comb_sigma 
                                + bckg + chi1_gauss;
    }
}

double sag( double * x, double * p ){

  double chi_amplitude = p[0];
  double & chi_centre    = p[1];
  double inv_chi_left_sigma   = 1.0/p[2];
  double inv_chi_right_sigma   = 1.0/p[3];
  double inv_chi_comb_sigma = 2.0/(p[2] + p[3]);
  double & bckg_amplitude = p[4];
  double & bckg_centre    = p[5];
  double inv_bckg_sigma   = 1.0/p[6];

  double & position = x[0];

  double bckg = bckg_amplitude * exp( -0.5 * ( (position-bckg_centre)*inv_bckg_sigma) 
                                           * ( (position-bckg_centre)*inv_bckg_sigma) )
                               * inv_sqrt_2pi * inv_bckg_sigma;

  if ( x[0] < chi_centre ){
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_left_sigma) 
                                    *((position-chi_centre)*inv_chi_left_sigma) )
                          * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } else {
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_right_sigma) 
                                     *((position-chi_centre)*inv_chi_right_sigma) )
                           * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } 

}

double sag_exp( double * x, double * p ){

  double chi_amplitude = p[0];
  double & chi_centre    = p[1];
  double inv_chi_left_sigma   = 1.0/p[2];
  double inv_chi_right_sigma   = 1.0/p[3];
  double inv_chi_comb_sigma = 2.0/(p[2] + p[3]);

  double & position = x[0];

  double & a = p[4];
  double & b = p[5];
  double & c = p[6];
  double & d = p[7];
  double bpos = ( position - b );
  double bckg = a*std::pow( bpos, c )*exp( -1.0*d*bpos );

  if ( x[0] < chi_centre ){
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_left_sigma) 
                                    *((position-chi_centre)*inv_chi_left_sigma) )
                          * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } else {
    return chi_amplitude * exp(-0.5*((position-chi_centre)*inv_chi_right_sigma) 
                                     *((position-chi_centre)*inv_chi_right_sigma) )
                           * inv_sqrt_2pi * inv_chi_comb_sigma + bckg;
  } 

}
  
  


void chi( const std::string & filepath, bool reprocess ){
  
  prep_style();

  bool process_ntuple = !file_exists( "./chi_snapshot.root" ) || reprocess ;

  if ( process_ntuple ){
    TChain input_chain;
    input_chain.Add( filepath.c_str());
    ROOT::RDataFrame input_frame( input_chain );
    auto frame = input_frame.Filter( "(Photon_Pt.size() != 0)" );
    frame = frame.Define( "minv", "Float_t( MuMuY_M[0]/1e3 )" );
    frame = frame.Define( "mumu_mass",  "Float_t( DiLept_M[0]/1000.0 )" );
    frame = frame.Define( "photon_eta",	"Float_t( Photon_Eta[0] )" );
    frame = frame.Define( "mupos_eta",  "Float_t( MuPlus_Eta[0] )" );
    frame = frame.Define( "muneg_eta",  "Float_t( MuMinus_Eta[0] )" );
    frame = frame.Define( "deltaEta_mu", "abs( MuMinus_Eta[0] - MuPlus_Eta[0] ) "  );
    frame = frame.Define( "deltaPhi_mu", "abs( MuMinus_Phi[0] - MuPlus_Phi[0] )"  );
    frame = frame.Define( "minvres", "Float_t( minv - mumu_mass + 3.097 )"  );
    frame = frame.Define( "photpt", "Photon_Pt[0]/1000.0"  );
    frame = frame.Define( "jpsipt", "DiLept_Pt[0]/1000.0"  );
    frame = frame.Filter( "abs( muneg_eta ) < 2.4" );
    frame = frame.Filter( "abs( mupos_eta ) < 2.4" );
    frame = frame.Filter( "abs( photon_eta ) < 2.4" );
    frame.Snapshot( "tree", "./chi_snapshot.root", "" );
  }


 
  TFile * file = new TFile( "./chi_snapshot.root", "READ" );
  TTree * tree = static_cast<TTree *>( file->Get( "tree" ) );

  std::string tau_cut =   "abs(DiMuonVertex_Tau[0])<0.1";
  std::string minv_cut =  "minvres>3.4&&minvres<4.0";
  std::string minvtau_cut = tau_cut + "&&" + minv_cut;
  std::string minvn_cut = "minv>3.4&&minv<4.0";
  std::string minvntau_cut = tau_cut + "&&" + minvn_cut;

  int minvres_bins = 30;
  double minvres_bins_dbl = 1.0*minvres_bins;
  double minbin= 3.4;
  double maxbin= 4.0;
  double bin_width_gev = (maxbin-minbin)/minvres_bins_dbl;

  TH1F * peak_fit = new TH1F( "peak_fit", "", minvres_bins, minbin, maxbin );
  TH1F * asym_fit2 = new TH1F( "asym_fit_2", "", minvres_bins*2, minbin, maxbin );
  TH1F * nres_fit = new TH1F( "nres_fit", "", minvres_bins*2, minbin, maxbin );
  TH1F * mc0 = new TH1F( "mc0", "mc0", 100, 2.9, 4.7 ); 
  TH1F * mc1 = new TH1F( "mc1", "mc1", 100, 2.9, 4.7 ); 
  TH1F * mc2 = new TH1F( "mc2", "mc2", 100, 2.9, 4.7 );
  TH2F * mc3 = new TH2F( "mc3", "mc3", 100, 0, 1, 100, 0, 1 );
  TH2F * mc4 = new TH2F("mc4", "mc4", 100, 0, 1, 100, 0, 1 );
  TH2F * mc5 = new TH2F("mc5", "mc5", 100, 5, 20, 100, 10, 40 );

  std::string cut = "abs(DiMuonVertex_Tau[0])<0.1";
  tree->Draw( "minv>>mc0", cut.c_str(), "e goff" );
  cut += "&&(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)";
  tree->Draw( "minv>>mc1", cut.c_str(), "e goff" );
  tree->Draw( "minvres>>mc2", cut.c_str(), "e goff" );
  tree->Draw( "deltaPhi_mu:deltaEta_mu>>mc3", cut.c_str(), "e goff" );
  cut += "&&(minvres<3.6)";
  tree->Draw( "deltaPhi_mu:deltaEta_mu>>mc4", cut.c_str(), "e goff" );
  tree->Draw( "jpsipt:photpt>>mc5", cut.c_str(), "e goff" );
  TH1D * phot = mc5->ProjectionX();
  TH1D * jpsi = mc5->ProjectionY();

  //std::string photon_cut = "&&(photpt<6)";
  std::string photon_cut = "&&(photpt>6)";
  //std::string photon_cut = "";

  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvtau_cut + photon_cut;
  tree->Draw( "minvres>>peak_fit", cut.c_str(), "e goff" );
  tree->Draw( "minvres>>asym_fit_2", cut.c_str(), "e goff" );
  
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvntau_cut + photon_cut;
  tree->Draw( "minv>>nres_fit", cut.c_str(), "e goff" );


  TH1F * minvres_full = new TH1F( "minvres_full", "", 60, 3.4, 4.0 ); 
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvtau_cut;
  tree->Draw( "minvres>>minvres_full", cut.c_str(), "e goff" );
  TH1F * minvres_abv6 = new TH1F( "minvres_abv6", "", 60, 3.4, 4.0 );
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvtau_cut + "&&(photonpt>6)";
  tree->Draw( "minvres>>minvres_abv6", cut.c_str(), "e goff" );
  TH1F * minvres_blw6 = new TH1F( "minvres_blw6", "", 60, 3.4, 4.0 );
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvtau_cut + "&&(photonpt<6)";
  tree->Draw( "minv>>minvres_blw6", cut.c_str(), "e goff" );
  TH1F * minvnoc_full = new TH1F( "minvnoc_full", "", 60, 3.4, 4.0 );
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvntau_cut;
  tree->Draw( "minv>>minvnoc_full", cut.c_str(), "e goff" );
  TH1F * minvnoc_abv6 = new TH1F( "minvnoc_abv6", "", 60, 3.4, 4.0 );
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvntau_cut + "&&(photonpt>6)";
  tree->Draw( "minv>>minvnoc_abv6", cut.c_str(), "e goff" );
  TH1F * minvnoc_blw6 = new TH1F( "minvnoc_blw6", "", 60, 3.4, 4.0 );
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + minvntau_cut + "&&(photonpt<6)";
  tree->Draw( "minv>>minvnov_blw6", cut.c_str(), "e goff" );

  TFile * output_store = new TFile( "./store.root", "RECREATE" );
  output_store->cd();

  minvres_full->Write();
  minvres_abv6->Write();
  minvres_blw6->Write();
  minvnoc_full->Write();
  minvnoc_abv6->Write();
  minvnoc_blw6->Write();

  output_store->Close();
  delete minvres_full;
  delete minvres_abv6;
  delete minvres_blw6;
  delete minvnoc_full;
  delete minvnoc_abv6;
  delete minvnoc_blw6;

  gStyle->SetOptStat( "ieoumrn" );
  TCanvas canvas( "", "", 100, 100, 3000, 1000 );
  canvas.Divide( 3, 1 );
  canvas.cd( 1 );

  mc0->SetName( "M_{inv, #mu#mu#gamma}" );
  mc0->Draw( "HIST" );
  set_axis_labels( mc0, "M_{inv} (GeV)", "Entries" );
  TPaveStats * mc0_stats = make_stats( mc0 );
  mc0_stats->Draw();

  canvas.cd( 2 );
  mc1->SetName( "With Photon Quality, M_{#mu#mu}" );
  mc1->Draw( "HIST" );
  set_axis_labels( mc1, "M_{inv} (GeV)", "Entries" );
  TPaveStats * mc1_stats = make_stats( mc1 );
  mc1_stats->Draw();

  canvas.cd( 3 );
  mc2->SetName( "M_{inv,res}" );
  mc2->Draw( "HIST" );
  set_axis_labels( mc2, "M_{inv} (GeV)", "Entries" );
  TPaveStats * mc2_stats = make_stats( mc2 );
  mc2_stats->Draw();

  canvas.SaveAs( "plots/fast_first_3.pdf" );

  canvas.Clear();
  canvas.Divide( 3, 1 );
    
  canvas.cd( 1 );
  mc5->SetName( "jpsi-photon pt map" );
  mc5->Draw( "COLZ" );
  set_axis_labels( mc5, "p_{T,j/#gamma}", "p_{T,j/#psi}" );
  TPaveStats * mc5_stats = make_stats( mc5 );
  mc5_stats->Draw();

  canvas.cd( 2 );
  phot->SetName( "photon" );
  phot->Draw( "HIST" );
  set_axis_labels( phot, "p_{T,#gamma}", "Entries" );
  TPaveStats * phot_stats = make_stats( phot );
  phot_stats->Draw();

  canvas.cd( 3 );
  jpsi->SetName( "jpsi" );
  jpsi->Draw( "COLZ" );
  set_axis_labels( jpsi, "p_{T,j/#psi}", "Entries" );
  TPaveStats * jpsi_stats = make_stats( jpsi );
  jpsi_stats->Draw();

  canvas.SaveAs( "plots/fast_last_3.pdf" );

  //canvas.cd( 4 );
  //mc3->SetName( "eta-phi" );
  //mc3->Draw( "COLZ" );
  //set_axis_labels( mc3, "#Delta#Phi", "#Delta#eta" );
  //TPaveStats * mc3_stats = make_stats( mc3 );
  //mc3_stats->Draw();

  //canvas.cd( 5 );
  //mc4->SetName( "eta-phi-chi" );
  //mc4->Draw( "COLZ" );
  //set_axis_labels( mc4, "#Delta#Phi", "#Delta#eta" );
  //TPaveStats * mc4_stats = make_stats( mc4 );
  //mc4_stats->Draw();


  TCanvas * c23 = new TCanvas( "c23", "", 200, 200, 1000, 1000 );
  TH1F * minvres_extra = new TH1F( "minvres_extra", "", 60, 3, 30 ); 
  cut = "(Photon_quality[0] == 0)&&(mumu_mass > 2.8 && mumu_mass < 3.4)&&" + tau_cut;
  tree->Draw( "minvres>>minvres_extra", cut.c_str(), "e goff" );
  c23->Divide( 1, 1 );
  TPad * apad = static_cast<TPad*>( c23->cd( 1 ) );
  minvres_extra->Draw( "HIST" );
  hist_prep_axes( minvres_extra );
  add_atlas_decorations( apad);
  set_axis_labels( minvres_extra, "M_{inv,res}", "Events" );
  TPaveStats * full_stats = make_stats( minvres_extra );
  full_stats->Draw();
  c23->SaveAs( "plots/full.pdf" );



  TFile * chi_file = new TFile( "./chi.root", "RECREATE" );
  mc0->Write(); mc1->Write(); mc2->Write();
  mc3->Write(); mc4->Write(); mc5->Write();
  phot->Write(); jpsi->Write();
  chi_file->Close();

  TH1F * asym_fit = static_cast<TH1F *>( peak_fit->Clone() );
  TH1F * asym_fit3 = static_cast<TH1F *>( asym_fit2->Clone() );
  TH1F * asym_fit4 = static_cast<TH1F *>( asym_fit2->Clone() );
  TH1F * asym_fit5 = static_cast<TH1F *>( asym_fit2->Clone() );
  TH1F * asym_fit7 = static_cast<TH1F *>( asym_fit2->Clone() );
  TH1F * asym_fit9 = static_cast<TH1F *>( asym_fit2->Clone() );

  //TH1F * nres_fit3 = static_cast<TH1F*>( nres_fit->Clone( "Minv3" )  );
  //TH1F * asym_fit2 = static_cast<TH1F *>( peak_fit->Clone() );
  //TH1F * asym_fit2 = static_cast<TH1F *>( peak_fit->Clone() );
  

  //TF1 * dual_gaussian = new TF1( "dualgaus", "gausn(0) + gausn(3)", 3.4, 3.8 );
  //TF1 * dual_gaussian = new TF1( "dualgaus", "gausn(0) + landau(3) + gausn(6)", 3.4, 3.8 );

  //TF1 * dual_gaussian = new TF1( "dgl", "[0]*e^(-0.5*((x-[1])/[2])^2)/(sqrt(2*pi)*[2]) + gausn(3) + [6]*e^(-0.5*((x-[7])/[2])^2)/(sqrt(2*pi)*[2])", 3.4, 4.0 );
  //TF1 * bckg_gauss = new TF1( "bckg_gauss", "gausn", 3.4, 4.0 );
  //TF1 * sign_gauss = new TF1( "sign_gauss", asymmetric_gaussian, 3.4, 4.0, 4, 1, TF1::EAddToList::kAdd );
  
  //TF1 * asymmetric_gaussian * agaus = new asymmetric_gaussian();
  //auto func = new TF1( "agaus", agaus, &asymmetric_gaussian::Evaluate, 
  //                      3.4, 4.0 , 7, 
  //                      "agaus", "Evaluate", TF1::EAddToList::kAdd );   // create TF1 class.
  //func->Print();


  //sign_gauss->SetParNames()
  //sign_gauss->SetParNames( "a_{\\chi}","x_{\\chi}","\\sigma_{l}","\\sigma_{r}" );
  //sign_gauss->SetParameter( 0, 15 );
  //sign_gauss->SetParLimits( 0, 5, 150 );
  //sign_gauss->SetParameter( 1, 3.525 );
  //sign_gauss->SetParLimits( 1, 3.45, 3.6 );
  //sign_gauss->SetParameter( 2, 0.04 );
  //sign_gauss->SetParLimits( 2, 0.01, 0.1 );
  //sign_gauss->SetParameter( 3, 0.04 );
  //sign_gauss->SetParLimits( 3, 0.01, 0.1 );



  //sign_gauss->AddToGlobalList();
  //sign_gauss->Print();

  //std::cout << sign_gauss->Eval( 3.7 ) << std::endl;

  TF1 * asym_gauss = new TF1( "asym", agaus_bckg, 3.4, 4.0, 7 );
  
  asym_gauss->SetParNames( "a_{\\chi}","x_{\\chi}","\\sigma_{l}","\\sigma_{r}", 
                           "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );
  asym_gauss->SetParameter( 0, 15 );
  asym_gauss->SetParLimits( 0, 5, 150 );
  asym_gauss->SetParameter( 1, 3.525 );
  asym_gauss->SetParLimits( 1, 3.45, 3.6 );
  asym_gauss->SetParameter( 2, 0.04 );
  asym_gauss->SetParLimits( 2, 0.01, 0.1 );
  asym_gauss->SetParameter( 3, 0.04 );
  asym_gauss->SetParLimits( 3, 0.01, 0.1 );
  asym_gauss->SetParameter( 4, 5 );
  asym_gauss->SetParameter( 5, 3.9 );
  asym_gauss->SetParLimits( 5, 3.6, 4 );
  asym_gauss->SetParameter( 6, 0.1 );
  asym_gauss->Print();

  


  //asym_gauss->SetParNames( "a_{\\chi}","x_{\\chi}","\\sigma_{l}","\\sigma_{r}", 
  //                         "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );

  TF1 * asym_2gauss = new TF1( "asym2", agaus_2_bckg, 3.4, 4.0, 9 );
  asym_2gauss->SetParNames( "a_{\\chi1,2}","x_{\\chi1}","\\sigma_{\\chi1}","x_{\\chi2}","\\sigma_{\\chi2l}","\\sigma_{\\chi2r}", 
                           "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );

  asym_2gauss->SetParameter( 0, 5 );
  asym_2gauss->SetParLimits( 0, 5, 150 );
  asym_2gauss->SetParameter( 1, 3.511 );
  asym_2gauss->SetParLimits( 1, 3.511, 3.511 );
  asym_2gauss->SetParameter( 2, 0.02 );
  asym_2gauss->SetParLimits( 2, 0.005, 0.1 );
  asym_2gauss->SetParameter( 3, 3.556 );
  asym_2gauss->SetParLimits( 3, 3.556, 3.556 );
  asym_2gauss->SetParameter( 4, 0.02 );
  asym_2gauss->SetParLimits( 4, 0.005, 0.1 );
  asym_2gauss->SetParameter( 5, 0.02 );
  asym_2gauss->SetParLimits( 5, 0.005, 0.1 );
  asym_2gauss->SetParameter( 6, 5 );
  asym_2gauss->SetParameter( 6, 3.9 );
  asym_2gauss->SetParLimits( 7, 3.6, 4 );
  asym_2gauss->SetParameter( 7, 0.1 );
  asym_2gauss->SetParameter( 8, 0.165 );
  asym_2gauss->Print();


  TF1 * asym2pin_gauss = new TF1( "apin", agaus2pin_bckg, 3.4, 4.0, 8 );
  asym2pin_gauss->SetParNames( "a_{\\chi1}","x_{\\chi}","\\sigma_{\\chi1}",
                                "\\sigma_{\\chi2l}","\\sigma_{\\chi2r}", 
                                "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );

  // set the chi 1 ( and chi2 amplitude )
  asym2pin_gauss->SetParameter( 0, 5 );
  asym2pin_gauss->SetParLimits( 0, 1, 150 );
  asym2pin_gauss->SetParameter( 1, 3.511 );
  asym2pin_gauss->SetParLimits( 1, 3.411, 3.611 );
  asym2pin_gauss->SetParameter( 2, 0.02 );
  asym2pin_gauss->SetParLimits( 2, 0.005, 0.1 );

  // set the chi2
  asym2pin_gauss->SetParameter( 3, 0.02 );
  asym2pin_gauss->SetParLimits( 3, 0.005, 0.1 );
  asym2pin_gauss->SetParameter( 4, 0.02 );
  asym2pin_gauss->SetParLimits( 4, 0.005, 0.1 );

  // set the bckg
  asym2pin_gauss->SetParameter( 5, 5 );
  asym2pin_gauss->SetParameter( 5, 3.9 );
  asym2pin_gauss->SetParLimits( 6, 3.6, 4 );
  asym2pin_gauss->SetParameter( 6, 0.1 );
  asym2pin_gauss->SetParameter( 7, 0.165 );
  asym2pin_gauss->Print();

  TF1 * dual_pin_agaus= new TF1( "dual_apin", dual_agaus_pin_bckg, 3.4, 4.0, 10 );
  dual_pin_agaus->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}","\\sigma_{\\chi1l}","\\sigma_{\\chi1r}",
                                "\\sigma_{\\chi2l}","\\sigma_{\\chi2r}", 
                                "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );

  // set the chi 1 ( and chi2 amplitude )
  dual_pin_agaus->SetParameter( 0, 5 );
  dual_pin_agaus->SetParLimits( 0, 1, 50 );
  dual_pin_agaus->SetParameter( 1, 1.0 );
  //dual_pin_agaus->SetParLimits( 1, 1.0, 1.0 );
  dual_pin_agaus->SetParLimits( 1, 0.9, 1.1 );

  dual_pin_agaus->SetParameter( 2, 3.511 );
  dual_pin_agaus->SetParLimits( 2, 3.411, 3.611 );

  dual_pin_agaus->SetParameter( 3, 0.02 );
  dual_pin_agaus->SetParLimits( 3, 0.005, 0.1 );
  dual_pin_agaus->SetParameter( 4, 0.02 );
  dual_pin_agaus->SetParLimits( 4, 0.005, 0.1 );
  dual_pin_agaus->SetParameter( 5, 0.02 );
  dual_pin_agaus->SetParLimits( 5, 0.005, 0.1 );
  dual_pin_agaus->SetParameter( 6, 0.02 );
  dual_pin_agaus->SetParLimits( 6, 0.005, 0.2 );

  dual_pin_agaus->SetParameter( 7, 10 );
  dual_pin_agaus->SetParLimits( 7, 3, 10000 );
  dual_pin_agaus->SetParameter( 8, 3.8 );
  dual_pin_agaus->SetParLimits( 8, 3.6, 4 );
  dual_pin_agaus->SetParameter( 9, 0.165 );
  dual_pin_agaus->Print();

  TF1 * dag_params_fn= new TF1( "dag_param_fn", dag_param, 3.4, 4.0, 10 );
  dag_params_fn->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}","\\sigma_{\\chi1l}","\\sigma_{\\chi1r}",
                                "\\sigma_{\\chi2l}","\\sigma_{\\chi2r}", 
                                "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );
  dag_params_fn->SetParameter( 0, 40 );
  dag_params_fn->SetParLimits( 0, 0, 100 );
  dag_params_fn->SetParameter( 1, 1.0 );
  dag_params_fn->SetParLimits( 1, 0.5, 1.5 );
  dag_params_fn->SetParameter( 2, 3.511 );
  dag_params_fn->SetParLimits( 2, 3.411, 3.611 );
  dag_params_fn->SetParameter( 3, 0.02 );
  dag_params_fn->SetParLimits( 3, 0.005, 0.1 );
  dag_params_fn->SetParameter( 4, 0.02 );
  dag_params_fn->SetParLimits( 4, 0.005, 0.1 );
  dag_params_fn->SetParameter( 5, 0.02 );
  dag_params_fn->SetParLimits( 5, 0.005, 0.1 );
  dag_params_fn->SetParameter( 6, 0.02 );
  dag_params_fn->SetParLimits( 6, 0.005, 0.2 );
  dag_params_fn->SetParameter( 7, 10 );
  dag_params_fn->SetParLimits( 7, 3, 100 );
  dag_params_fn->SetParameter( 8, 3.8 );
  dag_params_fn->SetParLimits( 8, 3.6, 4 );
  dag_params_fn->SetParameter( 9, 0.165 );


  TF1 * dag_exp_fn = new TF1( "dag_exp_fn", dag_exp, 3.4, 4.0, 11 );
  dag_exp_fn->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}",
                            "\\sigma_{\\chi1l}","\\sigma_{\\chi1r}",
                            "\\sigma_{\\chi2l}","\\sigma_{\\chi2r}", 
                            "a_{bckg}", "b_{bckg}", "c_{bckg}", "d_{bckg}" );
  dag_exp_fn->SetParameter( 0, 40 );
  dag_exp_fn->SetParLimits( 0, 0, 100 );
  dag_exp_fn->SetParameter( 1, 1.0 );
  dag_exp_fn->SetParLimits( 1, 0.5, 1.5 );
  dag_exp_fn->SetParameter( 2, 3.511 );
  dag_exp_fn->SetParLimits( 2, 3.5, 3.58 );
  dag_exp_fn->SetParameter( 3, 0.02589 );
  dag_exp_fn->SetParLimits( 3, 0.005, 0.05 );
  dag_exp_fn->SetParameter( 4, 0.04237 );
  dag_exp_fn->SetParLimits( 4, 0.005, 0.05 );
  dag_exp_fn->SetParameter( 5, 0.06061 );
  dag_exp_fn->SetParLimits( 5, 0.005, 0.1);
  dag_exp_fn->SetParameter( 6, 0.09301 );
  dag_exp_fn->SetParLimits( 6, 0.005, 0.1 );

  // the current shitemare
  dag_exp_fn->SetParameter( 7, 30 );
  //dag_exp_fn->SetParLimits( 7, 1, 100 );
  //dag_exp_fn->SetParameter( 8, 3.40 );
  //dag_exp_fn->SetParLimits( 8, 3.40, 3.40 );
  //dag_exp_fn->SetParameter( 8, -10 );
  //dag_exp_fn->SetParLimits( 8, -10, -10 );
  dag_exp_fn->SetParameter( 8, 0 );
  dag_exp_fn->SetParLimits( 8, 0, 3.1 );
  dag_exp_fn->SetParameter( 9, 1 );
  dag_exp_fn->SetParLimits( 9, 0, 50 );
  dag_exp_fn->SetParameter( 10, 1 );
  dag_exp_fn->SetParLimits( 10, 0, 50 );

  // best so far - 53.04/49
  //dag_exp_fn->SetParameter( 7, 1e10 );
  //dag_exp_fn->SetParLimits( 7, 1e9, 1e12 );
  //dag_exp_fn->SetParameter( 8, 3.40 );
  //dag_exp_fn->SetParLimits( 8, 3.40, 3.40 );
  //dag_exp_fn->SetParameter( 9, 12 );
  //dag_exp_fn->SetParLimits( 9, 10, 14 );
  //dag_exp_fn->SetParameter( 10, 22 );
  //dag_exp_fn->SetParLimits( 10, 15, 25.0 );

  // best so far - 58.52/49
  //dag_exp_fn->SetParameter( 7, 1e8 );
  //dag_exp_fn->SetParLimits( 7, 0.1, 1e9 );
  //dag_exp_fn->SetParameter( 8, 3.4 );
  //dag_exp_fn->SetParLimits( 8, 3.4, 3.4 );
  //dag_exp_fn->SetParameter( 9, 8 );
  //dag_exp_fn->SetParLimits( 9, 7, 10 );
  //dag_exp_fn->SetParameter( 10, 15 );
  //dag_exp_fn->SetParLimits( 10, 12, 18.0 );

  // best so far - 60.5/49
  //dag_exp_fn->SetParameter( 7, 100 );
  //dag_exp_fn->SetParLimits( 7, 0.1, 1e8 );
  //dag_exp_fn->SetParameter( 8, 3.4 );
  //dag_exp_fn->SetParLimits( 8, 3.4, 3.4 );
  //dag_exp_fn->SetParameter( 9, 8 );
  //dag_exp_fn->SetParLimits( 9, 7, 10 );
  //dag_exp_fn->SetParameter( 10, 15 );
  //dag_exp_fn->SetParLimits( 10, 12, 18.0 );

  // best so far - 62.19/49
  //dag_exp_fn->SetParameter( 7, 100 );
  //dag_exp_fn->SetParLimits( 7, 0.1, 1e8 );
  //dag_exp_fn->SetParameter( 8, 3.4 );
  //dag_exp_fn->SetParLimits( 8, 3.4, 3.4 );
  //dag_exp_fn->SetParameter( 9, 7 );
  //dag_exp_fn->SetParLimits( 9, 6, 8 );
  //dag_exp_fn->SetParameter( 10, 13 );
  //dag_exp_fn->SetParLimits( 10, 7, 15.0 );

  // best so far - 65.14/49
  //dag_exp_fn->SetParameter( 7, 100 );
  //dag_exp_fn->SetParLimits( 7, 0.1, 1e8 );
  //dag_exp_fn->SetParameter( 8, 3.4 );
  //dag_exp_fn->SetParLimits( 8, 3.4, 3.4 );
  //dag_exp_fn->SetParameter( 9, 6 );
  //dag_exp_fn->SetParLimits( 9, 4, 7 );
  //dag_exp_fn->SetParameter( 10, 10 );
  //dag_exp_fn->SetParLimits( 10, 7, 13.0 );

  TF1 * dag_clrs_fn = new TF1( "dual_apin", dag_clrs, 3.4, 4.0, 8 );
  dag_clrs_fn->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}","\\sigma_{\\chil}","\\sigma_{\\chir}",
                                "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );
  dag_clrs_fn->SetParameter( 0, 5 );
  dag_clrs_fn->SetParLimits( 0, 1, 50 );
  dag_clrs_fn->SetParameter( 1, 1.0 );
  dag_clrs_fn->SetParLimits( 1, 0.1, 3 );

  dag_clrs_fn->SetParameter( 2, 3.511 );
  dag_clrs_fn->SetParLimits( 2, 3.411, 3.611 );

  dag_clrs_fn->SetParameter( 3, 0.02 );
  dag_clrs_fn->SetParLimits( 3, 0.005, 0.1 );
  dag_clrs_fn->SetParameter( 4, 0.02 );
  dag_clrs_fn->SetParLimits( 4, 0.005, 0.3 );

  dag_clrs_fn->SetParameter( 5, 10 );
  dag_clrs_fn->SetParLimits( 5, 3, 100 );
  dag_clrs_fn->SetParameter( 6, 3.8 );
  dag_clrs_fn->SetParLimits( 6, 3.6, 4 );
  dag_clrs_fn->SetParameter( 7, 0.165 );
  dag_clrs_fn->Print();

   TF1 * dag_lnd_fn = new TF1( "dual_apin", dag_landau, 3.4, 4.0, 9 );
  dag_lnd_fn->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}",
                            "\\sigma_{\\chi1l}","\\sigma_{\\chi1r}",
                            "\\sigma_{\\chi2l}","\\sigma_{\\chi2r}",
                            "L_{1}", "L_{2}" );

  dag_lnd_fn->SetParameter( 0, 5 );
  dag_lnd_fn->SetParLimits( 0, 1, 100 );
  dag_lnd_fn->SetParameter( 1, 1.0 );
  dag_lnd_fn->SetParLimits( 1, 0.9, 1.1 );
  dag_lnd_fn->SetParameter( 2, 3.511 );
  dag_lnd_fn->SetParLimits( 2, 3.411, 3.611 );

  dag_lnd_fn->SetParameter( 3, 0.02 );
  dag_lnd_fn->SetParLimits( 3, 0.005, 0.1 );
  dag_lnd_fn->SetParameter( 4, 0.02 );
  dag_lnd_fn->SetParLimits( 4, 0.005, 0.1 );
  dag_lnd_fn->SetParameter( 5, 0.02 );
  dag_lnd_fn->SetParLimits( 5, 0.005, 0.1 );
  dag_lnd_fn->SetParameter( 6, 0.02 );
  dag_lnd_fn->SetParLimits( 6, 0.005, 0.1 );

  dag_lnd_fn->SetParameter( 7, 3.9 );
  dag_lnd_fn->SetParameter( 8, 1 );
  dag_lnd_fn->Print();



  TF1 * dual_gaussian = new TF1( "dgl", "[0]*e^(-0.5*((x-[1])/[2])^2)/(sqrt(2*pi)*[2]) + gausn(3)", 3.4, 4.0 );
  dual_gaussian->SetParameter( 0, 15 );
  dual_gaussian->SetParLimits( 0, 5, 150 );
  dual_gaussian->SetParameter( 1, 3.525 );
  dual_gaussian->SetParLimits( 1, 3.45, 3.6 );
  dual_gaussian->SetParameter( 2, 0.04 );
  dual_gaussian->SetParLimits( 2, 0.01, 0.1 );
  dual_gaussian->SetParameter( 3, 5 );
  dual_gaussian->SetParameter( 4, 3.9 );
  dual_gaussian->SetParLimits( 4, 3.6, 4 );
  dual_gaussian->SetParameter( 5, 0.1 );
  //dual_gaussian->SetParameter( 3, 558 );
  //dual_gaussian->SetParameter( 4, 3.7 );
  //dual_gaussian->SetParameter( 5, 0.08 );
  //dual_gaussian->SetParameter( 6, 12 );
  //dual_gaussian->SetParLimits( 6, 5, 90 );
  //dual_gaussian->SetParameter( 7, 3.556 );
  //dual_gaussian->SetParLimits( 7, 3.556, 3.556 );
  //dual_gaussian->SetParLimits( 2, 0.01, 0.1 );
  //dual_gaussian->SetParameter( 8, 0.1 );
  //dual_gaussian->SetParLimits( 8, 0.01, 0.1 );
  //dual_gaussian->SetParameter( 3, 3.75 );
  //dual_gaussian->SetParLimits( 3, 3.6, 3.8 );
  //dual_gaussian->SetParameter( 4, 0.2 );
  //dual_gaussian->SetParLimits( 4, 0.01, 0.5 );
  dual_gaussian->SetLineColorAlpha( kRed+1, 1.0 );
  dual_gaussian->SetLineWidth( 1.0 );
  dual_gaussian->SetLineStyle( 1 );
  peak_fit->Fit( dual_gaussian, "M", "", 3.4, 4.0 );

  TF1 * res_gausn = new TF1( "rg", "gausn(0) + gausn(3)", 3.4, 4.0 );
  res_gausn->SetParameter( 0, 15 );
  res_gausn->SetParLimits( 0, 5, 150 );
  res_gausn->SetParameter( 1, 3.525 );
  res_gausn->SetParLimits( 1, 3.45, 3.6 );
  res_gausn->SetParameter( 2, 0.04 );
  res_gausn->SetParLimits( 2, 0.01, 0.1 );
  res_gausn->SetParameter( 3, 5 );
  res_gausn->SetParameter( 4, 3.9 );
  res_gausn->SetParLimits( 4, 3.6, 4 );
  res_gausn->SetParameter( 5, 0.1 );


  //TF1 * nres_3g = new TF1( "nres_3g", "gausn(0) + gausn(3) + gausn(6)", 3.4, 4.0 );
  TF1 * nres_3g = new TF1( "nres_3g", gauss3, 3.4, 4.0, 8 );
  nres_3g->SetParameter( 0, 15 );
  nres_3g->SetParLimits( 0, 5, 150 );
  nres_3g->SetParameter( 1, 1.0);
  nres_3g->SetParLimits( 1, 0.5, 1.5 );
  nres_3g->SetParameter( 2, 3.5 );
  nres_3g->SetParLimits( 2, 3.4, 3.6 );
  nres_3g->SetParameter( 3, 0.04 );
  nres_3g->SetParLimits( 3, 0.01, 0.1 );
  nres_3g->SetParameter( 4, 0.04 );
  nres_3g->SetParLimits( 4, 0.01, 0.1 );
  nres_3g->SetParameter( 5, 5 );
  nres_3g->SetParameter( 6, 3.9 );
  nres_3g->SetParLimits( 6, 3.6, 4 );
  nres_3g->SetParameter( 7, 0.1 );
  nres_3g->SetParNames( "a_{\\chi1}","r_{chi12}","\\x_{\\chi}",
                        "\\sigma_{\\chi1}","\\sigma_{\\chi2}",
                        "a_{b}","x_{b}","\\sigma_{b}" );
  

  TF1 * gauss1 = new TF1( "fg", "gausn(0)", 3.4, 4.0 );
  //TF1 * gauss2 = new TF1( "sg", "gausn(0)", 3.4, 4.0 );
  TF1 * landau = new TF1( "sg", "gausn(0)", 3.4, 4.0 );
  gauss1->SetLineColorAlpha( kBlue+1, 1.0 );
  //gauss2->SetLineColorAlpha( kMagenta+1, 1.0 );
  landau->SetLineColorAlpha( kGreen+1, 1.0 );

  gauss1->SetLineStyle( 2 );
  //gauss2->SetLineStyle( 2 );
  landau->SetLineStyle( 2 );

  gauss1->SetParameter( 0, dual_gaussian->GetParameter( 0 ) );
  gauss1->SetParameter( 1, dual_gaussian->GetParameter( 1 ) );
  gauss1->SetParameter( 2, dual_gaussian->GetParameter( 2 ) );
  //gauss2->SetParameter( 0, dual_gaussian->GetParameter( 3 ) );
  //gauss2->SetParameter( 1, dual_gaussian->GetParameter( 4 ) );
  //gauss2->SetParameter( 2, dual_gaussian->GetParameter( 5 ) );
  landau->SetParameter( 0, dual_gaussian->GetParameter( 3 ) );
  landau->SetParameter( 1, dual_gaussian->GetParameter( 4 ) );
  landau->SetParameter( 2, dual_gaussian->GetParameter( 5 ) );
  //gauss2->SetParameter( 2, dual_gaussian->GetParameter( 5 ) );
  //gauss2->SetParameter( 0, dual_gaussian->GetParameter( 6 ) );
  //gauss2->SetParameter( 1, dual_gaussian->GetParameter( 7 ) );
  //gauss2->SetParameter( 2, dual_gaussian->GetParameter( 2 ) );

  

  TCanvas * peak_fit_canvas = new TCanvas( "pfc", "", 200, 200, 4000, 3000 );
  peak_fit_canvas->Divide( 4, 3 );
  gStyle->SetOptStat( "ieoumrn" );
  gStyle->SetOptFit( 1 );

  TPad * active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 1 ) );
  peak_fit->Draw( "HIST E1" );
  hist_prep_axes( peak_fit );
  add_atlas_decorations( active_pad );
  set_axis_labels( peak_fit, "M_{inv,res}", "Events" );
  dual_gaussian->Draw( "SAME" );
  gauss1->Draw( "SAME" ); 
  landau->Draw( "SAME" ); 
  peak_fit->SetName( "M_{inv,res}" );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( peak_fit, "M_{inv,res}" );
  legend->AddEntry( dual_gaussian, "total" );
  legend->AddEntry( gauss1, "#chi_{c}" );
  //legend->AddEntry( gauss2, "#chi_{c2}" );
  legend->AddEntry( landau, "bckg" );
  legend->Draw("SAME");
  TPaveStats * peak_fit_stats = make_stats( peak_fit );
  peak_fit_stats->Draw( "SAME" );
  

  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 2 ) );
  asym_fit->Draw( "HIST E1" );
  asym_fit->SetName( "M_{inv,res}" );
  asym_fit->Fit( asym_gauss, "M", "", 3.4, 4.0 );
  asym_gauss->Draw( "SAME" );
  TF1 * asymn_sign = new TF1( "asymn_sign", agaus, 3.4, 4.0, 4 );
  asymn_sign->SetParameter( 0, asym_gauss->GetParameter( 0 ) );
  asymn_sign->SetParameter( 1, asym_gauss->GetParameter( 1 ) );
  asymn_sign->SetParameter( 2, asym_gauss->GetParameter( 2 ) );
  asymn_sign->SetParameter( 3, asym_gauss->GetParameter( 3 ) );
  TF1 * gausn_bckg = new TF1( "gausn_bckg", "gausn", 3.4, 4.0 );
  gausn_bckg->SetParameter( 0, asym_gauss->GetParameter( 4 ) );
  gausn_bckg->SetParameter( 1, asym_gauss->GetParameter( 5 ) );
  gausn_bckg->SetParameter( 2, asym_gauss->GetParameter( 6 ) );
  asymn_sign->SetLineColorAlpha( kMagenta+1, 1.0 );
  gausn_bckg->SetLineColorAlpha( kGreen+1, 1.0 );
  asymn_sign->SetLineStyle( 2 ); 
  gausn_bckg->SetLineStyle( 2 );  
  asymn_sign->SetLineWidth( 1 );
  gausn_bckg->SetLineWidth( 1 );
  asymn_sign->Draw( "SAME" );  
  gausn_bckg->Draw( "SAME" );
  hist_prep_axes( asym_fit );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit, "M_{inv,res}", "Events" );
  TLegend * alegend = below_logo_legend();
  alegend->AddEntry( asym_fit, "M_{inv,res}" );
  alegend->AddEntry( asym_gauss, "asymn total" );
  alegend->AddEntry( asymn_sign, "asymn sign" );
  alegend->AddEntry( gausn_bckg, "gausn bckg" );
  alegend->Draw("SAME");
  TPaveStats * asym_fit_stats = make_stats( asym_fit );
  asym_fit_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 3 ) );
  asym_fit2->Draw( "HIST E1" );
  asym_fit2->SetName( "M_{inv,res}" );
  asym_fit2->Fit( asym2pin_gauss, "M", "", 3.4, 4.0 );
  asym2pin_gauss->Draw( "SAME" );
  TF1 * asymn_chi1  = new TF1( "asymn_chi1", "gausn(0)",   3.4, 4.0 );
  TF1 * asymn_chi2  = new TF1( "asymn_chi2", agaus,     3.4, 4.0, 4 );
  TF1 * gausn_bckg2 = new TF1( "asymn_bckg2", "gausn(0)",  3.4, 4.0 );
  asymn_chi1->SetParameter( 0, asym2pin_gauss->GetParameter( 0 ) );
  asymn_chi1->SetParameter( 1, asym2pin_gauss->GetParameter( 1 ) );
  asymn_chi1->SetParameter( 2, asym2pin_gauss->GetParameter( 2 ) );
  asymn_chi2->SetParameter( 0, asym2pin_gauss->GetParameter( 0 ) );
  asymn_chi2->SetParameter( 1, asym2pin_gauss->GetParameter( 1 ) + 0.045 );
  asymn_chi2->SetParameter( 2, asym2pin_gauss->GetParameter( 3 ) );
  asymn_chi2->SetParameter( 3, asym2pin_gauss->GetParameter( 4 ) );
  gausn_bckg2->SetParameter( 0, asym2pin_gauss->GetParameter( 5 ) );
  gausn_bckg2->SetParameter( 1, asym2pin_gauss->GetParameter( 6 ) );
  gausn_bckg2->SetParameter( 2, asym2pin_gauss->GetParameter( 7 ) );
  //std::cout << "chi1 integral: " << asymn_chi1->Integral( 3.4, 4.0 ) << std::endl;
  //std::cout << "chi2 integral: " << asymn_chi2->Integral( 3.4, 4.0 ) << std::endl; 
  //std::cout << "bckg integral: " << gausn_bckg2->Integral( 3.4, 4.0 ) << std::endl;
  asymn_chi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  asymn_chi1->SetLineWidth( 1 );
  asymn_chi1->SetLineStyle( 2 ); 
  asymn_chi2->SetLineColorAlpha( kOrange+1, 1.0 );
  asymn_chi2->SetLineWidth( 1 );
  asymn_chi2->SetLineStyle( 2 ); 
  gausn_bckg2->SetLineColorAlpha( kGreen+1, 1.0 );
  gausn_bckg2->SetLineStyle( 2 );  
  gausn_bckg2->SetLineWidth( 1 );
  asymn_chi1->Draw( "SAME" ); 
  asymn_chi2->Draw( "SAME" );
  gausn_bckg2->Draw( "SAME" );
  hist_prep_axes( asym_fit2 );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit2, "M_{inv,res}", "Events" );
  TLegend * alegend2 = below_logo_legend();
  alegend2->AddEntry( asym_fit2, "M_{inv,res}" );
  alegend2->AddEntry( asym2pin_gauss, "asym 2 pin gauss" );
  alegend2->AddEntry( asymn_chi1, "chi1" ); 
  alegend2->AddEntry( asymn_chi2, "chi2" );
  alegend2->AddEntry( gausn_bckg2, "gausn bckg" );
  alegend2->Draw("SAME");
  TPaveStats * asym_fit2_stats = make_stats( asym_fit2 );
  asym_fit2_stats->Draw( "SAME" );


  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 4 ) );
  asym_fit3->Draw( "HIST E1" );
  asym_fit3->SetName( "M_{inv,res}" );
  TFitResult res = *asym_fit3->Fit( dual_pin_agaus, "MS", "", 3.4, 4.0 );
  res.GetCorrelationMatrix().Print();
  dual_pin_agaus->Draw( "SAME" );
  TF1 * asymn_dual_chi1  = new TF1( "asymn_dual_chi1", agaus, 3.4, 4.0, 4);
  TF1 * asymn_dual_chi2  = new TF1( "asymn_dual_chi2", agaus, 3.4, 4.0, 4 );
  TF1 * gausn_bckg3 = new TF1( "asymn_bckg3", "gausn(0)",  3.4, 4.0 );
  asymn_dual_chi1->SetParameter( 0, dual_pin_agaus->GetParameter( 0 ) );
  asymn_dual_chi1->SetParameter( 1, dual_pin_agaus->GetParameter( 2 ) );
  asymn_dual_chi1->SetParameter( 2, dual_pin_agaus->GetParameter( 3 ) );
  asymn_dual_chi1->SetParameter( 3, dual_pin_agaus->GetParameter( 4 ) );
  asymn_dual_chi1->SetParError( 0, dual_pin_agaus->GetParError( 0 ) );
  asymn_dual_chi1->SetParError( 1, dual_pin_agaus->GetParError( 2 ) );
  asymn_dual_chi1->SetParError( 2, dual_pin_agaus->GetParError( 3 ) );
  asymn_dual_chi1->SetParError( 3, dual_pin_agaus->GetParError( 4 ) );

  asymn_dual_chi2->SetParameter( 0, dual_pin_agaus->GetParameter( 0 )*dual_pin_agaus->GetParameter(1) );
  asymn_dual_chi2->SetParameter( 1, dual_pin_agaus->GetParameter( 2 ) + 0.045 );
  asymn_dual_chi2->SetParameter( 2, dual_pin_agaus->GetParameter( 5 ) );
  asymn_dual_chi2->SetParameter( 3, dual_pin_agaus->GetParameter( 6 ) );
  double gp1 =  dual_pin_agaus->GetParError( 0 );
  double gp2 =  dual_pin_agaus->GetParError( 1 );
  asymn_dual_chi2->SetParError( 0, sqrt( gp1*gp1 + gp2*gp2 ) );
  asymn_dual_chi2->SetParError( 1, dual_pin_agaus->GetParError( 2 ) + 0.045 );
  asymn_dual_chi2->SetParError( 2, dual_pin_agaus->GetParError( 5 ) );
  asymn_dual_chi2->SetParError( 3, dual_pin_agaus->GetParError( 6 ) );

  gausn_bckg3->SetParameter( 0, dual_pin_agaus->GetParameter( 7 ) );
  gausn_bckg3->SetParameter( 1, dual_pin_agaus->GetParameter( 8 ) );
  gausn_bckg3->SetParameter( 2, dual_pin_agaus->GetParameter( 9 ) );
  gausn_bckg3->SetParError( 0, dual_pin_agaus->GetParError( 7 ) );
  gausn_bckg3->SetParError( 1, dual_pin_agaus->GetParError( 8 ) );
  gausn_bckg3->SetParError( 2, dual_pin_agaus->GetParError( 9 ) );

  //std::cout << "chi1 root integral: " << asymn_dual_chi1->Integral( 3.4, 4.0 )/0.01  << " \u00B1 ?" << asymn_dual_chi1->IntegralError( 3.4, 4.0, asymn_dual_chi1->GetParameters(), nullptr ) << std::endl;
  //std::cout << "chi2 root integral: " << asymn_dual_chi2->Integral( 3.4, 4.0 )/0.01  << " \u00B1 ?" << asymn_dual_chi2->IntegralError( 3.4, 4.0 ) << std::endl; 
  //std::cout << "bckg root integral: " << gausn_bckg3->Integral( 3.4, 4.0 )/0.01 << " \u00B1 ?"      << gausn_bckg3->IntegralError( 3.4, 4.0 ) <<  std::endl;
  /*
    the equation you have is  
    ( a / ( sqrt(2pi)*sigma ) ) * exp(  ( -( c - x )^2/2sigma^2))
  */
  double chi1_a  = asymn_dual_chi1->GetParameter( 0 );
  double chi1_ae  = asymn_dual_chi1->GetParError( 0 );
  double chi1_sle = asymn_dual_chi1->GetParError( 2 );
  double chi1_sre = asymn_dual_chi1->GetParError( 3 );
  double chi1_ci  = chi1_a;
  double chi1_cie1 = sqrt( chi1_sle*chi1_sle + chi1_sre*chi1_sre + chi1_ae*chi1_ae ) * 2.5066;
  double chi1_cie2 = chi1_ae/0.01;
  
  double chi2_a  = asymn_dual_chi2->GetParameter( 0 );
  double chi2_ae  = asymn_dual_chi2->GetParError( 0 );
  double chi2_sle = asymn_dual_chi2->GetParError( 2 );
  double chi2_sre = asymn_dual_chi2->GetParError( 3 );
  double chi2_ci  = chi2_a;
  double chi2_cie1 = sqrt( chi2_sle*chi2_sle + chi2_sre*chi2_sre + chi2_ae*chi2_ae ) * 2.5066;
  double chi2_cie2 = chi2_ae/0.01;

  std::cout << "chi1 calc integral: " << chi1_ci/0.01  << " \u00B1 " << chi1_cie1/0.01 << " or \u00B1 " << chi1_cie2 << std::endl;
  std::cout << "chi2 calc integral: " << chi2_ci/0.01  << " \u00B1 " << chi2_cie1/0.01 << " or \u00B1 " << chi2_cie2 << std::endl; 

  asymn_dual_chi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  asymn_dual_chi1->SetLineWidth( 1 );
  asymn_dual_chi1->SetLineStyle( 2 ); 
  asymn_dual_chi2->SetLineColorAlpha( kOrange+1, 1.0 );
  asymn_dual_chi2->SetLineWidth( 1 );
  asymn_dual_chi2->SetLineStyle( 2 ); 
  gausn_bckg3->SetLineColorAlpha( kGreen+1, 1.0 );
  gausn_bckg3->SetLineStyle( 2 );  
  gausn_bckg3->SetLineWidth( 1 );
  asymn_dual_chi1->Draw( "SAME" ); 
  asymn_dual_chi2->Draw( "SAME" );
  gausn_bckg3->Draw( "SAME" );
  hist_prep_axes( asym_fit3 );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit3, "M_{inv,res}", "Events" );
  TLegend * alegend3 = below_logo_legend();
  alegend3->AddEntry( asym_fit3, "M_{inv,res}" );
  alegend3->AddEntry( dual_pin_agaus, "asym 2 pin gauss" );
  alegend3->AddEntry( asymn_dual_chi1, "chi1" ); 
  alegend3->AddEntry( asymn_dual_chi2, "chi2" );
  alegend3->AddEntry( gausn_bckg3, "gausn bckg" );
  alegend3->Draw("SAME");
  TPaveStats * asym_fit3_stats = make_stats( asym_fit3 );
  asym_fit3_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 5 ) );
  asym_fit4->Draw( "HIST E1" );
  asym_fit4->SetName( "M_{inv,res}" );
  asym_fit4->Fit( dag_clrs_fn, "M", "", 3.4, 4.0 );
  dag_clrs_fn->Draw( "SAME" );
  TF1 * dag_clrs_chi1  = new TF1( "dag_clrs_chi1", agaus, 3.4, 4.0, 4);
  TF1 * dag_clrs_chi2  = new TF1( "dag_clrs_chi2", agaus, 3.4, 4.0, 4 );
  TF1 * gausn_bckg4 = new TF1( "dag_gauss3", "gausn(0)",  3.4, 4.0 );
  dag_clrs_chi1->SetParameter( 0, dag_clrs_fn->GetParameter( 0 ) );
  dag_clrs_chi1->SetParameter( 1, dag_clrs_fn->GetParameter( 2 ) );
  dag_clrs_chi1->SetParameter( 2, dag_clrs_fn->GetParameter( 3 ) );
  dag_clrs_chi1->SetParameter( 3, dag_clrs_fn->GetParameter( 4 ) );
  dag_clrs_chi2->SetParameter( 0, dag_clrs_fn->GetParameter( 0 )*dag_clrs_fn->GetParameter(1) );
  dag_clrs_chi2->SetParameter( 1, dag_clrs_fn->GetParameter( 2 ) + 0.045 );
  dag_clrs_chi2->SetParameter( 2, dag_clrs_fn->GetParameter( 3 ) );
  dag_clrs_chi2->SetParameter( 3, dag_clrs_fn->GetParameter( 4 ) );
  gausn_bckg4->SetParameter( 0, dag_clrs_fn->GetParameter( 5 ) );
  gausn_bckg4->SetParameter( 1, dag_clrs_fn->GetParameter( 6 ) );
  gausn_bckg4->SetParameter( 2, dag_clrs_fn->GetParameter( 7 ) );
  dag_clrs_chi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  dag_clrs_chi1->SetLineWidth( 1 );
  dag_clrs_chi1->SetLineStyle( 2 ); 
  dag_clrs_chi2->SetLineColorAlpha( kOrange+1, 1.0 );
  dag_clrs_chi2->SetLineWidth( 1 );
  dag_clrs_chi2->SetLineStyle( 2 ); 
  gausn_bckg4->SetLineColorAlpha( kGreen+1, 1.0 );
  gausn_bckg4->SetLineStyle( 2 );  
  gausn_bckg4->SetLineWidth( 1 );
  dag_clrs_chi1->Draw( "SAME" ); 
  dag_clrs_chi2->Draw( "SAME" );
  gausn_bckg4->Draw( "SAME" );
  hist_prep_axes( asym_fit4 );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit4, "M_{inv,res}", "Events" );
  TLegend * alegend4 = below_logo_legend();
  alegend4->AddEntry( asym_fit4, "M_{inv,res}" );
  alegend4->AddEntry( dag_clrs_fn, "asym 2 pin gauss" );
  alegend4->AddEntry( dag_clrs_chi1, "chi1" ); 
  alegend4->AddEntry( dag_clrs_chi2, "chi2" );
  alegend4->AddEntry( gausn_bckg4, "gausn bckg" );
  alegend4->Draw("SAME");
  TPaveStats * asym_fit4_stats = make_stats( asym_fit4 );
  asym_fit4_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 6 ) );
  asym_fit5->Draw( "HIST E1" );
  asym_fit5->SetName( "M_{inv,res}" );
  asym_fit5->Fit( dag_lnd_fn, "M", "", 3.4, 4.0 );
  dag_lnd_fn->Draw( "SAME" );
  TF1 * dag_lnd_chi1  = new TF1( "dag_lnd_chi1", agaus, 3.4, 4.0, 4);
  TF1 * dag_lnd_chi2  = new TF1( "dag_lnd_chi2", agaus, 3.4, 4.0, 4 );
  TF1 * bckg_lnd      = new TF1( "bckg_lnd", lnd, 3.4, 4.0, 2 );
  dag_lnd_chi1->SetParameter( 0, dag_lnd_fn->GetParameter( 0 ) );
  dag_lnd_chi1->SetParameter( 1, dag_lnd_fn->GetParameter( 2 ) );
  dag_lnd_chi1->SetParameter( 2, dag_lnd_fn->GetParameter( 3 ) );
  dag_lnd_chi1->SetParameter( 3, dag_lnd_fn->GetParameter( 4 ) );
  dag_lnd_chi2->SetParameter( 0, dag_lnd_fn->GetParameter( 0 )*dag_lnd_fn->GetParameter(1) );
  dag_lnd_chi2->SetParameter( 1, dag_lnd_fn->GetParameter( 2 ) + 0.045 );
  dag_lnd_chi2->SetParameter( 2, dag_lnd_fn->GetParameter( 5 ) );
  dag_lnd_chi2->SetParameter( 3, dag_lnd_fn->GetParameter( 6 ) );
  bckg_lnd->SetParameter( 0, dag_lnd_fn->GetParameter( 7 ) );
  bckg_lnd->SetParameter( 1, dag_lnd_fn->GetParameter( 8 ) );
  dag_lnd_chi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  dag_lnd_chi1->SetLineWidth( 1 );
  dag_lnd_chi1->SetLineStyle( 2 ); 
  dag_lnd_chi2->SetLineColorAlpha( kOrange+1, 1.0 );
  dag_lnd_chi2->SetLineWidth( 1 );
  dag_lnd_chi2->SetLineStyle( 2 ); 
  bckg_lnd->SetLineColorAlpha( kGreen+1, 1.0 );
  bckg_lnd->SetLineStyle( 2 );  
  bckg_lnd->SetLineWidth( 1 );
  dag_lnd_chi1->Draw( "SAME" ); 
  dag_lnd_chi2->Draw( "SAME" );
  bckg_lnd->Draw( "SAME" );
  hist_prep_axes( asym_fit5 );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit5, "M_{inv,res}", "Events" );
  TLegend * alegend5 = below_logo_legend();
  alegend5->AddEntry( asym_fit5, "M_{inv,res}" );
  alegend5->AddEntry( dag_lnd_fn, "asym 2 pin gauss" );
  alegend5->AddEntry( dag_lnd_chi1, "chi1" ); 
  alegend5->AddEntry( dag_lnd_chi2, "chi2" );
  alegend5->AddEntry( bckg_lnd, "landau bckg" );
  alegend5->Draw("SAME");
  TPaveStats * asym_fit5_stats = make_stats( asym_fit5 );
  asym_fit5_stats->Draw( "SAME" );


  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 7 ) );
  asym_fit7->Draw( "HIST E1" );
  asym_fit7->SetName( "M_{inv,res}" );
  TFitResult res7 = *asym_fit7->Fit( dag_params_fn, "MS", "", 3.4, 4.0 );
  res7.GetCorrelationMatrix().Print();
  dag_params_fn->Draw( "SAME" );
  TF1 * dag_pchi1  = new TF1( "dag_pchi1", agaus, 3.4, 4.0, 4 );
  TF1 * dag_pchi2  = new TF1( "asymn_dual_chi2", agaus, 3.4, 4.0, 4 );
  TF1 * dag_gaus3 = new TF1( "asymn_bckg3", "gausn(0)",  3.4, 4.0 );
  dag_pchi1->SetParameter( 0, dual_pin_agaus->GetParameter( 0 ) );
  dag_pchi1->SetParameter( 1, dual_pin_agaus->GetParameter( 2 ) );
  dag_pchi1->SetParameter( 2, dual_pin_agaus->GetParameter( 3 ) );
  dag_pchi1->SetParameter( 3, dual_pin_agaus->GetParameter( 4 ) );
  dag_pchi1->SetParError( 0, dual_pin_agaus->GetParError( 0 ) );
  dag_pchi1->SetParError( 1, dual_pin_agaus->GetParError( 2 ) );
  dag_pchi1->SetParError( 2, dual_pin_agaus->GetParError( 3 ) );
  dag_pchi1->SetParError( 3, dual_pin_agaus->GetParError( 4 ) );
  dag_pchi2->SetParameter( 0, dual_pin_agaus->GetParameter( 0 )*dual_pin_agaus->GetParameter(1) );
  dag_pchi2->SetParameter( 1, dual_pin_agaus->GetParameter( 2 ) + 0.045 );
  dag_pchi2->SetParameter( 2, dual_pin_agaus->GetParameter( 5 ) );
  dag_pchi2->SetParameter( 3, dual_pin_agaus->GetParameter( 6 ) );
  dag_pchi2->SetParError( 1, dual_pin_agaus->GetParError( 2 ) + 0.045 );
  dag_pchi2->SetParError( 2, dual_pin_agaus->GetParError( 5 ) );
  dag_pchi2->SetParError( 3, dual_pin_agaus->GetParError( 6 ) );
  dag_gaus3->SetParameter( 0, dual_pin_agaus->GetParameter( 7 ) );
  dag_gaus3->SetParameter( 1, dual_pin_agaus->GetParameter( 8 ) );
  dag_gaus3->SetParameter( 2, dual_pin_agaus->GetParameter( 9 ) );
  dag_gaus3->SetParError( 0, dual_pin_agaus->GetParError( 7 ) );
  dag_gaus3->SetParError( 1, dual_pin_agaus->GetParError( 8 ) );
  dag_gaus3->SetParError( 2, dual_pin_agaus->GetParError( 9 ) );
  dag_pchi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  dag_pchi1->SetLineWidth( 1 );
  dag_pchi1->SetLineStyle( 2 ); 
  dag_pchi2->SetLineColorAlpha( kOrange+1, 1.0 );
  dag_pchi2->SetLineWidth( 1 );
  dag_pchi2->SetLineStyle( 2 ); 
  dag_gaus3->SetLineColorAlpha( kGreen+1, 1.0 );
  dag_gaus3->SetLineStyle( 2 );  
  dag_gaus3->SetLineWidth( 1 );
  dag_pchi1->Draw( "SAME" ); 
  dag_pchi2->Draw( "SAME" );
  dag_gaus3->Draw( "SAME" );
  hist_prep_axes( asym_fit7 );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit7, "M_{inv,res}", "Events" );
  TLegend * alegend7 = below_logo_legend();
  alegend7->AddEntry( asym_fit7, "M_{inv,res}" );
  alegend7->AddEntry( dag_params_fn, "newparam" );
  alegend7->AddEntry( dag_pchi1, "chi1" ); 
  alegend7->AddEntry( dag_pchi2, "chi2" );
  alegend7->AddEntry( dag_gaus3, "gausn bckg" );
  alegend7->Draw("SAME");
  TPaveStats * dag_fit7_stats = make_stats( asym_fit7 );
  dag_fit7_stats->Draw( "SAME" );

  
  
  
  //active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 7 ) );
  //nres_fit3->Draw( "HIST E1" );
  //nres_fit3->Fit( nres_3g, "M", "", 3.4, 4.0 );
  //TF1 * sg7_chi1 = new TF1( "sg7_chi1", "gausn(0)", 3.4, 4.0 );
  //TF1 * sg7_chi2 = new TF1( "sg7_chi2", "gausn(0)", 3.4, 4.0 );
  //TF1 * bckg_gauss7 = new TF1( "bg7", "gausn(0)", 3.4, 4.0 );
  //nres_3g->Draw( "SAME" );
  //sg7_chi1->SetParameter( 0, nres_3g->GetParameter( 0 ) );
  //sg7_chi1->SetParameter( 1, nres_3g->GetParameter( 2 ) );
  //sg7_chi1->SetParameter( 2, nres_3g->GetParameter( 3 ) );
  //sg7_chi2->SetParameter( 0, nres_3g->GetParameter( 0 )*nres_3g->GetParameter( 1 ) ); 
  //sg7_chi2->SetParameter( 1, nres_3g->GetParameter( 2 ) + 0.045 );
  //sg7_chi2->SetParameter( 2, nres_3g->GetParameter( 4 ) );
  //bckg_gauss7->SetParameter( 0, nres_3g->GetParameter( 5 ) );
  //bckg_gauss7->SetParameter( 1, nres_3g->GetParameter( 6 ) );
  //bckg_gauss7->SetParameter( 2, nres_3g->GetParameter( 7 ) );
  //sg7_chi1->SetLineColor( kMagenta + 1);
  //sg7_chi1->SetLineStyle( 2 );
  //sg7_chi1->SetLineWidth( 1 );
  //sg7_chi2->SetLineColor( kOrange + 1);
  //sg7_chi2->SetLineStyle( 2 );
  //sg7_chi2->SetLineWidth( 1 );
  //bckg_gauss7->SetLineColor( kGreen + 1);
  //bckg_gauss7->SetLineStyle( 2 );
  //bckg_gauss7->SetLineWidth( 1 );
  //double sg7_chi2e = std::sqrt( nres_3g->GetParError(0)*nres_3g->GetParError(0)  + nres_3g->GetParError(0)*nres_3g->GetParError(1) )/0.01;
  //std::cout << sg7_chi1->GetParameter( 0 )/0.01 << " \u00B1 " << nres_3g->GetParError( 0 )/0.01 << std::endl; 
  //std::cout << sg7_chi2->GetParameter( 0 )/0.01 << " \u00B1 " << sg7_chi2e << std::endl; 
  ////std::cout << bckg_gauss7->GetParameter( 0 )/0.01 << " \u00B1 " << nres_3g->GetParError( 6 )/0.01 << std::endl; 
  //hist_prep_axes( nres_fit3 );
  //add_atlas_decorations( active_pad );
  //sg7_chi1->Draw( "SAME" );
  //sg7_chi2->Draw( "SAME" );
  //bckg_gauss7->Draw( "SAME" );
  //TLegend * alegend7 = below_logo_legend();
  //alegend7->AddEntry( nres_fit3, "M_{inv}" );
  //alegend7->AddEntry( nres_3g, "sym gauss" );
  //alegend7->AddEntry( sg7_chi1, "sign chi1" );
  //alegend7->AddEntry( sg7_chi2, "sign chi2" );
  //alegend7->AddEntry( bckg_gauss7, "bckg gauss" );
  //alegend7->Draw("SAME");
  //TPaveStats * nres_fit3_stats= make_stats( nres_fit3 );
  //nres_fit3_stats->Draw( "SAME" );


  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 8 ) );
  nres_fit->Draw( "HIST E1" );
  nres_fit->SetName( "M_{inv}" );
  nres_fit->Fit( res_gausn, "M", "", 3.4, 4.0 );
  res_gausn->Draw( "SAME" );
  hist_prep_axes( nres_fit );
  add_atlas_decorations( active_pad );
  TF1 * sign_gauss8 = new TF1( "sg8", "gausn(0)", 3.4, 4.0 );
  TF1 * bckg_gauss8 = new TF1( "bg8", "gausn(0)", 3.4, 4.0 );
  sign_gauss8->SetParameter( 0, res_gausn->GetParameter( 0 ) ); 
  sign_gauss8->SetParameter( 1, res_gausn->GetParameter( 1 ) );
  sign_gauss8->SetParameter( 2, res_gausn->GetParameter( 2 ) );
  sign_gauss8->SetParError( 0, res_gausn->GetParError( 0 ) ); 
  sign_gauss8->SetParError( 1, res_gausn->GetParError( 1 ) );
  sign_gauss8->SetParError( 2, res_gausn->GetParError( 2 ) );
  bckg_gauss8->SetParameter( 0, res_gausn->GetParameter( 3 ) );
  bckg_gauss8->SetParameter( 1, res_gausn->GetParameter( 4 ) );
  bckg_gauss8->SetParameter( 2, res_gausn->GetParameter( 5 ) );
  bckg_gauss8->SetParError( 0, res_gausn->GetParError( 3 ) );
  bckg_gauss8->SetParError( 1, res_gausn->GetParError( 4 ) );
  bckg_gauss8->SetParError( 2, res_gausn->GetParError( 5 ) );
  sign_gauss8->SetLineColorAlpha( kMagenta+1, 1.0 );
  sign_gauss8->SetLineWidth( 1.0 );
  sign_gauss8->SetLineStyle( 2.0 );
  bckg_gauss8->SetLineColorAlpha( kGreen+1, 1.0 );
  bckg_gauss8->SetLineWidth( 1.0 );
  bckg_gauss8->SetLineStyle( 2.0 );
  std::cout << sign_gauss8->GetParameter( 0 )/0.01 << " \u00B1 " << sign_gauss8->GetParError( 0 )/0.01 << std::endl; 
  std::cout << bckg_gauss8->GetParameter( 0 )/0.01 << " \u00B1 " << bckg_gauss8->GetParError( 0 )/0.01 << std::endl; 
  sign_gauss8->Draw( "SAME" ); 
  bckg_gauss8->Draw( "SAME" );
  TLegend * alegend6 = below_logo_legend();
  alegend6->AddEntry( nres_fit, "M_{inv}" );
  alegend6->AddEntry( res_gausn, "sym gauss" );
  alegend6->AddEntry( sign_gauss8, "sign gauss" );
  alegend6->AddEntry( bckg_gauss8, "bckg gauss" );
  alegend6->Draw("SAME");
  TPaveStats * nres_fit_stats= make_stats( nres_fit );
  nres_fit_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( peak_fit_canvas->cd( 9 ) );
  asym_fit9->Draw( "HIST E1" );
  asym_fit9->SetName( "M_{inv,res}" );
  dag_exp_fn->SetRange( 3.4, 4.0 );
  TFitResult res9 = *asym_fit9->Fit( dag_exp_fn, "MSEVR", "", 3.4, 4.0 );
  res9.GetCorrelationMatrix().Print();
  dag_exp_fn->Draw( "SAME" );
  TF1 * dag_exp_chi1  = new TF1( "dag_exp_chi1", agaus_r_chi1, 3.4, 4.0, 5 );
  TF1 * dag_exp_chi2  = new TF1( "dag_exp_chi2", agaus_r_chi2, 3.4, 4.0, 5 );
  TF1 * dag_exp_bckg = new TF1( "dag_exp_bckg", bckg_exp,  3.4, 4.0, 4 );
  dag_exp_chi1->SetParameter( 0, dag_exp_fn->GetParameter( 0 ) );
  dag_exp_chi1->SetParameter( 1, dag_exp_fn->GetParameter( 1 ) );
  dag_exp_chi1->SetParameter( 2, dag_exp_fn->GetParameter( 2 ) );
  dag_exp_chi1->SetParameter( 3, dag_exp_fn->GetParameter( 3 ) );
  dag_exp_chi1->SetParameter( 4, dag_exp_fn->GetParameter( 4 ) );

  dag_exp_chi2->SetParameter( 0, dag_exp_fn->GetParameter( 0 ) );
  dag_exp_chi2->SetParameter( 1, dag_exp_fn->GetParameter( 1 ) );
  dag_exp_chi2->SetParameter( 2, dag_exp_fn->GetParameter( 2 ) + 0.045 );
  dag_exp_chi2->SetParameter( 3, dag_exp_fn->GetParameter( 5 ) );
  dag_exp_chi2->SetParameter( 4, dag_exp_fn->GetParameter( 6 ) );

  dag_exp_bckg->SetParameter( 0, dag_exp_fn->GetParameter( 7 ) );
  dag_exp_bckg->SetParameter( 1, dag_exp_fn->GetParameter( 8 ) );
  dag_exp_bckg->SetParameter( 2, dag_exp_fn->GetParameter( 9 ) );
  dag_exp_bckg->SetParameter( 3, dag_exp_fn->GetParameter( 10 ) );



  dag_exp_chi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  dag_exp_chi1->SetLineWidth( 1 );
  dag_exp_chi1->SetLineStyle( 2 ); 
  dag_exp_chi2->SetLineColorAlpha( kOrange+1, 1.0 );
  dag_exp_chi2->SetLineWidth( 1 );
  dag_exp_chi2->SetLineStyle( 2 ); 
  dag_exp_bckg->SetLineColorAlpha( kGreen+1, 1.0 );
  dag_exp_bckg->SetLineStyle( 2 );  
  dag_exp_bckg->SetLineWidth( 1 );
  dag_exp_chi1->Draw( "SAME" ); 
  dag_exp_chi2->Draw( "SAME" );
  dag_exp_bckg->Draw( "SAME" );
  std::cout << dag_exp_bckg->Eval( 3.9 ) << std::endl;

  hist_prep_axes( asym_fit9 );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit9, "M_{inv,res}", "Events" );
  TLegend * alegend9 = below_logo_legend();
  alegend9->AddEntry( asym_fit9, "M_{inv,res}" );
  alegend9->AddEntry( dag_exp_fn, "dag_exp" );
  alegend9->AddEntry( dag_exp_chi1, "chi1" ); 
  alegend9->AddEntry( dag_exp_chi2, "chi2" );
  alegend9->AddEntry( dag_exp_bckg, "exp bckg" );
  alegend9->Draw("SAME");
  TPaveStats * dag_fit9_stats = make_stats( asym_fit9 );
  dag_fit9_stats->Draw( "SAME" );

  dag_exp_fn->Print();
  dag_exp_bckg->Print();

  peak_fit_canvas->SaveAs( "./plots/peakfit.pdf" );


  TCanvas * fit2 = new TCanvas( "", "", 200, 200, 1000, 1000 );
  fit2->Divide( 1, 1 );
  active_pad = static_cast<TPad *>( fit2->cd( 1 ) );
  asym_fit->Draw( "HIST E1" );
  asym_fit->SetName( "M_{inv,res}" );
  asym_fit->Fit( asym_gauss, "M", "", 3.4, 4.0 );
  asym_gauss->Draw( "SAME" );
  asymn_sign = new TF1( "asymn_sign", agaus, 3.4, 4.0, 4 );
  asymn_sign->SetParameter( 0, asym_gauss->GetParameter( 0 ) );
  asymn_sign->SetParameter( 1, asym_gauss->GetParameter( 1 ) );
  asymn_sign->SetParameter( 2, asym_gauss->GetParameter( 2 ) );
  asymn_sign->SetParameter( 3, asym_gauss->GetParameter( 3 ) );
  gausn_bckg = new TF1( "gausn_bckg", "gausn", 3.4, 4.0 );
  gausn_bckg->SetParameter( 0, asym_gauss->GetParameter( 4 ) );
  gausn_bckg->SetParameter( 1, asym_gauss->GetParameter( 5 ) );
  gausn_bckg->SetParameter( 2, asym_gauss->GetParameter( 6 ) );
  asymn_sign->SetLineColorAlpha( kMagenta+1, 1.0 );
  gausn_bckg->SetLineColorAlpha( kGreen+1, 1.0 );
  asymn_sign->SetLineStyle( 2 ); 
  gausn_bckg->SetLineStyle( 2 );  
  asymn_sign->SetLineWidth( 1 );
  gausn_bckg->SetLineWidth( 1 );
  asymn_sign->Draw( "SAME" );  
  gausn_bckg->Draw( "SAME" );
  hist_prep_axes( asym_fit );
  add_atlas_decorations( active_pad );
  set_axis_labels( asym_fit, "M_{inv,res}", "Events" );
  alegend = below_logo_legend();
  alegend->AddEntry( asym_fit, "M_{inv,res}" );
  alegend->AddEntry( asym_gauss, "asymn total" );
  alegend->AddEntry( asymn_sign, "asymn sign" );
  alegend->AddEntry( gausn_bckg, "gausn bckg" );
  alegend->Draw("SAME");
  asym_fit_stats = make_stats( asym_fit );
  asym_fit_stats->Draw( "SAME" );
  fit2->SaveAs( "fit2.pdf" );



  //`double fg_calc = ( dual_gaussian->GetParameter( 0 ) * dual_gaussian->GetParameter( 2 ) )/0.3989;
  //`double sg_calc = ( dual_gaussian->GetParameter( 3 ) * dual_gaussian->GetParameter( 5 ) )/0.3989;
  //`double total_acc = 0, fg_acc = 0, sg_acc = 0;
  //`for ( int idx = 1; idx <= peak_fit->GetNbinsX(); idx++ ){
  //`  double centre = peak_fit->GetBinCenter( idx );
  //`  total_acc += dual_gaussian->Eval( centre );
  //`  fg_acc += gauss1->Eval( centre );
  //`  sg_acc += gauss2->Eval( centre );
  //`}
  
  //std::cout << std::endl; 
  //std::cout << "First Gauss Integral Root:   " << ( gauss1->Integral( minbin, maxbin ) )/bin_width_gev << std::endl;
  //std::cout << "Second Gauss Integral Root:  " << ( gauss2->Integral( minbin, maxbin ) )/bin_width_gev << std::endl;
  //std::cout << "Total Gauss Integral Root:   " << dual_gaussian->Integral( minbin, maxbin )/bin_width_gev << std::endl;
  //std::cout << "First Gauss Integral Root Error:   " << ( gauss1->IntegralError( minbin, maxbin ) )/bin_width_gev << std::endl;
  //std::cout << "Second Gauss Integral Root Error:  " << ( gauss2->IntegralError( minbin, maxbin ) )/bin_width_gev << std::endl;
  //std::cout << "Total Gauss Integral Root Error:   " << dual_gaussian->IntegralError( minbin, maxbin )/bin_width_gev << std::endl;
  //std::cout << std::endl; 
  //std::cout << "First Gauss Integral Calc:   " << fg_calc/bin_width_gev << std::endl;
  //std::cout << "Second Gauss Integral Calc:  " << sg_calc/bin_width_gev << std::endl;
  //std::cout << "Total Gauss Integral Calc:   " << fg_calc/bin_width_gev + sg_calc/bin_width_gev << std::endl;
  //std::cout << std::endl; 
  //std::cout << "Accumulated gauss1:          " << fg_acc << std::endl;
  //std::cout << "Accumulated gauss2:          " << sg_acc << std::endl;
  //std::cout << "Accumulated total gauss:     " << total_acc << std::endl;
  std::cout << std::endl; 
  std::cout << "Graph Bin Width: " << bin_width_gev << std::endl; 

} 

void parameterise( YAML::Node & input, TF1 * function, const std::string & function_name ){
  
  if ( !input[function_name + "_value"] ){ return; }
  std::vector< double > value = input[ function_name + "_value" ].as<std::vector<double>>(); 
  std::vector< double > upper = input[ function_name + "_upper" ].as<std::vector<double>>();
  std::vector< double > lower = input[ function_name + "_lower" ].as<std::vector<double>>();

  for ( int param = 0; param < function->GetNpar(); param++ ){
    function->SetParameter( param, value.at( param ) );
    function->SetParLimits( param, lower.at( param ), upper.at( param ) );
  }

}


void fastchi( YAML::Node input ){

  prep_style();

  std::cout << input.IsDefined() << std::endl;
  TFile * store = new TFile( "./store.root", "READ" );
  TH1F * minvres_full = static_cast<TH1F *>( store->Get( "minvres_full" ) );
  //TH1F * minvres_abv6 = static_cast<TH1F *>( store->Get( "minvres_abv6" ) );
  //TH1F * minvres_blw6 = static_cast<TH1F *>( store->Get( "minvres_blw6" ) );
  //TH1F * minvnoc_full = static_cast<TH1F *>( store->Get( "minvnoc_full" ) );
  //TH1F * minvnoc_abv6 = static_cast<TH1F *>( store->Get( "minvnoc_abv6" ) );
  //TH1F * minvnoc_blw6 = static_cast<TH1F *>( store->Get( "minvnoc_blw6" ) );

  TF1 * dag_params_fn= new TF1( "dag_param_fn", dag_param, 3.4, 4.0, 10 );
  dag_params_fn->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}","\\sigma_{\\chi1l}","\\sigma_{\\chi1r}",
                                "\\sigma_{\\chi2l}","\\sigma_{\\chi2r}", 
                                "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" );
  parameterise( input, dag_params_fn, "dag_params_fn" );

  TF1 * dag_exp_fn = new TF1( "dag_exp_fn", dag_exp, 3.4, 4.0, 11 );
  dag_exp_fn->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}",
                            "\\sigma_{\\chi1l}","\\sigma_{\\chi1r}",
                            "\\sigma_{\\chi2l}","\\sigma_{\\chi2r}", 
                            "a_{bckg}", "b_{bckg}", "c_{bckg}", "d_{bckg}" );
  parameterise( input, dag_exp_fn, "dag_exp_fn" ); 

  TF1 * dag_pinlr_param_fn = new TF1( "dag_pinlr_fn", dag_pinlr_param, 3.4, 4.0, 8 );
  dag_pinlr_param_fn->SetParNames( "a_{\\chi}","a_{r}","x_{\\chi}",
                            "\\sigma_{\\chil}","\\sigma_{\\chir}",
                            "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" ) ;
  parameterise( input, dag_pinlr_param_fn, "dag_pinlr_fn" ); 

  TF1 * sag_fn = new TF1( "sag_fn", sag, 3.4, 4.0, 7 );
  sag_fn->SetParNames( "a_{\\chi}","x_{\\chi}",
                            "\\sigma_{\\chil}","\\sigma_{\\chir}",
                            "a_{bckg}", "x_{bckg}", "\\sigma_{bckg}" ) ;
  parameterise( input, sag_fn, "sag_fn" ); 

  TF1 * sag_exp_fn = new TF1( "sag_exp_fn", sag_exp, 3.4, 4.0, 8 );
  sag_exp_fn->SetParNames( "a_{\\chi}","x_{\\chi}",
                            "\\sigma_{\\chil}","\\sigma_{\\chir}",
                            "a_{bckg}", "b_{bckg}", "c_{bckg}", "d_{bckg}" ) ;
  parameterise( input, sag_exp_fn, "sag_exp_fn" ); 


  TCanvas * canvas = new TCanvas( "pfc", "", 200, 200, 3000, 2000 );
  canvas->Divide( 3, 2 );
  gStyle->SetOptStat( "ieoumrn" );
  gStyle->SetOptFit( 1 );

  TH1F * fit_1 = static_cast<TH1F *>( minvres_full->Clone() );
  TPad * active_pad = static_cast<TPad *>( canvas->cd( 1 ) );
  fit_1->Draw( "HIST E1" );
  fit_1->SetName( "M_{inv,res}" );
  TFitResult dag_params_result = *fit_1->Fit( dag_params_fn, "MS", "", 3.4, 4.0 );
  dag_params_result.GetCorrelationMatrix().Print();
  dag_params_fn->Draw( "SAME" );
  // god i can't just automate this because of the way it works.
  // you could toatlly pass a one of these and then a vector of functions
  // !! because all the functions have the same signature !!
  TF1 * dag_pchi1  = new TF1( "dag_pchi1", agaus, 3.4, 4.0, 4 );
  TF1 * dag_pchi2  = new TF1( "asymn_dual_chi2", agaus, 3.4, 4.0, 4 );
  TF1 * dag_gaus3 = new TF1( "asymn_bckg3", "gausn(0)",  3.4, 4.0 );
  dag_pchi1->SetParameter( 0, dag_params_fn->GetParameter( 0 )/(dag_params_fn->GetParameter(1) + 1) );
  dag_pchi1->SetParameter( 1, dag_params_fn->GetParameter( 2 ) );
  dag_pchi1->SetParameter( 2, dag_params_fn->GetParameter( 3 ) );
  dag_pchi1->SetParameter( 3, dag_params_fn->GetParameter( 4 ) );
  dag_pchi1->SetParError( 0, dag_params_fn->GetParError( 0 ) );
  dag_pchi1->SetParError( 1, dag_params_fn->GetParError( 2 ) );
  dag_pchi1->SetParError( 2, dag_params_fn->GetParError( 3 ) );
  dag_pchi1->SetParError( 3, dag_params_fn->GetParError( 4 ) );
  dag_pchi2->SetParameter( 0, 
    dag_params_fn->GetParameter( 1 )*dag_params_fn->GetParameter( 0 )/( dag_params_fn->GetParameter(1) + 1 ));
  dag_pchi2->SetParameter( 1, dag_params_fn->GetParameter( 2 ) + 0.045 );
  dag_pchi2->SetParameter( 2, dag_params_fn->GetParameter( 5 ) );
  dag_pchi2->SetParameter( 3, dag_params_fn->GetParameter( 6 ) );
  dag_pchi2->SetParError( 1, dag_params_fn->GetParError( 2 ) + 0.045 );
  dag_pchi2->SetParError( 2, dag_params_fn->GetParError( 5 ) );
  dag_pchi2->SetParError( 3, dag_params_fn->GetParError( 6 ) );
  dag_gaus3->SetParameter( 0, dag_params_fn->GetParameter( 7 ) );
  dag_gaus3->SetParameter( 1, dag_params_fn->GetParameter( 8 ) );
  dag_gaus3->SetParameter( 2, dag_params_fn->GetParameter( 9 ) );
  dag_gaus3->SetParError( 0, dag_params_fn->GetParError( 7 ) );
  dag_gaus3->SetParError( 1, dag_params_fn->GetParError( 8 ) );
  dag_gaus3->SetParError( 2, dag_params_fn->GetParError( 9 ) );
  dag_pchi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  dag_pchi1->SetLineWidth( 1 );
  dag_pchi1->SetLineStyle( 2 ); 
  dag_pchi2->SetLineColorAlpha( kOrange+1, 1.0 );
  dag_pchi2->SetLineWidth( 1 );
  dag_pchi2->SetLineStyle( 2 ); 
  dag_gaus3->SetLineColorAlpha( kGreen+1, 1.0 );
  dag_gaus3->SetLineStyle( 2 );  
  dag_gaus3->SetLineWidth( 1 );
  dag_pchi1->Draw( "SAME" ); 
  dag_pchi2->Draw( "SAME" );
  dag_gaus3->Draw( "SAME" );
  hist_prep_axes( fit_1 );
  add_atlas_decorations( active_pad );
  set_axis_labels( fit_1, "M_{inv,res}", "Events" );
  TLegend * dag_param_legend = below_logo_legend();
  dag_param_legend->AddEntry( fit_1, "M_{inv,res}" );
  dag_param_legend->AddEntry( dag_params_fn, "newparam" );
  dag_param_legend->AddEntry( dag_pchi1, "chi1" ); 
  dag_param_legend->AddEntry( dag_pchi2, "chi2" );
  dag_param_legend->AddEntry( dag_gaus3, "gausn bckg" );
  dag_param_legend->Draw("SAME");
  TPaveStats * dag_param_stats = make_stats( fit_1 );
  dag_param_stats->Draw( "SAME" );

  TH1F * fit_2 = static_cast<TH1F *>( minvres_full->Clone() );
  active_pad = static_cast<TPad *>( canvas->cd( 2 ) );
  fit_2->Draw( "HIST E1" );
  dag_exp_fn->SetRange( 3.4, 4.0 );
  TFitResult dag_exp_result = *fit_2->Fit( dag_exp_fn, "MSEVR", "", 3.4, 4.0 );
  dag_exp_result.GetCorrelationMatrix().Print();
  dag_exp_fn->Draw( "SAME" );
  TF1 * dag_exp_chi1  = new TF1( "dag_exp_chi1", agaus_r_chi1, 3.4, 4.0, 5 );
  TF1 * dag_exp_chi2  = new TF1( "dag_exp_chi2", agaus_r_chi2, 3.4, 4.0, 5 );
  TF1 * dag_exp_bckg = new TF1( "dag_exp_bckg", bckg_exp,  3.4, 4.0, 4 );
  dag_exp_chi1->SetParameter( 0, dag_exp_fn->GetParameter( 0 ) );
  dag_exp_chi1->SetParameter( 1, dag_exp_fn->GetParameter( 1 ) );
  dag_exp_chi1->SetParameter( 2, dag_exp_fn->GetParameter( 2 ) );
  dag_exp_chi1->SetParameter( 3, dag_exp_fn->GetParameter( 3 ) );
  dag_exp_chi1->SetParameter( 4, dag_exp_fn->GetParameter( 4 ) );
  dag_exp_chi2->SetParameter( 0, dag_exp_fn->GetParameter( 0 ) );
  dag_exp_chi2->SetParameter( 1, dag_exp_fn->GetParameter( 1 ) );
  dag_exp_chi2->SetParameter( 2, dag_exp_fn->GetParameter( 2 ) + 0.045 );
  dag_exp_chi2->SetParameter( 3, dag_exp_fn->GetParameter( 5 ) );
  dag_exp_chi2->SetParameter( 4, dag_exp_fn->GetParameter( 6 ) );
  dag_exp_bckg->SetParameter( 0, dag_exp_fn->GetParameter( 7 ) );
  dag_exp_bckg->SetParameter( 1, dag_exp_fn->GetParameter( 8 ) );
  dag_exp_bckg->SetParameter( 2, dag_exp_fn->GetParameter( 9 ) );
  dag_exp_bckg->SetParameter( 3, dag_exp_fn->GetParameter( 10 ) );
  dag_exp_chi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  dag_exp_chi1->SetLineWidth( 1 );
  dag_exp_chi1->SetLineStyle( 2 ); 
  dag_exp_chi2->SetLineColorAlpha( kOrange+1, 1.0 );
  dag_exp_chi2->SetLineWidth( 1 );
  dag_exp_chi2->SetLineStyle( 2 ); 
  dag_exp_bckg->SetLineColorAlpha( kGreen+1, 1.0 );
  dag_exp_bckg->SetLineStyle( 2 );  
  dag_exp_bckg->SetLineWidth( 1 );
  dag_exp_chi1->Draw( "SAME" ); 
  dag_exp_chi2->Draw( "SAME" );
  dag_exp_bckg->Draw( "SAME" );
  hist_prep_axes( fit_2 );
  add_atlas_decorations( active_pad );
  set_axis_labels( fit_2, "M_{inv,res}", "Events" );
  TLegend * dag_exp_legend = below_logo_legend();
  dag_exp_legend->AddEntry( fit_2, "M_{inv,res}" );
  dag_exp_legend->AddEntry( dag_exp_fn, "dag_exp" );
  dag_exp_legend->AddEntry( dag_exp_chi1, "chi1" ); 
  dag_exp_legend->AddEntry( dag_exp_chi2, "chi2" );
  dag_exp_legend->AddEntry( dag_exp_bckg, "exp bckg" );
  dag_exp_legend->Draw("SAME");
  TPaveStats * dag_exp_stats = make_stats( fit_2 );
  dag_exp_stats->Draw( "SAME" );

  TH1F * fit_3 = static_cast<TH1F *>( minvres_full->Clone() );
  active_pad = static_cast<TPad *>( canvas->cd( 3 ) );
  fit_3->Draw( "HIST E1" );
  fit_3->SetName( "M_{inv,res}" );
  TFitResult dag_pinlr_param_result = *fit_3->Fit( dag_pinlr_param_fn, "MS", "", 3.4, 4.0 );
  dag_pinlr_param_result.GetCorrelationMatrix().Print();
  dag_pinlr_param_fn->Draw( "SAME" );
  TF1 * dag_ppchi1  = new TF1( "dag_ppchi1", agaus, 3.4, 4.0, 4 );
  TF1 * dag_ppchi2  = new TF1( "dag_ppchi2", agaus, 3.4, 4.0, 4 );
  TF1 * dag_pgaus3 = new TF1(  "dag_pbckg3", "gausn(0)",  3.4, 4.0 );
  dag_ppchi1->SetParameter( 0, dag_pinlr_param_fn->GetParameter( 0 )/(dag_pinlr_param_fn->GetParameter(1) + 1) );
  dag_ppchi1->SetParameter( 1, dag_pinlr_param_fn->GetParameter( 2 ) );
  dag_ppchi1->SetParameter( 2, dag_pinlr_param_fn->GetParameter( 3 ) );
  dag_ppchi1->SetParameter( 3, dag_pinlr_param_fn->GetParameter( 4 ) );
  dag_ppchi2->SetParameter( 0, 
    dag_pinlr_param_fn->GetParameter( 1 )*dag_pinlr_param_fn->GetParameter( 0 )/( dag_pinlr_param_fn->GetParameter(1) + 1 ));
  dag_ppchi2->SetParameter( 1, dag_pinlr_param_fn->GetParameter( 2 ) + 0.045 );
  dag_ppchi2->SetParameter( 2, dag_pinlr_param_fn->GetParameter( 3 ) );
  dag_ppchi2->SetParameter( 3, dag_pinlr_param_fn->GetParameter( 4 ) );
  dag_pgaus3->SetParameter( 0, dag_pinlr_param_fn->GetParameter( 5 ) );
  dag_pgaus3->SetParameter( 1, dag_pinlr_param_fn->GetParameter( 6 ) );
  dag_pgaus3->SetParameter( 2, dag_pinlr_param_fn->GetParameter( 7 ) );

  dag_ppchi1->SetLineColorAlpha( kMagenta+1, 1.0 );
  dag_ppchi1->SetLineWidth( 1 );
  dag_ppchi1->SetLineStyle( 2 ); 
  dag_ppchi2->SetLineColorAlpha( kOrange+1, 1.0 );
  dag_ppchi2->SetLineWidth( 1 );
  dag_ppchi2->SetLineStyle( 2 ); 
  dag_pgaus3->SetLineColorAlpha( kGreen+1, 1.0 );
  dag_pgaus3->SetLineStyle( 2 );  
  dag_pgaus3->SetLineWidth( 1 );
  dag_ppchi1->Draw( "SAME" ); 
  dag_ppchi2->Draw( "SAME" );
  dag_pgaus3->Draw( "SAME" );
  hist_prep_axes( fit_3 );
  add_atlas_decorations( active_pad );
  set_axis_labels( fit_3, "M_{inv,res}", "Events" );
  TLegend * dag_pinlr_param_legend = below_logo_legend();
  dag_pinlr_param_legend->AddEntry( fit_3, "M_{inv,res}" );
  dag_pinlr_param_legend->AddEntry( dag_pinlr_param_fn, "pinlr" );
  dag_pinlr_param_legend->AddEntry( dag_ppchi1, "chi1" ); 
  dag_pinlr_param_legend->AddEntry( dag_ppchi2, "chi2" );
  dag_pinlr_param_legend->AddEntry( dag_pgaus3, "gausn bckg" );
  dag_pinlr_param_legend->Draw("SAME");
  TPaveStats * dag_pinlr_param_stats = make_stats( fit_3 );
  dag_pinlr_param_stats->Draw( "SAME" );

  TH1F * fit_4 = static_cast<TH1F *>( minvres_full->Clone() );
  active_pad = static_cast<TPad *>( canvas->cd( 4 ) );
  fit_4->Draw( "HIST E1" );
  fit_4->SetName( "M_{inv,res}" );
  TFitResult sag_result = *fit_4->Fit( sag_fn, "MS", "", 3.4, 4.0 );
  sag_result.GetCorrelationMatrix().Print();
  sag_fn->Draw( "SAME" );
  TF1 * sag_chi  = new TF1( "sag_chi", agaus, 3.4, 4.0, 4 );
  TF1 * sag_gaus = new TF1(  "sag_gaus", "gausn(0)",  3.4, 4.0 );
  sag_chi->SetParameter( 0, sag_fn->GetParameter( 0 ) );
  sag_chi->SetParameter( 1, sag_fn->GetParameter( 1 ) );
  sag_chi->SetParameter( 2, sag_fn->GetParameter( 2 ) );
  sag_chi->SetParameter( 3, sag_fn->GetParameter( 3 ) );
  sag_gaus->SetParameter( 0, sag_fn->GetParameter( 4 ) );
  sag_gaus->SetParameter( 1, sag_fn->GetParameter( 5 ) );
  sag_gaus->SetParameter( 2, sag_fn->GetParameter( 6 ) );
  sag_chi->SetLineColorAlpha( kMagenta+1, 1.0 );
  sag_chi->SetLineWidth( 1 );
  sag_chi->SetLineStyle( 2 ); 
  sag_gaus->SetLineColorAlpha( kGreen+1, 1.0 );
  sag_gaus->SetLineStyle( 2 );  
  sag_gaus->SetLineWidth( 1 );
  sag_chi->Draw( "SAME" ); 
  sag_gaus->Draw( "SAME" );
  hist_prep_axes( fit_4 );
  add_atlas_decorations( active_pad );
  set_axis_labels( fit_4, "M_{inv,res}", "Events" );
  TLegend * sag_legend = below_logo_legend();
  sag_legend->AddEntry( fit_4, "M_{inv,res}" );
  sag_legend->AddEntry( sag_fn, "sag" );
  sag_legend->AddEntry( sag_chi, "chi" ); 
  sag_legend->AddEntry( sag_gaus, "gausn bckg" );
  sag_legend->Draw("SAME");
  TPaveStats * sag_stats = make_stats( fit_4 );
  sag_stats->Draw( "SAME" );

  TH1F * fit_5 = static_cast<TH1F *>( minvres_full->Clone() );
  active_pad = static_cast<TPad *>( canvas->cd( 4 ) );
  fit_5->Draw( "HIST E1" );
  fit_5->SetName( "M_{inv,res}" );
  TFitResult sag_exp_result = *fit_5->Fit( sag_exp_fn, "MS", "", 3.4, 4.0 );
  sag_exp_result.GetCorrelationMatrix().Print();
  sag_exp_fn->Draw( "SAME" );
  TF1 * sag_exp_chi  = new TF1( "sag_exp_chi", agaus, 3.4, 4.0, 4 );
  TF1 * sag_exp_bckg = new TF1(  "sag_exp_bckg", bckg_exp,  3.4, 4.0, 4 );
  sag_exp_chi->SetParameter( 0, sag_exp_fn->GetParameter( 0 ) );
  sag_exp_chi->SetParameter( 1, sag_exp_fn->GetParameter( 1 ) );
  sag_exp_chi->SetParameter( 2, sag_exp_fn->GetParameter( 2 ) );
  sag_exp_chi->SetParameter( 3, sag_exp_fn->GetParameter( 3 ) );
  sag_exp_bckg->SetParameter( 0, sag_exp_fn->GetParameter( 4 ) );
  sag_exp_bckg->SetParameter( 1, sag_exp_fn->GetParameter( 5 ) );
  sag_exp_bckg->SetParameter( 2, sag_exp_fn->GetParameter( 6 ) );
  sag_exp_bckg->SetParameter( 3, sag_exp_fn->GetParameter( 7 ) );
  sag_exp_chi->SetLineColorAlpha( kMagenta+1, 1.0 );
  sag_exp_chi->SetLineWidth( 1 );
  sag_exp_chi->SetLineStyle( 2 ); 
  sag_exp_bckg->SetLineColorAlpha( kGreen+1, 1.0 );
  sag_exp_bckg->SetLineStyle( 2 );  
  sag_exp_bckg->SetLineWidth( 1 );
  sag_exp_chi->Draw( "SAME" ); 
  sag_exp_bckg->Draw( "SAME" );
  hist_prep_axes( fit_5 );
  add_atlas_decorations( active_pad );
  set_axis_labels( fit_5, "M_{inv,res}", "Events" );
  TLegend * sag_exp_legend = below_logo_legend();
  sag_exp_legend->AddEntry( fit_5, "M_{inv,res}" );
  sag_exp_legend->AddEntry( sag_exp_fn, "sag_exp" );
  sag_exp_legend->AddEntry( sag_exp_chi, "chi" ); 
  sag_exp_legend->AddEntry( sag_exp_bckg, "exp bckg" );
  sag_exp_legend->Draw("SAME");
  TPaveStats * sag_exp_stats = make_stats( fit_5 );
  sag_exp_stats->Draw( "SAME" );

  canvas->SaveAs( "fitting.png" );


}
