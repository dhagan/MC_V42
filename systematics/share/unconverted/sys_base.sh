ex=/home/atlas/dhagan/analysis/analysisFork/445/study/systematics/build/systematics

std_path=/home/atlas/dhagan/analysis/analysisFork/445

base_ef=${std_path}/TMVATrees/run/gen_base/Efficiencies_sign.root
base_hf=${std_path}/HistFactory/run/run_base/eval/hf_fit_base_A-qtA_S-BDT.root

## BDT Variable systematics
aaDPDY_hf=${std_path}/HistFactory/run/var_sys/run_aaDPDY/eval/hf_fit_aaDPDY_A-qtA_S-BDT.root
afDPDY_hf=${std_path}/HistFactory/run/var_sys/run_afDPDY/eval/hf_fit_afDPDY_A-qtA_S-BDT.root
faDPDY_hf=${std_path}/HistFactory/run/var_sys/run_faDPDY/eval/hf_fit_faDPDY_A-qtA_S-BDT.root
ffDPDY_hf=${std_path}/HistFactory/run/var_sys/run_ffDPDY/eval/hf_fit_ffDPDY_A-qtA_S-BDT.root
aaf_hf=${std_path}/HistFactory/run/var_sys/run_aaf/eval/hf_fit_aaf_A-qtA_S-BDT.root
afa_hf=${std_path}/HistFactory/run/var_sys/run_afa/eval/hf_fit_afa_A-qtA_S-BDT.root
faa_hf=${std_path}/HistFactory/run/var_sys/run_faa/eval/hf_fit_faa_A-qtA_S-BDT.root
extd_hf=${std_path}/HistFactory/run/var_sys/run_extd/eval/hf_fit_extd_A-qtA_S-BDT.root
noL_hf=${std_path}/HistFactory/run/var_sys/run_noL/eval/hf_fit_noL_A-qtA_S-BDT.root
noqt2_hf=${std_path}/HistFactory/run/var_sys/run_noqt2/eval/hf_fit_noqt2_A-qtA_S-BDT.root

## Subtraction systematics
sbm1_ef=${std_path}/TMVATrees/run/gen_sub_sys/Efficiencies_sign.root
sbm1_hf=${std_path}/HistFactory/run/run_sub_sys/eval/hf_fit_sub_sys_A-qtA_S-BDT_DiMuonMass1_Tau0.root
sbt1_ef=${std_path}/TMVATrees/run/gen_base/Efficiencies_sign.root
sbt1_hf=${std_path}/HistFactory/run/run_sub_sys/eval/hf_fit_sub_sys_A-qtA_S-BDT_DiMuonMass0_Tau1.root
sbt2_ef=${std_path}/TMVATrees/run/gen_base/Efficiencies_sign.root
sbt2_hf=${std_path}/HistFactory/run/run_sub_sys/eval/hf_fit_sub_sys_A-qtA_S-BDT_DiMuonMass0_Tau2.root

## dz systematics
dz02_ef=${std_path}/TMVATrees/run/gen_dz02/Efficiencies_sign.root
dz02_hf=${std_path}/HistFactory/run/run_tau_dz02/eval/hf_fit_tau_dz02_A-qtA_S-BDT.root

# purw systematics
purw_ef=${std_path}/TMVATrees/run/gen_pu_weighted/Efficiencies_sign.root
purw_hf=${std_path}/HistFactory/run/run_base/eval/hf_fit_base_A-qtA_S-BDT.root

## Combination bdt systematics
var_sys_ef="${base_ef}:${base_ef}:${base_ef}:${base_ef}:${base_ef}:${base_ef}:${base_ef}:${base_ef}:${base_ef}:${base_ef}"
var_sys_hf="${aaDPDY_hf}:${afDPDY_hf}:${faDPDY_hf}:${ffDPDY_hf}:${aaf_hf}:${afa_hf}:${faa_hf}:${extd_hf}:${noL_hf}:${noqt2_hf}"

## Combination subtraction systematics
sub_sys_ef="${sbm1_ef}:${sbt1_ef}:${sbt2_ef}" 
sub_sys_hf="${sbm1_hf}:${sbt1_hf}:${sbt2_hf}" 

## Scale Factor error
scale_fac="${std_path}/ScaleFactor/read/run/scale_factor_base.root"

## Extras
unique="aaDPDY:afDPDY:faDPDY:ffDPDY:aaf:afa:faa:extd:noL:noqt2#sbm1:sbt1:sbt2#dz02#purw" 
so_err_unique="ScaleFactors"
qta_range="qtA_15,-10,20"

mkdir -p sys

$ex -m "SYS" -p ${base_ef} -f ${base_hf} -z "${var_sys_ef}#${sub_sys_ef}#${dz02_ef}#${purw_ef}" -y "${var_sys_hf}#${sub_sys_hf}#${dz02_hf}#${purw_hf}" -t 99 -a qtA -v BDT -g sign -u ${unique} -r ${qta_range} -c "var#sub#dz02#purw" -s ${scale_fac} -e "${so_err_unique}" -q "base"
