executable="${ANA_IP}/plotting/systematics/build/systematics"

base_filepath="${OUT_PATH}/event_hf/qtb_lower/eval/"
base_efficiency="${OUT_PATH}/trees/qtb_lower/sign_efficiencies_qtb_lower.root"
base_unique="qbtsplit-1"

## BDT Variable systematics
aaDPDY_hf=${OUT_PATH}/event_hf/qtb_lower_aaDPDY/eval/
aaDPDY="${aaDPDY_hf}:${base_efficiency}:qtb_lower_aaDPDY"
faDPDY_hf=${OUT_PATH}/event_hf/qtb_lower_faDPDY/eval/
faDPDY="${faDPDY_hf}:${base_efficiency}:qtb_lower_faDPDY"
ffDPDY_hf=${OUT_PATH}/event_hf/qtb_lower_ffDPDY/eval/
ffDPDY="${ffDPDY_hf}:${base_efficiency}:qtb_lower_ffDPDY"
aaf_hf=${OUT_PATH}/event_hf/qtb_lower_aaf/eval/
aaf="${aaf_hf}:${base_efficiency}:qtb_lower_aaf"
afa_hf=${OUT_PATH}/event_hf/qtb_lower_afa/eval/
afa="${afa_hf}:${base_efficiency}:qtb_lower_afa"
faa_hf=${OUT_PATH}/event_hf/qtb_lower_faa/eval/
faa="${faa_hf}:${base_efficiency}:qtb_lower_faa"
extd_hf=${OUT_PATH}/event_hf/qtb_lower_extd/eval/
extd="${extd_hf}:${base_efficiency}:qtb_lower_extd"
noL_hf=${OUT_PATH}/event_hf/qtb_lower_noL/eval/
noL="${noL_hf}:${base_efficiency}:qtb_lower_noL"
noqt2Disc_hf=${OUT_PATH}/event_hf/qtb_lower_noqt2Disc/eval/
noqt2Disc="${noqt2Disc_hf}:${base_efficiency}:qtb_lower_noqt2Disc"

var="${aaDPDY}#${faDPDY}#${ffDPDY}#${aaf}#${afa}#${faa}#${extd}#${noL}#${noqt2Disc}"


## Subtraction systematics
sbm1_hf=${OUT_PATH}/event_hf/qtb_lower_sub_mass1_sys/eval/
sbm1_ef="${OUT_PATH}/trees/qtb_lower_sub_mass1_sys/sign_efficiencies_qtb_lower_sub_mass1_sys.root"
sbm1="${sbm1_hf}:${sbm1_ef}:qtb_lower_sub_mass1_sys"
sbt1_hf=${OUT_PATH}/event_hf/qtb_lower_sub_tau1_sys/eval/
sbt1_ef=${base_efficiency}
sbt1="${sbt1_hf}:${sbt1_ef}:qtb_lower_sub_tau1_sys"
sbt2_hf=${OUT_PATH}/event_hf/qtb_lower_sub_tau2_sys/eval/
sbt2_ef=${base_efficiency}
sbt2="${sbt2_hf}:${sbt2_ef}:qtb_lower_sub_tau2_sys"
yexp_ef=${base_efficiency}
yexp_hf=${OUT_PATH}/event_hf/qtb_lower_sub_yexp_sys/eval/
yexp="${yexp_hf}:${yexp_ef}:qtb_lower_sub_yexp_sys"

sub="${sbm1}#${sbt1}#${sbt2}#${yexp}"


## dz systematics
dz02_hf=${OUT_PATH}/event_hf/qtb_lower_dz02/eval/
dz02_ef=${OUT_PATH}/trees/qtb_lower_dz02/sign_efficiencies_qtb_lower_dz02.root
dz02="${dz02_hf}:${dz02_ef}:qtb_lower_dz02"

# purw systematics
purw_ef=${OUT_PATH}/trees/pu_weighted/sign_efficiencies_pu_weighted.root
purw_hf=${base_filepath}
purw="${purw_hf}:${purw_ef}:qtb_lower"


## Scale Factor error
sf_path="${OUT_PATH}/sf_conversion/qtbsplit/scale_factor_qtb_lower.root"
sf="${sf_path}:scalefactor"

## Extras
group_names="var:sub:dz02:purw:sf"
selection_file="${LIB_PATH}/share/hf_bounds.txt"
unique="qtb_lower"
systematics_group="${var}&${sub}"
systematics_singles="${dz02}#${purw}"
systematics_predefines="${sf}"


mkdir -p ${OUT_PATH}/systematics/
mkdir -p ${OUT_PATH}/systematics/sys

pushd ${OUT_PATH}/systematics >> /dev/null

$executable -i ${base_filepath} -e ${base_efficiency} -g "${systematics_group}" -y ${systematics_singles} -z ${systematics_predefines} -a "qtA" -s "BDT" -q ${group_names} -u ${unique} -v ${selection_file} -q "${group_names}"
##$executable -i ${base_hf} -e ${base_ef} -y "${systematics_hf}" -z ${systematics_ef} -d ${solo_err_hf} -n ${solo_uniques} -a "qtA" -s "BDT" -t "sign:99" -g ${group_names} -u ${uniques} -v ${selection_file} -m ${fit_file} -q "re445" -b

popd >> /dev/null
