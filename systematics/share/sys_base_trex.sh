executable="${exec_path}/systematics"

base_filepath="${OUT_PATH}/event_hf/base/eval/"
base_efficiency="${OUT_PATH}/trees/base/efficiency/sign_efficiencies_base.root"
trex_filepath="${OUT_PATH}/trex/base/trex_processed_base.root"
base_unique="qbtsplit-1"

## BDT Variable systematics
aaDPDY_hf=${OUT_PATH}/event_hf/base_aaDPDY/eval/
aaDPDY="${aaDPDY_hf}:${base_efficiency}:base_aaDPDY"
faDPDY_hf=${OUT_PATH}/event_hf/base_faDPDY/eval/
faDPDY="${faDPDY_hf}:${base_efficiency}:base_faDPDY"
ffDPDY_hf=${OUT_PATH}/event_hf/base_ffDPDY/eval/
ffDPDY="${ffDPDY_hf}:${base_efficiency}:base_ffDPDY"
aaf_hf=${OUT_PATH}/event_hf/base_aaf/eval/
aaf="${aaf_hf}:${base_efficiency}:base_aaf"
afa_hf=${OUT_PATH}/event_hf/base_afa/eval/
afa="${afa_hf}:${base_efficiency}:base_afa"
faa_hf=${OUT_PATH}/event_hf/base_faa/eval/
faa="${faa_hf}:${base_efficiency}:base_faa"
extd_hf=${OUT_PATH}/event_hf/base_extd/eval/
extd="${extd_hf}:${base_efficiency}:base_extd"
noL_hf=${OUT_PATH}/event_hf/base_noL/eval/
noL="${noL_hf}:${base_efficiency}:base_noL"
noqt2Disc_hf=${OUT_PATH}/event_hf/base_noqt2Disc/eval/
noqt2Disc="${noqt2Disc_hf}:${base_efficiency}:base_noqt2Disc"

var="${aaDPDY}#${faDPDY}#${ffDPDY}#${aaf}#${afa}#${faa}#${extd}#${noL}#${noqt2Disc}"


## Subtraction systematics
sbm1_hf=${OUT_PATH}/event_hf/base_sub_mass1_sys/eval/
sbm1_ef="${OUT_PATH}/trees/base_sub_mass1_sys/efficiency/sign_efficiencies_base_sub_mass1_sys.root"
sbm1="${sbm1_hf}:${sbm1_ef}:base_sub_mass1_sys"
sbt1_hf=${OUT_PATH}/event_hf/base_sub_tau1_sys/eval/
sbt1_ef=${base_efficiency}
sbt1="${sbt1_hf}:${sbt1_ef}:base_sub_tau1_sys"
sbt2_hf=${OUT_PATH}/event_hf/base_sub_tau2_sys/eval/
sbt2_ef=${base_efficiency}
sbt2="${sbt2_hf}:${sbt2_ef}:base_sub_tau2_sys"
yexp_ef=${base_efficiency}
yexp_hf=${OUT_PATH}/event_hf/base_sub_yexp_sys/eval/
yexp="${yexp_hf}:${yexp_ef}:base_sub_yexp_sys"

sub="${sbm1}#${sbt1}#${sbt2}#${yexp}"


## dz systematics
dz02_hf="${OUT_PATH}/event_hf/base_dz02/eval/"
dz02_ef="${OUT_PATH}/trees/base_dz02/efficiency/sign_efficiencies_base_dz02.root"
dz02="${dz02_hf}:${dz02_ef}:base_dz02"

# purw systematics
purw_ef="${OUT_PATH}/trees/base_pu_weighted/efficiency/sign_efficiencies_base_pu_weighted.root"
purw_hf="${base_filepath}"
purw="${purw_hf}:${purw_ef}:base"


## Scale Factor error
##sf_path="${OUT_PATH}/sf_conversion/qtbsplit/scale_factor_base.root"
sf_muon_path="${OUT_PATH}/trees/base/scalefactors/sf_data_base.root"
sf_muon="${sf_muon_path}:sf_muon"
sf_photon_path="${OUT_PATH}/trees/base/scalefactors/sf_data_base.root"
sf_photon="${sf_photon_path}:sf_photon"

## Extras
group_names="var:sub:dz02:purw:sf_muon:sf_photon"
selection_file="${LIB_PATH}/share/hf_bounds.txt"
unique="base"
systematics_group="${var}&${sub}"
systematics_singles="${dz02}#${purw}"
systematics_predefines="${sf_muon}#${sf_photon}"


mkdir -p ${OUT_PATH}/systematics/
mkdir -p ${OUT_PATH}/systematics/sys

pushd ${OUT_PATH}/systematics >> /dev/null

$executable -i ${base_filepath} -e ${base_efficiency} -t ${trex_filepath} -g "${systematics_group}" -y ${systematics_singles} -z ${systematics_predefines} -q ${group_names} -u ${unique} -v ${selection_file} -q "${group_names}" -a "qtA" -s "BDT" -m "Q12" -n "base_trex"

popd >> /dev/null
