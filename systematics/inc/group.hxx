
#include <anna.hxx>

void image_group( std::vector<TH1F*> group_systematics, TH1F * base, std::vector<std::string> group_uniques,
                 std::string store_unique );
