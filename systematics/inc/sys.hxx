#ifndef sys_hxx
#define sys_hxx

#include <anna.hxx>

// a structure for storing all the paths i'll need.
struct uncertainty_paths{
  std::string baseline;
  std::string systematics;
  std::string solo;
  std::string uniques;
  std::string groups;
};

void compare_systematic( TH1F * base_o, TH1F * sys_o, std::string & unique, variable_set & variables );

void sys( basic_fileset * baseline, std::vector< systematic_group * > & systematics, variable_set & variables, std::string & unique, bool normalise, const std::string & name="" );

#endif
