#ifndef fit_plot_hxx
#define fit_plot_hxx

#include <anna.hxx>

#include <TGraphErrors.h>

void fit_plot( variable_set & variables, std::string & unique );

#endif
