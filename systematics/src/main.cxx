#include <main.hxx>
#include <sys.hxx>
#include <fit_plot.hxx>


int help(){
  std::cout << "h     --    help      --  output help" << std::endl;
  std::cout << "d     --    data      --  path to a data file" << std::endl;
  std::cout << "s     --    sign      --  path to a sign file" << std::endl;
  std::cout << "b     --    bckg      --  path to a bckg file" << std::endl;
  std::cout << "a     --    avar      --  colon separated list of analysis bin variables" << std::endl;
  std::cout << "v     --    svar      --  colon separated list of spectator variables" << std::endl;
  std::cout << "t     --    slice     --  subtraction slice of interest, default 99" << std::endl;
  std::cout << "g     --    type      --  type of interest, data, sign, bckg" << std::endl;
  std::cout << "u     --    uniq      --  unique name for run" << std::endl;
  std::cout << "p     --    path      --  path and partial filename to files for recombination" << std::endl;
  std::cout << "e     --    dsign     --  path to signal component of simulated data file " << std::endl;
  std::cout << "w     --    dbckg     --  path to background component of simulated data file" << std::endl;
  std::cout << "                          or weighted efficiency path list" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"hfin",        required_argument,        0,      'i'},
      {"avar",        required_argument,        0,      'a'},
      {"svar",        required_argument,        0,      'v'},
      {"slice",       required_argument,        0,      't'},
      {"type",        required_argument,        0,      'g'},
      {"uniq",        required_argument,        0,      'u'},
      {"spath",       required_argument,        0,      'p'},
      {"syseff",      required_argument,        0,      'z'},
      {"syshf",       required_argument,        0,      'y'},
      {"range",       required_argument,        0,      'r'},
      {"comb",        required_argument,        0,      'c'},
      {"soerr",       required_argument,        0,      's'},
      {"soname",      required_argument,        0,      'e'},
      {"fulluniq",    required_argument,        0,      'q'},
      {"norm",        no_argument,              0,      'b'},
      {0,             0,                        0,      0}
    };

  std::string analysis_var, spectator_var, ranges, selection_path;
  std::string baseline_filepath, baseline_efficiency;
  std::string trex_filepath;
  std::string systematics_groups, systematics_singles, systematics_predefines;
  std::string unique, systematics_names, name;
  std::string mass;
  bool normalise = false;

  do {
    option = getopt_long(argc, argv, "i:a:v:t:u:p:g:y:z:r:c:s:e:q:m:n:d:bh", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'i': 
        baseline_filepath           = std::string(optarg);
        break;
      case 'e':
        baseline_efficiency         = std::string( optarg );
        break;
      case 'g':
        systematics_groups          = std::string( optarg );
        break;
      case 'y':
        systematics_singles         = std::string( optarg );
        break;
      case 'z':
        systematics_predefines      = std::string( optarg );
        break;
      case 'q':
        systematics_names           = std::string( optarg );
        break;
      case 'a': 
        analysis_var              = std::string( optarg );
        break;
      case 's': 
        spectator_var             = std::string( optarg );
        break;
      case 'r':
        ranges                    = std::string( optarg );
        break;
      case 'v':
        selection_path            = std::string( optarg );
        break;
      case 'u':
        unique                    = std::string( optarg );
        break;
      case 'm':
        mass                      = std::string( optarg );
        break;
      case 't':
        trex_filepath             = std::string( optarg );
        break;
      case 'b':
        normalise = true; 
        break;
      case 'n':
        name                      = std::string( optarg );
        break;
    }
  } while (option != -1);
  
  variable_set variables = variable_set( selection_path, ranges, analysis_var, spectator_var );

  basic_fileset * baseline = new basic_fileset();
  baseline->load_final_fileset( baseline_filepath, baseline_efficiency, unique ); 
  if ( !trex_filepath.empty() ){
    std::cout << "load" << std::endl;
    baseline->load_trex_fileset( trex_filepath, unique );
  }

  std::vector< systematic_group * > systematics;

  std::vector< std::string > systematic_group_names;
  split_strings( systematic_group_names, systematics_names, ":" );
  std::vector< std::string > group_strings;
  split_strings( group_strings, systematics_groups, "&" );
  int systematic_index = 0;


  for ( std::string & systematic_group_string : group_strings ){

    std::vector< std::string > individual_systematics;
    split_strings( individual_systematics, systematic_group_string, "#" );

    systematic_group * systematic = new systematic_group();
    systematic->set_group();
    systematic->group_name = systematic_group_names.at( systematic_index );
     
    for ( std::string & systematic_string : individual_systematics ){
      std::vector< std::string > systematic_paths;
      split_strings( systematic_paths, systematic_string, ":" );
      systematic->add_fileset( systematic_paths.at( 0 ), systematic_paths.at( 1 ), systematic_paths.at( 2 ) );
    }

    systematics.push_back( systematic );
    systematic_index += 1;
  }

  std::vector< std::string > single_strings;
  split_strings( single_strings, systematics_singles, "#" );

  for ( std::string & single_systematic_string : single_strings ){

    std::vector< std::string > systematic_paths;
    split_strings( systematic_paths, single_systematic_string, ":" );
    systematic_group * systematic = new systematic_group();
    systematic->set_single();
    systematic->group_name = systematic_group_names.at( systematic_index );
    systematic->add_fileset( systematic_paths.at( 0 ), systematic_paths.at( 1 ), systematic_paths.at( 2 ) );
    systematics.push_back( systematic );
    systematic_index += 1;

  }

  std::vector< std::string > predefine_strings;
  split_strings( predefine_strings, systematics_predefines, "#" );
  int predefines = 0;
  for ( std::string & predefine_systematic_string : predefine_strings ){
    fileset_type current_type = (predefines == 0) ? fileset_type::sf_muon : fileset_type::sf_photon;
    std::vector< std::string > systematic_paths;
    split_strings( systematic_paths, predefine_systematic_string, ":" );
    systematic_group * systematic = new systematic_group();
    systematic->set_predefined();
    systematic->group_name = systematic_group_names.at( systematic_index );
    systematic->add_file( systematic_paths.at(0), systematic_paths.at(1), current_type );
    systematics.push_back( systematic );
    systematic_index += 1;
    predefines++;
  }

  sys( baseline, systematics, variables, unique, normalise, name );
  
  if ( unique.find( "qtbsplit" ) != std::string::npos ){
    std::string var_plot_unique = "qtbsplit";
    fit_plot( variables, var_plot_unique );
  }



  return 0;
}

