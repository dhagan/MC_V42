#include <sys.hxx>
#include <group.hxx>


void compare_systematic( TH1F * base_o, TH1F * sys_o, const std::string & var, const std::string & unique, bound_mgr * selections ){

  TH1F * base = (TH1F *) base_o->Clone();
  TH1F * sys = (TH1F *) sys_o->Clone();

   // prep bound and variables
  bound var_bound = selections->get_bound( var );
  int bins = var_bound.get_bins();
  double min = var_bound.get_min();
  double max = var_bound.get_max();


  // ready lines and diffs
  TF1 * unit_line = new TF1( "unit_line", "1", min, max );
  TF1 * zero_line = new TF1( "zero_line", "1", min, max );
  TH1F * unit_hist = new TH1F( "unit", "", bins, min, max );
  for ( int idx = 1; idx <= unit_hist->GetNbinsX(); idx++ ){ unit_hist->SetBinContent( idx, 1.0); }
  TH1F * rel_diff = sys_to_error_hist( base, sys, false );
  TH1F * abs_diff = sys_to_error_hist( base, sys, true );
  TH1F * errorbar_hist = single_sys_to_error( base, sys );

  rel_diff->Add( rel_diff, unit_hist );

  // create symm error graphs
  TGraphAsymmErrors * rel_err_graph = diff_graph( base, sys, false );
  rel_err_graph->SetFillStyle( 1 );
  rel_err_graph->SetFillColorAlpha( kBlue+1, 0.6 );
  rel_err_graph->SetLineWidth( 0 );

  TGraphAsymmErrors * abs_err_graph = diff_graph( base, sys, true );
  abs_err_graph->SetFillStyle( 1 );
  abs_err_graph->SetFillColorAlpha( kBlue+1, 0.6 );
  abs_err_graph->SetLineWidth( 0 );

  // style base and sys;
  std::vector< float > base_style = { 1, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 3.0, 0, 0, 0 };
  std::vector< float > errorbar_style = { 1, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0, 0, 0 };
  std::vector< float > sys_style =  { 1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kRed+1, 0.6, 1 };

  style_hist( base, base_style );
  style_hist( sys, sys_style );
  style_hist( errorbar_hist, errorbar_style );

  // construct canvases
  TCanvas * error_canv = new TCanvas( "err_canv", "", 100, 100, 2000, 2000 );
  error_canv->Divide( 2, 2 );
  TPad * current_pad;

  current_pad = (TPad *) error_canv->cd( 1 );
  base->Draw( "HIST" );
  sys->Draw( "HIST SAMES" );
  hist_prep_axes( base, true );
  add_pad_title( current_pad, Form( "%s %s", var.c_str(), unique.c_str() ), true );
  add_atlas_decorations( current_pad, true, false );
  set_axis_labels( base, var_bound.get_x_str(), Form( "Extracted Yield %s", var_bound.get_y_str().c_str() ) );
  TLegend * base_legend = create_atlas_legend();
  base_legend->AddEntry( base, "Nominal" );
  base_legend->AddEntry( sys, "Systematic" );
  base_legend->Draw( "SAME" );

  current_pad = (TPad *) error_canv->cd( 2);
  errorbar_hist->Draw( "HIST E1" );
  hist_prep_axes( errorbar_hist, true );
  add_pad_title( current_pad, Form( "%s %s", var.c_str(), unique.c_str() ), true );
  add_atlas_decorations( current_pad, true, false );
  set_axis_labels( errorbar_hist, var_bound.get_x_str(), Form( "Extracted Yield %s", var_bound.get_y_str().c_str() ) );
  TLegend * errorbar_legend = create_atlas_legend();
  errorbar_legend->AddEntry( errorbar_hist, "#splitline{Nominal +}{sys error}" );
  errorbar_legend->Draw( "SAME" );


  current_pad = (TPad *) error_canv->cd( 3 );
  rel_diff->Draw( "HIST P" );
  rel_diff->SetMarkerStyle( 21 );
  rel_err_graph->Draw( "3 SAME ");
  unit_line->Draw( "SAME L" );
  hist_prep_axes( rel_diff, true );// true, 1.0 );
  add_pad_title( current_pad, Form( "Relative Error %s %s", var.c_str(), unique.c_str() ), false );
  add_atlas_decorations( current_pad, true, false );
  set_axis_labels( rel_diff, var_bound.get_x_str(), Form( "Relative Error %s", var_bound.get_y_str().c_str() ) );
  if ( unique.find( "dz02" ) != std::string::npos ){ rel_diff->GetYaxis()->SetRangeUser( 0.5, 1.5 ); }
  TLegend * rel_legend = create_atlas_legend();
  rel_legend->AddEntry( rel_err_graph, "Relative Error" );
  rel_legend->AddEntry( unit_line, "Nominal" );
  rel_legend->Draw( "SAME" );

  current_pad = (TPad *) error_canv->cd( 4 );
  abs_diff->Draw( "HIST P" );
  abs_diff->SetMarkerStyle( 21 );
  abs_err_graph->Draw( "3 SAME" );
  zero_line->Draw( "SAME L" );
  hist_prep_axes( abs_diff, false );//, true, 0.0 );
  add_pad_title( current_pad, Form( "Absolute Error %s %s", var.c_str(), unique.c_str() ), true );
  add_atlas_decorations( current_pad, true, false );
  set_axis_labels( abs_diff, var_bound.get_x_str(), Form( "Absolute Error %s", var_bound.get_y_str().c_str() ) );
  TLegend * abs_legend = create_atlas_legend();
  abs_legend->AddEntry( abs_err_graph, "Absolute Error" );
  abs_legend->AddEntry( zero_line, "Nominal" );
  abs_legend->Draw( "SAME" );

  error_canv->SaveAs( Form( "./sys/%s_%s_sys.png", var.c_str(), unique.c_str() ) );
  delete error_canv;

}


void sys( basic_fileset * baseline, std::vector< systematic_group * > & systematics, variable_set & variables, 
          std::string & unique, bool normalise, const std::string & name ){

  // Set the canvas/hist styles
  prep_style();

  bool mass_differential = ( variables.mass.find("Q12") != std::string::npos ); 
  bool var_differential = ( variables.analysis_variable.find( "qtA" ) != std::string::npos );
  bool differential = ( mass_differential && var_differential );

  if ( differential ){ 
    std::cout << "Differential mode." << std::endl;
  } else {
    std::cout << "Weighted mode" << std::endl;
  }

  bound analysis_bound = variables.analysis_bound;
  bound spectator_bound = variables.spectator_bound;
  hist_group baseline_histograms;  
  if ( baseline->typecheck( fileset_type::trex ) && differential ){
    std::cout << "Trex production." << std::endl;
    baseline_histograms = baseline->recreate_histograms_trex( variables, true );
  } else {
    baseline_histograms = (differential) ? baseline->recreate_histograms_differential( variables, true ) : baseline->recreate_histograms_weights( variables, true );
  }

  std::map< std::string, TH1F* > grouped_systematics;
  TH1F * baseline_signal = baseline_histograms.sign_hist;
  //baseline_signal->SetName( "baseline_signal" );
  for ( systematic_group * systematic : systematics ){
    systematic->prepare_systematics( variables, differential );
    grouped_systematics[ systematic->group_name ] = systematic->evaluate_systematic( variables, baseline_signal );
  }
  // combined systematics
  std::vector< TH1F *> grouped_systematics_histograms;
  std::vector< std::string > group_names;
  for ( systematic_group * systematic : systematics ){
    grouped_systematics_histograms.push_back( systematic->grouped_systematic );
    group_names.push_back( systematic->group_name );
  }

  // apply sf_corrections
  for ( systematic_group * systematic : systematics ){
    if ( systematic->predefined && systematic->systematics_files.at(0)->type == fileset_type::sf_muon ){
      systematic->systematics_files.at( 0 )->apply_scalefactor_correction( baseline_signal, variables, sf_muon );    
      for ( TH1F * sys_hist : grouped_systematics_histograms ){ 
        systematic->systematics_files.at( 0 )->apply_scalefactor_correction( sys_hist, variables, sf_muon );
      }
    }
    if ( systematic->predefined && systematic->systematics_files.at(0)->type == fileset_type::sf_photon ){
      systematic->systematics_files.at( 0 )->apply_scalefactor_correction( baseline_signal, variables, sf_photon );    
      for ( TH1F * sys_hist : grouped_systematics_histograms ){ 
        systematic->systematics_files.at( 0 )->apply_scalefactor_correction( sys_hist, variables, sf_photon );
      }
    }
  }


  style_mgr * style_manager = new style_mgr(); 
  style_manager->load_styles();

  TH1F * total_error_histogram = quadrature_error_combination( baseline_signal, grouped_systematics_histograms, false );
  TH1F * total_systematic_error_histogram = quadrature_error_combination( baseline_signal, grouped_systematics_histograms, true );
  TH1F * total_statistical_error_histogram = (TH1F *) baseline_signal->Clone();

  TH1F * total_error_absolute = errorbar_to_hist( total_error_histogram, true );
  TH1F * total_systematic_error_absolute =  errorbar_to_hist( total_systematic_error_histogram, true );
  TH1F * total_statistical_error_absolute = errorbar_to_hist( baseline_signal, true );
  

  TH1F * total_error_relative = errorbar_to_hist( total_error_histogram, false );
  TH1F * total_systematic_error_relative = errorbar_to_hist( total_systematic_error_histogram, false );
  TH1F * total_statistical_error_relative =  errorbar_to_hist( baseline_signal, false );

  TH1F * total_error_histogram_sg = (TH1F *) total_error_histogram->Clone();
  TH1F * total_error_histogram_dg = (TH1F *) total_error_histogram->Clone();
  TH1F * total_statistical_error_sg = (TH1F*) baseline_signal->Clone();
  TH1F * total_statistical_error_dg = (TH1F*) baseline_signal->Clone();

  style_manager->style_histogram( total_error_histogram, "total_histogram" );
  style_manager->style_histogram( total_error_relative, "total_error" );
  style_manager->style_histogram( total_error_absolute, "total_error" );

  style_manager->style_histogram( total_systematic_error_histogram, "systematics_histogram" );
  style_manager->style_histogram( total_systematic_error_relative,  "systematics_error" );
  style_manager->style_histogram( total_systematic_error_absolute,  "systematics_error" );

  style_manager->style_histogram( total_statistical_error_histogram, "statistical_histogram" );
  style_manager->style_histogram( total_statistical_error_relative,  "statistical_error" );
  style_manager->style_histogram( total_statistical_error_absolute,  "statistical_error" );

  style_manager->style_histogram( total_error_histogram_sg, "total_histogram" );
  style_manager->style_histogram( total_error_histogram_dg, "total_histogram" );
  style_manager->style_histogram( total_statistical_error_sg, "statistics_histogram"  );
  style_manager->style_histogram( total_statistical_error_dg, "statistics_histogram"  );

  TF1 * total_dg = prep_dg( variables.analysis_bound.get_min(), variables.analysis_bound.get_max() );
  TF1 * total_sg = prep_sg( variables.analysis_bound.get_min()+6, variables.analysis_bound.get_max()-6 );

  fit_mgr * fit_manager = new fit_mgr();
  fit_manager->load_fits();
  std::string parameter_key = Form( "systematics_fit_process_%s_%s_%s", variables.analysis_variable.c_str(), variables.mass.c_str(), unique.c_str() );
  std::cout << parameter_key << std::endl;
  fit_manager->load_parameterisation( total_dg, parameter_key, "dg", true );
  fit_manager->load_parameterisation( total_sg, parameter_key, "sg", true );
  
  TF1 * inner_sg = prep_sg( variables.analysis_bound.get_min(), variables.analysis_bound.get_max() );
  TF1 * outer_sg = prep_sg( variables.analysis_bound.get_min(), variables.analysis_bound.get_max() );

  inner_sg->SetLineStyle( 2 );
  inner_sg->SetLineColor( kRed+2 );

  
  TCanvas * test_canvas = new TCanvas( "systematics_canvas", "canv", 100, 100, 4500, 4500 );
  test_canvas->Divide( 3, 3 );

  // regular error bars of grouped systematics
  TPad * active_pad = (TPad *) test_canvas->cd( 1 );
  total_error_histogram->Draw( "HIST" );
  total_statistical_error_histogram->Draw( "E1 SAME" );
  total_systematic_error_histogram->Draw( "E1 SAME" );
  total_error_histogram->SetStats( kFALSE );
  hist_prep_axes( total_error_histogram );
  add_pad_title( active_pad, Form( "qtA distribution with errors - %s", variables.mass.c_str() ) );
  set_axis_labels( total_error_histogram, variables.analysis_bound.get_x_str(), Form( "Yield/%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_atlas_decorations( active_pad, true, false );
  TLegend * dist_pad_legend = create_atlas_legend();
  dist_pad_legend->AddEntry( baseline_signal, "statistical" );
  
  for ( std::string & group_name : group_names ){
    style_manager->style_histogram( grouped_systematics[ group_name ],  group_name + "_histogram" );
    grouped_systematics[ group_name ]->Draw( "SAME E1" );
    dist_pad_legend->AddEntry( grouped_systematics[ group_name ], group_name.c_str() , "LP" );
  } 
  dist_pad_legend->Draw();


  active_pad = (TPad *) test_canvas->cd( 2 );
  hist_prep_axes( total_error_absolute );
  total_error_absolute->Draw( "HIST" );
  total_statistical_error_absolute->Draw( "HIST SAME" );
  total_systematic_error_absolute->Draw( "HIST SAME" );
  total_error_absolute->SetStats( kFALSE );
  add_pad_title( active_pad, Form( "Absolute error - %s", variables.mass.c_str() ) );
  set_axis_labels( total_error_absolute, variables.analysis_bound.get_x_str(), Form( "Yield/%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_atlas_decorations( active_pad, true, false );
  TLegend * abs_pad_legend = create_atlas_legend();
  abs_pad_legend->AddEntry( total_error_absolute, "all uncertainty", "L" );
  abs_pad_legend->AddEntry( total_statistical_error_absolute, "statistical", "F" );
  abs_pad_legend->AddEntry( total_systematic_error_absolute, "all systematic", "L" );
  for ( std::string & group_name : group_names ){
    TH1F * temp_error = errorbar_to_hist( grouped_systematics[ group_name ], true );
    temp_error->Draw( "P SAME" );
    style_manager->style_histogram( temp_error, group_name + "_error" );
    abs_pad_legend->AddEntry( temp_error, group_name.c_str() , "LP" );
  } 
  abs_pad_legend->Draw();


  active_pad = (TPad *) test_canvas->cd( 3 );
  hist_prep_axes( total_error_relative );
  total_error_relative->Draw( "HIST" );
  total_error_relative->GetYaxis()->SetRangeUser( 0, 2 );
  total_error_relative->SetStats( kFALSE );
  total_statistical_error_relative->Draw( "HIST SAME" );
  total_systematic_error_relative->Draw( "HIST SAME" );
  set_axis_labels( total_error_relative, variables.analysis_bound.get_x_str(), Form( "Yield/%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( active_pad, Form( "Relative error - %s", variables.mass.c_str() ) );
  add_atlas_decorations( active_pad, true, false );
  TLegend * rel_pad_legend = create_atlas_legend();
  rel_pad_legend->AddEntry( total_error_relative, "all uncertainty", "L" );
  rel_pad_legend->AddEntry( total_statistical_error_relative, "statistical", "F" );
  rel_pad_legend->AddEntry( total_systematic_error_relative, "all systematic", "L" );
  for ( std::string & group_name : group_names ){
    TH1F * temp_error = errorbar_to_hist( grouped_systematics[ group_name ], false );
    temp_error->Draw( "P SAME" );
    style_manager->style_histogram( temp_error, group_name + "_error" );
    rel_pad_legend->AddEntry( temp_error, group_name.c_str() , "P" );
  } 
  rel_pad_legend->Draw();

    
  gStyle->SetOptStat( "krimes" ); 
  active_pad = (TPad *) test_canvas->cd( 7 );
  total_error_histogram->Draw( "HIST E1" );
  total_statistical_error_histogram->Draw( "SAME E1" );
  hist_prep_axes( total_error_histogram );
  set_axis_labels( total_error_histogram, variables.analysis_bound.get_x_str(), Form( "Yield/%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( active_pad, Form( "Total - %s", variables.mass.c_str() ) );
  add_atlas_decorations( active_pad, true, false );
  TLegend * total_legend = create_atlas_legend();
  total_legend->AddEntry( total_error_histogram, "total error", "L" );
  total_legend->AddEntry( total_statistical_error_histogram, "statistical error", "L" );
  total_legend->Draw();



  active_pad = (TPad *) test_canvas->cd( 8 );
  total_error_histogram_sg->Draw( "HIST E1" );
  total_statistical_error_sg->Draw( "SAME E1" );
  total_error_histogram_sg->Fit( total_sg, "M", "");
  hist_prep_axes( total_error_histogram_sg );
  set_axis_labels( total_error_histogram_sg, variables.analysis_bound.get_x_str(), Form( "Yield/%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( active_pad, Form( "Total - %s", variables.mass.c_str() ) );
  add_atlas_decorations( active_pad, true, false );
  total_sg->Draw( "SAME" );
  TPaveStats * sg_stats = make_stats( total_error_histogram_sg );
  sg_stats->Draw();

  
  active_pad = (TPad *) test_canvas->cd( 9 );
  total_error_histogram_dg->Draw( "HIST E1" );
  total_statistical_error_dg->Draw( "SAME E1" );
  total_error_histogram_dg->Fit( total_dg, "M", "" );
  split_dg( total_dg, inner_sg, outer_sg );
  hist_prep_axes( total_error_histogram_dg );
  set_axis_labels( total_error_histogram_dg, variables.analysis_bound.get_x_str(), Form( "Yield/%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( active_pad, Form( "Total - %s", variables.mass.c_str() ) );
  add_atlas_decorations( active_pad, true, false );
  total_dg->Draw( "SAME" );
  inner_sg->Draw( "SAME" );
  TPaveStats * dg_stats = make_stats( total_error_histogram_dg );
  dg_stats->Draw();


  std::string output_string = "systematics_" + variables.analysis_variable + "_" + variables.mass;
  if ( name.empty() ){ 
    output_string += "_" + unique;
  } else { 
    output_string += "_" + name;
  }
  if ( normalise ){ output_string += "_norm"; };
  output_string += ".png";
  test_canvas->SaveAs( output_string.c_str() );

  TFile * important_store = new TFile( "./important_store.root", "UPDATE" );
  important_store->cd();
  total_error_histogram_dg->SetName( unique.c_str() );
  total_error_histogram_dg->Write();
  important_store->Close();

  //fit_manager->save_parameterisation( total_dg, parameter_key, "dg", false, true );
  //fit_manager->save_parameterisation( total_sg, parameter_key, "sg", false, true );
  //fit_manager->save_fits();



}
//std::vector< float > base_error{ 0,0,1.0,0.00,1,1,1.00,1,0,            0.00,   0.00 };
  //std::vector< float > systematics_error{ 0,      0,          0.5,    0.00,   1,      1,      1.00,   1,1,            0.80,   0.00 };
  //std::vector< float > statistical_error{ 0,      0,          1.0,    0.00,   1,      1,      1.00,   0, 1,            0.3,   1001 };
  //std::vector< float > total_error{ 0,      0,          0.5,    0.00,   1,      1,      1.00,   3, 1,            0.50,   0    };
  //std::vector< float > var_error{ 23,     kRed+1,     1.00,   2.00,   0,      0,      0,      0, 0,            0.00,   0.00 };
  //std::vector< float > sub_error{ 22,     kBlue+1,    1.00,   2.00,   0,      0,      0,      0,  0,            0.00,   0.00 };
  //std::vector< float > dz02_error{ 33,     kGreen,     0.95,   2.00,   0,      0,      0,      0, 0,            0.00,   0.00 };
  //std::vector< float > purw_error{ 43,     kMagenta+1, 1.00,   2.00,   0,      0,      0,      0,  0,            0.00,   0.00 };
  //std::vector< float > sf_error{ 34,     kOrange+1,  1.00,   2.00,   0,      0,      0,      0, 0,            0.00,   0.00 };
  //std::vector< float > base_histogram{ 0,     0,           1.0,    1.00,   3,      1,      1.00,   3,       0,            0.00,   0.00 };
  //std::vector< float > systematics_histogram{ 0,     0,           0.5,    1.00,   1,      1,      1.00,   1,1,            0.80,   0.00 };
  //std::vector< float > statistical_histogram{ 1,     1,           0.5,    1.00,   1,      1,      1.00,   1, 1,            0.05,   0.00 };
  //std::vector< float > total_histogram{ 1,     1,           1.0,    1.00,   1,      1,      1.00,   3,1,            0.50,   0    };
  //std::vector< float > var_histogram{ 1,     0,           1.00,   1.00,   1,     kRed+1,   1.00,   1,0,            0.00,   0.00 };                                                                               
  //std::vector< float > sub_histogram{ 1,     0,           1.00,   1.00,   1,     kBlue+1,  1.00,   1,  0,            0.00,   0.00 };                                                                               
  //std::vector< float > dz02_histogram{ 1,     0,           0.95,   1.00,   1,     kGreen+1, 1.00,   1, 0,            0.00,   0.00 };                                                                               
  //std::vector< float > purw_histogram{ 1,     0,           1.00,   1.00,   1,     kMagenta+1,1.00,  1, 0,            0.00,   0.00 };                                                                               
  //std::vector< float > sf_histogram{ 1,     0,           1.00,   1.00,  1,     kOrange+1, 1.00,  1,  0,            0.00,   0.00 };
  //std::vector< float > fit{ 0,     0,           1.00,   1.00,  10,     kRed,      1.00,  1,  0,            0.00,   0.00 };

  
  //style_manager->set_stage( "systematics_fit_process" );
  //style_manager->add_style( "systematics_fit_process_baseline_error", base_error );
  //style_manager->add_style( "systematics_fit_process_systematics_error", systematics_error );
  //style_manager->add_style( "systematics_fit_process_statistical_error", statistical_error );
  //style_manager->add_style( "systematics_fit_process_total_error", total_error);
  //style_manager->add_style( "systematics_fit_process_var_error", var_error );
  //style_manager->add_style( "systematics_fit_process_sub_error", sub_error );
  //style_manager->add_style( "systematics_fit_process_dz02_error", dz02_error );
  //style_manager->add_style( "systematics_fit_process_purw_error", purw_error );
  //style_manager->add_style( "systematics_fit_process_sf_error", sf_error );
  //style_manager->add_style( "systematics_fit_process_baseline_histogram", base_histogram );
  //style_manager->add_style( "systematics_fit_process_systematics_histogram", systematics_histogram );
  //style_manager->add_style( "systematics_fit_process_statistical_histogram", statistical_histogram );
  //style_manager->add_style( "systematics_fit_process_total_histogram", total_histogram );
  //style_manager->add_style( "systematics_fit_process_var_histogram", var_histogram );
  //style_manager->add_style( "systematics_fit_process_sub_histogram", sub_histogram );
  //style_manager->add_style( "systematics_fit_process_dz02_histogram", dz02_histogram );
  //style_manager->add_style( "systematics_fit_process_purw_histogram", purw_histogram );
  //style_manager->add_style( "systematics_fit_process_sf_histogram", sf_histogram );
  //style_manager->add_style( "fit", );

  //style_manager->write_styles();

  
  //// logarithmic reproduction of pad 1
  //active_pad = (TPad *) test_canvas->cd( 4 ); 
  //TPad * dist_pad = (TPad *) ((TPad *) test_canvas->GetPad( 1 ))->Clone();
  //dist_pad->SetPad( active_pad->GetX1(), active_pad->GetY1(), active_pad->GetX2(), active_pad->GetY2() );
  //TList * prim_list = dist_pad->GetListOfPrimitives();
  //( (TH1F*) prim_list->At( 0 ))->GetYaxis()->SetRangeUser( abs_err_log_lower_limit, ( (TH1F*) prim_list->At( 0 ))->GetMaximum()*100 );
  //dist_pad->SetLogy();
  //dist_pad->Draw();

  //// logarithmic reproduction of pad 2
  //active_pad = (TPad *) test_canvas->cd( 5 ); 
  //TPad * abs_pad = (TPad *) ((TPad *) test_canvas->GetPad( 2 ))->Clone();
  //abs_pad->SetPad( active_pad->GetX1(), active_pad->GetY1(), active_pad->GetX2(), active_pad->GetY2() );
  //prim_list = abs_pad->GetListOfPrimitives();
  //( (TH1F*) prim_list->At( 0 ))->GetYaxis()->SetRangeUser( abs_err_log_lower_limit, ( (TH1F*) prim_list->At( 0 ))->GetMaximum()*100 );
  //abs_pad->SetLogy();
  //abs_pad->Draw();

  //// logarithmic reproduction of pad 3
  //active_pad = (TPad *) test_canvas->cd( 6 ); 
  //TPad * rel_pad = (TPad *) ((TPad *) test_canvas->GetPad( 3 ))->Clone();
  //rel_pad->SetPad( active_pad->GetX1(), active_pad->GetY1(), active_pad->GetX2(), active_pad->GetY2() );
  //prim_list = rel_pad->GetListOfPrimitives();
  //( (TH1F*) prim_list->At( 0 ))->GetYaxis()->SetRangeUser( 0.001, 100 );
  //rel_pad->SetLogy();
  //rel_pad->Draw();

  // final result with fully combined error.
  //active_pad = (TPad *) test_canvas->cd( 7 ); 
  //style_hist( stat_err_hist, style_map[ "stat_hist" ] );
  //gStyle->SetEndErrorSize( 15 );
  //combined_all_hist->Draw( "HIST E1 X0" );
  //stat_err_hist->Draw( "SAME HIST E1 X0" );
  ////stat_err_hist->SetEndErrorSize( 15 );
  //hist_prep_axes( combined_all_hist );
  //set_axis_labels( combined_all_hist, "qTa (J/#psi+#gamma) [GeV]", "Yield/2GeV");
  //add_pad_title( active_pad, Form( "qtA with combined error - Q%i", mass_index ) );
  //add_atlas_decorations( active_pad, true, false );
  //TLegend * combined_pad_legend = below_logo_legend();
  //combined_pad_legend->AddEntry( combined_all_hist, "Signal, all Uncertainty", "L" );
  //combined_pad_legend->AddEntry( stat_err_hist, "Statistical Uncertainty", "L" );
  //combined_pad_legend->Draw();

  //// fit of final result to a signle gaussian;
  //active_pad = (TPad *) test_canvas->cd( 8 ); 
  //sg_fit_comb_all_hist->Draw( "HIST E1 X0" );
  //sg_stat_err_hist->Draw( "SAME HIST E1 X0" ); 
  //hist_prep_axes( sg_fit_comb_all_hist );
  //set_axis_labels( sg_fit_comb_all_hist, "qTa (J/#psi+#gamma) [GeV]", "Yield/2GeV");
  //add_pad_title( active_pad, Form( "Single Gaussian Fit - Q%i", mass_index ) );
  //add_atlas_decorations( active_pad, true, false );
  //TLegend * sg_pad_legend = below_logo_legend();
  //sg_pad_legend->AddEntry( sg_fit_comb_all_hist, "Signal" );
  //sg_pad_legend->AddEntry( fit_sg, "Fit" );
  //sg_pad_legend->Draw();
  ////align_sg( fit_sg, sg_fit_comb_all_hist );
  //sg_fit_comb_all_hist->Fit( fit_sg, "MQR", "", abin_min, abin_max );
  //fit_sg->Draw( "SAME" );
  //TPaveStats * sg_stats = make_stats( sg_fit_comb_all_hist );
  //sg_stats->Draw();

  //// fit of final result to double gaussian
  //active_pad = (TPad *) test_canvas->cd( 9 ); 
  //dg_fit_comb_all_hist->Draw( "HIST E1 X0" );
  //dg_stat_err_hist->Draw( "SAME HIST E1 X0" ); 
  //hist_prep_axes( dg_fit_comb_all_hist );
  //set_axis_labels( dg_fit_comb_all_hist, "qTa (J/#psi+#gamma) [GeV]", "Yield/2GeV");
  //add_pad_title( active_pad, Form( "Double Gaussian Fit - Q%i", mass_index ) );
  //add_atlas_decorations( active_pad, true, false );
  //TLegend * dg_pad_legend = below_logo_legend();
  //dg_pad_legend->AddEntry( dg_fit_comb_all_hist, "Signal" );
  //dg_pad_legend->AddEntry( fit_dg, "Fit" );
  //dg_pad_legend->Draw();
  //dg_fit_comb_all_hist->Fit( fit_dg, "MQR", "", abin_min, abin_max );
  //split_dg( fit_dg, fit_dg_inner, fit_dg_outer );
  //fit_dg_inner->SetLineColor( kGreen+1 );
  //fit_dg_outer->SetLineColor( kMagenta+1 );
  //fit_dg_outer->SetLineStyle( 2 );
  //fit_dg->Draw( "SAME" );
  //fit_dg_inner->Draw( "SAME" );
  //fit_dg_outer->Draw( "SAME" );
  //dg_pad_legend->AddEntry( fit_dg_inner, "inner" );
  //dg_pad_legend->AddEntry( fit_dg_outer, "outer" );
  //TPaveStats * dg_stats = make_stats( dg_fit_comb_all_hist );
  //dg_stats->Draw();
  
















  



    //for ( TH1F * single_systematic_histogram : systematic->systematics_histograms ){
    //  compare_systematic( baseline_signal, single_systematic_histogram, single_systematic->unique, variables );
    //}

    


//gStyle->SetEndErrorSize( 15 );
  //style_hist( stat_err_hist, style_map[ "stat_hist" ] );
  //style_hist( sg_stat_err_hist, style_map[ "stat_hist" ] );
  //style_hist( dg_stat_err_hist, style_map[ "stat_hist" ] );






  
  
  ///// prep unique vectors
  ///std::vector< std::string > sys_all_uniques;
  ///std::vector< std::string > sys_group_uniques;
  ///std::vector< std::string > sys_group_hf_paths;
  ///std::vector< std::string > sys_group_eff_paths;
  ///split_strings( sys_all_uniques,     systematics.uniques, "#" );
  ///split_strings( sys_group_uniques,   systematics.groups,  "#" );
  ///split_strings( sys_group_hf_paths,  systematic_input_paths,  "#" );
  ///split_strings( sys_group_eff_paths, systematic_efficiency_paths, "#" );
  ///std::vector< std::string > other_paths, other_uniques;
  ///split_strings( other_paths,     solo_input_paths,     "#" );
  ///split_strings( other_uniques,   solo_uniques,    "#" );
       
  
	//double abs_err_upper_limit = 2000;  
	//double abs_err_log_lower_limit = 1;

  // prep systematics map

    // create structures to book systematics within group
    //std::vector< std::string > group_uniques, group_hf_paths, group_eff_paths;
    //std::string group_unique = sys_group_uniques.at( group_idx );
    //split_strings( group_uniques,     sys_all_uniques.at( group_idx ),        ":" );
    //split_strings( group_hf_paths,    sys_group_hf_paths.at( group_idx ),     ":" );
    //split_strings( group_eff_paths,   sys_group_eff_paths.at( group_idx ),    ":" );
    //std::vector< TH1F * > group_systematics;

    // iterate over individual systematics
  //  for ( int sys_idx = 0; sys_idx < (int) group_uniques.size(); sys_idx++ ){

  //    // prep individual systematic files and
  //    std::string current_unique          = group_uniques.at( sys_idx );

  //    TFile * sys_eff_file        = new TFile( group_eff_paths.at( sys_idx ).c_str(),    "READ");
  //    TFile * sys_integral_file   = new TFile( group_hf_paths.at( sys_idx ).c_str(),    "READ");
  //    TH1F * sys_ex_reco  = new TH1F( Form( "sys_ex_signal_reco_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
  //    TH1F * sys_eff      = (TH1F*) sys_eff_file->Get( Form( "eff_%s_%s", analysis_var.c_str(), mass_char ) );
  //
  //    // create reco histogram, apply efficiency correction.
  //    rc_hist( bin_name, err_name, sys_integral_file, sys_ex_reco, hf_error_bin );
  //    TH1F * sys_ex_corr = new TH1F( Form( "sys_corr_reco_%s_Q%i", current_unique.c_str(), mass_index ), "",
  //                                  abin_bins, abin_min, abin_max );


  //    sys_ex_corr->Divide( sys_ex_reco, sys_eff , 1.0, 1.0 );
  //    norms.push_back( sys_ex_corr->Integral() );
  //    if ( normalise ){ 
	//			if ( sys_ex_corr->GetSumw2N() == 0){ sys_ex_corr->Sumw2(kTRUE); }
  //      sys_ex_corr->Scale( 1.0/sys_ex_corr->Integral() ); 
	//			if ( sys_ex_corr->GetSumw2N() == 0){ sys_ex_corr->Sumw2(kTRUE); }
	//			sys_ex_corr->Scale( norms.at( 0 ) );
  //    }
  //    group_systematics.push_back( sys_ex_corr );

  //    // produce systematic differentials
  //    compare_systematic( base_ex_corr, sys_ex_corr, analysis_var, Form( "%s_%s_%s", version.c_str(), 
  //                        current_unique.c_str(), mass_char ), selections );

  // 	}

  //  // combine this systematic group
  //  TH1F * group_hist = new TH1F( Form( "group_sys_%s_Q%i", sys_group_uniques.at( group_idx ).c_str(), mass_index ),
  //                               "", abin_bins, abin_min, abin_max );
  //  if ( group_systematics.size() > 1 ){
  //    //image_group( group_systematics, base_ex_corr, group_uniques, Form( "%s_%s_%s", analysis_var.c_str(),
  //    //                                                                  mass_char, group_unique.c_str() ) );
  //    combine_sys_group( group_systematics, group_hist, base_ex_corr );
  //    grouped_systematics[ group_unique.c_str() ]  = group_hist;
  //  } else {
  //    grouped_systematics[ group_unique.c_str() ] = single_sys_to_error( base_ex_corr, group_systematics.at( 0 ) );
  //  }

  //}





//for ( int other_idx = 0; other_idx < (int) other_paths.size(); other_idx++ ){
  //  TFile * other_file = new TFile( other_paths.at( other_idx ).c_str() , "READ" );
  //  TH1F * other_hist = (TH1F *) other_file->Get( "err_Q12" );
  //  std::string other_unique = other_uniques.at( other_idx );
  //  grouped_systematics[ other_unique.c_str() ] = hist_to_errorbar( baseline_signal, other_hist, false );
  //}

  
