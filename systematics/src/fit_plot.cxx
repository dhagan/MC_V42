#include <fit_plot.hxx>

void fit_plot( variable_set & variables, std::string & unique ){

  prep_style();

  variables.mass = "Q12";
  bound & analysis_bound = variables.analysis_bound;
  //bound & spectator_bound = variables.spectator_bound;

  style_mgr * style_manager = new style_mgr(); 
  style_manager->load_styles();

  fit_mgr * fit_manager = new fit_mgr();
  fit_manager->load_fits();
  std::string split_key_base = Form( "systematics_fit_process_%s_%s_qtbsplit-", variables.analysis_variable.c_str(), variables.mass.c_str() );

  uint8_t splits = 3;
  float * x           = new float[3];
  float * c1          = new float[3];
  float * c1_err      = new float[3];
  float * mean        = new float[3];
  float * mean_err    = new float[3];
  float * sigma1      = new float[3];
  float * sigma1_err  = new float[3];
  float * c2          = new float[3];
  float * c2_err      = new float[3];
  float * sigma2      = new float[3];
  float * sigma2_err  = new float[3];

  std::vector< TF1 * > double_gaussians;
  double_gaussians.reserve( splits );
  for ( uint8_t index = 0; index < splits; index++ ){
    TF1 * double_gaussian = prep_dg( analysis_bound.get_min(), analysis_bound.get_max() );
    std::string key = split_key_base + std::to_string( index + 1 );
    fit_manager->load_parameterisation( double_gaussian, key, "dg", false );
    double_gaussians.push_back( double_gaussian );
    c1[index]         = double_gaussian->GetParameter( 0 );
    c1_err[index]     = double_gaussian->GetParError( 0 );
    mean[index]       = double_gaussian->GetParameter( 1 );
    mean_err[index]   = double_gaussian->GetParError( 1 );
    sigma1[index]     = double_gaussian->GetParameter( 2 );
    sigma1_err[index] = double_gaussian->GetParError( 2 );
    c2[index]         = double_gaussian->GetParameter( 3 );
    c2_err[index]     = double_gaussian->GetParError( 3 );
    sigma2[index]     = double_gaussian->GetParameter( 4 );
    sigma2_err[index] = double_gaussian->GetParError( 4 );
    x[index]      = index+1;
  }

  TF1 * sigma_2_fit = new TF1( "sigma_2_fit", "[0]*x+[1]");
  sigma_2_fit->SetParameter( 0, 0 );
  sigma_2_fit->SetParameter( 1, 2.7 );
  sigma_2_fit->SetParLimits( 0, -2, 2 );
  sigma_2_fit->SetParLimits( 1, 2, 4 );
  sigma_2_fit->SetLineStyle( 1 );
  sigma_2_fit->SetLineColor( kBlue + 1 );


  TGraphErrors * c1_graph     = new TGraphErrors( splits, x, c1,      nullptr, c1_err     );
  TGraphErrors * mean_graph   = new TGraphErrors( splits, x, mean,    nullptr, mean_err   );
  TGraphErrors * sigma1_graph = new TGraphErrors( splits, x, sigma1,  nullptr, sigma1_err );
  TGraphErrors * c2_graph     = new TGraphErrors( splits, x, c2,      nullptr, c2_err     );
  TGraphErrors * sigma2_graph = new TGraphErrors( splits, x, sigma2,  nullptr, sigma2_err );


  TH1F * hist = variables.analysis_bound.get_hist();

  double_gaussians.at( 0 )->SetLineColorAlpha( kRed, 1.0 ); 
  double_gaussians.at( 1 )->SetLineColorAlpha( kRed, 0.8 );
  double_gaussians.at( 2 )->SetLineColorAlpha( kRed, 0.6 );

  double_gaussians.at( 0 )->SetLineStyle( 1 ); 
  double_gaussians.at( 1 )->SetLineStyle( 2 );
  double_gaussians.at( 2 )->SetLineStyle( 3 );

  TCanvas * graph_canvas = new TCanvas( "param_canv", "", 200, 200, 3000, 2000 ); 
  graph_canvas->Divide( 3, 2 );

  TPad * current_pad = (TPad *) graph_canvas->cd( 1 ); 
  hist->Draw();
  hist->GetYaxis()->SetRangeUser( 0, 10000 );
  hist->SetStats( kFALSE );
  double_gaussians.at( 0 )->Draw( "SAME" );
  double_gaussians.at( 1 )->Draw( "SAME" );
  double_gaussians.at( 2 )->Draw( "SAME" );
  hist_prep_axes( hist );
  add_pad_title( current_pad, "Fits - Q12" );
  set_axis_labels( hist, variables.analysis_bound.get_x_str(), "Fit Value" );
  add_atlas_decorations( current_pad, true, false );
  TLegend * dist_pad_legend = create_atlas_legend();
  dist_pad_legend->AddEntry( double_gaussians.at( 0 ), "split-1" );
  dist_pad_legend->AddEntry( double_gaussians.at( 1 ), "split-2" );
  dist_pad_legend->AddEntry( double_gaussians.at( 2 ), "split-3" );
  dist_pad_legend->Draw();

  current_pad = (TPad *) graph_canvas->cd( 2 ); 
  c1_graph->Draw( "ALP" );
  c1_graph->SetStats( kFALSE );
  c1_graph->GetYaxis()->SetRangeUser( 0, 2000 );
  c1_graph->GetXaxis()->SetTitle( "qtb fit" );
  c1_graph->GetYaxis()->SetTitle( "C1" );
  c1_graph->GetXaxis()->SetRangeUser( 0, 5 );
  c1_graph->SetMarkerStyle( 21 );
  c1_graph->SetMarkerSize( 1 );
  c1_graph->SetMarkerColor( 1 );
  c1_graph->GetYaxis()->SetMaxDigits( 3 );
  add_pad_title( current_pad, "Double Gaussian Normalisation 1 - C1", true );
  add_atlas_decorations( current_pad, true, false );

  
  current_pad = (TPad *) graph_canvas->cd( 3 ); 
  mean_graph->Draw( "ALP" );
  mean_graph->SetStats( kFALSE );
  mean_graph->GetYaxis()->SetRangeUser( 0, 7 );
  mean_graph->GetXaxis()->SetTitle( "qtb fit" );
  mean_graph->GetYaxis()->SetTitle( "x [GeV]" );
  mean_graph->GetXaxis()->SetRangeUser( 0, 5 );
  mean_graph->SetMarkerStyle( 21 );
  mean_graph->SetMarkerSize( 1 );
  mean_graph->SetMarkerColor( 1 );
  add_pad_title( current_pad, "Double Gaussian Centre - x", false );
  add_atlas_decorations( current_pad, true, false );


  current_pad = (TPad *) graph_canvas->cd( 4 ); 
  sigma1_graph->Draw( "ALP" );
  sigma1_graph->SetStats( kFALSE );
  sigma1_graph->GetYaxis()->SetRangeUser( 0, 10 );
  sigma1_graph->GetXaxis()->SetTitle( "qtb fit" );
  sigma1_graph->GetYaxis()->SetTitle( "#sigma_{1} [GeV]" );
  sigma1_graph->GetYaxis()->SetMaxDigits( 3 );
  sigma1_graph->GetXaxis()->SetRangeUser( 0, 5 );
  sigma1_graph->SetMarkerStyle( 21 );
  sigma1_graph->SetMarkerSize( 1 );
  sigma1_graph->SetMarkerColor( 1 );
  add_pad_title( current_pad, "Double Gaussian Width 1 - #sigma_{1}", false );
  add_atlas_decorations( current_pad, true, false );


  current_pad = (TPad *) graph_canvas->cd( 5 ); 
  c2_graph->Draw( "ALP" );
  c2_graph->SetStats( kFALSE );
  c2_graph->GetYaxis()->SetRangeUser( 0, 4000 );
  c2_graph->GetYaxis()->SetMaxDigits( 3 );
  c2_graph->SetMarkerStyle( 21 );
  c2_graph->SetMarkerSize( 1 );
  c2_graph->SetMarkerColor( 1 );
  c2_graph->GetXaxis()->SetRangeUser( 0, 5 );
  c2_graph->GetXaxis()->SetTitle( "q_{T}^{B} fit" );
  c2_graph->GetYaxis()->SetTitle( "C2" );
  add_pad_title( current_pad, "Double Gaussian Normalisation 2 - C2", true );
  add_atlas_decorations( current_pad, true, false );

  gStyle->SetOptFit( 0 );
  current_pad = (TPad *) graph_canvas->cd( 6 ); 
  sigma2_graph->Draw( "ALP" );
  sigma2_graph->Fit( sigma_2_fit, "MQ" );
  sigma2_graph->GetYaxis()->SetRangeUser( 0, 6 );
  sigma2_graph->GetXaxis()->SetRangeUser( 0, 5 );
  sigma2_graph->GetXaxis()->SetTitle( "qtb fit" );
  sigma2_graph->GetYaxis()->SetTitle( "#sigma_{2} [GeV]" );
  sigma2_graph->SetMarkerStyle( 21 );
  sigma2_graph->SetMarkerSize( 1 );
  sigma2_graph->SetMarkerColor( 1 );
  add_pad_title( current_pad, "Double Gaussian Width 2 - #sigma_{2}", false );
  add_atlas_decorations( current_pad, true, false );
  TPaveText * statbox = new TPaveText( 0.56, 0.55, 0.80, 0.78, "NDC" );
  statbox->SetFillStyle( 0 );
  statbox->SetTextFont( 42 );
  statbox->SetTextSize( 0.02 );
  statbox->SetBorderSize( 1 );
  statbox->SetTextAlign( 32 );
  statbox->AddText( Form( "       m        - %.3f #pm %.3f", sigma_2_fit->GetParameter( 0 ), sigma_2_fit->GetParError( 0 ) ) );
  statbox->AddText( Form( "       c        - %.3f #pm %.3f", sigma_2_fit->GetParameter( 1 ), sigma_2_fit->GetParError( 1 ) ) );
  statbox->AddText( Form( "#chi^{2}/n.d.f. - %.3f /     %i", sigma_2_fit->GetChisquare(), sigma_2_fit->GetNDF() ) );
  TLegend * sigma2_legend = below_logo_legend();
  sigma2_legend->AddEntry( sigma2_graph, "#sigma_{2} value", "LP" );
  sigma2_legend->AddEntry( sigma_2_fit, "Linear Fit", "L" );
  //current_pad->Update();

  statbox->Draw();

    //TPaveStats * stats = (TPaveStats*) 
  //sigma2_graph->GetHistogram()->GetStats();
  //stats->SetX1NDC( 0.56 );
  //stats->SetX2NDC( 0.80 );
  //stats->SetY1NDC( 0.55 );
  //stats->SetY2NDC( 0.78 );
  //stats->Draw();

  graph_canvas->SaveAs( Form("%s_graphs.png", unique.c_str() ) );


}
