#include <anna.hxx>

void image_group( std::vector<TH1F*> group_systematics, TH1F * base, std::vector<std::string> group_uniques,
                  std::string store_unique ){
  

  TH1F * nominal = (TH1F *) base->Clone();
  std::vector<TH1F*> group_clone;
  for ( int sys_idx = 0; sys_idx < (int) group_systematics.size(); sys_idx++ ){
    group_clone.push_back( (TH1F*) (group_systematics.at(sys_idx))->Clone() );
  }

  TH1F * grouped = (TH1F *) base->Clone();
  grouped->Reset();

  combine_sys_group( group_clone, grouped, nominal );

  std::vector< float > nominal_style = { 1, 0, 0, 1.0, 1, 1, 1.0, 3.0, 0, 0, 0};
  std::vector< float > error_style =  { 1, 0, 0, 1.0, 1, 1, 0.7, 1.0, 0, 0, 0};

  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( "/home/atlas/dhagan/libs/configs/selection_bounds.txt" );

  bound qta_bound = selections->get_bound( "qTA" );

  TCanvas * overlay_canv = new TCanvas( "C", "C", 100, 100, 2000, 1000 );
  overlay_canv->Divide( 2 , 1 );


  TPad * current_pad = (TPad *) overlay_canv->cd( 1 );
  nominal->Draw( "HIST" );
  nominal->GetYaxis()->SetRangeUser( 0, 1.4*nominal->GetMaximum() );
  hist_prep_axes( nominal ); 
  //nominal->GetYaxis()->SetRangeUser( 0, 1.4*nominal->GetMaximum() );
  style_hist( nominal, nominal_style );
  add_pad_title( current_pad, Form( "Systematics group - %s", store_unique.c_str() ), true );
  set_axis_labels( nominal, qta_bound.get_x_str(), Form( "Yield/%s", qta_bound.get_y_str().c_str() ) );
  add_atlas_decorations( current_pad, true, false );
 	TLegend * ovly_legend = new TLegend( 0.6, 0.5, 0.88, 0.78 );
  ovly_legend->SetBorderSize( 0 );
  ovly_legend->SetFillColor( 0 );
  ovly_legend->SetFillStyle( 0 );
  ovly_legend->SetTextFont( 42 );
  ovly_legend->SetTextSize( 0.025 );
  ovly_legend->AddEntry( nominal, "Nominal" );
  for ( int sys_idx = 0; sys_idx < (int) group_systematics.size(); sys_idx++ ){
    if ( group_uniques.at( sys_idx ).find("afDPDY") != std::string::npos ){ continue; }
    group_clone.at( sys_idx )->Draw( "HIST SAME" );
    style_hist( group_clone.at( sys_idx ), error_style );
    group_clone.at( sys_idx )->SetLineColor( sys_idx + 2 );
    ovly_legend->AddEntry( group_clone.at( sys_idx ), group_uniques.at( sys_idx ).c_str() );
  }
  ovly_legend->Draw( "SAME" );

  current_pad = (TPad *) overlay_canv->cd( 2 );
  grouped->Draw( "HIST E1" );
  grouped->GetYaxis()->SetRangeUser( 0, 1.4*grouped->GetMaximum() );
  hist_prep_axes( grouped ); 
  //grouped->GetYaxis()->SetRangeUser( 0, 1.4*grouped->GetMaximum() );
  style_hist( grouped, error_style );
  add_pad_title( current_pad, Form( "Combined systematics - %s", store_unique.c_str() ), true );
  set_axis_labels( grouped, qta_bound.get_x_str(), Form( "Yield/%s", qta_bound.get_y_str().c_str() ) );
  add_atlas_decorations( current_pad, true, false );
  TLegend * g_legend = create_atlas_legend();
  g_legend->AddEntry( grouped, "error combined" );
  g_legend->Draw( "SAME" );

  overlay_canv->SaveAs( Form( "./gimg/%s.png", store_unique.c_str() ) );

  delete overlay_canv;

}
