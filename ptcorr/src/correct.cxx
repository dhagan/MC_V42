#include <correct.hxx>
#include <TF1.h>
#include <math.h>
#include <TMath.h>
#include <TFitResult.h>

#include <sys/stat.h>
inline bool file_exists(const std::string& name) {
  struct stat buffer;   
  return ( stat( name.c_str(), &buffer ) == 0 ); 
}

void prepare( YAML::Node & input, const basic_fileset & fileset, variable_set & variables, bool reprocess ){
  
  prep_style();

  bool process_ntuple = !file_exists( "./snapshot.root" ) || reprocess ;
  
  if ( !process_ntuple ){ return; }
 
  TTree * tree = fileset.sign_tree;
  bound photon_pt_bound = variables.bound_manager->get_bound( "PhotonPt" );
  bound dimuon_pt_bound = variables.bound_manager->get_bound( "DiMuonPt" );

  TH1F * dimuon_pt = dimuon_pt_bound.get_hist( "dimuon_pt" );
  TH1F * photon_pt = photon_pt_bound.get_hist( "photon_pt" );
  std::string cut_weight = input[ "cut_expression" ].as<std::string>();

  tree->Draw( "DiMuonPt>>dimuon_pt", cut_weight.c_str(), "e goff" );
  tree->Draw( "PhotonPt>>photon_pt", cut_weight.c_str(), "e goff" );

  TFile * output_store = new TFile( "./store.root", "RECREATE" );
  output_store->cd();
  dimuon_pt->Write();
  photon_pt->Write();
  output_store->Close();
  delete dimuon_pt;
  delete photon_pt;

} 

void parameterise( YAML::Node & input, TF1 * function, const std::string & function_name ){
  if ( !input[function_name + "_value"] ){ return; }
  std::vector< double > value = input[ function_name + "_value" ].as<std::vector<double>>(); 
  std::vector< double > upper = input[ function_name + "_upper" ].as<std::vector<double>>();
  std::vector< double > lower = input[ function_name + "_lower" ].as<std::vector<double>>();
  for ( int param = 0; param < function->GetNpar(); param++ ){
    function->SetParameter( param, value.at( param ) );
    function->SetParLimits( param, lower.at( param ), upper.at( param ) );
  }
}
/*

b should be around 3-4-5 GeV, n should be about 5-6-7, for jpsi distribution;
C*(1+pT/b)^(-n)

Correction to that:
(1+alpha*log(pT/14GeV))
*/
 
double jpsi_form( double * x, double * p){

  double c = p[0];
  double b_inv = 1.0/p[1];
  double n = p[2];


  return c*std::pow( ( 1 + x[0]*b_inv ), -1.0 * n );

}

double jpsi_form_corr( double * x, double * p){

  double initial = jpsi_form( x, p );
  double alpha = p[3];
  return initial*(1+alpha*log10( x[0]/14.0 ) );

}




void correct( YAML::Node & input, variable_set & variables ){

  prep_style();
  if ( variables.extra_bounds.empty() ){ std::cout << "ne" << std::endl; }

  TFile * store = new TFile( "./store.root", "READ" );
  store->cd();
  TH1F * dimuon_pt = static_cast<TH1F *>( store->Get( "dimuon_pt" ) );

  dimuon_pt->Scale( 1.0/dimuon_pt->Integral() );

  
  TF1 * jpsi_form_fn = new TF1( "jpsi_form_fn", jpsi_form_corr, 5, 30, 4 );
  jpsi_form_fn->SetParNames( "c", "b", "n", "alpha" );
  parameterise( input, jpsi_form_fn, "jpsi_form_fn" );

  TF1 * jpsi_form_corr_fn = new TF1( "jpsi_form_corr_fn", jpsi_form_corr, 5, 30, 4 );
  jpsi_form_corr_fn->SetParNames( "c", "b", "n", "alpha" );
  TF1 * jpsi_form_corr_fn_other = new TF1( "jpsi_form_corr_fn", jpsi_form_corr, 5, 30, 4 );
  jpsi_form_corr_fn->SetParNames( "c", "b", "n", "alpha" );

  //parameterise( input, jpsi_form_fn, "jpsi_form__fn" );


  TCanvas * canvas = new TCanvas( "cnv", "", 200, 200, 2000, 1000 );
  canvas->Divide( 2, 1 );
  gStyle->SetOptStat( "ieoumrn" );
  gStyle->SetOptFit( 1 );

  TH1F * fit_1 = static_cast<TH1F *>( dimuon_pt->Clone() );
  TPad * active_pad = static_cast<TPad *>( canvas->cd( 1 ) );
  fit_1->Draw( "HIST E1" );
  fit_1->SetName( "DiMuonPt" );
  TFitResult jpsi_form_result = *fit_1->Fit( jpsi_form_fn, "MS", "", 8, 25 );
  //dag_params_result.GetCorrelationMatrix().Print();
  jpsi_form_fn->Draw( "SAME" );
  hist_prep_axes( fit_1 );
  add_atlas_decorations( active_pad );
  set_axis_labels( fit_1, "P_{T,J/#psi}", "Entries" );
  TLegend * jpsi_form_legend = below_logo_legend();
  jpsi_form_legend->AddEntry( fit_1, "DiMuonPt" );
  jpsi_form_legend->AddEntry( jpsi_form_fn, "fit" );
  jpsi_form_legend->Draw("SAME");
  TPaveStats * jpsi_form_stats = make_stats( fit_1 );
  jpsi_form_stats->Draw( "SAME" );

  double jpsi_c = jpsi_form_fn->GetParameter( 0 );
  double jpsi_b = jpsi_form_fn->GetParameter( 1 );
  double jpsi_n = jpsi_form_fn->GetParameter( 2 );
  //double jpsi_alpha = jpsi_form_fn->GetParameter( 3 );
  double jpsi_alpha = input["corr_alpha"].as<double>();

  TF1 * jpsi_form_fn_copy = static_cast<TF1 *>( jpsi_form_fn->Clone() );
  jpsi_form_corr_fn->SetParameter( 0, jpsi_c );
  jpsi_form_corr_fn->SetParameter( 1, jpsi_b );
  jpsi_form_corr_fn->SetParameter( 2, jpsi_n );
  jpsi_form_corr_fn->SetParameter( 3, jpsi_alpha );
  jpsi_form_corr_fn->SetLineColorAlpha( kBlue+1, 0.4 );
  jpsi_form_corr_fn->SetLineWidth( 1.0 );

  jpsi_form_corr_fn_other->SetParameter( 0, jpsi_c );
  jpsi_form_corr_fn_other->SetParameter( 1, jpsi_b );
  jpsi_form_corr_fn_other->SetParameter( 2, jpsi_n );
  jpsi_form_corr_fn_other->SetParameter( 3, jpsi_form_fn->GetParameter( 3 )*(jpsi_form_fn->GetParameter( 3 )/jpsi_alpha ) );
  jpsi_form_corr_fn_other->SetLineColorAlpha( kBlue+1, 0.4 );
  jpsi_form_corr_fn_other->SetLineWidth( 1.0 );



  TH1F * fit_2 = static_cast<TH1F *>( dimuon_pt->Clone() );
  active_pad = static_cast<TPad *>( canvas->cd( 2 ) );
  active_pad->SetLogy();
  fit_2->Draw( "HIST E1" );
  jpsi_form_fn_copy->Draw( "SAME" );
  jpsi_form_corr_fn->Draw( "SAME" );
  jpsi_form_corr_fn_other->Draw( "SAME" );
  //jpsi_form_corr_lower_fn->Draw( "SAME" );
  hist_prep_axes( fit_2 );
  add_atlas_decorations( active_pad );
  set_axis_labels( fit_2, "P_{T,J/#psi}", "Entries" );
  TLegend * jpsi_form_corr_legend = below_logo_legend();
  jpsi_form_corr_legend->AddEntry( fit_2, "DiMuonPt" );
  jpsi_form_corr_legend->AddEntry( jpsi_form_fn_copy, "fit" );
  jpsi_form_corr_legend->Draw("SAME");
  TPaveStats * jpsi_form_corr_stats = make_stats( fit_2 );
  jpsi_form_corr_stats->Draw( "SAME" );

  canvas->SaveAs( "./plots/corr.png" );

}
