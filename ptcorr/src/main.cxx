#include <main.hxx>
#include <correct.hxx>


int main(int argc, char ** argv){

  ROOT::EnableThreadSafety();
  //ROOT::EnableImplicitMT( 6 );
  //laurel::initialise(); 

  int option_index{0},option{0};
  static struct option long_options[] = {
      { "input",          required_argument,    0,      'i'},
      { 0,                0,                    0,      0}
  };

  const char * config_path = "";

  do {
    option = getopt_long( argc, argv, "i:", long_options, &option_index );
    switch ( option ){
      case 'i':
        config_path = optarg; 
        option = -1;
        break;
    }
  } while ( option != -1 );


  if ( strlen( config_path ) == 0 ){ throw std::invalid_argument( "No config file provided" ); }

  YAML::Node config = YAML::LoadFile( config_path );
  std::vector< std::string > run_nodes = config["general"]["runs"].as<std::vector<std::string>>();
  bool multithreaded = config["general"]["multithreaded"].as<bool>();
  if ( multithreaded ){ std::cout << "Multithreaded not implemented" << std::endl; }

  for ( std::string & input : run_nodes ){

    std::cout << "Run: input" << std::endl;

    YAML::Node run = config[input];
    const std::string & unique = run["unique"].as<std::string>();
    std::map< std::string, bool > modes  = run["modes"].as< std::map< std::string, bool > >();
    
    std::string range = "";
    
    std::string avar = "qtA", svar = "BDT";
    if ( run[ "avar" ] ){ avar = run["avar"].as< std::string >(); }
    if ( run[ "svar" ] ){ svar = run["svar"].as< std::string >(); }
    if ( run[ "ranges" ] ){ range = run["ranges"].as< std::string >(); }
    
    variable_set variables = variable_set( "", range, avar, svar );
    
    if ( modes["prepare"] ){
      std::string filepath = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/";
      std::string unique = run["unique"].as<std::string>();
      filepath += unique + "/";
      basic_fileset fileset;
      fileset.load_subtraction_fileset( filepath, unique, true );
      bool reprocess = false;
      if ( run[ "reprocess" ] ){ reprocess = run["reprocess"].as< bool >(); }
      prepare( run, fileset, variables, reprocess );
    }
    if ( modes["correct"] ){
      correct( run, variables );
    }
  }
}
