unique="base_a"
fastyaml="$(pwd)/fast.yaml"

mkdir -p ${OUT_PATH}/ptcorr/${unique}
mkdir -p ${OUT_PATH}/ptcorr/${unique}/plots
pushd ${OUT_PATH}/ptcorr/${unique} >> /dev/null
ptcorr -i ${fastyaml}
popd >> /dev/null
