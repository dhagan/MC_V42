#include <main.hxx>
#include <yaml-cpp/yaml.h>

void prepare( YAML::Node & input, const basic_fileset & fileset, variable_set & variables, bool reprocess=false );
void correct( YAML::Node & input, variable_set & variables );

