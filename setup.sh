help_func()
{
   echo ""
   echo "Usage: $0 -i"
   echo -i "\tInitial execution, ensures reconfigure"
   return # Exit script after printing help
}

initial_flag=
OPTIND=1
while getopts "i" opt
do
   case "$opt" in
      i ) initial_flag=1 ;;
      ? ) help_func ;; # Print helpFunction in case parameter is non-existent
   esac
done


## Fresh shell setup
if [[ -z ${jpsigamma_tmd_setup} ]]; then


	## Title
	cat ./assets/banner
	##cat ./assets/banner | while read -r line; do echo -e "$line"; done
	echo ""

	## Initialise the submodule
	git submodule update --init --recursive
	
	## Check root and cmake are here
	if ! command -v root &> /dev/null
	then
	    echo "No root install, exiting."
	    return
	fi
	echo -e "$(root --version)"
	
	if ! command -v cmake &> /dev/null
	then
	    echo "No cmake install, exiting."
	    return
	fi
	echo -e $(cmake --version)
	echo ""

fi

mkdir -p build

pushd ./build >> /dev/null
if [ ! -z "$initial_flag" ]; then
	cmake -S .. -B . -G Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS=1
fi
ninja install -j10
ninja setup_environment -j10
source environment.sh
popd >> /dev/null
ln -sf ./build/compile_commands.json

## analysis variables
analysis_install_path=$(pwd)
analysis_storage_path="/run/media/crucible/Expansion/analysis/"
export IN_PATH="$analysis_storage_path/in"
export OUT_PATH="$analysis_storage_path/out/run"
export LOG_PATH="$analysis_storage_path/out/log"
export ANA_IP="$analysis_install_path"
export DEFAULT_STYLE_JSON="$analysis_install_path/lib/analysis_utils/share/default_style"
export DEFAULT_FIT_JSON="$analysis_install_path/lib/analysis_utils/share/default_fits"
export DEFAULT_SCRIPT_JSON="$analysis_install_path/lib/analysis_utils/share/default_scripts"
export exec_path="${analysis_install_path}/build/bin"
export LIB_PATH="${analysis_install_path}/lib/analysis_utils"
export SF_PATH="${IN_PATH}/sf_map/"
export DATAPATH="$analysis_install_path/build/data/"
source "${ANA_IP}/build/TRExFitter/xroofit/setup.sh"

export jpsigamma_tmd_setup=1
echo ""
