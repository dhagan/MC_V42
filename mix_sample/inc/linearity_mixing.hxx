#ifndef lin_hxx
#define lin_hxx

#include <common.hxx>

void linearity_mixing(std::unique_ptr<TTree> & sign_tree, std::unique_ptr<TTree> & bckg_tree);

#endif
