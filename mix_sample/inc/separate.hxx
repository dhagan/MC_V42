#ifndef separate_hxx
#define separate_hxx

#include <TTree.h>
#include <TFile.h>
#include <common.hxx>

void sep(std::unique_ptr<TTree> & sign_tree, std::unique_ptr<TTree> & bckg_tree, std::string unique_name, std::string vars);

#endif
