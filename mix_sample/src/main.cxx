#include <main.hxx>
#include <linearity_mixing.hxx>
#include <separate.hxx>

int help(){
  std::cout << "-d input                  --    path and tree name of data file" << std::endl;
  std::cout << "-s input                  --    path and tree name of signal MC file" << std::endl;
  std::cout << "-b input                  --    path and tree name of pp background MC file" << std::endl;
  std::cout << "-a input                  --    path and tree name of bb background MC file" << std::endl;
  std::cout << "-m mode                   --    safe mode, creates three samples of bckg and sign for training, validation, and evaluation" << std::endl;
  std::cout << "-n input name             --    unique name for this mixing run" << std::endl;
  std::cout << "-h help                   --    this readout" << std::endl;
  return 0;
}

int main(int argc, char * argv[]){
  
  if (argc < 2){
    std::cout << "insufficient parameters" << std::endl;
    return help();
  }

  static struct option long_options[] = {
      { "help",           no_argument,          0,      'h'},
      { "data",           required_argument,    0,      'd'},
      { "sign",           required_argument,    0,      's'},
      { "bckg",           required_argument,    0,      'b'},
      { "bbbg",           required_argument,    0,      'a'},
      { "mode",           required_argument,    0,      'm'},
      { "name",           required_argument,    0,      'n'},
      { "variables",      required_argument,    0,      'v'},
      { 0,                0,                    0,      0}
  };

  int option_index{0},option{0};
  std::string data_file, sign_file, bckg_file, unique, mode, vars{"all"};

  do {
    option = getopt_long( argc, argv, "h:d:s:b:a:u:m:v:", long_options, &option_index);
    switch (option){
      case 'h':
        return help();
      case 'd':
        data_file = std::string(optarg);
        break;
      case 's':
        sign_file = std::string(optarg);
        break;
      case 'b':
        bckg_file = std::string(optarg);
        break;
      case 'a':
        break;
      case 'm':
        mode = std::string(optarg);
        break;
      case 'u':
        unique = std::string(optarg);
        break;
      case 'v':
        vars = std::string(optarg);
        break;

    }
  } while ( option != -1 );

  if ( mode.empty() ){ std::cout << "no execution mode given" << std::endl; return help(); }
  if ( unique.empty()  ){ std::cout << "no unique given" << std::endl;    return help(); }

  if ( mode.find("sep") != std::string::npos ){
    if ( sign_file.empty() || bckg_file.empty() ){ std::cout << "correct file paths not provided for this mode" << std::endl; return help(); }
    std::vector<std::string> sign_strs, bckg_strs; 

    split_strings(sign_strs,sign_file,":"), split_strings(bckg_strs,bckg_file,":");
    
    std::unique_ptr<TFile> sign_file = std::unique_ptr<TFile>{ TFile::Open(sign_strs.at(0).c_str(),"READ") };
    std::unique_ptr<TTree> sign_tree = std::unique_ptr<TTree>{ (TTree*) sign_file->Get(sign_strs.at(1).c_str()) };
    std::unique_ptr<TFile> bckg_file = std::unique_ptr<TFile>{ TFile::Open(bckg_strs.at(0).c_str(),"READ") };
    std::unique_ptr<TTree> bckg_tree = std::unique_ptr<TTree>{ (TTree*) bckg_file->Get(bckg_strs.at(1).c_str()) };
    std::cout << "Signal file and tree:" << std::endl;
    std::cout << sign_strs.at(0) << std::endl; 
    std::cout << sign_strs.at(1) << std::endl;
    //TFile * sign_file = new TFile(sign_strs.at(0).c_str(),"READ");
    //TTree * sign_tree = (TTree*) sign_file->Get(sign_strs.at(1).c_str()); 
    std::cout << "Background file and tree:" << std::endl;
    std::cout << bckg_strs.at(0) << std::endl; 
    std::cout << bckg_strs.at(1) << std::endl;
    //TFile * bckg_file = new TFile(bckg_strs.at(0).c_str(),"READ");
    //TTree * bckg_tree = (TTree*) bckg_file->Get(bckg_strs.at(1).c_str()); 

    sep( sign_tree, bckg_tree, unique, vars );

  }

  if ( mode.find("lin") != std::string::npos ){
    std::vector<std::string> sign_strs, bckg_strs; 
    split_strings(sign_strs,sign_file,":"), split_strings(bckg_strs,bckg_file,":");
    std::cout << "Signal file and tree:" << std::endl;
    std::cout << sign_strs.at(0) << std::endl; 
    std::cout << sign_strs.at(1) << std::endl;
    std::cout << "Background file and tree:" << std::endl;
    std::cout << bckg_strs.at(0) << std::endl; 
    std::cout << bckg_strs.at(1) << std::endl;
    std::unique_ptr<TFile> sign_file = std::unique_ptr<TFile>{ TFile::Open(sign_strs.at(0).c_str(),"READ") };
    std::unique_ptr<TTree> sign_tree = std::unique_ptr<TTree>{ (TTree*) sign_file->Get(sign_strs.at(1).c_str()) };
    std::unique_ptr<TFile> bckg_file = std::unique_ptr<TFile>{ TFile::Open(bckg_strs.at(0).c_str(),"READ") };
    std::unique_ptr<TTree> bckg_tree = std::unique_ptr<TTree>{ (TTree*) bckg_file->Get(bckg_strs.at(1).c_str()) };
    linearity_mixing( sign_tree, bckg_tree );
  }

  if ( mode.find("rval") != std::string::npos ){
    std::vector<std::string> sign_strs, bckg_strs; 
    split_strings(sign_strs,sign_file,":"), split_strings(bckg_strs,bckg_file,":");
    std::cout << "Signal file and tree:" << std::endl;
    std::cout << sign_strs.at(0) << std::endl; 
    std::cout << sign_strs.at(1) << std::endl;
    std::cout << "Background file and tree:" << std::endl;
    std::cout << bckg_strs.at(0) << std::endl; 
    std::cout << bckg_strs.at(1) << std::endl;
    std::unique_ptr<TFile> sign_file = std::unique_ptr<TFile>{ TFile::Open(sign_strs.at(0).c_str(),"READ") };
    std::unique_ptr<TTree> sign_tree = std::unique_ptr<TTree>{ (TTree*) sign_file->Get(sign_strs.at(1).c_str()) };
    std::unique_ptr<TFile> bckg_file = std::unique_ptr<TFile>{ TFile::Open(bckg_strs.at(0).c_str(),"READ") };
    std::unique_ptr<TTree> bckg_tree = std::unique_ptr<TTree>{ (TTree*) bckg_file->Get(bckg_strs.at(1).c_str()) };
    linearity_mixing(sign_tree,bckg_tree);
  }


  return 0;
}
