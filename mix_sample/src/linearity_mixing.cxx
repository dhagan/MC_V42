#include <linearity_mixing.hxx>

void linearity_mixing(std::unique_ptr<TTree> & sign_tree, std::unique_ptr<TTree> & bckg_tree){
  


  float sign_AbsCosTheta,sign_AbsdPhi,sign_AbsPhi,sign_DPhi,sign_DY,sign_Phi,sign_costheta,sign_EventNumber,sign_Lambda,sign_qTSquared,sign_DiMuonMass,
        sign_DiMuonTau,sign_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_DiMuonPt,sign_PhotonPt,sign_AbsdY,sign_qxpsi,sign_qypsi,sign_qxgamma,sign_qygamma,
        sign_qxsum,sign_qysum,sign_qtA,sign_qtB, sign_qtL, sign_qtM;
  double sign_BDT;
  
  float bckg_AbsCosTheta,bckg_AbsdPhi,bckg_AbsPhi,bckg_DPhi,bckg_DY,bckg_Phi,bckg_costheta,bckg_EventNumber,bckg_Lambda,bckg_qTSquared,bckg_DiMuonMass,
        bckg_DiMuonTau,bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_DiMuonPt,bckg_PhotonPt,bckg_AbsdY,bckg_qxpsi,bckg_qypsi,bckg_qxgamma,bckg_qygamma,
        bckg_qxsum,bckg_qysum,bckg_qtA,bckg_qtB, bckg_qtL, bckg_qtM;
  double bckg_BDT;
  
  sign_EventNumber = 0; 
  bckg_EventNumber = 0; 
  sign_AbsdY = 0; 
  bckg_AbsdY = 0; 
  
  sign_EventNumber++; 
  bckg_EventNumber++; 
  sign_AbsdY++; 
  bckg_AbsdY++; 


  sign_tree->SetBranchAddress("BDT",                              &sign_BDT);
  sign_tree->SetBranchAddress("AbsCosTheta",                      &sign_AbsCosTheta);
	sign_tree->SetBranchAddress("AbsdPhi",                          &sign_AbsdPhi);
	sign_tree->SetBranchAddress("AbsPhi",                           &sign_AbsPhi);
	sign_tree->SetBranchAddress("DPhi",                             &sign_DPhi);
	sign_tree->SetBranchAddress("DY",                               &sign_DY);
	sign_tree->SetBranchAddress("Phi",                              &sign_Phi);
	sign_tree->SetBranchAddress("costheta",                         &sign_costheta);
	sign_tree->SetBranchAddress("Lambda",                           &sign_Lambda);
	sign_tree->SetBranchAddress("qTSquared",                        &sign_qTSquared); 
  sign_tree->SetBranchAddress("DiMuonMass",                       &sign_DiMuonMass);
	sign_tree->SetBranchAddress("DiMuonTau",                        &sign_DiMuonTau);
	sign_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",  &sign_Trigger_HLT_2mu4_bJpsimumu_noL2);
	sign_tree->SetBranchAddress("DiMuonPt",                         &sign_DiMuonPt);
	sign_tree->SetBranchAddress("PhotonPt",                         &sign_PhotonPt);
	sign_tree->SetBranchAddress("qxpsi",                            &sign_qxpsi);
	sign_tree->SetBranchAddress("qypsi",                            &sign_qypsi);
	sign_tree->SetBranchAddress("qxgamma",                          &sign_qxgamma);
	sign_tree->SetBranchAddress("qygamma",                          &sign_qygamma);
	sign_tree->SetBranchAddress("qxsum",                            &sign_qxsum);
	sign_tree->SetBranchAddress("qysum",                            &sign_qysum);
	sign_tree->SetBranchAddress("qtA",                              &sign_qtA);
	sign_tree->SetBranchAddress("qtB",                              &sign_qtB);
  sign_tree->SetBranchAddress("qtL",                              &sign_qtL);
	sign_tree->SetBranchAddress("qtM",                              &sign_qtM);

  bckg_tree->SetBranchAddress("BDT",                              &bckg_BDT);
  bckg_tree->SetBranchAddress("AbsCosTheta",                      &bckg_AbsCosTheta);
	bckg_tree->SetBranchAddress("AbsdPhi",                          &bckg_AbsdPhi);
	bckg_tree->SetBranchAddress("AbsPhi",                           &bckg_AbsPhi);
	bckg_tree->SetBranchAddress("DPhi",                             &bckg_DPhi);
	bckg_tree->SetBranchAddress("DY",                               &bckg_DY);
	bckg_tree->SetBranchAddress("Phi",                              &bckg_Phi);
	bckg_tree->SetBranchAddress("costheta",                         &bckg_costheta);
	bckg_tree->SetBranchAddress("Lambda",                           &bckg_Lambda);
	bckg_tree->SetBranchAddress("qTSquared",                        &bckg_qTSquared);
	bckg_tree->SetBranchAddress("DiMuonMass",                       &bckg_DiMuonMass);
	bckg_tree->SetBranchAddress("DiMuonTau",                        &bckg_DiMuonTau);
	bckg_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",  &bckg_Trigger_HLT_2mu4_bJpsimumu_noL2);
	bckg_tree->SetBranchAddress("DiMuonPt",                         &bckg_DiMuonPt);
	bckg_tree->SetBranchAddress("PhotonPt",                         &bckg_PhotonPt);
	bckg_tree->SetBranchAddress("qxpsi",                            &bckg_qxpsi);
	bckg_tree->SetBranchAddress("qypsi",                            &bckg_qypsi);
	bckg_tree->SetBranchAddress("qxgamma",                          &bckg_qxgamma);
	bckg_tree->SetBranchAddress("qygamma",                          &bckg_qygamma);
	bckg_tree->SetBranchAddress("qxsum",                            &bckg_qxsum);
	bckg_tree->SetBranchAddress("qysum",                            &bckg_qysum);
	bckg_tree->SetBranchAddress("qtA",                              &bckg_qtA);
	bckg_tree->SetBranchAddress("qtB",                              &bckg_qtB);
  bckg_tree->SetBranchAddress("qtL",                              &bckg_qtL);
	bckg_tree->SetBranchAddress("qtM",                              &bckg_qtM);

  std::vector<int> fractions = {5,10,25,50,60,75};

  for ( int & frac : fractions ){
    
    // This whole process is bound by background file size
    // Take the full set of background events
        
    double dbl_frac = frac/100.0;
    int bckg_evts = bckg_tree->GetEntries();
    int combo_files = 50/frac;
    if (frac > 50) combo_files = 1;

    //int sign_evts = (int) (bckg_evts*dbl_frac)/(1.0-dbl_frac);
    int sign_evts = bckg_evts*(dbl_frac/0.5);
    std::cout << " " << std::endl;
    std::cout << "background events     --  " << bckg_evts << std::endl;
    std::cout << "signal events         --  " << sign_evts << std::endl;
    std::cout << "fraction              --  " << dbl_frac << std::endl;
    //std::cout << "input fraction        --  " << dbl_frac << std::endl;
    //std::cout << "output fraction       --  " << ((double)sign_evts/((double)bckg_evts+sign_evts)) << std::endl;
    std::cout << "files                 -- " << combo_files << std::endl;

    for ( int file = 1; file <= combo_files; file++ ){  
        
      std::cout << ""  << std::endl;
      std::cout << "file           -- " << file << std::endl;

      TFile * lin_data_file = new TFile( Form( "./lin_data/lin_data_s%i_f%i.root", frac, file ), "RECREATE" ); 
      TTree * lin_data_tree = new TTree{ "tree", "tree"};
      
      TFile * lin_sign_file = new TFile( Form( "./lin_sign/lin_sign_s%i_f%i.root", frac, file ), "RECREATE" ); 
      TTree * lin_sign_tree = new TTree{ "tree", "tree" };

      TFile * lin_bckg_file = new TFile( Form( "./lin_bckg/lin_bckg_s%i_f%i.root", frac, file ),"RECREATE" ); 
      TTree * lin_bckg_tree = new TTree{ "tree", "tree" };

      
      float lin_data_AbsCosTheta,lin_data_AbsdPhi,lin_data_AbsPhi,lin_data_DPhi,lin_data_DY,lin_data_Phi,lin_data_costheta,lin_data_EventNumber,lin_data_Lambda,lin_data_qTSquared,lin_data_DiMuonMass,
            lin_data_DiMuonTau,lin_data_Trigger_HLT_2mu4_bJpsimumu_noL2,lin_data_DiMuonPt,lin_data_PhotonPt,lin_data_AbsdY,lin_data_qxpsi,lin_data_qypsi,lin_data_qxgamma,lin_data_qygamma,
            lin_data_qxsum,lin_data_qysum,lin_data_qtA,lin_data_qtB, lin_data_qtL, lin_data_qtM, lin_data_BDT;

      lin_data_EventNumber = 0; 
      lin_data_AbsdY = 0; 
      lin_data_EventNumber++;
      lin_data_AbsdY++; 


        
      lin_data_tree->Branch("BDT",                                      &lin_data_BDT,                                "BDT/F");
      lin_data_tree->Branch("AbsCosTheta",                              &lin_data_AbsCosTheta,                        "AbsCosTheta/F");
	    lin_data_tree->Branch("AbsdPhi",                                  &lin_data_AbsdPhi,                            "AbsdPhi/F");
	    lin_data_tree->Branch("AbsPhi",                                   &lin_data_AbsPhi,                             "AbsPhi/F");
	    lin_data_tree->Branch("DPhi",                                     &lin_data_DPhi,                               "DPhi/F");
	    lin_data_tree->Branch("DY",                                       &lin_data_DY,                                 "DY/F");
	    lin_data_tree->Branch("Phi",                                      &lin_data_Phi,                                "phi/F");
	    lin_data_tree->Branch("costheta",                                 &lin_data_costheta,                           "CosTheta/F");
	    lin_data_tree->Branch("Lambda",                                   &lin_data_Lambda,                             "Lambda/F");
	    lin_data_tree->Branch("qTSquared",                                &lin_data_qTSquared,                          "qTSquared/F");
	    lin_data_tree->Branch("DiMuonMass",                               &lin_data_DiMuonMass,                         "DiMuonMass/F");
	    lin_data_tree->Branch("DiMuonTau",                                &lin_data_DiMuonTau,                          "DiMuonTau/F");
	    lin_data_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &lin_data_Trigger_HLT_2mu4_bJpsimumu_noL2,    "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	    lin_data_tree->Branch("DiMuonPt",                                 &lin_data_DiMuonPt,                           "DiMuonPt/F");
	    lin_data_tree->Branch("PhotonPt",                                 &lin_data_PhotonPt,                           "PhotonPt/F");
	    lin_data_tree->Branch("qxpsi",                                    &lin_data_qxpsi,                              "qxpsi");
	    lin_data_tree->Branch("qypsi",                                    &lin_data_qypsi,                              "qypsi");
	    lin_data_tree->Branch("qxgamma",                                  &lin_data_qxgamma,                            "qxgamma");
	    lin_data_tree->Branch("qygamma",                                  &lin_data_qygamma,                            "qygamma");
	    lin_data_tree->Branch("qxsum",                                    &lin_data_qxsum,                              "qxsum");
	    lin_data_tree->Branch("qysum",                                    &lin_data_qysum,                              "qysum");
	    lin_data_tree->Branch("qtA",                                      &lin_data_qtA,                                "qtA");
	    lin_data_tree->Branch("qtB",                                      &lin_data_qtB,                                "qtB");
      lin_data_tree->Branch("qtL",                                      &lin_data_qtL,                                "qtL");
	    lin_data_tree->Branch("qtM",                                      &lin_data_qtM,                                "qtM");

      lin_sign_tree->Branch("BDT",                                      &lin_data_BDT,                                "BDT/F");
      lin_sign_tree->Branch("AbsCosTheta",                              &lin_data_AbsCosTheta,                        "AbsCosTheta/F");
	    lin_sign_tree->Branch("AbsdPhi",                                  &lin_data_AbsdPhi,                            "AbsdPhi/F");
	    lin_sign_tree->Branch("AbsPhi",                                   &lin_data_AbsPhi,                             "AbsPhi/F");
	    lin_sign_tree->Branch("DPhi",                                     &lin_data_DPhi,                               "DPhi/F");
	    lin_sign_tree->Branch("DY",                                       &lin_data_DY,                                 "DY/F");
	    lin_sign_tree->Branch("Phi",                                      &lin_data_Phi,                                "phi/F");
	    lin_sign_tree->Branch("costheta",                                 &lin_data_costheta,                           "CosTheta/F");
	    lin_sign_tree->Branch("Lambda",                                   &lin_data_Lambda,                             "Lambda/F");
	    lin_sign_tree->Branch("qTSquared",                                &lin_data_qTSquared,                          "qTSquared/F");
	    lin_sign_tree->Branch("DiMuonMass",                               &lin_data_DiMuonMass,                         "DiMuonMass/F");
	    lin_sign_tree->Branch("DiMuonTau",                                &lin_data_DiMuonTau,                          "DiMuonTau/F");
	    lin_sign_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &lin_data_Trigger_HLT_2mu4_bJpsimumu_noL2,    "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	    lin_sign_tree->Branch("DiMuonPt",                                 &lin_data_DiMuonPt,                           "DiMuonPt/F");
	    lin_sign_tree->Branch("PhotonPt",                                 &lin_data_PhotonPt,                           "PhotonPt/F");
	    lin_sign_tree->Branch("qxpsi",                                    &lin_data_qxpsi,                              "qxpsi");
	    lin_sign_tree->Branch("qypsi",                                    &lin_data_qypsi,                              "qypsi");
	    lin_sign_tree->Branch("qxgamma",                                  &lin_data_qxgamma,                            "qxgamma");
	    lin_sign_tree->Branch("qygamma",                                  &lin_data_qygamma,                            "qygamma");
	    lin_sign_tree->Branch("qxsum",                                    &lin_data_qxsum,                              "qxsum");
	    lin_sign_tree->Branch("qysum",                                    &lin_data_qysum,                              "qysum");
	    lin_sign_tree->Branch("qtA",                                      &lin_data_qtA,                                "qtA");
	    lin_sign_tree->Branch("qtB",                                      &lin_data_qtB,                                "qtB");
      lin_sign_tree->Branch("qtL",                                      &lin_data_qtL,                                "qtL");
	    lin_sign_tree->Branch("qtM",                                      &lin_data_qtM,                                "qtM");

      lin_bckg_tree->Branch("BDT",                                      &lin_data_BDT,                                "BDT/F");
      lin_bckg_tree->Branch("AbsCosTheta",                              &lin_data_AbsCosTheta,                        "AbsCosTheta/F");
	    lin_bckg_tree->Branch("AbsdPhi",                                  &lin_data_AbsdPhi,                            "AbsdPhi/F");
	    lin_bckg_tree->Branch("AbsPhi",                                   &lin_data_AbsPhi,                             "AbsPhi/F");
	    lin_bckg_tree->Branch("DPhi",                                     &lin_data_DPhi,                               "DPhi/F");
	    lin_bckg_tree->Branch("DY",                                       &lin_data_DY,                                 "DY/F");
	    lin_bckg_tree->Branch("Phi",                                      &lin_data_Phi,                                "phi/F");
	    lin_bckg_tree->Branch("costheta",                                 &lin_data_costheta,                           "CosTheta/F");
	    lin_bckg_tree->Branch("Lambda",                                   &lin_data_Lambda,                             "Lambda/F");
	    lin_bckg_tree->Branch("qTSquared",                                &lin_data_qTSquared,                          "qTSquared/F");
	    lin_bckg_tree->Branch("DiMuonMass",                               &lin_data_DiMuonMass,                         "DiMuonMass/F");
	    lin_bckg_tree->Branch("DiMuonTau",                                &lin_data_DiMuonTau,                          "DiMuonTau/F");
	    lin_bckg_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &lin_data_Trigger_HLT_2mu4_bJpsimumu_noL2,    "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	    lin_bckg_tree->Branch("DiMuonPt",                                 &lin_data_DiMuonPt,                           "DiMuonPt/F");
	    lin_bckg_tree->Branch("PhotonPt",                                 &lin_data_PhotonPt,                           "PhotonPt/F");
	    lin_bckg_tree->Branch("qxpsi",                                    &lin_data_qxpsi,                              "qxpsi");
	    lin_bckg_tree->Branch("qypsi",                                    &lin_data_qypsi,                              "qypsi");
	    lin_bckg_tree->Branch("qxgamma",                                  &lin_data_qxgamma,                            "qxgamma");
	    lin_bckg_tree->Branch("qygamma",                                  &lin_data_qygamma,                            "qygamma");
	    lin_bckg_tree->Branch("qxsum",                                    &lin_data_qxsum,                              "qxsum");
	    lin_bckg_tree->Branch("qysum",                                    &lin_data_qysum,                              "qysum");
	    lin_bckg_tree->Branch("qtA",                                      &lin_data_qtA,                                "qtA");
	    lin_bckg_tree->Branch("qtB",                                      &lin_data_qtB,                                "qtB");
      lin_bckg_tree->Branch("qtL",                                      &lin_data_qtL,                                "qtL");
	    lin_bckg_tree->Branch("qtM",                                      &lin_data_qtM,                                "qtM");

      std::cout << "Tree created" << std::endl;
      
      for (int bckg_idx = 0; bckg_idx < bckg_tree->GetEntries(); bckg_idx++){

        bckg_tree->GetEntry(bckg_idx);
        lin_data_BDT                                      = bckg_BDT;
        lin_data_AbsCosTheta                              = bckg_AbsCosTheta;
        lin_data_AbsdPhi                                  = bckg_AbsdPhi;
        lin_data_AbsPhi                                   = bckg_AbsPhi;
        lin_data_DPhi                                     = bckg_DPhi;
        lin_data_DY                                       = bckg_DY;
        lin_data_Phi                                      = bckg_Phi;
        lin_data_costheta                                 = bckg_costheta;
        //lin_data_EventNumber                              = bckg_EventNumber;
        lin_data_Lambda                                   = bckg_Lambda;
        lin_data_qTSquared                                = bckg_qTSquared;
        lin_data_DiMuonMass                               = bckg_DiMuonMass;
        lin_data_DiMuonTau                                = bckg_DiMuonTau;
        lin_data_Trigger_HLT_2mu4_bJpsimumu_noL2          = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
        lin_data_DiMuonPt                                 = bckg_DiMuonPt;
        lin_data_PhotonPt                                 = bckg_PhotonPt;
        //lin_data_AbsdY                                    = bckg_AbsdY;
        lin_data_qxpsi                                    = bckg_qxpsi;
        lin_data_qypsi                                    = bckg_qypsi;
        lin_data_qxgamma                                  = bckg_qxgamma;
        lin_data_qygamma                                  = bckg_qygamma;
        lin_data_qxsum                                    = bckg_qxsum;
        lin_data_qysum                                    = bckg_qysum;
        lin_data_qtA                                      = bckg_qtA;
        lin_data_qtB                                      = bckg_qtB;
        lin_data_qtL                                      = bckg_qtM;
        lin_data_tree->Fill();
        lin_bckg_tree->Fill();

      }
      std::cout << "Filled background events" << std::endl;
  
      int start_evt   = sign_evts + (sign_evts * (file-1));
      int final_evt   = sign_evts + (sign_evts * (file));

      for (int sign_idx = start_evt; sign_idx < final_evt; sign_idx++){

        sign_tree->GetEntry(sign_idx);
        lin_data_BDT                                     = sign_BDT;
        lin_data_AbsCosTheta                             = sign_AbsCosTheta;
        lin_data_AbsdPhi                                 = sign_AbsdPhi;
        lin_data_AbsPhi                                  = sign_AbsPhi;
        lin_data_DPhi                                    = sign_DPhi;
        lin_data_DY                                      = sign_DY;
        lin_data_Phi                                     = sign_Phi;
        lin_data_costheta                                = sign_costheta;
        //lin_data_EventNumber                             = sign_EventNumber;
        lin_data_Lambda                                  = sign_Lambda;
        lin_data_qTSquared                               = sign_qTSquared;
        lin_data_DiMuonMass                              = sign_DiMuonMass;
        lin_data_DiMuonTau                               = sign_DiMuonTau;
        lin_data_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
        lin_data_DiMuonPt                                = sign_DiMuonPt;
        lin_data_PhotonPt                                = sign_PhotonPt;
        //lin_data_AbsdY                                   = sign_AbsdY;
        lin_data_qxpsi                                   = sign_qxpsi;
        lin_data_qypsi                                   = sign_qypsi;
        lin_data_qxgamma                                 = sign_qxgamma;
        lin_data_qygamma                                 = sign_qygamma;
        lin_data_qxsum                                   = sign_qxsum;
        lin_data_qysum                                   = sign_qysum;
        lin_data_qtA                                     = sign_qtA;
        lin_data_qtB                                     = sign_qtB;
        lin_data_qtL                                     = sign_qtM;
        lin_data_tree->Fill();
        lin_sign_tree->Fill();

      }

      std::cout << "Filled signal events" << std::endl;

      lin_data_file->cd();
      lin_data_tree->Write();
      lin_data_file->Close();

      lin_sign_file->cd();
      lin_sign_tree->Write();
      lin_sign_file->Close();

      lin_bckg_file->cd();
      lin_bckg_tree->Write();
      lin_bckg_file->Close();

    }

  }

}
