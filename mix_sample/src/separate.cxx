#include <separate.hxx>

void sep(std::unique_ptr<TTree> & sign_tree, std::unique_ptr<TTree> & bckg_tree, std::string unique_name, std::string vars){
//void sep(TTree * sign_tree, TTree * bckg_tree, std::string unique_name, std::string vars){
//void sep(std::vector<std::string> sign_str, std::vector<std::string> bckg_str);
  
  std::cout << unique_name << std::endl;
  std::cout << vars << std::endl;

  TFile * sign_train_valid_file = new TFile("sign_train_valid.root","RECREATE");
  TTree * sign_train_valid_tree = new TTree("TreeD_sign_train_valid","sign_train_valid_tree");
  //std::unique_ptr<TTree> sign_train_valid_tree = std::unique_ptr<TTree>(new TTree("TreeD_sign_train_valid","sign_train_valid_tree"));
  
  TFile * sign_valid_file = new TFile("sign_valid.root","RECREATE");
  TTree * sign_valid_tree = new TTree("TreeD_sign_valid","sign_valid_tree");
  //std::unique_ptr<TTree> sign_valid_tree = std::unique_ptr<TTree>(new TTree("TreeD","sign_valid_tree"));

  TFile * sign_hstfc_file = new TFile("sign_hstfc.root","RECREATE");
  TTree * sign_hstfc_tree = new TTree("TreeD_sign_hstfc","sign_hstfc_tree");
  //std::unique_ptr<TTree> sign_hstfc_tree = std::unique_ptr<TTree>(new TTree("TreeD","sign_hstfc_tree"));

  TFile * bckg_train_valid_file = new TFile("bckg_train_valid.root","RECREATE");
  TTree * bckg_train_valid_tree = new TTree("TreeD_bckg_train_valid","bckg_train_valid_tree");
  //std::unique_ptr<TTree> bckg_train_valid_tree = std::unique_ptr<TTree>(new TTree("TreeD_bckg_train_valid","bckg_train_valid_tree"));

  TFile * bckg_valid_file = new TFile("bckg_valid.root","RECREATE");
  TTree * bckg_valid_tree = new TTree("TreeD_bckg_valid","bckg_valid_tree");
  //std::unique_ptr<TTree> bckg_valid_tree = std::unique_ptr<TTree>(new TTree("TreeD","bckg_valid_tree"));

  TFile * bckg_hstfc_file = new TFile("bckg_hstfc.root","RECREATE");
  TTree * bckg_hstfc_tree = new TTree("TreeD_bckg_hstfc","bckg_hstfc_tree");
  //std::unique_ptr<TTree> bckg_hstfc_tree = std::unique_ptr<TTree>(new TTree("TreeD","bckg_hstfc_tree"));

  float sign_AbsCosTheta,sign_AbsdPhi,sign_AbsPhi,sign_DPhi,sign_DY,sign_Phi,sign_costheta,sign_EventNumber,sign_Lambda,sign_qTSquared,sign_DiMuonMass,
        sign_DiMuonTau,sign_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_DiMuonPt,sign_PhotonPt,sign_AbsdY,sign_qxpsi,sign_qypsi,sign_qxgamma,sign_qygamma,
        sign_qxsum,sign_qysum,sign_qtA,sign_qtB, sign_qtL, sign_qtM;

  float bckg_AbsCosTheta,bckg_AbsdPhi,bckg_AbsPhi,bckg_DPhi,bckg_DY,bckg_Phi,bckg_costheta,bckg_EventNumber,bckg_Lambda,bckg_qTSquared,bckg_DiMuonMass,
        bckg_DiMuonTau,bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_DiMuonPt,bckg_PhotonPt,bckg_AbsdY,bckg_qxpsi,bckg_qypsi,bckg_qxgamma,bckg_qygamma,
        bckg_qxsum,bckg_qysum,bckg_qtA,bckg_qtB, bckg_qtL, bckg_qtM;

  sign_tree->SetBranchAddress("AbsCosTheta",                      &sign_AbsCosTheta);
	sign_tree->SetBranchAddress("AbsdPhi",                          &sign_AbsdPhi);
	sign_tree->SetBranchAddress("AbsPhi",                           &sign_AbsPhi);
	sign_tree->SetBranchAddress("DPhi",                             &sign_DPhi);
	sign_tree->SetBranchAddress("DY",                               &sign_DY);
	sign_tree->SetBranchAddress("Phi",                              &sign_Phi);
	sign_tree->SetBranchAddress("costheta",                         &sign_costheta);
	sign_tree->SetBranchAddress("EventNumber",                      &sign_EventNumber);
	sign_tree->SetBranchAddress("Lambda",                           &sign_Lambda);
	sign_tree->SetBranchAddress("qTSquared",                        &sign_qTSquared);
	sign_tree->SetBranchAddress("DiMuonMass",                       &sign_DiMuonMass);
	sign_tree->SetBranchAddress("DiMuonTau",                        &sign_DiMuonTau);
	sign_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",  &sign_Trigger_HLT_2mu4_bJpsimumu_noL2);
	sign_tree->SetBranchAddress("DiMuonPt",                         &sign_DiMuonPt);
	sign_tree->SetBranchAddress("PhotonPt",                         &sign_PhotonPt);
	sign_tree->SetBranchAddress("AbsdY",                            &sign_AbsdY);
	sign_tree->SetBranchAddress("qxpsi",                            &sign_qxpsi);
	sign_tree->SetBranchAddress("qypsi",                            &sign_qypsi);
	sign_tree->SetBranchAddress("qxgamma",                          &sign_qxgamma);
	sign_tree->SetBranchAddress("qygamma",                          &sign_qygamma);
	sign_tree->SetBranchAddress("qxsum",                            &sign_qxsum);
	sign_tree->SetBranchAddress("qysum",                            &sign_qysum);
	sign_tree->SetBranchAddress("qtA",                              &sign_qtA);
	sign_tree->SetBranchAddress("qtB",                              &sign_qtB);
  sign_tree->SetBranchAddress("qtL",                              &sign_qtL);
	sign_tree->SetBranchAddress("qtM",                              &sign_qtM);

  bckg_tree->SetBranchAddress("AbsCosTheta",                      &bckg_AbsCosTheta);
	bckg_tree->SetBranchAddress("AbsdPhi",                          &bckg_AbsdPhi);
	bckg_tree->SetBranchAddress("AbsPhi",                           &bckg_AbsPhi);
	bckg_tree->SetBranchAddress("DPhi",                             &bckg_DPhi);
	bckg_tree->SetBranchAddress("DY",                               &bckg_DY);
	bckg_tree->SetBranchAddress("Phi",                              &bckg_Phi);
	bckg_tree->SetBranchAddress("costheta",                         &bckg_costheta);
	bckg_tree->SetBranchAddress("EventNumber",                      &bckg_EventNumber);
	bckg_tree->SetBranchAddress("Lambda",                           &bckg_Lambda);
	bckg_tree->SetBranchAddress("qTSquared",                        &bckg_qTSquared);
	bckg_tree->SetBranchAddress("DiMuonMass",                       &bckg_DiMuonMass);
	bckg_tree->SetBranchAddress("DiMuonTau",                        &bckg_DiMuonTau);
	bckg_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",  &bckg_Trigger_HLT_2mu4_bJpsimumu_noL2);
	bckg_tree->SetBranchAddress("DiMuonPt",                         &bckg_DiMuonPt);
	bckg_tree->SetBranchAddress("PhotonPt",                         &bckg_PhotonPt);
	bckg_tree->SetBranchAddress("AbsdY",                            &bckg_AbsdY);
	bckg_tree->SetBranchAddress("qxpsi",                            &bckg_qxpsi);
	bckg_tree->SetBranchAddress("qypsi",                            &bckg_qypsi);
	bckg_tree->SetBranchAddress("qxgamma",                          &bckg_qxgamma);
	bckg_tree->SetBranchAddress("qygamma",                          &bckg_qygamma);
	bckg_tree->SetBranchAddress("qxsum",                            &bckg_qxsum);
	bckg_tree->SetBranchAddress("qysum",                            &bckg_qysum);
	bckg_tree->SetBranchAddress("qtA",                              &bckg_qtA);
	bckg_tree->SetBranchAddress("qtB",                              &bckg_qtB);
  bckg_tree->SetBranchAddress("qtL",                              &bckg_qtL);
	bckg_tree->SetBranchAddress("qtM",                              &bckg_qtM);

  
  float sign_train_valid_out_AbsCosTheta,sign_train_valid_out_AbsdPhi,sign_train_valid_out_AbsPhi,sign_train_valid_out_DPhi,sign_train_valid_out_DY,sign_train_valid_out_Phi,sign_train_valid_out_costheta,sign_train_valid_out_EventNumber,sign_train_valid_out_Lambda,sign_train_valid_out_qTSquared,sign_train_valid_out_DiMuonMass,
        sign_train_valid_out_DiMuonTau,sign_train_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_train_valid_out_DiMuonPt,sign_train_valid_out_PhotonPt,sign_train_valid_out_AbsdY,sign_train_valid_out_qxpsi,sign_train_valid_out_qypsi,sign_train_valid_out_qxgamma,sign_train_valid_out_qygamma,
        sign_train_valid_out_qxsum,sign_train_valid_out_qysum,sign_train_valid_out_qtA,sign_train_valid_out_qtB, sign_train_valid_out_qtL, sign_train_valid_out_qtM;

  float sign_valid_out_AbsCosTheta,sign_valid_out_AbsdPhi,sign_valid_out_AbsPhi,sign_valid_out_DPhi,sign_valid_out_DY,sign_valid_out_Phi,sign_valid_out_costheta,sign_valid_out_EventNumber,sign_valid_out_Lambda,sign_valid_out_qTSquared,sign_valid_out_DiMuonMass,
        sign_valid_out_DiMuonTau,sign_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_valid_out_DiMuonPt,sign_valid_out_PhotonPt,sign_valid_out_AbsdY,sign_valid_out_qxpsi,sign_valid_out_qypsi,sign_valid_out_qxgamma,sign_valid_out_qygamma,
        sign_valid_out_qxsum,sign_valid_out_qysum,sign_valid_out_qtA,sign_valid_out_qtB, sign_valid_out_qtL, sign_valid_out_qtM;
  
  float sign_hstfc_out_AbsCosTheta,sign_hstfc_out_AbsdPhi,sign_hstfc_out_AbsPhi,sign_hstfc_out_DPhi,sign_hstfc_out_DY,sign_hstfc_out_Phi,sign_hstfc_out_costheta,sign_hstfc_out_EventNumber,sign_hstfc_out_Lambda,sign_hstfc_out_qTSquared,sign_hstfc_out_DiMuonMass,
        sign_hstfc_out_DiMuonTau,sign_hstfc_out_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_hstfc_out_DiMuonPt,sign_hstfc_out_PhotonPt,sign_hstfc_out_AbsdY,sign_hstfc_out_qxpsi,sign_hstfc_out_qypsi,sign_hstfc_out_qxgamma,sign_hstfc_out_qygamma,
        sign_hstfc_out_qxsum,sign_hstfc_out_qysum,sign_hstfc_out_qtA,sign_hstfc_out_qtB, sign_hstfc_out_qtL, sign_hstfc_out_qtM;
  
  float bckg_train_valid_out_AbsCosTheta,bckg_train_valid_out_AbsdPhi,bckg_train_valid_out_AbsPhi,bckg_train_valid_out_DPhi,bckg_train_valid_out_DY,bckg_train_valid_out_Phi,bckg_train_valid_out_costheta,bckg_train_valid_out_EventNumber,bckg_train_valid_out_Lambda,bckg_train_valid_out_qTSquared,bckg_train_valid_out_DiMuonMass,
        bckg_train_valid_out_DiMuonTau,bckg_train_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_train_valid_out_DiMuonPt,bckg_train_valid_out_PhotonPt,bckg_train_valid_out_AbsdY,bckg_train_valid_out_qxpsi,bckg_train_valid_out_qypsi,bckg_train_valid_out_qxgamma,bckg_train_valid_out_qygamma,
        bckg_train_valid_out_qxsum,bckg_train_valid_out_qysum,bckg_train_valid_out_qtA,bckg_train_valid_out_qtB, bckg_train_valid_out_qtL, bckg_train_valid_out_qtM;
  
  float bckg_valid_out_AbsCosTheta,bckg_valid_out_AbsdPhi,bckg_valid_out_AbsPhi,bckg_valid_out_DPhi,bckg_valid_out_DY,bckg_valid_out_Phi,bckg_valid_out_costheta,bckg_valid_out_EventNumber,bckg_valid_out_Lambda,bckg_valid_out_qTSquared,bckg_valid_out_DiMuonMass,
        bckg_valid_out_DiMuonTau,bckg_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_valid_out_DiMuonPt,bckg_valid_out_PhotonPt,bckg_valid_out_AbsdY,bckg_valid_out_qxpsi,bckg_valid_out_qypsi,bckg_valid_out_qxgamma,bckg_valid_out_qygamma,
        bckg_valid_out_qxsum,bckg_valid_out_qysum,bckg_valid_out_qtA,bckg_valid_out_qtB, bckg_valid_out_qtL, bckg_valid_out_qtM;

  float bckg_hstfc_out_AbsCosTheta,bckg_hstfc_out_AbsdPhi,bckg_hstfc_out_AbsPhi,bckg_hstfc_out_DPhi,bckg_hstfc_out_DY,bckg_hstfc_out_Phi,bckg_hstfc_out_costheta,bckg_hstfc_out_EventNumber,bckg_hstfc_out_Lambda,bckg_hstfc_out_qTSquared,bckg_hstfc_out_DiMuonMass,
        bckg_hstfc_out_DiMuonTau,bckg_hstfc_out_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_hstfc_out_DiMuonPt,bckg_hstfc_out_PhotonPt,bckg_hstfc_out_AbsdY,bckg_hstfc_out_qxpsi,bckg_hstfc_out_qypsi,bckg_hstfc_out_qxgamma,bckg_hstfc_out_qygamma,
        bckg_hstfc_out_qxsum,bckg_hstfc_out_qysum,bckg_hstfc_out_qtA,bckg_hstfc_out_qtB, bckg_hstfc_out_qtL, bckg_hstfc_out_qtM;

  
  sign_train_valid_tree->Branch("AbsCosTheta",                              &sign_train_valid_out_AbsCosTheta,                       "AbsCosTheta/F");
  sign_train_valid_tree->Branch("AbsdPhi",                                  &sign_train_valid_out_AbsdPhi,                           "AbsdPhi/F");
  sign_train_valid_tree->Branch("AbsPhi",                                   &sign_train_valid_out_AbsPhi,                            "AbsPhi/F");
  sign_train_valid_tree->Branch("DPhi",                                     &sign_train_valid_out_DPhi,                              "DPhi/F");
  sign_train_valid_tree->Branch("DY",                                       &sign_train_valid_out_DY,                                "DY/F");
  sign_train_valid_tree->Branch("Phi",                                      &sign_train_valid_out_Phi,                               "phi/F");
  sign_train_valid_tree->Branch("costheta",                                 &sign_train_valid_out_costheta,                          "CosTheta/F");
  sign_train_valid_tree->Branch("EventNumber",                              &sign_train_valid_out_EventNumber,                       "EventNumber/F");
  sign_train_valid_tree->Branch("Lambda",                                   &sign_train_valid_out_Lambda,                            "Lambda/F");
  sign_train_valid_tree->Branch("qTSquared",                                &sign_train_valid_out_qTSquared,                         "qTSquared/F");
  sign_train_valid_tree->Branch("DiMuonMass",                               &sign_train_valid_out_DiMuonMass,                        "DiMuonMass/F");
  sign_train_valid_tree->Branch("DiMuonTau",                                &sign_train_valid_out_DiMuonTau,                         "DiMuonTau/F");
  sign_train_valid_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &sign_train_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
  sign_train_valid_tree->Branch("DiMuonPt",                                 &sign_train_valid_out_DiMuonPt,                          "DiMuonPt/F");
  sign_train_valid_tree->Branch("PhotonPt",                                 &sign_train_valid_out_PhotonPt,                          "PhotonPt/F");
  sign_train_valid_tree->Branch("AbsdY",                                    &sign_train_valid_out_AbsdY,                             "AbsdY");
  sign_train_valid_tree->Branch("qxpsi",                                    &sign_train_valid_out_qxpsi,                             "qxpsi");
  sign_train_valid_tree->Branch("qypsi",                                    &sign_train_valid_out_qypsi,                             "qypsi");
  sign_train_valid_tree->Branch("qxgamma",                                  &sign_train_valid_out_qxgamma,                           "qxgamma");
  sign_train_valid_tree->Branch("qygamma",                                  &sign_train_valid_out_qygamma,                           "qygamma");
  sign_train_valid_tree->Branch("qxsum",                                    &sign_train_valid_out_qxsum,                             "qxsum");
  sign_train_valid_tree->Branch("qysum",                                    &sign_train_valid_out_qysum,                             "qysum");
  sign_train_valid_tree->Branch("qtA",                                      &sign_train_valid_out_qtA,                               "qtA");
  sign_train_valid_tree->Branch("qtB",                                      &sign_train_valid_out_qtB,                               "qtB");
  sign_train_valid_tree->Branch("qtL",                                      &sign_train_valid_out_qtL,                               "qtL");
  sign_train_valid_tree->Branch("qtM",                                      &sign_train_valid_out_qtM,                               "qtM");

  sign_valid_tree->Branch("AbsCosTheta",                              &sign_valid_out_AbsCosTheta,                       "AbsCosTheta/F");
  sign_valid_tree->Branch("AbsdPhi",                                  &sign_valid_out_AbsdPhi,                           "AbsdPhi/F");
  sign_valid_tree->Branch("AbsPhi",                                   &sign_valid_out_AbsPhi,                            "AbsPhi/F");
  sign_valid_tree->Branch("DPhi",                                     &sign_valid_out_DPhi,                              "DPhi/F");
  sign_valid_tree->Branch("DY",                                       &sign_valid_out_DY,                                "DY/F");
  sign_valid_tree->Branch("Phi",                                      &sign_valid_out_Phi,                               "phi/F");
  sign_valid_tree->Branch("costheta",                                 &sign_valid_out_costheta,                          "CosTheta/F");
  sign_valid_tree->Branch("EventNumber",                              &sign_valid_out_EventNumber,                       "EventNumber/F");
  sign_valid_tree->Branch("Lambda",                                   &sign_valid_out_Lambda,                            "Lambda/F");
  sign_valid_tree->Branch("qTSquared",                                &sign_valid_out_qTSquared,                         "qTSquared/F");
  sign_valid_tree->Branch("DiMuonMass",                               &sign_valid_out_DiMuonMass,                        "DiMuonMass/F");
  sign_valid_tree->Branch("DiMuonTau",                                &sign_valid_out_DiMuonTau,                         "DiMuonTau/F");
  sign_valid_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &sign_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
  sign_valid_tree->Branch("DiMuonPt",                                 &sign_valid_out_DiMuonPt,                          "DiMuonPt/F");
  sign_valid_tree->Branch("PhotonPt",                                 &sign_valid_out_PhotonPt,                          "PhotonPt/F");
  sign_valid_tree->Branch("AbsdY",                                    &sign_valid_out_AbsdY,                             "AbsdY");
  sign_valid_tree->Branch("qxpsi",                                    &sign_valid_out_qxpsi,                             "qxpsi");
  sign_valid_tree->Branch("qypsi",                                    &sign_valid_out_qypsi,                             "qypsi");
  sign_valid_tree->Branch("qxgamma",                                  &sign_valid_out_qxgamma,                           "qxgamma");
  sign_valid_tree->Branch("qygamma",                                  &sign_valid_out_qygamma,                           "qygamma");
  sign_valid_tree->Branch("qxsum",                                    &sign_valid_out_qxsum,                             "qxsum");
  sign_valid_tree->Branch("qysum",                                    &sign_valid_out_qysum,                             "qysum");
  sign_valid_tree->Branch("qtA",                                      &sign_valid_out_qtA,                               "qtA");
  sign_valid_tree->Branch("qtB",                                      &sign_valid_out_qtB,                               "qtB");
  sign_valid_tree->Branch("qtL",                                      &sign_valid_out_qtL,                               "qtL");
  sign_valid_tree->Branch("qtM",                                      &sign_valid_out_qtM,                               "qtM");

  sign_hstfc_tree->Branch("AbsCosTheta",                              &sign_hstfc_out_AbsCosTheta,                       "AbsCosTheta/F");
  sign_hstfc_tree->Branch("AbsdPhi",                                  &sign_hstfc_out_AbsdPhi,                           "AbsdPhi/F");
  sign_hstfc_tree->Branch("AbsPhi",                                   &sign_hstfc_out_AbsPhi,                            "AbsPhi/F");
  sign_hstfc_tree->Branch("DPhi",                                     &sign_hstfc_out_DPhi,                              "DPhi/F");
  sign_hstfc_tree->Branch("DY",                                       &sign_hstfc_out_DY,                                "DY/F");
  sign_hstfc_tree->Branch("Phi",                                      &sign_hstfc_out_Phi,                               "phi/F");
  sign_hstfc_tree->Branch("costheta",                                 &sign_hstfc_out_costheta,                          "CosTheta/F");
  sign_hstfc_tree->Branch("EventNumber",                              &sign_hstfc_out_EventNumber,                       "EventNumber/F");
  sign_hstfc_tree->Branch("Lambda",                                   &sign_hstfc_out_Lambda,                            "Lambda/F");
  sign_hstfc_tree->Branch("qTSquared",                                &sign_hstfc_out_qTSquared,                         "qTSquared/F");
  sign_hstfc_tree->Branch("DiMuonMass",                               &sign_hstfc_out_DiMuonMass,                        "DiMuonMass/F");
  sign_hstfc_tree->Branch("DiMuonTau",                                &sign_hstfc_out_DiMuonTau,                         "DiMuonTau/F");
  sign_hstfc_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &sign_hstfc_out_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
  sign_hstfc_tree->Branch("DiMuonPt",                                 &sign_hstfc_out_DiMuonPt,                          "DiMuonPt/F");
  sign_hstfc_tree->Branch("PhotonPt",                                 &sign_hstfc_out_PhotonPt,                          "PhotonPt/F");
  sign_hstfc_tree->Branch("AbsdY",                                    &sign_hstfc_out_AbsdY,                             "AbsdY");
  sign_hstfc_tree->Branch("qxpsi",                                    &sign_hstfc_out_qxpsi,                             "qxpsi");
  sign_hstfc_tree->Branch("qypsi",                                    &sign_hstfc_out_qypsi,                             "qypsi");
  sign_hstfc_tree->Branch("qxgamma",                                  &sign_hstfc_out_qxgamma,                           "qxgamma");
  sign_hstfc_tree->Branch("qygamma",                                  &sign_hstfc_out_qygamma,                           "qygamma");
  sign_hstfc_tree->Branch("qxsum",                                    &sign_hstfc_out_qxsum,                             "qxsum");
  sign_hstfc_tree->Branch("qysum",                                    &sign_hstfc_out_qysum,                             "qysum");
  sign_hstfc_tree->Branch("qtA",                                      &sign_hstfc_out_qtA,                               "qtA");
  sign_hstfc_tree->Branch("qtB",                                      &sign_hstfc_out_qtB,                               "qtB");
  sign_hstfc_tree->Branch("qtL",                                      &sign_hstfc_out_qtL,                               "qtL");
  sign_hstfc_tree->Branch("qtM",                                      &sign_hstfc_out_qtM,                               "qtM");

  bckg_train_valid_tree->Branch("AbsCosTheta",                              &bckg_train_valid_out_AbsCosTheta,                       "AbsCosTheta/F");
  bckg_train_valid_tree->Branch("AbsdPhi",                                  &bckg_train_valid_out_AbsdPhi,                           "AbsdPhi/F");
  bckg_train_valid_tree->Branch("AbsPhi",                                   &bckg_train_valid_out_AbsPhi,                            "AbsPhi/F");
  bckg_train_valid_tree->Branch("DPhi",                                     &bckg_train_valid_out_DPhi,                              "DPhi/F");
  bckg_train_valid_tree->Branch("DY",                                       &bckg_train_valid_out_DY,                                "DY/F");
  bckg_train_valid_tree->Branch("Phi",                                      &bckg_train_valid_out_Phi,                               "phi/F");
  bckg_train_valid_tree->Branch("costheta",                                 &bckg_train_valid_out_costheta,                          "CosTheta/F");
  bckg_train_valid_tree->Branch("EventNumber",                              &bckg_train_valid_out_EventNumber,                       "EventNumber/F");
  bckg_train_valid_tree->Branch("Lambda",                                   &bckg_train_valid_out_Lambda,                            "Lambda/F");
  bckg_train_valid_tree->Branch("qTSquared",                                &bckg_train_valid_out_qTSquared,                         "qTSquared/F");
  bckg_train_valid_tree->Branch("DiMuonMass",                               &bckg_train_valid_out_DiMuonMass,                        "DiMuonMass/F");
  bckg_train_valid_tree->Branch("DiMuonTau",                                &bckg_train_valid_out_DiMuonTau,                         "DiMuonTau/F");
  bckg_train_valid_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &bckg_train_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
  bckg_train_valid_tree->Branch("DiMuonPt",                                 &bckg_train_valid_out_DiMuonPt,                          "DiMuonPt/F");
  bckg_train_valid_tree->Branch("PhotonPt",                                 &bckg_train_valid_out_PhotonPt,                          "PhotonPt/F");
  bckg_train_valid_tree->Branch("AbsdY",                                    &bckg_train_valid_out_AbsdY,                             "AbsdY");
  bckg_train_valid_tree->Branch("qxpsi",                                    &bckg_train_valid_out_qxpsi,                             "qxpsi");
  bckg_train_valid_tree->Branch("qypsi",                                    &bckg_train_valid_out_qypsi,                             "qypsi");
  bckg_train_valid_tree->Branch("qxgamma",                                  &bckg_train_valid_out_qxgamma,                           "qxgamma");
  bckg_train_valid_tree->Branch("qygamma",                                  &bckg_train_valid_out_qygamma,                           "qygamma");
  bckg_train_valid_tree->Branch("qxsum",                                    &bckg_train_valid_out_qxsum,                             "qxsum");
  bckg_train_valid_tree->Branch("qysum",                                    &bckg_train_valid_out_qysum,                             "qysum");
  bckg_train_valid_tree->Branch("qtA",                                      &bckg_train_valid_out_qtA,                               "qtA");
  bckg_train_valid_tree->Branch("qtB",                                      &bckg_train_valid_out_qtB,                               "qtB");
  bckg_train_valid_tree->Branch("qtL",                                      &bckg_train_valid_out_qtL,                               "qtL");
  bckg_train_valid_tree->Branch("qtM",                                      &bckg_train_valid_out_qtM,                               "qtM");

  bckg_valid_tree->Branch("AbsCosTheta",                              &bckg_valid_out_AbsCosTheta,                       "AbsCosTheta/F");
  bckg_valid_tree->Branch("AbsdPhi",                                  &bckg_valid_out_AbsdPhi,                           "AbsdPhi/F");
  bckg_valid_tree->Branch("AbsPhi",                                   &bckg_valid_out_AbsPhi,                            "AbsPhi/F");
  bckg_valid_tree->Branch("DPhi",                                     &bckg_valid_out_DPhi,                              "DPhi/F");
  bckg_valid_tree->Branch("DY",                                       &bckg_valid_out_DY,                                "DY/F");
  bckg_valid_tree->Branch("Phi",                                      &bckg_valid_out_Phi,                               "phi/F");
  bckg_valid_tree->Branch("costheta",                                 &bckg_valid_out_costheta,                          "CosTheta/F");
  bckg_valid_tree->Branch("EventNumber",                              &bckg_valid_out_EventNumber,                       "EventNumber/F");
  bckg_valid_tree->Branch("Lambda",                                   &bckg_valid_out_Lambda,                            "Lambda/F");
  bckg_valid_tree->Branch("qTSquared",                                &bckg_valid_out_qTSquared,                         "qTSquared/F");
  bckg_valid_tree->Branch("DiMuonMass",                               &bckg_valid_out_DiMuonMass,                        "DiMuonMass/F");
  bckg_valid_tree->Branch("DiMuonTau",                                &bckg_valid_out_DiMuonTau,                         "DiMuonTau/F");
  bckg_valid_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &bckg_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
  bckg_valid_tree->Branch("DiMuonPt",                                 &bckg_valid_out_DiMuonPt,                          "DiMuonPt/F");
  bckg_valid_tree->Branch("PhotonPt",                                 &bckg_valid_out_PhotonPt,                          "PhotonPt/F");
  bckg_valid_tree->Branch("AbsdY",                                    &bckg_valid_out_AbsdY,                             "AbsdY");
  bckg_valid_tree->Branch("qxpsi",                                    &bckg_valid_out_qxpsi,                             "qxpsi");
  bckg_valid_tree->Branch("qypsi",                                    &bckg_valid_out_qypsi,                             "qypsi");
  bckg_valid_tree->Branch("qxgamma",                                  &bckg_valid_out_qxgamma,                           "qxgamma");
  bckg_valid_tree->Branch("qygamma",                                  &bckg_valid_out_qygamma,                           "qygamma");
  bckg_valid_tree->Branch("qxsum",                                    &bckg_valid_out_qxsum,                             "qxsum");
  bckg_valid_tree->Branch("qysum",                                    &bckg_valid_out_qysum,                             "qysum");
  bckg_valid_tree->Branch("qtA",                                      &bckg_valid_out_qtA,                               "qtA");
  bckg_valid_tree->Branch("qtB",                                      &bckg_valid_out_qtB,                               "qtB");
  bckg_valid_tree->Branch("qtL",                                      &bckg_valid_out_qtL,                               "qtL");
  bckg_valid_tree->Branch("qtM",                                      &bckg_valid_out_qtM,                               "qtM");

  bckg_hstfc_tree->Branch("AbsCosTheta",                              &bckg_hstfc_out_AbsCosTheta,                       "AbsCosTheta/F");
  bckg_hstfc_tree->Branch("AbsdPhi",                                  &bckg_hstfc_out_AbsdPhi,                           "AbsdPhi/F");
  bckg_hstfc_tree->Branch("AbsPhi",                                   &bckg_hstfc_out_AbsPhi,                            "AbsPhi/F");
  bckg_hstfc_tree->Branch("DPhi",                                     &bckg_hstfc_out_DPhi,                              "DPhi/F");
  bckg_hstfc_tree->Branch("DY",                                       &bckg_hstfc_out_DY,                                "DY/F");
  bckg_hstfc_tree->Branch("Phi",                                      &bckg_hstfc_out_Phi,                               "phi/F");
  bckg_hstfc_tree->Branch("costheta",                                 &bckg_hstfc_out_costheta,                          "CosTheta/F");
  bckg_hstfc_tree->Branch("EventNumber",                              &bckg_hstfc_out_EventNumber,                       "EventNumber/F");
  bckg_hstfc_tree->Branch("Lambda",                                   &bckg_hstfc_out_Lambda,                            "Lambda/F");
  bckg_hstfc_tree->Branch("qTSquared",                                &bckg_hstfc_out_qTSquared,                         "qTSquared/F");
  bckg_hstfc_tree->Branch("DiMuonMass",                               &bckg_hstfc_out_DiMuonMass,                        "DiMuonMass/F");
  bckg_hstfc_tree->Branch("DiMuonTau",                                &bckg_hstfc_out_DiMuonTau,                         "DiMuonTau/F");
  bckg_hstfc_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &bckg_hstfc_out_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
  bckg_hstfc_tree->Branch("DiMuonPt",                                 &bckg_hstfc_out_DiMuonPt,                          "DiMuonPt/F");
  bckg_hstfc_tree->Branch("PhotonPt",                                 &bckg_hstfc_out_PhotonPt,                          "PhotonPt/F");
  bckg_hstfc_tree->Branch("AbsdY",                                    &bckg_hstfc_out_AbsdY,                             "AbsdY");
  bckg_hstfc_tree->Branch("qxpsi",                                    &bckg_hstfc_out_qxpsi,                             "qxpsi");
  bckg_hstfc_tree->Branch("qypsi",                                    &bckg_hstfc_out_qypsi,                             "qypsi");
  bckg_hstfc_tree->Branch("qxgamma",                                  &bckg_hstfc_out_qxgamma,                           "qxgamma");
  bckg_hstfc_tree->Branch("qygamma",                                  &bckg_hstfc_out_qygamma,                           "qygamma");
  bckg_hstfc_tree->Branch("qxsum",                                    &bckg_hstfc_out_qxsum,                             "qxsum");
  bckg_hstfc_tree->Branch("qysum",                                    &bckg_hstfc_out_qysum,                             "qysum");
  bckg_hstfc_tree->Branch("qtA",                                      &bckg_hstfc_out_qtA,                               "qtA");
  bckg_hstfc_tree->Branch("qtB",                                      &bckg_hstfc_out_qtB,                               "qtB");
  bckg_hstfc_tree->Branch("qtL",                                      &bckg_hstfc_out_qtL,                               "qtL");
  bckg_hstfc_tree->Branch("qtM",                                      &bckg_hstfc_out_qtM,                               "qtM");


  int bckg_start = 0;
  int bckg_sthrd = 26213;
  int bckg_end = 39320;
  
  int sign_start = 0;
  int sign_sthrd = 142666;
  int sign_end = 214000;


  for ( int sign_entry = sign_start; sign_entry < sign_sthrd; sign_entry++ ){

    sign_tree->GetEntry(sign_entry);
    sign_train_valid_out_AbsCosTheta                             = sign_AbsCosTheta;
    sign_train_valid_out_AbsdPhi                                 = sign_AbsdPhi;
    sign_train_valid_out_AbsPhi                                  = sign_AbsPhi;
    sign_train_valid_out_DPhi                                    = sign_DPhi;
    sign_train_valid_out_DY                                      = sign_DY;
    sign_train_valid_out_Phi                                     = sign_Phi;
    sign_train_valid_out_costheta                                = sign_costheta;
    sign_train_valid_out_EventNumber                             = sign_EventNumber;
    sign_train_valid_out_Lambda                                  = sign_Lambda;
    sign_train_valid_out_qTSquared                               = sign_qTSquared;
    sign_train_valid_out_DiMuonMass                              = sign_DiMuonMass;
    sign_train_valid_out_DiMuonTau                               = sign_DiMuonTau;
    sign_train_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
    sign_train_valid_out_DiMuonPt                                = sign_DiMuonPt;
    sign_train_valid_out_PhotonPt                                = sign_PhotonPt;
    sign_train_valid_out_AbsdY                                   = sign_AbsdY;
    sign_train_valid_out_qxpsi                                   = sign_qxpsi;
    sign_train_valid_out_qypsi                                   = sign_qypsi;
    sign_train_valid_out_qxgamma                                 = sign_qxgamma;
    sign_train_valid_out_qygamma                                 = sign_qygamma;
    sign_train_valid_out_qxsum                                   = sign_qxsum;
    sign_train_valid_out_qysum                                   = sign_qysum;
    sign_train_valid_out_qtA                                     = sign_qtA;
    sign_train_valid_out_qtB                                     = sign_qtB;
    sign_train_valid_out_qtL                                     = sign_qtM;
    sign_train_valid_tree->Fill();

  }
  
  sign_train_valid_file->cd();
  sign_train_valid_tree->Write();
  sign_train_valid_file->Close();

  
  for ( int sign_entry = sign_start; sign_entry < sign_sthrd; sign_entry++ ){

    sign_tree->GetEntry(sign_entry);
    if ( (((int) sign_EventNumber) % 2) == 0) continue;
    sign_valid_out_AbsCosTheta                             = sign_AbsCosTheta;
    sign_valid_out_AbsdPhi                                 = sign_AbsdPhi;
    sign_valid_out_AbsPhi                                  = sign_AbsPhi;
    sign_valid_out_DPhi                                    = sign_DPhi;
    sign_valid_out_DY                                      = sign_DY;
    sign_valid_out_Phi                                     = sign_Phi;
    sign_valid_out_costheta                                = sign_costheta;
    sign_valid_out_EventNumber                             = sign_EventNumber;
    sign_valid_out_Lambda                                  = sign_Lambda;
    sign_valid_out_qTSquared                               = sign_qTSquared;
    sign_valid_out_DiMuonMass                              = sign_DiMuonMass;
    sign_valid_out_DiMuonTau                               = sign_DiMuonTau;
    sign_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
    sign_valid_out_DiMuonPt                                = sign_DiMuonPt;
    sign_valid_out_PhotonPt                                = sign_PhotonPt;
    sign_valid_out_AbsdY                                   = sign_AbsdY;
    sign_valid_out_qxpsi                                   = sign_qxpsi;
    sign_valid_out_qypsi                                   = sign_qypsi;
    sign_valid_out_qxgamma                                 = sign_qxgamma;
    sign_valid_out_qygamma                                 = sign_qygamma;
    sign_valid_out_qxsum                                   = sign_qxsum;
    sign_valid_out_qysum                                   = sign_qysum;
    sign_valid_out_qtA                                     = sign_qtA;
    sign_valid_out_qtB                                     = sign_qtB;
    sign_valid_out_qtL                                     = sign_qtM;
    sign_valid_tree->Fill();

  }
  sign_valid_file->cd();
  sign_valid_tree->Write();
  sign_valid_file->Close();

  for ( int sign_entry = sign_sthrd; sign_entry < sign_end; sign_entry++ ){

    sign_tree->GetEntry(sign_entry);
    sign_hstfc_out_AbsCosTheta                             = sign_AbsCosTheta;
    sign_hstfc_out_AbsdPhi                                 = sign_AbsdPhi;
    sign_hstfc_out_AbsPhi                                  = sign_AbsPhi;
    sign_hstfc_out_DPhi                                    = sign_DPhi;
    sign_hstfc_out_DY                                      = sign_DY;
    sign_hstfc_out_Phi                                     = sign_Phi;
    sign_hstfc_out_costheta                                = sign_costheta;
    sign_hstfc_out_EventNumber                             = sign_EventNumber;
    sign_hstfc_out_Lambda                                  = sign_Lambda;
    sign_hstfc_out_qTSquared                               = sign_qTSquared;
    sign_hstfc_out_DiMuonMass                              = sign_DiMuonMass;
    sign_hstfc_out_DiMuonTau                               = sign_DiMuonTau;
    sign_hstfc_out_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
    sign_hstfc_out_DiMuonPt                                = sign_DiMuonPt;
    sign_hstfc_out_PhotonPt                                = sign_PhotonPt;
    sign_hstfc_out_AbsdY                                   = sign_AbsdY;
    sign_hstfc_out_qxpsi                                   = sign_qxpsi;
    sign_hstfc_out_qypsi                                   = sign_qypsi;
    sign_hstfc_out_qxgamma                                 = sign_qxgamma;
    sign_hstfc_out_qygamma                                 = sign_qygamma;
    sign_hstfc_out_qxsum                                   = sign_qxsum;
    sign_hstfc_out_qysum                                   = sign_qysum;
    sign_hstfc_out_qtA                                     = sign_qtA;
    sign_hstfc_out_qtB                                     = sign_qtB;
    sign_hstfc_out_qtL                                     = sign_qtM;
    sign_hstfc_tree->Fill();

  }
  sign_hstfc_file->cd();
  sign_hstfc_tree->Write();
  sign_hstfc_file->Close();


  for ( int bckg_entry = bckg_start; bckg_entry < bckg_sthrd; bckg_entry++ ){

    bckg_tree->GetEntry(bckg_entry);
    bckg_train_valid_out_AbsCosTheta                             = bckg_AbsCosTheta;
    bckg_train_valid_out_AbsdPhi                                 = bckg_AbsdPhi;
    bckg_train_valid_out_AbsPhi                                  = bckg_AbsPhi;
    bckg_train_valid_out_DPhi                                    = bckg_DPhi;
    bckg_train_valid_out_DY                                      = bckg_DY;
    bckg_train_valid_out_Phi                                     = bckg_Phi;
    bckg_train_valid_out_costheta                                = bckg_costheta;
    bckg_train_valid_out_EventNumber                             = bckg_EventNumber;
    bckg_train_valid_out_Lambda                                  = bckg_Lambda;
    bckg_train_valid_out_qTSquared                               = bckg_qTSquared;
    bckg_train_valid_out_DiMuonMass                              = bckg_DiMuonMass;
    bckg_train_valid_out_DiMuonTau                               = bckg_DiMuonTau;
    bckg_train_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
    bckg_train_valid_out_DiMuonPt                                = bckg_DiMuonPt;
    bckg_train_valid_out_PhotonPt                                = bckg_PhotonPt;
    bckg_train_valid_out_AbsdY                                   = bckg_AbsdY;
    bckg_train_valid_out_qxpsi                                   = bckg_qxpsi;
    bckg_train_valid_out_qypsi                                   = bckg_qypsi;
    bckg_train_valid_out_qxgamma                                 = bckg_qxgamma;
    bckg_train_valid_out_qygamma                                 = bckg_qygamma;
    bckg_train_valid_out_qxsum                                   = bckg_qxsum;
    bckg_train_valid_out_qysum                                   = bckg_qysum;
    bckg_train_valid_out_qtA                                     = bckg_qtA;
    bckg_train_valid_out_qtB                                     = bckg_qtB;
    bckg_train_valid_out_qtL                                     = bckg_qtM;
    bckg_train_valid_tree->Fill();

  }
  bckg_train_valid_file->cd();
  bckg_train_valid_tree->Write();
  bckg_train_valid_file->Close();

  
  for ( int bckg_entry = bckg_start; bckg_entry < bckg_sthrd; bckg_entry++ ){

    bckg_tree->GetEntry(bckg_entry);
    if ( (((int) bckg_EventNumber) % 2) == 0) continue;
    bckg_valid_out_AbsCosTheta                             = bckg_AbsCosTheta;
    bckg_valid_out_AbsdPhi                                 = bckg_AbsdPhi;
    bckg_valid_out_AbsPhi                                  = bckg_AbsPhi;
    bckg_valid_out_DPhi                                    = bckg_DPhi;
    bckg_valid_out_DY                                      = bckg_DY;
    bckg_valid_out_Phi                                     = bckg_Phi;
    bckg_valid_out_costheta                                = bckg_costheta;
    bckg_valid_out_EventNumber                             = bckg_EventNumber;
    bckg_valid_out_Lambda                                  = bckg_Lambda;
    bckg_valid_out_qTSquared                               = bckg_qTSquared;
    bckg_valid_out_DiMuonMass                              = bckg_DiMuonMass;
    bckg_valid_out_DiMuonTau                               = bckg_DiMuonTau;
    bckg_valid_out_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
    bckg_valid_out_DiMuonPt                                = bckg_DiMuonPt;
    bckg_valid_out_PhotonPt                                = bckg_PhotonPt;
    bckg_valid_out_AbsdY                                   = bckg_AbsdY;
    bckg_valid_out_qxpsi                                   = bckg_qxpsi;
    bckg_valid_out_qypsi                                   = bckg_qypsi;
    bckg_valid_out_qxgamma                                 = bckg_qxgamma;
    bckg_valid_out_qygamma                                 = bckg_qygamma;
    bckg_valid_out_qxsum                                   = bckg_qxsum;
    bckg_valid_out_qysum                                   = bckg_qysum;
    bckg_valid_out_qtA                                     = bckg_qtA;
    bckg_valid_out_qtB                                     = bckg_qtB;
    bckg_valid_out_qtL                                     = bckg_qtM;
    bckg_valid_tree->Fill();

  }
  bckg_valid_file->cd();
  bckg_valid_tree->Write();
  bckg_valid_file->Close();


  for ( int bckg_entry = bckg_sthrd; bckg_entry < bckg_end; bckg_entry++ ){

    bckg_tree->GetEntry(bckg_entry);
    bckg_hstfc_out_AbsCosTheta                             = bckg_AbsCosTheta;
    bckg_hstfc_out_AbsdPhi                                 = bckg_AbsdPhi;
    bckg_hstfc_out_AbsPhi                                  = bckg_AbsPhi;
    bckg_hstfc_out_DPhi                                    = bckg_DPhi;
    bckg_hstfc_out_DY                                      = bckg_DY;
    bckg_hstfc_out_Phi                                     = bckg_Phi;
    bckg_hstfc_out_costheta                                = bckg_costheta;
    bckg_hstfc_out_EventNumber                             = bckg_EventNumber;
    bckg_hstfc_out_Lambda                                  = bckg_Lambda;
    bckg_hstfc_out_qTSquared                               = bckg_qTSquared;
    bckg_hstfc_out_DiMuonMass                              = bckg_DiMuonMass;
    bckg_hstfc_out_DiMuonTau                               = bckg_DiMuonTau;
    bckg_hstfc_out_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
    bckg_hstfc_out_DiMuonPt                                = bckg_DiMuonPt;
    bckg_hstfc_out_PhotonPt                                = bckg_PhotonPt;
    bckg_hstfc_out_AbsdY                                   = bckg_AbsdY;
    bckg_hstfc_out_qxpsi                                   = bckg_qxpsi;
    bckg_hstfc_out_qypsi                                   = bckg_qypsi;
    bckg_hstfc_out_qxgamma                                 = bckg_qxgamma;
    bckg_hstfc_out_qygamma                                 = bckg_qygamma;
    bckg_hstfc_out_qxsum                                   = bckg_qxsum;
    bckg_hstfc_out_qysum                                   = bckg_qysum;
    bckg_hstfc_out_qtA                                     = bckg_qtA;
    bckg_hstfc_out_qtB                                     = bckg_qtB;
    bckg_hstfc_out_qtL                                     = bckg_qtM;
    bckg_hstfc_tree->Fill();

  }
 
  bckg_hstfc_file->cd();
  bckg_hstfc_tree->Write();
  bckg_hstfc_file->Close();


  return;

}


