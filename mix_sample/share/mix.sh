#!/bin/bash

#Script Shell Variables
executable=${ANA_IP}/mix_samples/build/mix_sample
log=${LOG_PATH}/bdt/${unique}.txt
unique="base"
data=${OUT_PATH}/bdt/${unique}/eval/eval_data.root:data_${unique}
sign=${OUT_PATH}/bdt/${unique}/eval/eval_sign.root:sign_${unique}
bckg=${OUT_PATH}/bdt/${unique}/eval/eval_bckg.root:bckg_${unique}
bbbg=${OUT_PATH}/bdt/${unique}/eval/eval_bbbg.root:bbbg_${unique}

touch ${log}
mkdir -p ${OUT_PATH}/mix_samples/${unique}
mkdir -p ${OUT_PATH}/mix_samples/${unique}/lin_sign
mkdir -p ${OUT_PATH}/mix_samples/${unique}/lin_bckg
mkdir -p ${OUT_PATH}/mix_samples/${unique}/lin_data

pushd ${OUT_PATH}/mix_samples/${unique}/ >> /dev/null

## mix
${executable} -s ${sign} -b ${bckg} -d ${data} -m "lin" -u ${unique} 2>&1 | tee ${log}


popd >> /dev/null
