#include <make_fiducials.hxx>

void make_fiducials( std::string & file_path, std::string & type, std::string & unique,  bound_mgr * selections, std::string & weight_var ){

	ROOT::EnableImplicitMT( 6 );

  // ready the dataframe, load it from the previously skimmed tree
  TChain chain( "tree" );
  chain.Add( file_path.c_str() );
  ROOT::RDataFrame input_frame( chain );
  
  // list relevant output columns, should probably be const/global
  std::vector< std::string > output_columns = { "ActIpX", "AvgIpX", "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
															 "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
															 "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
															 "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0", "weight" };


  // note reduced set of truth cuts.
  // these strings retrieve cuts from bound manager
  std::vector< std::string > reco_vector_cuts = { "mu_neg_pt", "mu_pos_pt" };
  std::vector< std::string > truth_vector_cuts = { "truth_mu_pos_pt", "truth_mu_neg_pt" };

	std::vector< std::string > reco_float_cuts = { "lambda", "costheta", "qtSquared", "AbsPhi", 
                                                  "DiMuonMass", "DiMuonTau", "qtA", "qtB", 
                                                  "DiMuonPt", "DeltaZ0", "Phot_Eta", "MuPos_Eta", "MuNeg_Eta", "PhotonPt" };
  std::vector< std::string > truth_float_cuts = { "Phot_Eta", "MuPos_Eta", "MuNeg_Eta", "PhotonPt" };

  // transform these into bounds, i should literally use a transform
  std::vector< bound > reco_vector_bounds, truth_vector_bounds;
  for ( std::string & bound_str : reco_vector_cuts ){
    reco_vector_bounds.push_back( selections->get_bound( bound_str ) );
  }

  for ( std::string & bound_str : truth_vector_cuts ){
    truth_vector_bounds.push_back( selections->get_bound( bound_str ) );
  }

  std::vector< bound > reco_float_bounds, truth_float_bounds;
	for ( std::string & bound_str : reco_float_cuts ){
    reco_float_bounds.push_back( selections->get_bound( bound_str ) );
  }

	for ( std::string & bound_str : truth_float_cuts ){
    truth_float_bounds.push_back( selections->get_bound( bound_str ) );
  }

  
	// two base operations before adding the redefinitions and cuts
  // filter the events to those that are minimally compatible with those definitions
  auto reco_filtered = input_frame.Filter( "HLT_2mu4_bJpsimumu_noL2 != 0" );
  reco_filtered = reco_filtered.Filter( "Photon_Pt.size() != 0" );
  reco_filtered = reco_filtered.Filter( "MuMinus_Pt.size() != 0" );
  reco_filtered = reco_filtered.Filter( "MuPlus_Pt.size() != 0" );

	// manual redefinitions of analysis variables -> 0th selections
	auto phi_lambda = phi_calc;
	auto dphi_lambda = dphi_calc;
	reco_filtered = reco_filtered.Define( "ActIpX", 																			"Float_t( actIpX )" );	
	reco_filtered = reco_filtered.Define( "AvgIpX", 																			"Float_t( avgIpX )" );	
	reco_filtered = reco_filtered.Define( "costheta", 																		"Float_t( MuMuGamma_CS_CosTheta[0] )" );
	reco_filtered = reco_filtered.Define( "AbsCosTheta", 																	"Float_t( abs(costheta) )" );
	reco_filtered = reco_filtered.Define( "DPhi",                                         dphi_lambda, { "DiLept_Phi", "Photon_Phi" } );
	reco_filtered = reco_filtered.Define( "AbsdPhi", 																			"Float_t( abs(DPhi) )" );
	reco_filtered = reco_filtered.Define( "Phi",                                          phi_lambda, {"MuMuGamma_CS_Phi"} );
	reco_filtered = reco_filtered.Define( "AbsPhi", 																			"Float_t( sqrt( MuMuGamma_CS_Phi[0] * MuMuGamma_CS_Phi[0]) )" );
	reco_filtered = reco_filtered.Define( "DY", 																					"Float_t(DiLept_Y[0] - Photon_Eta[0])" );
	reco_filtered = reco_filtered.Define( "AbsdY", 																				"Float_t( abs(DY) )" );
	reco_filtered = reco_filtered.Define( "EventNumber",																	"Float_t( evt )" );
	reco_filtered = reco_filtered.Define( "Lambda", 																			"Float_t( (MuMuY_M[0]/3097.0)*(MuMuY_M[0]/3097.0) )" );
	reco_filtered = reco_filtered.Define( "qTSquared",																		"Float_t( (MuMuY_Pt[0]/1000.0) * (MuMuY_Pt[0]/1000.0) )" );
	reco_filtered = reco_filtered.Define( "DiMuonMass",																		"Float_t( DiLept_M[0] )" );
	reco_filtered = reco_filtered.Define( "DiMuonTau",																		"Float_t( DiMuonVertex_Tau[0] )" );
	reco_filtered = reco_filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	reco_filtered = reco_filtered.Define( "DiMuonPt",																			"Float_t( DiLept_Pt[0]/1000.0 )" );
	reco_filtered = reco_filtered.Define( "PhotonPt",																			"Float_t( Photon_Pt[0]/1000.0 )" );
	reco_filtered = reco_filtered.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	reco_filtered = reco_filtered.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
 	reco_filtered = reco_filtered.Define( "qxsum", 																				"Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	reco_filtered = reco_filtered.Define( "qysum", 																				"Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	reco_filtered = reco_filtered.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	reco_filtered = reco_filtered.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	reco_filtered = reco_filtered.Define( "qtA",																					"Float_t( DiMuonPt - PhotonPt )" );
	reco_filtered = reco_filtered.Define( "qtB",																					"Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	reco_filtered = reco_filtered.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	reco_filtered = reco_filtered.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	reco_filtered = reco_filtered.Define( "JPsi_Eta",																			"Float_t( DiLept_Eta[0] )" );
	reco_filtered = reco_filtered.Define( "Phot_Eta",																			"Float_t( Photon_Eta[0] )" );
  reco_filtered = reco_filtered.Define( "MuPos_Eta",                                    "Float_t( MuPlus_Eta[0] )" );
  reco_filtered = reco_filtered.Define( "MuNeg_Eta",                                    "Float_t( MuMinus_Eta[0] )" );
	reco_filtered = reco_filtered.Define( "DeltaZ0",																			"Float_t( DiMuon_DeltaZ0[0] )" );
  reco_filtered = reco_filtered.Define( "Y",																			      "Float_t( MuMuY_Y[0] )" );
	reco_filtered = reco_filtered.Define( "Q",																			      "Float_t( MuMuY_M[0]/1000.0 )" );

  // photon quality and dr cut
  reco_filtered = reco_filtered.Filter( "Photon_quality[0] <= 1" );
	auto photon_dr_lambda = photon_dr_calc;
	reco_filtered = reco_filtered.Define( "phot_dR", photon_dr_lambda, { "Photon_Pt", "Photon_Eta", "Photon_Phi", "Photon_E", "mcPhoton_Pt", "mcPhoton_Eta", "mcPhoton_Phi" } ); 
  reco_filtered = reco_filtered.Filter( selections->get_bound( "phot_dR" ).get_cut() );

	// apply reductions from vector to float and apply cut
	for ( bound & cut_bound : reco_vector_bounds ){
		reco_filtered = reco_filtered.Redefine( cut_bound.get_var(),  []( const ROOT::RVecF &val ){ return val[0]; }, { cut_bound.get_var() } );
		reco_filtered = reco_filtered.Filter( cut_bound.get_cut() );
  }
  
	for ( bound & cut_bound : reco_float_bounds ){
    //if ( cut_bound.get_name().find( "lambda" ) != std::string::npos  ){ continue; } 
		reco_filtered = reco_filtered.Filter( cut_bound.get_cut() );
	}

   
  // filter reco for dimuon tau
  bound dz_bound = selections->get_bound( "DeltaZ0" );
	reco_filtered = reco_filtered.Filter( dz_bound.get_cut() );


  // two base operations before adding the redefinitions and cuts
  auto truth_filtered = input_frame.Filter( "HLT_2mu4_bJpsimumu_noL2 != 0" );
  truth_filtered = truth_filtered.Filter( "mcPhoton_Pt.size() != 0" );
  truth_filtered = truth_filtered.Filter( "mcMuMinus_Pt.size() != 0" );
  truth_filtered = truth_filtered.Filter( "mcMuPlus_Pt.size() != 0" );

	// manual redefinitions
	truth_filtered = truth_filtered.Define( "ActIpX", 																			"Float_t( actIpX )" );	
	truth_filtered = truth_filtered.Define( "AvgIpX", 																			"Float_t( avgIpX )" );	
	truth_filtered = truth_filtered.Define( "costheta", 																		"Float_t( Truth_MuMuGamma_CS_CosTheta[0] )" );
	truth_filtered = truth_filtered.Define( "AbsCosTheta", 																	"Float_t( abs(costheta) )" );
	truth_filtered = truth_filtered.Define( "DPhi",                                         dphi_lambda, { "mcDiLept_Phi", "mcPhoton_Phi" } );
	truth_filtered = truth_filtered.Define( "AbsdPhi", 																			"Float_t( abs(DPhi) )" );
	truth_filtered = truth_filtered.Define( "Phi",                                          phi_lambda, {"Truth_MuMuGamma_CS_Phi"} );
	truth_filtered = truth_filtered.Define( "AbsPhi", 																			"Float_t( sqrt( Truth_MuMuGamma_CS_Phi[0] * Truth_MuMuGamma_CS_Phi[0]) )" );
	truth_filtered = truth_filtered.Define( "DY", 																					"Float_t( mcDiLept_Eta[0] - mcPhoton_Eta[0])" );
	truth_filtered = truth_filtered.Define( "AbsdY", 																				"Float_t( abs(DY) )" );
	truth_filtered = truth_filtered.Define( "EventNumber",																	"Float_t( evt )" );
	truth_filtered = truth_filtered.Define( "Lambda", 																			"Float_t( (mcMuMuY_M[0]/3097.0) * (mcMuMuY_M[0]/3097.0) )" );
	truth_filtered = truth_filtered.Define( "qTSquared",																		"Float_t( (mcMuMuY_Pt[0]/1000.0) * (mcMuMuY_Pt[0]/1000.0) )" );
	truth_filtered = truth_filtered.Define( "DiMuonMass",																		"Float_t( mcDiLept_M[0] )" );
	truth_filtered = truth_filtered.Define( "DiMuonTau",																		"Float_t( 0 )" );
	truth_filtered = truth_filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	truth_filtered = truth_filtered.Define( "DiMuonPt",																			"Float_t( mcDiLept_Pt[0]/1000.0 )" );
	truth_filtered = truth_filtered.Define( "PhotonPt",																			"Float_t( mcPhoton_Pt[0]/1000.0 )" );
	truth_filtered = truth_filtered.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
 	truth_filtered = truth_filtered.Define( "qxsum", 																				"Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	truth_filtered = truth_filtered.Define( "qysum", 																				"Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	truth_filtered = truth_filtered.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qtA",																					"Float_t( DiMuonPt - PhotonPt )" );
	truth_filtered = truth_filtered.Define( "qtB",																					"Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	truth_filtered = truth_filtered.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	truth_filtered = truth_filtered.Define( "JPsi_Eta",																			"Float_t( mcDiLept_Eta[0] )" );
	truth_filtered = truth_filtered.Define( "Phot_Eta",																			"Float_t( mcPhoton_Eta[0] )" );
  truth_filtered = truth_filtered.Define( "MuPos_Eta",                                    "Float_t( mcMuPlus_Eta[0] )" );
  truth_filtered = truth_filtered.Define( "MuNeg_Eta",                                    "Float_t( mcMuMinus_Eta[0] )" );
	truth_filtered = truth_filtered.Define( "DeltaZ0",																			"Float_t( 0 )" );
  truth_filtered = truth_filtered.Define( "qx",                                           "Float_t( sqrt(qTSquared)*cos( mcMuMuY_Phi[0] ) )" );
  truth_filtered = truth_filtered.Define( "qy",                                           "Float_t( sqrt(qTSquared)*sin( mcMuMuY_Phi[0] ) )" );


	// apply reductions from vector to float and apply cut
	for ( bound & cut_bound : truth_vector_bounds ){
		truth_filtered = truth_filtered.Redefine( cut_bound.get_var(),  []( const ROOT::RVecF &val ){ return val[0]; }, { cut_bound.get_var() } );
		truth_filtered = truth_filtered.Filter( cut_bound.get_cut() );
  }

	for ( bound & cut_bound : truth_float_bounds ){
		truth_filtered = truth_filtered.Filter( cut_bound.get_cut() );
	}

  if ( weight_var.find( "pu_weight" ) != std::string::npos ){
    std::cout << "Weighting efficiencies by " << weight_var << std::endl;
    reco_filtered = reco_filtered.Define( "weight", { weight_var.c_str() } );
	  truth_filtered = truth_filtered.Define( "weight", { weight_var.c_str() } );
  } else if ( !weight_var.empty() ){
    std::cout << "Weighting efficiencies by " << weight_var << std::endl;
    TFile * weight_file = new TFile( Form( "./weights/%s_weights.root", unique.c_str() ), "READ" );
    TH1F * weight_hist = (TH1F *) weight_file->Get( Form( "weights_%s", weight_var.c_str() ) );
    auto weight_lambda = [weight_hist]( Float_t var ){ return weight_hist->GetBinContent( weight_hist->FindBin( var ) ); };
		reco_filtered = reco_filtered.Define( "weight",																			weight_lambda, { weight_var.c_str() }   );
	  truth_filtered = truth_filtered.Define( "weight",																		weight_lambda, { weight_var.c_str() }	 );
  } else {
	  reco_filtered = reco_filtered.Define( "weight",																			"Float_t( 1.0 )" );
	  truth_filtered = truth_filtered.Define( "weight",																		"Float_t( 1.0 )" );
  }

	
  //output trees
  std::string output_file = Form( "./efficiency/%s_fiducials_%s.root", type.c_str(), unique.c_str() );
  ROOT::RDF::RSnapshotOptions opts;
  opts.fMode = "UPDATE";
  std::vector< std::string > reco_output_columns = output_columns;
  reco_output_columns.push_back( "Y" );
  reco_output_columns.push_back( "Q" );
  reco_filtered.Snapshot( "reco_tree", output_file, reco_output_columns );
  truth_filtered.Snapshot( "truth_tree", output_file, output_columns, opts );

}
  //std::string output_filepath = Form( "./trees_%s.root", unique.c_str() );
  //stage_fileset * fileset = new stage_fileset( unique.c_str(), output_filepath );
  //fileset->load_file();
  //reco_filtered.Snapshot( "reco_tree", output_filepath, output_columns, opts );
  //truth_filtered.Snapshot( "truth_tree", output_filepath, output_columns, opts );

  //bound_mgr * eff_selections = new bound_mgr() ;
  //eff_selections->load_bound_mgr( hf_binning );

  //// open tree file manualy
  //TFile * efficiency_trees = new TFile( output_file.c_str(), "UPDATE" );
  //efficiency_trees->cd();
  //TTree * reco_tree = (TTree*) efficiency_trees->Get( "reco_tree" );
  //TTree * truth_tree = (TTree*) efficiency_trees->Get( "truth_tree" );

  //std::vector< std::string > efficiency_vars = { "qtA", "qtB", "Phi", "Lambda", "DiMuonPt", "PhotonPt" };
  //std::vector< std::string > mass_vars = { "Q0", "Q3", "Q4", "Q5", "Q12", "T0", "T3", "T4", "T5", "T12" };


  //for ( std::string & mass : mass_vars ){


  //  bound mass_bound = eff_selections->get_bound( mass );

  //  for ( std::string & var : efficiency_vars ){


  //    bound eff_bound = eff_selections->get_bound( var );
  //    int bins = eff_bound.get_bins();
  //    float min = eff_bound.get_min();
  //    float max = eff_bound.get_max();
  //    std::string reco_hist_name =  Form( "reco_%s_%s", var.c_str(), mass.c_str() );
  //    std::string truth_hist_name =  Form( "truth_%s_%s", var.c_str(), mass.c_str() );
  //    std::string eff_hist_name =  Form( "eff_%s_%s", var.c_str(), mass.c_str() );
  //    TH1F * reco_hist    = new TH1F( reco_hist_name.c_str(),   "", bins, min, max );
  //    TH1F * truth_hist   = new TH1F( truth_hist_name.c_str(),  "", bins, min, max );
  //    TH1F * eff_hist    = new TH1F( eff_hist_name.c_str(),   "", bins, min, max );
  //    std::string weight_with_cut = Form( "weight*(%s)", mass_bound.get_cut().c_str() );
  //    reco_tree->Draw( Form( "%s>>%s", var.c_str(), reco_hist_name.c_str() ), weight_with_cut.c_str(), "goff");
  //    truth_tree->Draw( Form( "%s>>%s", var.c_str(), truth_hist_name.c_str() ), weight_with_cut.c_str(), "goff");
  //    eff_hist->Divide( reco_hist, truth_hist, 1.0, 1.0, "B" ); 
  //    //fileset->efficiency_dir->cd();
  //    efficiency_trees->cd();
  //    reco_hist->Write();
  //    truth_hist->Write();
  //    eff_hist->Write();
  //  }
  //}

  //efficiency_trees->Close();
  //delete efficiency_trees;


