#include <make_sf.hxx>

void make_sf( const std::string & file_path, sample_type type, const std::string & unique_str, bound_mgr * selections ){

  if ( type != data ){ 
    std::cout << "ScaleFactor valid only for data, returning" << std::endl;
    return;
  }

	ROOT::EnableImplicitMT( 6 );

  const char * unique = unique_str.c_str();



  scalefactor sf;
  sf.load_muon_SF();
  sf.load_photon_SF();

  TChain chain( "tree" );
  chain.Add( file_path.c_str() );
  ROOT::RDataFrame input_frame( chain );
  std::string output_file = Form( "./scalefactors/sf_%s_%s.root", type_c[type], unique );

    
	std::vector< std::string > output_columns = { "ActIpX", "AvgIpX", "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
															 "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
															 "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
															 "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0", "qx", "qy", "photon_id_sf", "photon_id_sf_err", 
                               "mu_pl_sf", "mu_mi_sf", "mu_dr_sf", "SF" };

  std::vector< const char * > vector_cuts = { "mu_pos_pt", "mu_neg_pt" };
	std::vector< const char * > float_cuts = { "lambda", "costheta", "qtSquared", "AbsPhi", 
                                            "DiMuonMass", "DiMuonTau", "qtA", "qtB", 
                                            "DiMuonPt", "DeltaZ0", "Phot_Eta", "MuPos_Eta", "MuNeg_Eta", "PhotonPt" };

  //std::for_each( vector_cuts.begin(), vector_cuts.end(), [&vector_bounds, &selections ]( const std::string & element ){ vector_bounds.push_back( selections->get_bound( element ) ); } );
  //std::for_each( float_cuts.begin(), float_cuts.end(), [&float_bounds, &selections ]( const std::string & element ){ float_bounds.push_back( selections->get_bound( element ) ); } );


  //for ( const char * & bound_str : vector_cuts ){
  //  vector_bounds.push_back( selections->get_bound( bound_str ) );
  //}

	//for ( const char * & bound_str : float_cuts ){
  //  float_bounds.push_back( selections->get_bound( bound_str ) );
  //}
 

  std::vector< bound > vector_bounds, float_bounds; 
  std::transform( vector_cuts.begin(), vector_cuts.end(), std::back_inserter( vector_bounds ), [&selections]( const std::string & element ){ return selections->get_bound( element ); } );
  std::transform( float_cuts.begin(), float_cuts.end(), std::back_inserter( float_bounds ), [&selections]( const std::string & element ){ return selections->get_bound( element ); } );

  auto filtered = input_frame.Filter( "Photon_Pt.size() != 0" );
  filtered = filtered.Filter( "HLT_2mu4_bJpsimumu_noL2 != 0" );

	// manual redefinitions
	auto phi_lambda = phi_calc;
	auto dphi_lambda = dphi_calc;
  auto sf_lambda = [ &sf ](  const ROOT::RVecF & photon_pt, const ROOT::RVecF &photon_eta  ){ return sf.photon_SF( photon_pt[0], photon_eta[0] ); };
  auto sf_error_lambda = [ &sf ](  const ROOT::RVecF & photon_pt, const ROOT::RVecF &photon_eta  ){ return sf.photon_SF_uncertainty( photon_pt[0], photon_eta[0] ); };
  auto sf_mu = [ &sf ]( const ROOT::RVecF & mu_pt, const ROOT::RVecF & mu_eta ){ return sf.single_muon_SF( mu_pt[0]*1e-3, +1.0*mu_eta[0]*1e-3, dataset::Data15, single_mu_leg::mu4); };
  auto sf_mu_dr = [ &sf ]( const ROOT::RVecF & dr ){ return sf.muon_dr_SF ( dr[0], 0.0, dataset::Data15, dimu_trig::bDimu_noL2) ; };

	filtered = filtered.Define( "ActIpX", 												  "Float_t( actIpX )" );	
	filtered = filtered.Define( "AvgIpX", 												  "Float_t( avgIpX )" );	
	filtered = filtered.Define( "costheta", 											  "Float_t( MuMuGamma_CS_CosTheta[0] )" );
	filtered = filtered.Define( "AbsCosTheta", 										  "Float_t( abs(costheta) )" );
	filtered = filtered.Define( "DPhi",                             dphi_lambda, { "DiLept_Phi", "Photon_Phi" } );
	filtered = filtered.Define( "AbsdPhi", 												  "Float_t( abs(DPhi) )" );
	filtered = filtered.Define( "Phi",                              phi_lambda, {"MuMuGamma_CS_Phi"} );
	filtered = filtered.Define( "AbsPhi", 												  "Float_t( sqrt( MuMuGamma_CS_Phi[0] * MuMuGamma_CS_Phi[0]) )" );
	filtered = filtered.Define( "DY", 														  "Float_t(DiLept_Y[0] - Photon_Eta[0])" );
	filtered = filtered.Define( "AbsdY", 													  "Float_t( abs(DY) )" );
	filtered = filtered.Define( "EventNumber",										  "Float_t( evt )" );
	filtered = filtered.Define( "Lambda", 												  "Float_t( (MuMuY_M[0]/3097.0)*(MuMuY_M[0]/3097.0) )" );
	filtered = filtered.Define( "qTSquared",											  "Float_t( (MuMuY_Pt[0]/1000.0) * (MuMuY_Pt[0]/1000.0) )" );
	filtered = filtered.Define( "DiMuonMass",											  "Float_t( DiLept_M[0] )" );
	filtered = filtered.Define( "DiMuonTau",											  "Float_t( DiMuonVertex_Tau[0] )" );
	filtered = filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",  "Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	filtered = filtered.Define( "DiMuonPt",												  "Float_t( DiLept_Pt[0]/1000.0 )" );
	filtered = filtered.Define( "PhotonPt",												  "Float_t( Photon_Pt[0]/1000.0 )" );
	filtered = filtered.Define( "qxpsi",													  "Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	filtered = filtered.Define( "qypsi",													  "Float_t( (PhotonPt)*sin(DPhi) )" );
 	filtered = filtered.Define( "qxsum", 													  "Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	filtered = filtered.Define( "qysum", 													  "Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	filtered = filtered.Define( "qxgamma",												  "Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	filtered = filtered.Define( "qygamma",												  "Float_t( (DiMuonPt)*sin(DPhi) )" );
	filtered = filtered.Define( "qtA",														  "Float_t( DiMuonPt - PhotonPt )" );
	filtered = filtered.Define( "qtB",														  "Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	filtered = filtered.Define( "qtL",														  "Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	filtered = filtered.Define( "qtM",														  "Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	filtered = filtered.Define( "JPsi_Eta",												  "Float_t( DiLept_Eta[0] )" );
	filtered = filtered.Define( "Phot_Eta",												  "Float_t( Photon_Eta[0] )" );
  filtered = filtered.Define( "MuPos_Eta",                        "Float_t( MuPlus_Eta[0] )" );
  filtered = filtered.Define( "MuNeg_Eta",                        "Float_t( MuMinus_Eta[0] )" );
	filtered = filtered.Define( "DeltaZ0",												  "Float_t( DiMuon_DeltaZ0[0] )" );
  filtered = filtered.Define( "qx",                               "Float_t( sqrt(qTSquared)*cos(Phi) )" );
  filtered = filtered.Define( "qy",                               "Float_t( sqrt(qTSquared)*sin(Phi) )" );
  filtered = filtered.Define( "photon_id_sf",                     sf_lambda, { "Photon_Pt", "Photon_Eta" } );
  filtered = filtered.Define( "photon_id_sf_err",                 sf_error_lambda, { "Photon_Pt", "Photon_Eta" } );
  filtered = filtered.Define( "mu_pl_sf",                         sf_mu, { "MuPlus_Pt", "MuPlus_Eta" } );      
  filtered = filtered.Define( "mu_mi_sf",                         sf_mu, { "MuMinus_Pt", "MuMinus_Eta" } );      
  filtered = filtered.Define( "mu_dr_sf",                         sf_mu_dr, { "DiLept_dR" } );      
  filtered = filtered.Define( "SF",                               "mu_pl_sf*mu_mi_sf*mu_dr_sf" );      


  for ( bound & cut_bound : vector_bounds ){
		filtered = filtered.Redefine( cut_bound.get_var(),  []( const ROOT::RVecF &val ){ return val[0]; }, { cut_bound.get_var() } );
		filtered = filtered.Filter( cut_bound.get_cut() );
  }

	for ( bound & cut_bound : float_bounds ){ filtered = filtered.Filter( cut_bound.get_cut() ); }

  std::cout << output_file << std::endl;

  filtered.Snapshot( "tree", output_file, output_columns );

}
