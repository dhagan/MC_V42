#include <migration.hxx>

void migration( const std::string file_path, const std::string type,
                const std::string unique, const std::string cutflow_var,
                bound_mgr * selections ){

  if ( !cutflow_var.empty() ){ std::cout << cutflow_var << std::endl; }
  
  std::string output_filepath = "./migration/" + type + "_" + unique + "_migration.root";
  TFile * input_file = new TFile( file_path.c_str(), "READ" );
  TFile * output_file = new TFile( output_filepath.c_str(), "RECREATE" );

  TTree * input_tree = (TTree *) input_file->Get( "tree" );
  TTree * output_tree = new TTree( Form( "%s_migration", type.c_str() ), Form( "%s_tree", type.c_str() ) );

  UInt_t in_event, in_HLT_2mu4_bJpsimumu_noL2;
  float in_ActIpX, in_AvgIpX;
  std::vector< int > * in_Photon_quality{0};
  std::vector< float > * in_Truth_MuMuGamma_CS_CosTheta{0}, * in_MuMuGamma_CS_CosTheta{0}, * in_mcPhoton_Pt{0},
                       * in_Photon_Pt{0}, * in_Photon_Phi{0}, * in_Photon_E{0}, * in_MuMinus_Pt{0},
                       * in_mcMuMinus_Pt{0}, * in_MuMuY_M{0}, * in_mcMuPlus_Pt{0}, * in_MuPlus_Pt{0}, 
                       * in_mcMuMuY_M{0}, * in_MuMuY_Pt{0}, * in_mcMuMuY_Pt{0},
                       * in_MuMuGamma_CS_Phi{0}, * in_Truth_MuMuGamma_CS_Phi{0}, * in_DiLept_M{0}, * in_DiLept_Y{0},
                       * in_DiLept_Phi{0}, * in_DiLept_Pt{0}, * in_DiMuonVertex_Tau{0}, * in_DiLept_Eta{0}, 
                       * in_Photon_Eta{0}, * in_mcPhoton_Eta{0}, * in_mcPhoton_Phi{0}, * in_mcDiLept_Pt{0},
                       * in_DiMuon_DeltaZ0{0}, * in_DiMuonVertex_Muon0_Px{0}, * in_DiMuonVertex_Muon0_Py{0}, 
                       * in_DiMuonVertex_Muon0_Pz{0}, * in_DiMuonVertex_Muon1_Px{0}, * in_DiMuonVertex_Muon1_Py{0}, 
                       * in_DiMuonVertex_Muon1_Pz{0}, * in_MuPlus_M{0}, * in_MuMinus_M{0}, * in_mcDiLept_M{0},
                       * in_mcDiLept_Phi{0};

  input_tree->SetBranchAddress("actIpX", &in_ActIpX );
  input_tree->SetBranchAddress("avgIpX", &in_AvgIpX );
  input_tree->SetBranchAddress("evt", &in_event );
  input_tree->SetBranchAddress("Truth_MuMuGamma_CS_CosTheta", &in_Truth_MuMuGamma_CS_CosTheta );
  input_tree->SetBranchAddress("Photon_quality", &in_Photon_quality );
  input_tree->SetBranchAddress("MuMuGamma_CS_CosTheta", &in_MuMuGamma_CS_CosTheta );
  input_tree->SetBranchAddress("mcPhoton_Pt", &in_mcPhoton_Pt );
  input_tree->SetBranchAddress("Photon_Pt", &in_Photon_Pt );
  input_tree->SetBranchAddress("Photon_Phi", &in_Photon_Phi );
  input_tree->SetBranchAddress("Photon_E", &in_Photon_E );
  input_tree->SetBranchAddress("MuMinus_Pt", &in_MuMinus_Pt );
  input_tree->SetBranchAddress("mcMuMinus_Pt", &in_mcMuMinus_Pt );
  input_tree->SetBranchAddress("mcMuPlus_Pt", &in_mcMuPlus_Pt );
  input_tree->SetBranchAddress("MuPlus_Pt", &in_MuPlus_Pt );
  input_tree->SetBranchAddress("MuMuY_M", &in_MuMuY_M );
  input_tree->SetBranchAddress("mcMuMuY_M", &in_mcMuMuY_M );
  input_tree->SetBranchAddress("mcDiLept_M", &in_mcDiLept_M );
  input_tree->SetBranchAddress("mcDiLept_Phi", &in_mcDiLept_Phi );
  input_tree->SetBranchAddress("MuMuY_Pt", &in_MuMuY_Pt );
  input_tree->SetBranchAddress("mcMuMuY_Pt", &in_mcMuMuY_Pt );
  input_tree->SetBranchAddress("MuMuGamma_CS_Phi", &in_MuMuGamma_CS_Phi );
  input_tree->SetBranchAddress("Truth_MuMuGamma_CS_Phi", &in_Truth_MuMuGamma_CS_Phi );
  input_tree->SetBranchAddress("DiLept_M", &in_DiLept_M );
  input_tree->SetBranchAddress("DiLept_Y", &in_DiLept_Y );
  input_tree->SetBranchAddress("DiLept_Phi", &in_DiLept_Phi );
  input_tree->SetBranchAddress("DiLept_Pt", &in_DiLept_Pt );
  input_tree->SetBranchAddress("DiMuonVertex_Tau", &in_DiMuonVertex_Tau );
  input_tree->SetBranchAddress("HLT_2mu4_bJpsimumu_noL2", &in_HLT_2mu4_bJpsimumu_noL2 );
  input_tree->SetBranchAddress("DiLept_Eta", &in_DiLept_Eta );
  input_tree->SetBranchAddress("Photon_Eta", &in_Photon_Eta );
  input_tree->SetBranchAddress("mcPhoton_Eta", &in_mcPhoton_Eta );
  input_tree->SetBranchAddress("mcPhoton_Phi", &in_mcPhoton_Phi );
  input_tree->SetBranchAddress("mcDiLept_Pt", &in_mcDiLept_Pt );
  input_tree->SetBranchAddress("DiMuon_DeltaZ0", &in_DiMuon_DeltaZ0 );
  // obj store
  input_tree->SetBranchAddress("DiMuonVertex_Muon0_Px", &in_DiMuonVertex_Muon0_Px );
  input_tree->SetBranchAddress("DiMuonVertex_Muon0_Py", &in_DiMuonVertex_Muon0_Py );
  input_tree->SetBranchAddress("DiMuonVertex_Muon0_Pz", &in_DiMuonVertex_Muon0_Pz );
  input_tree->SetBranchAddress("DiMuonVertex_Muon1_Px", &in_DiMuonVertex_Muon1_Px );
  input_tree->SetBranchAddress("DiMuonVertex_Muon1_Py", &in_DiMuonVertex_Muon1_Py );
  input_tree->SetBranchAddress("DiMuonVertex_Muon1_Pz", &in_DiMuonVertex_Muon1_Pz );
  input_tree->SetBranchAddress("MuPlus_M", &in_MuPlus_M );
  input_tree->SetBranchAddress("MuMinus_M", &in_MuMinus_M );

  //float AbsCosTheta, AbsdPhi, AbsdY, DPhi, DY, Phi, costheta, AbsPhi, EventNumber, Lambda, qTSquared, DiMuonMass, 
  //      DiMuonTau, DiMuonPt, PhotonPt, Trigger_HLT_2mu4_bJpsimumu_noL2, qxpsi, qypsi, qxgamma, qygamma, qxsum, 
  //      qysum, qtA, qtB, qtL, qtM, phot_dR, ActIpX, AvgIpX, JPsi_Eta, Phot_Eta, DeltaZ0;
 
  Long64_t n_entries = input_tree->GetEntries();

  bool reco_pass, truth_pass;
  float Lambda;
  float tr_lambda, tr_qta, tr_qtb, tr_dimu_pt, tr_ph_pt;
  float re_lambda, re_qta, re_qtb, re_dimu_pt, re_ph_pt;

  output_tree->Branch( "reco_pass",     &reco_pass );
  output_tree->Branch( "truth_pass",    &truth_pass );
  output_tree->Branch( "tr_lambda",     &tr_lambda );
  output_tree->Branch( "tr_qta",        &tr_qta );
  output_tree->Branch( "tr_qtb",        &tr_qtb );
  output_tree->Branch( "tr_dimu_pt",      &tr_dimu_pt );
  output_tree->Branch( "tr_ph_pt",      &tr_ph_pt );
  output_tree->Branch( "re_lambda",     &re_lambda );
  output_tree->Branch( "re_qta",        &re_qta );
  output_tree->Branch( "re_qtb",        &re_qtb );
  output_tree->Branch( "re_dimu_pt",      &re_dimu_pt );
  output_tree->Branch( "re_ph_pt",      &re_ph_pt );
  output_tree->Branch( "Lambda",        &Lambda );

  //selections.get_bound( "lambda" );
  bound photon_bound = selections->get_bound( "Photon_pt" );
  bound muon_bound = selections->get_bound( "muon_pt" );

  float photon_pt_min     = photon_bound.get_min();
  float mu_pt_min         = muon_bound.get_min();

  bound lambda_bound      = selections->get_bound( "lambda" );
  bound jpsi_mass_bound   = selections->get_bound( "jpsi_mass" );  
  bound dimuon_tau_bound  = selections->get_bound( "dimuon_tau" );  
  bound phi_bound         = selections->get_bound( "abs_phi_cs" );
  bound qta_bound         = selections->get_bound( "qTA" );
  bound qtb_bound         = selections->get_bound( "qTB" );
  bound qt2_bound         = selections->get_bound( "qT2" );

  std::cout << "" << std::endl;
  std::cout << "Looping over " << n_entries << " entries" << std::endl;

  for ( Long64_t entry = 0; entry < n_entries; entry++ ){

    input_tree->GetEntry( entry );

    truth_pass = true;
    reco_pass = true;

    if ( in_HLT_2mu4_bJpsimumu_noL2   == 0 ){ continue; }
    
    if ( in_mcPhoton_Pt->size()       == 0 ){ truth_pass = false; }
    if ( in_mcMuMinus_Pt->size()      == 0 ){ truth_pass = false; }
	  if ( in_mcMuPlus_Pt->size()       == 0 ){ truth_pass = false; }

    if ( truth_pass ){

      if ( abs( in_mcPhoton_Pt->at(0)/1000.0 )  <  photon_pt_min ){ truth_pass = false; }
	    if ( abs( in_mcMuMinus_Pt->at(0)/1000.0 ) < mu_pt_min ){ truth_pass = false; }
	    if ( abs( in_mcMuPlus_Pt->at(0)/1000.0 )  < mu_pt_min ){ truth_pass = false; }

      tr_lambda = ( in_mcMuMuY_M->at(0)/3097.0 ) * ( in_mcMuMuY_M->at(0)/3097.0 );
      if ( tr_lambda < lambda_bound.get_min() || tr_lambda > lambda_bound.get_max() ){ truth_pass = false; }
      if ( in_mcDiLept_M->at(0)/1000.0 < jpsi_mass_bound.get_min() ){ truth_pass = false; }
      if ( in_mcDiLept_M->at(0)/1000.0 > jpsi_mass_bound.get_max() ){ truth_pass = false; };

      float tr_abs_phi = abs( in_Truth_MuMuGamma_CS_Phi->at(0) );
      if ( tr_abs_phi < phi_bound.get_min() || tr_abs_phi > phi_bound.get_max() ){ truth_pass = false; }

	    double mcdPhi = in_mcDiLept_Phi->at(0) - in_mcPhoton_Phi->at(0) ;
      while( mcdPhi > M_PI ){ mcdPhi -= 2*M_PI; }
	    while( mcdPhi < -M_PI ){ mcdPhi += 2*M_PI; }

      tr_qta = ( in_mcDiLept_Pt->at(0)/1000.0 ) - ( in_mcPhoton_Pt->at(0)/1000.0 );
      tr_qtb = sqrt( in_mcDiLept_Pt->at(0)/1000.0 * in_mcPhoton_Pt->at(0)/1000.0) * sin( mcdPhi );

      tr_ph_pt = in_mcPhoton_Pt->at( 0 );
      tr_dimu_pt = in_mcDiLept_Pt->at( 0 ); 
    }

    //reco section
    if ( in_Photon_Pt->size()      ==  0 ){ reco_pass = false; }
    if ( in_MuMinus_Pt->size()     ==  0 ){ reco_pass = false; }
    if ( in_MuPlus_Pt->size()      ==  0 ){ reco_pass = false; }

    if ( reco_pass ){
      if ( abs( in_Photon_Pt->at(0)/1000.0 )  < photon_pt_min ){ reco_pass = false; }
      if ( abs( in_MuMinus_Pt->at(0)/1000.0 ) < mu_pt_min    ){ reco_pass = false; }
      if ( abs( in_MuPlus_Pt->at(0)/1000.0 )  < mu_pt_min    ){ reco_pass = false; }

      re_lambda = ( in_MuMuY_M->at(0)/3097.0 ) * ( in_MuMuY_M->at(0)/3097.0 );
      if ( re_lambda < lambda_bound.get_min() || re_lambda > lambda_bound.get_max()  ){ reco_pass = false; }
      if ( in_DiLept_M->at(0)/1000.0 < jpsi_mass_bound.get_min() ){ reco_pass = false; }
      if ( in_DiLept_M->at(0)/1000.0 > jpsi_mass_bound.get_max() ){ reco_pass = false; }

      double re_abs_phi = abs( in_MuMuGamma_CS_Phi->at(0));
      if ( re_abs_phi < phi_bound.get_min() || re_abs_phi > phi_bound.get_max() ){ reco_pass = false; }

      double dPhi = ( in_DiLept_Phi->at(0) - in_Photon_Phi->at(0));
      while( dPhi > M_PI ){ dPhi -= 2*M_PI; }
      while( dPhi < -M_PI ){ dPhi += 2*M_PI; }
      if (in_DiMuonVertex_Tau->at(0) < dimuon_tau_bound.get_min() ){ reco_pass = false; }
      if (in_DiMuonVertex_Tau->at(0) > dimuon_tau_bound.get_max() ){ reco_pass = false; }

      re_qta = ( in_DiLept_Pt->at(0)/1000.0 - in_Photon_Pt->at(0)/1000.0 );
      re_qtb = sqrt( in_DiLept_Pt->at(0)/1000.0 * in_Photon_Pt->at(0)/1000.0 ) * sin( dPhi );

      if ( in_Photon_quality->at(0) > 1 ){ reco_pass = false; }
      TLorentzVector tlv_truth_photon, tlv_reco_photon, tlv_truth_muplus, tlv_reco_muplus;
      tlv_reco_photon.SetPtEtaPhiE( in_Photon_Pt->at(0), in_Photon_Eta->at(0), in_Photon_Phi->at(0), in_Photon_E->at(0) );
      tlv_truth_photon.SetPtEtaPhiM( in_mcPhoton_Pt->at(0), in_mcPhoton_Eta->at(0), in_mcPhoton_Phi->at(0),0.);
      Float_t PhotonDRTruthReco  = tlv_reco_photon.DeltaR( tlv_truth_photon );
      if ( PhotonDRTruthReco > 0.12 ){  reco_pass = false; }

      re_ph_pt = in_Photon_Pt->at( 0 );
      re_dimu_pt = in_DiLept_Pt->at( 0 ); 


    }


    if ( !reco_pass ){
      re_qta    = -9999.9; 
      re_qtb    = -9999.9; 
      re_lambda = -9999.9; 
    }
    if ( !truth_pass ){
      tr_qta    = -9999.9; 
      tr_qtb    = -9999.9; 
      tr_lambda = -9999.9; 
    }

    if ( !truth_pass && !reco_pass ){
      continue;
    }
    
    Lambda = re_lambda;

    output_tree->Fill();
  }

  output_file->cd();
  output_tree->Write();
  output_file->Close();
  delete output_file;

  input_file->Close();
  delete input_file;

}
