#include <make_gen_ntuple.hxx>

void make_gen_ntuple( const std::string & file_path, const std::string & unique, bound_mgr * selections ){

  TChain chain( "truthTree" );
  chain.Add( file_path.c_str() );
  ROOT::RDataFrame input_frame( chain );

  auto dphi_lambda = dphi_calc;
  auto phi_lambda = phi_calc;

  auto output_frame = input_frame.Filter( "abs(mcDecPhoton_Eta) < 2.5" );
  output_frame      = output_frame.Filter( "abs(mcDecMuMi_Eta) < 2.4" );
  output_frame      = output_frame.Filter( "abs(mcDecMuPl_Eta) < 2.4" );

  output_frame      = output_frame.Define( "DPhi",                            dphi_lambda, { "mcDecDiLept_Phi", "mcDecPhoton_Phi" } );
  output_frame      = output_frame.Define( "qtA",                                         "Float_t( mcDecDiLept_Pt/1000.0 - mcDecPhoton_Pt/1000.0 )" );
	output_frame      = output_frame.Define( "qtB",                                         "Float_t( sqrt( (mcDecDiLept_Pt/1000.0)*(mcDecPhoton_Pt/1000.0) )*sin(DPhi) )" );
	output_frame      = output_frame.Define( "Phi",                             phi_lambda, {"truth_angleDec_cs_phi_mumugamma"} );
  output_frame      = output_frame.Define( "costheta", 																		"Float_t( truth_angleDec_cs_theta_mumugamma )" );
	output_frame      = output_frame.Define( "AbsCosTheta", 																"Float_t( abs(costheta) )" );
	output_frame      = output_frame.Define( "AbsdPhi", 																		"Float_t( abs(DPhi) )" );
	output_frame      = output_frame.Define( "AbsPhi", 																			"Float_t( abs( truth_angleDec_cs_phi_mumugamma ) )" );
	output_frame      = output_frame.Define( "DY", 																					"Float_t( mcDecDiLept_Rap - mcDecPhoton_Eta)" );
	output_frame      = output_frame.Define( "AbsdY", 																			"Float_t( abs(DY) )" );
	output_frame      = output_frame.Define( "Lambda", 																			"Float_t( (mcDecMuMuY_M/3097.0)*(mcDecMuMuY_M/3097.0) )" );
	output_frame      = output_frame.Define( "qTSquared",																		"Float_t( (mcDecMuMuY_Pt/1000.0) * (mcDecMuMuY_Pt/1000.0) )" );
	output_frame      = output_frame.Define( "DiMuonMass",																	"Float_t( mcDecDiLept_M )" );
	output_frame      = output_frame.Define( "DiMuonTau",																		"Float_t( 0 )" );
	output_frame      = output_frame.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( 1 )" );
	output_frame      = output_frame.Define( "DiMuonPt",																		"Float_t( mcDecDiLept_Pt/1000.0 )" );
	output_frame      = output_frame.Define( "PhotonPt",																		"Float_t( mcDecPhoton_Pt/1000.0 )" );
	output_frame      = output_frame.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	output_frame      = output_frame.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
 	output_frame      = output_frame.Define( "qxsum", 																			"Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	output_frame      = output_frame.Define( "qysum", 																			"Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	output_frame      = output_frame.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	output_frame      = output_frame.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	output_frame      = output_frame.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	output_frame      = output_frame.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	output_frame      = output_frame.Define( "JPsi_Eta",																		"Float_t( mcDecDiLept_Eta )" );
	output_frame      = output_frame.Define( "Phot_Eta",																		"Float_t( mcDecPhoton_Eta )" );
  output_frame      = output_frame.Define( "MuPos_Eta",                                   "Float_t( mcDecMuPl_Eta )" );
  output_frame      = output_frame.Define( "MuNeg_Eta",                                   "Float_t( mcDecMuMi_Eta )" );
  output_frame      = output_frame.Define( "MuPos_Pt",                                    "Float_t( mcDecMuPl_Pt/1000.0 )" );
  output_frame      = output_frame.Define( "MuNeg_Pt",                                    "Float_t( mcDecMuMi_Pt/1000.0 )" );
	output_frame      = output_frame.Define( "DeltaZ0",																			"Float_t( 0 )" );
  output_frame      = output_frame.Define( "qx",                                          "Float_t( sqrt(qTSquared)*cos( mcDecMuMuY_Phi ) )" );
  output_frame      = output_frame.Define( "qy",                                          "Float_t( sqrt(qTSquared)*sin( mcDecMuMuY_Phi ) )" );
  

	std::vector< std::string > cut_names = { "lambda", "costheta", "qtSquared", "AbsPhi", 
                                            "DiMuonMass", "DiMuonTau", "qtA", "qtB", 
                                            "DiMuonPt", "Phot_Eta", "MuPos_Eta", "MuNeg_Eta" };
  
  //bool generator = true;
  //std::vector< std::string > acceptance_cuts;
  //if ( generator ){
  //  acceptance_cuts = { "gen_photon_pt_generator", "gen_mu_pos_pt_generator", "gen_mu_neg_pt_generator" };
  //} else {
  //  acceptance_cuts = { "gen_photon_pt_acceptance", "gen_mu_pos_pt_acceptance", "gen_mu_neg_pt_acceptance" };
  //}
  //cut_names.insert( cut_names.end(), acceptance_cuts.begin(), acceptance_cuts.end() );

	std::vector< bound > float_bounds; 
	for ( std::string & bound_str : cut_names ){
    float_bounds.push_back( selections->get_bound( bound_str ) );
  }
  for ( bound & cut_bound : float_bounds ){
		output_frame = output_frame.Filter( cut_bound.get_cut() );
	}

  std::vector< std::string > output_columns = { "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
															 "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
															 "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
															 "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0", "qx", "qy", "MuPos_Pt", "MuNeg_Pt" };

  std::string output_file = Form( "./gen/gen_%s.root", unique.c_str() );
  output_frame.Snapshot( "tree", output_file.c_str(), output_columns );

}
