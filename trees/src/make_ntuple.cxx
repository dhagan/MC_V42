#include <make_ntuple.hxx>

#include <BphysEffTool.h>

void make_ntuple( const std::string & file_path, const std::string & type,
                  const std::string & unique, const std::string & cutflow_var,
                  bound_mgr * selections ){

	ROOT::EnableImplicitMT( 6 );

  //scalefactor sf;
  //sf.load_muon_SF();
  //sf.load_photon_SF();

  //Bphys::BphysEffTool * bphys_sf_tool = new Bphys::BphysEffTool();

  std::cout << "producing \"" << type << "\" ntuple." << std::endl;

	// Ready the input and output
  TChain chain( "tree" );
  chain.Add( file_path.c_str() );
  ROOT::RDataFrame input_frame( chain );
  std::string output_file = Form( "./%s/%s_%s.root", type.c_str(), type.c_str(), unique.c_str() );
  std::string sf_file = Form( "./%s/sf_%s_%s.root", type.c_str(), type.c_str(), unique.c_str() );

	// define cuts and output columns
	std::vector< std::string > output_columns = { "ActIpX", "AvgIpX", "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
															 "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
															 "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
															 "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0", "qx", "qy", "MuPos_Pt", "MuPos_Eta", "MuNeg_Pt", "MuNeg_Eta", "DiLept_DeltaR", "DiLept_Rap" };

	std::vector< std::string > vector_cuts = { "mu_pos_pt", "mu_neg_pt" };
	std::vector< std::string > float_cuts = { "lambda", "costheta", "qtSquared", "AbsPhi", 
                                            "DiMuonMass", "DiMuonTau", "qtA", "qtB", 
                                            "DiMuonPt", "DeltaZ0", "Phot_Eta", "MuPos_Eta", "MuNeg_Eta", "PhotonPt" };

	std::vector< bound > vector_bounds; 
  for ( std::string & bound_str : vector_cuts ){
    vector_bounds.push_back( selections->get_bound( bound_str ) );
  }

	std::vector< bound > float_bounds; 
	for ( std::string & bound_str : float_cuts ){
    float_bounds.push_back( selections->get_bound( bound_str ) );
  }

  // prepare cutflow object and hist structure, 
  bound cf_bound = selections->get_bound( cutflow_var );
  ROOT::RDF::TH1DModel cf_hist_model = { "cutflow_var", "", cf_bound.get_bins(), cf_bound.get_min(), cf_bound.get_max() };
  std::vector< std::string > all_cuts = { "photon_count", "trigger" };
  all_cuts.insert( all_cuts.end(), vector_cuts.begin(), vector_cuts.end() );
  all_cuts.insert( all_cuts.end(), float_cuts.begin(), float_cuts.end() );
  all_cuts.push_back( "Photon_quality" );
  if ( type.find( "sign" ) != std::string::npos ){
    all_cuts.push_back( "Truth_match" );
  }
  auto cut_delimit = []( std::string acc, std::string cut ){
    return  std::string( std::move( acc ) + ":" + cut );
  };
  std::string all_cut_string = std::accumulate( all_cuts.begin(), all_cuts.end(), std::string("no_cut"), cut_delimit ); 
  cutflow cf_selection( all_cut_string, true, cf_bound.get_var(), 0 ); 
  cf_selection.set_var_str( cutflow_var );

  
	// two base operations before adding the redefinitions and cuts
  cf_selection.set_cut( 0, *input_frame.Count() );
  auto filtered = input_frame.Filter( "Photon_Pt.size() != 0" );
  cf_selection.set_cut( 1, *filtered.Count() );
  filtered = filtered.Filter( "HLT_2mu4_bJpsimumu_noL2 != 0" );
  cf_selection.set_cut( 2, *filtered.Count() );
  
	// manual redefinitions
	auto phi_lambda = phi_calc;
	auto dphi_lambda = dphi_calc;

	filtered = filtered.Define( "ActIpX", 																			"Float_t( actIpX )" );	
	filtered = filtered.Define( "AvgIpX", 																			"Float_t( avgIpX )" );	
	filtered = filtered.Define( "costheta", 																		"Float_t( MuMuGamma_CS_CosTheta[0] )" );
	filtered = filtered.Define( "AbsCosTheta", 																	"Float_t( abs(costheta) )" );
	filtered = filtered.Define( "DPhi",                                         dphi_lambda, { "DiLept_Phi", "Photon_Phi" } );
	filtered = filtered.Define( "AbsdPhi", 																			"Float_t( abs(DPhi) )" );
	filtered = filtered.Define( "Phi",                                          phi_lambda, {"MuMuGamma_CS_Phi"} );
	filtered = filtered.Define( "AbsPhi", 																			"Float_t( sqrt( MuMuGamma_CS_Phi[0] * MuMuGamma_CS_Phi[0]) )" );
	filtered = filtered.Define( "DY", 																					"Float_t(DiLept_Y[0] - Photon_Eta[0])" );
	filtered = filtered.Define( "AbsdY", 																				"Float_t( abs(DY) )" );
	filtered = filtered.Define( "EventNumber",																	"Float_t( evt )" );
	filtered = filtered.Define( "Lambda", 																			"Float_t( (MuMuY_M[0]/3097.0)*(MuMuY_M[0]/3097.0) )" );
	filtered = filtered.Define( "qTSquared",																		"Float_t( (MuMuY_Pt[0]/1000.0) * (MuMuY_Pt[0]/1000.0) )" );
	filtered = filtered.Define( "DiMuonMass",																		"Float_t( DiLept_M[0] )" );
	filtered = filtered.Define( "DiMuonTau",																		"Float_t( DiMuonVertex_Tau[0] )" );
	filtered = filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	filtered = filtered.Define( "DiMuonPt",																			"Float_t( DiLept_Pt[0]/1000.0 )" );
	filtered = filtered.Define( "PhotonPt",																			"Float_t( Photon_Pt[0]/1000.0 )" );
	filtered = filtered.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	filtered = filtered.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
 	filtered = filtered.Define( "qxsum", 																				"Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	filtered = filtered.Define( "qysum", 																				"Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	filtered = filtered.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	filtered = filtered.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	filtered = filtered.Define( "qtA",																					"Float_t( DiMuonPt - PhotonPt )" );
	filtered = filtered.Define( "qtB",																					"Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	filtered = filtered.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	filtered = filtered.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	filtered = filtered.Define( "JPsi_Eta",																			"Float_t( DiLept_Eta[0] )" );
	filtered = filtered.Define( "Phot_Eta",																			"Float_t( Photon_Eta[0] )" );
  filtered = filtered.Define( "MuPos_Eta",                                    "Float_t( MuPlus_Eta[0] )" );
  filtered = filtered.Define( "MuNeg_Eta",                                    "Float_t( MuMinus_Eta[0] )" );
  filtered = filtered.Define( "MuPos_Pt",                                     "Float_t( MuPlus_Pt[0]/1000.0 )" );
  filtered = filtered.Define( "MuNeg_Pt",                                     "Float_t( MuMinus_Pt[0]/1000.0 )" );
  filtered = filtered.Define( "DiLept_Rap",                                   "Float_t( DiLept_Y[0] )" );
  filtered = filtered.Define( "DiLept_DeltaR",                                "Float_t( DiLept_dR[0] )" );
	filtered = filtered.Define( "DeltaZ0",																			"Float_t( DiMuon_DeltaZ0[0] )" );
  filtered = filtered.Define( "qx",                                           "Float_t( sqrt(qTSquared)*cos( MuMuY_Phi[0] ) )" );
  filtered = filtered.Define( "qy",                                           "Float_t( sqrt(qTSquared)*sin( MuMuY_Phi[0] ) )" );

  if ( chain.GetBranch( "pu_weight" ) ){
    output_columns.push_back( "pu_weight" );
  } else {
    filtered = filtered.Define( "pu_weight",                                  "Float_t( 1.0 )" );
    output_columns.push_back( "pu_weight" );
  }

  int current_cut = 3;

	// apply reductions from vector to float and apply cut
	for ( bound & cut_bound : vector_bounds ){
		filtered = filtered.Redefine( cut_bound.get_var(),  []( const ROOT::RVecF &val ){ return val[0]; }, { cut_bound.get_var() } );
		filtered = filtered.Filter( cut_bound.get_cut() );
    cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
    current_cut++;
  }

	for ( bound & cut_bound : float_bounds ){
		filtered = filtered.Filter( cut_bound.get_cut() );
    cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
    current_cut++;
	}

  filtered = filtered.Filter( "Photon_quality[0] <= 1" );
  cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
  current_cut++;

  if ( type.find( "sign" ) != std::string::npos ){
		auto photon_dr_lambda = photon_dr_calc;
		filtered = filtered.Define( "phot_dR", photon_dr_lambda, { "Photon_Pt", "Photon_Eta", "Photon_Phi", "Photon_E", "mcPhoton_Pt", "mcPhoton_Eta", "mcPhoton_Phi" } ); 
    filtered = filtered.Filter( selections->get_bound( "phot_dR" ).get_cut() );
    cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
    output_columns.push_back( "phot_dR" );
    current_cut++;
	}
  
	// output the filtered tree
	std::cout << output_file << std::endl;
  filtered.Snapshot( "tree", output_file, output_columns );

  cf_selection.write( (type + "_" + unique), cf_bound );

}
