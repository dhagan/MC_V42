
#include <make_truth_ntuple.hxx>

void make_truth_ntuple( const std::string & file_path, const std::string & type,
                        const std::string & unique, const std::string & cutflow_var,
                        bound_mgr * selections ){

  ROOT::EnableImplicitMT( 6 );
  std::cout << "producing \"truth\" ntuple." << std::endl;

  if ( !cutflow_var.empty() ){ std::cout << cutflow_var << std::endl; }
 
	// Ready the input and output
  TChain chain( "tree" );
  chain.Add( file_path.c_str() );
  ROOT::RDataFrame input_frame( chain );

  

  std::vector< std::string > output_columns = { "ActIpX", "AvgIpX", "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
															 "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
															 "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
															 "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0", "MuPos_Pt", "MuNeg_Pt", "qx", "qy" };


  std::vector< std::string > truth_vector_cuts = { "truth_mu_pos_pt", "truth_mu_neg_pt" };
  std::vector< std::string > truth_float_cuts = { "lambda", "costheta", "qtSquared", "AbsPhi", 
                                            "DiMuonMass", "DiMuonTau", "qtA", "qtB", 
                                            "DiMuonPt", "DeltaZ0", "Phot_Eta", "MuPos_Eta", "MuNeg_Eta", "PhotonPt" };


  std::vector< bound > truth_vector_bounds, truth_float_bounds;
  for ( std::string & bound_str : truth_vector_cuts ){
    truth_vector_bounds.push_back( selections->get_bound( bound_str ) );
  }

	for ( std::string & bound_str : truth_float_cuts ){
    truth_float_bounds.push_back( selections->get_bound( bound_str ) );
  }

  auto truth_filtered = input_frame.Filter( "HLT_2mu4_bJpsimumu_noL2 != 0" );
  truth_filtered = truth_filtered.Filter( "mcPhoton_Pt.size() != 0" );
  truth_filtered = truth_filtered.Filter( "mcMuMinus_Pt.size() != 0" );
  truth_filtered = truth_filtered.Filter( "mcMuPlus_Pt.size() != 0" );

	// manual redefinitions
  auto phi_lambda = phi_calc;
	auto dphi_lambda = dphi_calc;
	truth_filtered = truth_filtered.Define( "ActIpX", 																			"Float_t( actIpX )" );	
	truth_filtered = truth_filtered.Define( "AvgIpX", 																			"Float_t( avgIpX )" );	
	truth_filtered = truth_filtered.Define( "costheta", 																		"Float_t( Truth_MuMuGamma_CS_CosTheta[0] )" );
	truth_filtered = truth_filtered.Define( "AbsCosTheta", 																	"Float_t( abs(costheta) )" );
	truth_filtered = truth_filtered.Define( "DPhi",                                         dphi_lambda, { "mcDiLept_Phi", "mcPhoton_Phi" } );
	truth_filtered = truth_filtered.Define( "AbsdPhi", 																			"Float_t( abs(DPhi) )" );
	truth_filtered = truth_filtered.Define( "Phi",                                          phi_lambda, {"Truth_MuMuGamma_CS_Phi"} );
	truth_filtered = truth_filtered.Define( "AbsPhi", 																			"Float_t( sqrt( Truth_MuMuGamma_CS_Phi[0] * Truth_MuMuGamma_CS_Phi[0]) )" );
	truth_filtered = truth_filtered.Define( "DY", 																					"Float_t( mcDiLept_Eta[0] - mcPhoton_Eta[0])" );
	truth_filtered = truth_filtered.Define( "AbsdY", 																				"Float_t( abs(DY) )" );
	truth_filtered = truth_filtered.Define( "EventNumber",																	"Float_t( evt )" );
	truth_filtered = truth_filtered.Define( "Lambda", 																			"Float_t( (mcMuMuY_M[0]/3097.0) * (mcMuMuY_M[0]/3097.0) )" );
	truth_filtered = truth_filtered.Define( "qTSquared",																		"Float_t( (mcMuMuY_Pt[0]/1000.0) * (mcMuMuY_Pt[0]/1000.0) )" );
	truth_filtered = truth_filtered.Define( "DiMuonMass",																		"Float_t( mcDiLept_M[0] )" );
	truth_filtered = truth_filtered.Define( "DiMuonTau",																		"Float_t( 0 )" );
	truth_filtered = truth_filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	truth_filtered = truth_filtered.Define( "DiMuonPt",																			"Float_t( mcDiLept_Pt[0]/1000.0 )" );
	truth_filtered = truth_filtered.Define( "PhotonPt",																			"Float_t( mcPhoton_Pt[0]/1000.0 )" );
	truth_filtered = truth_filtered.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
 	truth_filtered = truth_filtered.Define( "qxsum", 																				"Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	truth_filtered = truth_filtered.Define( "qysum", 																				"Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	truth_filtered = truth_filtered.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qtA",																					"Float_t( DiMuonPt - PhotonPt )" );
	truth_filtered = truth_filtered.Define( "qtB",																					"Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	truth_filtered = truth_filtered.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	truth_filtered = truth_filtered.Define( "JPsi_Eta",																			"Float_t( mcDiLept_Eta[0] )" );
	truth_filtered = truth_filtered.Define( "Phot_Eta",																			"Float_t( mcPhoton_Eta[0] )" );
  truth_filtered = truth_filtered.Define( "MuPos_Eta",                                    "Float_t( mcMuPlus_Eta[0] )" );
  truth_filtered = truth_filtered.Define( "MuNeg_Eta",                                    "Float_t( mcMuMinus_Eta[0] )" );
  truth_filtered = truth_filtered.Define( "MuPos_Pt",                                     "Float_t( mcMuPlus_Pt[0]/1000.0 )" );
  truth_filtered = truth_filtered.Define( "MuNeg_Pt",                                     "Float_t( mcMuMinus_Pt[0]/1000.0 )" );
	truth_filtered = truth_filtered.Define( "DeltaZ0",																			"Float_t( 0 )" );
  truth_filtered = truth_filtered.Define( "qx",                                           "Float_t( sqrt(qTSquared)*cos( mcMuMuY_Phi[0] ) )" );
  truth_filtered = truth_filtered.Define( "qy",                                           "Float_t( sqrt(qTSquared)*sin( mcMuMuY_Phi[0] ) )" );



	// apply reductions from vector to float and apply cut
	for ( bound & cut_bound : truth_vector_bounds ){
		truth_filtered = truth_filtered.Redefine( cut_bound.get_var(),  []( const ROOT::RVecF &val ){ return val[0]; }, { cut_bound.get_var() } );
		truth_filtered = truth_filtered.Filter( cut_bound.get_cut() );
  }

	for ( bound & cut_bound : truth_float_bounds ){
		truth_filtered = truth_filtered.Filter( cut_bound.get_cut() );
	}

  std::string output_file = Form( "./truth/%s_truth_%s.root", type.c_str(), unique.c_str() );
  truth_filtered.Snapshot( "tree", output_file, output_columns );


}
