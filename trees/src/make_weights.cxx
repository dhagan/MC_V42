#include <make_weights.hxx>

void make_weights( std::string & file_path, std::string & unique, bound_mgr * selections ){


  std::vector<std::string> files;
  split_strings( files, file_path, ":" );

  TFile * data_file = new TFile( files.at(0).c_str(), "READ" );
  TFile * sign_file = new TFile( files.at(1).c_str(), "READ" );

  TTree * data_tree = (TTree *) data_file->Get( "tree" );
  TTree * sign_tree = (TTree *) sign_file->Get( "tree" );

  std::vector< std::string > weight_vars = { "DiMuonPt", "ActIpX", "Lambda", "PhotonPt", "Phi", 
                                            "DeltaZ0", "Phot_Eta", "JPsi_Eta", "AvgIpX" };
  
  TFile * output_file = new TFile( Form( "./weights/%s_weights.root", unique.c_str() ), "RECREATE" );
  output_file->cd();

  for ( std::string & var : weight_vars ){

    bound var_bound = selections->get_bound( var );
    int var_bins = var_bound.get_bins();
    float var_min = var_bound.get_min();
    float var_max = var_bound.get_max();

    TH1F * data_hist = new TH1F( Form( "data_%s", var.c_str() ), "", var_bins, var_min, var_max );
    TH1F * sign_hist = new TH1F( Form( "sign_%s", var.c_str() ), "", var_bins, var_min, var_max );
    TH1F * weight_hist = new TH1F( Form( "weights_%s", var.c_str() ), "", var_bins, var_min, var_max );

    data_tree->Draw( Form( "%s>>data_%s", var.c_str(), var.c_str() ), "", "goff" );
    sign_tree->Draw( Form( "%s>>sign_%s", var.c_str(), var.c_str() ), "", "goff" );

    data_hist->Scale( 1.0 / data_hist->Integral() );
    sign_hist->Scale( 1.0 / sign_hist->Integral() );

    weight_hist->Divide( data_hist, sign_hist, 1.0, 1.0 );
    weight_hist->Write();
		data_hist->Write();
		sign_hist->Write();
    
  }

  output_file->Close();

}
