#include <migration.hxx>
#include <make_ntuple.hxx>
#include <make_gen_ntuple.hxx>
#include <make_truth_ntuple.hxx>
#include <make_efficiencies.hxx>
#include <make_fiducials.hxx>
#include <make_weights.hxx>
#include <make_sf.hxx>

// THERE HAS TO BE A GOOD WAY OF DOING THIS????
struct selection_arguments{
  std::string input_file;
  std::string type;
  std::string unique;
  std::string range;
  std::string selection_file;
  std::string cutflow_var;
  std::string weight_var;
  std::string hf_binning;
  std::string mode;
};



int help(){
std::cout << "Usage: " << std::endl;
std::cout << "./make_ntuple --input,-i INPUT_FILE(s) --type,-t TYPE --unique,-u UNIQUE --selections,-s SELECTIONS \\ " << std::endl;
std::cout << "						  [ --efficiency,-e ] [ --make_weights,-k ] [ --reco,-o ] [ --weight_var,-w WEIGHTING_VARIABLE ] \\ " << std::endl;
std::cout << "						  [ --truth,-s ]  [ --ranges,-r RANGES ] [ --migration,-m ] [ --help,-h ] " << std::endl;
std::cout << " " << std::endl;
std::cout << "Required arguments; " << std::endl;
std::cout << "  --input,-i            Files to be processed, modes requiring multiple files should delimit filepaths with a colon \":\". " << std::endl;
std::cout << "  --type,-t             Type of input file, necessary as processing does differ between the 4 file types. Signal MC has " << std::endl;
std::cout << "                        some truth dependent cuts. Information for these cuts is generally not available in other file " << std::endl;
std::cout << "                        types, nor is it physically sensible to apply. Accepts sign, bckg, data, and bbbg. " << std::endl;
std::cout << "  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is " << std::endl;
std::cout << "                        simply for bookkeeping. " << std::endl;
std::cout << "  --selections,-v       A filepath to a selections file. This holds the information of all the variables binning and " << std::endl;
std::cout << "                        ranges, along with the cuts applied thorughout this program. This should be located in the " << std::endl;
std::cout << "                        share directory of the analysis library and commited to that repository for record. It is " << std::endl;
std::cout << "                        reasonable to write extra selection files into the analysis util library to keep systematics " << std::endl;
std::cout << "                        distinct. " << std::endl;
std::cout << " " << std::endl;
std::cout << " " << std::endl;
std::cout << "Processing modes; " << std::endl;
std::cout << "  --reco,-o             Produce the standard reco tree, works for all types. " << std::endl;
std::cout << "  --truth,-s            Produce a truth tree, this will only operate with \"sign\" type. " << std::endl;
std::cout << "  --make_weights,-k     Produce histograms for the reweighting. Requires data and sign input; \"data_path:sign_path\". " << std::endl;
std::cout << "  --efficiency,-e       Produce efficiency graphs, only operates for \"sign\" type. " << std::endl;
std::cout << "  --migration,-m        Produce migration trees. These should be provided to the dedicated \"migration\" program. " << std::endl;
std::cout << " " << std::endl;
std::cout << " " << std::endl;
std::cout << "Optional arguements " << std::endl;
std::cout << "  --ranges, -r          A string that will be processed into a bound for a variable, can be used to replace or augment " << std::endl;
std::cout << "                        any variable definition already supplied by the selections file as a method of smaller and " << std::endl;
std::cout << "                        faster systematic variations. " << std::endl;
std::cout << "  --weight_var,-w       Variable to be used for efficiency reweighting " << std::endl;
 return 0;
}

int main(int argc, char ** argv){

  int option_index{0},option{0};
  static struct option long_options[] = {
      { "help",           no_argument,          0,      'h'},
      { "input",          required_argument,    0,      'i'},
      { "type",           required_argument,    0,      't'},
      { "unique",         required_argument,    0,      'u'},
      { "ranges",         required_argument,    0,      'r'},
      { "cutflow",        required_argument,    0,      'f'},
      { "efficiency",     no_argument,          0,      'e'},
      { "migration",      no_argument,          0,      'm'},
      { "selection",      required_argument,    0,      'v'},
      { "truth",          no_argument,          0,      's'},
      { "generator",      no_argument,          0,      'g'},
      { "weight_var",     required_argument,    0,      'w'},
      { "make_weights",   required_argument,    0,      'k'},
      { "reco",           no_argument,          0,      'o'},
      { "hf_bin",         required_argument,    0,      'b'},
      { 0,                0,                    0,      0}
  };

  std::string input_file, input_type;
  std::string cuts{""}, unique{""};
  std::string ranges{""};
  std::string cutflow_var{"qtA"};
  std::string selection_file;
  std::string weight_var = "", hf_binning = "";
  std::string json_path = "";

  bool eff = false, weight = false, sf;
  bool mig = false, reco = false, truth = false;
  bool fiducials = false;
  bool generator = false;
  
  do {
    option = getopt_long( argc, argv, "i:t:u:r:v:f:w:b:J:zsmelvogkh", long_options, &option_index );
    switch (option){
      case 'i':
        input_file  = std::string(optarg);
        break;
      case 't':
        input_type  = std::string(optarg);
        break;
      case 'u':
        unique      = std::string(optarg);
        break;
      case 'r':
        ranges      = std::string(optarg);
        break;
      case 'v':
        selection_file  = std::string( optarg );
        break;
      case 'f':
        cutflow_var   =  std::string( optarg ); 
        break;
      case 'e':
        eff = true;
        break;
      case 'm':
        mig = true;
        break;
      case 'k':
        weight = true;
        break;
      case 'w':
        weight_var =    std::string( optarg );
        break;
      case 's':
        truth = true;
        break;
      case 'o':
        reco = true;
        break;
      case 'g':
        generator = true;
        break;
      case 'b':
        hf_binning = std::string( optarg );
        break;
      case 'l':
        sf = true;
        break;
      case 'h':
        return help();
        break;
      case 'j':
        json_path       = std::string( optarg );
        break;
      case 'z':
        fiducials = true;
        break;

    }
  } while ( option != -1 );



  if ( !json_path.empty() ){
    nlohmann::json args_json;
    std::ifstream input_file_stream;
    input_file_stream.open( json_path );
    input_file_stream >> args_json;
    input_file_stream.close();
  }



  sample_type type = static_cast<sample_type>( std::distance( type_c.begin(), std::find( type_c.begin(), type_c.end(), input_type ) ) ); 



  bound_mgr * selections = new bound_mgr() ;
  selections->load_bound_mgr( selection_file );
  if ( !ranges.empty() ){
    selections->process_bounds_string( ranges );
  }

  if ( mig ){
    migration( input_file, input_type, unique, "", selections );
    return 0;
  }

  if ( generator ){
    make_gen_ntuple( input_file, unique,  selections );
    return 0;
  }

  if ( reco ){
    make_ntuple( input_file, input_type, unique, cutflow_var,  selections );
    return 0;
  }

  if ( eff && ( input_type.find( "sign" ) != std::string::npos ) ){
    make_efficiency( input_file, input_type, unique, selections, weight_var, hf_binning );
    return 0;
  }

  if ( fiducials && ( input_type.find( "sign" ) != std::string::npos ) ){
    make_fiducials( input_file, input_type, unique, selections, weight_var );
    return 0;
  }

  if ( weight ){
    make_weights( input_file, unique, selections ); 
    return 0;
  }


  if ( truth && ( input_type.find( "sign" ) != std::string::npos ) ){
    make_truth_ntuple( input_file, input_type, unique, "",  selections );
    return 0;
  }

  if ( sf ){
    make_sf( input_file, type, unique, selections );
    return 0;
  }



}

