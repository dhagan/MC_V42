executable=${ANA_IP}/trees/build/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
ranges_list=( "${ranges_1}" "${ranges_2}" ) 
unique=( "lower" "upper" )


for index in {0..1}; do

	output_unique="qtb_${unique[$index]}_pu_weighted"
	range="${ranges_list[$index]}"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/trees/${output_unique}/objstore
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

	${executable} -i ${efficiency_file} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e -w "pu_weight"

	popd >> /dev/null

done
