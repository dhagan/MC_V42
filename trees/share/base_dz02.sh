executable=${exec_path}/make_ntuple
unique="base_dz02"
log=${LOG_PATH}/trees/${unique}.txt
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"
eff_selections=${LIB_PATH}/share/hf_bounds.txt
files=( "sign" "data" "bckg" "bbbg" )
ranges="DeltaZ0:abs(DeltaZ0)&10:0:2"

touch ${log}
mkdir -p ${OUT_PATH}/trees/${unique}
mkdir -p ${OUT_PATH}/trees/${unique}/efficiency
mkdir -p ${OUT_PATH}/trees/${unique}/cutflow
mkdir -p ${OUT_PATH}/trees/${unique}/objstore
pushd ${OUT_PATH}/trees/${unique} >> /dev/null

## Generate the 4 trees
for file_index in {0..3}; do
	file="${files[$file_index]}"
	echo dz02_${file}
	mkdir -p ${file}
	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
	${executable} -i ${input_file} -t ${file} -u ${unique} -v ${selections} -r ${ranges} -o
done

## Generate efficiencies.
${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -v ${selections} -b ${eff_selections} -r ${ranges} -e

popd >> /dev/null
