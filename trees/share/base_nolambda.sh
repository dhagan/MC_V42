executable=${exec_path}/make_ntuple
unique="base_nolambda"
##selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
selections=${LIB_PATH}/share/ntuple_selection_bounds_nolambda.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
mig_selections=${LIB_PATH}/share/migration_bounds.txt
scalefactor_input="${IN_PATH}/ntuples/data_ntuple.root"
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"
generator_file="${IN_PATH}/000/gen_ntuple.root"
files=( "sign" "data" "bckg" "bbbg" )

mkdir -p ${OUT_PATH}/trees/${unique}
mkdir -p ${OUT_PATH}/trees/${unique}/cutflow
mkdir -p ${OUT_PATH}/trees/${unique}/objstore
mkdir -p ${OUT_PATH}/trees/${unique}/truth
mkdir -p ${OUT_PATH}/trees/${unique}/gen
mkdir -p ${OUT_PATH}/trees/${unique}/efficiency
mkdir -p ${OUT_PATH}/trees/${unique}/scalefactors
mkdir -p ${OUT_PATH}/trees/${unique}/migration

pushd ${OUT_PATH}/trees/${unique} >> /dev/null

## Generate the 4 trees
for file_index  in {1..1};
do
	file="${files[$file_index]}"
	mkdir -p ${file}
	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
	${executable} -i ${input_file} -t ${file} -u ${unique} -v ${selections} -o -f "MuMuY_M[0]/1000.0"
done

### Generate sf
##${executable} -i "${scalefactor_input}" -t "data" -u ${unique} -v ${selections} -l

## Generate truth
##${executable} -i "${efficiency_file}" -t "sign" -u ${unique} -v ${selections} -s
##
#### Generate 000 generator level
##${executable} -i ${generator_file} -u ${unique} -v ${selections} -g
##
#### Generate efficiencies.
##${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -v ${selections} -b ${eff_selections} -e

## Generate migration
##${executable} -i ${efficiency_file} -t "sign" -u ${unique} -v ${mig_selections} -m

popd >> /dev/null
