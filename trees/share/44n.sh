executable=${ANA_IP}/trees/build/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_input="${IN_PATH}/ntuples/sign_ntuple.root"
files=( "sign" "data" "bckg" "bbbg" )

ranges_1="PhotonPt:PhotonPt&10:5::"
ranges_2="PhotonPt:PhotonPt&10:6::"
ranges_3="PhotonPt:PhotonPt&10:7::"
ranges_4="PhotonPt:PhotonPt&10:8::"
ranges_5="PhotonPt:PhotonPt&10:9::"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" )

for index in {0..4}; do

	number=$(($index + 5))
	output_unique="base_44${number}"
	range="${ranges_list[$index]}"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

	## Generate the 4 trees
	for file_index in {0..3}; do
		file="${files[$file_index]}"
		mkdir -p ${file}
		input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
		${executable} -i ${input_file} -t ${file} -u ${output_unique} -v ${selections} -r ${range} -o
	done

	${executable} -i ${efficiency_input} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e
	popd >> /dev/null

done
