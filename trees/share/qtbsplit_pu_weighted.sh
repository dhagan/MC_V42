executable=${ANA_IP}/trees/build/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )

for index in {0..7}; do

	number=$(($index + 1))
	output_unique="qtbsplit-${number}_pu_weighted"
	range="${ranges_list[$index]}"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

	${executable} -i ${efficiency_file} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e -w "pu_weight"
	
	popd >> /dev/null

done
