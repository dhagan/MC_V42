executable=${ANA_IP}/trees/build/make_ntuple
unique="nomass"
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
scalefactor_input="${IN_PATH}/ntuples/data_ntuple.root"
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"
generator_file="${IN_PATH}/000/gen_ntuple.root"
files=( "sign" "data" "bckg" "bbbg" )
ranges="Lambda:Lambda&100:0:10000"

mkdir -p ${OUT_PATH}/trees/${unique}
mkdir -p ${OUT_PATH}/trees/${unique}/cutflow
mkdir -p ${OUT_PATH}/trees/${unique}/objstore
mkdir -p ${OUT_PATH}/trees/${unique}/truth
mkdir -p ${OUT_PATH}/trees/${unique}/gen
mkdir -p ${OUT_PATH}/trees/${unique}/efficiency
mkdir -p ${OUT_PATH}/trees/${unique}/scalefactors

pushd ${OUT_PATH}/trees/${unique} >> /dev/null

## Generate the 4 trees
##for file_index  in {0..3};
##do
##	file="${files[$file_index]}"
##	mkdir -p ${file}
##	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
##	${executable} -i ${input_file} -t ${file} -u ${unique} -v ${selections} -o 
##done

### Generate sf
${executable} -i "${scalefactor_input}" -t "data" -u ${unique} -v ${selections} -l -r ${ranges}

#### Generate truth
##${executable} -i "${efficiency_file}" -t "sign" -u ${unique} -v ${selections} -s
##
#### Generate generator mode
##${executable} -i ${generator_file} -u ${unique} -v ${selections} -g
##
#### Generate efficiencies.
##${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -v ${selections} -b ${eff_selections} -e

popd >> /dev/null
