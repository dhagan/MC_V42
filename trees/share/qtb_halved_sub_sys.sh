executable=${ANA_IP}/trees/build/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_input="${IN_PATH}/ntuples/sign_ntuple.root"

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
range_sub_sys="DiMuonMass:DiMuonMass&100:2740.0:3460.0"
ranges=( "${ranges_1}" "${ranges_2}" ) 
unique=( "lower" "upper" )

for index in {0..1}; do

	range="${ranges[$index]}#${range_sub_sys}"
	output_unique="qtb_${unique[$index]}_sub_mass1_sys"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/trees/${output_unique}/objstore
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null
	
	${executable} -i ${efficiency_input} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e
	
	popd >> /dev/null

done
