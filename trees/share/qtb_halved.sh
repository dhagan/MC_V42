executable=${exec_path}/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_input="${IN_PATH}/ntuples/sign_ntuple.root"
files=( "sign" "data" "bckg" "bbbg" )

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
ranges=( "${ranges_1}" "${ranges_2}" ) 
unique=( "lower" "upper" )

for index in {0..0}; do

	range="${ranges[$index]}"
	output_unique="qtb_${unique[$index]}"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

	## Generate the 4 trees
	##for file_index in {0..3}; do
	##	file="${files[$file_index]}"
	##	mkdir -p ${file}
	##	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
	##	${executable} -i ${input_file} -t ${file} -u ${output_unique} -v ${selections} -r ${range} -o
	##done

	##${executable} -i ${efficiency_input} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e
	${executable} -i ${efficiency_input} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -z
	popd >> /dev/null

done
