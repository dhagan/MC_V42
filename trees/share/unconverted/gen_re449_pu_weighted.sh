executable=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/build/make_tmva_trees

unique="re449_pu_weighted"
files=("data" "bckg" "bbbg")
files_sign=( "sign" )
ranges="PhotonPt_9000.0,:qtA_-10,20:qtB_0,20"
bins="qtA_15:qtB_10"

mkdir -p gen_${unique}
mkdir -p gen_${unique}/cutflow
mkdir -p gen_${unique}/objstore
cd gen_${unique}

##for file in ${files_sign[@]}
##do
##	mkdir -p ${file}
##	$executable -i "/home/atlas/dhagan/analysis/pwr/combine/new_sign_ntuple.root" -t ${file} -r ${ranges} -b ${bins} -u ${unique} -w "PU" -e -o
##done


for file in ${files[@]}
do
	mkdir -p ${file}
	$executable -i "/home/atlas/dhagan/analysisFiles/analysisFork/${file}_ntuple.root" -t ${file} -r ${ranges} -b ${bins} -u ${unique} -o
done
cd ..

