executable=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/build/make_tmva_trees

unique="lower_mean_actipx"
files=("sign" "bckg" "bbbg")
ranges="qtA_-10,20:qtB_0,20"
bins="qtA_15:qtB_10"

mkdir -p gen_${unique}
mkdir -p gen_${unique}/cutflow
mkdir -p gen_${unique}/objstore
cd gen_${unique}

for file in ${files[@]}
do
	mkdir -p ${file}
	$executable -i "/home/atlas/dhagan/analysisFiles/analysisFork/${file}_ntuple.root" -t ${file} -r ${ranges} -b ${bins} -u ${unique} -e
done

files=( "data" )
ranges="qtA_-10,20:qtB_0,20:ActIpX_0.0,13.07"
for file in ${files[@]}
do
	mkdir -p ${file}
	$executable -i "/home/atlas/dhagan/analysisFiles/analysisFork/${file}_ntuple.root" -t ${file} -r ${ranges} -b ${bins} -u ${unique} -e
done
cd ..
