mkdir -p data_bound-0/split0/ 
mkdir -p signal_bound-0/split0/ 
mkdir -p pp_bound-0/split0/
mkdir -p bb_bound-0/split0/

mkdir -p data/lin/ 
mkdir -p data/sim/ 
mkdir -p data/base/ 

mkdir -p sign/lin/ 
mkdir -p sign/sim/ 
mkdir -p sign/base/ 

mkdir -p bckg/lin/
mkdir -p bckg/sim/
mkdir -p bckg/base/

mkdir -p bbbg/base/

s_opts=(5 10 20 30 40 50)
s_opts2=(5 10 25 50)
f_nos=(10 5 2 1)

for s in ${s_opts[@]}
do
	mkdir -p sim_signal_bound-0/s${s}
	mkdir -p sim_data_bound-0/s${s}
	mkdir -p sim_pp_bound-0/s${s}
done


mkdir -p lin_sign/
mkdir -p lin_bckg/
for s in ${s_opts2[@]}
do
	mkdir -p lin_data/s${s}
done
	
mkdir -p lin_data/s5/f1/
mkdir -p lin_data/s5/f2/
mkdir -p lin_data/s5/f3/
mkdir -p lin_data/s5/f4/
mkdir -p lin_data/s5/f5/
mkdir -p lin_data/s5/f6/
mkdir -p lin_data/s5/f7/
mkdir -p lin_data/s5/f8/
mkdir -p lin_data/s5/f9/
mkdir -p lin_data/s5/f10/

mkdir -p lin_data/s10/f1
mkdir -p lin_data/s10/f2
mkdir -p lin_data/s10/f3
mkdir -p lin_data/s10/f4
mkdir -p lin_data/s10/f5

mkdir -p lin_data/s25/f1
mkdir -p lin_data/s25/f2

mkdir -p lin_data/s50/f1
