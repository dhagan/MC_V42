#! /bin/bash

./gen_jpsi_weighted.sh
./gen_pho_weighted.sh
./gen_phi_weighted.sh
./gen_jpsi_eta_weighted.sh
./gen_phot_eta_weighted.sh
./gen_z_weighted.sh
./gen_actipx_weighted.sh
./gen_minv_weighted.sh
