executable=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/build/make_tmva_trees

unique="pu_weighted"
##files=("data" "sign" "bckg" "bbbg")
files=( "sign" )
ranges="qtA_-10,20:qtB_0,20"
bins="qtA_15:qtB_10"

mkdir -p gen_${unique}
mkdir -p gen_${unique}/cutflow
mkdir -p gen_${unique}/objstore
cd gen_${unique}

for file in ${files[@]}
do
	mkdir -p ${file}
	$executable -i "/home/atlas/dhagan/analysis/pwr/combine/new_sign_ntuple.root" -t ${file} -r ${ranges} -b ${bins} -u ${unique} -w "PU" -e
done
cd ..
