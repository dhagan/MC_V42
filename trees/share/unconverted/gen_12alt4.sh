executable=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/build/make_tmva_trees

unique="12alt4"
##files=("data" "sign" "bckg" "bbbg")
files=( "sign" )
ranges="qtA_-12,14:qtB_0,20"
bins="qtA_12:qtB_10"

mkdir -p gen_${unique}
mkdir -p gen_${unique}/cutflow
mkdir -p gen_${unique}/objstore
cd gen_${unique}

for file in ${files[@]}
do
	mkdir -p ${file}
	$executable -i "/home/atlas/dhagan/analysisFiles/analysisFork/${file}_ntuple.root" -t ${file} -r ${ranges} -b ${bins} -u ${unique} -e
done
cd ..
