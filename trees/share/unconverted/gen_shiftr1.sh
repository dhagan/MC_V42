executable=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/build/make_tmva_trees

unique=shiftr1
files=( "data" "sign" "bckg" "bbbg" )
ranges="qtA_-9,21:qtB_0,20"
bins="qtA_15:qtB_10"

mkdir -p gen_${unique}
mkdir -p gen_${unique}/cutflow
mkdir -p gen_${unique}/objstore
cd gen_${unique}

for file in ${files[@]}
do
	mkdir -p ${file}
	$executable -i "/home/atlas/dhagan/analysisFiles/analysisFork/${file}_ntuple.root" -t ${file} -r ${ranges} -u ${unique} -b ${bins} -e
done
cd ..
