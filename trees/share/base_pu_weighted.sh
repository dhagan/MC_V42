executable=${ANA_IP}/trees/build/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"
eff_selections=${LIB_PATH}/share/hf_bounds.txt
output_unique="base_pu_weighted"

mkdir -p ${OUT_PATH}/trees/${output_unique}
mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
mkdir -p ${OUT_PATH}/trees/${output_unique}/objstore
pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

## Generate weighted efficiencies
${executable} -i ${efficiency_file} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -e -w "pu_weight"

popd >> /dev/null
