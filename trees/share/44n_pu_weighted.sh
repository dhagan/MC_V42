executable=${ANA_IP}/trees/build/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"

ranges_1="photon_pt:Photon_Pt&10:5::"
ranges_2="photon_pt:Photon_Pt&10:6::"
ranges_3="photon_pt:Photon_Pt&10:7::"
ranges_4="photon_pt:Photon_Pt&10:8::"
ranges_5="photon_pt:Photon_Pt&10:9::"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" )

for index in {0..4}; do

	number=$(($index + 5))
	output_unique="base_44${number}_pu_weighted"
	range="${ranges_list[$index]}"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

	${executable} -i ${efficiency_file} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e -w "pu_weight"

	popd >> /dev/null
done
