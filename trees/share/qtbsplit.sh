executable=${exec_path}/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_input="${IN_PATH}/ntuples/sign_ntuple.root"
scalefactor_input="${IN_PATH}/ntuples/data_ntuple.root"

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )
files=( "sign" "data" "bckg" "bbbg" )

for index in {0..0}; do

	number=$(($index + 1))
	output_unique="qtbsplit-${number}"
	range="${ranges_list[$index]}"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/trees/${output_unique}/scalefactors
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

	## Generate the 4 trees
	##for file_index in {0..3}; do
	##	file="${files[$file_index]}"
	##	mkdir -p ${file}
	##	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
	##	${executable} -i ${input_file} -t ${file} -u ${output_unique} -v ${selections} -r ${range} -o
	##done
	##${executable} -i ${efficiency_input} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e
	${executable} -i ${efficiency_input} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -z

	##${executable} -i "${scalefactor_input}" -t "data" -u ${output_unique} -v ${selections} -l -r ${range}

	popd >> /dev/null


done
