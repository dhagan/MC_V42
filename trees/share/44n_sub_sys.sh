executable=${ANA_IP}/trees/build/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_input="${IN_PATH}/ntuples/sign_ntuple.root"

ranges_1="photon_pt:Photon_Pt&10:5::"
ranges_2="photon_pt:Photon_Pt&10:6::"
ranges_3="photon_pt:Photon_Pt&10:7::"
ranges_4="photon_pt:Photon_Pt&10:8::"
ranges_5="photon_pt:Photon_Pt&10:9::"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" )

for index in {0..4}; do

	number=$(($index + 5))
	output_unique="base_44${number}_sub_mass1_sys"
	range="${ranges_list[$index]}#DiMuonMass:DiMuonMass&100:2740.0:3460.0"

	mkdir -p ${OUT_PATH}/trees/${output_unique}
	mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
	pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

	## Generate Efficiencies
	${executable} -i ${efficiency_input} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${range} -e
	popd >> /dev/null

done
