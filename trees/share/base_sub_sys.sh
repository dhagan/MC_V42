executable=${exec_path}/make_ntuple
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"
eff_selections=${LIB_PATH}/share/hf_bounds.txt
files=( "sign" "data" "bckg" "bbbg" )
ranges="DiMuonMass:DiMuonMass&100:2740.0:3460.0"
output_unique="base_sub_mass1_sys"

mkdir -p ${OUT_PATH}/trees/${output_unique}
mkdir -p ${OUT_PATH}/trees/${output_unique}/cutflow
mkdir -p ${OUT_PATH}/trees/${output_unique}/efficiency
mkdir -p ${OUT_PATH}/trees/${output_unique}/objstore
pushd ${OUT_PATH}/trees/${output_unique} >> /dev/null

## Generate the 4 trees
##for file_index in {0..3}; do
##	file="${files[$file_index]}"
##	mkdir -p ${file}
##	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
##	${executable} -i ${input_file} -t ${file} -u ${output_unique} -v ${selections} -r ${ranges} -o
##done

## Generate efficiencies.
${executable} -i ${efficiency_file} -t "sign" -u "${output_unique}" -v ${selections} -b ${eff_selections} -r ${ranges} -e

popd >> /dev/null
