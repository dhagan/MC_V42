# Tree Generation Scripts

## Scripts and purpose.
| Script name  | Function |
| :---         | :---     |
|  base.sh                 | This is the baseline script.                                                            |
|  jpsi\_pt.sh             | $`J/\psi P_{T}`$.                                                                       |
|  phot\_pt.sh             | $`\gamma P_{T}`$.                                                                       |
|  wide.sh                 | Wider $`q_{T}^{A}`$ processing, 25 bins from -20 to 30 GeV.                             |
|  noqt2.sh                | no $`q_{T}^{@} < 400`$GeV cut.                                                          |
|  jp8.sh                  | Generate $` P_{T,J/\psi} > 8 GeV `$.                                                    |
|  jp9.sh                  | Generate $` P_{T,J/\psi} > 9 GeV `$.                                                    |
|  jp10.sh                 | Generate $` P_{T,J/\psi} > 10 GeV `$.                                                   |
|  re446.sh                | Generate $` P_{T,\gamma} > 6 GeV `$.                                                    |
|  re447.sh                | Generate $` P_{T,\gamma} > 7 GeV `$.                                                    |
|  re448.sh                | Generate $` P_{T,\gamma} > 8 GeV `$.                                                    |
|  re449.sh                | Generate $` P_{T,\gamma} > 9 GeV `$.                                                    |
|  shiftleft\_0p5.sh       | Shift $`q_{T}^{A} `$ range $`0.5`$ GeV leftwards.                                       |
|  shiftright\_0p5.sh      | Shift $`q_{T}^{A} `$ range $`0.5`$ GeV rightwards.                                      |
|  shiftleft\_1p0.sh       | Shift $`q_{T}^{A} `$ range $`1.0`$ GeV leftwards.                                       |
|  shiftright\_1p0.sh      | Shift $`q_{T}^{A} `$ range $`1.0`$ GeV rightwards.                                      |
|  qtA\_10\_bins.sh        | $`q_{T}^{A} `$ with 10 bins                                                             |
|  qtA\_12\_bins.sh        | $`q_{T}^{A} `$ with 12 bins                                                             |
|  dz02.sh                 | Limiting $`\Delta z_{0}`$ to $`2\mu m`$                                                 |
|  pu_\_weighted           | Baseline with pileup reweighted efficiencies                                            |
|  sub\_sys.sh             | Selection with reduced $`M_{J/\psi} range`$                                             |
|  re447\_sub\_sys.sh      | Selection with  $` P_{T,\gamma} > 7 GeV `$ with reduced $`M_{J/\psi} range`$            |
|  re447\_dz02.sh          | Selection with  $` P_{T,\gamma} > 7 GeV `$ and $`\Delta z_{0} \leq 2\mu m`$             |
|  re449\_dz02.sh          | Selection with  $` P_{T,\gamma} > 9 GeV `$ and $`\Delta z_{0} \leq 2\mu m`$             |
|  re447\_pu\_weighted.sh  | Selection with  $` P_{T,\gamma} > 7 GeV `$ and pileup reweighted efficiency             |
|  re449\_pu\_weighted.sh  | Selection with  $` P_{T,\gamma} > 9 GeV `$ and pileup reweighted efficiency             |
|  re447\_sub\_sys.sh      | Selection with  $` P_{T,\gamma} > 7 GeV `$ with reduced $`M_{J/\psi} range`$            |
|  re449\_sub\_sys.sh      | Selection with  $` P_{T,\gamma} > 9 GeV `$ with reduced $`M_{J/\psi} range`$            |


## Script Structure
Below is how the scripts are structured. All of the defined environment variables have been defined by the setup script. This setup script must be sourced before executing any of the scripts.

Variable definitions come first.
```
executable=${ANA_IP}/trees/build/make_ntuple
file_dir=$HOME/data/analysis/
unique="base"
log=${LOG_PATH}/trees/${unique}.txt
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
efficiency_file="${file_dir}/sign_ntuple.root"
weight_selections=${LIB_PATH}/share/reweight_bounds.txt
data_for_weight=${OUT_PATH}/trees/gen_${unique}/data/data_base.root 
sign_for_weight=${OUT_PATH}/trees/gen_${unique}/sign/sign_base.root 
files=( "sign" "data" "bckg" "bbbg" )
```
${ANA\_IP} is used as the install path for the analysis, executable is relatively pathed from this.
file\_dir is the location of the input ntuples for this step.
log is the location of the log file.
selections and weight\_selections are the paths to the selections files.
efficiency\_file is just the path to the signal ntuple.
data\_for\_weight and signal\_for\_weight are the paths for data and signal for computing weights for reweighting.
files is simply a bash array for looping over all 4 input ntuples.


Create the log file for storing information from runs, similarly create the storage structure necessary for outputting files properly
```
touch ${log}
mkdir -p ${OUT_PATH}/trees/gen_${unique}
mkdir -p ${OUT_PATH}/trees/gen_${unique}/cutflow
mkdir -p ${OUT_PATH}/trees/gen_${unique}/objstore
```

Enter into the output directory for executing the program
```
pushd ${OUT_PATH}/trees/gen_${unique} >> /dev/null
```


Generate the the 4 trees, iterating over all ntuples
```
for file in ${files[@]}
do
	mkdir -p ${file}
	input_file="${file_dir}/${file}_ntuple.root"
	${executable} -i ${input_file} -t ${file} -u ${unique} -s ${selections} -o 2>&1 | tee ${log}
done
```

Example generating weights
```
${executable} -i "${data_for_weight}:${sign_for_weight}" -u ${unique} -s ${weight_selections} -k
```

Example generate efficiencies.
```
${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -s ${selections} -e
```

Example weighted efficiency
```
${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -s ${selections} -e -w "DiMuonPt"
```

Exit back to starting directory.
```
popd >> /dev/null
```
