executable=${ANA_IP}/trees/build/make_ntuple
file_dir=$HOME/data/analysis/
unique="shiftleft_0p5"
log=${LOG_PATH}/trees/${unique}.txt
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"
weight_selections=${LIB_PATH}/share/reweight_bounds.txt
data_for_weight=${OUT_PATH}/trees/gen_${unique}/data/data_base.root 
sign_for_weight=${OUT_PATH}/trees/gen_${unique}/sign/sign_base.root 
files=( "sign" "data" "bckg" "bbbg" )
ranges="qtA:qtA&15:-10.5:19.5"

touch ${log}
mkdir -p ${OUT_PATH}/trees/gen_${unique}
mkdir -p ${OUT_PATH}/trees/gen_${unique}/cutflow
mkdir -p ${OUT_PATH}/trees/gen_${unique}/objstore

pushd ${OUT_PATH}/trees/gen_${unique} >> /dev/null

## Generate the 4 trees
for file in ${files[@]}
do
	mkdir -p ${file}
	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
	${executable} -i ${input_file} -t ${file} -u ${unique} -v ${selections} -r ${ranges} -f "qtA" -o 2>&1 | tee ${log}
done

## Generate efficiencies.
${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -v ${selections} -r ${ranges} -e

popd >> /dev/null
