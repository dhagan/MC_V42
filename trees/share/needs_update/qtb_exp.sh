executable=${ANA_IP}/trees/build/make_ntuple
unique="qtb_exp"
log=${LOG_PATH}/trees/${unique}.txt
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
eff_selections=${LIB_PATH}/share/hf_bounds.txt
efficiency_file="${IN_PATH}/ntuples/sign_ntuple.root"
weight_selections=${LIB_PATH}/share/reweight_bounds.txt
data_for_weight=${OUT_PATH}/trees/gen_${unique}/data/data_base.root 
sign_for_weight=${OUT_PATH}/trees/gen_${unique}/sign/sign_base.root 
files=( "sign" "data" "bckg" "bbbg" )
ranges="photon_pt:Photon_Pt&1000:5000:10e30"

touch ${log}
mkdir -p ${OUT_PATH}/trees/gen_${unique}
mkdir -p ${OUT_PATH}/trees/gen_${unique}/cutflow
mkdir -p ${OUT_PATH}/trees/gen_${unique}/objstore

pushd ${OUT_PATH}/trees/gen_${unique} >> /dev/null

## Generate the 4 trees
for file in ${files[@]}
do
	mkdir -p ${file}
	input_file="${IN_PATH}/ntuples/${file}_ntuple.root"
	${executable} -i ${input_file} -t ${file} -u ${unique} -v ${selections} -r ${ranges} -f "qtA" -o 2>&1 | tee ${log}
done

## Generate weights
##${executable} -i "${data_for_weight}:${sign_for_weight}" -u ${unique} -v ${weight_selections} -k

## Generate efficiencies.
##${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -v ${selections} -b ${eff_selections} -e

## Example weighted efficiency
##${executable} -i ${efficiency_file} -t "sign" -u "${unique}" -s ${selections} -e -w "DiMuonPt"

popd >> /dev/null
