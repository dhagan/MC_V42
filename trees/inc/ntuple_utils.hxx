#ifndef ntuple_utils_hxx
#define ntuple_utils_hxx

#include <anna.hxx>
#include <ROOT/RDataFrame.hxx>


bool VatoCS( const TLorentzVector &mupl, const TLorentzVector &mumi, const TLorentzVector &gamma,
	           TVector3 &CSAxis, TVector3 &xAxis, TVector3 &yAxis,
	           double &cosTheta_dimu, double &cosTheta_gamma,
	           double &phi_dimu, double &phi_gamma );

struct Jpsi {
  Float_t DiMuonMass;
  Float_t DiMuonTau;
  Float_t DiMuonPt;
  Float_t DiLept_Y;
  Float_t DiLept_Phi;
  Float_t Trigger_HLT_2mu4_bJpsimumu_noL2;
  Float_t EventNumber;
  TLorentzVector mupl, mumi;
  TLorentzVector p4;
  Float_t old_lambda, old_qt2;
  Float_t old_csPhi, old_csCosTheta;
};

struct Photon {
  Float_t PhotonPt;
  Float_t Photon_Eta;
  Float_t Photon_Phi;
  TLorentzVector p4;
};

Float_t dphi_calc( const ROOT::RVecF &DiLept_Phi, const ROOT::RVecF &Photon_Phi );
Float_t phi_calc( const ROOT::RVecF &MuMuGamma_CS_Phi );
Float_t photon_dr_calc( const ROOT::RVecF &Photon_Pt, const ROOT::RVecF &Photon_Eta, const ROOT::RVecF &Photon_Phi, const ROOT::RVecF &Photon_E,
											 const ROOT::RVecF &mcPhoton_Pt,  const ROOT::RVecF &mcPhoton_Eta, const ROOT::RVecF &mcPhoton_Phi );
Float_t eff_photon_dr_calc( const ROOT::RVecF &Photon_Pt, const ROOT::RVecF &Photon_Eta, const ROOT::RVecF &Photon_Phi, const ROOT::RVecF &Photon_E,
											 const ROOT::RVecF &mcPhoton_Pt,  const ROOT::RVecF &mcPhoton_Eta, const ROOT::RVecF &mcPhoton_Phi );


#endif
