#include <anna.hxx>

#include <ROOT/RDataFrame.hxx>

#include <ntuple_utils.hxx>

void make_truth_ntuple( const std::string & file_path, const std::string & type,
                        const std::string & unique, const std::string & cutflow_var,
                        bound_mgr * selections );

 
