#include <anna.hxx>

#include <ntuple_utils.hxx>

void make_fiducials( std::string & file_path, std::string & type, std::string & unique, 
  bound_mgr * selections, std::string & weight_var );


