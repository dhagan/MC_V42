#include <anna.hxx>
#include <ntuple_utils.hxx>
#include <ROOT/RDataFrame.hxx>



void make_ntuple( const std::string & file_path, const std::string & type,
                  const std::string & unique, const std::string & cutflow_var,
                  bound_mgr * selections );
