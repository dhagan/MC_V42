#include <anna.hxx>

#include <ntuple_utils.hxx>

void make_efficiency( std::string & file_path, std::string & type, std::string & unique, 
  bound_mgr * selections, std::string & weight_var, std::string & hf_binning );

//ROOT::RDataFrame make_reco_tree( ROOT::RDataFrame & input_frame );
//ROOT::RDataFrame make_truth_tree( ROOT::RDataFrame & input_frame );
