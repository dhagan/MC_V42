
#ifndef make_gen_ntuple_hxx
#define make_gen_ntuple_hxx

#include <anna.hxx>

#include <ROOT/RDataFrame.hxx>

#include <ntuple_utils.hxx>

void make_gen_ntuple( const std::string & file_path, const std::string & unique, bound_mgr * selections );

#endif
