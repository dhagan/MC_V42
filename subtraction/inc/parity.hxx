#ifndef parity_hxx
#define parity_hxx

#include <main.hxx>

void parity( std::string & input_path, std::string type, bound_mgr * selections, 
             std::string & analysis_var, std::string & spectator_var, std::string & unique ); 

void positive( std::string & sign_path, std::string & bckg_path, std::string & data_path,
               bound_mgr * selections, std::string & analysis_var, std::string & spectator_var );

#endif
