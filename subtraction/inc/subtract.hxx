#ifndef subtract_hxx
#define subtract_hxx

#include <string>
#include "bound_mgr.hxx"
#include "cutflow.hxx"
#include "visualise.hxx"
#include <ROOT/RDataFrame.hxx>
#include <fileset.hxx>
#include <variable_set.hxx>

void subtract( basic_fileset * fileset, variable_set * variables, std::string & type, std::string & cutflow_path, std::string & y_values, double tau_midpoint = 0 ); 

#endif
