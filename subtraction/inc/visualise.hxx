#ifndef visualise_hxx
#define visualise_hxx

#include <anna.hxx>

void visualise( std::string & subtraction_path, std::vector< std::string > & vars, std::string & type, bound_mgr * bounds, std::string & unique );

#endif
