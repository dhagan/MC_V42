#include "parity.hxx"



void parity( std::string & input_path, std::string type, bound_mgr * selections, 
    std::string & analysis_var, std::string & spectator_var, std::string & unique ){

  TFile * file = new TFile( input_path.c_str(), "READ" );
  TTree * tree = (TTree *) file->Get( "tree" );
  std::string output_file_string = Form( "./subtracted_%s_%s_A-%s_S-%s.root", unique.c_str(), 
                                  type.c_str(), analysis_var.c_str(), spectator_var.c_str() );
  TFile * output_file = new TFile( output_file_string.c_str(), "RECREATE" );

  bound analysis_bound = selections->get_bound( analysis_var );
  bound spectator_bound = selections->get_bound( spectator_var );
  int spec_bins = spectator_bound.get_bins();
  float spec_min = spectator_bound.get_min();
  float spec_max = spectator_bound.get_max();

  std::vector<std::string> mass_strs = { "Q0", "Q3", "Q4", "Q5", "Q12" };
  std::vector< std::string > ana_cut_series = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( 0,  true );

  std::vector< TH1F * > output_histograms;
  std::vector< std::string > output_names;

  for ( std::string & mass : mass_strs ){

    bound mass_bound = selections->get_bound( mass );

    for ( size_t cut = 0; cut < ana_cut_series.size(); cut++ ){
      
      TH1F * output_histogram = new TH1F( Form( "%s_%s_%i", mass.c_str(), spectator_var.c_str(), (int) cut+1 ), "", spec_bins, spec_min, spec_max );
      TH1F * output_histogram_no_subtraction = new TH1F( Form( "%s_%s_%i_no_sub", mass.c_str(), spectator_var.c_str(), (int) cut+1 ), "", spec_bins, spec_min, spec_max );
      output_histogram->Sumw2( kTRUE );
      output_histogram_no_subtraction->Sumw2( kTRUE );
      tree->Draw( Form( "%s>>%s_%s_%i", spectator_var.c_str(), mass.c_str(), spectator_var.c_str(), (int) cut+1 ), 
                  Form( "subtraction_weight*(%s&&%s)", ana_cut_series[cut].c_str(), mass_bound.get_cut().c_str() ) );
      tree->Draw( Form( "%s>>%s_%s_%i_no_sub", spectator_var.c_str(), mass.c_str(), spectator_var.c_str(), (int) cut+1 ), 
                  Form( "%s&&%s", ana_cut_series[cut].c_str(), mass_bound.get_cut().c_str() ) );
      std::string output_name = Form( "%s_99_npos_%s_%s-%i", spectator_var.c_str(), mass.c_str(), analysis_var.c_str(), (int) cut+1 );
      std::string output_name_no_sub = Form( "%s_00_npos_%s_%s-%i", spectator_var.c_str(), mass.c_str(), analysis_var.c_str(), (int) cut+1 );
      output_histograms.push_back( output_histogram );
      output_names.push_back( output_name );
      output_histograms.push_back( output_histogram_no_subtraction );
      output_names.push_back( output_name_no_sub );


    }
  }

  output_file->cd();
  for ( size_t hist_no = 0; hist_no < output_histograms.size(); hist_no++ ){
    output_histograms[ hist_no ]->Write( output_names[hist_no].c_str() );
  }

  output_file->Close();
  
}

void positive( std::string & sign_path, std::string & bckg_path, std::string & data_path,
               bound_mgr * selections, std::string & analysis_var, std::string & spectator_var ){

  TFile * data_file = new TFile( data_path.c_str(), "UPDATE" );
  TFile * sign_file = new TFile( sign_path.c_str(), "UPDATE" );
  TFile * bckg_file = new TFile( bckg_path.c_str(), "UPDATE" );

  bound analysis_bound = selections->get_bound( analysis_var );
  bound spectator_bound = selections->get_bound( spectator_var );

  std::vector<std::string> mass_strs = { "Q0", "Q3", "Q4", "Q5", "Q12" };
  std::vector< std::string > ana_cut_series = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( 0,  true );

  for ( std::string & mass : mass_strs ){

    for ( size_t cut = 0; cut < ana_cut_series.size(); cut++ ){

      std::string hist_name = Form( "%s_99_npos_%s_%s-%i", spectator_var.c_str(), mass.c_str(), analysis_var.c_str(), (int) cut+1 );
      std::string output_name = Form( "%s_99_pos_%s_%s-%i", spectator_var.c_str(), mass.c_str(), analysis_var.c_str(), (int) cut+1 );
      TH1F * data_hist = (TH1F *) data_file->Get( hist_name.c_str() );
      TH1F * sign_hist = (TH1F *) sign_file->Get( hist_name.c_str() );
      TH1F * bckg_hist = (TH1F *) bckg_file->Get( hist_name.c_str() );

      pos_hists( data_hist, sign_hist, bckg_hist );

      sign_file->cd();
      sign_hist->Write( output_name.c_str() );
      bckg_file->cd();
      bckg_hist->Write( output_name.c_str() );
      data_file->cd();
      data_hist->Write( output_name.c_str() );

    }

  }

  sign_file->Close(); 
  bckg_file->Close();
  data_file->Close();

}
