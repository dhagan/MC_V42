#include <main.hxx>

#include "subtract.hxx"
#include "parity.hxx"
#include "visualise.hxx"

#include <stdexcept>

int help(){
  std::cout << "Usage: " << std::endl;
  std::cout << "./subtract --input,-i INPUT_PATH  \\ " << std::endl;
  std::cout << "           --unique,-u UNIQUE --type,-t TYPE  \\ " << std::endl;
  std::cout << " " << std::endl;
  std::cout << "Required arguments; " << std::endl;
  std::cout << "  --input,-i            Input file path, in \"pos\" mode, supply data, sign, and bckg paths in colon " << std::endl;
  std::cout << "                        separated list " << std::endl;
  std::cout << "  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is " << std::endl;
  std::cout << "  --type,-t             data, sign, bckg, bbbg" << std::endl;
  std::cout << " " << std::endl;
  std::cout << "Optional arguments; " << std::endl;
  std::cout << "  --help,-h             This message. " << std::endl;

  return 0;
}

int main(int argc, char *argv[]){

  static struct option long_options[] = {
      { "input",          required_argument,    0,      'i'},
      { "type",           required_argument,    0,      't'},
      { "unique",         required_argument,    0,      'u'},
      { "parity",         required_argument,    0,      'p'},
      { "help",           no_argument,          0,      'h'},
      { 0,                0,                    0,      0}
  };

  int option_index{0},option{0};

  // default args
  std::string input_path;
  std::string type;
  std::string unique;
  std::string selections, ranges, extra_cut;
  std::string spectator_var, analysis_var;
  std::string sub_constants, y_values, extra_var, cutflow_path;
  bool parity_mode = false;
  double midpoint = 0;
  
  do {
    option = getopt_long( argc, argv, "i:u:t:v:a:s:r:b:y:l:e:m:c:hp", long_options, &option_index);
    switch ( option ){
      case 'i':
        input_path          = std::string( optarg );
        break;
      case 'u':
        unique              = std::string( optarg );
        break;
      case 't':
        type                = std::string( optarg );
        break;
      case 'v':
        selections          = std::string( optarg );
        break;
      case 'a':
        analysis_var        = std::string( optarg );
        break;
      case 's':
        spectator_var       = std::string( optarg );
        break;
      case 'r':
        ranges              = std::string( optarg ); 
        break;
      case 'b':
        sub_constants       = std::string( optarg );
        break;
      case 'y':
        y_values            = std::string( optarg ); 
        break;
      case 'c':
        cutflow_path        = std::string( optarg ); 
        break;
      case 'm':
        midpoint            = std::atof( optarg );
        break;
      case 'e':
        extra_cut           = std::string( optarg );
        break;
      case 'p':
        parity_mode = true;
        break;
      case 'h':
        return help();
    }
  } while ( option != -1);

  // check we've got all the necessary stuff
  bool unconfigured = false;
  if ( input_path.empty() ){ std::cout << "Path to input files not provided." << std::endl; unconfigured = true; }
  if ( unique.empty() ){ std::cout << "Type not provided" << std::endl; unconfigured = true; }
  if ( unique.empty() ){ std::cout << "Unique not provided." << std::endl; unconfigured = true; }
  if ( spectator_var.empty() ){ std::cout << "Spectator variable not provided." << std::endl; unconfigured = true; }
  if ( analysis_var.empty() ){ std::cout << "Analysis variable not provided." << std::endl; unconfigured = true; }
  if ( unconfigured ){
    throw std::invalid_argument( "Missing arguments, rerun with required arguments." );
    return 0;
  }


  variable_set * variables = new variable_set( selections, ranges, analysis_var, spectator_var );
  variables->set_extra_bounds( extra_cut );
  basic_fileset * fileset;


  
  if ( !parity_mode ){ 
    //fileset = new basic_fileset( unique, new TFile( input_path.c_str(), "READ") );
    fileset = new basic_fileset();
    fileset->file = new TFile( input_path.c_str(), "READ" );
    fileset->set_unique( unique );
    std::cout << "subtract"<< std::endl;
    subtract( fileset, variables, type, cutflow_path, y_values, midpoint );
  } else {

    // sign bckg data
    std::vector< std::string > input_vec;
    split_strings( input_vec, input_path, ":" );
    std::string sign_path = input_vec.at( 0 );
    std::string bckg_path = input_vec.at( 1 );
    std::string data_path = input_vec.at( 2 );
    std::cout << "Parity" << std::endl;
    parity( sign_path, "sign", variables->bound_manager, analysis_var, spectator_var, unique );
    parity( bckg_path, "bckg", variables->bound_manager, analysis_var, spectator_var, unique );
    parity( data_path, "data", variables->bound_manager, analysis_var, spectator_var, unique );
  
    std::cout << "positive" << std::endl;
    std::string parity_sign = Form( "./subtracted_%s_sign_A-%s_S-%s.root", unique.c_str(), analysis_var.c_str(), spectator_var.c_str() );
    std::string parity_bckg = Form( "./subtracted_%s_bckg_A-%s_S-%s.root", unique.c_str(), analysis_var.c_str(), spectator_var.c_str() );
    std::string parity_data = Form( "./subtracted_%s_data_A-%s_S-%s.root", unique.c_str(), analysis_var.c_str(), spectator_var.c_str() );
    positive( parity_sign, parity_bckg, parity_data, variables->bound_manager, analysis_var, spectator_var );

  }

  return 0;

}
