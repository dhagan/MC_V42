#include "subtract.hxx"


Float_t subtraction_weight( const ROOT::RVecF & jpsi_mass, const ROOT::RVecF & jpsi_tau, std::map< int, float > & weight_map, std::vector<double> & mass_edges, std::vector<double> & tau_edges ){

    int weight_index = 0;
    if ( tau_edges[0] < jpsi_tau[0] && jpsi_tau[0] < tau_edges[1] ){
      weight_index += 1;
    } else if ( tau_edges[1] < jpsi_tau[0] && jpsi_tau[0] < tau_edges[2] ){
      weight_index += 2;
    }

    if ( mass_edges[0] < jpsi_mass[0] ){
      weight_index += 10;
    }
    if ( mass_edges[1] < jpsi_mass[0] ){
      weight_index += 10;
    }
    if ( mass_edges[2] < jpsi_mass[0] ){
      weight_index += 10;
    } 
    if ( mass_edges[3] < jpsi_mass[0] ){
      weight_index += 10;
    }
    if ( mass_edges[4] < jpsi_mass[0] ){
      return 0;
    }
    return weight_map[ weight_index ];
}

// midpoints shifted up 1 for the 0 set;
int floor_search( const ROOT::RVecF & value, std::vector<double> & edges, int lower, int upper ){

  if ( lower > upper ){ return 0; }

  if( edges.back() < value[0] ){ return 0; }
  if( edges.front() > value[0] ){ return 0; } 
  int midpoint = ( lower + upper ) / 2;
  if( edges.at( midpoint ) == value[0] ){ return midpoint + 1; } 
  if ( edges.at( midpoint - 1 ) <= value[0] ){
    if ( ( value[0] < edges.at( midpoint ) ) ){ 
      return midpoint;
    } 
  } 
  if( value[0] < edges.at( midpoint ) ){ return floor_search( value, edges, lower, midpoint-1 ); } 
  return floor_search( value, edges, midpoint+1, upper );
}


int floor_search_double( const ROOT::RVecD & value, std::vector<double> & edges, int lower, int upper ){

  if ( lower > upper ){ return 0; }
  if( edges.back() < value[0] ){ return 0; }
  if( edges.front() > value[0] ){ return 0; } 
  int midpoint = ( lower + upper ) / 2;
  if( edges.at( midpoint ) == value[0] ){ return midpoint + 1; } 
  if ( edges.at( midpoint - 1 ) <= value[0] ){
    if ( ( value[0] < edges.at( midpoint ) ) ){ 
      return midpoint;
    } 
  } 
  if( value[0] < edges.at( midpoint ) ){ return floor_search_double( value, edges, lower, midpoint ); } 
  return floor_search_double( value, edges, midpoint+1, upper );
}

std::map< int, std::map< int, float > > prepare_yexp_map( std::string & y_values, bound analysis_bound ){

  std::vector< std::string > y_strs;        
  std::vector< double > y_dbls;
  split_strings( y_strs, y_values, ":" );   
  if ( y_strs.size() != (size_t) analysis_bound.get_bins() ){  std::cout << "WARNING: Number of y values provided does not match number of analysis bins" << std::endl; }
  y_dbls.reserve( y_strs.size() );
  std::transform( y_strs.begin(), y_strs.end(), std::back_inserter( y_dbls ), []( std::string y ){ return std::stod(y); } );

  std::map< int, std::map< int, float > > weight_map_yexp;
  std::map< int, float > out_map;
  out_map[11] = 0.0; 
  out_map[12] = 0.0;
  out_map[21] = 0.0;
  out_map[22] = 0.0;
  out_map[31] = 0.0;
  out_map[32] = 0.0;
  out_map[41] = 0.0;
  out_map[42] = 0.0;
  weight_map_yexp[ 0 ] = out_map;

  int analysis_bin = 1;
  for ( double & y_dbl : y_dbls ){ 
    std::map< int, float > temp_map;
    temp_map[11] = -y_dbl; 
    temp_map[12] = y_dbl;
    temp_map[21] = 1.0;
    temp_map[22] = -1.0;
    temp_map[31] = 1.0;
    temp_map[32] = -1.0;
    if ( y_dbl != 0 ){
      temp_map[41] = -1.0/y_dbl;
      temp_map[42] = 1.0/y_dbl;
    } else {
      temp_map[41] = 0.0;
      temp_map[42] = 0.0;
    }
    weight_map_yexp[ analysis_bin ] = temp_map;
    analysis_bin += 1;
  }
  return weight_map_yexp;
}


void subtract( basic_fileset * fileset, variable_set * variables, std::string & type, std::string & cutflow_path, std::string & y_values, double tau_midpoint ){


  TChain chain( "tree" );
  fileset->file->Close();
  const char * unique = fileset->get_unique();
  std::string filename = fileset->file->GetName();
  chain.Add( filename.c_str() );
  ROOT::RDataFrame input_frame( chain );

  std::string output_file = Form( "./%s_%s.root", type.c_str(), unique );

  bound mass_bound = variables->bound_manager->get_bound( "sub_mass" ); 
  bound tau_bound = variables->bound_manager->get_bound( "sub_tau" ); 
  bound sub_mid = variables->bound_manager->get_bound( "sub_mid" );
  bound & analysis_bound = variables->analysis_bound;
  bound & spectator_bound = variables->spectator_bound;

  std::vector< double > mass_edges = mass_bound.get_edges();
  std::vector< double > tau_edges = tau_bound.get_edges();
  tau_edges[ 1 ] = ( tau_midpoint != 0 ) ? tau_midpoint : sub_mid.get_min();
  std::vector< double > analysis_edges = variables->analysis_bound.get_edges();
  std::vector< double > spectator_edges = variables->spectator_bound.get_edges();

  std::map< int, std::map< int, float > > weight_map_yexp;
  bool y_mode = false;
  if ( !y_values.empty() ){
    y_mode = true;
    weight_map_yexp = prepare_yexp_map( y_values, variables->analysis_bound );
  }

  std::map< int, float > weight_map;
  weight_map[11] = -1.0; 
  weight_map[12] = 1.0;
  weight_map[21] = 1.0;
  weight_map[22] = -1.0;
  weight_map[31] = 1.0;
  weight_map[32] = -1.0;
  weight_map[41] = -1.0;
  weight_map[42] = 1.0;

  std::cout << "Subtraction format: " << std::endl;
  std::cout << "Tau edges: " << tau_edges[0] << " " << tau_edges[1] << " " << tau_edges[2] << std::endl;
  std::cout << "Mass edges: " << mass_edges[0] << " " << mass_edges[1] << " " << mass_edges[2] << " " << mass_edges[3] << " " << mass_edges[4] << std::endl;
  

  auto sub_weight_func = [ &weight_map, &mass_edges, &tau_edges ]( const ROOT::RVecF & jpsi_mass, const ROOT::RVecF & jpsi_tau ){ 
    return subtraction_weight( jpsi_mass, jpsi_tau, weight_map, mass_edges, tau_edges );
  };

  auto sub_weight_yexp_func = [ &weight_map_yexp, &mass_edges, &tau_edges ]( const ROOT::RVecF & jpsi_mass, const ROOT::RVecF & jpsi_tau, const int & ana_bin ){
    return subtraction_weight( jpsi_mass, jpsi_tau, weight_map_yexp[ ana_bin ], mass_edges, tau_edges );
  };

  int analysis_lower = 0; int analysis_upper = analysis_bound.get_bins();
  auto analysis_floor_search_lambda = [ &analysis_edges, &analysis_lower, &analysis_upper, &analysis_bound]( const ROOT::RVecF & analysis_variable ){
    analysis_lower = 0; analysis_upper = analysis_bound.get_bins();
    return int( floor_search( analysis_variable, analysis_edges, analysis_lower, analysis_upper ) );
  };

  int spectator_lower = 0; int spectator_upper = spectator_bound.get_bins();
  auto spectator_floor_search_lambda = [ &spectator_edges, &spectator_lower, &spectator_upper, &spectator_bound]( const ROOT::RVecD & spectator_variable ){
    spectator_lower = 0; spectator_upper = spectator_bound.get_bins();
    return int( floor_search_double( spectator_variable, spectator_edges, spectator_lower, spectator_upper ) );
  };

  auto output_frame = input_frame.Define( "ana_bin", analysis_floor_search_lambda, { analysis_bound.get_var() } );
  output_frame = output_frame.Define( "spec_bin", spectator_floor_search_lambda, { spectator_bound.get_var() } );

  if( y_mode ){
    output_frame = output_frame.Define( "subtraction_weight", sub_weight_yexp_func, { "DiMuonMass", "DiMuonTau", "ana_bin" } ); 
  } else  {
    output_frame = output_frame.Define( "subtraction_weight", sub_weight_func, { "DiMuonMass", "DiMuonTau" } ); 
  }

  if ( !variables->extra_bounds.empty() ){
    auto filtered_frame = output_frame.Filter( variables->extra_bounds.at( 0 ).get_cut() );
    std::for_each( variables->extra_bounds.begin()+1, variables->extra_bounds.end(), [&filtered_frame]( bound & extra_bound ){ filtered_frame = filtered_frame.Filter( extra_bound.get_cut() ); } );
    filtered_frame.Snapshot( "tree", output_file, "" );
  } 
  else {
    output_frame.Snapshot( "tree", output_file, "" );
  }

  if ( !cutflow_path.empty() ){

    int bins = analysis_bound.get_bins();
    float min = analysis_bound.get_min();
    float max = analysis_bound.get_max();
     
    TH1F * subtraction_end_hist = new TH1F( "end_hist", "", bins, min, max );
    TFile * readin_file = new TFile( output_file.c_str(), "READ" );
    TTree * readin_tree = (TTree *) readin_file->Get( "tree" );
    readin_tree->Draw( "qtA>>sub_hist", "subtraction_weight" );

    cutflow * cutflow_subtraction = new cutflow();
    //int current_cut = cutflow_subtraction->cuts - 2;
    int current_cut = 12;// cutflow_subtraction->cuts - 2;

    std::vector< std::string > cutflow_vec;
    split_strings( cutflow_vec, cutflow_path, ":" );
    std::string cutflow_unique = Form(  "%s_%s", type.c_str(), cutflow_vec.at(1).c_str() );
    cutflow_subtraction->load_cutflow( cutflow_vec.at( 0 ), cutflow_unique ); 
    std::vector<std::string> subtraction_steps = { "subtraction" };
    cutflow_subtraction->expand_cutflow( subtraction_steps );
    cutflow_subtraction->set_dist( current_cut, subtraction_end_hist );
    cutflow_subtraction->write( Form( "%s_%s", type.c_str(), unique ), analysis_bound );
  
  }

  //return visualise( output_file, output_columns, type, variables->bound_manager, fileset->unique );

}
