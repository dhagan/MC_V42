#include <visualise.hxx>


void visualise( std::string & subtraction_path, std::vector<std::string> & vars, std::string & type, bound_mgr * bounds, std::string & unique ){

  if ( !unique.empty() ){ std::cout << unique << std::endl; }
  if ( bounds != nullptr ){ 
    std::cout << "bounds valid" << std::endl;
  }

  prep_style();
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  //std::vector<std::string> masses = { "Q0", "Q3", "Q4", "Q5", "Q12" };
  std::vector<std::string> masses = { "Q12" };
  std::vector< std::pair< std::string, std::string > >  var_pairs;
  std::vector< std::string > out_vars;;
  

  for ( size_t outer_idx = 0; outer_idx < vars.size(); outer_idx++ ){
    if ( vars[outer_idx].find( "subtraction_weight" ) != std::string::npos ) continue;
    if ( vars[outer_idx].find( "ana_bin" ) != std::string::npos ) continue;
    out_vars.push_back( vars[outer_idx] );
    for ( size_t inner_idx = outer_idx+1; inner_idx < vars.size(); inner_idx++ ){
      if ( vars[inner_idx].find( "subtraction_weight" ) != std::string::npos ) continue;
      var_pairs.push_back( std::pair<std::string, std::string>( vars[outer_idx], vars[inner_idx] ) );
    }
  }

  TFile * output_file = new TFile( subtraction_path.c_str(), "READ" );
  TTree * output_tree = (TTree *) output_file->Get( "tree" );

  bool var_comp = false;

  for ( std::string & mass : masses ){

    bound mass_bound = bounds->get_bound( mass );

    for ( std::string & out_var : out_vars ){

      bound var_bound = bounds->get_bound( out_var );
      TH1F * unweighted_hist = new TH1F( Form( "unweighted_%s", mass.c_str() ), "", var_bound.get_bins(), var_bound.get_min(), var_bound.get_max() );
      TH1F * weighted_hist = new TH1F( Form( "weighted_%s", mass.c_str() ), "", var_bound.get_bins(), var_bound.get_min(), var_bound.get_max() );
      TH1F * ratio_hist = new TH1F( Form( "ratio_%s", mass.c_str() ), "", var_bound.get_bins(), var_bound.get_min(), var_bound.get_max() );
      TH1F * delta_hist = new TH1F( Form( "delta_%s", mass.c_str() ), "", var_bound.get_bins(), var_bound.get_min(), var_bound.get_max() );

      output_tree->Draw( Form( "%s>>unweighted_%s", var_bound.get_var().c_str(), mass.c_str() ), 
                         mass_bound.get_cut().c_str(), "goff" );
      output_tree->Draw( Form( "%s>>weighted_%s", var_bound.get_var().c_str(), mass.c_str() ), Form( "subtraction_weight*(%s)",
                          mass_bound.get_cut().c_str() ), "goff" );

      delta_hist->Add( unweighted_hist, weighted_hist, 1.0, -1.0 );
      ratio_hist->Divide( unweighted_hist, weighted_hist, 1.0, 1.0 );


      TCanvas * visualise_canvas = new TCanvas( Form( "%s", out_var.c_str() ), "", 200, 200, 2000, 2000 );
      visualise_canvas->Divide( 2, 2 );
      gStyle->SetOptStat( "ourmei" );

      TPad * vis_pad = (TPad *) visualise_canvas->cd( 1 );
      unweighted_hist->Draw( "HIST E1" );
      hist_prep_axes( unweighted_hist );
      add_pad_title( vis_pad, Form("Unweighted %s", out_var.c_str() ), true );
      add_atlas_decorations( vis_pad, true, false );
      set_axis_labels( unweighted_hist, var_bound.get_x_str(), "Yield" );
      TPaveStats * unweighted_stats = make_stats( unweighted_hist );
      unweighted_stats->Draw();


      vis_pad = (TPad *) visualise_canvas->cd( 2 );
      weighted_hist->Draw( "HIST E1" ); 
      hist_prep_axes( weighted_hist );
      add_pad_title( vis_pad, Form("weighted %s", out_var.c_str() ), true );
      add_atlas_decorations( vis_pad, true, false );
      set_axis_labels( weighted_hist, var_bound.get_x_str(), "Yield" );
      TPaveStats * weighted_stats = make_stats( weighted_hist );
      weighted_stats->Draw();

      

      vis_pad = (TPad *) visualise_canvas->cd( 3 );
      delta_hist->Draw( "HIST E1" ); 
      hist_prep_axes( delta_hist );
      add_pad_title( vis_pad, Form("delta %s", out_var.c_str() ), true );
      add_atlas_decorations( vis_pad, true, false );
      set_axis_labels( delta_hist, var_bound.get_x_str(), "Yield" );
      TPaveStats * delta_stats = make_stats( delta_hist );
      delta_stats->Draw();


      vis_pad = (TPad *) visualise_canvas->cd( 4 );
      ratio_hist->Draw( "HIST E1" );
      hist_prep_axes( ratio_hist );
      add_pad_title( vis_pad, Form("ratio %s", out_var.c_str() ), true );
      add_atlas_decorations( vis_pad, true, false );
      set_axis_labels( ratio_hist, var_bound.get_x_str(), "Yield" );
      TPaveStats * ratio_stats = make_stats( ratio_hist );
      ratio_stats->Draw();

      visualise_canvas->SaveAs( Form( "./visualise/%s_%s_%s.png", type.c_str(), mass.c_str(), out_var.c_str() ) );
      delete unweighted_hist;
      delete weighted_hist;
      delete ratio_hist;
      delete delta_hist;
      delete visualise_canvas;

    }

    if ( !var_comp ){ continue; }

    for ( std::pair<std::string, std::string> & var_pair : var_pairs ){


      std::cout << var_pair.first << " " << var_pair.second << std::endl;
      bound x_bound = bounds->get_bound( var_pair.first );
      bound y_bound = bounds->get_bound( var_pair.second );


      TH2F * unweighted_hist = new TH2F( Form( "unweighted_%s", mass.c_str() ), "", x_bound.get_bins(), x_bound.get_min(), x_bound.get_max(), 
                                                           y_bound.get_bins(), y_bound.get_min(), y_bound.get_max() );

      TH2F * weighted_hist = new TH2F( Form( "weighted_%s", mass.c_str() ), "", x_bound.get_bins(), x_bound.get_min(), x_bound.get_max(), 
                                                         y_bound.get_bins(), y_bound.get_min(), y_bound.get_max() );

      TH2F * delta_hist = new TH2F( Form( "delta_%s", mass.c_str() ), "", x_bound.get_bins(), x_bound.get_min(), x_bound.get_max(), 
                                                           y_bound.get_bins(), y_bound.get_min(), y_bound.get_max() );

      TH2F * ratio_hist = new TH2F( Form( "ratio_%s", mass.c_str() ), "", x_bound.get_bins(), x_bound.get_min(), x_bound.get_max(), 
                                                           y_bound.get_bins(), y_bound.get_min(), y_bound.get_max() );


      std::string pair_name = Form( "%s_%s", x_bound.get_var().c_str(), y_bound.get_var().c_str() );

      output_tree->Draw( Form( "%s:%s>>unweighted_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), mass.c_str() ), 
                         mass_bound.get_cut().c_str(), "goff" );
      output_tree->Draw( Form( "%s:%s>>weighted_%s", y_bound.get_var().c_str(), x_bound.get_var().c_str(), mass.c_str() ), 
                         Form( "subtraction_weight*(%s)", mass_bound.get_cut().c_str() ), "goff" );
    
      delta_hist->Add( unweighted_hist, weighted_hist, 1.0, -1.0 );
      ratio_hist->Add( unweighted_hist, weighted_hist, 1.0, 1.0 );


      TCanvas * visualise_canvas = new TCanvas( Form( "%s", pair_name.c_str() ), "", 200, 200, 2000, 2000 );
      visualise_canvas->Divide( 2, 2 );

      TPad * vis_pad = (TPad *) visualise_canvas->cd( 1 );
      unweighted_hist->Draw( "COLZ" );
      auto unweighted_ltx = new TLatex();
      unweighted_ltx->SetTextSize( 0.025 );
      unweighted_ltx->DrawLatexNDC( 0.54, 0.77, Form("Correlation = %.3f", unweighted_hist->GetCorrelationFactor() ) );
      unweighted_ltx->DrawLatexNDC( 0.54, 0.74, Form("Covariance = %.3f", unweighted_hist->GetCovariance()) );
      hist_prep_text( unweighted_hist );
      add_pad_title( vis_pad, Form("Unweighted %s", pair_name.c_str() ), false );
      set_axis_labels( unweighted_hist, x_bound.get_x_str(), y_bound.get_x_str() );


      vis_pad = (TPad *) visualise_canvas->cd( 2 );
      weighted_hist->Draw( "COLZ" );
      auto weighted_ltx = new TLatex();
      weighted_ltx->SetTextSize( 0.025 );
      weighted_ltx->DrawLatexNDC( 0.54, 0.77, Form("Correlation = %.3f", weighted_hist->GetCorrelationFactor() ) );
      weighted_ltx->DrawLatexNDC( 0.54, 0.74, Form("Covariance = %.3f", weighted_hist->GetCovariance()) );
      hist_prep_text( weighted_hist );
      add_pad_title( vis_pad, Form("weighted %s", pair_name.c_str() ), false );
      set_axis_labels( weighted_hist, x_bound.get_x_str(), y_bound.get_x_str() );
      

      vis_pad = (TPad *) visualise_canvas->cd( 3 );
      delta_hist->Draw( "COLZ" );
      auto delta_ltx = new TLatex();
      delta_ltx->SetTextSize( 0.025 );
      delta_ltx->DrawLatexNDC( 0.54, 0.77, Form("Correlation = %.3f", delta_hist->GetCorrelationFactor() ) );
      delta_ltx->DrawLatexNDC( 0.54, 0.74, Form("Covariance = %.3f", delta_hist->GetCovariance()) );
      hist_prep_text( delta_hist );
      add_pad_title( vis_pad, Form("delta %s", pair_name.c_str() ), false );
      set_axis_labels( delta_hist, x_bound.get_x_str(), y_bound.get_x_str() );

      vis_pad = (TPad *) visualise_canvas->cd( 4 );
      ratio_hist->Draw( "COLZ" );
      auto ratio_ltx = new TLatex();
      ratio_ltx->SetTextSize( 0.025 );
      ratio_ltx->DrawLatexNDC( 0.54, 0.77, Form("Correlation = %.3f", ratio_hist->GetCorrelationFactor() ) );
      ratio_ltx->DrawLatexNDC( 0.54, 0.74, Form("Covariance = %.3f", ratio_hist->GetCovariance()) );
      hist_prep_text( ratio_hist );
      add_pad_title( vis_pad, Form("ratio %s", pair_name.c_str() ), false );
      set_axis_labels( ratio_hist, x_bound.get_x_str(), y_bound.get_x_str() );

      visualise_canvas->SaveAs( Form( "./visualise/%s_%s_%s.png", type.c_str(), mass.c_str(), pair_name.c_str() ) );
      
      delete weighted_hist;
      delete unweighted_hist;
      delete ratio_hist;
      delete delta_hist;


      
    }

  }

}
