#!/bin/bash 
 
executable=${exec_path}/subtraction
input_unique="base"
output_unique="base"
data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root 
sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
selections=${LIB_PATH}/share/hf_bounds.txt

mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null

## create tree
$executable -i ${bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" &
$executable -i ${sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" &
$executable -i ${bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" &
$executable -i ${data} -t data -u ${output_unique} -v ${selections} -a "qtA" -s "BDT"

popd >> /dev/null
