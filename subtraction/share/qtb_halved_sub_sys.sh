#!/bin/bash 
executable=${exec_path}/subtraction

selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/event_subtraction/sub_${output_unique}.txt

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
ranges=( "${ranges_1}" "${ranges_2}" ) 
unique=( "lower" "upper" )

## THESE ARE CORRECT
yexp_split_1="0.78447:0.74781:0.77952:0.80927:0.83749:0.77692:0.80010:0.78693:0.76213:0.76644:0.75170:0.80035:0.77653:0.79290:0.76675"
yexp_split_2="0.69336:0.81399:0.74518:0.80277:0.79018:0.79390:0.78243:0.78562:0.78201:0.76609:0.76938:0.74280:0.80731:0.76314:0.79370"
yexp_list=( "${yexp_split_1}" "${yexp_split_2}" )

## input
input_unique="base"
input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root 
input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root

for index in {0..1}; do

	qtb_range="${ranges[$index]}"
	
	sub_mass1_output_unique="qtb_${unique[$index]}_sub_mass1_sys"
	sub_mass1_ranges="sub_mass:DiMuonMass&4:2740.0:3460.0#${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_mass1_ranges}
	$executable -i ${input_sign} -t sign -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_mass1_ranges}
	$executable -i ${input_bckg} -t bckg -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_mass1_ranges}
	$executable -i ${input_data} -t data -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_mass1_ranges}
	popd >> /dev/null


	sub_tau1_output_unique="qtb_${unique[$index]}_sub_tau1_sys"
	sub_tau1_ranges="${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.81" -r ${sub_tau1_ranges}
	$executable -i ${input_sign} -t sign -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.81" -r ${sub_tau1_ranges}
	$executable -i ${input_bckg} -t bckg -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.81" -r ${sub_tau1_ranges}
	$executable -i ${input_data} -t data -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.81" -r ${sub_tau1_ranges}
	popd >> /dev/null


	sub_tau2_output_unique="qtb_${unique[$index]}_sub_tau2_sys"
	sub_tau2_ranges="${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.91" -r ${sub_tau2_ranges}
	$executable -i ${input_sign} -t sign -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.91" -r ${sub_tau2_ranges}
	$executable -i ${input_bckg} -t bckg -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.91" -r ${sub_tau2_ranges}
	$executable -i ${input_data} -t data -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -m "0.91" -r ${sub_tau2_ranges}
	popd >> /dev/null

	sub_yexp_output_unique="qtb_${unique[$index]}_sub_yexp_sys"
	yexp_string="${yexp_list[$index]}"
	sub_yexp_ranges="${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_yexp_ranges} -y ${yexp_string}  
	$executable -i ${input_sign} -t sign -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_yexp_ranges} -y ${yexp_string}
	$executable -i ${input_bckg} -t bckg -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_yexp_ranges} -y ${yexp_string}
	$executable -i ${input_data} -t data -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${sub_yexp_ranges} -y ${yexp_string}
	popd >> /dev/null




done
