#!/bin/bash 
 
executable=${exec_path}/subtraction
selections=${LIB_PATH}/share/hf_bounds.txt

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
ranges=( "${ranges_1}" "${ranges_2}" ) 
unique=( "lower" "upper" )

var_sys=( aaf afa faa extd noqt2Disc noL aaDPDY faDPDY ffDPDY )

for index in {0..1}; do

	qtb_range="${ranges[$index]}"

	for var in ${var_sys[@]}; do
	
		input_unique="base_${var}"
		output_unique="qtb_${unique[$index]}_${var}"

		input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root
		input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
		input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
		input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
	
		## create dirs and push
		mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
		mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
		mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
		pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null
	
		## create trees
		$executable -i ${input_bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${qtb_range}
		$executable -i ${input_sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${qtb_range}
		$executable -i ${input_bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${qtb_range}
		$executable -i ${input_data} -t data -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${qtb_range}
		
		
		popd >> /dev/null
	
	done

done

##number=4
##range="${ranges_4}"
##
##for var in ${var_sys[@]}; do
##
##	input_unique="${var}"
##	output_unique="qtbsplit-${number}_${input_unique}"
##
##	input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root
##	input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
##	input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
##	input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
##
##
##	## create dirs and push
##	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
##	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
##	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
##	pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null
##
##	## create trees
##	$executable -i ${input_bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	$executable -i ${input_sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	$executable -i ${input_bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	$executable -i ${input_data} -t data -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	
##	
##	popd >> /dev/null
##
##done
