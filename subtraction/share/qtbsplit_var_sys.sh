#!/bin/bash 
 
executable=${exec_path}/subtraction
selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/event_subtraction/sub_${output_unique}.txt

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )

var_sys=( aaf afa faa extd noqt2Disc noL aaDPDY faDPDY ffDPDY )

for index in {0..3}; do

	number=$(($index + 1))
	range="${ranges_list[$index]}"

	for var in ${var_sys[@]}; do
	
		input_unique="base_${var}"
		output_unique="qtbsplit-${number}_${var}"

		input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root
		input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
		input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
		input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
	
		## create dirs and push
		mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
		mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
		mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
		pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null
	
		$executable -i ${input_bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
		$executable -i ${input_sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
		$executable -i ${input_bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
		$executable -i ${input_data} -t data -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
		
		popd >> /dev/null
	
	done

done

##number=4
##range="${ranges_4}"
##
##for var in ${var_sys[@]}; do
##
##	input_unique="${var}"
##	output_unique="qtbsplit-${number}_${input_unique}"
##
##	input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root
##	input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
##	input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
##	input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
##
##
##	## create dirs and push
##	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
##	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
##	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
##	pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null
##
##	## create trees
##	$executable -i ${input_bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	$executable -i ${input_sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	$executable -i ${input_bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	$executable -i ${input_data} -t data -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
##	
##	
##	popd >> /dev/null
##
##done
