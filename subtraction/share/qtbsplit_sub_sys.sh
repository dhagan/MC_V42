#!/bin/bash 
executable=${exec_path}/subtraction

selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/event_subtraction/sub_${output_unique}.txt

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )

## NEED THE REST OF THESE
yexp_split_1="0.78203:0.72581:0.76098:0.81034:0.82040:0.76847:0.79868:0.77090:0.74835:0.75117:0.76679:0.76151:0.75860:0.80466:0.75292"
yexp_split_2="0.88259:0.79370:0.76314:0.78038:0.87695:0.78254:0.79297:0.80681:0.75077:0.78528:0.77584:0.83274:0.80375:0.76098:0.72880"
yexp_split_3="0.68098:0.70670:0.81516:0.83819:0.81786:0.78043:0.80775:0.78344:0.78771:0.76254:0.70690:0.81338:0.77024:0.81607:0.82983"
yexp_split_4="0.78203:0.72581:0.76098:0.81034:0.82040:0.76847:0.79868:0.77090:0.74835:0.75117:0.76679:0.76151:0.75860:0.80466:0.75292"
yexp_split_5="0.62996:0.84343:0.74095:0.77413:0.78886:0.79956:0.78347:0.77535:0.78150:0.75650:0.77072:0.73281:0.80721:0.73863:0.00000"
yexp_split_6="0.74951:0.94991:0.80272:0.80659:0.78518:0.84399:0.73730:0.80242:0.74773:0.80704:0.78465:0.76098:0.76059:0.62996:0.00000"
yexp_split_7="0.64850:0.75395:0.65097:0.78090:0.76722:0.81607:0.76314:0.74690:0.79370:0.71926:0.84343:0.61980:0.00000:0.00000:0.00000"
yexp_split_8="0.79370:0.00000:0.84343:0.72112:0.70949:0.91964:0.86305:0.74690:0.66943:0.58480:0.00000:0.00000:0.00000:0.00000:0.00000"
yexp_list=( "${yexp_split_1}" "${yexp_split_3}" "${yexp_split_3}" "${yexp_split_4}" "${yexp_split_5}" "${yexp_split_6}" "${yexp_split_7}" "${yexp_split_8}" )

## input
input_unique="base"
input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root 
input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root

for index in {0..3}; do

	qtb_range="${ranges_list[$index]}"
	number=$(($index + 1))
	
	sub_mass1_output_unique="qtbsplit-${number}_sub_mass1_sys"
	sub_mass1_ranges="DiMuonMass:DiMuonMass&100:2740.0:3460.0#${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_mass1_ranges}
	$executable -i ${input_sign} -t sign -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_mass1_ranges}
	$executable -i ${input_bckg} -t bckg -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_mass1_ranges}
	$executable -i ${input_data} -t data -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_mass1_ranges}
	popd >> /dev/null


	sub_tau1_output_unique="qtbsplit-${number}_sub_tau1_sys"
	sub_tau1_ranges="${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.81" -r ${sub_tau1_ranges}
	$executable -i ${input_sign} -t sign -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.81" -r ${sub_tau1_ranges}
	$executable -i ${input_bckg} -t bckg -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.81" -r ${sub_tau1_ranges}
	$executable -i ${input_data} -t data -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.81" -r ${sub_tau1_ranges}
	popd >> /dev/null


	sub_tau2_output_unique="qtbsplit-${number}_sub_tau2_sys"
	sub_tau2_ranges="${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.91" -r ${sub_tau2_ranges}
	$executable -i ${input_sign} -t sign -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.91" -r ${sub_tau2_ranges}
	$executable -i ${input_bckg} -t bckg -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.91" -r ${sub_tau2_ranges}
	$executable -i ${input_data} -t data -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -e "qtB" -m "0.91" -r ${sub_tau2_ranges}
	popd >> /dev/null

	sub_yexp_output_unique="qtbsplit-${number}_sub_yexp_sys"
	yexp_string="${yexp_list[$index]}"
	sub_yexp_ranges="${qtb_range}"
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique} >> /dev/null
	$executable -i ${input_bbbg} -t bbbg -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_yexp_ranges} -y ${yexp_string}  
	$executable -i ${input_sign} -t sign -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_yexp_ranges} -y ${yexp_string}
	$executable -i ${input_bckg} -t bckg -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_yexp_ranges} -y ${yexp_string}
	$executable -i ${input_data} -t data -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${sub_yexp_ranges} -y ${yexp_string}
	popd >> /dev/null




done
