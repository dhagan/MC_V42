#!/bin/bash 
 
executable=${exec_path}/subtraction
input_unique="base_dz02"
output_unique="base_dz02"
data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root 
sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
selections=${LIB_PATH}/share/hf_bounds.txt

mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null

## create tree
$executable -i ${bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" &
$executable -i ${sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" &
$executable -i ${bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" &
$executable -i ${data} -t data -u ${output_unique} -v ${selections} -a "qtA" -s "BDT"

## parity with original
##tree_data=${OUT_PATH}/event_subtraction/${unique}/data_base.root 
##tree_sign=${OUT_PATH}/event_subtraction/${unique}/sign_base.root
##tree_bckg=${OUT_PATH}/event_subtraction/${unique}/bckg_base.root
##$executable -i "${tree_sign}:${tree_bckg}:${tree_data}" -v ${selections} -a "qtA" -s "BDT" -u ${unique} -p

popd >> /dev/null
