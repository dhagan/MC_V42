#!/bin/bash 
 
executable=${exec_path}/subtraction

input_unique="base"
input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root 
input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
selections=${LIB_PATH}/share/hf_bounds.txt

sub_mass1_output_unique="base_sub_mass1_sys"
sub_mass1_ranges="sub_mass:DiMuonMass&4:2740.0:3460.0"
mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}
mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}/cutflow
mkdir -p ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique}/visualise
pushd ${OUT_PATH}/event_subtraction/${sub_mass1_output_unique} >> /dev/null
$executable -i ${input_bbbg} -t bbbg -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -s "BDT" -r ${sub_mass1_ranges}
$executable -i ${input_sign} -t sign -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -s "BDT" -r ${sub_mass1_ranges}
$executable -i ${input_bckg} -t bckg -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -s "BDT" -r ${sub_mass1_ranges} 
$executable -i ${input_data} -t data -u ${sub_mass1_output_unique} -v ${selections} -a "qtA" -s "BDT" -r ${sub_mass1_ranges}
popd >> /dev/null


sub_tau1_output_unique="base_sub_tau1_sys"
mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}
mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}/cutflow
mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique}/visualise
pushd ${OUT_PATH}/event_subtraction/${sub_tau1_output_unique} >> /dev/null
$executable -i ${input_bbbg} -t bbbg -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.81" 
$executable -i ${input_sign} -t sign -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.81"
$executable -i ${input_bckg} -t bckg -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.81"
$executable -i ${input_data} -t data -u ${sub_tau1_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.81"
popd >> /dev/null

sub_tau2_output_unique="base_sub_tau2_sys"
mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}
mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}/cutflow
mkdir -p ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique}/visualise
pushd ${OUT_PATH}/event_subtraction/${sub_tau2_output_unique} >> /dev/null
$executable -i ${input_bbbg} -t bbbg -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.91" 
$executable -i ${input_sign} -t sign -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.91"
$executable -i ${input_bckg} -t bckg -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.91"
$executable -i ${input_data} -t data -u ${sub_tau2_output_unique} -v ${selections} -a "qtA" -s "BDT" -m "0.91"
popd >> /dev/null

sub_yexp_output_unique="base_sub_yexp_sys"
## old Y vals
##y_vals="0.739719:0.77385:0.760145:0.804769:0.813582:0.786442:0.796945:0.790633:0.777149:0.771881:0.765521:0.775144:0.794001:0.784213:0.776001"
## new y vals
y_vals="0.901446:0.802792:0.822946:0.755878:0.827522:0.748066:0.77701:0.768229:0.739077:0.747116:0.728682:0.760118:0.746179:0.749078:0.819365"
mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}
mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}/cutflow
mkdir -p ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique}/visualise
pushd ${OUT_PATH}/event_subtraction/${sub_yexp_output_unique} >> /dev/null
$executable -i ${input_bbbg} -t bbbg -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -s "BDT" -y ${y_vals} 
$executable -i ${input_sign} -t sign -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -s "BDT" -y ${y_vals}
$executable -i ${input_bckg} -t bckg -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -s "BDT" -y ${y_vals}
$executable -i ${input_data} -t data -u ${sub_yexp_output_unique} -v ${selections} -a "qtA" -s "BDT" -y ${y_vals}
popd >> /dev/null
