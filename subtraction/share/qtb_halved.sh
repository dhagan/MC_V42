#!/bin/bash 
 
executable=${exec_path}/subtraction
input_unique=base

data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root 
sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/event_subtraction/sub_${output_unique}.txt

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
ranges=( "${ranges_1}" "${ranges_2}" ) 
unique=( "lower" "upper" )

for index in {0..1}; do

	## create specifics
	range="${ranges[$index]}"
	output_unique="qtb_${unique[$index]}"

	tree_data=${OUT_PATH}/event_subtraction/${output_unique}/data_${output_unique}.root 
	tree_sign=${OUT_PATH}/event_subtraction/${output_unique}/sign_${output_unique}.root
	tree_bckg=${OUT_PATH}/event_subtraction/${output_unique}/bckg_${output_unique}.root

	## create dirs and push
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null

	## create trees
	$executable -i ${bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${range} 
	$executable -i ${sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${range}
	$executable -i ${bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${range}
	$executable -i ${data} -t data -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -s "BDT" -r ${range}
	
	popd >> /dev/null

done
