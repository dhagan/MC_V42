#!/bin/bash 
 
executable=${exec_path}/subtraction
selections=${LIB_PATH}/share/hf_bounds.txt
var_sys=( aaf afa faa noqt2Disc noL aaDPDY faDPDY ffDPDY )

for var in ${var_sys[@]}; do
	
	input_unique="base_${var}"
	output_unique="${input_unique}"

	input_data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root
	input_sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
	input_bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
	input_bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
	
	## create dirs and push
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null
	
	## create trees
	$executable -i ${input_bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" 
	$executable -i ${input_sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -s "BDT"
	$executable -i ${input_bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -s "BDT"
	$executable -i ${input_data} -t data -u ${output_unique} -v ${selections} -a "qtA" -s "BDT"
	
	
	popd >> /dev/null
	
done
