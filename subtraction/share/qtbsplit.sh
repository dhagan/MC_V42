#!/bin/bash 
 
executable=${exec_path}/subtraction
input_unique=base

data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root 
sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root
bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root
bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root
selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/event_subtraction/sub_${output_unique}.txt


ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"

ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )

for index in {0..0}; do

	## create specifics
	range="${ranges_list[$index]}"
	number=$(($index + 1))
	output_unique="qtbsplit-${number}"
	echo ${output_unique}
	tree_data=${OUT_PATH}/event_subtraction/${output_unique}/data_${output_unique}.root 
	tree_sign=${OUT_PATH}/event_subtraction/${output_unique}/sign_${output_unique}.root
	tree_bckg=${OUT_PATH}/event_subtraction/${output_unique}/bckg_${output_unique}.root

	## create dirs and push
	touch ${log}
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/cutflow
	mkdir -p ${OUT_PATH}/event_subtraction/${output_unique}/visualise
	pushd ${OUT_PATH}/event_subtraction/${output_unique} >> /dev/null

	## create trees
	$executable -i ${bbbg} -t bbbg -u ${output_unique} -v ${selections} -a "qtA" -s "BDT" -e "qtB" -r ${range}
	$executable -i ${sign} -t sign -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
	$executable -i ${bckg} -t bckg -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
	$executable -i ${data} -t data -u ${output_unique} -v ${selections} -a "qtA" -e "qtB" -r ${range}
	
	## parity
	##$executable -i "${tree_sign}:${tree_bckg}:${tree_data}" -v ${selections} -a "qtA" -s "BDT" -u ${output_unique} -p -e "qtB" -r ${range}
	
	popd >> /dev/null

done
