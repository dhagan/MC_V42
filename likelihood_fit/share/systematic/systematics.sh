lhf="${exec_path}/likelihood_hists"
yaml="${ANA_IP}/likelihood_fit/share/systematic/systematics.yaml"

mkdir -p ${OUT_PATH}/trex/systematics/
pushd ${OUT_PATH}/trex/systematics/ >> /dev/null
likelihood_hists -i ${yaml}
popd >> /dev/null
