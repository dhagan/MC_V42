lhf="${exec_path}/likelihood_hists"
yaml="${ANA_IP}/likelihood_fit/share/full/full.yaml"

mkdir -p ${OUT_PATH}/trex/full/
pushd ${OUT_PATH}/trex/full/ >> /dev/null
$lhf -i ${yaml}
popd >> /dev/null
