#!/bin/bash
new=( aaf afa faa noqt2Disc noL aaDPDY faDPDY ffDPDY noL )

for var in ${new[@]}; do
	cp -r base_final_core base_final_${var}
	cd base_final_${var}
	sed -i "s/_ncr/_${var}/g" ./*
	sed -i "s/_final_core/_final_${var}/g" ./*
	./run.sh
	cd ..
done
