#!/bin/bash

trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"

yaml="${ANA_IP}/likelihood_fit/share/${unique}/config.yaml"
post_yaml="${ANA_IP}/likelihood_fit/share/${unique}/post_config.yaml"

unique="base_a"
mkdir -p ${OUT_PATH}/trex/tabling
pushd ${OUT_PATH}/trex/tabling >> /dev/null

for bin in {1..15}; do
	mkdir -p ${OUT_PATH}/trex/${unique}/qtA${bin}-15
	pushd ${OUT_PATH}/trex/${unique}/qtA${bin}-15 >> /dev/null
	popd >> /dev/null
done

