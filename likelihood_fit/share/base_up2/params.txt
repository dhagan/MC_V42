NormFactor: "mu"
	Title: "sign_mu"
  	Nominal: MU
  	Min: 0.001
  	Max: 15.0
	Constant: FALSE
  	Samples: "sign"

NormFactor: "spp"
	Title: "spp"
  	Nominal: PP
  	Min: 0.001
  	Max: 40.0
	Constant: FALSE
  	Samples: "bckg"

NormFactor: "dat"
	Title: "data"
  	Nominal: DATA
	Constant: TRUE
  	Samples: "data"
