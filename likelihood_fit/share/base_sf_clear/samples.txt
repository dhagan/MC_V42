Sample: "data"
  	Title: "Data 2015"
  	Type: DATA
	LineColor: 1

Sample: "sign"
  	Title: "MC Signal"
  	Type: SIGNAL
	LineColor: 2
	NormalizedByTheory: TRUE
	SeparateGammas: TRUE

Sample: "bckg"
  	Title: "MC Background"
  	Type: BACKGROUND
	LineColor: 4
	NormalizedByTheory: TRUE
	SeparateGammas: TRUE
