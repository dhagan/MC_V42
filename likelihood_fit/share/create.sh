#!/bin/bash
new=( aaf afa faa noqt2Disc noL aaDPDY faDPDY ffDPDY )

for var in ${new[@]}; do
	cp -r base_ncr_nosys "base_${var}"
	cd base_${var}
	sed -i "s/_ncr_nosys/_${var}/g" ./*.yaml
	sed -i "s/_ncr_nosys/_${var}/g" ./*.txt
	sed -i "s/_ncr_nosys/_${var}/g" ./*.sh
	sed -i "s/_ncr/_${var}/g" ./*.sh
	./run.sh
	cd ..
done
