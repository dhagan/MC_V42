%%Systematic: jpsi_weight
%%	%% sign
%%	Title: jpsi_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "jpsi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: SIGNAL
%%
%%Systematic: jpsi_weight
%%	%% sign
%%	Title: jpsi_weight
%%	HistoNameDown: "cr"
%%	NuisanceParameter: "jpsi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: CONTROL
%%
%%Systematic: jpsi_weight
%%	%% bckg
%%	Title: jpsi_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "jpsi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: SIGNAL
%%
%%Systematic: jpsi_weight
%%	%% bckg
%%	Title: jpsi_weight
%%	HistoNameDown: "cr"
%%	NuisanceParameter: "jpsi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: CONTROL
%%
%%Systematic: photon_weight
%%	%% sign
%%	Title: photon_weight
%%	HistoNameUp: "sr"
%%	NuisanceParameter: "photon_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: SIGNAL
%%
%%Systematic: photon_weight
%%	%% sign
%%	Title: photon_weight
%%	HistoNameUp: "cr"
%%	NuisanceParameter: "photon_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: CONTROL
%%
%%Systematic: photon_weight
%%	%% bckg
%%	Title: photon_weight
%%	HistoNameUp: "sr"
%%	NuisanceParameter: "photon_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: SIGNAL
%%
%%Systematic: photon_weight
%%	%% bckg
%%	Title: photon_weight
%%	HistoNameUp: "cr"
%%	NuisanceParameter: "photon_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: CONTROL

Systematic: photon_sf
	%% sign
	Title: photon_sf
	HistoNameUp: "sr"
	NuisanceParameter: "photon_sf"
 	Symmetrisation: ONESIDED
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: photon_sf
	%% sign
	Title: photon_sf
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "photon_sf"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: photon_sf
	%% bckg
	Title: photon_sf
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "photon_sf"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: photon_sf
	%% bckg
	Title: photon_sf
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "photon_sf"
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL

Systematic: msf
	%% sign
	Title: msf
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "msf"
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: msf
	%% sign
	Title: msf
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "msf"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: msf
	%% bckg
	Title: msf
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "msf"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: msf
	%% bckg
	Title: msf
	HistoNameUp: "cr"
	NuisanceParameter: "msf"
 	Symmetrisation: ONESIDED
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL


Systematic: mr
	Title: mr
	NuisanceParameter: reconstruction_mr
	Type: OVERALL
	Samples: sign, bckg
	OverallUp: 0.01
	OverallDown: -0.01
  	Regions: SIGNAL, CONTROL
