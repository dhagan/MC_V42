Sample: "data"
  	Title: "Data 2015"
  	Type: DATA
	LineColor: 1

Sample: "sign"
  	Title: "MC Signal"
  	Type: SIGNAL
	LineColorRGB: 175,0,0
	FillColorRGB: 175,0,0
	NormalizedByTheory: TRUE
	SeparateGammas: TRUE

Sample: "bckg"
  	Title: "MC Background"
  	Type: BACKGROUND
	LineColorRGB: 135,0,255
	FillColorRGB: 135,0,255
	NormalizedByTheory: TRUE
	SeparateGammas: TRUE
