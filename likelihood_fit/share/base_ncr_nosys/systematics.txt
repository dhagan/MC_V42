%%Systematic: jpsi_weight
%%	%% sign
%%	Title: jpsi_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "jpsi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: SIGNAL
%%
%%Systematic: jpsi_weight
%%	%% bckg
%%	Title: jpsi_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "jpsi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: SIGNAL
%%
%%Systematic: phot_weight
%%	%% sign
%%	Title: photon_weight
%%	HistoNameUp: "sr"
%%	NuisanceParameter: "photon_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: SIGNAL
%%
%%Systematic: phot_weight
%%	%% bckg
%%	Title: photon_weight
%%	HistoNameUp: "sr"
%%	NuisanceParameter: "photon_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: SIGNAL
%%
%%Systematic: dy_weight
%%	%% sign
%%	Title: dy_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "dy_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: SIGNAL
%%
%%Systematic: dy_weight
%%	%% bckg
%%	Title: dy_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "dy_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: SIGNAL
%%
%%Systematic: dphi_weight
%%	%% sign
%%	Title: dphi_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "dphi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: SIGNAL
%%
%%
%%Systematic: dphi_weight
%%	%% bckg
%%	Title: dphi_weight
%%	HistoNameDown: "sr"
%%	NuisanceParameter: "dphi_weight"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: SIGNAL
%%
%%Systematic: photon_sf
%%	%% sign
%%	Title: photon_sf
%%	HistoNameUp: "sr"
%%	NuisanceParameter: "photon_sf"
%% 	Symmetrisation: ONESIDED
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: Analysis
%%
%%Systematic: photon_sf
%%	%% bckg
%%	Title: photon_sf
%%	HistoNameUp: "sr"
%% 	Symmetrisation: ONESIDED
%%	NuisanceParameter: "photon_sf"
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: Analysis
%%
%%Systematic: msf
%%	%% sign
%%	Title: msf
%%	HistoNameUp: "sr"
%% 	Symmetrisation: ONESIDED
%%	NuisanceParameter: "msf"
%%	Type: HISTO
%%	Samples: sign
%%  	Regions: Analysis
%%
%%Systematic: msf
%%	%% bckg
%%	Title: msf
%%	HistoNameUp: "sr"
%% 	Symmetrisation: ONESIDED
%%	NuisanceParameter: "msf"
%%	Type: HISTO
%%	Samples: bckg
%%  	Regions: Analysis
%%
%%Systematic: mr
%%	Title: mr
%%	NuisanceParameter: reconstruction_mr
%%	Type: OVERALL
%%	Samples: sign, bckg
%%	OverallUp: 0.01
%%	OverallDown: -0.01
%%  	Regions: Analysis
