#!/bin/bash

trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"
unique="explore"
yaml="${ANA_IP}/likelihood_fit/share/${unique}/config.yaml"

mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i ${yaml}
popd >> /dev/null

for bin in {7..7}; do
	config="${OUT_PATH}/trex/${unique}/configs/qtA${bin}-15.config"
	mkdir -p ${OUT_PATH}/trex/${unique}/qtA${bin}-15
	pushd ${OUT_PATH}/trex/${unique}/qtA${bin}-15 >> /dev/null
	$trex hwdfp ${config}	
	popd >> /dev/null
done
