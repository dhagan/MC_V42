#!/bin/bash
new=( mass1 tau1 tau2 yexp )

for var in ${new[@]}; do
	cp -r base_final_core base_final_${var}
	cd base_final_${var}
	sed -i "s/_ncr/_${var}/g" ./*
	sed -i "s/_final_core/_final_${var}/g" ./*
	./run.sh
	cd ..
done
