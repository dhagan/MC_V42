lhf="${exec_path}/likelihood_hists"
yaml="${ANA_IP}/likelihood_fit/share/sf/sf.yaml"

mkdir -p ${OUT_PATH}/trex/sf/
pushd ${OUT_PATH}/trex/sf/ >> /dev/null
$lhf -i ${yaml}
popd >> /dev/null
