#!/bin/bash

trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"
unique="base"
yaml="${ANA_IP}/likelihood_fit/share/${unique}/config.yaml"
jpsi_yaml="${ANA_IP}/likelihood_fit/share/${unique}/jpsi_config.yaml"
photon_yaml="${ANA_IP}/likelihood_fit/share/${unique}/photon_config.yaml"
photon_sf_yaml="${ANA_IP}/likelihood_fit/share/${unique}/photon_sf_lower_config.yaml"
post_yaml="${ANA_IP}/likelihood_fit/share/${unique}/post_config.yaml"

##mkdir -p ${OUT_PATH}/trex/${unique}_jpsi_weight/hists
##pushd ${OUT_PATH}/trex/${unique}_jpsi_weight/ >> /dev/null
##$generation -i ${jpsi_yaml}
##popd >> /dev/null
##
##mkdir -p ${OUT_PATH}/trex/${unique}_photon_weight/hists
##pushd ${OUT_PATH}/trex/${unique}_photon_weight/ >> /dev/null
##$generation -i ${photon_yaml}
##popd >> /dev/null
##
##mkdir -p ${OUT_PATH}/trex/${unique}_photon_sf_lower/hists
##pushd ${OUT_PATH}/trex/${unique}_photon_sf_lower/ >> /dev/null
##$generation -i ${photon_sf_yaml}
##popd >> /dev/null
##
##mkdir -p ${OUT_PATH}/trex/${unique}/hists
##mkdir -p ${OUT_PATH}/trex/${unique}/configs
##mkdir -p ${OUT_PATH}/trex/${unique}/sf_vis
##mkdir -p ${OUT_PATH}/trex/${unique}/sf_var
##pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
##$generation -i ${yaml}
##popd >> /dev/null
##
##
##
####for bin in {6..6}; do
##for bin in {1..15}; do
for bin in {2..2}; do
##for bin in {12..12}; do
	config="${OUT_PATH}/trex/${unique}/configs/qtA${bin}-15.config"
	mkdir -p ${OUT_PATH}/trex/${unique}/qtA${bin}-15
	pushd ${OUT_PATH}/trex/${unique}/qtA${bin}-15 >> /dev/null
	$trex hwdfp ${config}	
	popd >> /dev/null
done

pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i ${post_yaml}
popd >> /dev/null

##echo "h"
###$trex h ${config}
##echo "w"
##$trex w ${config}
##echo "d"
##$trex d ${config}
##echo "f"
##$trex f ${config}
##echo "p"
##$trex p ${config}
##echo "done"
####cp -r ./explore/Fits/explore.txt "./${idx}.txt"
