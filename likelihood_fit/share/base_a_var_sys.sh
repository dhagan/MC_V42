#! /bin/bash

uniques=( "base_a_aaf" "base_a_afa" "base_a_faa" "base_a_extd" "base_a_noqt2Disc" "base_a_noL" "base_a_aaDPDY" "base_a_faDPDY" "base_a_ffDPDY" )

for unique_index in {0..8};
do
	unique="${uniques[$unique_index]}"
	pushd ${unique} >> /dev/null
	./run.sh
	popd >> /dev/null
done
