#!/bin/bash

trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"
unique="base_a"
yaml="${ANA_IP}/likelihood_fit/share/${unique}/config.yaml"
post_yaml="${ANA_IP}/likelihood_fit/share/${unique}/post_config.yaml"

mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
mkdir -p ${OUT_PATH}/trex/${unique}/weights
mkdir -p ${OUT_PATH}/trex/${unique}/musf
mkdir -p ${OUT_PATH}/trex/${unique}/musf/stat_variations/
mkdir -p ${OUT_PATH}/trex/${unique}/musf/error/
mkdir -p ${OUT_PATH}/trex/${unique}/musf/systematic/
mkdir -p ${OUT_PATH}/trex/${unique}/photon_sf 
mkdir -p ${OUT_PATH}/trex/${unique}/photon_sf/error/
mkdir -p ${OUT_PATH}/trex/${unique}/photon_sf/systematic/
mkdir -p ${OUT_PATH}/trex/${unique}/jpsi_weight 
mkdir -p ${OUT_PATH}/trex/${unique}/jpsi_weight/error/
mkdir -p ${OUT_PATH}/trex/${unique}/jpsi_weight/systematic/
mkdir -p ${OUT_PATH}/trex/${unique}/photon_weight 
mkdir -p ${OUT_PATH}/trex/${unique}/photon_weight/error/
mkdir -p ${OUT_PATH}/trex/${unique}/photon_weight/systematic/

pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null 
$generation -i ${yaml} 
popd >> /dev/null

for bin in {1..15}; do
	config="${OUT_PATH}/trex/${unique}/configs/qtA${bin}-15.config"
	mkdir -p ${OUT_PATH}/trex/${unique}/qtA${bin}-15
	pushd ${OUT_PATH}/trex/${unique}/qtA${bin}-15 >> /dev/null
	$trex hwdf ${config}
	$trex pra ${config}
	popd >> /dev/null
done

pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i ${post_yaml}
popd >> /dev/null
