Sample: "data"
  	Title: "Data 2015"
  	Type: DATA
	LineColor: 1

Sample: "sign"
  	Title: "MC Signal"
  	Type: SIGNAL
	LineColor: 2
	NormalizedByTheory: TRUE
	BuildPullTable: NORM+SHAPE
	SeparateGammas: TRUE

Sample: "bckg"
  	Title: "MC Background"
  	Type: BACKGROUND
	LineColor: 4
	BuildPullTable: NORM+SHAPE
	NormalizedByTheory: TRUE
	SeparateGammas: TRUE
