  	POI: mu,spp
	Logo: FALSE
	MCstatThreshold: 0.01
  	MCstatConstraint: "Poisson"
	SystPruningShape: 0.005
 	SystPruningNorm: 0.005
	ReadFrom: HIST
	DebugLevel: 1
	HistoChecks: NOCRASH
	ImageFormat: "png"
	ApplyGammaCorrection: TRUE	
	PlotOptions: CHI2
	PlotOptionsSummary: CHI2
  	GetChi2: TRUE	
	SystCategoryTables: TRUE
	SystControlPlots: TRUE
	SystDataPlots: TRUE
	SystErrorBars: TRUE
	CleanTables: TRUE
	CmeLabel: "13 TeV"
  	LumiLabel: "2.5 fb^{-1}"
  	RankingPlot: "all"
  	AtlasLabel: "Work In Progress"
	DoSummaryPlot: TRUE
	DoMergedPlot: TRUE
  	DoTables: TRUE
  	DoSignalRegionsPlot: TRUE
  	DoPieChartPlot: TRUE
	DoSystNormalizationPlots: TRUE
	UseGammasForCorr: TRUE
	UseATLASRounding: TRUE
	UseGammaPulls: TRUE
	PrePostFitCanvasSize: 750, 750
	SummaryCanvasSize: 750, 750
	LegendNColumns: 1
	LegendNColumnsSummary: 1
	LegendNColumnsMerge: 1
	LegendX1: 0.7
	LegendX1Summary: 0.7
	LegendX1Merge: 0.8
	%%CombinerFormat: TRUE




Fit: "fit"
  	FitType: SPLUSB
  	FitRegion: CRSR
	GetGoodnessOfFit: TRUE
	ToleranceScale: 0.5
	MaximumNumberFCNcalls: 50000
	doLHscan: mu,spp
	doImpactscan: alpha_mureco,mu:alpha_photon_sf,mu:alpha_mureco,spp:alpha_photon_sf,spp
	doLHscan: mu,spp
	LHscanMin: 0
	LHscanMax: 5

Region: "SIGNAL"
  	Type: SIGNAL
	HistoName: "sr"
  	Label: "Signal region"
  	ShortLabel: "Sign"
	Binning: "AutoBin","TransfoD",3.,3.

Region: "CONTROL"
  	Type: CONTROL
	HistoName: "cr"
  	Label: "Control region"
  	ShortLabel: "ctrl"
	Binning: "AutoBin","TransfoD",3.,3.
