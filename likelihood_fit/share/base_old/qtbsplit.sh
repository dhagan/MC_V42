#!/bin/bash
#
trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"
selections=${LIB_PATH}/share/hf_bounds.txt

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )


for index in {0..0}; do

	## create specifics
	range="${ranges_list[$index]}"
	number=$(($index + 1))
	output_unique="qtbsplit-${number}"
	input_unique="qtbsplit-${number}"
	input_sub=${OUT_PATH}/event_subtraction/${input_unique}/
	statistics=${OUT_PATH}/event_hf/${input_unique}/eval/stat_${input_unique}.root


	mkdir -p ${OUT_PATH}/trex/${output_unique}/hists
	mkdir -p ${OUT_PATH}/trex/${output_unique}/configs
	pushd ${OUT_PATH}/trex/${output_unique}/ >> /dev/null
	##$generation -i "${input_sub}" -a "qtA" -s "BDT" -c "qtB" -u ${output_unique} -v ${selections} -m "gen"
	$generation -i "${input_sub}" -a "qtA" -s "BDT" -c "qtB" -u ${output_unique} -v ${selections} -m "script" -S ${statistics}
	popd >> /dev/null

	for bin in {1..15}; do
		
		hist_config="${OUT_PATH}/trex/${output_unique}/configs/${output_unique}_hist_2R_qtA${bin}-15.config"
		mkdir -p ${OUT_PATH}/trex/${output_unique}/${output_unique}_2R_qtA${bin}-15
		pushd ${OUT_PATH}/trex/${output_unique}/${output_unique}_2R_qtA${bin}-15 >> /dev/null
		$trex hwdfp ${hist_config}
		popd >> /dev/null
	
	done

	##pushd ${OUT_PATH}/trex/${output_unique}/ >> /dev/null
	##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -m "postprocess" -t .

	## WHATS THE PROBELM??
	## Basically, need to make sure the uniques in the configs are updated with the unique call, this is naming the output trexfile as 'base'
	## All those converging fits need reprocessed.
	## Need to do more to guarantee convergence.

	##popd >> /dev/null

done
