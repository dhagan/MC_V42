#!/bin/bash
generation="${exec_path}/likelihood_hists"

selections=${LIB_PATH}/share/hf_bounds.txt
ranges="BDT:BDT&50:-1:1"



outer_unique="base"
fragments=( aaf afa faa extd noqt2Disc noL aaDPDY faDPDY ffDPDY )

for unique_fragment in ${fragments[@]}; do
	unique="${outer_unique}_${unique_fragment}"
	input_sub=${OUT_PATH}/event_subtraction/${unique}/
	statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
	mkdir -p ${OUT_PATH}/trex/${unique}/hists
	mkdir -p ${OUT_PATH}/trex/${unique}/configs
	pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
	$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
	popd >> /dev/null
done

