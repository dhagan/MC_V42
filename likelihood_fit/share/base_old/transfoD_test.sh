#!/bin/bash
#
trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"

unique="base"
input_sub=${OUT_PATH}/event_subtraction/${unique}/
selections=${LIB_PATH}/share/hf_bounds.txt
statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
ranges="BDT:BDT&50:-1:1"



dir_unique="transfoD_2"
mkdir -p ${OUT_PATH}/trex/${dir_unique}/hists
mkdir -p ${OUT_PATH}/trex/${dir_unique}/configs
pushd ${OUT_PATH}/trex/${dir_unique}/ >> /dev/null
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "script" -S ${statistics}
popd >> /dev/null

for bin in {1..15}; do
	
	hist_config="${OUT_PATH}/trex/${dir_unique}/configs/${unique}_hist_2R_qtA${bin}-15.config"
	mkdir -p ${OUT_PATH}/trex/${dir_unique}/${unique}_2R_qtA${bin}-15
	pushd ${OUT_PATH}/trex/${dir_unique}/${unique}_2R_qtA${bin}-15 >> /dev/null
	$trex hwdfp ${hist_config}
	popd >> /dev/null

done

##pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "postprocess" -t .
##popd >> /dev/null



