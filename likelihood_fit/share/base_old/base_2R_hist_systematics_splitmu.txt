Systematic: phot_sf
	%% sign
	Title: phot_sf
	HistoNameUp: "sr0"
	NuisanceParameter: "phot_sf"
 	Symmetrisation: ONESIDED
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: phot_sf
	%% sign
	Title: phot_sf
	HistoNameUp: "cr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_sf"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: phot_sf
	%% bckg
	Title: phot_sf
	HistoNameUp: "sr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_sf"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: phot_sf
	%% bckg
	Title: phot_sf
	HistoNameUp: "cr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_sf"
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL




Systematic: muon_pl_sf
	%% sign
	Title: muon_pl_sf
	HistoNameUp: "sr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_pl_sf"
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: muon_pl_sf
	%% sign
	Title: muon_pl_sf
	HistoNameUp: "cr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_pl_sf"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: muon_pl_sf
	%% bckg
	Title: muon_pl_sf
	HistoNameUp: "sr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_pl_sf"
	NuisanceParameter: "muon_pl_sf"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: muon_pl_sf
	%% bckg
	Title: muon_pl_sf
	HistoNameUp: "cr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_pl_sf"
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL





Systematic: muon_mi_sf
	%% sign
	Title: muon_mi_sf
	HistoNameUp: "sr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_mi_sf"
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: muon_mi_sf
	%% sign
	Title: muon_mi_sf
	HistoNameUp: "cr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_mi_sf"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: muon_mi_sf
	%% bckg
	Title: muon_mi_sf
	HistoNameUp: "sr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_mi_sf"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: muon_mi_sf
	%% bckg
	Title: muon_mi_sf
	HistoNameUp: "cr0"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_mi_sf"
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL
