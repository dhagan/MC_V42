#!/bin/bash
generation="${exec_path}/likelihood_hists"

selections=${LIB_PATH}/share/hf_bounds.txt
ranges="BDT:BDT&50:-1:1"




##sub_sys_list=( "sub_mass1_sys" "sub_tau1_sys" "sub_tau2_sys" "sub_yexp_sys" )
unique="base_sub_tau1_sys"
input_sub=${OUT_PATH}/event_subtraction/${unique}/
statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
popd >> /dev/null


unique="base_sub_tau2_sys"
input_sub=${OUT_PATH}/event_subtraction/${unique}/
statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
popd >> /dev/null

unique="base_sub_tau1_sys"
input_sub=${OUT_PATH}/event_subtraction/${unique}/
statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
popd >> /dev/null

unique="base_sub_mass1_sys"
input_sub=${OUT_PATH}/event_subtraction/${unique}/
statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
popd >> /dev/null

unique="base_sub_yexp_sys"
input_sub=${OUT_PATH}/event_subtraction/${unique}/
statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
popd >> /dev/null
