#!/bin/bash
#
trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"
output_unique="base_scale"
input_unique="base"
input_sub=${OUT_PATH}/event_subtraction/${input_unique}/
selections=${LIB_PATH}/share/hf_bounds.txt
statistics=${OUT_PATH}/event_hf/${input_unique}/eval/stat_${input_unique}.root

mkdir -p ${OUT_PATH}/trex/${output_unique}/hists
mkdir -p ${OUT_PATH}/trex/${output_unique}/configs
pushd ${OUT_PATH}/trex/${output_unique}/ >> /dev/null
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${input_unique} -v ${selections} -m "gen" -e "scale"
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${input_unique} -v ${selections} -m "script" -e "scale" -S ${statistics}
popd >> /dev/null

for bin in {1..1}; do
	
	hist_config="${OUT_PATH}/trex/${output_unique}/configs/${output_unique}_hist_2R_qtA${bin}-15.config"
	mkdir -p ${OUT_PATH}/trex/${output_unique}/${output_unique}_2R_qtA${bin}-15
	pushd ${OUT_PATH}/trex/${output_unique}/${output_unique}_2R_qtA${bin}-15 >> /dev/null
	$trex hwdfp ${hist_config}
	popd >> /dev/null

done


##pushd ${OUT_PATH}/trex/${output_unique}/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -m "postprocess" -t .
##popd >> /dev/null



