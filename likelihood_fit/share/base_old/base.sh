#!/bin/bash
#
trex="${exec_path}/trex-fitter"
generation="${exec_path}/likelihood_hists"
unique="base"
input_sub=${OUT_PATH}/scalefactors/${unique}/
##input_sub=${OUT_PATH}/event_subtraction/${unique}/
selections=${LIB_PATH}/share/hf_bounds.txt
statistics=${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root
ranges="BDT:BDT&50:-1:1"

mkdir -p ${OUT_PATH}/trex/${unique}/hists
mkdir -p ${OUT_PATH}/trex/${unique}/configs
pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "gen" -r ${ranges}
$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "script" -S ${statistics}
popd >> /dev/null
##
##
##
#### base_sf_phot_lower
##mkdir -p ${OUT_PATH}/trex/${unique}_photon_sf_lower/hists
##pushd ${OUT_PATH}/trex/${unique}_photon_sf_lower/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_photon_sf_lower -v ${selections} -m "gen" -r ${ranges} -w "photon_lower_sf*muon_sf"
##popd >> /dev/null
##
#### base_jpsi_weight
##mkdir -p ${OUT_PATH}/trex/${unique}_jpsi_weight/hists
##pushd ${OUT_PATH}/trex/${unique}_jpsi_weight/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_jpsi_weight -v ${selections} -m "gen" -r ${ranges} -e "jpsi_weight"
##popd >> /dev/null
##
#### base_phot_weight
##mkdir -p ${OUT_PATH}/trex/${unique}_phot_weight/hists
##pushd ${OUT_PATH}/trex/${unique}_phot_weight/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_phot_weight -v ${selections} -m "gen" -r ${ranges} -e "phot_weight"
##popd >> /dev/null


##for bin in {1..15}; do
for bin in {7..7}; do
	
	hist_config="${OUT_PATH}/trex/${unique}/configs/${unique}_hist_2R_qtA${bin}-15.config"
	mkdir -p ${OUT_PATH}/trex/${unique}/${unique}_2R_qtA${bin}-15
	pushd ${OUT_PATH}/trex/${unique}/${unique}_2R_qtA${bin}-15 >> /dev/null
	$trex hwdfp ${hist_config}
	popd >> /dev/null

done
##
##pushd ${OUT_PATH}/trex/${unique}/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -v ${selections} -m "postprocess" -t .
##popd >> /dev/null













## OLDER ITERATIONS
## base_pu_weight
##mkdir -p ${OUT_PATH}/trex/${unique}_pu_weight/hists
##pushd ${OUT_PATH}/trex/${unique}_pu_weight/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique}_pu_weight -v ${selections} -m "gen" -r ${ranges} -P
##popd >> /dev/null

#### base_sf_phot_upper
##mkdir -p ${OUT_PATH}/trex/${unique}_photon_sf_upper/hists
##pushd ${OUT_PATH}/trex/${unique}_photon_sf_upper/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_photon_sf_upper -v ${selections} -m "gen" -r ${ranges} -w "photon_upper_sf*SF"
##popd >> /dev/null


#### base_sf_muon_upper
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_sf_upper/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_sf_upper/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_sf_upper -v ${selections} -m "gen" -r ${ranges} -w "photon_id_sf*SFUpper"
##popd >> /dev/null
##
#### base_sf_muon_lower
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_sf_lower/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_sf_lower/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_sf_lower -v ${selections} -m "gen" -r ${ranges} -w "photon_id_sf*SFLower"
##popd >> /dev/null



#### base_sf_muon_pl_upper
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_sf_upper/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_sf_upper/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_sf_upper -v ${selections} -m "gen" -r ${ranges} -w "photon_upper_sf*mu_pl_sf_upper*mu_mi_sf_upper*mu_dr_sf"
##popd >> /dev/null
##
#### base_sf_muon_pl_lower
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_pl_sf_lower/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_pl_sf_lower/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_sf_lower -v ${selections} -m "gen" -r ${ranges} -w "photon_lower_sf*mu_pl_sf_lower*mu_mi_sf_lower*mu_dr_sf"
##popd >> /dev/null
##


## base_sf_muon_pl_upper
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_pl_sf_upper/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_pl_sf_upper/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_pl_sf_upper -v ${selections} -m "gen" -r ${ranges} -w "photon_upper_sf*mu_pl_sf_upper*mu_mi_sf*mu_dr_sf"
##popd >> /dev/null
##
#### base_sf_muon_pl_lower
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_pl_sf_lower/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_pl_sf_lower/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_pl_sf_lower -v ${selections} -m "gen" -r ${ranges} -w "photon_lower_sf*mu_pl_sf_lower*mu_mi_sf*mu_dr_sf"
##popd >> /dev/null
##
#### base_sf_muon_mi_upper
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_mi_sf_upper/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_mi_sf_upper/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_mi_sf_upper -v ${selections} -m "gen" -r ${ranges} -w "photon_upper_sf*mu_pl_sf*mu_mi_sf_upper*mu_dr_sf"
##popd >> /dev/null
##
#### base_sf_muon_mi_lower
##mkdir -p ${OUT_PATH}/trex/${unique}_muon_mi_sf_lower/hists
##pushd ${OUT_PATH}/trex/${unique}_muon_mi_sf_lower/ >> /dev/null
##$generation -i "${input_sub}" -a "qtA" -s "BDT" -u ${unique} -o ${unique}_muon_mi_sf_lower -v ${selections} -m "gen" -r ${ranges} -w "photon_lower_sf*mu_pl_sf*mu_mi_sf_lower*mu_dr_sf"
##popd >> /dev/null






