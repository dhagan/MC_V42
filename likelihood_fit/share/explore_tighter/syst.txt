Systematic: jpsi_weight
	%% sign
	Title: jpsi_weight
	HistoNameUp: "sr"
	NuisanceParameter: "jpsi_weight"
 	Symmetrisation: ONESIDED
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: jpsi_weight
	%% sign
	Title: jpsi_weight
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "jpsi_weight"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: jpsi_weight
	%% bckg
	Title: jpsi_weight
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "jpsi_weight"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: jpsi_weight
	%% bckg
	Title: jpsi_weight
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "jpsi_weight"
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL

Systematic: phot_weight
	%% sign
	Title: phot_weight
	HistoNameUp: "sr"
	NuisanceParameter: "phot_weight"
 	Symmetrisation: ONESIDED
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: phot_weight
	%% sign
	Title: phot_weight
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_weight"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: phot_weight
	%% bckg
	Title: phot_weight
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_weight"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: phot_weight
	%% bckg
	Title: phot_weight
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_weight"
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL

Systematic: phot_sf
	%% sign
	Title: phot_sf
	HistoNameUp: "sr"
	NuisanceParameter: "phot_sf"
 	Symmetrisation: ONESIDED
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: phot_sf
	%% sign
	Title: phot_sf
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_sf"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: phot_sf
	%% bckg
	Title: phot_sf
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_sf"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: phot_sf
	%% bckg
	Title: phot_sf
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "phot_sf"
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL

Systematic: muon_sf
	%% sign
	Title: muon_sf
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_sf"
	Type: HISTO
	Samples: sign
  	Regions: SIGNAL

Systematic: muon_sf
	%% sign
	Title: muon_sf
	HistoNameUp: "cr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_sf"
	Type: HISTO
	Samples: sign
  	Regions: CONTROL

Systematic: muon_sf
	%% bckg
	Title: muon_sf
	HistoNameUp: "sr"
 	Symmetrisation: ONESIDED
	NuisanceParameter: "muon_sf"
	Type: HISTO
	Samples: bckg
  	Regions: SIGNAL

Systematic: muon_sf
	%% bckg
	Title: muon_sf
	HistoNameUp: "cr"
	NuisanceParameter: "muon_sf"
 	Symmetrisation: ONESIDED
	Type: HISTO
	Samples: bckg
  	Regions: CONTROL


Systematic: mureco
	Title: mureco
	NuisanceParameter: mureco
	Type: OVERALL
	Samples: sign, bckg
	OverallUp: 0.01
	OverallDown: -0.01
  	Regions: SIGNAL, CONTROL

