#include <func_hf.hxx>

#ifndef fast_hf_hxx
#define fast_hf_hxx

struct generate_settings {
  std::string pt, sf, pu;
  bool zero = false;
  std::vector< region > regions;
  std::vector< std::string > validation_variables;
  const std::vector< const char * > region_out_str = { "sr", "cr" };
  generate_settings( const std::string & pt, const std::string & sf, const std::string & pu, bool zero, 
                    const std::string & sr_type, const std::string & cr_type ){
    this->pt = pt;
    this->sf = sf;
    this->pu = pu;
    this->zero = zero;
    regions.push_back( region_convert( sr_type ) ); 
    regions.push_back( region_convert( cr_type ) ); 
  }
};

void generate_hists( basic_fileset * fileset, variable_set & variables, generate_settings settings );
void generate_hists( YAML::Node & run_node, variable_set & variables);

#endif
