#include <func_hf.hxx>
#include <yaml-cpp/yaml.h>

#ifndef trexsys_hxx
#define trexsys_hxx

TH1F * systematic( YAML::Node & run_node, variable_set & variables, bool return_uniform = false );
TH1F * systematic( const std::string & base_unique, const std::string & group_unique,
                    std::vector<std::string> systematic_uniques, variable_set & variables, 
                    bool return_uniform = false );

#endif
