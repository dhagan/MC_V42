#include <func_hf.hxx>
#include <yaml-cpp/yaml.h>

void postprocess( const std::string & output_unique, std::string & input_unique, std::string & input_filepath, 
                    variable_set & variables, const std::string & trex_path, const std::string & nonstat_unique,
                    const std::string & eff_unique );

void fit_visualise( const std::string & output_unique, variable_set & variables );

void wv( basic_fileset * fileset, const std::string & output_unique, variable_set & variables );
void wv_gen_full( basic_fileset * fileset, const std::string & output_unique, variable_set & variables );
void wv_gen_diff( basic_fileset * fileset, const std::string & output_unique, variable_set & variables );
void sf_explore( basic_fileset * fileset, const std::string & output_unique, variable_set & variables );