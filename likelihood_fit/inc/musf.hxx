#include <func_hf.hxx>

#ifndef musf_hf_hxx
#define musf_hf_hxx

void musf( YAML::Node & run_node, variable_set & variables, sample_type type, region region );
void musf_input( YAML::Node & run_node, variable_set & variables, region sr, region cr );

#endif
