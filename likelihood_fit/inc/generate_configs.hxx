#include <func_hf.hxx>

#ifndef trex_config_hxx
#define trex_config_hxx

#include "generate_hists.hxx"

void generate_configs( YAML::Node & run_node, variable_set & variables );

#endif
