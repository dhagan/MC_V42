#include <func_hf.hxx>
#include <yaml-cpp/yaml.h>

void trex_data_dump( YAML::Node run_node, variable_set & variables, basic_fileset * fileset, bool systematics=true );