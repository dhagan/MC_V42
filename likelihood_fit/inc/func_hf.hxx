
#ifndef func_hf_hxx
#define func_hf_hxx

#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "RooCategory.h"
#include "Roo1DTable.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCBShape.h"
#include "RooAbsReal.h"
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooPolynomial.h"
#include "RooDataHist.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include <ROOT/RDataFrame.hxx>

#include <TObject.h>

#include <anna.hxx>

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;

struct measure{
  Measurement measure;
  RooStats::HistFactory::Channel channel;
  RooStats::HistFactory::Sample sign, bckg;
  RooWorkspace * wkspc;
  RooAbsPdf * pdf;
  RooAbsData * data;
};

measure produce_measurement( TH1F * sign, TH1F * bckg, TH1F * data );
void pos_hists( TH1F * data, TH1F * sign, TH1F * bckg );
TH1F * output_histogram( RooFitResult * results, TH1F * sign, TH1F * bckg, TH1F * data, std::string & output_string, fit_results & fit_stats );

#include <yaml-cpp/yaml.h>

inline float uniform_error( float  max, float min ){
  return ( max - min )*0.57735026919;
}
inline float absolute_quadrature_two( float left, float right){
  return std::sqrt( left*left + right*right );
}

std::tuple< TH1F *, hist_group > get_result( const std::string & sys_unique, variable_set & variables );

//void add_pad( TH1F * hist, const std::string & name, const std::string & y_axis, bound & bound ){
//  int current_pad_number = static_cast<TPad*>(gPad)->GetNumber();
//  TPad * next_pad = static_cast<TPad*>( gPad->GetCanvas()->cd( ++current_pad_number ) );
//  hist->Draw( "HIST E1" );
//  hist_prep_axes(  hist, true );
//  set_axis_labels( hist, bound.get_ltx().c_str(), y_axis.c_str() );
//  add_pad_title( next_pad, name.c_str(), true );
//  add_atlas_decorations( next_pad, true, false );
//}

#endif
