#include <func_hf.hxx>
#include <yaml-cpp/yaml.h>

void quickplot( YAML::Node & run_node, variable_set & variables );
void quickplot_nr( YAML::Node & run_node, variable_set & variables );
