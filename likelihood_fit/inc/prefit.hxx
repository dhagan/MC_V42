#include <func_hf.hxx>

#ifndef prefit_hf_hxx
#define prefit_hf_hxx

void prefit_visualise( const std::string & unique, variable_set & variables );
void sf_variation( const std::string & unique, variable_set & variables );
void image_scalefactors( basic_fileset * fileset, variable_set & variables );
void image_bloom( basic_fileset * fileset, variable_set & variables );

#endif
