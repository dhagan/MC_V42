#include <func_hf.hxx>

void generate_sr( std::string & input_file, std::string & abin_var, std::string & spec_var,
         std::string & cuts, std::string & unique, bound_mgr * selections );
