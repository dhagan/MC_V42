
#include <func_hf.hxx>
#include <yaml-cpp/yaml.h>

#ifndef variation_hf_hxx
#define variation_hf_hxx

void variation( YAML::Node & run_node, variable_set & variables, sample_type type, region region );
void variation_input( YAML::Node & run_node, variable_set & variables, region sr, region cr );

#endif
