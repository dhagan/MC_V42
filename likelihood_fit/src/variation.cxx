#include <variation.hxx>

void add_spec_pad( TH1F * hist, const std::string & name, const std::string & y_axis, variable_set & variables ){
  int current_pad_number = static_cast<TPad*>(gPad)->GetNumber();
  TPad * next_pad = static_cast<TPad*>( gPad->GetCanvas()->cd( ++current_pad_number ) );
  hist->Draw( "HIST E1" );
  hist_prep_axes(  hist, true );
  set_axis_labels( hist, variables.spectator_bound.get_name().c_str(), y_axis.c_str() );
  add_pad_title( next_pad, name.c_str(), true );
  add_atlas_decorations( next_pad, true, false );
}

//void variation( YAML::Node & run_node, basic_fileset * fileset, variable_set & variables, sample_type type, region region ){
void variation( YAML::Node & run_node, variable_set & variables, sample_type type, region region ){

  if ( !run_node[ "variation_name" ] ){ throw std::logic_error( "No variation type named." ); }
  std::string variation_name = run_node[ "variation_name" ].as<std::string>();  

  bound & ana_bound = variables.analysis_bound;
  bound & spec_bound = variables.spectator_bound;
  bound & mass_bound = variables.bound_manager->get_bound( "Q12" );
  bound & qtb_bound = variables.bound_manager->get_bound( "qtB" );

  std::string unique = run_node[ "output_unique"].as<std::string>();
  std::string input_unique = run_node["input_unique"].as<std::string>();
  std::string input_filepath = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + input_unique;
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( input_filepath, input_unique, true );

  TTree * sf_tree = fileset->get_tree( type );
  
  std::vector< std::string > analysis_cut_series = ana_bound.get_cut_series();
  std::vector< std::string > analysis_cut_names = ana_bound.get_series_names();
  char mass_qtb[200]; sprintf( mass_qtb, "(%s&&%s)", mass_bound.get_cut().c_str(), qtb_bound.get_cut().c_str() );

  std::string nominal_pt_weight = "1.0";
  std::string nominal_pu_weight = "pu_weight";
  std::string nominal_sf_weight = "photon_sf_l20_c20*muon_sf"; 
  if ( run_node["nominal_pt_weight"] ){ nominal_pt_weight = run_node["nominal_pt_weight"].as<std::string>(); }
  if ( run_node["nominal_pu_weight"] ){ nominal_pu_weight = run_node["nominal_pu_weight"].as<std::string>(); }
  if ( run_node["nominal_sf_weight"] ){ nominal_sf_weight = run_node["nominal_sf_weight"].as<std::string>(); }
  char nominal_weight[100]; 
  sprintf( nominal_weight, "%s*%s*%s", nominal_sf_weight.c_str(), nominal_pu_weight.c_str(), nominal_pt_weight.c_str() );

  std::string pt_weight = run_node["pt_weight"].as<std::string>();
  std::string pu_weight = run_node["pu_weight"].as<std::string>();
  std::string sf_weight = run_node["sf_weight"].as<std::string>();
  char variation_weight[100]; 
  sprintf( variation_weight, "%s*%s*%s", sf_weight.c_str(), pu_weight.c_str(), pt_weight.c_str() );

  std::cout << "Nominal weight: " << nominal_weight << std::endl;
  std::cout << "Variation weight: " << variation_weight << std::endl;

  for ( int analysis_bin = 0; analysis_bin < 15; analysis_bin++ ){

    // the nominal
    char nominal_draw_expr[50]; sprintf( nominal_draw_expr, "BDT>>%s", analysis_cut_names[analysis_bin].c_str() ); 
    char nominal_cut_expr[400]; sprintf( nominal_cut_expr, "(subtraction_weight*%s)*(%s&&%s&&%s)", 
      nominal_weight, mass_qtb, analysis_cut_series[analysis_bin].c_str(), region_cuts[region] );
    TH1F nominal_hist = TH1F( analysis_cut_names[analysis_bin].c_str(), "", 50, spec_bound.get_min(), spec_bound.get_max() );
    sf_tree->Draw( nominal_draw_expr, nominal_cut_expr, "goff e" );

    char variation_hist_name[50]; sprintf( variation_hist_name, "%s_var", analysis_cut_names[analysis_bin].c_str() );
    char variation_draw_expr[100]; sprintf( variation_draw_expr, "BDT>>%s", variation_hist_name ); 
    char variation_cw_expr[500]; sprintf( variation_cw_expr, "(subtraction_weight*%s)*(%s&&%s&&%s)", 
      variation_weight, mass_qtb, analysis_cut_series[analysis_bin].c_str(), region_cuts[region] );  
    TH1F variation_hist = TH1F( variation_hist_name, "", 50, spec_bound.get_min(), spec_bound.get_max() );
    sf_tree->Draw( variation_draw_expr, variation_cw_expr, "goff e" );

    TH1F rel_err_hist = TH1F( "rel_total_hist", "", 50, spec_bound.get_min(), spec_bound.get_max() );
    TH1F err_hist = TH1F( "abs_total_hist", "", 50, spec_bound.get_min(), spec_bound.get_max() );
    for ( int spec_bin_idx = 1; spec_bin_idx <= 50; spec_bin_idx++ ){
      double total_bin_err = variation_hist.GetBinContent( spec_bin_idx ) - nominal_hist.GetBinContent( spec_bin_idx );
      double total_abs_bin_err = abs( total_bin_err );
      err_hist.SetBinContent( spec_bin_idx, nominal_hist.GetBinContent( spec_bin_idx ) );
      err_hist.SetBinError(  spec_bin_idx, total_abs_bin_err );
      if ( nominal_hist.GetBinContent( spec_bin_idx ) ){
        rel_err_hist.SetBinContent( spec_bin_idx, ( nominal_hist.GetBinContent( spec_bin_idx ) + total_bin_err )/( nominal_hist.GetBinContent( spec_bin_idx ) ) );
      } else {
        rel_err_hist.SetBinContent( spec_bin_idx, 0 );
      }
      rel_err_hist.SetBinError( spec_bin_idx, 0.0 );
    }

    TH1F * difference = (TH1F *) nominal_hist.Clone();
    TH1F * ratio= (TH1F *) nominal_hist.Clone();
    difference->Reset(); ratio->Reset();
    difference->Add( &variation_hist, &nominal_hist, 1.0, -1.0 );
    ratio->Divide( &variation_hist, &nominal_hist, 1.0, 1.0 );

    TH1F * rel_error = (TH1F *) nominal_hist.Clone();
    rel_error->Reset();
    rel_error->Divide( &err_hist, &nominal_hist );
    
    prep_style();

    TCanvas * canv = new TCanvas( "canv", "", 200, 200, 2000, 2000 );
    canv->Divide( 2, 2 );
    add_spec_pad( &nominal_hist,    "Nominal",    "Yield",  variables );
    add_spec_pad( &variation_hist,  "Variation",  "Yield",  variables );
    add_spec_pad( ratio,           "Ratio",      "Ratio",  variables );
    add_spec_pad( difference,      "Difference", "#Delta", variables );
    std::string canv_name = "./" + variation_name + "/" + analysis_cut_names[analysis_bin] + "_" + region_c[region] + "_" + type_c[type] + "_variation.png";
    canv->SaveAs( canv_name.c_str() ); 

    nominal_hist.SetLineColor( kRed+1 );
    variation_hist.SetLineColor( kBlue+1 );
    TCanvas * err_canv = new TCanvas( "err_canv", "", 200, 200, 3000, 1000 );
    err_canv->Divide( 3, 1 );
    add_spec_pad( &nominal_hist, "Comparison", "Yield", variables );
    variation_hist.Draw( "HIST E1 SAME" );
    TLegend * comp_legend = create_stat_legend();
    comp_legend->AddEntry( &nominal_hist, "Nominal" );
    comp_legend->AddEntry( &variation_hist, "Variation" );
    comp_legend->Draw( "SAME" );
    add_spec_pad( rel_error, "Relative Error", "Error", variables );
    add_spec_pad( &rel_err_hist, "non-abs Relative Error", "Yield", variables );

    std::string err_canv_name = "./" + variation_name + "/" + analysis_cut_names[analysis_bin] + "_" + region_c[region] + "_" + type_c[type] + "_total.png";
    err_canv->SaveAs( err_canv_name.c_str() ); 
    
    std::string save_filename = "./" + variation_name + "/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[region] + "_" + type_c[type] + "_error.root";
    TFile * sys_file = new TFile( save_filename.c_str(), "RECREATE" );
    sys_file->cd();
    rel_error->Write( "rel_error" );
    rel_err_hist.Write( "rel_value" );
    sys_file->Close();

    delete canv;
    delete err_canv;
    delete difference;
    delete ratio;
    delete rel_error;

  }


}

void variation_input( YAML::Node & run_node, variable_set & variables, region sr, region cr ){

  const std::string unique = run_node[ "output_unique" ].as<std::string>();

  bound & ana_bound = variables.analysis_bound;
  bound & spec_bound = variables.spectator_bound;
  std::vector< std::string > analysis_cut_names = ana_bound.get_series_names();

  if ( !run_node[ "variation_name" ] ){ throw std::logic_error( "No variation type named." ); }
  std::string variation_name = run_node[ "variation_name" ].as<std::string>();  


  for ( int analysis_bin = 0; analysis_bin < 15; analysis_bin++ ){

    std::string nominal_sign_filename = "./hists/sign_" + unique + "_" + analysis_cut_names[analysis_bin] + ".root"; 
    std::string nominal_bckg_filename = "./hists/bckg_" + unique + "_" + analysis_cut_names[analysis_bin] + ".root"; 
    TFile * nominal_sign_file = new TFile( nominal_sign_filename.c_str(), "READ" );
    TFile * nominal_bckg_file = new TFile( nominal_bckg_filename.c_str(), "READ" );

    TH1F * sign_sr = (TH1F *) nominal_sign_file->Get( "sr" );
    TH1F * sign_cr = (TH1F *) nominal_sign_file->Get( "cr" );
    TH1F * bckg_sr = (TH1F *) nominal_bckg_file->Get( "sr" );
    TH1F * bckg_cr = (TH1F *) nominal_bckg_file->Get( "cr" );

    TH1F * variation_sign_sr = ( TH1F * ) sign_sr->Clone( "sign_srv" ); 
    TH1F * variation_sign_cr = ( TH1F * ) sign_cr->Clone( "sign_crv" );
    TH1F * variation_bckg_sr = ( TH1F * ) bckg_sr->Clone( "bckg_srv" );
    TH1F * variation_bckg_cr = ( TH1F * ) bckg_cr->Clone( "bckg_crv" );
    variation_sign_sr->Reset(); 
    variation_sign_cr->Reset();
    variation_bckg_sr->Reset();
    variation_bckg_cr->Reset();

    std::string err_sign_sr_filename = "./" + variation_name + "/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[sr] + "_sign_error.root";
    std::string err_sign_cr_filename = "./" + variation_name + "/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[cr] + "_sign_error.root";
    std::string err_bckg_sr_filename = "./" + variation_name + "/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[sr] + "_bckg_error.root";
    std::string err_bckg_cr_filename = "./" + variation_name + "/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[cr] + "_bckg_error.root";
    TFile * sign_sr_err_file = new TFile( err_sign_sr_filename.c_str(), "READ" );
    TFile * sign_cr_err_file = new TFile( err_sign_cr_filename.c_str(), "READ" ); 
    TFile * bckg_sr_err_file = new TFile( err_bckg_sr_filename.c_str(), "READ" );
    TFile * bckg_cr_err_file = new TFile( err_bckg_cr_filename.c_str(), "READ" ); 

    TH1F * sign_sr_rel_err = (TH1F *) sign_sr_err_file->Get( "rel_value" ); 
    TH1F * sign_cr_rel_err = (TH1F *) sign_cr_err_file->Get( "rel_value" ); 
    TH1F * bckg_sr_rel_err = (TH1F *) bckg_sr_err_file->Get( "rel_value" ); 
    TH1F * bckg_cr_rel_err = (TH1F *) bckg_cr_err_file->Get( "rel_value" ); 

    for ( int spec_bin_idx = 1; spec_bin_idx <= spec_bound.get_bins(); spec_bin_idx++ ){
      
      variation_sign_sr->SetBinContent( spec_bin_idx, sign_sr->GetBinContent( spec_bin_idx ) * ( sign_sr_rel_err->GetBinContent( spec_bin_idx ) ) );
      variation_sign_sr->SetBinError( spec_bin_idx, sign_sr->GetBinError( spec_bin_idx ) );
      variation_sign_cr->SetBinContent( spec_bin_idx, sign_cr->GetBinContent( spec_bin_idx ) * ( sign_cr_rel_err->GetBinContent( spec_bin_idx ) ) );
      variation_sign_cr->SetBinError( spec_bin_idx, sign_cr->GetBinError( spec_bin_idx ) );
      variation_bckg_sr->SetBinContent( spec_bin_idx, bckg_sr->GetBinContent( spec_bin_idx ) * ( bckg_sr_rel_err->GetBinContent( spec_bin_idx ) ) );
      variation_bckg_sr->SetBinError( spec_bin_idx, bckg_sr->GetBinError( spec_bin_idx ) );
      variation_bckg_cr->SetBinContent( spec_bin_idx, bckg_cr->GetBinContent( spec_bin_idx ) * ( bckg_cr_rel_err->GetBinContent( spec_bin_idx ) ) );
      variation_bckg_cr->SetBinError( spec_bin_idx, bckg_cr->GetBinError( spec_bin_idx ) );

    }

    sign_sr_err_file->Close(); 
    sign_cr_err_file->Close();
    bckg_sr_err_file->Close();
    bckg_cr_err_file->Close();

    std::string output_sign_name = "./" + variation_name + "/systematic/sign_" + analysis_cut_names[analysis_bin] + ".root";
    std::string output_bckg_name = "./" + variation_name + "/systematic/bckg_" + analysis_cut_names[analysis_bin] + ".root";
    TFile * output_sign_file = new TFile( output_sign_name.c_str(), "RECREATE" );
    output_sign_file->cd();
    variation_sign_sr->Write( "sr" );
    variation_sign_cr->Write( "cr" );
    output_sign_file->Close();

    TFile * output_bckg_file = new TFile( output_bckg_name.c_str(), "RECREATE" );
    output_bckg_file->cd();
    variation_bckg_sr->Write( "sr" );
    variation_bckg_cr->Write( "cr" );
    output_bckg_file->Close();

    nominal_sign_file->Close();
    nominal_bckg_file->Close();
    sign_sr_err_file->Close(); 
    sign_cr_err_file->Close();
    bckg_sr_err_file->Close();
    bckg_cr_err_file->Close();

  }

}
