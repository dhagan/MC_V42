#include <full_result.hxx>
#include <regex>
#include <systematic.hxx>

// i know what's happening here, things are being translated because muon_sf is being droppedd
// this needs to be done with some form of thingy recognition
//double rel_effect_systxt( const std::string & line ){
double rel_effect_systxt( const std::vector<std::string> & lines, const std::string & param ){
  std::regex param_regex( ".*" + param + ".*" );
  std::string line = "";
  std::for_each( lines.begin(), lines.end(), [&param_regex, &line]( const std::string & current_line ){
    if ( std::regex_search( current_line, param_regex ) ){ line = current_line; }
  } );
  if ( line.empty() ){ return 0.0; }
  std::vector<std::string> columns = {};
  split_strings( columns, line, "|" );
  std::vector<std::string> mc_sign = {};
  split_strings( mc_sign, columns.at(2), "/" );
  return std::stod( mc_sign[0], nullptr );
}

std::vector< double > get_effects( const std::string & filepath ){
  std::ifstream param_effect_file( filepath );
  std::vector< std::string > lines; 
  std::string line = "";
  while ( std::getline( param_effect_file, line ) ){ lines.push_back( line ); }
  return {
    rel_effect_systxt( lines, "photon_sf" ),
    rel_effect_systxt( lines, "msf" ),
    rel_effect_systxt( lines, "mr" )
  };
}


void full_result( YAML::Node run_node, variable_set & variables ){

  prep_style();  

  /* what do we want?
   * We want to have the baseline
   * the nosys
   * the relative error of systematics
  */ 

  // baseline with error  
  std::string base_unique = run_node[ "baseline_unique" ].as<std::string>();
  std::string stat_unique = run_node[ "stat_unique" ].as<std::string>();
  std::string input_unique, input_filepath;
  if ( run_node[ "input_unique" ] ){ 
      input_unique = run_node[ "input_unique" ].as< std::string >();
      input_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
        + "/scalefactors/" 
        + run_node[ "input_unique" ].as< std::string >() ;
    }
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( base_unique );
  fileset->load_subtraction_fileset( input_filepath, base_unique, true );

  std::vector< TH1F * > sys_errors = {};
  std::vector< std::string > systematic_groups = run_node[ "systematic_uniques" ].as< std::vector<std::string>>();
  std::transform( systematic_groups.begin(), systematic_groups.end(), std::back_inserter( sys_errors ),
    [ &sys_errors, &run_node, &stat_unique, &variables]( const std::string & group_name ){
      return systematic( stat_unique, group_name, 
                         run_node[group_name].as<std::vector<std::string>>(), 
                         variables, true );
    }
  );


  auto[ baseline_total, baseline_extracted ] = get_result( base_unique, variables );
  // baseline with just stats
  auto[ nosys_total, nosys_extracted ] = get_result( "base_a_statonly", variables );

  TH1F * sign_extracted = baseline_extracted.sign_hist;
  TH1F * sign_extracted_nosys = nosys_extracted.sign_hist;

  bound & ana_bound = variables.analysis_bound;
  TH1F * mu_calc = ana_bound.get_hist();

  // parse yield effects 
  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + base_unique + "/";
  TH1F * photon_sf_signal = variables.analysis_bound.get_hist( "psfs_" + base_unique );
  TH1F * muon_sf_signal = variables.analysis_bound.get_hist( "msrs_" + base_unique );
  TH1F * muon_reco_signal = variables.analysis_bound.get_hist( "mres_" + base_unique );
  TH1F * photon_sf_control = variables.analysis_bound.get_hist( "psfc_" + base_unique );
  TH1F * muon_sf_control = variables.analysis_bound.get_hist( "msrc_" + base_unique );
  TH1F * muon_reco_control = variables.analysis_bound.get_hist( "mrec_" + base_unique );

  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();

  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){
    
    // Needs fleshed out just a little
    double sign_yield = sign_extracted->Integral();
    double data_yield = baseline_extracted.data_hist->Integral();
    double mu = ( data_yield != 0 ) ? sign_yield/data_yield : 0.0;
    mu_calc->SetBinContent( ana_idx+1, mu );
    
    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + base_unique + "/";
    std::string plots_path = trex_qta_path + "Plots/";
    std::string table_path = trex_qta_path + "Tables/";

    std::vector<double> signal_effects = get_effects( table_path + "/SIGNAL_syst_postFit.txt" );
    std::vector<double> control_effects = get_effects(table_path + "/CONTROL_syst_postFit.txt" );

    photon_sf_signal->SetBinContent( ana_idx+1,   signal_effects.at( 0 ) );
    muon_sf_signal->SetBinContent( ana_idx+1,     signal_effects.at( 1 ) );
    muon_reco_signal->SetBinContent( ana_idx+1,   signal_effects.at( 2 ) );
    photon_sf_control->SetBinContent( ana_idx+1,  control_effects.at( 0 ) );
    muon_sf_control->SetBinContent( ana_idx+1,    control_effects.at( 1 ) );
    muon_reco_control->SetBinContent( ana_idx+1,  control_effects.at( 2 ) );

    photon_sf_signal->SetBinError( ana_idx+1, 0 );
    muon_sf_signal->SetBinError( ana_idx+1, 0 );
    muon_reco_signal->SetBinError( ana_idx+1, 0 );
    photon_sf_control->SetBinError( ana_idx+1, 0 );
    muon_sf_control->SetBinError( ana_idx+1, 0 );
    muon_reco_control->SetBinError( ana_idx+1, 0 );

  }


  TH1F * sign_extracted_nosys_copy = static_cast<TH1F *>( sign_extracted_nosys->Clone() );

  TCanvas * rel_canv = new TCanvas( "", "", 200, 200, 4000, 2000 );
  rel_canv->Divide( 4, 2 );

  TPad * active_pad = static_cast<TPad *>( rel_canv->cd( 1 ) );
  sign_extracted->Draw( "HIST E1" );
  sign_extracted_nosys_copy->Draw( "HIST E1 SAME" );
  sign_extracted_nosys_copy->SetLineColorAlpha( kRed+1, 1.0 );
  sign_extracted_nosys_copy->SetLineStyle( 2 );
  hist_prep_axes( sign_extracted );
  set_axis_labels( sign_extracted, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "Extracted" );

  active_pad = static_cast<TPad *>( rel_canv->cd( 2 ) );
  photon_sf_signal->Draw( "HIST E1" );
  hist_prep_axes( photon_sf_signal );
  set_axis_labels( photon_sf_signal, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "photon sf signal" );

  active_pad = static_cast<TPad *>( rel_canv->cd( 3 ) );
  muon_sf_signal->Draw( "HIST E1" );
  hist_prep_axes( muon_sf_signal );
  set_axis_labels( muon_sf_signal, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "muon sf signal" );

  active_pad = static_cast<TPad *>( rel_canv->cd( 4 ) );
  muon_reco_signal->Draw( "HIST E1" );
  hist_prep_axes( muon_reco_signal );
  set_axis_labels( muon_reco_signal, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "muon reco signal" );

  active_pad = static_cast<TPad *>( rel_canv->cd( 5 ) );
  sign_extracted_nosys->Draw( "HIST E1" );
  hist_prep_axes( sign_extracted_nosys );
  set_axis_labels( sign_extracted_nosys, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "nosys" );

  active_pad = static_cast<TPad *>( rel_canv->cd( 6 ) );
  photon_sf_control->Draw( "HIST E1" );
  hist_prep_axes( photon_sf_control );
  set_axis_labels( photon_sf_control, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "photon sf control" );

  active_pad = static_cast<TPad *>( rel_canv->cd( 7 ) );
  muon_sf_control->Draw( "HIST E1" );
  hist_prep_axes( muon_sf_control );
  set_axis_labels( muon_sf_control, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "muon sf control" );

  active_pad = static_cast<TPad *>( rel_canv->cd( 8 ) );
  muon_reco_control->Draw( "HIST E1" );
  hist_prep_axes( muon_reco_control );
  set_axis_labels( muon_reco_control, "q_{T}^{A}", "Relative Effect" );
  add_pad_title( active_pad, "muon reco control" );

  rel_canv->SaveAs( "relatives.png" );


}