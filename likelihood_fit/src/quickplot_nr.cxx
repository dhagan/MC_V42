#include <quickplot.hxx>
#include <TGaxis.h>
#include <yaml-cpp/yaml.h>

void quickplot_nr( YAML::Node & run_node, variable_set & variables ){

  std::string unique = run_node[ "output_unique" ].as<std::string>();
  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string efficiency_path = std::string( std::getenv( "OUT_PATH" ) ) + "/trees/";
  std::string efficiency_unique = run_node["eff_unique"].as<std::string>();
  efficiency_path += efficiency_unique + "/sign_efficiencies_" + efficiency_unique + ".root";
  basic_fileset * fileset = new basic_fileset();
  fileset->load_efficiency_fileset( efficiency_path, "" );
  //fileset->set_unique( unique );
  //fileset->load_subtraction_fileset( input_filepath, input_unique, true );

  std::vector< std::string > ana_cut_series = variables.analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names  = variables.analysis_bound.get_series_names();
  

  TH1F * sign = variables.analysis_bound.get_hist();
  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_cut_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    std::string table_path = trex_qta_path + "Tables/";
    YAML::Node table_postfit_result;
    try {
      table_postfit_result = YAML::LoadFile( table_path + "Table_postfit.yaml" );
    } catch ( std::exception & a ) {
      std::cout << " WARNING >>>>>> Fit fails in bin " << ana_idx+1 << std::endl;
      continue; 
    }
    
    std::cout << table_path + "Table_postfit.yaml" << std::endl;

    //std::cout << table_postfit_result << std::endl;

    double postfit_table_signal_yield = table_postfit_result[0]["Samples"][0]["Yield"].as<double>();
    double postfit_table_signal_error = table_postfit_result[0]["Samples"][0]["Error"].as<double>();

    sign->SetBinContent( ana_idx+1, postfit_table_signal_yield  );
    sign->SetBinError( ana_idx+1, postfit_table_signal_error );

  }

  prep_style();
  gStyle->SetOptStat( "krimes" );

  sign->SetLineColor( kRed+1 );
  TH1F * sign_noeff = (TH1F *) sign->Clone( "sign_noeff" ); 
  fileset->apply_sign_efficiency( sign, variables ); 

  TF1 * dg = prep_dg( -6, 20 );
  align_dg( dg, sign );
  dg->SetLineColor( kBlack );
  dg->SetParLimits( 2, 0, 5 );
  dg->SetParLimits( 4, 10, 15 );

  sign->Fit( dg, "MQ", "", -5, 14.0 );

  TCanvas * fitcanv = new TCanvas( "fc", "", 200, 200, 2000, 1000 );
  fitcanv->Divide( 2, 1 );

  TPad * active_pad = (TPad *) fitcanv->cd( 1 );
  sign_noeff->Draw( "HIST E1 ");
  hist_prep_axes( sign_noeff );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( sign_noeff, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit", true );
  TLegend * fs1_leg = below_logo_legend();
  fs1_leg->AddEntry( sign_noeff, "sign" );
  fs1_leg->Draw();
  TPaveStats * fsstat = make_stats( sign_noeff );
  fsstat->Draw();

  active_pad = (TPad *) fitcanv->cd( 2 );
  sign->Draw( "HIST E1 ");
  dg->Draw( "SAME");
  add_atlas_decorations( active_pad, true );
  set_axis_labels( sign, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit, efficiency corrected", true );
  hist_prep_axes( sign );
  TLegend * fs_leg = below_logo_legend();
  fs_leg->AddEntry( sign, "sign" );
  fs_leg->AddEntry( dg, "2gauss" );
  fs_leg->Draw();
  TPaveStats * stat = make_stats( sign );
  stat->Draw();

  fitcanv->SaveAs( "yield.png" );

}

  //TH1F * signy = static_cast<TH1F*>( sign_noeff->Clone() );
  //active_pad = (TPad *) fitcanv->cd( 3 );
  //signy->Draw( "HIST E1 ");
  //active_pad->SetLogy();
  //hist_prep_axes( signy );
  //add_atlas_decorations( active_pad, true );
  //set_axis_labels( signy, variables.analysis_bound.get_x_str(), "Yield" );
  //add_pad_title( active_pad, "Postfit, logy", true );
  //TLegend * fsy_leg = below_logo_legend();
  //fsy_leg->AddEntry( signy, "signy" );
  //fsy_leg->Draw();
  //TPaveStats * staty = make_stats( signy );
  //staty->Draw();
  
  
  
  //TCanvas * yieldcanv = new TCanvas( "yc", "", 200, 200, 2000, 2000 );
  //yieldcanv->Divide( 2, 2 );

  //TPad * active_pad = (TPad *) yieldcanv->cd( 1 );
  //bckg->Draw( "HIST E1 ");
  //sign->Draw( "HIST E1 SAME");
  //hist_prep_axes( bckg );
  //add_atlas_decorations( active_pad, true );
  //set_axis_labels( bckg, "q_{T}^{A}", "Yield" );
  //add_pad_title( active_pad, "Postfit", true );
  //TLegend * sb_leg = below_logo_legend();
  //sb_leg->AddEntry( sign, "sign" );
  //sb_leg->AddEntry( bckg, "bckg" );
  //sb_leg->Draw();

  //active_pad = (TPad *) yieldcanv->cd( 2 );
  //data->Draw( "HIST E1 ");
  //fit->Draw( "HIST E1 SAME");
  //hist_prep_axes( data );
  //add_atlas_decorations( active_pad, true );
  //set_axis_labels( data, "q_{T}^{A}", "Yield" );
  //add_pad_title( active_pad, "Postfit", true );
  //TLegend * df_leg = below_logo_legend();
  //df_leg->AddEntry( data, "data" );
  //df_leg->AddEntry( fit, "fit" );
  //df_leg->Draw();

  //TH1F * datanorm = static_cast< TH1F * >( data->Clone() );
  //TH1F * signnorm = static_cast< TH1F * >( sign->Clone() );
  //TH1F * bckgnorm = static_cast< TH1F * >( bckg->Clone() );
  //datanorm->Scale( 1.0/datanorm->Integral() ); 
  //signnorm->Scale( 1.0/signnorm->Integral() );
  //bckgnorm->Scale( 1.0/bckgnorm->Integral() );

  //active_pad = (TPad *) yieldcanv->cd( 3 );
  ////datanorm->Draw( "HIST E1 ");
  //signnorm->Draw( "HIST E1");
  //bckgnorm->Draw( "HIST E1 SAME");
  //hist_prep_axes( datanorm );
  //add_atlas_decorations( active_pad, true );
  //set_axis_labels( data, "q_{T}^{A}", "Density" );
  //add_pad_title( active_pad, "Postfit Normalised", true );
  //TLegend * df_norm_leg = below_logo_legend();
  ////df_norm_leg->AddEntry( datanorm, "Data" );
  //df_norm_leg->AddEntry( signnorm, "Sign" );
  //df_norm_leg->AddEntry( bckgnorm, "Bckg" );
  //df_norm_leg->Draw();

  //yieldcanv->SaveAs( "yieldfit.png" );

  //std::string trex_input_hist_path = trex_qta_path + "Histograms/" + unique + "_histos.root";
  //std::string input_sign_hist_path = input_hists + "sign_" + unique + "_" + qta_name + ".root";
  //std::string input_bckg_hist_path = input_hists + "bckg_" + unique + "_" + qta_name + ".root";
  //std::string input_data_hist_path = input_hists + "data_" + unique + "_" + qta_name + ".root";
  //std::string plots_path = trex_qta_path + "Plots/";
