#include <postprocess.hxx>
#include <TGaxis.h>
#include <yaml-cpp/yaml.h>
#include <iomanip>

TLegend * fill_pad( hist_group & hists, const std::string name, TPad * pad ){

  hists.sign_hist->Draw( "HIST E1" );
  hists.data_hist->Draw( "HIST E1 SAME" );
  hists.bckg_hist->Draw( "HIST E1 SAME" );
  hist_prep_axes( hists.sign_hist, true );
  hists.sign_hist->GetYaxis()->SetRangeUser( 0, 
      std::max( { hists.sign_hist->GetMaximum(), hists.bckg_hist->GetMaximum(), hists.data_hist->GetMaximum() } )*1.1 );
  set_axis_labels( hists.sign_hist, "BDT [score]", "Yield/ [score^{-1}]" );
  add_pad_title( pad, name, true );
  add_atlas_decorations( pad, true, false );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( hists.sign_hist,  "sign", "LP" );
  legend->AddEntry( hists.bckg_hist,  "bckg", "LP" );
  legend->AddEntry( hists.data_hist,  "data", "LP" );
  legend->Draw();
  return legend;
}

void postprocess( const std::string & output_unique, std::string & input_unique, std::string & input_filepath, 
                  variable_set & variables, const std::string & trex_path, const std::string & nonsys_unique,
                  const std::string & eff_unique ){
  
  prep_style();
  TGaxis::SetMaxDigits( 3 );

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( output_unique );
  fileset->load_subtraction_fileset( input_filepath, input_unique, true );
  fileset->load_trex_results( trex_path, output_unique, variables );
  std::string eff_path = std::string( std::getenv( "OUT_PATH" ) ) + "/trees/";
  eff_path += eff_unique + "/sign_efficiencies_" + eff_unique + ".root";
  fileset->load_efficiency_fileset( eff_path, "" );
  fileset->process_trex_results( false );
  
  basic_fileset * readin = new basic_fileset();
  readin->load_trex_fileset(  "./trex_processed_" + output_unique + ".root", output_unique );

  hist_group hists = readin->recreate_histograms_trex( variables, false );
  TCanvas * temp_canv = new TCanvas( "canv", "", 200, 200, 2000, 1000 );
  hists.sign_hist->SetLineColor( kRed+1 );
  hists.bckg_hist->SetLineColor( kBlue+1 );
  hists.data_hist->SetLineColor( kBlack );
  TH1F * add_hist = (TH1F *) hists.sign_hist->Clone( "add" );
  add_hist->Add( hists.sign_hist, hists.bckg_hist, 1.0, 1.0 );
  add_hist->SetLineColor( kGreen + 1 );
  hists.sign_hist->SetLineColor( kRed+1 );


  temp_canv->Divide( 2, 1 );
  TPad * pad = (TPad*) temp_canv->cd( 1 );
  hists.bckg_hist->Draw( "HIST E1" );
  hists.sign_hist->Draw( "HIST E1 SAME" );
  add_atlas_decorations( pad, true );
  set_axis_labels( hists.bckg_hist, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( pad, "Signal and background", true );
  
  pad = (TPad *) temp_canv->cd( 2 );
  hists.data_hist->Draw( "HIST E1" );
  add_hist->Draw( "HIST E1 SAME" );
  hists.data_hist->GetYaxis()->SetRangeUser( 0, add_hist->GetMaximum()*1.5 );
  add_atlas_decorations( pad, true );
  set_axis_labels( hists.data_hist, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( pad, "Data and Fit", true );
  temp_canv->SaveAs( "trex_reco.png" );

  TFile output_reco( "./reco.root", "RECREATE" );
  output_reco.cd();
  hists.sign_hist->Write( "sign" );
  hists.bckg_hist->Write( "bckg" );
  hists.data_hist->Write( "data" );
  add_hist->Write( "fit" );
  output_reco.Close();

  if ( nonsys_unique.empty() ){ return; } 
  std::string nonsys_path = std::string( std::getenv( "OUT_PATH" ) ) + "/trex/" + nonsys_unique + "/reco.root";
  TFile * nonsys_file = new TFile( nonsys_path.c_str(), "READ" );

  TH1F * nonsys_sign = (TH1F *) nonsys_file->Get( "sign" ); 
  TH1F * nonsys_bckg = (TH1F *) nonsys_file->Get( "bckg" );
  TH1F * nonsys_data = (TH1F *) nonsys_file->Get( "data" );
  TH1F * nonsys_fit =  (TH1F *) nonsys_file->Get( "fit" );
  nonsys_sign->SetLineColor( kRed + 2 ); 
  nonsys_bckg->SetLineColor( kBlue + 2 ); 
  nonsys_data->SetLineColor( kBlack );
  nonsys_fit->SetLineColor( kGreen+2 );
  nonsys_sign->SetLineStyle( 2 ); 
  nonsys_bckg->SetLineStyle( 2 ); 
  nonsys_data->SetLineStyle( 2 );
  nonsys_fit->SetLineStyle( 2 );

  TCanvas * nonsys_comp = new TCanvas( "nsc", "", 200, 200, 3000, 2000 );
  nonsys_comp->Divide( 3, 2  );
 
  TPad * active_pad = (TPad*) nonsys_comp->cd( 1 );
  hists.bckg_hist->Draw( "HIST E1" );
  hists.sign_hist->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( hists.bckg_hist, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( pad, "Signal and background, Systematics", true );
  TLegend * sys_sign_bckg_legend = create_atlas_legend();
  sys_sign_bckg_legend->AddEntry(  hists.sign_hist, "sign" );
  sys_sign_bckg_legend->AddEntry(  hists.bckg_hist, "bckg" );
  sys_sign_bckg_legend->Draw( "SAME" );

  active_pad = (TPad*) nonsys_comp->cd( 2 );
  nonsys_bckg->Draw( "HIST E1" );
  nonsys_sign->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( nonsys_bckg, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( pad, "Signal and background, Stats Only", true );
  TLegend * nonsys_sign_bckg_legend = create_atlas_legend();
  nonsys_sign_bckg_legend->AddEntry(  nonsys_sign, "sign" );
  nonsys_sign_bckg_legend->AddEntry(  nonsys_bckg, "bckg" );
  nonsys_sign_bckg_legend->Draw( "SAME" );

  active_pad = (TPad*) nonsys_comp->cd( 3 );
  nonsys_fit->Draw( "HIST E1" );
  add_hist->Draw( "HIST E1 SAME" );
  hists.data_hist->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( nonsys_fit, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( pad, "Fit Results", true );
  TLegend * nsc_fit_lgd = create_atlas_legend();
  nsc_fit_lgd->AddEntry( hists.data_hist, "Data" );
  nsc_fit_lgd->AddEntry( add_hist, "Syst" );
  nsc_fit_lgd->AddEntry( nonsys_fit, "No Sys" );
  nsc_fit_lgd->Draw( "SAME" );
  
  active_pad = (TPad*) nonsys_comp->cd( 5 );
  nonsys_sign->Draw( "HIST E1" );
  hists.sign_hist->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( nonsys_sign, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( pad, "Signal", true );
  TLegend * nsc_sign = create_atlas_legend();
  nsc_sign->AddEntry( nonsys_sign, "No Sys" );
  nsc_sign->AddEntry( hists.sign_hist, "Syst" );

  active_pad = (TPad*) nonsys_comp->cd( 6 );
  nonsys_bckg->Draw( "HIST E1" );
  hists.bckg_hist->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( hists.bckg_hist, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( pad, "Background", true );
  TLegend * nsc_bckg = create_atlas_legend();
  nsc_bckg->AddEntry( nonsys_bckg, "No Sys" );
  nsc_bckg->AddEntry( hists.bckg_hist, "Syst" );

  nonsys_comp->SaveAs( "trex_reco_nsc.png" );

  delete nonsys_comp;

  TF1 * dg = prep_dg( -6, 20 );
  align_dg( dg, hists.sign_hist );
  dg->SetLineColor( kBlack );
  dg->SetParLimits( 2, 0, 5 );
  dg->SetParLimits( 4, 10, 15 );
  //dg->SetParameter( 4, 40 );

  TF1 * sg = prep_sg( -4, 13 );
  align_sg( sg, hists.sign_hist );
  sg->SetLineColor( kBlack );
  //sg->SetLineStyle( 2 );

  // you need to load up a qtb_lower fileset for this operation to work.
  fileset->apply_sign_efficiency( hists.sign_hist, variables ); 

  hists.sign_hist->Fit( dg, "MQ", "", -5, 14.0 );
  TH1F * quicksign= (TH1F *) hists.sign_hist->Clone( "idk" );
  quicksign->Fit( sg, "MQ", "", -5, 14.0 );

  TCanvas * fit_canv = new TCanvas( "fc", "", 200, 200, 2000, 1000 );
  fit_canv->Divide( 2, 1 );

  active_pad = (TPad*) fit_canv->cd( 1 );
  hists.sign_hist->Draw( "HIST E1" );
  dg->Draw( "SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( hists.sign_hist, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( active_pad, "Signal", true );
  TLegend * fit_nsc = below_logo_legend();
  fit_nsc->AddEntry( hists.sign_hist, "Syst" );
  fit_nsc->AddEntry( dg, "2g fit" );
  fit_nsc->Draw();
  TPaveStats * fit_stat = ( TPaveStats *) make_stats( hists.sign_hist, false, false );
  fit_stat->Draw();

  active_pad = (TPad*) fit_canv->cd( 2 );
  quicksign->Draw( "HIST E1" );
  sg->Draw( "SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( quicksign, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str() );
  add_pad_title( active_pad, "Signal", true );
  TLegend * fit_nsc_1g = below_logo_legend();
  fit_nsc_1g->AddEntry( quicksign, "Syst" );
  fit_nsc_1g->AddEntry( sg, "1g fit" );
  fit_nsc_1g->Draw();
  TPaveStats * fit_stat_1g = ( TPaveStats *) make_stats( quicksign, false, false );
  fit_stat_1g->Draw();

  fit_canv->SaveAs( "fast_fit.png" );

}

//void fit_visualise( const std::string & unique, variable_set & variables ){
void fit_visualise( const std::string & output_unique, variable_set & variables ){

  std::vector< std::string > analysis_bins = variables.analysis_bound.get_series_names(); 
  std::string workdir = std::string( std::getenv( "OUT_PATH" ) ) + "/trex/" + output_unique;

  basic_fileset * readin = new basic_fileset();
  readin->load_trex_fileset(  "./trex_processed_" + output_unique + ".root", output_unique );

  TTree * trex_tree = readin->trex_tree;
    
  fit_results * results_entry = new fit_results();
  trex_tree->SetBranchAddress( "trex_results", &results_entry );

  TH1F * mu_ve = variables.analysis_bound.get_hist();
  TH1F * spp_ve = variables.analysis_bound.get_hist();

  for ( size_t bin = 0; bin < analysis_bins.size(); bin++ ){

    TCanvas * hist_canvas = new TCanvas( analysis_bins[bin].c_str(), "", 200, 200, 2000, 2000 );
    hist_canvas->Divide( 2, 2 );
  
    std::string result_path = workdir + "/" + analysis_bins[bin] + "/" + output_unique + "/Histograms/";
    std::string output_sr_name = Form( "Output SR - %s %s",  output_unique.c_str(), variables.spectator_variable.c_str() );
    std::string output_cr_name = Form( "Output CR - %s %s",  output_unique.c_str(), variables.spectator_variable.c_str() );
    std::string output_sr_scaled_name = Form( "Output SR FIT - %s %s",  output_unique.c_str(), variables.spectator_variable.c_str() );
    std::string output_cr_scaled_name = Form( "Output CR FIT - %s %s",  output_unique.c_str(), variables.spectator_variable.c_str() );

    std::string output_filepath =  result_path + output_unique + "_histos.root";
    TFile * output_file = new TFile( output_filepath.c_str(), "READ" ); 
    TH1F * output_sign_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/sign/nominal/SIGNAL_sign" ) );
    TH1F * output_bckg_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/bckg/nominal/SIGNAL_bckg" ) );
    TH1F * output_data_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/data/nominal/SIGNAL_data" ) );
    TH1F * output_data_sr_scale = static_cast< TH1F * >( output_file->Get( "SIGNAL/data/nominal/SIGNAL_data" ) );
    output_sign_sr->SetLineColor( kRed + 1 ); 
    output_bckg_sr->SetLineColor( kBlue + 1 );
    output_data_sr->SetLineColor( 1 );
    hist_group output_sr_hists( output_sign_sr, output_bckg_sr, output_data_sr );
    TPad * output_sr_pad = (TPad *) hist_canvas->cd( 1 );
    fill_pad(  output_sr_hists, output_sr_name, output_sr_pad );
    
    TH1F * output_sign_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/sign/nominal/CONTROL_sign" ) );
    TH1F * output_bckg_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/bckg/nominal/CONTROL_bckg" ) );
    TH1F * output_data_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/data/nominal/CONTROL_data" ) );
    TH1F * output_data_cr_scale = static_cast< TH1F * >( output_file->Get( "CONTROL/data/nominal/CONTROL_data" ) );
    output_sign_cr->SetLineColor( kRed + 1 ); 
    output_bckg_cr->SetLineColor( kBlue + 1 );
    output_data_cr->SetLineColor( 1 );
    hist_group output_cr_hists( output_sign_cr, output_bckg_cr, output_data_cr );
    TPad * output_cr_pad = (TPad *) hist_canvas->cd( 2 );
    fill_pad(  output_cr_hists, output_cr_name, output_cr_pad );
    TH1F * output_sign_sr_scale = (TH1F *) output_sign_sr->Clone(); 
    TH1F * output_bckg_sr_scale = (TH1F *) output_bckg_sr->Clone();
    //TH1F * output_data_sr_scale = (TH1F *) output_data_sr->Clone();
    TH1F * output_sign_cr_scale = (TH1F *) output_sign_cr->Clone(); 
    TH1F * output_bckg_cr_scale = (TH1F *) output_bckg_cr->Clone();
    //TH1F * output_data_cr_scale = (TH1F *) output_data_cr->Clone();



    trex_tree->GetEntry( bin );
    output_sign_sr_scale->Scale( (*results_entry).mu_value  );
    output_bckg_sr_scale->Scale( (*results_entry).spp_value );
    //output_data_sr_scale->Add( output_sign_sr_scale, output_bckg_sr_scale );
    hist_group output_sr_scale_hists( output_sign_sr_scale, output_bckg_sr_scale, output_data_sr_scale );
    TPad * output_sr_scale_pad = (TPad *) hist_canvas->cd( 3 );
    TLegend * sr_scale_legend = fill_pad(  output_sr_scale_hists, output_sr_scaled_name, output_sr_scale_pad );
    TH1F * output_fit_sr = (TH1F *) output_sign_sr_scale->Clone( "fit SR" );
    output_fit_sr->Reset();
    output_fit_sr->SetLineColor( kGreen+1 );
    output_fit_sr->Add( output_sign_sr_scale, output_bckg_sr_scale, 1.0, 1.0 );
    output_fit_sr->Draw( "HIST SAME E1" );
    sr_scale_legend->AddEntry( output_fit_sr );



    output_sign_cr_scale->Scale( (*results_entry).mu_value  );
    output_bckg_cr_scale->Scale( (*results_entry).spp_value );
    //output_data_cr_scale->Add( output_sign_cr_scale, output_bckg_cr_scale );
    hist_group output_cr_scale_hists( output_sign_cr_scale, output_bckg_cr_scale, output_data_cr_scale );
    TPad * output_cr_scale_pad = (TPad *) hist_canvas->cd( 4 );
    TLegend * cr_scale_legend = fill_pad(  output_cr_scale_hists, output_cr_scaled_name, output_cr_scale_pad );
    TH1F * output_fit_cr = (TH1F *) output_sign_cr_scale->Clone( "fit CR" );
    output_fit_cr->Reset();
    output_fit_cr->SetLineColor( kGreen+1 );
    output_fit_cr->Add( output_sign_cr_scale, output_bckg_cr_scale, 1.0, 1.0 );
    output_fit_cr->Draw( "HIST SAME E1" );
    cr_scale_legend->AddEntry( output_fit_cr );


    std::string output_canvas_name = "./trex_output_qtA-" + std::to_string( bin+1 ) + ".png";
    hist_canvas->SaveAs( output_canvas_name.c_str() );

    std::cout << "qtA-" << bin+1 << std::endl;
    std::cout << std::setw(4) << "sr sign int:        " << output_sign_sr->Integral() << std::endl; 
    std::cout << std::setw(4) << "sr bckg int:        " << output_bckg_sr->Integral() << std::endl; 
    std::cout << std::setw(4) << "sr data int:        " << output_data_sr->Integral() << std::endl; 
    std::cout << std::setw(4) << "sr sign fit int:    " << output_sign_sr_scale->Integral() << std::endl; 
    std::cout << std::setw(4) << "sr bckg fit int:    " << output_bckg_sr_scale->Integral() << std::endl; 
    std::cout << std::setw(4) << "sr fit int:         " << output_fit_sr->Integral() << std::endl; 
    std::cout << std::setw(4) << "cr sign int:        " << output_sign_cr->Integral() << std::endl; 
    std::cout << std::setw(4) << "cr bckg int:        " << output_bckg_cr->Integral() << std::endl; 
    std::cout << std::setw(4) << "cr data int:        " << output_data_cr->Integral() << std::endl; 
    std::cout << std::setw(4) << "cr sign fit int:    " << output_sign_cr_scale->Integral() << std::endl; 
    std::cout << std::setw(4) << "cr bckg fit int:    " << output_bckg_cr_scale->Integral() << std::endl; 
    std::cout << std::setw(4) << "cr fit int:         " << output_fit_cr->Integral() << std::endl; 
    std::cout << std::setw(4) << "full sign int:      " << output_sign_sr_scale->Integral() + output_sign_cr_scale->Integral() << std::endl; 
    std::cout << std::setw(4) << "full bckg int:      " << output_bckg_sr_scale->Integral() + output_bckg_cr_scale->Integral() << std::endl; 
    std::cout << std::setw(4) << "full data int:      " << output_data_sr->Integral() + output_data_cr->Integral() << std::endl; 
    std::cout << std::setw(4) << "full fit int:       " << output_sign_sr_scale->Integral() + output_sign_cr_scale->Integral() + output_bckg_sr_scale->Integral() + output_bckg_cr_scale->Integral()      << std::endl; 
    std::cout << std::setw(4) << "mu value:           " << ( *results_entry ).mu_value << std::endl; 
    std::cout << std::setw(4) << "mu error:           " << ( *results_entry ).mu_error << std::endl; 
    std::cout << std::setw(4) << "spp value:          " << ( *results_entry ).spp_value << std::endl; 
    std::cout << std::setw(4) << "spp error:          " << ( *results_entry ).spp_error << std::endl; 
    std::cout << std::setw(4) << "chi2_1 error:       " << ( *results_entry ).chi2_v1 << std::endl; 
    std::cout << std::setw(4) << "chi2_2 error:       " << ( *results_entry ).chi2_v2 << std::endl; 

    mu_ve->SetBinContent( bin+1, (*results_entry).mu_value );
    mu_ve->SetBinError( bin+1, (*results_entry).mu_error );
    spp_ve->SetBinContent( bin+1, (*results_entry).spp_value );
    spp_ve->SetBinError( bin+1, (*results_entry).spp_error );

  }

  mu_ve->SetLineColor( kRed );
  spp_ve->SetLineColor( kBlue );

  TCanvas * paramcanv = new TCanvas( "params", "", 200, 200, 2000, 1000 );
  paramcanv->Divide( 2, 1 );

  TPad * active_pad = (TPad *) paramcanv->cd( 1 );
  mu_ve->Draw( "HIST E1" );
  hist_prep_axes( mu_ve );
  mu_ve->GetYaxis()->SetRangeUser( 0, 1 );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( mu_ve, "q_{T}^{A}", "#mu" );
  add_pad_title( active_pad, "#mu", true );

  active_pad = (TPad *) paramcanv->cd( 2 );
  spp_ve->Draw( "HIST E1" );
  hist_prep_axes( spp_ve );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( spp_ve, "q_{T}^{A}", "spp" );
  add_pad_title( active_pad, "spp", true );

  paramcanv->SaveAs( "params.png" );

}

void wv( basic_fileset * fileset, const std::string & output_unique, variable_set & variables ){

  wv_gen_full( fileset, output_unique, variables );

  std::vector< std::string > analysis_cut_series = variables.analysis_bound.get_cut_series();
  std::vector< std::string > analysis_cut_names = variables.analysis_bound.get_series_names();

  TTree * sign_tree = fileset->sign_tree;
  TTree * bckg_tree = fileset->bckg_tree;

  TH1F * jpsi_weight_full_sign = new TH1F( "jwfs", "", 40, 0, 3 );
  TH1F * phot_weight_full_sign = new TH1F( "pwfs", "", 40, 0, 3 );
  TH1F * jpsi_weight_full_bckg = new TH1F( "jwfb", "", 40, 0, 3 );
  TH1F * phot_weight_full_bckg = new TH1F( "pwfb", "", 40, 0, 3 );

  sign_tree->Draw( "jpsi_weight>>jwfs", "", "e goff" );
  sign_tree->Draw( "phot_weight>>pwfs", "", "e goff" );
  bckg_tree->Draw( "jpsi_weight>>jwfb", "", "e goff" );
  bckg_tree->Draw( "phot_weight>>pwfb", "", "e goff" );

  jpsi_weight_full_sign->SetLineColor( kRed+1 );
  phot_weight_full_sign->SetLineColor( kRed+1 );
  jpsi_weight_full_bckg->SetLineColor( kBlue+1 );
  phot_weight_full_bckg->SetLineColor( kBlue+1 ); 

  TCanvas * full_range = new TCanvas( "fvr", "", 200, 200, 2000, 1000 );
  full_range->Divide( 2, 1 );

  TPad * active_pad = (TPad *) full_range->cd( 1 );
  jpsi_weight_full_sign->Draw( "HIST E1" );
  jpsi_weight_full_bckg->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( jpsi_weight_full_sign, "jpsi weight", "value" );
  add_pad_title( active_pad, "J/#psi p_{T} weight", true );
  TLegend * full_jpsi = create_atlas_legend();
  full_jpsi->AddEntry( jpsi_weight_full_sign, "sign" );
  full_jpsi->AddEntry( jpsi_weight_full_bckg, "bckg" );

  active_pad = (TPad *) full_range->cd( 2 );
  phot_weight_full_sign->Draw( "HIST E1" );
  phot_weight_full_bckg->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( phot_weight_full_sign, "photon weight", "value" );
  add_pad_title( active_pad, "#gamma p_{T} weight", true );
  TLegend * full_phot = create_atlas_legend();
  full_phot->AddEntry( phot_weight_full_sign, "sign" );
  full_phot->AddEntry( phot_weight_full_bckg, "bckg" );

  full_range->SaveAs( "weights/full.png" );

  TH1F * jpsi_weight_sign_mnst = new TH1F( "jws_mnst", "", 15, 0, 15 );
  TH1F * phot_weight_sign_mnst = new TH1F( "pws_mnst", "", 15, 0, 15 );
  TH1F * jpsi_weight_bckg_mnst = new TH1F( "jwb_mnst", "", 15, 0, 15 );
  TH1F * phot_weight_bckg_mnst = new TH1F( "pwb_mnst", "", 15, 0, 15 );

  delete jpsi_weight_full_sign; 
  delete phot_weight_full_sign;
  delete jpsi_weight_full_bckg;
  delete phot_weight_full_bckg;

  for ( int ana_bin = 0; ana_bin < variables.analysis_bound.get_bins(); ana_bin++ ){

    TH1F * jpsi_weight_sign = new TH1F( "jwfs", "", 20, 0, 3 );
    TH1F * phot_weight_sign = new TH1F( "pwfs", "", 20, 0, 3 );
    TH1F * jpsi_weight_bckg = new TH1F( "jwfb", "", 20, 0, 3 );
    TH1F * phot_weight_bckg = new TH1F( "pwfb", "", 20, 0, 3 );

    std::cout << analysis_cut_series[ana_bin].c_str() << std::endl;

    sign_tree->Draw( "jpsi_weight>>jwfs", analysis_cut_series[ana_bin].c_str(), "e goff" );
    sign_tree->Draw( "phot_weight>>pwfs", analysis_cut_series[ana_bin].c_str(), "e goff" );
    bckg_tree->Draw( "jpsi_weight>>jwfb", analysis_cut_series[ana_bin].c_str(), "e goff" );
    bckg_tree->Draw( "phot_weight>>pwfb", analysis_cut_series[ana_bin].c_str(), "e goff" );

    jpsi_weight_sign->SetLineColor( kRed+1 );  
    phot_weight_sign->SetLineColor( kRed+1 );
    jpsi_weight_bckg->SetLineColor( kBlue+1 );  
    phot_weight_bckg->SetLineColor( kBlue+1 ); 


    TCanvas * weight_canv = new TCanvas( "fr", "", 200, 200, 2000, 1000 );
    weight_canv->Divide( 2, 1 );

    active_pad = (TPad *) weight_canv->cd( 1 );
    jpsi_weight_sign->Draw( "HIST E1" );
    jpsi_weight_bckg->Draw( "HIST E1 SAME" );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( jpsi_weight_sign, "jpsi weight", "value" );
    add_pad_title( active_pad, "J/#psi p_{T} weight", true );
    TLegend * full_jpsi = create_atlas_legend();
    full_jpsi->AddEntry( jpsi_weight_sign, "sign" );
    full_jpsi->AddEntry( jpsi_weight_bckg, "bckg" );

    active_pad = (TPad *) weight_canv->cd( 2 );
    phot_weight_sign->Draw( "HIST E1" );
    phot_weight_bckg->Draw( "HIST E1 SAME" );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( phot_weight_sign, "photon weight", "value" );
    add_pad_title( active_pad, "#gamma p_{T} weight", true );
    TLegend * full_phot = create_atlas_legend();
    full_phot->AddEntry( phot_weight_sign, "sign" );
    full_phot->AddEntry( phot_weight_bckg, "bckg" );
    full_phot->Draw();

    std::string weight_image_name = "./weights/" + analysis_cut_names[ana_bin] + ".png";
    weight_canv->SaveAs( weight_image_name.c_str() );

    jpsi_weight_sign_mnst->SetBinContent( ana_bin + 1, jpsi_weight_sign->GetMean() ); 
    phot_weight_sign_mnst->SetBinContent( ana_bin + 1, phot_weight_sign->GetMean() );
    jpsi_weight_bckg_mnst->SetBinContent( ana_bin + 1, jpsi_weight_bckg->GetMean() );
    phot_weight_bckg_mnst->SetBinContent( ana_bin + 1, jpsi_weight_bckg->GetMean() );
    jpsi_weight_sign_mnst->SetBinError( ana_bin + 1, jpsi_weight_sign->GetStdDev() ); 
    phot_weight_sign_mnst->SetBinError( ana_bin + 1, phot_weight_sign->GetStdDev() );
    jpsi_weight_bckg_mnst->SetBinError( ana_bin + 1, jpsi_weight_bckg->GetStdDev() );
    phot_weight_bckg_mnst->SetBinError( ana_bin + 1, jpsi_weight_bckg->GetStdDev() );

    delete jpsi_weight_sign; 
    delete phot_weight_sign;
    delete jpsi_weight_bckg;
    delete phot_weight_bckg;
    delete weight_canv; 

  }


  jpsi_weight_sign_mnst->SetLineColor( kRed+1 );
  phot_weight_sign_mnst->SetLineColor( kRed+1 );
  jpsi_weight_bckg_mnst->SetLineColor( kBlue+1 );
  phot_weight_bckg_mnst->SetLineColor( kBlue+1 );

  TCanvas * stat_canv = new TCanvas( "sr", "", 200, 200, 2000, 1000 );
  stat_canv->Divide( 2, 1 );

  active_pad = (TPad *) stat_canv->cd( 1 );
  jpsi_weight_sign_mnst->Draw( "HIST E1" );
  jpsi_weight_bckg_mnst->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( jpsi_weight_sign_mnst, "qTa bin", "J/#psi weight mean" );
  add_pad_title( active_pad, "J/#psi p_{T} weight", true );
  TLegend * stat_jpsi = create_atlas_legend();
  stat_jpsi->AddEntry( jpsi_weight_sign_mnst, "sign" );
  stat_jpsi->AddEntry( jpsi_weight_bckg_mnst, "bckg" );

  active_pad = (TPad *) stat_canv->cd( 2 );
  phot_weight_sign_mnst->Draw( "HIST E1" );
  phot_weight_bckg_mnst->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( phot_weight_sign_mnst, "qtA bin", "#gamma weight mean" );
  add_pad_title( active_pad, "#gamma p_{T} weight", true );
  TLegend * stat_phot = create_atlas_legend();
  stat_phot->AddEntry( phot_weight_sign_mnst, "sign" );
  stat_phot->AddEntry( phot_weight_bckg_mnst, "bckg" );

  stat_canv->SaveAs( "weights/stats.png" );

  std::cout << output_unique << std::endl;

  delete jpsi_weight_sign_mnst; 
  delete phot_weight_sign_mnst;
  delete jpsi_weight_bckg_mnst;
  delete phot_weight_bckg_mnst;
  delete stat_canv;


}

void wv_gen_full( basic_fileset * fileset, const std::string & output_unique, variable_set & variables ){

  std::cout << output_unique << std::endl;

  TTree * sign_tree = fileset->get_tree( sample_type::sign );
  TTree * bckg_tree = fileset->get_tree( sample_type::bckg );
  TTree * data_tree = fileset->get_tree( sample_type::data );

  TH1F * jpsi_pt_data = variables.bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_data" );
  TH1F * jpsi_pt_sign = variables.bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_sign" );
  TH1F * jpsi_pt_bckg = variables.bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_bckg" );
  TH1F * jpsi_pt_sr = variables.bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_sr" );
  TH1F * jpsi_pt_br = variables.bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_br" );

  TH1F * phot_pt_data = variables.bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_data" );
  TH1F * phot_pt_sign = variables.bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_sign" );
  TH1F * phot_pt_bckg = variables.bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_bckg" );
  TH1F * phot_pt_sr = variables.bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_sr" );
  TH1F * phot_pt_br = variables.bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_br" );

  bound & qtb_bound = variables.bound_manager->get_bound( "qtB" );

  char sf_cut_expr[300]; sprintf( sf_cut_expr, "subtraction_weight*(%s)", qtb_bound.get_cut().c_str() ); 
  std::cout << sf_cut_expr << std::endl;
  data_tree->Draw( "DiMuonPt>>dimu_pt_data", sf_cut_expr, "e goff" );
  sign_tree->Draw( "DiMuonPt>>dimu_pt_sign", sf_cut_expr, "e goff" );
  bckg_tree->Draw( "DiMuonPt>>dimu_pt_bckg", sf_cut_expr, "e goff" );

  data_tree->Draw( "PhotonPt>>phot_pt_data", sf_cut_expr, "e goff" );
  sign_tree->Draw( "PhotonPt>>phot_pt_sign", sf_cut_expr, "e goff" );
  bckg_tree->Draw( "PhotonPt>>phot_pt_bckg", sf_cut_expr, "e goff" );

  jpsi_pt_data->Scale( 1.0/jpsi_pt_data->Integral() );
  jpsi_pt_sign->Scale( 1.0/jpsi_pt_sign->Integral() );
  jpsi_pt_bckg->Scale( 1.0/jpsi_pt_bckg->Integral() );
  jpsi_pt_data->SetLineColor( kBlack );
  jpsi_pt_sign->SetLineColor( kRed );
  jpsi_pt_bckg->SetLineColor( kBlue );

  phot_pt_data->Scale( 1.0/phot_pt_data->Integral() );
  phot_pt_sign->Scale( 1.0/phot_pt_sign->Integral() );
  phot_pt_bckg->Scale( 1.0/phot_pt_bckg->Integral() );
  phot_pt_data->SetLineColor( kBlack );
  phot_pt_sign->SetLineColor( kRed );
  phot_pt_bckg->SetLineColor( kBlue );


  jpsi_pt_sr->Divide( jpsi_pt_data, jpsi_pt_sign, 1.0, 1.0 );
  jpsi_pt_br->Divide( jpsi_pt_data, jpsi_pt_bckg, 1.0, 1.0 );
  jpsi_pt_sr->SetLineColor( kRed );
  jpsi_pt_br->SetLineColor( kBlue );

  phot_pt_sr->Divide( phot_pt_data, phot_pt_sign, 1.0, 1.0 );
  phot_pt_br->Divide( phot_pt_data, phot_pt_bckg, 1.0, 1.0 );
  phot_pt_sr->SetLineColor( kRed );
  phot_pt_br->SetLineColor( kBlue );

  gStyle->SetOptStat( "krimes" );


  TCanvas * reweight_draws = new TCanvas( "rwd", "", 200, 200, 2000, 2000 );
  reweight_draws->Divide( 2, 2 );

  TPad * active_pad = (TPad *) reweight_draws->cd( 1 );
  jpsi_pt_sign->Draw( "HIST E1" );
  jpsi_pt_sign->SetStats( false );
  jpsi_pt_bckg->Draw( "HIST E1 SAME" );
  jpsi_pt_data->Draw( "HIST E1 SAME" );
  hist_prep_axes( jpsi_pt_sign );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( jpsi_pt_sign, "J/#psi p_{t}", "Yield" );
  add_pad_title( active_pad, "J/#psi p_{T}", true );
  TLegend * full_jpsi = below_logo_legend();
  full_jpsi->AddEntry( jpsi_pt_sign, "sign" );
  full_jpsi->AddEntry( jpsi_pt_bckg, "bckg" );
  full_jpsi->AddEntry( jpsi_pt_data, "data" );
  full_jpsi->Draw();
  


  active_pad = (TPad *) reweight_draws->cd( 2 );
  jpsi_pt_sr->Draw( "HIST E1" ); 
  jpsi_pt_br->Draw( "HIST E1 SAMES" );
  hist_prep_axes( jpsi_pt_sr );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( jpsi_pt_sr, "J/#psi p_{T}", "Weight" );
  add_pad_title( active_pad, "J/#psi p_{T} Weight", true );
  TLegend * rat_jpsi = below_logo_legend();
  rat_jpsi->AddEntry( jpsi_pt_sr, "sign" );
  rat_jpsi->AddEntry( jpsi_pt_br, "bckg" );
  rat_jpsi->Draw();
  TPaveStats * jrs_stats = make_stats( jpsi_pt_sr, true, true );
  jrs_stats->Draw();
  TPaveStats * jrb_stats = make_stats( jpsi_pt_br, true, false );
  jrb_stats->Draw();



  active_pad = (TPad *) reweight_draws->cd( 3 );
  phot_pt_sign->Draw( "HIST E1" );
  phot_pt_sign->SetStats( false );
  phot_pt_bckg->Draw( "HIST E1 SAME" );
  phot_pt_data->Draw( "HIST E1 SAME" );
  hist_prep_axes( phot_pt_sign );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( phot_pt_sign, "#gamma p_{T}", "Yield" );
  add_pad_title( active_pad, "#gamma p_{T}", true );
  TLegend * full_phot = below_logo_legend();
  full_phot->AddEntry( phot_pt_sign, "sign" );
  full_phot->AddEntry( phot_pt_bckg, "bckg" );
  full_phot->AddEntry( phot_pt_data, "data" );
  full_phot->Draw();
  
  active_pad = (TPad *) reweight_draws->cd( 4 );
  phot_pt_sr->Draw( "HIST E1" );
  phot_pt_br->Draw( "HIST E1 SAMES" );
  hist_prep_axes( phot_pt_sr );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( phot_pt_sr, "#gamma p_{T} weight", "Weight" );
  add_pad_title( active_pad, "#gamma p_{T} Weight", true );
  TLegend * rat_phot = below_logo_legend();
  rat_phot->AddEntry( phot_pt_sr, "sign" );
  rat_phot->AddEntry( phot_pt_br, "bckg" );
  rat_phot->Draw();
  TPaveStats * prs_stats = make_stats( phot_pt_sr, true, true );
  prs_stats->Draw();
  TPaveStats * prb_stats = make_stats( phot_pt_br, true, false );
  prb_stats->Draw();

  reweight_draws->SaveAs( "weights/creation.png" );

}

void sf_explore( basic_fileset * fileset, const std::string & output_unique, variable_set & variables ){

  prep_style();

  bound & ana_bound = variables.analysis_bound;
  bound & mass_bound = variables.bound_manager->get_bound( "Q12" );
  bound & qtb_bound = variables.bound_manager->get_bound( "qtB" );

  std::vector<std::string> cut_series = ana_bound.get_cut_series();
  std::vector<std::string> cut_names = ana_bound.get_series_names();

  TTree * sign_tree = fileset->sign_tree;
  TH1F * sf_hist = new TH1F( "sf_hist", "", 1000, -1.0, 2.0 );
  TH1F * sf_upper_hist = new TH1F( "sf_upper_hist", "", 1000, -1.0, 2.0 );
  TH1F * avg_sf_hist = ana_bound.get_hist( "base" ); 
  TH1F * avg_sf_hist_l10 = ana_bound.get_hist( "l10" ); 
  TH1F * avg_sf_hist_l20 = ana_bound.get_hist( "l20" ); 
  TH1F * avg_sf_hist_l20_c20 = ana_bound.get_hist( "l20_c20" ); 
  TH1F * avg_sf_hist_step = ana_bound.get_hist( "step" ); 

  char mass_qtb[200]; sprintf( mass_qtb, "(%s&&%s)", mass_bound.get_cut().c_str(), qtb_bound.get_cut().c_str() );

  for ( int bin_idx = 0; bin_idx < ana_bound.get_bins(); bin_idx++ ){
    
    char sibg_expression[600];
    const char * ana_cut = cut_series.at(bin_idx).c_str();
    sprintf( sibg_expression, "(subtraction_weight)*(%s&&%s)", mass_qtb, ana_cut );

    sign_tree->Draw( "photon_sf>>sf_hist", sibg_expression, "e goff" );
    sign_tree->Draw( "photon_upper_sf>>sf_upper_hist", sibg_expression, "e goff" );
    avg_sf_hist->SetBinContent( bin_idx+1, sf_hist->GetMean() );
    avg_sf_hist->SetBinError( bin_idx+1, sf_upper_hist->GetMean() - sf_hist->GetMean() );
    sf_hist->Reset();
    sf_upper_hist->Reset();

    sign_tree->Draw( "photon_sf>>sf_hist", sibg_expression, "e goff" );
    sign_tree->Draw( "photon_upper_sf_l10>>sf_upper_hist", sibg_expression, "e goff" );
    avg_sf_hist_l10->SetBinContent( bin_idx+1, sf_hist->GetMean() );
    avg_sf_hist_l10->SetBinError( bin_idx+1, sf_upper_hist->GetMean() - sf_hist->GetMean() );
    sf_hist->Reset();
    sf_upper_hist->Reset();

    sign_tree->Draw( "photon_sf>>sf_hist", sibg_expression, "e goff" );
    sign_tree->Draw( "photon_upper_sf_l20>>sf_upper_hist", sibg_expression, "e goff" );
    avg_sf_hist_l20->SetBinContent( bin_idx+1, sf_hist->GetMean() );
    avg_sf_hist_l20->SetBinError( bin_idx+1, sf_upper_hist->GetMean() - sf_hist->GetMean() );
    sf_hist->Reset();
    sf_upper_hist->Reset();

    sign_tree->Draw( "photon_sf_l20_c20>>sf_hist", sibg_expression, "e goff" );
    sign_tree->Draw( "photon_upper_sf_l20_c20>>sf_upper_hist",  sibg_expression, "e goff" );
    avg_sf_hist_l20_c20->SetBinContent( bin_idx+1, sf_hist->GetMean() );
    avg_sf_hist_l20_c20->SetBinError( bin_idx+1, sf_upper_hist->GetMean() - sf_hist->GetMean() );
    sf_hist->Reset();
    sf_upper_hist->Reset();

    sign_tree->Draw( "photon_sf_step>>sf_hist", sibg_expression, "e goff" );
    sign_tree->Draw( "photon_upper_sf>>sf_upper_hist",  sibg_expression, "e goff" );
    avg_sf_hist_step->SetBinContent( bin_idx+1, sf_hist->GetMean() );
    avg_sf_hist_step->SetBinError( bin_idx+1, sf_upper_hist->GetMean() - sf_hist->GetMean() );
    sf_hist->Reset();
    sf_upper_hist->Reset();


  }


  TCanvas * sf_scales = new TCanvas( "sf_scales", "", 200, 200, 2000, 3000 );
  sf_scales->Divide( 2, 3 );

  TPad * active_pad = static_cast<TPad *>( sf_scales->cd( 1 ) );
  avg_sf_hist->Draw( "HIST E1" );
  hist_prep_axes( avg_sf_hist  );
  avg_sf_hist->GetYaxis()->SetRangeUser( 0.85, 1.1 );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( avg_sf_hist, ana_bound.get_x_str(), "Photon SF" );
  add_pad_title( active_pad, "Photon SF", true );
  TLegend * base_leg = create_atlas_legend() ;
  base_leg->AddEntry( avg_sf_hist, "Base" );
  base_leg->Draw();

  active_pad = static_cast<TPad *>( sf_scales->cd( 2 ) );
  avg_sf_hist_l10->Draw( "HIST E1" );
  hist_prep_axes( avg_sf_hist_l10 );
  avg_sf_hist_l10->GetYaxis()->SetRangeUser( 0.85, 1.1 );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( avg_sf_hist_l10, ana_bound.get_x_str(), "Photon SF" );
  add_pad_title( active_pad, "Photon SF - L10", true );
  TLegend * l10_leg = create_atlas_legend();
  l10_leg->AddEntry( avg_sf_hist_l10, "L10" );
  l10_leg->Draw();

  active_pad = static_cast<TPad *>( sf_scales->cd( 3 ) );
  avg_sf_hist_l20->Draw( "HIST E1" );
  hist_prep_axes( avg_sf_hist_l20 );
  avg_sf_hist_l20->GetYaxis()->SetRangeUser( 0.85, 1.1 );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( avg_sf_hist_l20, ana_bound.get_x_str(), "Photon SF" );
  add_pad_title( active_pad, "Photon SF - L20", true );
  TLegend * l20_leg = create_atlas_legend();
  l20_leg->AddEntry( avg_sf_hist_l20, "L20" );
  l20_leg->Draw();

  active_pad = static_cast<TPad *>( sf_scales->cd( 4 ) );
  avg_sf_hist_l20_c20->Draw( "HIST E1" );
  hist_prep_axes( avg_sf_hist_l20_c20 );
  avg_sf_hist_l20_c20->GetYaxis()->SetRangeUser( 0.85, 1.1 );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( avg_sf_hist_l20_c20, ana_bound.get_x_str(), "Photon SF" );
  add_pad_title( active_pad, "Photon SF - l20_c20", true );
  TLegend * l20_c20_leg = create_atlas_legend();
  l20_c20_leg->AddEntry( avg_sf_hist_l20_c20, "l20_c20" );
  l20_c20_leg->Draw();

  active_pad = static_cast<TPad *>( sf_scales->cd( 5 ) );
  avg_sf_hist_step->Draw( "HIST E1" );
  hist_prep_axes( avg_sf_hist_step );
  avg_sf_hist_step->GetYaxis()->SetRangeUser( 0.85, 1.1 );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( avg_sf_hist_step, ana_bound.get_x_str(), "Photon SF" );
  add_pad_title( active_pad, "Photon SF - step", true );
  TLegend * step_leg = create_atlas_legend();
  step_leg->AddEntry( avg_sf_hist_step, "step" );
  step_leg->Draw();


  std::cout << output_unique << std::endl;
  sf_scales->SaveAs( "sf_scaling.png" );

}

//void wv_gen_diff( basic_fileset * fileset, const std::string & output_unique, variable_set & variables );
