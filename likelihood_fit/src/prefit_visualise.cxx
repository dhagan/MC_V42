#include <prefit.hxx>
#include <sys/stat.h>



TLegend * fill_BDT_pad( hist_group & hists, const std::string name, TPad * pad ){

  hists.sign_hist->Draw( "HIST E1" );
  hists.data_hist->Draw( "HIST E1 SAME" );
  hists.bckg_hist->Draw( "HIST E1 SAME" );
  hist_prep_axes( hists.sign_hist, true );
  hists.sign_hist->GetYaxis()->SetRangeUser( 0, 
      std::max( { hists.sign_hist->GetMaximum(), hists.bckg_hist->GetMaximum(), hists.data_hist->GetMaximum() } )*1.1 );
  set_axis_labels( hists.sign_hist, "BDT [score]", "Yield/ [score^{-1}]" );
  add_pad_title( pad, name, true );
  add_atlas_decorations( pad, true, false );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( hists.sign_hist,  "sign", "LP" );
  legend->AddEntry( hists.bckg_hist,  "bckg", "LP" );
  legend->AddEntry( hists.data_hist,  "data", "LP" );
  legend->Draw();
  return legend;

}

void prefit_visualise( const std::string & unique, variable_set & variables ){

  prep_style();

  std::cout << "Processing input graphs: " << unique << std::endl;

  std::string workdir = std::string( std::getenv( "OUT_PATH" ) ) + "/trex/" + unique;
  std::string nominal_input_histogram_path = workdir + "/hists/";
  std::vector< std::string > analysis_bins = variables.analysis_bound.get_series_names(); 

  for ( std::string & analysis_bin : analysis_bins ){

    std::string sign_filepath = nominal_input_histogram_path + "/sign_" + unique + "_" + analysis_bin + ".root";
    std::string bckg_filepath = nominal_input_histogram_path + "/bckg_" + unique + "_" + analysis_bin + ".root";
    std::string data_filepath = nominal_input_histogram_path + "/data_" + unique + "_" + analysis_bin + ".root";

    TFile * input_sign_file = new TFile( sign_filepath.c_str(), "READ" );
    TFile * input_bckg_file = new TFile( bckg_filepath.c_str(), "READ" );
    TFile * input_data_file = new TFile( data_filepath.c_str(), "READ" );

    TH1F * sign_sr = static_cast< TH1F * >( input_sign_file->Get( "sr" ) );
    TH1F * bckg_sr = static_cast< TH1F * >( input_bckg_file->Get( "sr" ) );
    TH1F * data_sr = static_cast< TH1F * >( input_data_file->Get( "sr" ) );
    sign_sr->SetLineColor( kRed + 1 ); 
    bckg_sr->SetLineColor( kBlue + 1 );
    data_sr->SetLineColor( 1 );
    hist_group input_sr_hists( sign_sr, bckg_sr, data_sr );
  
    TH1F * sign_cr = static_cast< TH1F * >( input_sign_file->Get( "cr" ) );
    TH1F * bckg_cr = static_cast< TH1F * >( input_bckg_file->Get( "cr" ) );
    TH1F * data_cr = static_cast< TH1F * >( input_data_file->Get( "cr" ) );
    sign_cr->SetLineColor( kRed + 1 ); 
    bckg_cr->SetLineColor( kBlue + 1 );
    data_cr->SetLineColor( 1 );
    hist_group input_cr_hists( sign_cr, bckg_cr, data_cr );


    TCanvas * hist_canvas = new TCanvas( analysis_bin.c_str(), "", 200, 200, 2000, 2000 );
    hist_canvas->Divide( 2, 2 );

    std::string input_sr_name = Form( "Input SR - %s %s",  unique.c_str(), variables.spectator_variable.c_str() );
    std::string input_cr_name = Form( "Input CR - %s %s",  unique.c_str(), variables.spectator_variable.c_str() );
    std::string output_sr_name = Form( "Output SR - %s %s",  unique.c_str(), variables.spectator_variable.c_str() );
    std::string output_cr_name = Form( "Output CR - %s %s",  unique.c_str(), variables.spectator_variable.c_str() );

    TPad * input_sr_pad = (TPad *) hist_canvas->cd( 1 );
    fill_BDT_pad(  input_sr_hists, input_sr_name, input_sr_pad );

    TPad * input_cr_pad = (TPad *) hist_canvas->cd( 2 );
    fill_BDT_pad(  input_cr_hists, input_cr_name, input_cr_pad );

    std::string result_path = workdir + "/" + analysis_bin + "/" + unique + "/Histograms/";
    std::string hist_output_path = workdir + "/" + analysis_bin + "_hists.png";

    struct stat sb;
    if ( stat( result_path.c_str(), &sb ) == 0 ){

      std::string output_filepath =  result_path + unique + "_histos.root";
      TFile * output_file = new TFile( output_filepath.c_str(), "READ" ); 
      TH1F * output_sign_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/sign/nominal/SIGNAL_sign" ) );
      TH1F * output_bckg_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/bckg/nominal/SIGNAL_bckg" ) );
      TH1F * output_data_sr = static_cast< TH1F * >( output_file->Get( "SIGNAL/data/nominal/SIGNAL_data" ) );
      output_sign_sr->SetLineColor( kRed + 1 ); 
      output_bckg_sr->SetLineColor( kBlue + 1 );
      output_data_sr->SetLineColor( 1 );
      hist_group output_sr_hists( output_sign_sr, output_bckg_sr, output_data_sr );
      TPad * output_sr_pad = (TPad *) hist_canvas->cd( 3 );
      fill_BDT_pad(  output_sr_hists, output_sr_name, output_sr_pad );
  
      TH1F * output_sign_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/sign/nominal/CONTROL_sign" ) );
      TH1F * output_bckg_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/bckg/nominal/CONTROL_bckg" ) );
      TH1F * output_data_cr = static_cast< TH1F * >( output_file->Get( "CONTROL/data/nominal/CONTROL_data" ) );
      output_sign_cr->SetLineColor( kRed + 1 ); 
      output_bckg_cr->SetLineColor( kBlue + 1 );
      output_data_cr->SetLineColor( 1 );
      hist_group output_cr_hists( output_sign_cr, output_bckg_cr, output_data_cr );
      TPad * output_cr_pad = (TPad *) hist_canvas->cd( 4 );
      fill_BDT_pad(  output_cr_hists, output_cr_name, output_cr_pad );

      hist_canvas->SaveAs( hist_output_path.c_str() );

    } else {
      hist_canvas->SaveAs( hist_output_path.c_str() );
    }

  }

}

void image_scalefactors( basic_fileset * fileset, variable_set & variables ){

  prep_style();

  std::vector< std::string > bdt_cuts = variables.spectator_bound.get_cut_series();
  std::vector< std::string > bdt_names = variables.spectator_bound.get_series_names();

  TTree * sign_tree = fileset->sign_tree;
  TTree * bckg_tree = fileset->bckg_tree;


  TH1F * muon_sf_sign_mean = variables.spectator_bound.get_hist();
  TH1F * muon_sf_bckg_mean = variables.spectator_bound.get_hist();
  TH1F * photon_sf_sign_mean = variables.spectator_bound.get_hist();
  TH1F * photon_sf_bckg_mean = variables.spectator_bound.get_hist();
   

  for ( size_t bin_idx = 0; bin_idx < bdt_cuts.size(); bin_idx++ ){

    TCanvas * scalefact_canv = new TCanvas( bdt_names.at( bin_idx ).c_str(), "", 200, 200, 2000, 2000 );
    scalefact_canv->Divide( 2, 2 );

    TH1F * photon_sign = new TH1F( "photon_sign", "", 20, 0.95, 1.2 );  
    TH1F * photon_bckg = new TH1F( "photon_bckg", "", 20, 0.95, 1.2 );  
    photon_sign->SetLineColor( kRed + 1 );
    photon_bckg->SetLineColor( kBlue + 1 );

    TH1F * photon_err_sign = new TH1F( "photon_err_sign", "", 20, 0.0, 0.1 );  
    TH1F * photon_err_bckg = new TH1F( "photon_err_bckg", "", 20, 0.0, 0.1 );  
    photon_err_sign->SetLineColor( kRed + 1 );
    photon_err_bckg->SetLineColor( kBlue + 1 );


    TH1F * muon_sign = new TH1F( "muon_sign", "", 20, 0.7, 1.2 );  
    TH1F * muon_bckg = new TH1F( "muon_bckg", "", 20, 0.7, 1.2 );  
    muon_sign->SetLineColor( kRed + 1 );
    muon_bckg->SetLineColor( kBlue + 1 );
    TH1F * muon_sys_sign = new TH1F( "muon_sys_sign", "", 20, 0, 0.1 );  
    TH1F * muon_sys_bckg = new TH1F( "muon_sys_bckg", "", 20, 0, 0.1 );  
    muon_sys_sign->SetLineColor( kRed + 1 );
    muon_sys_bckg->SetLineColor( kBlue + 1 );


    sign_tree->Draw( "photon_sf>>photon_sign", bdt_cuts.at(bin_idx).c_str(), "e goff" );
    sign_tree->Draw( "photon_sf_err>>photon_err_sign", bdt_cuts.at(bin_idx).c_str(),"e goff"  );
    sign_tree->Draw( "muon_sf>>muon_sign", bdt_cuts.at(bin_idx).c_str(), "e goff" );
    sign_tree->Draw( "muon_sf_systematic>>muon_sys_sign", bdt_cuts.at(bin_idx).c_str(), "e goff" );

    bckg_tree->Draw( "photon_sf>>photon_bckg", bdt_cuts.at(bin_idx).c_str(), "e goff" );
    bckg_tree->Draw( "photon_sf_err>>photon_err_bckg", bdt_cuts.at(bin_idx).c_str(),"e goff"  );
    bckg_tree->Draw( "muon_sf>>muon_bckg", bdt_cuts.at(bin_idx).c_str(), "e goff" );
    bckg_tree->Draw( "muon_sf_systematic>>muon_sys_bckg", bdt_cuts.at(bin_idx).c_str(), "e goff" );

    std::cout << bin_idx << std::endl;

    muon_sf_sign_mean->SetBinContent(   bin_idx+1,    muon_sign->GetMean()   );
    muon_sf_sign_mean->SetBinError(     bin_idx+1,    muon_sign->GetRMS()    );
    muon_sf_bckg_mean->SetBinContent(   bin_idx+1,    muon_bckg->GetMean()   ); 
    muon_sf_bckg_mean->SetBinError(     bin_idx+1,    muon_bckg->GetRMS()    ); 
    photon_sf_sign_mean->SetBinContent( bin_idx+1,    photon_sign->GetMean() ); 
    photon_sf_sign_mean->SetBinError(   bin_idx+1,    photon_sign->GetRMS()  ); 
    photon_sf_bckg_mean->SetBinContent( bin_idx+1,    photon_bckg->GetMean() );
    photon_sf_bckg_mean->SetBinError(   bin_idx+1,    photon_bckg->GetRMS()  ); 

    TPad * active_pad = (TPad *) scalefact_canv->cd( 1 );
    photon_sign->Draw( "HIST E1" );
    photon_bckg->Draw( "HIST E1 SAME" );
    hist_prep_axes(  photon_sign, true );
    set_axis_labels( photon_sign, "Photon SF", "Yield" );
    add_pad_title( active_pad, bdt_names.at(bin_idx).c_str(), true );
    add_atlas_decorations( active_pad, true, false );

    active_pad = (TPad *) scalefact_canv->cd( 2 );
    photon_err_sign->Draw( "HIST E1" );
    photon_err_bckg->Draw( "HIST E1 SAME" );
    hist_prep_axes(  photon_err_sign, true );
    set_axis_labels( photon_err_sign, "Photon SF Err", "Yield" );
    add_pad_title( active_pad, bdt_names.at(bin_idx).c_str(), true );
    add_atlas_decorations( active_pad, true, false );

    active_pad = (TPad *) scalefact_canv->cd( 3 );
    muon_sign->Draw( "HIST E1" );
    muon_bckg->Draw( "HIST E1 SAME" );
    hist_prep_axes(  muon_sign, true );
    set_axis_labels( muon_sign, "Muon Trigger SF", "Yield" );
    add_pad_title( active_pad, bdt_names.at(bin_idx).c_str(), true );
    add_atlas_decorations( active_pad, true, false );

    active_pad = (TPad *) scalefact_canv->cd( 4 );
    muon_sys_sign->Draw( "HIST E1" );
    muon_sys_bckg->Draw( "HIST E1 SAME" );
    hist_prep_axes(  muon_sys_sign, true );
    set_axis_labels( muon_sys_sign, "Muon Trigger SF Err", "Yield" );
    add_pad_title( active_pad, bdt_names.at(bin_idx).c_str(), true );
    add_atlas_decorations( active_pad, true, false );

    std::string savestring = "./sf_vis/" + bdt_names.at(bin_idx) + ".png";
    scalefact_canv->SaveAs( savestring.c_str() );

    delete photon_sign;
    delete photon_bckg;
    delete muon_sign;
    delete muon_bckg;
    delete photon_err_sign;
    delete photon_err_bckg;
    delete muon_sys_sign;
    delete muon_sys_bckg;

  }

  TCanvas * fullsf_canv = new TCanvas( "fullsf", "", 200, 200, 2000, 1000 ); 
  fullsf_canv->Divide( 2, 1 );

  muon_sf_sign_mean->SetLineColor( kRed + 1 );
  muon_sf_bckg_mean->SetLineColor( kBlue + 1 );
  photon_sf_sign_mean->SetLineColor( kRed + 1 );
  photon_sf_bckg_mean->SetLineColor( kBlue + 1 );


  TPad * active_pad = (TPad *) fullsf_canv->cd( 1 ); 
  muon_sf_sign_mean->Draw( "HIST E1" );
  muon_sf_bckg_mean->Draw( "HIST E1 SAME" );
  hist_prep_axes(  muon_sf_sign_mean, true );
  set_axis_labels( muon_sf_sign_mean, "BDT Bin", "Average SF" );
  add_pad_title( active_pad, "Average Muon SF", true );
  add_atlas_decorations( active_pad, true, false );

  active_pad = (TPad *) fullsf_canv->cd( 2 ); 
  photon_sf_sign_mean->Draw( "HIST E1" );
  photon_sf_bckg_mean->Draw( "HIST E1 SAME" );
  hist_prep_axes(  photon_sf_sign_mean, true );
  set_axis_labels( photon_sf_sign_mean, "BDT Bin", "Average SF" );
  add_pad_title( active_pad, "Average Photon SF", true );
  add_atlas_decorations( active_pad, true, false );

  fullsf_canv->SaveAs( "./sf_vis/SF.png" );

  delete muon_sf_sign_mean;
  delete muon_sf_bckg_mean;
  delete photon_sf_sign_mean;
  delete photon_sf_bckg_mean;
  delete fullsf_canv;

}

void sf_variation( const std::string & output_unique, variable_set & variables ){

  std::cout << output_unique << std::endl;
  std::vector< std::string > analysis_bins = variables.analysis_bound.get_series_names(); 
  std::string workdir = std::string( std::getenv( "OUT_PATH" ) ) + "/trex/" + output_unique;

  //TH1F * qta_nominal_sign_sr = variables.analysis_bound.get_hist();
  //TH1F * qta_nominal_bckg_sr = variables.analysis_bound.get_hist();
  //TH1F * qta_muonsf_sign_sr = variables.analysis_bound.get_hist();
  //TH1F * qta_muonsf_bckg_sr = variables.analysis_bound.get_hist();
  //TH1F * qta_nominal_sign_cr = variables.analysis_bound.get_hist();
  //TH1F * qta_nominal_bckg_cr = variables.analysis_bound.get_hist();
  //TH1F * qta_muonsf_sign_cr = variables.analysis_bound.get_hist();
  //TH1F * qta_muonsf_bckg_cr = variables.analysis_bound.get_hist();

  for ( size_t bin = 0; bin < analysis_bins.size(); bin++ ){

    //std::string nominal_path = workdir + "/" + analysis_bins[bin] + "/" + output_unique + "/Histograms/";
    std::string nominal_path = workdir + "/hists/", sf_path = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + output_unique;
    std::string tail = "_" + output_unique + "_" + analysis_bins[bin] + ".root";
    std::string nominal_sign_path = nominal_path + "sign" + tail;
    std::string nominal_bckg_path = nominal_path + "bckg" + tail;
    std::string nominal_data_path = nominal_path + "data" + tail;
    std::string sf_sign_path = nominal_path + "sign" + tail;
    std::string sf_bckg_path = nominal_path + "bckg" + tail;
    std::string sf_data_path = nominal_path + "data" + tail;


    TFile * nominal_sign_file = new TFile( nominal_sign_path.c_str(), "READ" );
    TFile * nominal_bckg_file = new TFile( nominal_bckg_path.c_str(), "READ" );
    TFile * nominal_data_file = new TFile( nominal_data_path.c_str(), "READ" );

    TFile * sf_sign_file = new TFile( sf_sign_path.c_str(), "READ" );
    TFile * sf_bckg_file = new TFile( sf_bckg_path.c_str(), "READ" );
    TFile * sf_data_file = new TFile( sf_data_path.c_str(), "READ" );

    TH1F * nominal_sign_sr = (TH1F *) nominal_sign_file->Get( "sr" );
    TH1F * nominal_bckg_sr = (TH1F *) nominal_bckg_file->Get( "sr" );
    TH1F * nominal_data_sr = (TH1F *) nominal_data_file->Get( "sr" );
    TH1F * sf_sign_sr = (TH1F *) sf_sign_file->Get( "sr0" );
    TH1F * sf_bckg_sr = (TH1F *) sf_bckg_file->Get( "sr0" );
    TH1F * sf_data_sr = (TH1F *) sf_data_file->Get( "sr0" );

    nominal_sign_sr->SetLineColor( kRed + 1 ); 
    nominal_bckg_sr->SetLineColor( kBlue + 1 );
    nominal_data_sr->SetLineColor( kBlack + 1 );
    sf_sign_sr->SetLineColor( kRed + 1 ); 
    sf_bckg_sr->SetLineColor( kBlue + 1 );
    sf_data_sr->SetLineColor( kBlack + 1 );
    sf_sign_sr->SetLineStyle( 2 ); 
    sf_bckg_sr->SetLineStyle( 2 );
    sf_data_sr->SetLineStyle( 2 );

    TH1F * nominal_sign_cr = (TH1F *) nominal_sign_file->Get( "cr" );
    TH1F * nominal_bckg_cr = (TH1F *) nominal_bckg_file->Get( "cr" );
    TH1F * nominal_data_cr = (TH1F *) nominal_data_file->Get( "cr" );
    TH1F * sf_sign_cr = (TH1F *) sf_sign_file->Get( "cr0" );
    TH1F * sf_bckg_cr = (TH1F *) sf_bckg_file->Get( "cr0" );
    TH1F * sf_data_cr = (TH1F *) sf_data_file->Get( "cr0" );

    nominal_sign_cr->SetLineColor( kRed + 1 ); 
    nominal_bckg_cr->SetLineColor( kBlue + 1 );
    nominal_data_cr->SetLineColor( kBlack + 1 );
    sf_sign_cr->SetLineColor( kRed + 1 ); 
    sf_bckg_cr->SetLineColor( kBlue + 1 );
    sf_data_cr->SetLineColor( kBlack + 1 );
    sf_sign_cr->SetLineStyle( 2 ); 
    sf_bckg_cr->SetLineStyle( 2 );
    sf_data_cr->SetLineStyle( 2 );

    //qta_nominal_sign_sr->SetBinContent( bin+1, nominal_sign_sr->Integral() ); 
    //qta_nominal_bckg_sr->SetBinContent( bin+1,  );
    //qta_muonsf_sign_sr->SetBinContent(  bin+1,  );
    //qta_muonsf_bckg_sr->SetBinContent(  bin+1,  );
    //qta_nominal_sign_cr->SetBinContent( bin+1,  );
    //qta_nominal_bckg_cr->SetBinContent( bin+1,  );
    //qta_muonsf_sign_cr->SetBinContent(  bin+1,  );
    //qta_muonsf_bckg_cr->SetBinContent(  bin+1,  );

    TCanvas * bin_canv = new TCanvas( analysis_bins[bin].c_str(), "", 200, 200, 3000, 2000 );
    bin_canv->Divide( 3, 2 );

    TPad * active_pad = (TPad *) bin_canv->cd( 1 ); 
    nominal_sign_sr->Draw( "HIST E1" );
    sf_sign_sr->Draw( "HIST E1 SAME" );
    hist_prep_axes(  nominal_sign_sr, true );
    set_axis_labels( nominal_sign_sr, "BDT Bin", "" );
    add_pad_title( active_pad, "Signal BDT", true );
    add_atlas_decorations( active_pad, true, false );
    TLegend * sr_sign_legend = create_atlas_legend();
    sr_sign_legend->AddEntry( nominal_sign_sr, "nominal sr" );
    sr_sign_legend->AddEntry( sf_sign_sr, "sf sr" );
    sr_sign_legend->Draw( "SAME" );

    active_pad = (TPad *) bin_canv->cd( 2 ); 
    nominal_bckg_sr->Draw( "HIST E1" );
    sf_bckg_sr->Draw( "HIST E1 SAME" );
    hist_prep_axes(  nominal_bckg_sr, true );
    set_axis_labels( nominal_bckg_sr, "BDT Bin", "" );
    add_pad_title( active_pad, "Background BDT", true );
    add_atlas_decorations( active_pad, true, false );
    TLegend * sr_bckg_legend = create_atlas_legend();
    sr_bckg_legend->AddEntry( nominal_bckg_sr, "nominal sr" );
    sr_bckg_legend->AddEntry( sf_bckg_sr, "sf_sr" );
    sr_bckg_legend->Draw( "SAME" );

    active_pad = (TPad *) bin_canv->cd( 3 ); 
    nominal_data_sr->Draw( "HIST E1" );
    sf_data_sr->Draw( "HIST E1 SAME" );
    hist_prep_axes(  nominal_data_sr, true );
    set_axis_labels( nominal_data_sr, "BDT Bin", "" );
    add_pad_title( active_pad, "Background BDT", true );
    add_atlas_decorations( active_pad, true, false );
    TLegend * sr_data_legend = create_atlas_legend();
    sr_data_legend->AddEntry( nominal_data_sr, "nominal sr" );
    sr_data_legend->AddEntry( sf_data_sr, "sf_sr" );
    sr_data_legend->Draw( "SAME" );

    active_pad = (TPad *) bin_canv->cd( 4 ); 
    nominal_sign_cr->Draw( "HIST E1" );
    sf_sign_cr->Draw( "HIST E1 SAME" );
    hist_prep_axes(  nominal_sign_cr, true );
    set_axis_labels( nominal_sign_cr, "BDT Bin", "" );
    add_pad_title( active_pad, "Signal BDT", true );
    add_atlas_decorations( active_pad, true, false );
    TLegend * cr_sign_legend = create_atlas_legend();
    cr_sign_legend->AddEntry( nominal_sign_cr, "nominal cr" );
    cr_sign_legend->AddEntry( sf_sign_cr, "sf cr" );
    cr_sign_legend->Draw( "SAME" );


    active_pad = (TPad *) bin_canv->cd( 5 ); 
    nominal_bckg_cr->Draw( "HIST E1" );
    sf_bckg_cr->Draw( "HIST E1 SAME" );
    hist_prep_axes(  nominal_bckg_cr, true );
    set_axis_labels( nominal_bckg_cr, "BDT Bin", "" );
    add_pad_title( active_pad, "Background BDT", true );
    add_atlas_decorations( active_pad, true, false );
    TLegend * cr_bckg_legend = create_atlas_legend();
    cr_bckg_legend->AddEntry( nominal_bckg_cr, "nominal cr" );
    cr_bckg_legend->AddEntry( sf_bckg_cr, "sf_cr" );
    cr_bckg_legend->Draw( "SAME" );

    active_pad = (TPad *) bin_canv->cd( 6 ); 
    nominal_data_cr->Draw( "HIST E1" );
    sf_data_cr->Draw( "HIST E1 SAME" );
    hist_prep_axes(  nominal_data_cr, true );
    set_axis_labels( nominal_data_cr, "BDT Bin", "" );
    add_pad_title( active_pad, "Background BDT", true );
    add_atlas_decorations( active_pad, true, false );
    TLegend * cr_data_legend = create_atlas_legend();
    cr_data_legend->AddEntry( nominal_data_cr, "nominal cr" );
    cr_data_legend->AddEntry( sf_data_cr, "sf_cr" );
    cr_data_legend->Draw( "SAME" );

    std::string savestring = "./sf_var/" + analysis_bins.at( bin ) + ".png";
    bin_canv->SaveAs( savestring.c_str() );

    nominal_sign_file->Close();
    nominal_bckg_file->Close();
    nominal_data_file->Close();
    sf_sign_file->Close(); 
    sf_bckg_file->Close();
    sf_data_file->Close();

    delete nominal_sign_cr; 
    delete nominal_bckg_cr;
    delete nominal_data_cr;
    delete sf_sign_cr; 
    delete sf_bckg_cr; 
    delete sf_data_cr; 
    delete nominal_sign_sr; 
    delete nominal_bckg_sr;
    delete nominal_data_sr;
    delete sf_sign_sr; 
    delete sf_bckg_sr; 
    delete sf_data_sr; 

  }

  
}

// lets think about how I image bloom
// the average error?
// bloom fraction/bin
void image_bloom( basic_fileset * fileset, variable_set & variables ){

  TTree * sign_tree = fileset->get_tree( sample_type::sign );
  TTree * bckg_tree = fileset->get_tree( sample_type::bckg );

  std::vector< std::string > ana_cut_names = variables.analysis_bound.get_series_names();
  std::vector< std::string > ana_cut_series = variables.analysis_bound.get_cut_series();

  TH1F * sign_bloom_fraction = variables.analysis_bound.get_hist();
  TH1F * bckg_bloom_fraction = variables.analysis_bound.get_hist();

  TH1F * sign_db_bloomval = variables.analysis_bound.get_hist();
  TH1F * bckg_db_bloomval = variables.analysis_bound.get_hist();
  TH1F * sign_qb_bloomval = variables.analysis_bound.get_hist();
  TH1F * bckg_qb_bloomval = variables.analysis_bound.get_hist();

  TH1F * sign_error = variables.analysis_bound.get_hist();
  TH1F * bckg_error = variables.analysis_bound.get_hist();
  TH1F * sign_db_error = variables.analysis_bound.get_hist();
  TH1F * bckg_db_error = variables.analysis_bound.get_hist();
  TH1F * sign_qb_error = variables.analysis_bound.get_hist();
  TH1F * bckg_qb_error = variables.analysis_bound.get_hist();


  TH1F * b_hist = new TH1F( "bh", "", 100, -10, 10 );
  TH1F * nb_hist = new TH1F( "nbh", "", 100, -10, 10 );
  TH1F * e_hist = new TH1F( "eh", "", 100, -10, 10 );

  for ( size_t bin_idx = 0; bin_idx < ana_cut_names.size(); bin_idx++ ){

    char qta_window[300]; sprintf( qta_window, "%s&&(abs(qtB)>0.000)&&(abs(qtB)<6.000)", ana_cut_series[bin_idx].c_str() );
    char qta_window_bloom[300]; sprintf( qta_window_bloom, "%s&&(photon_bloom_db > 1)&&(abs(qtB)>0.000)&&(abs(qtB)<6.000)", ana_cut_series[bin_idx].c_str() );

    sign_tree->Draw( "photon_bloom_db>>bh", qta_window_bloom, "e goff" );
    sign_tree->Draw( "photon_bloom_db>>nbh", qta_window, "e goff" );
    sign_bloom_fraction->SetBinContent( bin_idx+1, b_hist->Integral()/nb_hist->Integral() );
    bckg_tree->Draw( "photon_bloom_db>>bh", qta_window_bloom, "e goff" );
    bckg_tree->Draw( "photon_bloom_db>>nbh", qta_window, "e goff" );
    bckg_bloom_fraction->SetBinContent( bin_idx+1, b_hist->Integral()/nb_hist->Integral() );

    sign_tree->Draw( "photon_sf_err>>eh", qta_window, "e goff" );
    sign_error->SetBinContent( bin_idx+1, e_hist->GetMean() );
    sign_error->SetBinError( bin_idx+1, e_hist->GetStdDev() );
    bckg_tree->Draw( "photon_sf_err>>eh", qta_window, "e goff" );
    bckg_error->SetBinContent( bin_idx+1, e_hist->GetMean() );
    bckg_error->SetBinError( bin_idx+1, e_hist->GetStdDev() );

    sign_tree->Draw( "photon_sf_err_db>>eh", qta_window, "e goff" );
    sign_db_error->SetBinContent( bin_idx+1, e_hist->GetMean() );
    sign_db_error->SetBinError( bin_idx+1, e_hist->GetStdDev() );
    bckg_tree->Draw( "photon_sf_err_db>>eh", qta_window, "e goff" );
    bckg_db_error->SetBinContent( bin_idx+1, e_hist->GetMean() );
    bckg_db_error->SetBinError( bin_idx+1, e_hist->GetStdDev() );

    sign_tree->Draw( "photon_sf_err_qb>>eh", qta_window, "e goff" );
    sign_qb_error->SetBinContent( bin_idx+1, e_hist->GetMean() );
    sign_qb_error->SetBinError( bin_idx+1, e_hist->GetStdDev() );
    bckg_tree->Draw( "photon_sf_err_qb>>eh", qta_window, "e goff" );
    bckg_qb_error->SetBinContent( bin_idx+1, e_hist->GetMean() );
    bckg_qb_error->SetBinError( bin_idx+1, e_hist->GetStdDev() );

    sign_tree->Draw( "photon_bloom_db>>eh", qta_window, "e goff" );
    sign_db_bloomval->SetBinContent( bin_idx+1, e_hist->GetMean() );
    sign_db_bloomval->SetBinError( bin_idx+1, e_hist->GetStdDev() );
    bckg_tree->Draw( "photon_bloom_db>>eh", qta_window, "e goff" );
    bckg_db_bloomval->SetBinContent( bin_idx+1, e_hist->GetMean() );
    bckg_db_bloomval->SetBinError( bin_idx+1, e_hist->GetStdDev() );
    
    sign_tree->Draw( "photon_bloom_qb>>eh", qta_window, "e goff" );
    sign_qb_bloomval->SetBinContent( bin_idx+1, e_hist->GetMean() );
    sign_qb_bloomval->SetBinError( bin_idx+1, e_hist->GetStdDev() );
    bckg_tree->Draw( "photon_bloom_qb>>eh", qta_window, "e goff" );
    bckg_qb_bloomval->SetBinContent( bin_idx+1, e_hist->GetMean() );
    bckg_qb_bloomval->SetBinError( bin_idx+1, e_hist->GetStdDev() );

  }

  sign_bloom_fraction->SetLineColor( kRed + 1 );
  bckg_bloom_fraction->SetLineColor( kBlue + 1 );
  sign_error->SetLineColor( kRed + 1 );
  bckg_error->SetLineColor( kBlue + 1 );
  sign_db_error->SetLineColor( kRed + 1 );
  bckg_db_error->SetLineColor( kBlue + 1 );
  sign_qb_error->SetLineColor( kRed + 1 );
  bckg_qb_error->SetLineColor( kBlue + 1 );
  sign_db_bloomval->SetLineColor( kRed + 1 );
  bckg_db_bloomval->SetLineColor( kBlue + 1 );
  sign_qb_bloomval->SetLineColor( kRed + 1 );
  bckg_qb_bloomval->SetLineColor( kBlue + 1 );


  gStyle->SetOptStat( 0 );

  TCanvas * bloom_canv = new TCanvas( "bcanv", "", 200, 200, 3000, 2000 );
  bloom_canv->Divide( 3, 2 );

  TPad * active_pad = (TPad *) bloom_canv->cd( 1 );
  sign_error->Draw( "HIST E1" );
  bckg_error->Draw( "HIST E1 SAME" );
  hist_prep_axes(  sign_error, true );
  set_axis_labels( sign_error, "q_{T}^{A} [GeV]", "Mean SF Error" );
  add_pad_title( active_pad, "SF error", true );
  add_atlas_decorations( active_pad, true, false );
  TLegend * eo_legend = create_atlas_legend();
  eo_legend->AddEntry( sign_error, "sign" );
  eo_legend->AddEntry( bckg_error, "bckg" );
  eo_legend->Draw();

  active_pad = (TPad *) bloom_canv->cd( 2 );
  sign_db_error->Draw( "HIST E1" );
  bckg_db_error->Draw( "HIST E1 SAME" );
  hist_prep_axes(  sign_db_error, true );
  set_axis_labels( sign_db_error, "q_{T}^{A} [GeV]", "Mean SF Error" );
  add_pad_title( active_pad, "DB SF error", true );
  add_atlas_decorations( active_pad, true, false );
  TLegend * ed_legend = create_atlas_legend();
  ed_legend->AddEntry( sign_db_error, "sign" );
  ed_legend->AddEntry( bckg_db_error, "bckg" );
  ed_legend->Draw();

  active_pad = (TPad *) bloom_canv->cd( 3 );
  sign_qb_error->Draw( "HIST E1" );
  bckg_qb_error->Draw( "HIST E1 SAME" );
  hist_prep_axes(  sign_qb_error, true );
  set_axis_labels( sign_qb_error, "q_{T}^{A} [GeV]", "Mean SF Error" );
  add_pad_title( active_pad, "QB SF error", true );
  add_atlas_decorations( active_pad, true, false );
  TLegend * eq_legend = create_atlas_legend();
  eq_legend->AddEntry( sign_qb_error, "sign" );
  eq_legend->AddEntry( bckg_qb_error, "bckg" );
  eq_legend->Draw();


  active_pad = (TPad *) bloom_canv->cd( 4 );
  sign_bloom_fraction->Draw( "HIST" );
  bckg_bloom_fraction->Draw( "HIST SAME" );
  hist_prep_axes(  sign_bloom_fraction, true );
  set_axis_labels( sign_bloom_fraction, "q_{T}^{A} [GeV]", "bloom fraction" );
  add_pad_title( active_pad, "Bloom Fraction", true );
  add_atlas_decorations( active_pad, true, false );
  TLegend * bloom_legend = create_atlas_legend();
  bloom_legend->AddEntry( sign_bloom_fraction, "sign" );
  bloom_legend->AddEntry( bckg_bloom_fraction, "bckg" );
  bloom_legend->Draw();

  active_pad = (TPad *) bloom_canv->cd( 5 );
  sign_db_bloomval->Draw( "HIST E1" );
  bckg_db_bloomval->Draw( "HIST E1 SAME" );
  hist_prep_axes(  sign_db_bloomval, true );
  set_axis_labels( sign_db_bloomval, "q_{T}^{A} [GeV]", "Mean Bloom" );
  add_pad_title( active_pad, "DB", true );
  add_atlas_decorations( active_pad, true, false );
  TLegend * dbe_legend = create_atlas_legend();
  dbe_legend->AddEntry( sign_db_bloomval, "sign" );
  dbe_legend->AddEntry( bckg_db_bloomval, "bckg" );
  dbe_legend->Draw();

  active_pad = (TPad *) bloom_canv->cd( 6 );
  sign_qb_bloomval->Draw( "HIST E1" );
  bckg_qb_bloomval->Draw( "HIST E1 SAME" );
  hist_prep_axes(  sign_qb_bloomval, true );
  set_axis_labels( sign_qb_bloomval, "q_{T}^{A} [GeV]", "Mean Bloom" );
  add_pad_title( active_pad, "QB", true );
  add_atlas_decorations( active_pad, true, false );
  TLegend * qbe_legend = create_atlas_legend();
  qbe_legend->AddEntry( sign_qb_bloomval, "sign" );
  qbe_legend->AddEntry( bckg_qb_bloomval, "bckg" );
  qbe_legend->Draw();

  bloom_canv->SaveAs( "./photon_sf_bloom.png" );


}

  //TCanvas * bd_canv = new TCanvas( "bd_canv", "", 200, 200, 1000, 1000 );
  //bd_canv->Divide( 1, 1 );

  //TH1F * bloom_diff = (TH1F *) sign_bloom_fraction->Clone();
  //bloom_diff->Reset();

  //for ( size_t bin_idx = 1; bin_idx <= ana_cut_names.size(); bin_idx++ ){

  //  double s = sign_bloom_fraction->GetBinContent( bin_idx );
  //  double b = bckg_bloom_fraction->GetBinContent( bin_idx );
  //  bloom_diff->SetBinContent( bin_idx, s - b + 1.0 );