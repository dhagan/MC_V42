#include <main.hxx>
#include <func_hf.hxx>
#include <generate_hists.hxx>
#include <generate_configs.hxx>
#include <generate_sr.hxx>
#include <prefit.hxx>
#include <postprocess.hxx>
#include <mwe.hxx>
#include <musf.hxx>
#include <variation.hxx>
#include <systematic.hxx>
#include <trex_data_dump.hxx>
#include <quickplot.hxx>
#include <full_result.hxx>
#include <validation.hxx>

#include <yaml-cpp/yaml.h>

int help(){
  std::cout << " Usage:" << std::endl;
  std::cout << " ./gen --input,-i INPUT_PATH" << std::endl;
  std::cout << " " << std::endl;
  return 0;
}

void parse_filename( std::string & filename, std::string & avar, std::string & svar ){
  std::vector< std::string > files;
  split_strings( files, filename, ":" );
  std::string parse = files.at(0);
  parse = parse.substr( parse.find( "A" ), parse.length() );
  avar = parse.substr( 0, parse.find( "_" ) );
  parse = parse.substr( parse.find( "S" ), parse.length() );
  svar = parse.substr( 0, parse.find( "_" ) );
};

int main( int argc, char * argv[] ){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "help",        no_argument,              0,      'h'},
      { "input",       required_argument,        0,      'i'},
      {0,             0,                        0,      0}
    };

  std::string input = "";
  do {
    option = getopt_long(argc, argv, "i:ph", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'i': 
        input         = std::string( optarg );
        break;
    }
  } while (option != -1);

  if ( input.empty() ){
    std::cout << "No input, exiting..." << std::endl;
    return 0;
  }

  std::cout << input << std::endl;
  YAML::Node config = YAML::LoadFile( input );
  std::vector< std::string > run_list = config["general"]["runs"].as< std::vector<std::string > >();
  if ( run_list.size() == 0 ){ 
    std::cout << "No runs provided exiting..." << std::endl;
  }

  for ( std::string & run : run_list ){

    YAML::Node run_node = config[ run ];

    std::string input_filepath = "";
    std::string avar = "qtA", svar = "BDT", cuts = "";
    std::string ranges;
    //std::string sr_type = "sr", cr_type = "cr";
    std::string input_unique, output_unique;
    std::string stats_path = "", trex_path = "", config_path = "";

    std::vector< std::string > modes = run_node[ "modes" ].as< std::vector< std::string > >();
    if ( modes.size() == 0 ){ 
      std::cout << "No mode provided in node:" << std::endl; 
      std::cout << run_node << std::endl;
      std::cout << "continuuing...." << std::endl;
      continue;
    }
    if ( run_node[ "avar" ] ){ avar = run_node["avar"].as< std::string >(); }
    if ( run_node[ "svar" ] ){ svar = run_node["svar"].as< std::string >(); }
    if ( run_node[ "ranges" ] ){ ranges = run_node["ranges"].as< std::string >(); }
    if ( run_node[ "input_unique" ] ){ 
      input_unique = run_node[ "input_unique" ].as< std::string >();
      input_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
        + "/scalefactors/" 
        + input_unique;
    } 
    if ( run_node[ "output_unique" ] ){ output_unique = run_node[ "output_unique" ].as< std::string >(); }
    if ( run_node[ "cuts" ] ){ cuts = run_node[ "cuts" ].as< std::string >(); }
    if ( run_node[ "config_path" ] ){ config_path = run_node[ "config_path" ].as< std::string >();
    } else { ( config_path = output_unique ); }
  
    if ( run_node[ "trex_path" ] ){ trex_path = run_node[ "trex_path" ].as< std::string >();
    } else { trex_path = std::string( std::getenv( "OUT_PATH" ) ) + "/trex/" + output_unique + "/"; }


    variable_set variables = variable_set( "", ranges, avar, svar );
    variables.set_extra_bounds( cuts );

    for ( std::string & mode : modes ){
  
      if ( mode.find( "script" ) != std::string::npos ){
        generate_configs( run_node, variables );
      }

      if ( mode.find( "gen" ) != std::string::npos ){
        generate_hists( run_node, variables );  
      }
      
      if ( mode.find( "variation" ) != std::string::npos ){
        region sr_type = region_convert( run_node[ "sr_type" ].as< std::string >() );
        region cr_type = region_convert( run_node[ "cr_type" ].as< std::string >() );
        variation( run_node, variables, sample_type::sign, sr_type );
        variation( run_node, variables, sample_type::sign, cr_type );
        variation( run_node, variables, sample_type::bckg, sr_type );
        variation( run_node, variables, sample_type::bckg, cr_type );
        variation_input( run_node, variables, sr_type, cr_type );
      }  

      if ( mode.find( "musf" ) != std::string::npos ){
        region sr_type = region_convert( run_node[ "sr_type" ].as< std::string >() );
        region cr_type = region_convert( run_node[ "cr_type" ].as< std::string >() );
        musf( run_node, variables, sample_type::sign, sr_type );
        musf( run_node, variables, sample_type::bckg, sr_type );
        std::cout << "huh" << std::endl;
        musf_input( run_node, variables, sr_type, cr_type );
        std::cout << "huh" << std::endl;
      } 

      if ( mode.find( "postprocess" ) != std::string::npos ){
        quickplot_nr( run_node, variables );
      }
      

      // slightly more deprecated functions
      if ( mode.find( "systematic" ) != std::string::npos ){
        systematic( run_node, variables );
        continue;
      }
      if ( mode.find( "validation" ) != std::string::npos ){
        validation( run_node, variables );
        continue;
      }
      if ( mode.find( "full" ) != std::string::npos ){ 
        full_result( run_node, variables );
        continue;
      }

      if ( mode.find( "sf_exp" ) != std::string::npos ){
        basic_fileset * fileset = new basic_fileset();
        fileset->set_unique( output_unique );
        fileset->load_subtraction_fileset( input_filepath, input_unique, true );
        sf_explore( fileset, output_unique, variables );
      }

      //if ( mode.find( "postprocess" ) != std::string::npos ){
      //  //std::string eff_unique = run_node[ "eff_unique" ].as<std::string>();
      //  //postprocess( output_unique, input_unique, input_filepath, 
      //  //             variables, trex_path, nonstat_unique,
      //  //             eff_unique );
      //  //fit_visualise( output_unique, variables );
      //  //basic_fileset * fileset = new basic_fileset();
      //  //fileset->set_unique( output_unique );
      //  //fileset->load_subtraction_fileset( input_filepath, input_unique, true );
      //  //wv( fileset, output_unique, variables );
      //  //image_bloom( fileset, variables );
      //  //trex_data_dump( run_node, variables, fileset, systematics );
      //  continue;
      //}    
    }
  }
 
  return 0;

}

//else if ( mode.find( "sr" ) != std::string::npos ){
//
//  if ( avar.empty() || svar.empty() ){ parse_filename( input, avar, svar ); }
//  bound_mgr * selections = new bound_mgr();
//  selections->load_bound_mgr( bounds_path );
//  if ( !range.empty() ){ selections->process_bounds_string( range ); }
//  generate_sr( input, avar, svar, cuts, output_unique, selections );  

//if ( sr_type != cr_type ){
        //  musf( run_node, variables, sample_type::sign, sr_type );
        //  musf( run_node, variables, sample_type::bckg, sr_type );
        //  musf( run_node, variables, sample_type::sign, cr_type );
        //  musf( run_node, variables, sample_type::bckg, cr_type );
        //} else {