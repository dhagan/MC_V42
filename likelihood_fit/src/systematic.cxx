#include <systematic.hxx>

TH1F * systematic( YAML::Node & run_node, variable_set & variables, bool return_uniform ){
  std::string group_unique = run_node[ "group_unique" ].as<std::string>();
  std::string base_unique = run_node[ "baseline_unique" ].as<std::string>();
  std::vector<std::string> systematic_uniques = run_node[ "systematic_uniques" ].as<std::vector<std::string>>();
  return systematic( base_unique, group_unique, systematic_uniques, variables, return_uniform );
}


  
TH1F * systematic( const std::string & base_unique, const std::string & group_unique,
                    std::vector<std::string> systematic_uniques, variable_set & variables, 
                    bool return_uniform ){

  prep_style();

  std::vector< hist_group > extracted_results;
  std::vector< TH1F * > total_fit_yields;
  std::vector< TH1F * > sign_abs_differences;
  extracted_results.reserve( systematic_uniques.size() );

 for ( std::string & sys_unique : systematic_uniques ){

    auto[ total_yield, extracted ] = get_result( sys_unique, variables );
    extracted_results.push_back( std::move( extracted ) );
    total_fit_yields.push_back( total_yield );

  }

  auto[ baseline_total, baseline_extracted ] = get_result( base_unique, variables );

  TH1F * zero = new TH1F( "zero", "", 15, -10, 20 );
  for ( int bin = 1; bin <= 15; bin++ ){ zero->SetBinContent(bin, 0.0 ); }
  zero->SetLineColor( kBlack );

  std::transform( 
    extracted_results.begin(), extracted_results.end(), std::back_inserter( sign_abs_differences ),
    [ &baseline_extracted ]( hist_group & results ){
      TH1F * abs_diff = static_cast<TH1F*>( results.sign_hist->Clone() );
      abs_diff->Add( baseline_extracted.sign_hist, abs_diff, 1.0, -1.0 );
      return abs_diff;
    }
  );

  TH1F * max_hist = variables.analysis_bound.get_hist( "max_hist" );
  TH1F * min_hist = static_cast<TH1F *>( baseline_extracted.sign_hist->Clone( "min_hist" ) );
  std::for_each( 
    extracted_results.begin(), extracted_results.end(),
    [ &max_hist, &min_hist]( hist_group & results ){
      for ( int bin_idx = 1; bin_idx <= max_hist->GetNbinsX(); bin_idx++ ){
        if ( results.sign_hist->GetBinContent( bin_idx ) >= max_hist->GetBinContent( bin_idx ) ){
          max_hist->SetBinContent( bin_idx, results.sign_hist->GetBinContent( bin_idx ) );
        }
        if ( results.sign_hist->GetBinContent( bin_idx) <= min_hist->GetBinContent( bin_idx ) ){
          min_hist->SetBinContent( bin_idx, results.sign_hist->GetBinContent( bin_idx ) );
        }
      }
    }
  );

  TH1F * stat_hist = static_cast< TH1F * >( baseline_extracted.sign_hist->Clone( "stat_hist" ) );
  TH1F * sys_hist = static_cast< TH1F * >( baseline_extracted.sign_hist->Clone( "sys_hist" ) );
  TH1F * errorbar_hist = static_cast< TH1F * >( baseline_extracted.sign_hist->Clone( "errorbar_hist" ) );
  for ( int bin_idx = 1; bin_idx <= max_hist->GetNbinsX(); bin_idx++ ){
    double uni_error = uniform_error( max_hist->GetBinContent( bin_idx ), min_hist->GetBinContent( bin_idx ) );
    sys_hist->SetBinContent( bin_idx, uni_error );
    errorbar_hist->SetBinError( bin_idx, absolute_quadrature_two( uni_error, errorbar_hist->GetBinError( bin_idx ) ) ); 
  }

  min_hist->SetLineColorAlpha( kRed+2, 1.0 );
  max_hist->SetLineColorAlpha( kRed+2, 1.0 );

  std::vector< int > colors = { kRed, kBlue, kGreen, kMagenta };
  auto color_itr = colors.begin();

  TCanvas * sys_canv = new TCanvas( "sc", "",  200, 200, 2000, 2000 );
  sys_canv->Divide( 2, 2 );

  TPad * active_pad = static_cast<TPad*>( sys_canv->cd( 1 ) );
  zero->Draw( "HIST" );
  hist_prep_axes( zero );
  zero->GetYaxis()->SetRangeUser( -400, 600 );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( zero, "q_{T}^{A}", "#Delta_{absolute}" );
  add_pad_title( active_pad, "Postfit, absolute deviation", false );
  TLegend * abs_leg = below_logo_legend();
  //abs_leg->SetY2NDC( 0.4 );
  int step = 0;
  std::for_each(   
    sign_abs_differences.begin(), sign_abs_differences.end(), 
    [ &step, &color_itr, &abs_leg ]( TH1F * hist ){  
      hist->Draw( "HIST SAME P" ); 
      hist->Draw( "HIST SAME" ); 
      hist->SetLineColorAlpha( *color_itr+step, 1.0 );
      hist->SetLineStyle( 1 );
      hist->SetLineWidth( 1 );
      hist->SetMarkerStyle( step+2 );
      hist->SetMarkerColor( *color_itr+step );
      abs_leg->AddEntry( hist, hist->GetName() );
      step++;
      if ( step >= 4 ){ step = 0; color_itr++; }
    }
  );
  abs_leg->Draw();

  active_pad = static_cast<TPad*>( sys_canv->cd( 2 ) );
  baseline_extracted.sign_hist->Draw( "HIST" );
  max_hist->Draw( "HIST SAME" );
  min_hist->Draw( "HIST SAME" );
  hist_prep_axes( baseline_extracted.sign_hist );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( baseline_extracted.sign_hist, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Nominal, maximal deviations", false );
  TLegend * var_leg = create_atlas_legend();
  var_leg->AddEntry( baseline_extracted.sign_hist, "nominal" );
  var_leg->AddEntry( max_hist, "max" );
  var_leg->AddEntry( min_hist, "min" );
  var_leg->Draw();

  active_pad = static_cast<TPad*>( sys_canv->cd( 3 ) );
  stat_hist->Draw( "HIST E1" );
  hist_prep_axes( stat_hist );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( stat_hist, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Nominal, stat error", true );
  TLegend * stat_leg = create_atlas_legend();
  stat_leg->AddEntry( stat_hist, "stat" );
  stat_leg->Draw();

  active_pad = static_cast<TPad*>( sys_canv->cd( 4 ) );
  errorbar_hist->Draw( "HIST E1" );
  hist_prep_axes( errorbar_hist );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( errorbar_hist, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Nominal, stat+sys error", true );
  TLegend * err_leg = create_atlas_legend();
  err_leg->AddEntry( errorbar_hist, "nominal" );
  err_leg->Draw();
  
  std::string output_filename = group_unique + ".png";
  sys_canv->SaveAs( output_filename.c_str() );

  for( auto hg : extracted_results ){ hg.erase(); }
  extracted_results.clear();
  sign_abs_differences.clear();
  delete zero;
  delete baseline_total;
  baseline_extracted.erase();
  delete sys_canv;
  delete max_hist;
  delete min_hist;

  if ( return_uniform ){
    return sys_hist;
  } else {
    delete sys_hist;
    return nullptr;
  }

  //std::for_each( 
  //  sign_abs_differences.begin(), sign_abs_differences.end(), 
  //  []( TH1F * hist ){ 
  //    std::cout << hist->GetName() << std::setw(10) << " " << hist->Integral() << std::endl; 
  //  } 
  //);

}
