#include <validation.hxx>


void validation( YAML::Node run_node, variable_set & variables ){

  std::string unique = run_node[ "input_unique" ].as<std::string>();
  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();
  std::vector< std::string > val_vars = run_node[ "validation_variables" ].as<std::vector<std::string>>();
  std::string config_name = run_node[ "config_name" ].as<std::string>();
  std::string install_path = std::getenv("ANA_IP");
  char config_path[100];
  sprintf( config_path, "%s/likelihood_fit/share/%s", install_path.c_str(), config_name.c_str() );


  std::vector< bound > val_bounds;
  std::vector< std::vector<float> > val_bins;
  std::vector< TH1F * > val_hists;
  char bins_path[250];
  sprintf( bins_path, "%s/bins.yaml", config_path );
  YAML::Node bin_yaml = YAML::LoadFile( bins_path );
  std::for_each( val_vars.cbegin(), val_vars.cend(),
    [ &variables, &val_bounds, &val_bins, &val_hists, &bin_yaml ]( const std::string & variable ){
      val_bounds.push_back( variables.bound_manager->get_bound( variable ) );
      val_bins.push_back( bin_yaml[variable+"_arr"].as<std::vector<float>>() );
      for ( float & bine : val_bins.back() ){ std::cout << bine << " "; };
      std::cout << std::endl;
      val_hists.emplace_back( new TH1F( variable.c_str(), "", val_bins.back().size()-1, &val_bins.back()[0] ) );
    }  
  );

  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();

  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    std::string plots_path = trex_qta_path + "Plots/";

    for ( size_t val_idx = 0; val_idx < val_bounds.size(); val_idx++ ){

      std::string val_file = plots_path + "/VALIDATION" 
                                  + std::to_string( val_idx + 1 ) + "_postfit.yaml";
      YAML::Node val_node = YAML::LoadFile( val_file );
      std::vector< double > yields = val_node["Samples"][0]["Yield"].as<std::vector<double>>();
      TH1F * val_hist = val_hists[val_idx];
      for ( int bin_idx = 0; bin_idx < val_hist->GetNbinsX(); bin_idx++ ){
        val_hist->AddBinContent( bin_idx+1, yields[bin_idx] );
      }
    }
  }

  prep_style();
  gStyle->SetOptStat( "rmien" );
  std::cout << val_hists.size() << std::endl;
  std::for_each( val_hists.begin(), val_hists.end(),
    []( TH1F * val_hist ){
      TCanvas * val_canv = new TCanvas( "val_canv", "", 200,200, 1000, 1000 );
      val_canv->Divide( 1, 1 );
      TPad * active_pad = static_cast<TPad *>( val_canv->cd(1) );
      val_hist->Draw( "HIST E1");
      hist_prep_axes( val_hist );
      add_atlas_decorations( active_pad, true );
      set_axis_labels( val_hist, val_hist->GetName(), "Yield" );
      add_pad_title( active_pad, val_hist->GetName() );
      TLegend * val_legend = below_logo_legend();
      val_legend->AddEntry( val_hist, "sign", "lep" );
      val_legend->Draw( "SAME" );
      TPaveStats * val_stats = make_stats( val_hist );
      val_stats->Draw( "SAME" );
      std::string output_name = "./validate/" + std::string( val_hist->GetName() ) + "_validation.png";
      val_canv->SaveAs( output_name.c_str() );
      delete val_canv;
      //delete active_pad;
      //delete val_legend;
      //delete val_stats;
    }
  );



}