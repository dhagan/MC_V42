#include "generate_hists.hxx"

void minipos( TH1D * data, TH1D * sign, TH1D * bckg, bool trex ){

  int value = (int) trex;

  for (int bin_number = 1; bin_number <= data->GetNbinsX(); bin_number++){

    if ( data->GetBinContent( bin_number ) <= 0){
      data->SetBinContent( bin_number, value );
      data->SetBinError( bin_number, sqrt(value) );
    }
    if ( sign->GetBinContent( bin_number ) <= 0){
      sign->SetBinContent( bin_number, value );
      sign->SetBinError( bin_number, sqrt(value) );
    }
    if (bckg->GetBinContent( bin_number ) <= 0){
      bckg->SetBinContent( bin_number, value );
      bckg->SetBinError( bin_number, sqrt(value) );
    }
    if ((bckg->GetBinContent( bin_number ) <= 0 ) && ( sign->GetBinContent( bin_number ) <= 0 ) ){
      data->SetBinContent( bin_number, value );
      data->SetBinError( bin_number, sqrt(value) );
    }
  }
}


//void generate_hists( basic_fileset * fileset, variable_set & variables, generate_settings settings ){

const std::vector< const char * > region_out_str = { "sr", "cr" };

void generate_hists( YAML::Node & run_node, variable_set & variables ){

  prep_style();
  gROOT->SetBatch( true );

  std::vector<std::string>  mass_bins = { "Q12" }; 
  
  std::string unique = run_node[ "output_unique"].as<std::string>();
  std::string input_unique = run_node["input_unique"].as<std::string>();
  std::string input_filepath = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + input_unique;
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( input_filepath, input_unique, true );
  
  bound & analysis_bound =  variables.analysis_bound;
  bound & spectator_bound = variables.spectator_bound;
  bound & mass_bound = variables.bound_manager->get_bound( "Q12" );
  bound & qtb_bound = variables.bound_manager->get_bound( "qtB" );

  std::vector< std::string > validation_variables;
  if ( run_node[ "validation_variables"] ){ 
    validation_variables = run_node[ "validation_variables" ].as<std::vector<std::string>>();
  }

  std::vector< bound > validation_bounds;
  std::transform( validation_variables.begin(), validation_variables.end(),
    std::back_inserter( validation_bounds ),
    [ &variables ]( const std::string & variable ){
      return variables.bound_manager->get_bound( variable );
    }
  );

  // these _MUST_ be supplied
  std::string pt = run_node["pt_weight"].as<std::string>();
  std::string pu = run_node["pu_weight"].as<std::string>();
  std::string sf = run_node["sf_weight"].as<std::string>();
  region sr_type = region_convert( run_node["sr_type"].as<std::string>() ) ;
  region cr_type = region_convert( run_node["cr_type"].as<std::string>() ) ;
  std::vector<region> regions = { sr_type, cr_type };
  bool zero = run_node[ "zero" ].as<bool>();

  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names();


  std::string data_weight = "1.0"; 
  if ( run_node["data_weight"] ){
    data_weight = run_node["data_weight"].as<std::string>(); 
  }

  std::string forced_region = "1.0";
  if ( run_node["force_region"] ){
    region forced = region_convert( run_node["force_region"].as<std::string>() );
    forced_region = region_cuts[ forced ];
  }

  char mass_qtb[400]; 
  std::string extra_cut = "";
  if ( !variables.extra_bounds.empty() ){
    std::for_each( variables.extra_bounds.begin(), variables.extra_bounds.end()-1, 
      [&extra_cut]( bound & extra ){
        extra_cut += "(" + extra.get_cut() + ")&&";
      }
    );
    extra_cut += "(" + variables.extra_bounds.back().get_cut() + ")";
    sprintf( mass_qtb, "(%s&&%s&&%s&&%s)", mass_bound.get_cut().c_str(), qtb_bound.get_cut().c_str(), extra_cut.c_str(), forced_region.c_str() );
  } else {
    sprintf( mass_qtb, "(%s&&%s&&%s)", mass_bound.get_cut().c_str(), qtb_bound.get_cut().c_str(), forced_region.c_str() );
  }
 
  char data_draw[100], sign_draw[100], bckg_draw[100];
  sprintf( data_draw, "%s>>data", spectator_bound.get_var().c_str() );
  sprintf( sign_draw, "%s>>sign", spectator_bound.get_var().c_str() );
  sprintf( bckg_draw, "%s>>bckg", spectator_bound.get_var().c_str() );

  const char * pt_weight;
  if ( !pt.empty() ){
    // this should be jpsi_weight or phot_weight
    pt_weight = pt.c_str();
  } else {
    pt_weight = "1.0";
  }

  char pu_weight[50];
  if ( pu.empty() ){
    sprintf( pu_weight, "pu_weight" );
  } else {
    sprintf( pu_weight, "%s", pu.c_str() );  
  }

  const char * scalefactor_weight;
  if ( sf.empty() ){
    scalefactor_weight = "photon_sf*muon_sf";
  } else {
    // this should be varied with photon_sf_upper*muon_sf, or photon_sf_lower*muon_sf
    // muon variations should be handled by the scalefactor program
    scalefactor_weight = sf.c_str();
  }

  char weight_expression[200];
  sprintf( weight_expression, "%s*%s*%s", scalefactor_weight, pu_weight, pt_weight );
  std::cout << "Weighting: " << weight_expression << std::endl;

  std::cout << "Regions: " << std::endl;
  for ( size_t region_idx = 0; region_idx < regions.size(); region_idx++ ){
      region generation_region = regions[ region_idx ];
      std::cout << "Region:     " << region_c[ generation_region ] << std::endl;
      std::cout << "Region cut: " << region_cuts[ generation_region ] << std::endl;
  }
  std::cout << std::endl;

  for ( size_t cut_no = 0; cut_no < ana_cuts.size(); cut_no++ ){

    const char * ana_cut = ana_cuts.at(cut_no).c_str();
    const char * ana_cut_name = ana_cut_names.at(cut_no).c_str();
    std::cout << "qT region:  " << ana_cut_name << std::endl;
    std::cout << "qT cut:     " << ana_cut << std::endl; 


    char sign_filename[100], bckg_filename[100], data_filename[100];
    sprintf( data_filename, "./hists/data_%s_%s.root", unique.c_str(), ana_cut_name );
    sprintf( sign_filename, "./hists/sign_%s_%s.root", unique.c_str(), ana_cut_name );
    sprintf( bckg_filename, "./hists/bckg_%s_%s.root", unique.c_str(), ana_cut_name );

    TFile * sign_file = new TFile( sign_filename, "RECREATE" );
    TFile * bckg_file = new TFile( bckg_filename, "RECREATE" );
    TFile * data_file = new TFile( data_filename, "RECREATE" );

    for ( size_t region_idx = 0; region_idx < regions.size(); region_idx++ ){

      region generation_region = regions[ region_idx ];

      TH1D * data = new TH1D( "data", "", 50, -1.0, 1.0 ); //spectator_bound.get_hist( "data", 50 );
      TH1D * sign = new TH1D( "sign", "", 50, -1.0, 1.0 ); //spectator_bound.get_hist( "sign", 50 );
      TH1D * bckg = new TH1D( "bckg", "", 50, -1.0, 1.0 ); //spectator_bound.get_hist( "bckg", 50 );
      data->Sumw2( kTRUE );
      sign->Sumw2( kTRUE );
      bckg->Sumw2( kTRUE );

      // double check this!!!
      char data_expression[600], sibg_expression[600];
      sprintf( data_expression, "(subtraction_weight*%s)*(%s&&%s&&%s)", data_weight.c_str(), mass_qtb, ana_cut, region_cuts[ generation_region ]   );
      sprintf( sibg_expression, "(subtraction_weight*%s)*(%s&&%s&&%s)", weight_expression, mass_qtb, ana_cut, region_cuts[ generation_region ]   );
      std::cout << "data expression:   " << data_expression << std::endl;
      std::cout << "sibg expression:   " << sibg_expression << std::endl;
      std::cout << std::endl;

      fileset->data_tree->Draw( data_draw, data_expression,    "e goff" );
      fileset->sign_tree->Draw( sign_draw, sibg_expression, "e goff" );
      fileset->bckg_tree->Draw( bckg_draw, sibg_expression, "e goff" );
      
      if ( zero ){ minipos( data, sign, bckg, false ); }

      sign_file->cd();
      sign->Write( region_out_str[ region_idx ] );
      bckg_file->cd();
      bckg->Write( region_out_str[ region_idx ] );
      data_file->cd();
      data->Write( region_out_str[ region_idx ] );
      delete data;
      delete sign;
      delete bckg;
          
    }
    
      
    for ( size_t val_idx=0; val_idx < validation_bounds.size(); val_idx++ ){

      bound & val_bound = validation_bounds.at( val_idx );

      char val_data_draw[100], val_sign_draw[100], val_bckg_draw[100];
      sprintf( val_data_draw, "%s>>val_data", val_bound.get_var().c_str() );
      sprintf( val_sign_draw, "%s>>val_sign", val_bound.get_var().c_str() );
      sprintf( val_bckg_draw, "%s>>val_bckg", val_bound.get_var().c_str() );

      TH1D * val_data = new TH1D( "val_data", "", 50, val_bound.get_min(), val_bound.get_max() ); //spectator_bound.get_hist( "data", 50 );
      TH1D * val_sign = new TH1D( "val_sign", "", 50, val_bound.get_min(), val_bound.get_max() ); //spectator_bound.get_hist( "sign", 50 );
      TH1D * val_bckg = new TH1D( "val_bckg", "", 50, val_bound.get_min(), val_bound.get_max() ); //spectator_bound.get_hist( "bckg", 50 );
      val_data->Sumw2( kTRUE );
      val_sign->Sumw2( kTRUE );
      val_bckg->Sumw2( kTRUE );

      char val_data_expression[600], val_sibg_expression[600];
      sprintf( val_data_expression, "(subtraction_weight)*(%s&&%s)", mass_qtb, ana_cut );
      sprintf( val_sibg_expression, "(subtraction_weight*%s)*(%s&&%s)", weight_expression, mass_qtb, ana_cut );
      std::cout << "val_data expression:   " << val_data_expression << std::endl;
      std::cout << "val_sibg expression:   " << val_sibg_expression << std::endl;
      std::cout << std::endl;

      fileset->data_tree->Draw( val_data_draw, val_data_expression, "e goff" );
      fileset->sign_tree->Draw( val_sign_draw, val_sibg_expression, "e goff" );
      fileset->bckg_tree->Draw( val_bckg_draw, val_sibg_expression, "e goff" );
      
      if ( zero ){ minipos( val_data, val_sign, val_bckg, false ); }
      std::string write_name = "vr" + std::to_string( val_idx+1 );
      sign_file->cd();
      val_sign->Write( write_name.c_str() );
      bckg_file->cd();
      val_bckg->Write( write_name.c_str() );
      data_file->cd();
      val_data->Write( write_name.c_str() );
      delete val_data;
      delete val_sign;
      delete val_bckg;
          
    }
  
    sign_file->Close();
    delete sign_file;
    bckg_file->Close();
    delete bckg_file;
    data_file->Close();
    delete data_file;

  }

}
