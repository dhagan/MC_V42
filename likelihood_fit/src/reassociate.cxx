#include <reassociate.hxx>

//// midpoints shifted up 1 for the 0 set;
//int floor_search( const ROOT::RVecF & value, std::vector<double> & edges, int lower, int upper ){
//
//  if ( lower > upper ){ return 0; }
//
//  if( edges.back() < value[0] ){ return 0; }
//  if( edges.front() > value[0] ){ return 0; } 
//  int midpoint = ( lower + upper ) / 2;
//  if( edges.at( midpoint ) == value[0] ){ return midpoint + 1; } 
//  if ( edges.at( midpoint - 1 ) <= value[0] ){
//    if ( ( value[0] < edges.at( midpoint ) ) ){ 
//      return midpoint;
//    } 
//  } 
//  if( value[0] < edges.at( midpoint ) ){ return floor_search( value, edges, lower, midpoint-1 ); } 
//  return floor_search( value, edges, midpoint+1, upper );
//}


int floor_search( const ROOT::RVecF & value, std::vector<double> & edges, int lower, int upper ){

  if ( lower > upper ){ return 0; }
  if( edges.back() < value[0] ){ return 0; }
  if( edges.front() > value[0] ){ return 0; } 
  int midpoint = ( lower + upper ) / 2;
  if( edges.at( midpoint ) == value[0] ){ return midpoint + 1; } 
  if ( edges.at( midpoint - 1 ) <= value[0] ){
    if ( ( value[0] < edges.at( midpoint ) ) ){ 
      return midpoint;
    } 
  } 
  if( value[0] < edges.at( midpoint ) ){ return floor_search( value, edges, lower, midpoint ); } 
  return floor_search( value, edges, midpoint+1, upper );
}

int bfs( const ROOT::RVecF & value, std::vector<double> & edges, int lower, int upper ){
  if ( lower > upper ){ return -1; }
  if( edges.back() < value[0] || edges.front() > value[0] ){ return -1; }
  int midpoint = ( lower + upper ) / 2;
  if ( edges.at( midpoint ) <= value[0] && value[0] < edges.at( midpoint + 1 ) ){
    return midpoint;
  } 
  if( value[0] < edges.at( midpoint ) ){ 
    return bfs( value, edges, lower, midpoint );
  } 
  else {
    return bfs( value, edges, midpoint, upper );
  }
}


void reassociate( YAML::Node run_node, variable_set & variables, basic_fileset * input_fileset ){

  std::cout << run_node.IsDefined() << std::endl;
  std::cout << variables.analysis_variable << std::endl;
  std::cout << input_fileset->get_unique() << std::endl;

  /*
    * parse the binnings out from the plot yield files
    * create a lambda and a binary search to index data
    * use the floor search and floor search double on this
    * need to find a maximal row length, there's always gonna be 15 columns
  
  */

  bound & analysis_bound = variables.analysis_bound;
  //bound & spectator_bound = variables.spectator_bound;

  std::vector< std::string > ana_cut_series = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names  = analysis_bound.get_series_names();
  
  std::string unique = run_node[ "output_unique" ].as<std::string>();
  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string input_hists = trex_path + "hists/";



  //auto[ baseline_total, baseline_extracted ] = get_result( unique, variables );


  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_cut_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    std::string trex_input_hist_path = trex_qta_path + "Histograms/" + unique + "_histos.root";
    std::string input_sign_hist_path = input_hists + "sign_" + unique + "_" + qta_name + ".root";
    std::string input_bckg_hist_path = input_hists + "bckg_" + unique + "_" + qta_name + ".root";
    std::string input_data_hist_path = input_hists + "data_" + unique + "_" + qta_name + ".root";
    std::string plots_path = trex_qta_path + "Plots/";
    std::string table_path = trex_qta_path + "Tables/";




    // individual bin yields
    //YAML::Node sr_prefit  = YAML::LoadFile( plots_path + "SIGNAL_prefit.yaml" ); 
    //YAML::Node cr_prefit  = YAML::LoadFile( plots_path + "CONTROL_prefit.yaml" ); 
    //YAML::Node sr_postfit = YAML::LoadFile( plots_path + "SIGNAL_postFit.yaml" ); 
    //YAML::Node cr_postfit = YAML::LoadFile( plots_path + "CONTROL_postFit.yaml" ); 

    // Get the overall yield 
    //YAML::Node table_postfit_result = YAML::LoadFile( table_path + "Table_postfit.yaml" );
    //std::cout << table_path + "Table_postfit.yaml"  << std::endl;

    // pull out the histograms for operating with later when calcuating the yields per ana-spec bin
    //TFile * trex_input_file = new TFile( trex_input_hist_path.c_str(), "READ" );
    //TH1F * input_trex_sign_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/sign/nominal/SIGNAL_sign" ) );
    //TH1F * input_trex_bckg_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/bckg/nominal/SIGNAL_bckg" ) );
    //TH1F * input_trex_data_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/data/nominal/SIGNAL_data" ) );
    //TH1F * input_trex_sign_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/sign/nominal/CONTROL_sign" ) );
    //TH1F * input_trex_bckg_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/bckg/nominal/CONTROL_bckg" ) );
    //TH1F * input_trex_data_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/data/nominal/CONTROL_data" ) );









  // this one is fine
  //int analysis_lower = 0; int analysis_upper = analysis_bound.get_bins();
  //auto analysis_floor_search_lambda = [ &analysis_edges, &analysis_lower, &analysis_upper, &analysis_bound]( const ROOT::RVecF & analysis_variable ){
  //  analysis_lower = 0; analysis_upper = analysis_bound.get_bins();
  //  return int( floor_search( analysis_variable, analysis_edges, analysis_lower, analysis_upper ) );
  //};

  // this needs updated to operate with flexible binning
  //int spectator_lower = 0; int spectator_upper = spectator_bound.get_bins();
  //auto spectator_floor_search_lambda = [ &spectator_edges, &spectator_lower, &spectator_upper, &spectator_bound]( const ROOT::RVecD & spectator_variable ){
  //  spectator_lower = 0; spectator_upper = spectator_bound.get_bins();
  //  return int( floor_search_double( spectator_variable, spectator_edges, spectator_lower, spectator_upper ) );
  //}; 
  





  
  //  
  //std::vector< std::vector< float > > all_sign_weights( analysis_bound.get_bins() + 2 );
  //std::vector< std::vector< float > > all_bckg_weights( analysis_bound.get_bins() + 2 );

  //std::vector< float > sign_weight( spectator_bound.get_bins() + 2 );
  //std::vector< float > bckg_weight( spectator_bound.get_bins() + 2 );
  //sign_weight.at( 0 ) = 0;
  //sign_weight.at( spectator_bound.get_bins() + 1 ) = 0;
  //bckg_weight.at( 0 ) = 0;
  //bckg_weight.at( spectator_bound.get_bins() + 1 ) = 0;

  //for ( int bin = 1; bin <= spectator_bound.get_bins(); bin++ ){

  //  // you'll have to do some funny scaling here, gonna have to do a yield rescaling, but that doesn't assume that things are linear
  //  float sign_bin_weight = sign_scaled->GetBinContent( bin )/fit->GetBinContent( bin );
  //  float bckg_bin_weight = bckg_scaled->GetBinContent( bin )/fit->GetBinContent( bin );
  //  if (sign_scaled->GetBinContent( bin ) == 0 || fit->GetBinContent( bin ) == 0 ){ sign_bin_weight = 0; }
  //  if (bckg_scaled->GetBinContent( bin ) == 0 || fit->GetBinContent( bin ) == 0 ){ bckg_bin_weight = 0; }
  //  sign_weight_hist->SetBinContent( bin, sign_bin_weight );
  //  bckg_weight_hist->SetBinContent( bin, bckg_bin_weight );
  //  sign_weight.at( bin ) = sign_bin_weight;
  //  bckg_weight.at( bin ) = bckg_bin_weight;


  //}

  //all_sign_weights.at( cut_no + 1 ) = sign_weight;
  //all_bckg_weights.at( cut_no + 1 ) = bckg_weight;





  //auto sign_weight_lambda = [ &all_sign_weights ]( 
  //  const ROOT::RVecI & ana_bin, const ROOT::RVecI & spec_bin ){ return double( all_sign_weights.at( ana_bin[0] ).at( spec_bin[0] ) );};

  //auto bckg_weight_lambda = [ &all_bckg_weights ]( const ROOT::RVecI & ana_bin, const ROOT::RVecI & spec_bin ){ return double( all_bckg_weights.at( ana_bin[0] ).at( spec_bin[0] ) ); };

  //TChain chain( "tree" );
  //chain.Add( files.at(0).c_str() );
  //ROOT::RDataFrame full_frame( chain );
  //auto output_frame = full_frame.Define( "hf_sign_weight", sign_weight_lambda, { "ana_bin", "spec_bin" } );
  //output_frame = output_frame.Define( "hf_bckg_weight", bckg_weight_lambda, { "ana_bin", "spec_bin" } );
  //output_frame.Snapshot(  "tree", Form("./eval/weight_%s.root", unique.c_str() )  );


  }

}