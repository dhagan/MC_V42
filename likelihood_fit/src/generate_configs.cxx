#include "generate_configs.hxx"
#include <string>
#include <iostream>

#include <yaml-cpp/yaml.h>

struct sys_paths{
  char file_up[100];
  char path_up[150];
};

void prep_sys_paths( sys_paths & paths, const char * line, const char * up, const char * unique, const char * bin, const char * out ){
  std::sprintf( paths.file_up,    "\tHistoFileUp: %.4s_%s \n", &( line[4] ), bin );
  std::sprintf( paths.path_up,    "\tHistoPathUp: \"%s/trex/%s/%s/systematic/\" \n", out, unique, up );
}

void output_sys_paths( sys_paths & paths, std::ofstream & output ){
  output << paths.path_up;
  output << paths.file_up;
}


//void generate_configs( basic_fileset * fileset, variable_set & variables, const std::string & config_name, 
//                        bool systematics, bool initials, bool statonly_processing, generate_settings settings ){


void generate_configs( YAML::Node & run_node, variable_set & variables ){

  std::vector< std::string > validation_variables;
  if ( run_node[ "validation_variables"] ){ 
    validation_variables = run_node[ "validation_variables" ].as<std::vector<std::string>>();
  }

  bound & analysis_bound =  variables.analysis_bound;
  std::string unique = run_node["output_unique"].as<std::string>();

  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names();
  std::string var_ltx = analysis_bound.get_ltx();
  
  const char * install_path = std::getenv("ANA_IP");
  char config_path[100];
  sprintf( config_path, "%s/likelihood_fit/share/%s", install_path, unique.c_str() );

  char config_core_path[150], config_param_path[150], config_sample_path[150], config_systematic_path[200];
  sprintf( config_core_path,        "%s/core.txt",        config_path );
  sprintf( config_param_path,       "%s/params.txt",      config_path );
  sprintf( config_sample_path,      "%s/samples.txt",     config_path );
  sprintf( config_systematic_path,  "%s/systematics.txt", config_path );

  std::ifstream core( config_core_path ); 
  std::ifstream param( config_param_path ); 
  std::ifstream sample( config_sample_path ); 
  std::ifstream systematic( config_systematic_path ); 
 
  const int line_size = 200;
  char line[line_size], hf[50], nominal_val[30];

  double mu_value = 0.2, spp_value = 4.5, data_value = 1.0;

  std::vector< double > initial_mu, initial_spp;
  char param_initial_path[250];
  sprintf( param_initial_path, "%s/initials.yaml", config_path );
  YAML::Node initial_values = YAML::LoadFile( param_initial_path );
  for ( int bin = 1; bin <= analysis_bound.get_bins(); bin++ ){
    initial_mu.push_back( initial_values[ std::to_string( bin ) ]["mu"].as<double>() );
    initial_spp.push_back( initial_values[ std::to_string( bin ) ]["spp"].as<double>() );
  } 

  std::vector< bound > validation_bounds;
  std::vector< std::string > validation_binnings;
  char bins_path[250];
  sprintf( bins_path, "%s/bins.yaml", config_path );
  YAML::Node binnings = YAML::LoadFile( bins_path );
  std::for_each( validation_variables.begin(), validation_variables.end(),
    [ &variables, &validation_bounds, &validation_binnings, &binnings ]( const std::string & variable ){
      validation_bounds.push_back( variables.bound_manager->get_bound( variable ) );
      validation_binnings.push_back( binnings[variable].as<std::string>() );
    }  
  );
  
  const char * out_path = std::getenv( "OUT_PATH" );
  char hist_directory[150];
  sprintf( hist_directory, "\tHistoPath: \"%s/trex/%s/hists/\" \n", out_path, unique.c_str() );

  sys_paths systematic_paths;

  for ( size_t cut_no = 0; cut_no < ana_cuts.size(); cut_no++ ){

    mu_value = initial_mu[ cut_no ];
    spp_value = initial_spp[ cut_no ];

    const char * ana_cut_name = ana_cut_names[cut_no].c_str();
    char output_config[64];
    sprintf( output_config, "./configs/%s.config", ana_cut_name );
    std::ofstream config( output_config ); 

    config << "Job: \"" << unique << "\"\n";
    config << "\tLabel : \"" << var_ltx << " - " << cut_no+1 << "\"\n";

    while ( core.getline( line, line_size ) ){ config << line << "\n"; }
    config << "\n";

    for ( size_t val_idx = 0; val_idx < validation_bounds.size(); val_idx++ ){
      std::string val_no = std::to_string(val_idx + 1);
      config << "Region: \"VALIDATION" << val_no << "\"\n";
      config << "\tType: VALIDATION\n";
      config << "\tHistoName: \"vr" << val_no << "\"\n";
      config << "\tLabel: \"Validation Region " << val_no << "\"\n";
      config << "\tShortLabel: \"val" << val_no << "\"\n";
      config << "\tVariableTitle: \"" << validation_bounds.at( val_idx ).get_ltx() << "\"\n";
      config << "\tBinning: " << validation_binnings.at( val_idx ) << "\n";
      config << "\n";
    } 

    core.clear();
    core.seekg(0); 

    while ( sample.getline( line, line_size ) ){ 
      config << line << "\n";
      const char * samp = strstr( line, "Sample:" );
      if ( std::string( line ).find( "NormToSample" ) != std::string::npos ){ continue;}
      if ( samp != NULL ){
        config << hist_directory;
        std::sprintf( hf, "\tHistoFile: %.4s_%s_%s \n", &( line[9] ), unique.c_str(), ana_cut_name );
        config << hf;
      }
    }
    config << "\n";
    sample.clear();
    sample.seekg(0); 

    while ( param.getline( line, line_size ) ){ 
      const char * nominal = strstr( line, "Nominal:" );
      if ( nominal != NULL ){
        if ( strstr( line, "MU") != NULL ){
          std::sprintf( nominal_val, "\tNominal: %.5f\n", mu_value ); 
        } else if ( strstr( line, "PP") != NULL ){
          std::sprintf( nominal_val, "\tNominal: %.5f\n", spp_value ); 
        } else if ( strstr( line, "DATA") != NULL ){
          std::sprintf( nominal_val,"\tNominal: %.5f\n", data_value ); 
        }
        config << nominal_val;
      } else { config << line << "\n"; }
    }
    config << "\n";
    param.clear();
    param.seekg(0);

    char current_systematic[400] = "";
    bool sys_mode = false;
    while ( systematic.getline( line, line_size ) ){ 
      if ( strstr( line, "%%Systematic:" ) != NULL ){
        sprintf( current_systematic, "%s\n", &( line[14] ) );
        current_systematic[ strlen(current_systematic) - 1 ] = '\0';
        systematic.getline( line, line_size );
        sys_mode = false;
        config << line << "\n";
      } else if ( strstr( line, "Systematic:" ) != NULL ){
        sprintf( current_systematic, "%s\n", &( line[12] ) );
        current_systematic[ strlen(current_systematic) - 1 ] = '\0';
        sys_mode = true;
        config << line << "\n";
      } else if ( sys_mode ){
        if ( strstr( current_systematic, "photon_sf" ) ){
          prep_sys_paths( systematic_paths, line, "photon_sf", unique.c_str(), ana_cut_name, out_path );
        } else if ( strstr( current_systematic, "msf" ) ){
          prep_sys_paths( systematic_paths, line, "musf", unique.c_str(), ana_cut_name, out_path );
        } else if ( strstr( current_systematic, "jpsi_weight" ) ){
          prep_sys_paths( systematic_paths, line, "jpsi_weight", unique.c_str(), ana_cut_name, out_path );
        } else if ( strstr( current_systematic, "phot_weight" ) ){
          prep_sys_paths( systematic_paths, line, "phot_weight", unique.c_str(), ana_cut_name, out_path );
        } else if ( strstr( current_systematic, "dphi_weight" ) ){
          prep_sys_paths( systematic_paths, line, "dphi_weight", unique.c_str(), ana_cut_name, out_path );
        } else if ( strstr( current_systematic, "absdPhi_weight" ) ){
          prep_sys_paths( systematic_paths, line, "absdPhi_weight", unique.c_str(), ana_cut_name, out_path );
        } else if ( strstr( current_systematic, "dy_weight" ) ){
          prep_sys_paths( systematic_paths, line, "dy_weight", unique.c_str(), ana_cut_name, out_path );
        } else if ( strstr( current_systematic, "mr" ) ){
          sys_mode = false;
          config << line << "\n";
          continue;
        } 
        output_sys_paths( systematic_paths, config ); 
        sys_mode = false;
      } else { 
        config << line << "\n";
      }
    }
    config << "\n";
    systematic.clear();
    systematic.seekg(0); 
     

    config.close();

  }
  
}
