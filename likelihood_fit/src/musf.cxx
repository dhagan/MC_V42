#include <prefit.hxx>
#include <sys/stat.h>

void prepare_histograms( bound & spec_bound, const std::string & analysis_bin, std::vector< TH1F> & hists, int variations = 100 ){
  for ( int hist_index = 0; hist_index < variations; hist_index++ ){
      std::string hist_name = analysis_bin + "_v" + std::to_string( hist_index );
      hists.push_back( TH1F( hist_name.c_str(), hist_name.c_str(), 50, spec_bound.get_min(), spec_bound.get_max() ) );
      hists.back().Sumw2( kTRUE );
  }
}

void musf( YAML::Node & run_node, variable_set & variables, sample_type type, region region ){

  prep_style();

  int variations = ( run_node["variations"] ) ? run_node["variations"].as<int>() : 100;

  std::string unique = run_node[ "output_unique"].as<std::string>();
  std::string input_unique = run_node["input_unique"].as<std::string>();
  std::string input_filepath = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + input_unique;
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( input_filepath, input_unique, true );
  
  TTree * sf_tree = fileset->get_tree( type );
  bound & ana_bound = variables.analysis_bound;
  bound & spec_bound = variables.spectator_bound;
  bound & mass_bound = variables.bound_manager->get_bound( "Q12" );
  bound & qtb_bound = variables.bound_manager->get_bound( "qtB" );

  std::vector< std::string > analysis_cut_series = ana_bound.get_cut_series();
  std::vector< std::string > analysis_cut_names = ana_bound.get_series_names();
  char mass_qtb[200]; sprintf( mass_qtb, "(%s&&%s)", mass_bound.get_cut().c_str(), qtb_bound.get_cut().c_str() );


  std::string nominal_pt_weight = "1.0";
  std::string nominal_pu_weight = "pu_weight";
  std::string nominal_photon_sf_weight = "photon_sf_l20_c20"; 
  if ( run_node["nominal_pt_weight"] ){         nominal_pt_weight         = run_node["nominal_pt_weight"].as<std::string>(); }
  if ( run_node["nominal_pu_weight"] ){         nominal_pu_weight         = run_node["nominal_pu_weight"].as<std::string>(); }
  if ( run_node["nominal_photon_sf_weight"] ){  nominal_photon_sf_weight  = run_node["nominal_photon_sf_weight"].as<std::string>(); }
  char nominal_weight[75]; 
  sprintf( nominal_weight, "%s*%s*%s", nominal_photon_sf_weight.c_str(), nominal_pu_weight.c_str(), nominal_pt_weight.c_str() );

  std::string pt_weight         = run_node["pt_weight"].as<std::string>();
  std::string pu_weight         = run_node["pu_weight"].as<std::string>();
  std::string photon_sf_weight  = run_node["photon_sf_weight"].as<std::string>();
  char variation_weight[75]; 
  sprintf( variation_weight, "%s*%s*%s", photon_sf_weight.c_str(), pu_weight.c_str(), pt_weight.c_str() );

  std::cout << "Nominal weight: " << nominal_weight << std::endl;
  std::cout << "Variation weight: " << nominal_weight << std::endl;

  //for ( int analysis_bin = 0; analysis_bin < ana_bound.get_bins(); analysis_bin++ ){
  for ( int analysis_bin = 0; analysis_bin < ana_bound.get_bins(); analysis_bin++ ){

    // the nominal
    char nominal_draw_expr[50]; sprintf( nominal_draw_expr, "BDT>>%s", analysis_cut_names[analysis_bin].c_str() ); 
    char nominal_cut_expr[400]; sprintf( nominal_cut_expr, "(subtraction_weight*muon_sf*%s)*(%s&&%s&&%s)", 
      nominal_weight, mass_qtb, analysis_cut_series[analysis_bin].c_str(), region_cuts[region] );
    TH1F nominal_hist = TH1F( analysis_cut_names[analysis_bin].c_str(), "", 50, spec_bound.get_min(), spec_bound.get_max() );
    sf_tree->Draw( nominal_draw_expr, nominal_cut_expr, "goff e" );

    // the systematic
    char sys_draw_expr[50]; sprintf( sys_draw_expr, "BDT>>%s_sys", analysis_cut_names[analysis_bin].c_str() ); 
    char sys_cut_expr[400]; sprintf( sys_cut_expr, "(subtraction_weight*%s*(muon_sf*(1+muon_sf_systematic)))*(%s&&%s&&%s)",
      variation_weight, mass_qtb, analysis_cut_series[analysis_bin].c_str(), region_cuts[region]);
    char sys_name[50]; sprintf( sys_name, "%s_sys", analysis_cut_names[analysis_bin].c_str() ); 
    TH1F sys_upper_sf = TH1F( sys_name, "", 50, spec_bound.get_min(), spec_bound.get_max() );
    TH1F sys_err_sf = TH1F( "sys_err", "", 50, spec_bound.get_min(), spec_bound.get_max() );
    for ( int spec_bin_idx = 1; spec_bin_idx <= 50; spec_bin_idx++ ){
      sys_err_sf.SetBinContent( spec_bin_idx, nominal_hist.GetBinContent( spec_bin_idx ) );
      sys_err_sf.SetBinError( spec_bin_idx, sys_upper_sf.GetBinContent( spec_bin_idx ) - sys_upper_sf.GetBinContent( spec_bin_idx ) );
    }


    std::string variations_name = "./musf/stat_variations/" + analysis_cut_names[analysis_bin] + "_" + region_c[region] + "_" + type_c[type] + ".root";
    TFile * output_var_file = new TFile( variations_name.c_str(), "RECREATE" );

    // make variations, default 100
    std::vector< TH1F > output_histograms;
    output_histograms.reserve( variations );
    prepare_histograms( spec_bound, analysis_cut_names.at( analysis_bin ), output_histograms, variations );

    for ( int hist = 0; hist < variations; hist++ ){
      char var_draw_expr[50]; sprintf( var_draw_expr, "BDT>>%s_v%i", analysis_cut_names[analysis_bin].c_str(), hist ); 
      char var_cut_expr[400]; sprintf( var_cut_expr, "(subtraction_weight*%s*muon_sf_samples[%i])*(%s&&%s&&%s)", variation_weight, hist, mass_qtb, analysis_cut_series[analysis_bin].c_str(), region_cuts[region] );
      std::cout << var_draw_expr << ": " << std::endl;
      std::cout << var_cut_expr << std::endl;
      sf_tree->Draw( var_draw_expr, var_cut_expr, "goff e" );
    }

    output_var_file->cd();
    for ( TH1F & hist : output_histograms ){ hist.Write(); }; 
    output_var_file->Close();
 

    // compile 100 variations 
    char err_name[50]; sprintf( err_name, "%s_error", analysis_cut_names[analysis_bin].c_str() ); 
    TH1F compiled_sf = TH1F( err_name, "", 50, spec_bound.get_min(), spec_bound.get_max() );
    for ( int spec_bin_idx = 1; spec_bin_idx <= 50; spec_bin_idx++ ){
      TH1F spectator_bin = TH1F( std::to_string( spec_bin_idx ).c_str(), "", 100, -10000, 10000 );
      for ( int hist = 0; hist < variations; hist++ ){
        spectator_bin.Fill( output_histograms.at( hist ).GetBinContent( spec_bin_idx ) );
      }
      compiled_sf.SetBinContent( spec_bin_idx, spectator_bin.GetMean() );
      compiled_sf.SetBinError( spec_bin_idx, spectator_bin.GetStdDev() );
    }


    for ( int spec_bin_idx = 1; spec_bin_idx <= 50; spec_bin_idx++ ){
      nominal_hist.SetBinError( spec_bin_idx, 0.0 );
    }

    // compile the total error
    TH1F total_err_sf = TH1F( "total_err", "", 50, spec_bound.get_min(), spec_bound.get_max() );
    for ( int spec_bin_idx = 1; spec_bin_idx <= 50; spec_bin_idx++ ){

      double total_bin_err = nominal_hist.GetBinContent( spec_bin_idx ) * std::sqrt(
          ( compiled_sf.GetBinError(spec_bin_idx)*compiled_sf.GetBinError(spec_bin_idx) )/( nominal_hist.GetBinContent( spec_bin_idx )*nominal_hist.GetBinContent( spec_bin_idx ) )
        + ( sys_err_sf.GetBinError(spec_bin_idx)*sys_err_sf.GetBinError(spec_bin_idx) )/( nominal_hist.GetBinContent( spec_bin_idx )*nominal_hist.GetBinContent( spec_bin_idx ) )
      );
      total_err_sf.SetBinContent(  spec_bin_idx, nominal_hist.GetBinContent( spec_bin_idx ) );
      total_err_sf.SetBinError(  spec_bin_idx, total_bin_err );
    }


    TCanvas * sf_canv = new TCanvas( "sf_canv", "", 200, 200, 2000, 2000 );
    sf_canv->Divide( 2, 2 );

    TPad * active_pad = (TPad*) sf_canv->cd( 1 );
    compiled_sf.Draw( "HIST E1" );
    hist_prep_axes( &compiled_sf, true );
    set_axis_labels( &compiled_sf, "BDT", "Yield" );
    add_pad_title( active_pad, "Variations SF", true );
    add_atlas_decorations( active_pad, true, false );

    active_pad = (TPad*) sf_canv->cd( 2 );
    nominal_hist.Draw( "HIST E1" );
    hist_prep_axes( &nominal_hist, true );
    set_axis_labels( &nominal_hist, "BDT", "Yield" );
    add_pad_title( active_pad, "Regular BDT", true );
    add_atlas_decorations( active_pad, true, false );

    active_pad = (TPad*) sf_canv->cd( 3 );
    TH1F diff_hist = TH1F( "diff", "", 50, spec_bound.get_min(), spec_bound.get_max() );
    diff_hist.Add( &compiled_sf, &nominal_hist, 1.0, -1.0 );
    diff_hist.Draw( "HIST E1" );
    hist_prep_axes( &diff_hist, true );
    set_axis_labels( &diff_hist, "BDT", "Yield" );
    add_pad_title( active_pad, "Difference BDT", true );
    add_atlas_decorations( active_pad, true, false );

    active_pad = (TPad*) sf_canv->cd( 4 );
    TH1F ratio_hist = TH1F( "ratio", "", 50, spec_bound.get_min(), spec_bound.get_max() );
    ratio_hist.Divide( &compiled_sf, &nominal_hist, 1.0, 1.0 );
    ratio_hist.Draw( "HIST E1" );
    hist_prep_axes( &ratio_hist, true );
    set_axis_labels( &ratio_hist, "BDT", "Yield" );
    add_pad_title( active_pad, "Ratio BDT", true );
    add_atlas_decorations( active_pad, true, false );
    ratio_hist.GetYaxis()->SetRangeUser( 0.9, 1.1 );

    std::string output_file = "./musf/" + analysis_cut_names[analysis_bin] 
                            + "_" + region_c[region] + "_" + type_c[type] 
                            + "_" + std::to_string(variations) + "_variation.png";
    sf_canv->SaveAs( output_file.c_str() );


    TCanvas * total_sf_canv = new TCanvas( "total_sf", "", 200, 200, 2000, 2000 );
    total_sf_canv->Divide( 2, 2 );
    
    active_pad = (TPad*) total_sf_canv->cd( 1 );
    total_err_sf.Draw( "HIST E1" );
    hist_prep_axes( &total_err_sf, true );
    set_axis_labels( &total_err_sf, "BDT", "Yield" );
    add_pad_title( active_pad, "BDT total_err", true );
    add_atlas_decorations( active_pad, true, false );

    active_pad = (TPad*) total_sf_canv->cd( 2 );
    TH1F rat2 = TH1F( "rat2", "", 50, spec_bound.get_min(), spec_bound.get_max() );
    rat2.Divide( &total_err_sf, &nominal_hist, 1.0, 1.0 );
    rat2.Draw( "HIST E1" );
    hist_prep_axes( &rat2, true );
    set_axis_labels( &rat2, "BDT", "Yield" );
    add_pad_title( active_pad, "BDT rel error", true );
    add_atlas_decorations( active_pad, true, false );
    rat2.GetYaxis()->SetRangeUser( 0.8, 1.2 );


    active_pad = (TPad*) total_sf_canv->cd( 3 );
    sys_err_sf.Draw( "HIST E1" );
    hist_prep_axes( &sys_err_sf, true );
    set_axis_labels( &sys_err_sf, "BDT", "Yield" );
    add_pad_title( active_pad, "BDT sys error", true );
    add_atlas_decorations( active_pad, true, false );


    std::string output_tot_file = "./musf/" + analysis_cut_names[analysis_bin]  + "_" + region_c[region] + "_" + type_c[type] + "_total.png";
    total_sf_canv->SaveAs( output_tot_file.c_str() );

    std::string sys_filename = "./musf/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[region] + "_" + type_c[type] + "_error.root";
    TFile * sys_file = new TFile( sys_filename.c_str(), "RECREATE" );
    sys_file->cd();
    rat2.Write( "rel_error");
    sys_file->Close();


    delete sf_canv;
    delete total_sf_canv;
  }

}

void musf_input( YAML::Node & run_node, variable_set & variables, region sr, region cr ){
  std::cout << "goes?" << std::endl;

  bound & ana_bound = variables.analysis_bound;
  bound & spec_bound = variables.spectator_bound;
  std::vector< std::string > analysis_cut_names = ana_bound.get_series_names();
  std::string unique = run_node[ "output_unique"].as<std::string>();
  
  for ( int analysis_bin = 0; analysis_bin < 15; analysis_bin++ ){

    std::string nominal_sign_filename = "./hists/sign_" + unique + "_" + analysis_cut_names[analysis_bin] + ".root"; 
    std::string nominal_bckg_filename = "./hists/bckg_" + unique + "_" + analysis_cut_names[analysis_bin] + ".root"; 
    TFile * nominal_sign_file = new TFile( nominal_sign_filename.c_str(), "READ" );
    TFile * nominal_bckg_file = new TFile( nominal_bckg_filename.c_str(), "READ" );

    TH1F * sign_sr = (TH1F *) nominal_sign_file->Get( "sr" );
    TH1F * sign_cr = (TH1F *) nominal_sign_file->Get( "cr" );
    TH1F * bckg_sr = (TH1F *) nominal_bckg_file->Get( "sr" );
    TH1F * bckg_cr = (TH1F *) nominal_bckg_file->Get( "cr" );

    TH1F * variation_sign_sr = ( TH1F * ) sign_sr->Clone( "sign_srv" ); 
    TH1F * variation_sign_cr = ( TH1F * ) sign_cr->Clone( "sign_crv" );
    TH1F * variation_bckg_sr = ( TH1F * ) bckg_sr->Clone( "bckg_srv" );
    TH1F * variation_bckg_cr = ( TH1F * ) bckg_cr->Clone( "bckg_crv" );
    variation_sign_sr->Reset(); 
    variation_sign_cr->Reset();
    variation_bckg_sr->Reset();
    variation_bckg_cr->Reset();

    std::string err_sign_sr_filename = "./musf/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[sr] + "_sign_error.root";
    std::string err_sign_cr_filename = "./musf/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[cr] + "_sign_error.root";
    std::string err_bckg_sr_filename = "./musf/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[sr] + "_bckg_error.root";
    std::string err_bckg_cr_filename = "./musf/error/" + analysis_cut_names[analysis_bin] + "_" + region_c[cr] + "_bckg_error.root";
    std::cout << err_sign_sr_filename << std::endl;
    TFile * sign_sr_err_file = new TFile( err_sign_sr_filename.c_str(), "READ" );
    TFile * sign_cr_err_file = new TFile( err_sign_cr_filename.c_str(), "READ" ); 
    TFile * bckg_sr_err_file = new TFile( err_bckg_sr_filename.c_str(), "READ" );
    TFile * bckg_cr_err_file = new TFile( err_bckg_cr_filename.c_str(), "READ" ); 
    sign_sr_err_file->ls();

    TH1F * sign_sr_rel_err = (TH1F *) sign_sr_err_file->Get( "rel_error" ); 
    TH1F * sign_cr_rel_err = (TH1F *) sign_cr_err_file->Get( "rel_error" ); 
    TH1F * bckg_sr_rel_err = (TH1F *) bckg_sr_err_file->Get( "rel_error" ); 
    TH1F * bckg_cr_rel_err = (TH1F *) bckg_cr_err_file->Get( "rel_error" ); 

    // leave error alone?
    // This creates UPPER variations.
    for ( int spec_bin_idx = 1; spec_bin_idx < spec_bound.get_bins(); spec_bin_idx++ ){
      
      variation_sign_sr->SetBinContent( spec_bin_idx, sign_sr->GetBinContent( spec_bin_idx ) * ( 1 + sign_sr_rel_err->GetBinError( spec_bin_idx ) ) );
      variation_sign_sr->SetBinError( spec_bin_idx, sign_sr->GetBinError( spec_bin_idx ) );
      variation_sign_cr->SetBinContent( spec_bin_idx, sign_cr->GetBinContent( spec_bin_idx ) * ( 1 + sign_cr_rel_err->GetBinError( spec_bin_idx ) ) );
      variation_sign_cr->SetBinError( spec_bin_idx, sign_cr->GetBinError( spec_bin_idx ) );
      variation_bckg_sr->SetBinContent( spec_bin_idx, bckg_sr->GetBinContent( spec_bin_idx ) * ( 1 + bckg_sr_rel_err->GetBinError( spec_bin_idx ) ) );
      variation_bckg_sr->SetBinError( spec_bin_idx, bckg_sr->GetBinError( spec_bin_idx ) );
      variation_bckg_cr->SetBinContent( spec_bin_idx, bckg_cr->GetBinContent( spec_bin_idx ) * ( 1 + bckg_cr_rel_err->GetBinError( spec_bin_idx ) ) );
      variation_bckg_cr->SetBinError( spec_bin_idx, bckg_cr->GetBinError( spec_bin_idx ) );

    }

    sign_sr_err_file->Close(); 
    sign_cr_err_file->Close();
    bckg_sr_err_file->Close();
    bckg_cr_err_file->Close();

    std::string output_sign_name = "./musf/systematic/sign_" + analysis_cut_names[analysis_bin] + ".root";
    std::string output_bckg_name = "./musf/systematic/bckg_" + analysis_cut_names[analysis_bin] + ".root";
    TFile * output_sign_file = new TFile( output_sign_name.c_str(), "RECREATE" );
    output_sign_file->cd();
    variation_sign_sr->Write( "sr" );
    variation_sign_cr->Write( "cr" );
    output_sign_file->Close();

    TFile * output_bckg_file = new TFile( output_bckg_name.c_str(), "RECREATE" );
    output_bckg_file->cd();
    variation_bckg_sr->Write( "sr" );
    variation_bckg_cr->Write( "cr" );
    output_bckg_file->Close();
    std::cout << "Finished musf for " << unique << std::endl;

    delete sign_sr; 
    delete sign_cr;
    delete bckg_sr;
    delete bckg_cr;
    //delete sign_sr_rel_err; 
    //delete sign_cr_rel_err;
    //delete bckg_sr_rel_err;
    //delete bckg_cr_rel_err;
    delete variation_sign_sr;  
    delete variation_sign_cr;
    delete variation_bckg_sr;
    delete variation_bckg_cr;

  }

}