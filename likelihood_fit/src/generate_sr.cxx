#include "generate_sr.hxx"

void minipos_sr( TH1F * data, TH1F * sign, TH1F * bckg, bool trex ){

  int value = (int) trex;

  for (int bin_number = 1; bin_number <= data->GetNbinsX(); bin_number++){

    if ( data->GetBinContent( bin_number ) <= 0){
      data->SetBinContent( bin_number, value );
      data->SetBinError( bin_number, sqrt(value) );
    }
    if ( sign->GetBinContent( bin_number ) <= 0){
      sign->SetBinContent( bin_number, value );
      sign->SetBinError( bin_number, sqrt(value) );
    }
    if (bckg->GetBinContent( bin_number ) <= 0){
      bckg->SetBinContent( bin_number, value );
      bckg->SetBinError( bin_number, sqrt(value) );
    }
    if ((bckg->GetBinContent( bin_number ) <= 0 ) && ( sign->GetBinContent( bin_number ) <= 0 ) ){
      data->SetBinContent( bin_number, value );
      data->SetBinError( bin_number, sqrt(value) );
    }
  }
}


void generate_sr( std::string & input, std::string & abin_var, std::string & spec_var, std::string & cuts, 
        std::string & unique, bound_mgr * selections ){

  if ( !unique.empty() ){ std::cout << unique << std::endl; }

  prep_style();
  gROOT->SetBatch(true);

  std::vector<std::string>  mass_bins = { "Q12" }; 
 
  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int spec_bins = spectator_bound.get_bins();
  float spec_min = spectator_bound.get_min();
  float spec_max = spectator_bound.get_max();

  std::vector<float> data_style = { 21, 1,        1, 1, 1, 1.0,      1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> sign_style = { 1,  kRed+1,   1, 1, 1, kRed+1,   1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> bckg_style = { 1,  kBlue+1,  1, 1, 1, kBlue+1,  1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> fit_style  = { 1,  kGreen+1, 1, 1, 2, kGreen+1, 1.0, 2.0, 0, 0, 0 }; 

  std::vector< std::string > files;
  split_strings( files, input, ":" );
  TFile * file_data = new TFile( files.at(0).c_str(),      "READ");
  TFile * file_sign = new TFile( files.at(1).c_str(),      "READ");
  TFile * file_bckg = new TFile( files.at(2).c_str(),      "READ");

  TTree * data_tree = (TTree *) file_data->Get( "tree" );
  TTree * sign_tree = (TTree *) file_sign->Get( "tree" );
  TTree * bckg_tree = (TTree *) file_bckg->Get( "tree" );
  
  std::vector< std::string > extra_cuts;
  split_strings( extra_cuts, cuts, ":" );
  std::vector< bound > extra_cut_bounds;
  if ( !cuts.empty() ){ 
    auto proc_cut = [&selections]( std::string & cut ){ return selections->get_bound( cut ); };
    std::transform( extra_cuts.begin(), extra_cuts.end(), std::back_inserter( extra_cut_bounds ), proc_cut );
  }

  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( analysis_bound.get_bins() );

  std::vector< std::vector< float > > all_sign_weights( analysis_bound.get_bins() + 2 );
  std::vector< std::vector< float > > all_bckg_weights( analysis_bound.get_bins() + 2 );
  std::vector< float > empty_bins( spectator_bound.get_bins() + 2 );
  std::fill( empty_bins.begin(), empty_bins.end(), 0 );
  all_sign_weights.at( 0 ) = empty_bins;
  all_sign_weights.at( analysis_bound.get_bins() + 1 ) = empty_bins;
  all_bckg_weights.at( 0 ) = empty_bins;
  all_bckg_weights.at( analysis_bound.get_bins() + 1 ) = empty_bins;

  const char * sr_cut = "1";
  const char * cr_cut = "0";

  TFile * sign_file = new TFile( "./sign.root", "RECREATE" );
  sign_file->Close();
  delete sign_file;
  TFile * bckg_file = new TFile( "./bckg.root", "RECREATE" );
  bckg_file->Close();
  delete bckg_file;
  TFile * data_file = new TFile( "./data.root", "RECREATE" );
  data_file->Close();
  delete data_file;

  for ( std::string & mass : mass_bins ){
    
    bound mass_bound = selections->get_bound( mass );
    std::string mass_cut = mass_bound.get_cut();

    fit_results fit_stats;
    int ana_bin = 0;
    TTree * stats_tree = new TTree( mass.c_str(), "" );
    stats_tree->Branch( "ana_bin",   &ana_bin );
    stats_tree->Branch( "fit_stats", &fit_stats, 32000, 2 );
    
    for ( size_t cut_no = 0; cut_no < ana_cuts.size(); cut_no++ ){

      std::string & ana_cut = ana_cuts[cut_no];
      std::string & ana_cut_name = ana_cut_names[cut_no];
      for ( bound & extra_bound : extra_cut_bounds ){ ana_cut += "&&" + extra_bound.get_cut(); }

      TH1F * data_sr = new TH1F( Form( "data_sr_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );
      TH1F * sign_sr = new TH1F( Form( "sign_sr_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );
      TH1F * bckg_sr = new TH1F( Form( "bckg_sr_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );

      TH1F * data_cr = new TH1F( Form( "data_cr_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );
      TH1F * sign_cr = new TH1F( Form( "sign_cr_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );
      TH1F * bckg_cr = new TH1F( Form( "bckg_cr_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );

      data_tree->Draw( Form( "%s>>data_sr_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str(), sr_cut ) );
      sign_tree->Draw( Form( "%s>>sign_sr_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str(), sr_cut ) );
      bckg_tree->Draw( Form( "%s>>bckg_sr_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str(), sr_cut ) );

      data_tree->Draw( Form( "%s>>data_cr_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str(), cr_cut ) );
      sign_tree->Draw( Form( "%s>>sign_cr_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str(), cr_cut ) );
      bckg_tree->Draw( Form( "%s>>bckg_cr_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str(), cr_cut ) );

      minipos_sr( data_sr, sign_sr, bckg_sr, false );

      //std::cout << "data_sr, data_cr, sign_sr, sign_cr, bckg_sr, bckg_cr" << std::endl;
      //for ( int bin = 1; bin <= spec_bins; bin++ ){
      //  std::cout  << data_sr->GetBinContent( bin ) << " ";
      //  std::cout  << data_cr->GetBinContent( bin ) << " ";
      //  std::cout  << sign_sr->GetBinContent( bin ) << " ";
      //  std::cout  << sign_cr->GetBinContent( bin ) << " ";
      //  std::cout  << bckg_sr->GetBinContent( bin ) << " ";
      //  std::cout  << bckg_cr->GetBinContent( bin ) << std::endl;
      //}

      TFile * sign_file = new TFile( "./sign.root", "UPDATE" );
      sign_file->cd();
      sign_sr->Write( "sr" );
      sign_cr->Write( "cr" );
      sign_file->Close();
      delete sign_file;

      TFile * bckg_file = new TFile( "./bckg.root", "UPDATE" );
      bckg_file->cd();
      bckg_sr->Write( "sr" );
      bckg_cr->Write( "cr" );
      bckg_file->Close();
      delete bckg_file;

      TFile * data_file = new TFile( "./data.root", "UPDATE" );
      data_file->cd();
      data_sr->Write( "sr" );
      data_cr->Write( "cr" );
      data_file->Close();
      delete data_file;

    }

  }
      
}
