#include <mwe.hxx>


void mwe( int bins ){

  TH1F * data_sr = new TH1F( "data_sr", "", 3, 0, 2 );
  TH1F * sign_sr = new TH1F( "sign_sr", "", 3, 0, 2 );
  TH1F * bckg_sr = new TH1F( "bckg_sr", "", 3, 0, 2 );

  TH1F * data_cr = new TH1F( "data_cr", "", 3, 0, 2 );
  TH1F * sign_cr = new TH1F( "sign_cr", "", 3, 0, 2 );
  TH1F * bckg_cr = new TH1F( "bckg_cr", "", 3, 0, 2 );


  if ( bins == 2 ){
    //51345 65201 22044 3848 9462 13990
    //49284 14    168115 13 7782 1

    data_sr->SetBinContent( 1, 51345 ); 
    data_sr->SetBinContent( 2, 49284 );

    sign_sr->SetBinContent( 1, 22044 ); 
    sign_sr->SetBinContent( 2, 168115 );

    bckg_sr->SetBinContent( 1, 9462 ); 
    bckg_sr->SetBinContent( 2, 7782 );

    data_cr->SetBinContent( 1, 65201 ); 
    data_cr->SetBinContent( 2, 14 );

    sign_cr->SetBinContent( 1, 3848 ); 
    sign_cr->SetBinContent( 2, 13 );

    bckg_cr->SetBinContent( 1, 13990 ); 
    bckg_cr->SetBinContent( 2, 21);

  }
  
  TFile * sign_file = new TFile( "./sign.root", "RECREATE" );
  sign_file->cd();
  sign_sr->Write( "sr" );
  sign_cr->Write( "cr" );
  sign_file->Close();
  delete sign_file;
  delete sign_sr;
  delete sign_cr;


  TFile * bckg_file = new TFile( "./bckg.root", "RECREATE" );
  bckg_file->cd();
  bckg_sr->Write( "sr" );
  bckg_cr->Write( "cr" );
  bckg_file->Close();
  delete bckg_file;
  delete bckg_cr;
  delete bckg_sr;

  TFile * data_file = new TFile( "./data.root", "RECREATE" );
  data_file->cd();
  data_sr->Write( "sr" );
  data_cr->Write( "cr" );
  data_file->Close();
  delete data_file;
  delete data_sr;
  delete data_cr;

}
