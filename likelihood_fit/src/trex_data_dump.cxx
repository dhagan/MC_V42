#include <postprocess.hxx>
#include <TGaxis.h>
#include <yaml-cpp/yaml.h>
#include <iomanip>


double integral( std::vector< double > & vec ){
  return std::accumulate( vec.begin(), vec.end(), 0.0 );
}
double scale_integral( std::vector< double > & vec, double scale ){
  return scale*std::accumulate( vec.begin(), vec.end(), 0.0 );
}

double gamma_scale_integral( std::vector< double > & vec, std::vector<double> gamma, double scale ){
  std::vector<double> gamma_vec;
  std::transform( vec.begin(), vec.end(), gamma.begin(), std::back_inserter( gamma_vec ), std::multiplies<double>() );
  return scale*std::accumulate( gamma_vec.begin(), gamma_vec.end(), 0.0 );
}



inline void write_small( double number ){
  std::cout << std::fixed << std::setprecision( 3 ) << std::setw(15)  << std::setfill( ' ' ) << number;
}

inline void write_big( double number ){
  std::cout << std::fixed << std::setprecision( 3 ) << std::setw(100)  << std::setfill( '-' ) << ' ' << number << std::endl;
}

inline std::vector< double > to_vec( TH1F * hist ){
  std::vector< double > vec;
  vec.reserve( hist->GetNbinsX() );
  for ( int bin = 1; bin <= hist->GetNbinsX(); bin++ ){
    vec.push_back( hist->GetBinContent( bin ) );
  }
  return vec;
}

void trex_data_dump( YAML::Node run_node, variable_set & variables, basic_fileset * fileset, bool do_systematics ){

  std::vector< std::string > ana_cut_series = variables.analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names  = variables.analysis_bound.get_series_names();

  std::string unique = run_node[ "output_unique" ].as<std::string>();
  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string input_hists = trex_path + "hists/";

  TH1F * sign = variables.analysis_bound.get_hist();
  TH1F * bckg = variables.analysis_bound.get_hist();
  TH1F * data = variables.analysis_bound.get_hist();
  TH1F * fit = variables.analysis_bound.get_hist();
  
  TH1F * alpha_photon_sf = variables.analysis_bound.get_hist( "photonsf" );
  TH1F * alpha_muon_sf = variables.analysis_bound.get_hist( "muonsf" );
  TH1F * alpha_mureco = variables.analysis_bound.get_hist( "mureco" );

  int spacing = 15;

  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_cut_names[ ana_idx ];
    std::cout << std::setw(100) << std::setfill('#') << ' ' << std::endl;
    std::cout << std::setw(100) << qta_name << std::endl;
    std::cout << std::setw(100) << std::setfill('#') << ' ' << std::endl;
    std::cout << std::setfill(' ') << std::endl;
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    std::string trex_input_hist_path = trex_qta_path + "Histograms/" + unique + "_histos.root";
    std::string input_sign_hist_path = input_hists + "sign_" + unique + "_" + qta_name + ".root";
    std::string input_bckg_hist_path = input_hists + "bckg_" + unique + "_" + qta_name + ".root";
    std::string input_data_hist_path = input_hists + "data_" + unique + "_" + qta_name + ".root";
    std::string plots_path = trex_qta_path + "Plots/";
    std::string table_path = trex_qta_path + "Tables/";

    YAML::Node sr_prefit  = YAML::LoadFile( plots_path + "SIGNAL_prefit.yaml" ); 
    YAML::Node cr_prefit  = YAML::LoadFile( plots_path + "CONTROL_prefit.yaml" ); 
    YAML::Node sr_postfit = YAML::LoadFile( plots_path + "SIGNAL_postFit.yaml" ); 
    YAML::Node cr_postfit = YAML::LoadFile( plots_path + "CONTROL_postFit.yaml" ); 

    YAML::Node table_postfit_result = YAML::LoadFile( table_path + "Table_postfit.yaml" );
    std::cout << table_path + "Table_postfit.yaml"  << std::endl;


    std::cout << "Is do_systematics? ::: " << do_systematics << std::endl;

    //TFile * input_sign_file = new TFile( input_sign_hist_path.c_str(), "READ" );
    //TFile * input_bckg_file = new TFile( input_sign_hist_path.c_str(), "READ" );
    //TFile * input_data_file = new TFile( input_sign_hist_path.c_str(), "READ" );

    //TH1F * input_sign_sr = (TH1F *) input_sign_file->Get( "sr" );
    //TH1F * input_sign_cr = (TH1F *) input_sign_file->Get( "cr" ); 
    //TH1F * input_bckg_sr = (TH1F *) input_bckg_file->Get( "sr" ); 
    //TH1F * input_bckg_cr = (TH1F *) input_bckg_file->Get( "cr" ); 
    //TH1F * input_data_sr = (TH1F *) input_data_file->Get( "sr" ); 
    //TH1F * input_data_cr = (TH1F *) input_data_file->Get( "cr" ); 

    TFile * trex_input_file = new TFile( trex_input_hist_path.c_str(), "READ" );
    TH1F * input_trex_sign_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/sign/nominal/SIGNAL_sign" ) );
    TH1F * input_trex_bckg_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/bckg/nominal/SIGNAL_bckg" ) );
    TH1F * input_trex_data_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/data/nominal/SIGNAL_data" ) );
    TH1F * input_trex_sign_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/sign/nominal/CONTROL_sign" ) );
    TH1F * input_trex_bckg_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/bckg/nominal/CONTROL_bckg" ) );
    TH1F * input_trex_data_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/data/nominal/CONTROL_data" ) );

    TH1F * input_trex_photon_sf_sign_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/sign/photon_sf/SIGNAL_sign_photon_sf_Up" ) );
    TH1F * input_trex_photon_sf_bckg_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/bckg/photon_sf/SIGNAL_bckg_photon_sf_Up" ) );
    TH1F * input_trex_photon_sf_sign_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/sign/photon_sf/CONTROL_sign_photon_sf_Up" ) );
    TH1F * input_trex_photon_sf_bckg_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/bckg/photon_sf/CONTROL_bckg_photon_sf_Up" ) );

    TH1F * input_trex_photon_sf_shape_sign_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/sign/photon_sf/SIGNAL_sign_photon_sf_Shape_Up" ) );
    TH1F * input_trex_photon_sf_shape_bckg_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/bckg/photon_sf/SIGNAL_bckg_photon_sf_Shape_Up" ) );
    TH1F * input_trex_photon_sf_shape_sign_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/sign/photon_sf/CONTROL_sign_photon_sf_Shape_Up" ) );
    TH1F * input_trex_photon_sf_shape_bckg_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/bckg/photon_sf/CONTROL_bckg_photon_sf_Shape_Up" ) );

    TH1F * input_trex_mureco_sign_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/sign/mureco/SIGNAL_sign_mureco_Up" ) );
    TH1F * input_trex_mureco_bckg_sr = static_cast< TH1F * >( trex_input_file->Get( "SIGNAL/bckg/mureco/SIGNAL_bckg_mureco_Up" ) );
    TH1F * input_trex_mureco_sign_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/sign/mureco/CONTROL_sign_mureco_Up" ) );
    TH1F * input_trex_mureco_bckg_cr = static_cast< TH1F * >( trex_input_file->Get( "CONTROL/bckg/mureco/CONTROL_bckg_mureco_Up" ) ); 

    if ( do_systematics ){

      input_trex_photon_sf_sign_sr->Add( input_trex_photon_sf_sign_sr, input_trex_sign_sr, 1.0, -1.0 ); 
      input_trex_photon_sf_bckg_sr->Add( input_trex_photon_sf_bckg_sr, input_trex_bckg_sr, 1.0, -1.0 );
      input_trex_photon_sf_sign_cr->Add( input_trex_photon_sf_sign_cr, input_trex_sign_cr, 1.0, -1.0 );
      input_trex_photon_sf_bckg_cr->Add( input_trex_photon_sf_bckg_cr, input_trex_bckg_cr, 1.0, -1.0 );
      input_trex_photon_sf_shape_sign_sr->Add( input_trex_photon_sf_shape_sign_sr, input_trex_sign_sr, 1.0, -1.0 ); 
      input_trex_photon_sf_shape_bckg_sr->Add( input_trex_photon_sf_shape_bckg_sr, input_trex_bckg_sr, 1.0, -1.0 );
      input_trex_photon_sf_shape_sign_cr->Add( input_trex_photon_sf_shape_sign_cr, input_trex_sign_cr, 1.0, -1.0 );
      input_trex_photon_sf_shape_bckg_cr->Add( input_trex_photon_sf_shape_bckg_cr, input_trex_bckg_cr, 1.0, -1.0 );
      input_trex_mureco_sign_sr->Add( input_trex_mureco_sign_sr, input_trex_sign_sr, 1.0, -1.0 ); 
      input_trex_mureco_bckg_sr->Add( input_trex_mureco_bckg_sr, input_trex_bckg_sr, 1.0, -1.0 );
      input_trex_mureco_sign_cr->Add( input_trex_mureco_sign_cr, input_trex_sign_cr, 1.0, -1.0 );
      input_trex_mureco_bckg_cr->Add( input_trex_mureco_bckg_cr, input_trex_bckg_cr, 1.0, -1.0 );

    }

    std::vector< double > trex_photon_sf_sign_sr = {}, trex_photon_sf_bckg_sr = {}, trex_photon_sf_sign_cr = {}, trex_photon_sf_bckg_cr = {}; 
    std::vector< double > trex_photon_sf_shape_sign_sr = {}, trex_photon_sf_shape_bckg_sr = {}, trex_photon_sf_shape_sign_cr = {}, trex_photon_sf_shape_bckg_cr = {};
    std::vector< double > trex_mureco_sign_sr = {},  trex_mureco_bckg_sr = {}, trex_mureco_sign_cr = {}, trex_mureco_bckg_cr = {};

    if ( do_systematics ){

      trex_photon_sf_sign_sr = to_vec( input_trex_photon_sf_sign_sr );
      trex_photon_sf_bckg_sr = to_vec( input_trex_photon_sf_bckg_sr );
      trex_photon_sf_sign_cr = to_vec( input_trex_photon_sf_sign_cr );
      trex_photon_sf_bckg_cr = to_vec( input_trex_photon_sf_bckg_cr );
      trex_photon_sf_shape_sign_sr = to_vec( input_trex_photon_sf_shape_sign_sr );
      trex_photon_sf_shape_bckg_sr = to_vec( input_trex_photon_sf_shape_bckg_sr );
      trex_photon_sf_shape_sign_cr = to_vec( input_trex_photon_sf_shape_sign_cr );
      trex_photon_sf_shape_bckg_cr = to_vec( input_trex_photon_sf_shape_bckg_cr );
      trex_mureco_sign_sr = to_vec( input_trex_mureco_sign_sr );
      trex_mureco_bckg_sr = to_vec( input_trex_mureco_bckg_sr );
      trex_mureco_sign_cr = to_vec( input_trex_mureco_sign_cr );
      trex_mureco_bckg_cr = to_vec( input_trex_mureco_bckg_cr );

    }

    std::vector< double > trex_sr_prefit_sign_yields = to_vec( input_trex_sign_sr ); 
    std::vector< double > trex_sr_prefit_bckg_yields = to_vec( input_trex_bckg_sr );
    std::vector< double > trex_sr_prefit_data_yields = to_vec( input_trex_data_sr );
    std::vector< double > trex_cr_prefit_sign_yields = to_vec( input_trex_sign_cr );
    std::vector< double > trex_cr_prefit_bckg_yields = to_vec( input_trex_bckg_cr );
    std::vector< double > trex_cr_prefit_data_yields = to_vec( input_trex_data_cr );


    std::string trex_roofit_out = trex_qta_path + "Fits/" + unique + ".root";
    TFile * roofit_out = new TFile( trex_roofit_out.c_str(), "READ" );
    RooFitResult * rfr = (RooFitResult *) roofit_out->Get( "nll_simPdf_newasimovData_with_constr" );
    rfr->Print();

    std::vector< double > sr_prefit_sign_yields = sr_prefit["Samples"][0]["Yield"].as<std::vector<double>>();
    std::vector< double > sr_prefit_bckg_yields = sr_prefit["Samples"][1]["Yield"].as<std::vector<double>>();
    std::vector< double > sr_prefit_data_yields = sr_prefit["Data"][0]["Yield"].as<std::vector<double>>();
    std::vector< double > cr_prefit_sign_yields = cr_prefit["Samples"][0]["Yield"].as<std::vector<double>>();
    std::vector< double > cr_prefit_bckg_yields = cr_prefit["Samples"][1]["Yield"].as<std::vector<double>>();
    std::vector< double > cr_prefit_data_yields = cr_prefit["Data"][0]["Yield"].as<std::vector<double>>();

    std::vector< double > sr_postfit_sign_yields = sr_postfit["Samples"][0]["Yield"].as<std::vector<double>>();
    std::vector< double > sr_postfit_bckg_yields = sr_postfit["Samples"][1]["Yield"].as<std::vector<double>>();
    std::vector< double > sr_postfit_yields = sr_postfit["Total"][0]["Yield"].as<std::vector<double>>();
    std::vector< double > cr_postfit_sign_yields = cr_postfit["Samples"][0]["Yield"].as<std::vector<double>>();
    std::vector< double > cr_postfit_bckg_yields = cr_postfit["Samples"][1]["Yield"].as<std::vector<double>>();
    std::vector< double > cr_postfit_yields = cr_postfit["Total"][0]["Yield"].as<std::vector<double>>();

    int sr_bins = sr_prefit_sign_yields.size();
    int cr_bins = cr_prefit_sign_yields.size();

    RooRealVar * mu = (RooRealVar * ) rfr->floatParsFinal().find( "mu" );
    RooRealVar * spp = (RooRealVar * ) rfr->floatParsFinal().find( "spp" );
    double mu_d = mu->getVal();
    double spp_d = spp->getVal();

    std::cout << 843 << std::endl;

    RooRealVar * phs = 0, * mur = 0, * musf = 0;
    double phs_d = 0, phs_e = 0, mur_d = 0, mur_e = 0, musf_d = 0, musf_e = 0;

    std::cout << 846 << std::endl;

    if ( do_systematics ){

      phs = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_photon_sf" );
      phs_d = phs->getVal();
      phs_e = phs->getError();
      mur = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_mureco" );
      mur_d = mur->getVal();
      mur_e = mur->getError();
      musf = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_muon_sf" );
      musf_d = musf->getVal();
      musf_e = musf->getError();
      alpha_photon_sf->SetBinContent( ana_idx+1, phs_d );
      alpha_photon_sf->SetBinError( ana_idx+1, phs_e );
      alpha_mureco->SetBinContent( ana_idx+1, mur_d );
      alpha_mureco->SetBinError( ana_idx+1, mur_e );
      alpha_muon_sf->SetBinContent( ana_idx+1, musf_d );
      alpha_muon_sf->SetBinError( ana_idx+1, musf_e );

    }


    std::cout << 200 << std::endl;

    std::vector< double > sr_gammas;
    std::vector< double > cr_gammas;

    for ( int bin = 0; bin < sr_bins; bin++ ){
      std::string binstr = std::string("gamma_stat_SIGNAL_bin_") + std::to_string(bin);
      std::cout << binstr << std::endl;
      RooRealVar * var = (RooRealVar *) rfr->floatParsFinal().find( binstr.c_str() );
      if ( var != nullptr ){
        sr_gammas.push_back( var->getVal() );
      } else {
        sr_gammas.push_back( 0 );
      }
    }
    std::cout << 232 << std::endl;
    for ( int bin = 0; bin < cr_bins; bin++ ){
      
      std::string binstr = std::string("gamma_stat_CONTROL_bin_") + std::to_string(bin);
      std::cout << binstr << std::endl;
      RooRealVar * var = (RooRealVar *) rfr->floatParsFinal().find( binstr.c_str() );
      if ( var != nullptr ){
        cr_gammas.push_back( var->getVal() );
      } else {
        cr_gammas.push_back( 0 );
      }
    }

    std::cout << 238 << std::endl;

  

    std::cout << "TREX INPUT" << std::endl;
    std::cout << "SIGNAL - BDT Bins" << std::endl;
    std::cout << "sign prefit:       ";
    for ( double & sr : trex_sr_prefit_sign_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "sign prefit integral: "; write_big( integral( trex_sr_prefit_sign_yields ) );
    std::cout << "bckg prefit:       ";
    for ( double & sr : trex_sr_prefit_bckg_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "bckg prefit integral: "; write_big( integral( trex_sr_prefit_bckg_yields ));
    std::cout << "data prefit:       ";
    for ( double & sr : trex_sr_prefit_data_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "data prefit integral: "; write_big( integral( trex_sr_prefit_data_yields ) );
    std::cout << std::endl;
    std::cout << "CONTROL - BDT Bins" << std::endl;
    std::cout << "sign prefit:       ";
    for ( double & cr : trex_cr_prefit_sign_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "sign prefit integral: "; write_big( integral( trex_cr_prefit_sign_yields ) );
    std::cout << "bckg prefit:       ";
    for ( double & cr : trex_cr_prefit_bckg_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "bckg prefit integral: "; write_big( integral( trex_cr_prefit_bckg_yields ) );
    std::cout << "data prefit:       ";
    for ( double & cr : trex_cr_prefit_data_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "data prefit integral: "; write_big( integral( trex_cr_prefit_data_yields ) );
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "TREX INPUT - Calculation" << std::endl;
    std::cout << "SIGNAL" << std::endl;
    std::cout << "sign prefit * mu:  ";
    for ( double & sr : trex_sr_prefit_sign_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << sr * mu_d; }
    std::cout << std::endl;
    std::cout << "sign prefit * mu  Integral: "; write_big( scale_integral( trex_sr_prefit_sign_yields, mu_d ) );
    std::cout << "bckg prefit * spp: ";
    for ( double & sr : trex_sr_prefit_bckg_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << sr * spp_d; }
    std::cout << std::endl;
    std::cout << "bckg prefit * spp Integral: "; write_big( scale_integral( trex_sr_prefit_bckg_yields, spp_d ) );
    std::cout << "CONTROL" << std::endl;
    std::cout << "sign prefit * mu:  ";
    for ( double & cr : trex_cr_prefit_sign_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << cr * mu_d; }
    std::cout << std::endl;
    std::cout << "sign prefit * mu  Integral: "; write_big( scale_integral( trex_cr_prefit_sign_yields, mu_d ) );
    std::cout << "bckg prefit * spp: ";
    for ( double & cr : trex_cr_prefit_bckg_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << cr * spp_d; }
    std::cout << std::endl;
    std::cout << "bckg prefit * spp Integral: "; write_big( scale_integral( trex_cr_prefit_bckg_yields, spp_d ) );
    std::cout << std::endl;





    std::cout << "YAML" << std::endl;
    std::cout << "SIGNAL - BDT Bins" << std::endl;
    std::cout << "sign prefit:       ";
    for ( double & sr : sr_prefit_sign_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "sign prefit integral: "; write_big( integral( sr_prefit_sign_yields ) );
    std::cout << "bckg prefit:       ";
    for ( double & sr : sr_prefit_bckg_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "bckg prefit integral: "; write_big( integral( sr_prefit_bckg_yields ));
    std::cout << "data prefit:       ";
    for ( double & sr : sr_prefit_data_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "data prefit integral: "; write_big( integral( sr_prefit_data_yields ) );
    std::cout << std::endl;
    std::cout << "sign postfit:      ";
    for ( double & sr : sr_postfit_sign_yields ){ write_small( sr ); }
    std::cout << std::endl; 
    std::cout << "sign postfit integral: "; write_big( integral( sr_postfit_sign_yields ) );
    std::cout << "bckg postfit:      ";
    for ( double & sr : sr_postfit_bckg_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "bckg postfit integral: "; write_big( integral( sr_postfit_bckg_yields ) );
    std::cout << "yield postfit:     ";
    for ( double & sr : sr_postfit_yields ){ write_small( sr ); }
    std::cout << std::endl;
    std::cout << "yield postfit integral:"; write_big( integral( sr_postfit_yields ) );
    std::cout << std::endl;
    std::cout << "CONTROL - BDT Bins" << std::endl;
    std::cout << "sign prefit:       ";
    for ( double & cr : cr_prefit_sign_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "sign prefit integral: "; write_big( integral( cr_prefit_sign_yields ) );
    std::cout << "bckg prefit:       ";
    for ( double & cr : cr_prefit_bckg_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "bckg prefit integral: "; write_big( integral( cr_prefit_bckg_yields ) );
    std::cout << "data prefit:       ";
    for ( double & cr : cr_prefit_data_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "data prefit integral: "; write_big( integral( cr_prefit_data_yields ) );
    std::cout << std::endl;
    std::cout << "sign postfit:      ";
    for ( double & cr : cr_postfit_sign_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "sign postfit integral: "; write_big( integral( cr_postfit_sign_yields ) );
    std::cout << "bckg postfit:      ";
    for ( double & cr : cr_postfit_bckg_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "bckg postfit integral: "; write_big( integral( cr_postfit_bckg_yields ) );
    std::cout << "yield postfit:     ";
    for ( double & cr : cr_postfit_yields ){ write_small( cr ); }
    std::cout << std::endl;
    std::cout << "yield postfit integral:"; write_big( integral( cr_postfit_yields ) );
    std::cout << std::endl;


    std::cout << "YAML - Calculation" << std::endl;
    std::cout << "SIGNAL" << std::endl;
    std::cout << "sign prefit * mu:  ";
    for ( double & sr : sr_prefit_sign_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << sr * mu_d; }
    std::cout << std::endl;
    std::cout << "sign prefit * mu  Integral: "; write_big( scale_integral( sr_prefit_sign_yields, mu_d ) );
    std::cout << "bckg prefit * spp: ";
    for ( double & sr : sr_prefit_bckg_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << sr * spp_d; }
    std::cout << std::endl;
    std::cout << "bckg prefit * spp Integral: "; write_big( scale_integral( sr_prefit_bckg_yields, spp_d ) );
    std::cout << "CONTROL" << std::endl;
    std::cout << "sign prefit * mu:  ";
    for ( double & cr : cr_prefit_sign_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << cr * mu_d; }
    std::cout << std::endl;
    std::cout << "sign prefit * mu  Integral: "; write_big( scale_integral( cr_prefit_sign_yields, mu_d ) );
    std::cout << "bckg prefit * spp: ";
    for ( double & cr : cr_prefit_bckg_yields ){ std::cout << std::fixed << std::setprecision( 3 ) << std::setw(spacing)  << std::setfill( ' ' ) << cr * spp_d; }
    std::cout << std::endl;
    std::cout << "bckg prefit * spp Integral: "; write_big( scale_integral( cr_prefit_bckg_yields, spp_d ) );
    std::cout << std::endl;


    std::cout << "YAML - FULL PARAM CALCULATION" << std::endl;
    std::cout << "SIGNAL" << std::endl;
    std::cout << "sign prefit * gamma * mu  Integral: "; write_big( gamma_scale_integral( sr_prefit_sign_yields, sr_gammas, mu_d ) );
    std::cout << "bckg prefit * gamma * spp Integral: "; write_big( gamma_scale_integral( sr_prefit_bckg_yields, sr_gammas, spp_d ) );
    std::cout << "CONTROL" << std::endl;
    std::cout << "sign prefit * gamma * mu  Integral: "; write_big( gamma_scale_integral( cr_prefit_sign_yields, cr_gammas, mu_d ) );
    std::cout << "bckg prefit * gamma * spp Integral: "; write_big( gamma_scale_integral( cr_prefit_bckg_yields, cr_gammas, spp_d ) );
    std::cout << std::endl;

    if ( do_systematics ){

      std::cout << "TREX - EXPERIMENTAL PHOTON_SF" << std::endl;
      std::cout << "SIGNAL" << std::endl;
      std::cout << "sign prefit * gamma * mu * alpha_photon_sf  Integral: "; write_big( gamma_scale_integral( trex_photon_sf_sign_sr, sr_gammas, mu_d*phs_d ) );
      std::cout << "bckg prefit * gamma * spp * alpha_photon_sf Integral: "; write_big( gamma_scale_integral( trex_photon_sf_bckg_sr, sr_gammas, spp_d*phs_d ) );
      std::cout << "CONTROL" << std::endl;
      std::cout << "sign prefit * gamma * mu * alpha_photon_sf  Integral: "; write_big( gamma_scale_integral( trex_photon_sf_sign_cr, cr_gammas, mu_d*phs_d ) );
      std::cout << "bckg prefit * gamma * spp * alpha_photon_sf Integral: "; write_big( gamma_scale_integral( trex_photon_sf_bckg_cr, cr_gammas, spp_d*phs_d ) );
      std::cout << std::endl;

      std::cout << "TREX - EXPERIMENTAL PHOTON_SF SHAPE " << std::endl;
      std::cout << "SIGNAL" << std::endl;
      std::cout << "sign prefit * gamma * mu * alpha_photon_sf_shape  Integral: "; write_big( gamma_scale_integral( trex_photon_sf_shape_sign_sr, sr_gammas, mu_d*phs_d ) );
      std::cout << "bckg prefit * gamma * spp * alpha_photon_sf_shape Integral: "; write_big( gamma_scale_integral( trex_photon_sf_shape_bckg_sr, sr_gammas, spp_d*phs_d ) );
      std::cout << "CONTROL" << std::endl;
      std::cout << "sign prefit * gamma * mu * alpha_photon_sf_shape  Integral: "; write_big( gamma_scale_integral( trex_photon_sf_shape_sign_cr, cr_gammas, mu_d*phs_d ) );
      std::cout << "bckg prefit * gamma * spp * alpha_photon_sf_shape Integral: "; write_big( gamma_scale_integral( trex_photon_sf_shape_bckg_cr, cr_gammas, spp_d*phs_d ) );
      std::cout << std::endl;

      std::cout << "TREX - EXPERIMENTAL MURECO" << std::endl;
      std::cout << "SIGNAL" << std::endl;
      std::cout << "sign prefit * gamma * mu * alpha_mureco  Integral: "; write_big( gamma_scale_integral( trex_mureco_sign_sr, sr_gammas, mu_d*mur_d ) );
      std::cout << "bckg prefit * gamma * spp * alpha_mureco Integral: "; write_big( gamma_scale_integral( trex_mureco_bckg_sr, sr_gammas, spp_d*mur_d ) );
      std::cout << "CONTROL" << std::endl;
      std::cout << "sign prefit * gamma * mu * alpha_mureco  Integral: "; write_big( gamma_scale_integral( trex_mureco_sign_cr, cr_gammas, mu_d*mur_d ) );
      std::cout << "bckg prefit * gamma * spp * alpha_mureco Integral: "; write_big( gamma_scale_integral( trex_mureco_bckg_cr, cr_gammas, spp_d*mur_d ) );
      std::cout << std::endl;

    }

    std::cout << "YAML/TREX YIELD DIFFERENCE" << std::endl;
    std::cout << "SIGNAL" << std::endl;
    std::cout << "sign Integral: "; write_big( gamma_scale_integral( sr_prefit_sign_yields, sr_gammas, mu_d )  - integral( sr_postfit_sign_yields ) );
    std::cout << "bckg Integral: "; write_big( gamma_scale_integral( sr_prefit_bckg_yields, sr_gammas, spp_d ) - integral( sr_postfit_bckg_yields ) );
    std::cout << "CONTROL" << std::endl;       
    std::cout << "sign Integral: "; write_big( gamma_scale_integral( cr_prefit_sign_yields, cr_gammas, mu_d )  - integral( cr_postfit_sign_yields ) );
    std::cout << "bckg Integral: "; write_big( gamma_scale_integral( cr_prefit_bckg_yields, cr_gammas, spp_d ) - integral( cr_postfit_bckg_yields ) );
    std::cout << std::endl;

    if ( do_systematics ){

      double total_diff = gamma_scale_integral( sr_prefit_sign_yields, sr_gammas, mu_d )  - integral( sr_postfit_sign_yields )
                        + gamma_scale_integral( sr_prefit_bckg_yields, sr_gammas, spp_d ) - integral( sr_postfit_bckg_yields )
                        + gamma_scale_integral( cr_prefit_sign_yields, cr_gammas, mu_d )  - integral( cr_postfit_sign_yields )
                        + gamma_scale_integral( cr_prefit_bckg_yields, cr_gammas, spp_d ) - integral( cr_postfit_bckg_yields );

      double photsf_counter = gamma_scale_integral( trex_photon_sf_sign_sr, sr_gammas, mu_d*phs_d ) 
                            + gamma_scale_integral( trex_photon_sf_bckg_sr, sr_gammas, spp_d*phs_d )
                            + gamma_scale_integral( trex_photon_sf_sign_cr, cr_gammas, mu_d*phs_d ) 
                            + gamma_scale_integral( trex_photon_sf_bckg_cr, cr_gammas, spp_d*phs_d );

      double photsf_shape_counter = gamma_scale_integral( trex_photon_sf_shape_sign_sr, sr_gammas, mu_d*phs_d ) 
                                  + gamma_scale_integral( trex_photon_sf_shape_bckg_sr, sr_gammas, spp_d*phs_d )
                                  + gamma_scale_integral( trex_photon_sf_shape_sign_cr, cr_gammas, mu_d*phs_d ) 
                                  + gamma_scale_integral( trex_photon_sf_shape_bckg_cr, cr_gammas, spp_d*phs_d );


      double mureco_counter = gamma_scale_integral( trex_mureco_sign_sr, sr_gammas, mu_d*mur_d ) 
                            + gamma_scale_integral( trex_mureco_bckg_sr, sr_gammas, spp_d*mur_d )
                            + gamma_scale_integral( trex_mureco_sign_cr, cr_gammas, mu_d*mur_d ) 
                            + gamma_scale_integral( trex_mureco_bckg_cr, cr_gammas, spp_d*mur_d );

      std::cout << "total_diff   : " << total_diff << std::endl;
      std::cout << "photsf       : " << photsf_counter << std::endl;
      std::cout << "photsfshape  : " << photsf_shape_counter << std::endl;
      std::cout << "mureco       : " << mureco_counter << std::endl;
      std::cout << "total alpha  : " << photsf_counter + mureco_counter << std::endl;
      std::cout << "missing      : " << total_diff + photsf_counter + mureco_counter + photsf_shape_counter << std::endl;
      std::cout << std::endl;

    }

    std::cout << "TREX FIT RESULT" << std::endl;
    std::cout <<  "mu: "; write_big( mu_d );
    std::cout <<  "spp:"; write_big( spp_d );
    std::cout << std::endl;

    double sign_postfit_integral = integral( sr_postfit_sign_yields ) + integral( cr_postfit_sign_yields );
    double bckg_postfit_integral = integral( sr_postfit_bckg_yields ) + integral( cr_postfit_bckg_yields );
    double sign_prefit_integral = integral( sr_prefit_sign_yields ) + integral( cr_prefit_sign_yields );
    double bckg_prefit_integral = integral( sr_prefit_bckg_yields ) + integral( cr_prefit_bckg_yields ); 

    std::cout << "CALCULATED PARAMETERS" << std::endl;
    std::cout << "mu: "; write_big( sign_postfit_integral/sign_prefit_integral );
    std::cout << "spp:"; write_big( bckg_postfit_integral/bckg_prefit_integral );
    std::cout << std::endl;

    double postfit_table_signal_yield = table_postfit_result[0]["Samples"][0]["Yield"].as<double>();
    double postfit_table_signal_error = table_postfit_result[0]["Samples"][0]["Error"].as<double>();
    double postfit_table_control_yield = table_postfit_result[1]["Samples"][0]["Yield"].as<double>();
    double postfit_table_control_error = table_postfit_result[1]["Samples"][0]["Error"].as<double>();

    sign->SetBinContent( ana_idx+1, postfit_table_signal_yield + postfit_table_control_yield );
    sign->SetBinError( ana_idx+1, std::sqrt( postfit_table_signal_error * postfit_table_signal_error
                        + postfit_table_control_error * postfit_table_control_error ) );
    bckg->SetBinContent( ana_idx+1, integral( sr_postfit_bckg_yields ) + integral( cr_postfit_bckg_yields ) );
    data->SetBinContent( ana_idx+1, integral( sr_prefit_data_yields ) + integral( cr_prefit_data_yields )  );
    fit->SetBinContent( ana_idx+1,  integral( cr_postfit_yields ) + integral( sr_postfit_yields ) );

  }


  sign->SetLineColor( kRed+1 );
  bckg->SetLineColor( kBlue+1 );
  data->SetLineColor( kBlack );
  fit->SetLineColor( kGreen+1 );
  fit->SetLineStyle( 2 );

  prep_style();
  gStyle->SetOptStat( "" );

  TCanvas * yieldcanv = new TCanvas( "yc", "", 200, 200, 2000, 2000 );
  yieldcanv->Divide( 2, 2 );

  TPad * active_pad = (TPad *) yieldcanv->cd( 1 );
  bckg->Draw( "HIST E1 ");
  sign->Draw( "HIST E1 SAME");
  hist_prep_axes( bckg );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( bckg, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit", true );
  TLegend * sb_leg = below_logo_legend();
  sb_leg->AddEntry( sign, "sign" );
  sb_leg->AddEntry( bckg, "bckg" );
  sb_leg->Draw();

  active_pad = (TPad *) yieldcanv->cd( 2 );
  data->Draw( "HIST E1 ");
  fit->Draw( "HIST E1 SAME");
  hist_prep_axes( data );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( data, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit", true );
  TLegend * df_leg = below_logo_legend();
  df_leg->AddEntry( data, "data" );
  df_leg->AddEntry( fit, "fit" );
  df_leg->Draw();

  TH1F * datanorm = static_cast< TH1F * >( data->Clone() );
  TH1F * signnorm = static_cast< TH1F * >( sign->Clone() );
  TH1F * bckgnorm = static_cast< TH1F * >( bckg->Clone() );
  datanorm->Scale( 1.0/datanorm->Integral() ); 
  signnorm->Scale( 1.0/signnorm->Integral() );
  bckgnorm->Scale( 1.0/bckgnorm->Integral() );

  active_pad = (TPad *) yieldcanv->cd( 3 );
  //datanorm->Draw( "HIST E1 ");
  signnorm->Draw( "HIST E1");
  bckgnorm->Draw( "HIST E1 SAME");
  hist_prep_axes( datanorm );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( data, "q_{T}^{A}", "Density" );
  add_pad_title( active_pad, "Postfit Normalised", true );
  TLegend * df_norm_leg = below_logo_legend();
  //df_norm_leg->AddEntry( datanorm, "Data" );
  df_norm_leg->AddEntry( signnorm, "Sign" );
  df_norm_leg->AddEntry( bckgnorm, "Bckg" );
  df_norm_leg->Draw();

  yieldcanv->SaveAs( "yieldfit.png" );
  
  TF1 * dg = prep_dg( -6, 20 );
  align_dg( dg, sign );
  dg->SetLineColor( kBlack );
  dg->SetParLimits( 2, 0, 5 );
  dg->SetParLimits( 4, 10, 15 );

  //TF1 * dg = prep_dg( -6, 20 );
  //align_dg( dg, sign );
  //dg->SetLineColor( kBlack );
  //dg->SetParLimits( 2, 0, 5 );
  //dg->SetParLimits( 4, 10, 15 );

  TH1F * sign_noeff = (TH1F *) sign->Clone( "sign_noeff" );

  std::string eff_path = std::string( std::getenv( "OUT_PATH" ) ) + "/trees/";
  std::string eff_unique = run_node["eff_unique"].as<std::string>();
  eff_path += eff_unique + "/sign_efficiencies_" + eff_unique + ".root";
  fileset->load_efficiency_fileset( eff_path, "" );
  fileset->apply_sign_efficiency( sign, variables ); 
  gStyle->SetOptStat( "krimes" );
  sign->Fit( dg, "MQ", "", -5, 14.0 );

  TCanvas * fitcanv = new TCanvas( "fc", "", 200, 200, 2000, 2000 );
  fitcanv->Divide( 2, 2 );

  active_pad = (TPad *) fitcanv->cd( 1 );
  sign_noeff->Draw( "HIST E1 ");
  hist_prep_axes( sign_noeff );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( sign_noeff, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit", true );
  TLegend * fs1_leg = below_logo_legend();
  fs1_leg->AddEntry( sign_noeff, "sign" );
  fs1_leg->Draw();
  TPaveStats * fsstat = make_stats( sign_noeff );
  fsstat->Draw();


  active_pad = (TPad *) fitcanv->cd( 2 );
  sign->Draw( "HIST E1 ");
  dg->Draw( "SAME");
  hist_prep_axes( sign );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( sign, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit, efficiency corrected", true );
  TLegend * fs_leg = below_logo_legend();
  fs_leg->AddEntry( sign, "sign" );
  fs_leg->AddEntry( dg, "2gauss" );
  fs_leg->Draw();
  TPaveStats * stat = make_stats( sign );
  stat->Draw();


  TH1F * signy = static_cast<TH1F*>( sign_noeff->Clone() );

  active_pad = (TPad *) fitcanv->cd( 3 );
  signy->Draw( "HIST E1 ");
  active_pad->SetLogy();
  hist_prep_axes( signy );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( signy, variables.analysis_bound.get_x_str(), "Yield" );
  add_pad_title( active_pad, "Postfit, logy", true );
  TLegend * fsy_leg = below_logo_legend();
  fsy_leg->AddEntry( signy, "signy" );
  fsy_leg->Draw();
  TPaveStats * staty = make_stats( signy );
  staty->Draw();


  fitcanv->SaveAs( "yielddg.png" );

  if ( do_systematics ){

    alpha_photon_sf->SetMarkerColor( kBlack );
    alpha_photon_sf->SetLineColor( kBlack );
    alpha_photon_sf->SetMarkerStyle( 8 );
    alpha_photon_sf->SetMarkerSize( 1.0 );
    alpha_mureco->SetMarkerColor( kBlack );
    alpha_mureco->SetLineColor( kBlack );
    alpha_mureco->SetMarkerStyle( 8 );
    alpha_mureco->SetMarkerSize( 1 );
    alpha_muon_sf->SetMarkerColor( kBlack );
    alpha_muon_sf->SetLineColor( kBlack );
    alpha_muon_sf->SetMarkerStyle( 8 );
    alpha_muon_sf->SetMarkerSize( 1 );

    gStyle->SetOptStat( " " );
    TCanvas * alphaphot_canv = new TCanvas( "alphaphot", "", 200, 200, 3000, 1000 );
    alphaphot_canv->Divide( 3, 1 );

    active_pad = (TPad *) alphaphot_canv->cd( 1 );
    alpha_photon_sf->Draw( "P E1" );
    alpha_photon_sf->GetYaxis()->SetRangeUser( -4.0, 4.0 );
    hist_prep_axes( alpha_photon_sf );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( alpha_photon_sf, "q_{T}^{A}", "#alpha_{photon sf}" );
    add_pad_title( active_pad, "Postfit, alpha, photon sf", true );

    active_pad = (TPad *) alphaphot_canv->cd( 2 );
    alpha_mureco->Draw( "P E1" );
    alpha_mureco->GetYaxis()->SetRangeUser( -0.05, 0.05 );
    hist_prep_axes( alpha_mureco );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( alpha_mureco, "q_{T}^{A}", "#alpha_{mureco}" );
    add_pad_title( active_pad, "Postfit, alpha, mureco", true );

    active_pad = (TPad *) alphaphot_canv->cd( 3 );
    alpha_muon_sf->Draw( "P E1" );
    alpha_muon_sf->GetYaxis()->SetRangeUser( -1, 1 );
    hist_prep_axes( alpha_muon_sf );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( alpha_muon_sf, "q_{T}^{A}", "#alpha_{muon sf}" );
    add_pad_title( active_pad, "Postfit, alpha, muon sf", true );

    alphaphot_canv->SaveAs( "./alphas.png" );

  }

  std::cout << max_bins << std::endl;

}