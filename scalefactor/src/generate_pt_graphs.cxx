#include <generate_pt_graphs.hxx>

std::map< std::string, TH1F *> generate_pt_graphs( basic_fileset * fileset, const char * unique, sample_type type, variable_set * variables, bool threaded ){

	ROOT::EnableImplicitMT( 6 );

  //TH1F * jpsi_pt_data = variables->bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_data" );
  //TH1F * jpsi_pt_mc = variables->bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_mc" );
  //TH1F * jpsi_pt_ratio = variables->bound_manager->get_bound( "DiMuonPt" ).get_hist( "dimu_pt_ratio" );
  TH1F * jpsi_pt_data   = new TH1F( "dimu_pt_data", "", 100, 5, 30 );
  TH1F * jpsi_pt_mc     = new TH1F( "dimu_pt_mc", "", 100, 5, 30 );
  TH1F * jpsi_pt_ratio  = new TH1F( "dimu_pt_ratio", "", 100, 5, 30 );
  jpsi_pt_data->Sumw2();
  jpsi_pt_mc->Sumw2();
  jpsi_pt_ratio->Sumw2();  

  //TH1F * phot_pt_data = variables->bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_data" );
  //TH1F * phot_pt_mc = variables->bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_mc" );
  //TH1F * phot_pt_ratio = variables->bound_manager->get_bound( "PhotonPt" ).get_hist( "phot_pt_ratio" );
  TH1F * phot_pt_data   = new TH1F( "phot_pt_data", "", 100, 5, 30 );
  TH1F * phot_pt_mc     = new TH1F( "phot_pt_mc", "", 100, 5, 30 );
  TH1F * phot_pt_ratio  = new TH1F( "phot_pt_ratio", "", 100, 5, 30 );
  phot_pt_data->Sumw2();
  phot_pt_mc->Sumw2();  
  phot_pt_ratio->Sumw2();


  bound & qtb_bound = variables->bound_manager->get_bound( "qtB" );
  char sf_cut_expr[300]; sprintf( sf_cut_expr, "subtraction_weight*(%s)", qtb_bound.get_cut().c_str() ); 
  std::cout << sf_cut_expr << std::endl;

  TTree * data_tree = fileset->get_tree( sample_type::data );
  TTree * mc_tree = fileset->get_tree( type );

  data_tree->Draw( "DiMuonPt>>dimu_pt_data", sf_cut_expr, "e goff" );
  mc_tree->Draw( "DiMuonPt>>dimu_pt_mc", sf_cut_expr, "e goff" );

  data_tree->Draw( "PhotonPt>>phot_pt_data", sf_cut_expr, "e goff" );
  mc_tree->Draw( "PhotonPt>>phot_pt_mc", sf_cut_expr, "e goff" );

  jpsi_pt_data->Scale( 1.0/jpsi_pt_data->Integral() );
  jpsi_pt_mc->Scale( 1.0/jpsi_pt_mc->Integral() );

  phot_pt_data->Scale( 1.0/phot_pt_data->Integral() );
  phot_pt_mc->Scale( 1.0/phot_pt_mc->Integral() );

  jpsi_pt_ratio->Divide( jpsi_pt_data, jpsi_pt_mc, 1.0, 1.0 );
  phot_pt_ratio->Divide( phot_pt_data, phot_pt_mc, 1.0, 1.0 );

  

  std::map< std::string, TH1F *> outputs;
  if ( !threaded ){
    char filename[150]; sprintf( filename, "pt_ratio_%s_%s.root", type_c[type], unique );
    TFile * output_file = new TFile( filename, "RECREATE" ); 
    output_file->cd();
    jpsi_pt_ratio->Write( "jpsi_weights" );
    phot_pt_ratio->Write( "photon_weights" );
    jpsi_pt_mc->Write( "jpsi_pt_mc" );
    jpsi_pt_data->Write( "jpsi_pt_data" );
    phot_pt_mc->Write( "phot_pt_mc" );
    phot_pt_data->Write( "phot_pt_data" );
    output_file->Close();
    delete jpsi_pt_ratio;
    delete phot_pt_ratio;
  } else {
    outputs["jpsi_weights"] = jpsi_pt_ratio;
    outputs["photon_weights"] = phot_pt_ratio;
  }

  delete jpsi_pt_data;
  delete jpsi_pt_mc;
  delete phot_pt_data;
  delete phot_pt_mc;
   
  return outputs;


}
