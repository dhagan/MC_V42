#include <process_scalefactors.hxx>

//double mean = std::accumulate( bdt_hists.begin(), bdt_hists.end(), 0, [ &bin ]( const double & total, TH1F * hist ){ return total + hist->GetBinContent( bin ); } )/bdt_hists.size();
//return std::sqrt( std::accumulate( bdt_hists.begin(), bdt_hists.end(), 0, [ &bin ]( const double & rms, TH1F * hist ){ return rms + (hist->GetBinContent( bin )*hist->GetBinContent( bin ) ); } )/bdt_hists.size() );
double calculate_rms( int & bin, std::vector< TH1F *> & bdt_hists ){
  double rms = 0;
  for ( TH1F * hist : bdt_hists ){ rms += ( hist->GetBinContent( bin ) * hist->GetBinContent( bin ) ); }
  rms /= (float) bdt_hists.size();
  return std::sqrt( rms );
}

double calculate_rmse( int & bin, std::vector< TH1F *> & bdt_hists ){

  double mean = 0;
  for ( TH1F * hist : bdt_hists ){ mean += hist->GetBinContent( bin ); }
  mean /= (float) bdt_hists.size();

  double rms = 0;
  for ( TH1F * hist : bdt_hists ){ rms += ( ( hist->GetBinContent( bin ) - mean ) * ( hist->GetBinContent( bin ) - mean ) ); }
  rms /= (float) bdt_hists.size();
  return std::sqrt( rms );
}

double calculate_mean( int & bin, std::vector< TH1F *> & bdt_hists ){

  double mean = 0;
  for ( TH1F * hist : bdt_hists ){ mean += hist->GetBinContent( bin ); }
  mean /= (float) bdt_hists.size();

  return mean;
}

TH1F * aggregate( int & bin, std::vector< TH1F * > & bdt_hists ){
  TH1F * aggregate_hist = new TH1F( std::to_string( random() ).c_str() ,"", 1000, -20000, 20000 );
  for ( TH1F * hist : bdt_hists ){ 
    aggregate_hist->Fill( hist->GetBinContent( bin ) );
  }
  return aggregate_hist;
}


void plot_histogram( TH1F * hist, bound & spectator_bound, const char * unique, const std::string & name="" ){

  TCanvas * current_canvas = new TCanvas( hist->GetName(), "", 200, 200, 1000, 1000 );
  current_canvas->Divide( 1, 1 );
  TPad * current_pad = (TPad *) current_canvas->cd( 1 );
  hist->Draw( "HIST" );
  hist_prep_axes( hist, true );
  hist->GetYaxis()->SetRangeUser( 0, hist->GetMaximum()*1.2 ); 
  add_pad_title( current_pad, Form( "%s %s", spectator_bound.get_var().c_str(), unique ), true );
  add_atlas_decorations( current_pad, true, false );
  set_axis_labels( hist, spectator_bound.get_x_str(), Form( "%s", spectator_bound.get_y_str().c_str() ) );
  TLegend * hist_legend = below_logo_legend();
  hist_legend->AddEntry( hist, hist->GetName() );
  hist_legend->Draw( "SAME" );
  TPaveStats * stats = make_stats( hist, false, false );
  stats->Draw( "SAME" );
  char hist_plot_filename[200]; sprintf( hist_plot_filename, "plots/%s.png", hist->GetName() );
  if ( !name.empty() ){ sprintf( hist_plot_filename, "plots/%s.png", name.c_str() ); };
  current_canvas->SaveAs( hist_plot_filename );
  delete current_canvas;
}

//void process_scalefactors( TFile * variations, variable_set * variables, const char * unique, sample_type type, region region, bool threaded ){


std::map< std::string, TH1F *> process_scalefactors( TFile * variations, variable_set * variables, const char * unique, sample_type type, region region, bool threaded ){

  if ( !threaded ){
    prep_style();
    gStyle->SetOptStat( "rimes" );
  }

  std::map< std::string, TH1F *> output;

  bound & analysis_bound = variables->analysis_bound;
  bound & spectator_bound = variables->spectator_bound;
  bound & qtb_bound = variables->bound_manager->get_bound( "qtB" );
  std::string qtb_cut = qtb_bound.get_cut();
  std::vector< std::string > analysis_cut_series = analysis_bound.get_cut_series();
  std::vector< std::string > analysis_cut_names = analysis_bound.get_series_names();
  std::vector< TH1F * > output_histograms;
  if ( !threaded ){ output_histograms.reserve( analysis_bound.get_bins()*3 ); }

  for ( int analysis_bin = 0; analysis_bin < analysis_bound.get_bins(); analysis_bin++ ){
    std::vector< TH1F * > bdt_hists;
    char hist_name[100];
    for ( int hist = 0; hist < 100; hist++ ){
      sprintf( hist_name, "%s_v%i", analysis_cut_names[analysis_bin].c_str(), hist );
      bdt_hists.push_back( (TH1F * ) variations->Get( hist_name ) );
    }
    sprintf( hist_name, "%s_syst", analysis_cut_names[analysis_bin].c_str() );
    TH1F * nominal_hist = (TH1F * ) variations->Get( analysis_cut_names[analysis_bin].c_str());
    TH1F * syst_hist = (TH1F * ) variations->Get( hist_name );
    if ( !threaded ){ std::cout << "Variations loaded..." << std::endl; }

    TH1F * rms_hist = spectator_bound.get_hist( "rms_" + analysis_cut_names[ analysis_bin ], 50 );
    TH1F * mean_hist = spectator_bound.get_hist( "mean_" + analysis_cut_names[ analysis_bin ], 50 );
    TH1F * rmse_hist = spectator_bound.get_hist( "rmse_" + analysis_cut_names[ analysis_bin ], 50 );
    TH1F * rmse_rel_hist = spectator_bound.get_hist( "rmse_rel_" + analysis_cut_names[ analysis_bin ], 50 );
    TH1F * syst_rel_hist = spectator_bound.get_hist( "syst_rel_" + analysis_cut_names[ analysis_bin ], 50 );
    for ( int bin = 1; bin <= 50; bin++ ){  
      rms_hist->SetBinContent( bin, calculate_rms( bin, bdt_hists ) );
      rmse_hist->SetBinContent( bin, calculate_rmse( bin, bdt_hists ) );
      mean_hist->SetBinContent( bin, calculate_mean( bin, bdt_hists ) ); 
    }
    if ( !threaded ){ std::cout << "RMS Calculated..." << std::endl << std::endl; }


    rmse_rel_hist->Divide( rmse_hist, nominal_hist );
    syst_rel_hist->Divide( syst_hist, nominal_hist );
    TH1F * err_hist = spectator_bound.get_hist( "err_" + analysis_cut_names[ analysis_bin ], 50 );
    for ( int bin = 1; bin <= 50; bin++ ){  
      double stat_err = rmse_rel_hist->GetBinContent( bin );
      double syst_err = syst_rel_hist->GetBinContent( bin );
      err_hist->SetBinContent( bin, std::sqrt( stat_err*stat_err + syst_err*syst_err ) );
    }

    if ( !threaded ){
      std::cout << "Error Calculated..." << std::endl << std::endl;
      output_histograms.push_back( err_hist );
      output_histograms.push_back( rms_hist );
      output_histograms.push_back( rmse_hist );
      output_histograms.push_back( mean_hist );
    }
    bdt_hists.clear();

    char percent_buffer[300]; 
    sprintf( percent_buffer, "percent_%s_%s_%s_%s", type_c[type], unique, region_c[region], analysis_cut_names[analysis_bin].c_str() );
    TH1F * perc_hist = (TH1F *) err_hist->Clone( percent_buffer );
    perc_hist->Scale( 100 );
    if ( !threaded ){ plot_histogram( perc_hist, spectator_bound, unique ); }

    char variation_filename[160]; sprintf( variation_filename, "%s_%s_%s.root", type_c[type], unique, analysis_cut_names[analysis_bin].c_str() );
    std::string variation_hist = std::string( region_c[region] ) + "0";
    TH1F * variation = spectator_bound.get_hist( variation_hist, 50 );
    if ( !threaded ){ plot_histogram( nominal_hist, spectator_bound, unique, std::to_string(analysis_bin) + type_str[type] + region_c[region] ); }
    for ( int bin = 1; bin <= 50; bin++ ){  
      variation->SetBinContent( bin, nominal_hist->GetBinContent( bin )*( 1 + err_hist->GetBinContent( bin ) )  );
      variation->SetBinError( bin, nominal_hist->GetBinError( bin )*( 1 + err_hist->GetBinContent( bin ) )  );
    }

    output[ analysis_cut_names[analysis_bin] ] = variation;

    if ( !threaded ){ 
      std::string variation_name = "var"+std::string( unique ) + std::string(variation->GetName()) +std::to_string( analysis_bin ) + std::string( type_c[type] );
      plot_histogram( variation, spectator_bound, unique, variation_name );
      TH1F * delta_hist = spectator_bound.get_hist( "delta" + std::to_string( analysis_bin ) + region_c[region] + type_c[type], 50 );
      delta_hist->Add( variation, nominal_hist, 1.0, -1.0 );
      plot_histogram( delta_hist, spectator_bound, unique ); 
    }

  }

  if ( !threaded ){
    char output_filepath[200]; sprintf( output_filepath, "sf_%s_%s_%s.root", type_c[type], unique, region_c[region] );
    TFile * output_file = new TFile( output_filepath, "RECREATE" );
    output_file->cd();
    for ( TH1F * hist : output_histograms ){
      hist->Write();
    }
    output_file->Close();

    std::map< std::string, TH1F *>::iterator pairs;
    for ( pairs = output.begin(); pairs != output.end(); pairs++ ){
  
      sprintf( output_filepath, "%s_%s_%s.root", type_c[type], unique, pairs->first.c_str() );
      TFile * output_file = new TFile( output_filepath, "UPDATE" );
      TList * keys = output_file->GetListOfKeys();
      TH1F * other = nullptr;
      for ( TObject * obj : *keys ){
        if ( ( std::string( region_c[region] ) + "0" ).find( obj->GetName() ) == std::string::npos ){
          other = (TH1F *) output_file->Get( obj->GetName() );
          other->SetDirectory( nullptr );
        }
      }
      output_file->Close();
      output_file = new TFile( output_filepath, "RECREATE" );
      if ( other ){ 
        other->Write();
      }
      pairs->second->Write( );
      output_file->Close(); 

    }







  }


  return output;
}
