#include <ntuple_utils.hxx>
#include <calculate_scalefactors.hxx>
#include <generate_variations.hxx>
#include <generate_pt_graphs.hxx>
#include <process_scalefactors.hxx>

#include <yaml-cpp/yaml.h>
#include <sys/stat.h>

#include <thread>

// thread input struct
typedef struct sf_run {
  std::string unique, input_unique;
  std::string validation_unique;
  std::vector< std::string > closure_uniques;
  bool closure_internal;
  std::string input_filepath;
  std::map< std::string, bool > modes; 
  variable_set * variables;
  region phys_region;
  sample_type type; 
  bool batch;

  sf_run( YAML::Node node ){
    unique = node["unique"].as<std::string>();
    if ( node["input_unique"] ){
      input_unique = node["input_unique"].as<std::string>();
    } else {
      input_unique = unique;
    }
    input_filepath = node["input_filepath"].as<std::string>();
    modes = node["modes"].as<std::map<std::string, bool>>(); 
    std::cout << node["region"].as<std::string>() << std::endl;
    phys_region = region_convert( node["region"].as<std::string>() );
    type = sample_type_convert( node["type"].as<std::string>() );
    std::string bound_filepath = node["bound_filepath"].as<std::string>();
    std::string ranges = node["ranges"].as<std::string>();
    std::string analysis_variable = node["analysis_variable"].as<std::string>();
    std::string spectator_variable = node["spectator_variable"].as<std::string>();
    variables = new variable_set( bound_filepath, ranges, analysis_variable, spectator_variable );
    validation_unique = "";
    closure_uniques = {};
    if ( node["validation_unique"] ){ validation_unique = node["validation_unique"].as<std::string>(); }
    if ( node["closure_uniques"] ){ closure_uniques = node["closure_uniques"].as<std::vector<std::string>>(); }
    if ( node["closure_internal"] ){ closure_internal = node["closure_internal"].as<bool>(); }
    if ( node["batch"] ){ 
      batch = node["batch"].as<bool>();
    } else {
      batch = false;
    }
  }

  sf_run(){}
} sf_run;

// thread output struct
typedef struct sf_run_out {
  std::string unique;
  region phys_region;
  sample_type type;
  std::map< std::string, TH1F *> weight_output;
  std::map< std::string, TH1F * > variation_output;
} sf_run_out;


void prepare_runs( std::stack<sf_run> & configs, std::vector< sf_run_out > & run_out, YAML::Node & config, std::vector<std::string> & run_nodes ){

  std::cout << "Preparing runs: " << std::endl;
  for ( std::string & run_name : run_nodes ){ std::cout << "\t" << run_name << std::endl; }
  std::reverse( run_nodes.begin(), run_nodes.end() );

  for ( std::string & run_name : run_nodes ){
    std::cout << "Run: " << run_name << std::endl;
    YAML::Node run_node = config[run_name];
    sf_run sf_config( run_node );
    configs.push( sf_config );
    run_out.push_back( sf_run_out() );
    std::cout << std::endl;
  }

  std::cout << configs.size() << " runs prepared" << std::endl;
  std::cout << std::endl;

}

void run( sf_run config, sf_run_out & output, bool threaded = false ){

  output.unique = config.unique;
  output.phys_region = config.phys_region;
  output.type = config.type;
  
  std::string sf_filepath = "";
  if ( config.modes["calculate"] && !threaded ){
    std::cout << 90 << std::endl;

    if ( config.batch ){
      std::string workdir = "./" + config.unique;
      std::cout << "workdir: " << workdir << std::endl;
      std::cout << mkdir( workdir.c_str(), 744 );
      std::cout << chdir( workdir.c_str() );
    }
    std::string current_filepath = std::getenv( "OUT_PATH" ) + config.input_filepath;
    std::cout << current_filepath << std::endl;
    calculate_scalefactors( current_filepath.c_str(), config.unique.c_str(), config.input_unique.c_str(), 
      config.type, config.phys_region, config.validation_unique, config.closure_uniques, config.closure_internal );
    if ( config.batch ){ std::cout << chdir( ".." ); }
    std::cout << std::endl;
  } 

}
int main(int argc, char ** argv){

  ROOT::EnableThreadSafety();

  int option_index{0},option{0};
  static struct option long_options[] = {
      { "input",          required_argument,    0,      'i'},
      { 0,                0,                    0,      0}
  };

  const char * config_path = "";

  do {
    option = getopt_long( argc, argv, "i:", long_options, &option_index );
    switch ( option ){
      case 'i':
        config_path = optarg; 
        option = -1;
        break;
    }
  } while ( option != -1 );

  std::stack < sf_run > configs;
  std::vector< std::thread > threads;
  std::vector< sf_run_out > run_out;

  if ( strlen( config_path ) == 0 ){ throw std::invalid_argument( "No config file provided" ); }

  YAML::Node config = YAML::LoadFile( config_path );
  auto run_nodes = config["general"]["runs"].as<std::vector<std::string>>();
  bool threaded = config["general"]["threaded"].as<bool>();

  prepare_runs( configs, run_out, config, run_nodes ); 

  int out_int = 0;
  while ( !configs.empty() ){
    sf_run config = configs.top();
    configs.pop();
    run( config, std::ref( run_out.at(out_int) ), threaded );
  }


}

  //if ( threaded ){
    //  threads.push_back( std::thread( run, config, std::ref( run_out[out_int] ), threaded ) );
    //  out_int++;
    //} else {
      
    //}
  

  //for ( std::thread & thread : threads ){
  //  thread.join();
  //}
  //if ( threaded ){
  //  write_output( run_out, threaded );
  //}



  //if ( config.modes["weight"] && output.type == sample_type::sign ){
  //if ( config.modes["weight"] ){ //&& output.type == sample_type::sign ){
  //  basic_fileset * sub_files = new basic_fileset();
  //  std::string subtraction_filepath = std::getenv( "OUT_PATH" ) + config.input_filepath;
  //  sub_files->load_subtraction_fileset( subtraction_filepath, config.input_unique );
  //  output.weight_output = generate_pt_graphs( sub_files, config.unique.c_str(), config.type, config.variables, threaded );
  //}
  
  //if ( config.modes["generate"] ){ 
  //  std::string sf_filepath = std::getenv( "OUT_PATH" ) + std::string( "/scalefactors/" ) 
  //    + config.unique + "/" + std::string( type_c[config.type] ) + "/" 
  //    + std::string( type_c[config.type] ) + "_" + config.unique + "_" 
  //    + region_c[config.phys_region] + ".root";
  //  std::cout << sf_filepath << std::endl;
  //  std::cout << region_c[ config.phys_region ] << std::endl;
  //  generate_variations( sf_filepath, config.variables, config.unique.c_str(), config.type, config.phys_region );
  //}
  //  
  //if ( config.modes["process"] ){ 
  //  char variation_filepath[200]; 
  //  sprintf( variation_filepath, "./variations_%s_%s_%s.root", config.unique.c_str(), type_c[config.type], region_c[config.phys_region] );
  //  TFile * variation_file = new TFile( variation_filepath );
  //  output.variation_output = process_scalefactors( variation_file, config.variables, config.unique.c_str(), config.type, config.phys_region, threaded );
  //}



//void write_output( std::vector< sf_run_out > & run_out, bool threaded ){
//
//  // write out hists
//  char output_filepath[150];
//  std::map< std::string, TH1F *>::iterator pairs;
//  for ( sf_run_out & outputs : run_out ){
//
//    std::map<std::string, TH1F *> & output_map = outputs.variation_output;
//    for ( pairs = output_map.begin(); pairs != output_map.end(); pairs++ ){
//  
//      sprintf( output_filepath, "%s_%s_%s.root", type_c[outputs.type], outputs.unique.c_str(), pairs->first.c_str() );
//      TFile * output_file = new TFile( output_filepath, "UPDATE" );
//      TList * keys = output_file->GetListOfKeys();
//      TH1F * other = nullptr;
//      for ( TObject * obj : *keys ){
//        if ( ( std::string( region_c[outputs.phys_region] ) + "0" ).find( obj->GetName() ) == std::string::npos ){
//          other = (TH1F *) output_file->Get( obj->GetName() );
//          other->SetDirectory( nullptr );
//        }
//      }
//      output_file->Close();
//      output_file = new TFile( output_filepath, "RECREATE" );
//      if ( other ){ 
//        other->Write();
//      }
//      pairs->second->Write( );
//      output_file->Close(); 
//
//    }
//
//    output_map = outputs.weight_output;
//    if ( output_map.size() != 0  && threaded ){ 
//      sprintf( output_filepath, "pt_ratio_%s_%s.root", type_c[outputs.type], outputs.unique.c_str() );
//      TFile * output_file = new TFile( output_filepath, "RECREATE" );
//      for ( pairs = output_map.begin(); pairs != output_map.end(); pairs++ ){
//        std::string name = pairs->first;
//        (pairs->second)->Write( name.c_str() );  
//      } 
//      output_file->Close();
//    }
//
//  }
//
//}

