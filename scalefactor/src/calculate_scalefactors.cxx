#include <calculate_scalefactors.hxx>
#include <sys/stat.h>

void calculate_scalefactors( const char * input_filepath, const char * unique, const char * input_unique,
  sample_type type, region region, const std::string & validation_unique, const std::vector<std::string> & closure_uniques,
  bool closure_internal ){

  ROOT::EnableThreadSafety();
	ROOT::EnableImplicitMT( 6 );

  mkdir( "./sign", 744 );
  mkdir( "./bckg", 744 );
  mkdir( "./data", 744 );

  //if ( type == sample_type::bckg ){
  //  bloom_d *= 0.728
  //  bloom_q *= 0.728
  //}

  // messy, fix the next 200 or so lines when I have time
  scalefactor sf_nominal;
  sf_nominal.load_muon_SF();
  sf_nominal.load_photon_SF();

  scalefactor sf_rec;
  sf_rec.load_muon_SF();
  sf_rec.load_photon_SF();
  
  scalefactor sf_step;
  sf_step.load_muon_SF();
  sf_step.load_photon_SF();


  scalefactor sf_double_bloom;
  sf_double_bloom.load_muon_SF();
  sf_double_bloom.load_photon_SF();
  sf_double_bloom.set_bounds_error_bloom( 2.0 );

  scalefactor sf_quad_bloom;
  sf_quad_bloom.load_muon_SF();
  sf_quad_bloom.load_photon_SF();
  sf_quad_bloom.set_bounds_error_bloom( 4.0 );

  scalefactor sf_qb_lt_bloom;
  sf_qb_lt_bloom.load_muon_SF();
  sf_qb_lt_bloom.load_photon_SF();
  sf_qb_lt_bloom.set_bounds_error_bloom( 4.0 );
  sf_qb_lt_bloom.set_bloom_bounds( 6e3 );

  scalefactor sf_l10;
  sf_l10.load_muon_SF();
  sf_l10.load_photon_SF();
  sf_l10.set_linear_scaling( 0.1 );

  scalefactor sf_l20;
  sf_l20.load_muon_SF();
  sf_l20.load_photon_SF();
  sf_l20.set_linear_scaling( 0.2 );
  
  scalefactor sf_l30;
  sf_l30.load_muon_SF();
  sf_l30.load_photon_SF();
  sf_l30.set_linear_scaling( 0.3 );

  scalefactor sf_l20_c20;
  sf_l20_c20.load_muon_SF();
  sf_l20_c20.load_photon_SF();
  sf_l20_c20.set_linear_scaling( 0.2 );
  sf_l20_c20.set_central_linear_scaling( 0.02/5.0 );


  Bphys::BphysEffTool * bphys_sf_tool = new Bphys::BphysEffTool();
  
  
  auto sf_photon       = [ &sf_nominal ](  const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_nominal.photon_SF( photon_pt, photon_eta[0] ); };
  auto sf_photon_error = [ &sf_nominal ](  const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_nominal.photon_SF_uncertainty( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper = [ &sf_nominal ](  const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_nominal.photon_SF_upper( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower = [ &sf_nominal ](  const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_nominal.photon_SF_lower( photon_pt, photon_eta[0] ); };

  auto sf_photon_rec       = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 1.0;}
    return (float) sf_rec.photon_SF( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_upper_rec_usable = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 1.0;}
    return (float) sf_rec.photon_SF_upper( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_error_rec = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 1.0; }
    return (float) sf_rec.photon_SF_uncertainty( photon_pt, photon_eta[0] 
  ); };
  auto sf_photon_upper_rec = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 2.0; }
    return (float) sf_rec.photon_SF_upper( photon_pt, photon_eta[0] ); 
  };
  auto sf_photon_lower_rec = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 0.0; }
    return (float) sf_rec.photon_SF_lower( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_step       = [ &sf_step ]( const float & photon_pt, const ROOT::RVecF &photon_eta ){ 
    float sub = ( photon_pt < 10000 ) ? 0.01 : 0.00;
    return (float) sf_step.photon_SF( photon_pt, photon_eta[0] ) - sub;  
  };

  std::vector<double> extended_eta_bin_edge = { 0.0, 0.6, 0.8, 1.4, 1.5, 1.8, 2.4 };
  std::vector<double> bin_values = { 1.01, 0.91, 0.89, 0.71, 0.87, 0.94 };
  std::vector<double> bin_errors = { 0.2202, 0.0419, 0.0076, 0.1823, 0.0637, 0.0975 };
  TH1F * sf_extended = new TH1F( "sf_extended", "", 6, &extended_eta_bin_edge[0] );
  for ( int bin = 1; bin <=6; bin++ ){
    sf_extended->SetBinContent( bin, bin_values.at( bin-1 ) );
    sf_extended->SetBinError( bin, bin_errors.at( bin-1 ) );
  }
  auto sf_photon_extended = [ &sf_extended, &sf_rec ]( const float &photon_pt, const ROOT::RVecF &photon_eta ){ 
    if ( photon_pt < 7000.0 ){ return 1.0f; }
    if ( photon_pt < 10000.0 ){
      return (float) sf_extended->GetBinContent( sf_extended->FindBin( abs( photon_eta[0] ) ) );
    }
    return (float) sf_rec.photon_SF( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_extended_upper = [ &sf_extended, &sf_rec ]( const float &photon_pt, const ROOT::RVecF &photon_eta ){ 
    if ( photon_pt < 7000.0 ){ return 2.0f; }
    if ( photon_pt < 10000.0 ){
      int bin = sf_extended->FindBin( abs( photon_eta[0] ) );
      return (float) ( sf_extended->GetBinContent( bin ) + sf_extended->GetBinError( bin ) );
    }
    return (float) sf_rec.photon_SF_upper( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_extended_risk = [ &sf_extended, &sf_rec ]( const float &photon_pt, const ROOT::RVecF &photon_eta ){ 
    if ( photon_pt < 10000.0 ){
      return (float) sf_extended->GetBinContent( sf_extended->FindBin( abs( photon_eta[0] ) ) );
    }
    return (float) sf_rec.photon_SF( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_extended_risk_upper = [ &sf_extended, &sf_rec ]( const float &photon_pt, const ROOT::RVecF &photon_eta ){ 
    if ( photon_pt < 10000.0 ){
      int bin = sf_extended->FindBin( abs( photon_eta[0] ) );
      return (float) ( sf_extended->GetBinContent( bin ) + sf_extended->GetBinError( bin ) );
    }
    return (float) sf_rec.photon_SF_upper( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_upper_rec_50 = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 1.5; }
    return (float) sf_rec.photon_SF_upper( photon_pt, photon_eta[0] ); 
  };
  
  auto sf_photon_upper_rec_25 = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 1.25; }
    return (float) sf_rec.photon_SF_upper( photon_pt, photon_eta[0] ); 
  };

  auto sf_photon_upper_rec_10 = [ &sf_rec ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ 
    if ( photon_pt < 10000 ){ return (float) 1.10; }
    return (float) sf_rec.photon_SF_upper( photon_pt, photon_eta[0] ); 
  };


 
  auto sf_photon_db       = [ &sf_double_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_double_bloom.photon_SF( photon_pt, photon_eta[0] ); };
  auto sf_photon_error_db = [ &sf_double_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_double_bloom.photon_SF_uncertainty( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper_db = [ &sf_double_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_double_bloom.photon_SF_upper( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower_db = [ &sf_double_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_double_bloom.photon_SF_lower( photon_pt, photon_eta[0] ); };
  auto sf_photon_bloom_db = [ &sf_double_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_double_bloom.bounds_bloom( photon_pt, photon_eta[0] ); };

  auto sf_photon_qb       = [ &sf_quad_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_quad_bloom.photon_SF( photon_pt, photon_eta[0] ); };
  auto sf_photon_error_qb = [ &sf_quad_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_quad_bloom.photon_SF_uncertainty( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper_qb = [ &sf_quad_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_quad_bloom.photon_SF_upper( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower_qb = [ &sf_quad_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_quad_bloom.photon_SF_lower( photon_pt, photon_eta[0] ); };
  auto sf_photon_bloom_qb = [ &sf_quad_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_quad_bloom.bounds_bloom( photon_pt, photon_eta[0] ); };

  auto sf_photon_qb_lt       = [ &sf_qb_lt_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_qb_lt_bloom.photon_SF( photon_pt, photon_eta[0] ); };
  auto sf_photon_error_qb_lt = [ &sf_qb_lt_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_qb_lt_bloom.photon_SF_uncertainty( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper_qb_lt = [ &sf_qb_lt_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_qb_lt_bloom.photon_SF_upper( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower_qb_lt = [ &sf_qb_lt_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_qb_lt_bloom.photon_SF_lower( photon_pt, photon_eta[0] ); };
  auto sf_photon_bloom_qb_lt = [ &sf_qb_lt_bloom ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_qb_lt_bloom.bounds_bloom( photon_pt, photon_eta[0] ); };


  auto sf_photon_l10       = [ &sf_l10 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l10.photon_SF( photon_pt, photon_eta[0] ); };
  auto sf_photon_error_l10 = [ &sf_l10 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l10.photon_SF_uncertainty_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper_l10 = [ &sf_l10 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l10.photon_SF_upper_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower_l10 = [ &sf_l10 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l10.photon_SF_lower_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_scaling_l10 = [ &sf_l10 ]( const float & photon_pt  ){ return (float) sf_l10.get_scaling( photon_pt ); };

  auto sf_photon_l20       = [ &sf_l20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20.photon_SF( photon_pt, photon_eta[0] ); };
  auto sf_photon_error_l20 = [ &sf_l20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20.photon_SF_uncertainty_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper_l20 = [ &sf_l20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20.photon_SF_upper_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower_l20 = [ &sf_l20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20.photon_SF_lower_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_scaling_l20 = [ &sf_l20 ]( const float & photon_pt  ){ return (float) sf_l20.get_scaling( photon_pt ); };

  auto sf_photon_l30       = [ &sf_l30 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l30.photon_SF( photon_pt, photon_eta[0] ); };
  auto sf_photon_error_l30 = [ &sf_l30 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l30.photon_SF_uncertainty_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper_l30 = [ &sf_l30 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l30.photon_SF_upper_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower_l30 = [ &sf_l30 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l30.photon_SF_lower_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_scaling_l30 = [ &sf_l30 ]( const float & photon_pt  ){ return (float) sf_l30.get_scaling( photon_pt ); };


  auto sf_photon_l20_c20       = [ &sf_l20_c20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20_c20.photon_SF_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_error_l20_c20 = [ &sf_l20_c20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20_c20.photon_SF_uncertainty_linear( photon_pt, photon_eta[0] ); };
  auto sf_photon_upper_l20_c20 = [ &sf_l20_c20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20_c20.photon_SF_upper_linear_central( photon_pt, photon_eta[0] ); };
  auto sf_photon_lower_l20_c20 = [ &sf_l20_c20 ]( const float & photon_pt, const ROOT::RVecF &photon_eta  ){ return (float) sf_l20_c20.photon_SF_lower_linear_central( photon_pt, photon_eta[0] ); };
  auto sf_photon_scaling_l20_c20 = [ &sf_l20_c20 ]( const float & photon_pt  ){ return (float) sf_l20_c20.get_central_scaling( photon_pt ); };

  auto fullsf = [ &bphys_sf_tool ]( const ROOT::RVecF & mu_pl_pT, const ROOT::RVecF & mu_pl_eta, const ROOT::RVecF & mu_mi_pT, const ROOT::RVecF & mu_mi_eta,
                                    const ROOT::RVecF & dR, const ROOT::RVecF & y ){ 
    return (float) bphys_sf_tool->getFullSF( Bphys::DATASET::Data15, Bphys::DIMUTRIGLEGS::dimutriglegs_2mu4, Bphys::DIMUTRIG::bDimu, mu_pl_pT[0], 1.0*mu_pl_eta[0], mu_mi_pT[0], -1.0*mu_mi_eta[0], dR[0], y[0] );
  };

  auto fullsf_syst = [ &bphys_sf_tool ]( const ROOT::RVecF & mu_pl_pT, const ROOT::RVecF & mu_mi_pT, const ROOT::RVecF & dR, const ROOT::RVecF & y ){ 
    return (float) bphys_sf_tool->getSFSyst( Bphys::DATASET::Data15, Bphys::DIMUTRIGLEGS::dimutriglegs_2mu4, Bphys::DIMUTRIG::bDimu, mu_pl_pT[0],  mu_mi_pT[0], dR[0], y[0] );
  };
  auto fullsf_samp = [ &bphys_sf_tool ]( const ROOT::RVecF & mu_pl_pT, const ROOT::RVecF & mu_pl_eta, const ROOT::RVecF & mu_mi_pT, const ROOT::RVecF & mu_mi_eta,
                                    const ROOT::RVecF & dR, const ROOT::RVecF & y ){ 
    return bphys_sf_tool->getSampledSFs( Bphys::DATASET::Data15, Bphys::DIMUTRIGLEGS::dimutriglegs_2mu4, Bphys::DIMUTRIG::bDimu, mu_pl_pT[0], 1.0*mu_pl_eta[0], mu_mi_pT[0], -1.0*mu_mi_eta[0], dR[0], y[0] );
  };

  
	// Ready the input and output
  TChain chain( "tree" );
  std::string filepath = std::string( input_filepath ) + "/" + std::string( type_c[type] )  + "_" + input_unique + ".root";
  chain.Add( filepath.c_str() );
  ROOT::RDataFrame input_frame( chain );

  const char * region_cut = ( region == NR ) ? "1.0" : region_cuts[region];
  auto filtered = input_frame.Filter( region_cut );

  // quite a messy process of assigning all those exploartory scalefactory branches
  filtered = filtered.Define( "photon_mev", "Float_t ( PhotonPt*1000.0 )");
  filtered = filtered.Define( "photon_sf",              sf_photon, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf",        sf_photon_upper, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf",        sf_photon_lower, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err",          sf_photon_error, { "photon_mev","Phot_Eta" } );

  filtered = filtered.Define( "photon_sf_rec",           sf_photon_rec, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_rec",     sf_photon_upper_rec, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_rec",     sf_photon_lower_rec, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_rec",       sf_photon_error_rec, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_rec_usable", sf_photon_upper_rec_usable, { "photon_mev", "Phot_Eta" } );

  filtered = filtered.Define( "photon_sf_db",           sf_photon_db, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_db",     sf_photon_upper_db, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_db",     sf_photon_lower_db, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_db",       sf_photon_error_db, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_bloom_db",        sf_photon_bloom_db, { "photon_mev", "Phot_Eta" } );

  filtered = filtered.Define( "photon_sf_qb",           sf_photon_qb, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_qb",     sf_photon_upper_qb, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_qb",     sf_photon_lower_qb, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_qb",       sf_photon_error_qb, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_bloom_qb",        sf_photon_bloom_qb, { "photon_mev", "Phot_Eta" } );

  filtered = filtered.Define( "photon_sf_qb_lt",        sf_photon_qb_lt, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_qb_lt",  sf_photon_upper_qb_lt, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_qb_lt",  sf_photon_lower_qb_lt, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_qb_lt",    sf_photon_error_qb_lt, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_bloom_qb_lt",     sf_photon_bloom_qb_lt, { "photon_mev", "Phot_Eta" } );

  filtered = filtered.Define( "photon_sf_l10",           sf_photon_l10, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_l10",     sf_photon_upper_l10, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_l10",     sf_photon_lower_l10, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_l10",       sf_photon_error_l10, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_bloom_l10",        sf_photon_scaling_l10, { "photon_mev" } );

  filtered = filtered.Define( "photon_sf_l20",           sf_photon_l20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_l20",     sf_photon_upper_l20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_l20",     sf_photon_lower_l20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_l20",       sf_photon_error_l20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_bloom_l20",        sf_photon_scaling_l20, { "photon_mev" } );

  filtered = filtered.Define( "photon_sf_l30",           sf_photon_l30, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_l30",     sf_photon_upper_l30, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_l30",     sf_photon_lower_l30, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_l30",       sf_photon_error_l30, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_bloom_l30",        sf_photon_scaling_l30, { "photon_mev" } );

  filtered = filtered.Define( "photon_sf_l20_c20",           sf_photon_l20_c20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_upper_sf_l20_c20",     sf_photon_upper_l20_c20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_lower_sf_l20_c20",     sf_photon_lower_l20_c20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_err_l20_c20",       sf_photon_error_l20_c20, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_bloom_l20_c20",        sf_photon_scaling_l20_c20, { "photon_mev" } );

  filtered = filtered.Define( "photon_sf_step",           sf_photon_step, { "photon_mev", "Phot_Eta" } );


  filtered = filtered.Define( "photon_sf_extended",             sf_photon_extended, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_extended_upper",       sf_photon_extended_upper, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_extended_risk",             sf_photon_extended_risk, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_extended_risk_upper",       sf_photon_extended_risk_upper, { "photon_mev", "Phot_Eta" } );

  filtered = filtered.Define( "photon_sf_rec_50",       sf_photon_upper_rec_50, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_rec_25",       sf_photon_upper_rec_25, { "photon_mev", "Phot_Eta" } );
  filtered = filtered.Define( "photon_sf_rec_10",       sf_photon_upper_rec_10, { "photon_mev", "Phot_Eta" } );


  filtered = filtered.Define( "muon_sf",                fullsf, { "MuPos_Pt", "MuPos_Eta", "MuNeg_Pt", "MuNeg_Eta", "DiLept_DeltaR", "DiLept_Rap" } ); 
  filtered = filtered.Define( "muon_sf_samples",        fullsf_samp, { "MuPos_Pt", "MuPos_Eta", "MuNeg_Pt", "MuNeg_Eta", "DiLept_DeltaR", "DiLept_Rap" } );
  filtered = filtered.Define( "muon_sf_systematic",     fullsf_syst, { "MuPos_Pt", "MuNeg_Pt", "DiLept_DeltaR", "DiLept_Rap" } ); 

  std::vector<TH1F *> weight_hists = {};

  // this is for the validation process, also should be a little cleaner
  bool valid_type = ( type == sample_type::sign || type == sample_type::bckg );

  if ( valid_type && !validation_unique.empty() ){

    // retrieve the validation graphs
    std::string ratio_path = std::string( std::getenv( "OUT_PATH" ) ) + "/lh_summary/validate/"
                          + validation_unique + "/ratios.root";
    TFile * weight_file = new TFile( ratio_path.c_str(), "READ" ); 
    TH1F * jpsi_weights = (TH1F *) weight_file->Get( "DiMuonPt" );
    TH1F * phot_weights = (TH1F *) weight_file->Get( "PhotonPt" );
    TH1F * dphi_weights = (TH1F *) weight_file->Get( "DPhi" );
    TH1F * dy_weights = (TH1F *) weight_file->Get( "DY" );


    weight_hists.push_back( jpsi_weights ); 
    weight_hists.push_back( phot_weights );
    weight_hists.push_back( dphi_weights );
    weight_hists.push_back( dy_weights   ); 


    // create reweighting functions
    auto jpsi_weight = [ &jpsi_weights, &valid_type ]( const ROOT::RVecF & jpsi_pt ){ 
      return valid_type ? jpsi_weights->GetBinContent( jpsi_weights->FindBin( (double_t) jpsi_pt[0] ) ) : 1.0;
    };
    auto phot_weight = [ &phot_weights, &valid_type ]( const ROOT::RVecF & phot_pt ){ 
      return valid_type ? phot_weights->GetBinContent( phot_weights->FindBin( (double_t) phot_pt[0] ) ) : 1.0 ;
    };

    auto dphi_weight = [ &dphi_weights, &valid_type ]( const ROOT::RVecF & dphi ){ 
      return valid_type ? dphi_weights->GetBinContent( dphi_weights->FindBin( (double_t) dphi[0] ) ) : 1.0 ;
    };

    auto dy_weight = [ &dy_weights, &valid_type ]( const ROOT::RVecF & dy ){ 
      return valid_type ? dy_weights->GetBinContent( dy_weights->FindBin( (double_t) dy[0] ) ) : 1.0 ;
    };

    auto absdphi_weight = [ &dphi_weights, &valid_type ]( const ROOT::RVecF & absdphi ){ 
      return valid_type ? dphi_weights->GetBinContent( dphi_weights->FindBin( (double_t) absdphi[0] ) ) : 1.0 ;
    };
  
    // create variable photonpt reweight options
    std::vector< TH1F * > weight_hists = {};
    std::vector< std::pair< std::string, std::function< Double_t( const ROOT::RVecF & ) > > > photon_pt_weights = {};
    std::vector< std::string > photon_keys;
    TList * keylist = weight_file->GetListOfKeys();
    for ( auto key : *keylist ){ 
      std::string key_str = std::string( key->GetName() );
      if ( key_str.find( "PhotonPt_" ) == std::string::npos ){ continue; }
      TH1F * weight_hist = static_cast< TH1F * >( weight_file->Get( key_str.c_str() ) );
      std::string branch_name = "phot_weight" +  key_str.substr( key_str.find( '_' ) );
      photon_pt_weights.push_back( std::make_pair< std::string, std::function< Double_t( const ROOT::RVecF & ) >>( branch_name.c_str(),
        [ weight_hist ]( const ROOT::RVecF & phot_pt ){
          return weight_hist->GetBinContent( weight_hist->FindBin( (double_t) phot_pt[0] ) );
        }
       )
      );
    }

    // create weight branches
    filtered = filtered.Define( "jpsi_weight",            jpsi_weight,    { "DiMuonPt" } );
    filtered = filtered.Define( "phot_weight",            phot_weight,    { "PhotonPt" } ); 
    filtered = filtered.Define( "dphi_weight",            dphi_weight,    { "DPhi" } );
    filtered = filtered.Define( "absdphi_weight",         absdphi_weight, { "AbsdPhi" } );
    filtered = filtered.Define( "dy_weight",              dy_weight,      { "DY" } ); 
    for ( auto define_params : photon_pt_weights ){
      std::cout << define_params.first << std::endl;
      filtered = filtered.Define( define_params.first.c_str(), define_params.second, { "PhotonPt" } );
    }


  }  

  /*
  I figured out the faff here hold on
  the leaked memory isn't safe from the loop, sticking something after it shits the pant
  push it back, reference the pushed back element
  that should give you the consistency to avoid the segfault 
  */
  std::string cw_path = std::string( std::getenv( "OUT_PATH" ) ) 
    + "/int_note/jpsipt/cwf_DiMuonPt.root";
  TFile * jpsi_weight_file = new TFile( cw_path.c_str(), "READ" ); 
  TH1F * jpsi_weight_hist = static_cast<TH1F *>( jpsi_weight_file->Get( "cw" ) );
  auto internal_jpsi_weight = [ &jpsi_weight_hist, &valid_type ](const ROOT::RVecF & jpsi_pt ){ 
    float cw = jpsi_weight_hist->GetBinContent( jpsi_weight_hist->FindBin( jpsi_pt[0] ) );
    return valid_type ? cw : 1.0 ;
  };
  filtered = filtered.Define( "DiMuonPt_Int", internal_jpsi_weight, { "DiMuonPt" } );
  

  std::vector<TH1F *> photon_sf_closure_hists = {};
  photon_sf_closure_hists.reserve( closure_uniques.size());

  for ( std::string closure_unique : closure_uniques ){
    
    std::string closure_name = "";
    std::string cw_path = std::string( std::getenv( "OUT_PATH" ) ) + "/lh_summary/validate/"
                        + closure_unique + "/cwf_PhotonPt.root";
    TFile * weight_file = new TFile( cw_path.c_str(), "READ" ); 
    TH1F * closure_weight_hist = static_cast<TH1F *>( weight_file->Get( "cw" ) );
    photon_sf_closure_hists.push_back( static_cast<TH1F *>( closure_weight_hist->Clone( "cwm" ) ) );
    TH1F * mem_hist = photon_sf_closure_hists.back();

    auto closure_weight_lowpt = [ &mem_hist, &valid_type ]( const float & photon_pt ){ 
      float cw = ( photon_pt < 10000.0 ) ? ( mem_hist->GetBinContent( mem_hist->FindBin( (double_t) photon_pt/1000.0 ) )) : 1.0;
      return valid_type ? cw : 1.0 ;
    };
    closure_name = "closure_weight_lowpt_" + closure_unique;
    filtered = filtered.Define( closure_name.c_str(), closure_weight_lowpt, { "photon_mev" } );
    
    auto closure_weight_lowpt_upper = [ &mem_hist, &valid_type ]( const float & photon_pt ){ 
      float cw = 1.0;
      if ( photon_pt < 10000.0 ){
        int bin = mem_hist->FindBin( (double_t) photon_pt/1000.0 );
        cw = mem_hist->GetBinContent( bin ) + mem_hist->GetBinError( bin );
      }
      return valid_type ? cw : 1.0 ;
    };
    closure_name = "closure_weight_lowpt_" + closure_unique + "_upper";
    filtered = filtered.Define( closure_name.c_str(), closure_weight_lowpt_upper, { "photon_mev" } );

    auto closure_weight = [ &mem_hist, &valid_type ]( const float & photon_pt ){ 
      float cw = mem_hist->GetBinContent( mem_hist->FindBin( (double_t) photon_pt/1000.0 ) );
      return valid_type ? cw : 1.0 ;
    };
    closure_name = "closure_weight_" + closure_unique;
    filtered = filtered.Define( closure_name.c_str(), closure_weight, { "photon_mev" } );


    auto closure_weight_upper = [ &mem_hist, &valid_type ]( const float & photon_pt ){ 
      int bin = mem_hist->FindBin( (double_t) photon_pt/1000.0 );
      double cw = mem_hist->GetBinContent( bin ) + mem_hist->GetBinError( bin );
      return valid_type ? cw : 1.0 ;
    }; 
    closure_name = "closure_weight_" + closure_unique + "_upper";
    filtered = filtered.Define( closure_name.c_str(), closure_weight_upper, { "photon_mev" } );
    
  }

  if ( closure_internal ){ std::cout << "closures made\n" << std::flush;}
  

	// output the filtered tree
  std::string output_file = "./" + std::string( type_c[type] ) + "/" + type_c[type] + "_" + unique;
  std::string main_output_file = output_file + ".root";
  filtered.Snapshot( "tree", main_output_file, "" );
  

  // for the old control and signal region processes, 
  // snapshot the limited trees associated with those areas.
  if ( region == NR ){

    auto filtered_sr = filtered.Filter( region_cuts[ SR ] );
    auto filtered_cr = filtered.Filter( region_cuts[ CR ] );
    std::string output_sr_file = output_file + "_sr.root";
    std::string output_cr_file = output_file + "_cr.root";
    filtered_sr.Snapshot( "tree", output_sr_file, "" );
    filtered_cr.Snapshot( "tree", output_cr_file, "" );

  } else if ( region == NRA ){
 
    auto filtered_sr = filtered.Filter( region_cuts[ SRA ] );
    auto filtered_cr = filtered.Filter( region_cuts[ CRA ] );
    std::string output_sr_file = output_file + "_sra.root";
    std::string output_cr_file = output_file + "_cra.root";
    filtered_sr.Snapshot( "tree", output_sr_file, "" );
    filtered_cr.Snapshot( "tree", output_cr_file, "" );

  }

}

//filtered = filtered.Define( "mu_pl_sf_upper",                               sf_mu_upper, { "MuPlus_Pt", "MuPlus_Eta" } );      
//filtered = filtered.Define( "mu_mi_sf_upper",                               sf_mu_upper, { "MuMinus_Pt", "MuMinus_Eta" } );      
//filtered = filtered.Define( "mu_pl_sf_lower",                               sf_mu_lower, { "MuPlus_Pt", "MuPlus_Eta" } );      
//filtered = filtered.Define( "mu_mi_sf_lower",                               sf_mu_lower, { "MuMinus_Pt", "MuMinus_Eta" } );      

//auto sf_mu_upper = [ &sf ]( const ROOT::RVecF & mu_pt, const ROOT::RVecF & mu_eta ){ 
//  return (float) ( sf.single_muon_SF( mu_pt[0]*1e-3, +1.0*mu_eta[0]*1e-3, dataset::Data15, single_mu_leg::mu4) + sf.single_muon_SF_error( mu_pt[0]*1e-3, +1.0*mu_eta[0]*1e-3, dataset::Data15, single_mu_leg::mu4) ); };
//auto sf_mu_lower = [ &sf ]( const ROOT::RVecF & mu_pt, const ROOT::RVecF & mu_eta ){ 
//  return (float) ( sf.single_muon_SF( mu_pt[0]*1e-3, +1.0*mu_eta[0]*1e-3, dataset::Data15, single_mu_leg::mu4) - sf.single_muon_SF_error( mu_pt[0]*1e-3, +1.0*mu_eta[0]*1e-3, dataset::Data15, single_mu_leg::mu4) ); };
//auto sf_mu = [ &sf ]( const ROOT::RVecF & mu_pt, const ROOT::RVecF & mu_eta ){ return (float) sf.single_muon_SF( mu_pt[0]*1e-3, +1.0*mu_eta[0]*1e-3, dataset::Data15, single_mu_leg::mu4); };
////auto sf_mu_dr = [ &sf ]( const ROOT::RVecF & dr ){ return (float) sf.muon_dr_SF ( dr[0], 0.0, dataset::Data15, dimu_trig::bDimu_noL2) ; };

