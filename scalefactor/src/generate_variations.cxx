#include <generate_variations.hxx>
#include <thread>

std::mutex mutex;

void prepare_sf_hists ( bound & spec_bound, const std::string & analysis_bin, std::vector< TH1F> & hists ){
  int hist_index = 0;
  int start = hists.size();
  std::generate_n( std::back_inserter( hists ), 100, [&hist_index, &spec_bound, analysis_bin ](){ 
      std::string hist_name = analysis_bin + "_v" + std::to_string( hist_index );
      TH1F h( hist_name.c_str(), hist_name.c_str(), 50, spec_bound.get_min(), spec_bound.get_max() ); 
      hist_index++;
      return h;
    } );
  std::for_each( hists.begin() + start, hists.end(), []( TH1F hist ){ hist.Sumw2( kTRUE ); } );  
}

void sf_mt_draw( TTree * tree, TH1F * hist, const std::string & draw_expr, const std::string & cut_expr, std::vector< TH1F * > & hists ){

  tree->Draw( draw_expr.c_str(), cut_expr.c_str(), "e goff" );
  mutex.lock();
  hists.push_back( hist ); 
  mutex.unlock();
};

void sf_mt_str_draw( const std::string & filepath, const std::string & hist_name, const std::string & draw_expr, const std::string & cut_expr, std::vector< TH1F > & hists ){


  TFile * file = new TFile( filepath.c_str(), "READ" );
  TTree * tree = static_cast<TTree *>( file->Get( "tree" ) );
  TH1F hist( hist_name.c_str(), hist_name.c_str(), 50, -1.0, 1.0 );  
  tree->Draw( draw_expr.c_str(), cut_expr.c_str(), "e goff" );
  mutex.lock();
  hists.push_back( hist ); 
  mutex.unlock();
  delete tree;
  file->Close();


};


void generate_variations( const std::string & sf_filepath, variable_set * variables, const char * unique, sample_type type, region region ){

  bound & analysis_bound = variables->analysis_bound;
  bound & spectator_bound = variables->spectator_bound;
  bound & mass_bound = variables->bound_manager->get_bound( "Q12" );
  bound & qtb_bound = variables->bound_manager->get_bound( "qtB" );

  bool mt = true;
  std::vector< std::thread > threads;

  std::vector< std::string > analysis_cut_series = analysis_bound.get_cut_series();
  std::vector< std::string > analysis_cut_names = analysis_bound.get_series_names();
  char mass_qtb[200]; sprintf( mass_qtb, "(%s&&%s)", mass_bound.get_cut().c_str(), qtb_bound.get_cut().c_str() );
  std::vector< TH1F > output_histograms;
  output_histograms.reserve( analysis_bound.get_bins()*101 ); 
  std::vector< TH1F * > other_output_histograms;

  TFile * sf_file = new TFile( sf_filepath.c_str(), "READ" );
  TTree * sf_tree = static_cast< TTree * >( sf_file->Get( "tree" ) );  
  if ( !sf_tree  ){ throw std::invalid_argument( "No sf tree present" ); }


  //for ( int analysis_bin = 0; analysis_bin < analysis_bound.get_bins(); analysis_bin++ ){

    if ( !mt ){


      for ( int analysis_bin = 0; analysis_bin < analysis_bound.get_bins(); analysis_bin++ ){
      //for ( int analysis_bin = 0; analysis_bin < 1; analysis_bin++ ){

        std::cout << "Processing analysis_bin " << analysis_bin << std::endl;
        prepare_sf_hists( spectator_bound, analysis_cut_names[analysis_bin].c_str(), output_histograms ); 
        TFile * nmt_sf_file = new TFile( sf_filepath.c_str(), "READ" );
        TTree * nmt_sf_tree = static_cast< TTree * >( nmt_sf_file->Get( "tree" ) );  
        if ( !nmt_sf_tree  ){ throw std::invalid_argument( "No sf tree present" ); }
        for ( int hist = 0; hist < 100; hist++ ){
          char var_draw_expr[50]; sprintf( var_draw_expr, "BDT>>%s_v%i", analysis_cut_names[analysis_bin].c_str(), hist ); 
          char var_cut_expr[300]; sprintf( var_cut_expr, "subtraction_weight*photon_sf*muon_sf_samples[%i]*(%s&&%s)", hist, analysis_cut_series[analysis_bin].c_str(), mass_qtb );
          std::cout << var_draw_expr << " " << var_cut_expr << std::endl;
          nmt_sf_tree->Draw( var_draw_expr, var_cut_expr, "goff e" );
        }

        TH1F * bdt_hist = spectator_bound.get_hist( analysis_cut_names[ analysis_bin ], 50 );
        bdt_hist->Sumw2( kTRUE );
        char sf_draw_expr[30]; sprintf( sf_draw_expr, "BDT>>%s", bdt_hist->GetName() ); 
        char sf_cut_expr[300]; sprintf( sf_cut_expr, "subtraction_weight*muon_sf*photon_sf*(%s&&%s)", analysis_cut_series[analysis_bin].c_str(), mass_qtb ); 
        nmt_sf_tree->Draw( sf_draw_expr, sf_cut_expr, "goff e" );

        TH1F * syst_hist = spectator_bound.get_hist( analysis_cut_names[ analysis_bin ] + "_syst_upper", 50 );
        syst_hist->Sumw2( kTRUE );
        std::cout << syst_hist->GetName() << std::endl;
        sprintf( sf_draw_expr, "BDT>>%s", syst_hist->GetName() ); 
        sprintf( sf_cut_expr, "(subtraction_weight*muon_sf*photon_sf)*(1.0 + muon_sf_systematic)*(%s&&%s)", analysis_cut_series[analysis_bin].c_str(), mass_qtb ); 
        nmt_sf_tree->Draw( sf_draw_expr, sf_cut_expr, "goff e" );

        TH1F * rel_syst_hist = spectator_bound.get_hist( analysis_cut_names[ analysis_bin ] + "_syst", 50 ); 

        other_output_histograms.push_back( rel_syst_hist );
        other_output_histograms.push_back( bdt_hist );

      }


    } else {

      for ( int analysis_bin = 0; analysis_bin < analysis_bound.get_bins(); analysis_bin++ ){
        std::cout << "Processing analysis_bin " << analysis_bin << std::endl;

        int thread_spawn = 0;
        for ( int hist = 0; hist < 100; hist++ ){
          char var_draw_expr[50]; sprintf( var_draw_expr, "BDT>>%s_v%i", analysis_cut_names[analysis_bin].c_str(), hist ); 
          char var_cut_expr[300]; sprintf( var_cut_expr, "subtraction_weight*photon_sf*muon_sf_samples[%i]*(%s&&%s)", hist, analysis_cut_series[analysis_bin].c_str(), mass_qtb );
          
          std::string hist_name = analysis_cut_names[analysis_bin] + "_v" + std::to_string( hist );
          threads.push_back( std::thread( sf_mt_str_draw, 
                std::string( sf_filepath ), 
                std::string( hist_name ),
                std::string( var_draw_expr ), 
                std::string( var_cut_expr ),
                std::ref( output_histograms )
              ) );

          thread_spawn++;
          if ( thread_spawn >= 10 ){
            for ( std::thread & thread : threads ){
              if ( thread.joinable() ){ thread.join(); }
            }
            thread_spawn = 0;
            threads.clear();
          }
        }
 
        TH1F bdt_hist( analysis_cut_names[ analysis_bin ].c_str(), "", 50, -1.0, 1.0 );
        bdt_hist.Sumw2( kTRUE );
        char sf_draw_expr[30]; sprintf( sf_draw_expr, "BDT>>%s", bdt_hist.GetName() ); 
        char sf_cut_expr[300]; sprintf( sf_cut_expr, "subtraction_weight*photon_sf*muon_sf*(%s&&%s)", analysis_cut_series[analysis_bin].c_str(), mass_qtb ); 
        sf_tree->Draw( sf_draw_expr, sf_cut_expr, "goff e" );

        std::string syst_name = analysis_cut_names[ analysis_bin ] + "_syst_upper";
        TH1F syst_hist( syst_name.c_str(), syst_name.c_str(), 50, -1.0, 1.0 );
        syst_hist.Sumw2( kTRUE );
        sprintf( sf_draw_expr, "BDT>>%s", syst_hist.GetName() ); 
        sprintf( sf_cut_expr, "(subtraction_weight*muon_sf*photon_sf)*(1.0 + muon_sf_systematic)*(%s&&%s)", analysis_cut_series[analysis_bin].c_str(), mass_qtb ); 
        sf_tree->Draw( sf_draw_expr, sf_cut_expr, "goff e" );

        std::string rel_syst_name = analysis_cut_names[ analysis_bin ] + "_syst";
        TH1F rel_syst_hist( rel_syst_name.c_str(), "", 50, -1.0, 1.0 );
        rel_syst_hist.Add( &syst_hist, &bdt_hist, 1.0, -1.0 );

        output_histograms.push_back( rel_syst_hist );
        output_histograms.push_back( bdt_hist );
      
    }     

  }

  char output_filepath[200]; sprintf( output_filepath, "./variations_%s_%s_%s.root", unique, type_c[type], region_c[region] );
  TFile * output_file = new TFile( output_filepath, "RECREATE" );
  output_file->cd();
  for ( TH1F & hist : output_histograms ){ 
    output_file->cd();
    hist.Write();
  }

  output_file->Close();

  delete sf_tree;
  sf_file->Close();

}

//filtered = filtered.Define( "SFUpper",                                      "Float_t( SF*( 1.0 + SFSyst ) )" ); 
//filtered = filtered.Define( "SFLower",                                      "Float_t( SF*( 1.0 - SFSyst ) )" ); 

//threads.push_back( std::thread( sf_mt_draw, sf_tree, 
//      new TH1F( hist_name.c_str(), hist_name.c_str(), spectator_bound.get_bins(), spectator_bound.get_min(), spectator_bound.get_max() ), 
//      //      std::string( var_draw_expr ), 
        //      std::string( var_cut_expr ), 
        //      std::ref( output_histograms)
        //  ) );
