#include <ntuple_utils.hxx>

//void generate_pt_graphs( basic_fileset * fileset, const char * unique, sample_type type, variable_set * variables );
std::map< std::string, TH1F *> generate_pt_graphs( basic_fileset * fileset, const char * unique, sample_type type, variable_set * variables, bool threaded );
