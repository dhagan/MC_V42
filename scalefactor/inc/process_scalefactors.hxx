#include <anna.hxx>
#include <scalefactor.hxx>
#include <ntuple_utils.hxx>
#include <ROOT/RDataFrame.hxx>

//void process_scalefactors( TFile * variations, variable_set * variables, const char * unique, sample_type type, region region, bool threaded );
std::map< std::string, TH1F *> process_scalefactors( TFile * variations, variable_set * variables, const char * unique, sample_type type, region region, bool threaded );
