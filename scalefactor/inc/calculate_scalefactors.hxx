#include <anna.hxx>

#include <ntuple_utils.hxx>
#include <ROOT/RDataFrame.hxx>
#include <BphysEffTool.h>

void calculate_scalefactors( const char * input_filepath, const char * unique, const char * input_unique,
    sample_type type, region=NR, const std::string & validation_unique="", const std::vector<std::string> & closure_uniques={},
    bool closure_internal = false );
