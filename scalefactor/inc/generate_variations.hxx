#include <common.hxx>
#include <bound_mgr.hxx>
#include <cutflow.hxx>
#include <fileset.hxx>
#include <scalefactor.hxx>
#include <ntuple_utils.hxx>

void generate_variations( const std::string & sf_filepath, variable_set * variables, const char * unique, sample_type type, region region );

