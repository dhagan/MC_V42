# Tree Generation

## Description
This program is for the conversion of trees output from an AthAnalysis algorithm on the grid, to a skimmed and simplified tree for the analysis. The main function of the program is the generation of trees for signal MC (sign), pp background MC(bckg), data, and the bb background MC (bbbg). Along with this there is options to produce efficiency plots, reweight these efficiencies by certain other variables, to generate the weights used for this reweighting, and to produce trees for visualising bin migration. For signal MC there also exists the option to output a matching tree for the truth MC, instead of the standard reco.

## Usage
In general, scripts have been written to manage the generation of trees along with a good number of the systematic variations on these used in this analysis, these == run scripts are stored in the ./share directory ==. These should be used and committed to a users respective branch in preference to rewriting the commands each time. These scripts also handle the generation of the directory structure used for this program, and ensure that output is stored and labelled properly in the 'out' directory at the root of the analysis package's install.
```
Usage:
./make_ntuple --input,-i INPUT_FILE(s) --type,-t TYPE --unique,-u UNIQUE --selections,-v SELECTIONS \
						  [ --efficiency,-e ] [ --make_weights,-k ] [ --reco,-o ] [ --weight_var,-w WEIGHTING_VARIABLE ] \
						  [ --truth,-s ]  [ --ranges,-r RANGES ] [ --migration,-m ] [ --help,-h ]

Required arguments;
  --input,-i            Files to be processed, modes requiring multiple files should delimit filepaths with a colon ":".      
  --type,-t             Type of input file, necessary as processing does differ between the 4 file types. Signal MC has 
                        some truth dependent cuts. Information for these cuts is generally not available in other file 
                        types, nor is it physically sensible to apply. Accepts sign, bckg, data, and bbbg.
  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is 
                        simply for bookkeeping.
  --selections,-v       A filepath to a selections file. This holds the information of all the variables binning and 
                        ranges, along with the cuts applied thorughout this program. This should be located in the 
                        share directory of the analysis library and commited to that repository for record. It is
                        reasonable to write extra selection files into the analysis util library to keep systematics
                        distinct.


Processing modes;
  --reco,-o             Produce the standard reco tree, works for all types. 
  --truth,-s            Produce a truth tree, this will only operate with "sign" type. 
  --make_weights,-k     Produce histograms for the reweighting. Requires data and sign input; "data_path:sign_path".
  --efficiency,-e       Produce efficiency graphs, only operates for "sign" type.
  --migration,-m        Produce migration trees. These should be provided to the dedicated "migration" program.


Optional arguements;
  --ranges, -r          A string that will be processed into a bound for a variable, can be used to replace or augment
                        any variable definition already supplied by the selections file as a method of smaller and 
                        faster systematic variations.
  --weight_var,-w       Variable to be used for efficiency reweighting.

```
Some options such as efficiencies require more than one file to process, give these as part of the input argument delimited by a colon ':'. 
