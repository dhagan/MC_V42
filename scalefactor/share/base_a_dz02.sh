unique="base_a_dz02"
yaml="$(pwd)/${unique}.yaml"
files=( "sign" "bckg" "data" )

mkdir -p ${OUT_PATH}/scalefactors/${unique}
mkdir -p ${OUT_PATH}/scalefactors/${unique}/plots
pushd ${OUT_PATH}/scalefactors/${unique} >> /dev/null

for file_index  in {0..2};
do
	file="${files[$file_index]}"
	mkdir -p ${file}
	echo ${file}
done
scalefactors -i ${yaml}

popd >> /dev/null




##for file_index  in {0..1};
##do
##	file="${files[$file_index]}"
##	mkdir -p ${file}
##	subtraction_filepath="${OUT_PATH}/event_subtraction/${unique}/${file}_${unique}.root"
##	allsub_path="${OUT_PATH}/event_subtraction/${unique}/"
##	${executable} -b ${allsub_path} -t ${file} -u ${unique} -v ${selections} -w
##	${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -c -n "sr"
##	${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -c -n "cr"
##done


##for file_index  in {1..1};
##do
##	file="${files[$file_index]}"
##	mkdir -p ${file}
##	subtraction_filepath="${OUT_PATH}/event_subtraction/${unique}/${file}_${unique}.root"
##	##${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -s -n "sr"
##	${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -c -n "cr"
##done

#popd >> /dev/null
