unique="mini"
yaml="$(pwd)/${unique}.yaml"
files=( "sign" "bckg" "data" )

mkdir -p ${OUT_PATH}/scalefactors/${unique}
mkdir -p ${OUT_PATH}/scalefactors/${unique}/plots
pushd ${OUT_PATH}/scalefactors/${unique} >> /dev/null

for file_index  in {0..2};
do
	file="${files[$file_index]}"
	mkdir -p ${file}
done
scalefactors -i ${yaml}

popd >> /dev/null
