
unique="sub_sys"
yaml="$(pwd)/${unique}.yaml"
mkdir -p ${OUT_PATH}/scalefactors/

pushd ${OUT_PATH}/scalefactors/ >> /dev/null
scalefactors -i ${yaml}
popd >> /dev/null
