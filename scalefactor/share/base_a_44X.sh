
uniques=( "base_a_446" "base_a_447" "base_a_448" "base_a_449" "base_a_4410" )
files=( "sign" "bckg" "data" )

for unique_index in {0..4};
do
	unique="${uniques[$unique_index]}"

	yaml="$(pwd)/${unique}.yaml"
	mkdir -p ${OUT_PATH}/scalefactors/${unique}
	mkdir -p ${OUT_PATH}/scalefactors/${unique}/plots
	pushd ${OUT_PATH}/scalefactors/${unique} >> /dev/null

	echo ${unique}

	for file_index in {0..2};
	do
		file="${files[$file_index]}"
		mkdir -p ${file}
		echo ${file}
	done
	scalefactors -i ${yaml}

	popd >> /dev/null

done

popd >> /dev/null
