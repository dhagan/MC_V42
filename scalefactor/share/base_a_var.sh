
uniques=( "base_a_aaf" "base_a_afa" "base_a_faa" "base_a_noqt2Disc" "base_a_noL" "base_a_aaDPDY" "base_a_faDPDY" "base_a_ffDPDY" )
files=( "sign" "bckg" "data" )

for unique_index in {4..4};
do
	unique="${uniques[$unique_index]}"

	yaml="$(pwd)/${unique}.yaml"
	mkdir -p ${OUT_PATH}/scalefactors/${unique}
	mkdir -p ${OUT_PATH}/scalefactors/${unique}/plots
	pushd ${OUT_PATH}/scalefactors/${unique} >> /dev/null

	echo ${unique}

	for file_index in {0..2};
	do
		file="${files[$file_index]}"
		mkdir -p ${file}
		echo ${file}
	done
	scalefactors -i ${yaml}

	popd >> /dev/null

done

popd >> /dev/null

