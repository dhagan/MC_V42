#include <func_hf.hxx>

#ifndef fast_hf_hxx
#define fast_hf_hxx

void fast_hf( std::string & input_file, std::string & abin_var, std::string & spec_var,
         std::string & cuts, std::string & unique, bound_mgr * selections );

#endif
