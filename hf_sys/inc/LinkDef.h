#ifdef __MAKECINT__

#include <hf.hxx>

#pragma extra_include "hf.hxx";
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class fit_results+;

#endif 

