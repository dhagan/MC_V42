
#ifndef func_hf_hxx
#define func_hf_hxx

#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "RooCategory.h"
#include "Roo1DTable.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCBShape.h"
#include "RooAbsReal.h"
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooPolynomial.h"
#include "RooDataHist.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include <ROOT/RDataFrame.hxx>

#include <anna.hxx>

#include <TObject.h>

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;

struct measure{
  Measurement measure;
  RooStats::HistFactory::Channel channel;
  RooStats::HistFactory::Sample sign, bckg;
  RooWorkspace * wkspc;
  RooAbsPdf * pdf;
  RooAbsData * data;
};

measure produce_measurement( TH1F * sign, TH1F * bckg, TH1F * data );
TH1F * output_histogram( RooFitResult * results, TH1F * sign, TH1F * bckg, TH1F * data, std::string & output_string, fit_results & fit_stats );

#endif
