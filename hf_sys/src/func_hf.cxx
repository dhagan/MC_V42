#include <func_hf.hxx>

typedef struct syspaths{
  std::string photon

} syspaths;

enum sys_types{
  mu_sf, photon_sf, :
}

void generate_sys_histograms( syspaths & systematics ){


}


measure produce_measurement( TH1F * sign, TH1F * bckg, TH1F * data, syspaths & systematics ){
  
  Measurement measurement( "measurement","measurement");
  measurement.SetOutputFilePrefix( "./results/example_UsingC" );
  measurement.SetPOI( "mu" ); 
  measurement.AddConstantParam( "Lumi" );
  measurement.SetLumi( 1.0 );
  measurement.SetExportOnly( true );
    
  // Create a channel
  RooStats::HistFactory::Channel chan( "channel1" );
  chan.SetData( data );
  chan.SetStatErrorConfig( 0.05, "Poisson" ); //original
  
  // Create the signal sample
  RooStats::HistFactory::Sample signal( "sign" );
  signal.SetHisto( sign );
  signal.AddNormFactor( "mu", 0.2, 0.0, 5.0 );
  signal.SetNormalizeByTheory( true );
  chan.AddSample( signal );
  
  // Background
  RooStats::HistFactory::Sample background( "bckg" );
  background.SetHisto( bckg );
  background.AddNormFactor( "ScalingPP", 2.0, 0, 10.0 );
  background.SetNormalizeByTheory( true );
  chan.AddSample( background );
  
  measurement.AddChannel( chan );
  RooWorkspace * workspace = RooStats::HistFactory::MakeModelAndMeasurementFast( measurement );
  RooAbsPdf * pd = workspace->pdf( "simPdf" );
  RooAbsData * d = workspace->data( "obsData" );

  measure complete_measure;
  complete_measure.measure = measurement;
  complete_measure.channel = chan;
  complete_measure.sign = signal;
  complete_measure.bckg = background;
  complete_measure.wkspc = workspace;
  complete_measure.pdf = pd;
  complete_measure.data = d;

  return complete_measure;
}

TH1F * output_histogram( RooFitResult * results, TH1F * sign, TH1F * bckg, TH1F * data, std::string & output_string, fit_results & fit_stats ){


  // scale
  sign->Scale( fit_stats.mu_value );
  bckg->Scale( fit_stats.spp_value );
  TH1F * fit = (TH1F *) sign->Clone( "fit" );
  fit->Reset();
  fit->SetLineColor( kGreen+1 );
  fit->Add( sign );
  fit->Add( bckg );

  TCanvas * result_canvas = new TCanvas( "result_canv", "result_canv", 200, 200, 1000, 1000 );
  result_canvas->Divide( 1 );
   
  TPad * result_pad = (TPad *) result_canvas->cd( 1 ); 

  data->Draw( "E1 P" );
  sign->Draw("HIST E1 SAME");
  bckg->Draw("HIST E1 SAME");
  fit->Draw("HIST E1 SAME");
  hist_prep_axes( data );
  add_atlas_decorations( result_pad, true );
  set_axis_labels( data, "BDT Score", Form("Entries/%.2f", data->GetBinWidth(8) ) );
  add_pad_title( result_pad, Form( "%s", output_string.c_str() ) );
  TLegend * results_legend = below_logo_legend();
  results_legend->AddEntry( data,   "data",       "lep");
  results_legend->AddEntry( sign,   "sign_extr",  "l" );
  results_legend->AddEntry( bckg,   "bckg_extr",  "l" );
  results_legend->AddEntry( fit,    "Fit",        "l" );
  results_legend->Draw();

  auto ltx = new TLatex();
  ltx->SetTextSize( 0.025 );
  ltx->DrawLatexNDC( 0.54, 0.77, Form("n_{sign_extr} = %.0f #pm %0.0f", sign->Integral() ,fit_stats.sign_fit_error ));
  ltx->DrawLatexNDC( 0.54, 0.74, Form("n_{bckg_extr} = %.0f #pm %0.0f", bckg->Integral() ,fit_stats.bckg_fit_error ));
  ltx->DrawLatexNDC( 0.54, 0.71, Form("#mu = %.4f #pm %.4f", fit_stats.mu_value, fit_stats.mu_error));
  ltx->DrawLatexNDC( 0.54, 0.68, Form("SPP = %.4f #pm %0.4f",fit_stats.spp_value, fit_stats.spp_error ) );
  ltx->DrawLatexNDC( 0.54, 0.65, Form("#chi^{2}_{V1}/ndf = %.4f/%i",fit_stats.chi2_v1, fit_stats.dof) );
  ltx->DrawLatexNDC( 0.54, 0.61, Form("#chi^{2}_{V2}/ndf = %.4f/%i",fit_stats.chi2_v2, fit_stats.dof ) );

  if ( results->status() == 0 ){
    result_canvas->SaveAs(Form( "./fit/%s.png", output_string.c_str() ));
  } else if ( results->status() == 4 ){
    TPaveText * warning = new TPaveText( 0.4, 0.5, 0.6, 0.8, "NDC" );
    warning->AddText( "NO CONVERGENCE: MINUIT 4" );
    warning->SetTextFont( 42 );
    warning->SetTextSize( 0.03 );
    warning->SetTextColor( 2 );
    warning->SetFillStyle( 0 );
    warning->SetLineColor( 0 );
    warning->SetLineStyle( 0 );
    warning->SetLineWidth( 0 );
    warning->SetBorderSize( 0 );
    warning->Draw();
    result_canvas->SaveAs( Form( "./fit/%s-FAILED.png", output_string.c_str() ) );
    result_canvas->SaveAs( Form( "./fit_failed/%s-FAILED.png", output_string.c_str() ) );
  }
  delete result_canvas;

  TH1F * error = new TH1F( Form( "errh_%s", output_string.c_str() ), "", 3, 0.0, 3.0);
  error->SetBinContent( 1, 1 );
  error->SetBinError(1, sqrt( data->Integral() ) );
  error->SetBinContent( 2, 1 );
  error->SetBinError( 2, fit_stats.sign_fit_error );
  error->SetBinContent( 3, 1 );
  error->SetBinError( 3, fit_stats.bckg_fit_error );

  return error;

}


