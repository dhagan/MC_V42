#!/bin/bash

executable=${exec_path}/histfactory_fit
selections=${LIB_PATH}/share/hf_bounds.txt

var_sys_list=( "aaf" "afa" "faa" "extd" "noqt2Disc" "noL" "ffDPDY" "aaDPDY" "faDPDY" )

for sys_index in {0..8}; do

	## create specifics
	sys="${var_sys_list[${sys_index}]}"
	input_unique="base_${sys}"
	output_unique="base_${sys}"

	data=${OUT_PATH}/event_subtraction/${input_unique}/data_${input_unique}.root
	sign=${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root
	bckg=${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root

	mkdir -p ${OUT_PATH}/event_hf/${output_unique}
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit_failed
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/eval
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/test
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/df
	
	pushd ${OUT_PATH}/event_hf/${output_unique} >> /dev/null
	
	$executable -i "${data}:${sign}:${bckg}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -f &
	
	popd >> /dev/null



done
