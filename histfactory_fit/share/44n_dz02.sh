#!/bin/bash

executable=${ANA_IP}/event_hf/build/hf

ranges_1="PhotonPt:abs(PhotonPt)&1:5:"
ranges_2="PhotonPt:abs(PhotonPt)&1:6:"
ranges_3="PhotonPt:abs(PhotonPt)&1:7:"
ranges_4="PhotonPt:abs(PhotonPt)&1:8:"
ranges_5="PhotonPt:abs(PhotonPt)&1:9:"

selections=${LIB_PATH}/share/hf_bounds.txt
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" )

for index in {0..4}; do

	## create specifics
	range="${ranges_list[$index]}"
	number=$(($index + 5))
	output_unique="base_44${number}"
	input_unique="base_44${number}"

	data=${OUT_PATH}/event_subtraction/${input_unique}/data_${input_unique}.root
	sign=${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root
	bckg=${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root
	log=${LOG_PATH}/event_hf/event_hf_${output_unique}.txt

	touch ${log}
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit_failed
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/eval
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/test
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/df
	
	pushd ${OUT_PATH}/event_hf/${output_unique} >> /dev/null
	
	$executable -i "${data}:${sign}:${bckg}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -c "PhotonPt" -r "${ranges}" ##2&>1 | tee -a ${log}
	
	popd >> /dev/null
done
