#!/bin/bash

executable=${ANA_IP}/event_hf/build/hf

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"

selections=${LIB_PATH}/share/hf_bounds.txt
ranges_list=( "${ranges_1}" "${ranges_2}" )
uniques_list=( "lower" "upper" )

for index in {0..1}; do

	## create specifics
	range="${ranges_list[$index]}"
	output_unique="qtb_${uniques_list[${index}]}_dz02"
	input_unique="qtb_${uniques_list[${index}]}_dz02"

	data=${OUT_PATH}/event_subtraction/${input_unique}/data_${input_unique}.root
	sign=${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root
	bckg=${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root

	mkdir -p ${OUT_PATH}/event_hf/${output_unique}
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit_failed
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/eval
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/test
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/df
	
	pushd ${OUT_PATH}/event_hf/${output_unique} >> /dev/null
	
	$executable -i "${data}:${sign}:${bckg}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -c "qtB" -r "${ranges}" &
	
	popd >> /dev/null
done
