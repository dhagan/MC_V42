#!/bin/bash

executable=${exec_path}/histfactory_fit
input_unique="base_dz02"
output_unique="base_dz02"
data=${OUT_PATH}/event_subtraction/${input_unique}/data_${input_unique}.root
sign=${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root
bckg=${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root
selections=${LIB_PATH}/share/hf_bounds.txt

mkdir -p ${OUT_PATH}/event_hf/${output_unique}
mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit
mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit_failed
mkdir -p ${OUT_PATH}/event_hf/${output_unique}/eval
mkdir -p ${OUT_PATH}/event_hf/${output_unique}/test
mkdir -p ${OUT_PATH}/event_hf/${output_unique}/df

pushd ${OUT_PATH}/event_hf/${output_unique} >> /dev/null

$executable -i "${data}:${sign}:${bckg}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -f 

popd >> /dev/null
