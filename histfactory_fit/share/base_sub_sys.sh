#!/bin/bash

executable=${exec_path}/histfactory_fit
selections=${LIB_PATH}/share/hf_bounds.txt

sub_sys_list=( "sub_mass1_sys" "sub_tau1_sys" "sub_tau2_sys" "sub_yexp_sys" )

for sys_index in {0..3}; do

	## create specifics
	sys="${sub_sys_list[${sys_index}]}"
	input_unique="base_${sys}"
	output_unique="base_${sys}"

	data=${OUT_PATH}/event_subtraction/${input_unique}/data_${input_unique}.root
	sign=${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root
	bckg=${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root

	mkdir -p ${OUT_PATH}/event_hf/${output_unique}
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit_failed
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/eval
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/test
	mkdir -p ${OUT_PATH}/event_hf/${output_unique}/df
	
	pushd ${OUT_PATH}/event_hf/${output_unique} >> /dev/null
	
	$executable -i "${data}:${sign}:${bckg}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -f
	
	popd >> /dev/null



done
