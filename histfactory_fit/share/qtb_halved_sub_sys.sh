#!/bin/bash

executable=${ANA_IP}/event_hf/build/hf

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
selections=${LIB_PATH}/share/hf_bounds.txt
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )
uniques_list=( "lower" "upper" );
sub_sys_list=( sub_mass1_sys sub_tau1_sys sub_tau2_sys sub_yexp_sys )


for sys in ${sub_sys_list[@]}; do

	for index in {0..1}; do
	
		## create specifics
		range="${ranges_list[$index]}"
		output_unique="qtb_${uniques_list[$index]}_${sys}"
		input_unique="qtb_${uniques_list[$index]}_${sys}"
	
		data=${OUT_PATH}/event_subtraction/${input_unique}/data_${input_unique}.root
		sign=${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root
		bckg=${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root
	
		mkdir -p ${OUT_PATH}/event_hf/${output_unique}
		mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit
		mkdir -p ${OUT_PATH}/event_hf/${output_unique}/fit_failed
		mkdir -p ${OUT_PATH}/event_hf/${output_unique}/eval
		mkdir -p ${OUT_PATH}/event_hf/${output_unique}/test
		mkdir -p ${OUT_PATH}/event_hf/${output_unique}/df
		
		pushd ${OUT_PATH}/event_hf/${output_unique} >> /dev/null
		
		$executable -i "${data}:${sign}:${bckg}" -a "qtA" -s "BDT" -u ${output_unique} -v ${selections} -c "qtB" -r "${ranges}" &
		
		popd >> /dev/null
	done

done
