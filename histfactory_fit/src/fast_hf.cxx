#include "fast_hf.hxx"

void fast_hf( std::string & input, std::string & abin_var, std::string & spec_var, std::string & cuts, 
        std::string & unique, bound_mgr * selections ){

  prep_style();
  gROOT->SetBatch(true);

  std::vector<std::string>  mass_bins = { "Q12" }; 
 
  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int spec_bins = spectator_bound.get_bins();
  float spec_min = spectator_bound.get_min();
  float spec_max = spectator_bound.get_max();

  std::vector<float> data_style = { 21, 1,        1, 1, 1, 1.0,      1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> sign_style = { 1,  kRed+1,   1, 1, 1, kRed+1,   1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> bckg_style = { 1,  kBlue+1,  1, 1, 1, kBlue+1,  1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> fit_style  = { 1,  kGreen+1, 1, 1, 2, kGreen+1, 1.0, 2.0, 0, 0, 0 }; 

  std::vector< std::string > files;
  split_strings( files, input, ":" );
  TFile * file_data = new TFile( files.at(0).c_str(),      "READ");
  TFile * file_sign = new TFile( files.at(1).c_str(),      "READ");
  TFile * file_bckg = new TFile( files.at(2).c_str(),      "READ");

  TTree * data_tree = (TTree *) file_data->Get( "tree" );
  TTree * sign_tree = (TTree *) file_sign->Get( "tree" );
  TTree * bckg_tree = (TTree *) file_bckg->Get( "tree" );
  
  std::vector< std::string > extra_cuts;
  split_strings( extra_cuts, cuts, ":" );
  std::vector< bound > extra_cut_bounds;
  if ( !cuts.empty() ){ 
    auto proc_cut = [&selections]( std::string & cut ){ return selections->get_bound( cut ); };
    std::transform( extra_cuts.begin(), extra_cuts.end(), std::back_inserter( extra_cut_bounds ), proc_cut );
  }

  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( analysis_bound.get_bins() );

  TFile * diff_file = new TFile( Form( "./eval/diff_%s.root", unique.c_str() ), "RECREATE" );        
  TFile * stat_file = new TFile( Form( "./eval/stat_%s.root", unique.c_str() ), "RECREATE" );        


  std::vector< std::vector< float > > all_sign_weights( analysis_bound.get_bins() + 2 );
  std::vector< std::vector< float > > all_bckg_weights( analysis_bound.get_bins() + 2 );
  std::vector< float > empty_bins( spectator_bound.get_bins() + 2 );
  std::fill( empty_bins.begin(), empty_bins.end(), 0 );
  all_sign_weights.at( 0 ) = empty_bins;
  all_sign_weights.at( analysis_bound.get_bins() + 1 ) = empty_bins;
  all_bckg_weights.at( 0 ) = empty_bins;
  all_bckg_weights.at( analysis_bound.get_bins() + 1 ) = empty_bins;

  for ( std::string & mass : mass_bins ){
    
    bound mass_bound = selections->get_bound( mass );
    std::string mass_cut = mass_bound.get_cut();

    TH1F * chi2_v1_hist = new TH1F( Form( "chi2_v1_%s", mass.c_str() ), "chi2_v1", analysis_bound.get_bins(), analysis_bound.get_min(), analysis_bound.get_max() );
    TH1F * chi2_v2_hist = new TH1F( Form( "chi2_v2_%s", mass.c_str() ), "chi2_v2", analysis_bound.get_bins(), analysis_bound.get_min(), analysis_bound.get_max() );
    TH1F * dof_hist = new TH1F( Form( "dof_%s", mass.c_str() ), "", analysis_bound.get_bins(), analysis_bound.get_min(), analysis_bound.get_max() );

    fit_results fit_stats;
    int ana_bin = 0;
    TTree * stats_tree = new TTree( mass.c_str(), "" );
    stats_tree->Branch( "ana_bin",   &ana_bin );
    stats_tree->Branch( "fit_stats", &fit_stats, 32000, 2 );


    char data_hist_name[100], sign_hist_name[100], bckg_hist_name[100];
    char data_draw[150], sign_draw[150], bckg_draw[150];
    char data_weight_cut[400], mc_weight_cut[400];
    
    for ( size_t cut_no = 0; cut_no < ana_cuts.size(); cut_no++ ){

      std::string & ana_cut = ana_cuts[cut_no];
      std::string & ana_cut_name = ana_cut_names[cut_no];

      for ( bound & extra_bound : extra_cut_bounds ){ ana_cut += "&&" + extra_bound.get_cut(); }

      sprintf( data_hist_name, "data_%s_%s", mass.c_str(), ana_cut_name.c_str() );
      sprintf( sign_hist_name, "sign_%s_%s", mass.c_str(), ana_cut_name.c_str() );
      sprintf( bckg_hist_name, "bckg_%s_%s", mass.c_str(), ana_cut_name.c_str() );

      sprintf( data_draw, "%s>>%s", spectator_bound.get_var().c_str(), data_hist_name );
      sprintf( sign_draw, "%s>>%s", spectator_bound.get_var().c_str(), sign_hist_name );
      sprintf( bckg_draw, "%s>>%s", spectator_bound.get_var().c_str(), bckg_hist_name );

      sprintf( data_weight_cut, "subtraction_weight*((%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str() ) ;
      sprintf( mc_weight_cut, "(photon_sf*muon_sf*pu_weight*subtraction_weight)*((%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str() ) ;

      TH1F * data = new TH1F( data_hist_name, "", spec_bins, spec_min, spec_max );
      TH1F * sign = new TH1F( sign_hist_name, "", spec_bins, spec_min, spec_max );
      TH1F * bckg = new TH1F( bckg_hist_name, "", spec_bins, spec_min, spec_max );

      data_tree->Draw( data_draw, data_weight_cut );
      sign_tree->Draw( sign_draw, mc_weight_cut, "e goff" );
      bckg_tree->Draw( bckg_draw, mc_weight_cut, "e goff" );

      pos_hists( data, sign, bckg );
      TH1F * sign_scaled = (TH1F *) sign->Clone();
      TH1F * bckg_scaled = (TH1F *) bckg->Clone(); 
      TH1F * data_cloned = (TH1F *) data->Clone(); 
      TH1F * output_sign = (TH1F *) sign->Clone();
      TH1F * output_bckg = (TH1F *) bckg->Clone(); 
      TH1F * output_data = (TH1F *) data->Clone(); 

      style_hist( output_data, data_style );
      style_hist( output_sign, sign_style );
      style_hist( output_bckg, bckg_style );

      measure fitting = produce_measurement( sign, bckg, data );
      RooFitResult * fr = fitting.pdf->fitTo( *(fitting.data), Strategy(2), Save(1), PrintLevel( -1 ) );

      std::string output_string = Form( "%s_hf_99_pos_%s_%s-%i_%s", spectator_bound.get_var().c_str(), mass.c_str(), 
                                        analysis_bound.get_var().c_str(), (int) cut_no+1, unique.c_str() );

      fit_stats = fit_results( fr, output_sign, output_bckg, output_data );
      chi2_v1_hist->SetBinContent( cut_no+1, fit_stats.chi2_v1 );
      chi2_v2_hist->SetBinContent( cut_no+1, fit_stats.chi2_v2 );
      dof_hist->SetBinContent( cut_no+1, fit_stats.ndof );

      TH1F * errh = output_histogram( fr, output_sign, output_bckg, output_data, output_string, fit_stats );

      ana_bin = cut_no + 1;

      RooRealVar * mu = ( RooRealVar *) fr->floatParsFinal().find( "mu" );
      RooRealVar * pp = ( RooRealVar *) fr->floatParsFinal().find( "ScalingPP" );

      sign_scaled->Scale( mu->getVal() );
      bckg_scaled->Scale( pp->getVal() );      

      TH1F * fit = (TH1F *) sign_scaled->Clone();
      fit->Reset();
      fit->Add( sign_scaled, bckg_scaled, 1.0, 1.0 );
      
      //integral_store->cd();
      diff_file->cd();
      std::string input_hist_name      = std::string{ mass + "_" + (cut_no+1) };
      data_cloned->Write( Form( "data_%s", input_hist_name.c_str() ) );
      sign_scaled->Write( Form( "sign_%s", input_hist_name.c_str() ) );
      bckg_scaled->Write( Form( "bckg_%s", input_hist_name.c_str() ) );
      errh->Write( Form( "errh_%s", input_hist_name.c_str() ) );

      if ( mass.find( "Q12" ) == std::string::npos ){ continue; }

      TH1F * sign_weight_hist = new TH1F( Form( "sign_weight_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spectator_bound.get_bins(), 0, spectator_bound.get_bins() );
      TH1F * bckg_weight_hist = new TH1F( Form( "bckg_weight_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spectator_bound.get_bins(), 0, spectator_bound.get_bins() );

      std::vector< float > sign_weight( spectator_bound.get_bins() + 2 );
      std::vector< float > bckg_weight( spectator_bound.get_bins() + 2 );
      sign_weight.at( 0 ) = 0;
      sign_weight.at( spectator_bound.get_bins() + 1 ) = 0;
      bckg_weight.at( 0 ) = 0;
      bckg_weight.at( spectator_bound.get_bins() + 1 ) = 0;

      for ( int bin = 1; bin <= spectator_bound.get_bins(); bin++ ){

        float sign_bin_weight = sign_scaled->GetBinContent( bin )/fit->GetBinContent( bin );
        float bckg_bin_weight = bckg_scaled->GetBinContent( bin )/fit->GetBinContent( bin );
        if (sign_scaled->GetBinContent( bin ) == 0 || fit->GetBinContent( bin ) == 0 ){ sign_bin_weight = 0; }
        if (bckg_scaled->GetBinContent( bin ) == 0 || fit->GetBinContent( bin ) == 0 ){ bckg_bin_weight = 0; }
        sign_weight_hist->SetBinContent( bin, sign_bin_weight );
        bckg_weight_hist->SetBinContent( bin, bckg_bin_weight );
        sign_weight.at( bin ) = sign_bin_weight;
        bckg_weight.at( bin ) = bckg_bin_weight;


      }

      all_sign_weights.at( cut_no + 1 ) = sign_weight;
      all_bckg_weights.at( cut_no + 1 ) = bckg_weight;

      stat_file->cd();
      sign_weight_hist->Write();
      bckg_weight_hist->Write();
      stats_tree->Fill(); 

    }
  
    stat_file->cd();
    chi2_v1_hist->Write();
    chi2_v2_hist->Write();
    dof_hist->Write();
    stats_tree->Write();
  }

  for ( std::vector< float > & flt_vec : all_sign_weights ){
    for ( float & flt : flt_vec ){ std::cout << Form( "%.5f", flt ) << "   "; }
    std::cout << std::endl;
  }

  auto sign_weight_lambda = [ &all_sign_weights ]( const ROOT::RVecI & ana_bin, const ROOT::RVecI & spec_bin ){
    return double( all_sign_weights.at( ana_bin[0] ).at( spec_bin[0] ) );
  };

  auto bckg_weight_lambda = [ &all_bckg_weights ]( const ROOT::RVecI & ana_bin, const ROOT::RVecI & spec_bin ){
    return double( all_bckg_weights.at( ana_bin[0] ).at( spec_bin[0] ) );
  };

  TChain chain( "tree" );
  chain.Add( files.at(0).c_str() );
  ROOT::RDataFrame full_frame( chain );
  auto output_frame = full_frame.Define( "hf_sign_weight", sign_weight_lambda, { "ana_bin", "spec_bin" } );
  output_frame = output_frame.Define( "hf_bckg_weight", bckg_weight_lambda, { "ana_bin", "spec_bin" } );
  output_frame.Snapshot(  "tree", Form("./eval/weight_%s.root", unique.c_str() )  );

}
