#include "safe_hf.hxx"


void safe_hf( std::string & input, std::string & abin_var, std::string & spec_var, std::string & cuts, 
        std::string & unique, bound_mgr * selections ){

  prep_style();
  gROOT->SetBatch(true);

  std::vector<std::string>  mass_bins = { "Q12" }; 
 
  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int spec_bins = spectator_bound.get_bins();
  float spec_min = spectator_bound.get_min();
  float spec_max = spectator_bound.get_max();

  std::vector<float> data_style = { 21, 1,        1, 1, 1, 1.0,      1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> sign_style = { 1,  kRed+1,   1, 1, 1, kRed+1,   1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> bckg_style = { 1,  kBlue+1,  1, 1, 1, kBlue+1,  1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> fit_style  = { 1,  kGreen+1, 1, 1, 2, kGreen+1, 1.0, 2.0, 0, 0, 0 }; 

  std::vector< std::string > files;
  split_strings( files, input, ":" );
  TFile * file_data = new TFile( files.at(0).c_str(),      "READ");
  TFile * file_sign = new TFile( files.at(1).c_str(),      "READ");
  TFile * file_bckg = new TFile( files.at(2).c_str(),      "READ");

  TTree * data_tree = (TTree *) file_data->Get( "tree" );
  TTree * sign_tree = (TTree *) file_sign->Get( "tree" );
  TTree * bckg_tree = (TTree *) file_bckg->Get( "tree" );
  
  std::vector< std::string > extra_cuts;
  split_strings( extra_cuts, cuts, ":" );
  std::vector< bound > extra_cut_bounds;
  if ( !cuts.empty() ){ 
    auto proc_cut = [&selections]( std::string & cut ){ return selections->get_bound( cut ); };
    std::transform( extra_cuts.begin(), extra_cuts.end(), std::back_inserter( extra_cut_bounds ), proc_cut );
  }

  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( analysis_bound.get_bins() );

  TFile * diff_file = new TFile( Form( "./eval/diff_%s.root", unique.c_str() ), "RECREATE" );        
  TFile * stat_file = new TFile( Form( "./eval/stat_%s.root", unique.c_str() ), "RECREATE" );        

  for ( std::string & mass : mass_bins ){
    
    bound mass_bound = selections->get_bound( mass );
    std::string mass_cut = mass_bound.get_cut();

    TH1F * chi2_v1_hist = new TH1F( Form( "chi2_v1_%s", mass.c_str() ), "chi2_v1", analysis_bound.get_bins(), analysis_bound.get_min(), analysis_bound.get_max() );
    TH1F * chi2_v2_hist = new TH1F( Form( "chi2_v2_%s", mass.c_str() ), "chi2_v2", analysis_bound.get_bins(), analysis_bound.get_min(), analysis_bound.get_max() );
    TH1F * dof_hist = new TH1F( Form( "dof_%s", mass.c_str() ), "", analysis_bound.get_bins(), analysis_bound.get_min(), analysis_bound.get_max() );

    fit_results fit_stats;
    int ana_bin = 0;
    TTree * stats_tree = new TTree( mass.c_str(), "" );
    stats_tree->Branch( "ana_bin",   &ana_bin );
    stats_tree->Branch( "fit_stats", &fit_stats, 32000, 2 );
    
    for ( size_t cut_no = 0; cut_no < ana_cuts.size(); cut_no++ ){

      std::string & ana_cut = ana_cuts[cut_no];
      std::string & ana_cut_name = ana_cut_names[cut_no];
      for ( bound & extra_bound : extra_cut_bounds ){ ana_cut += "&&" + extra_bound.get_cut(); }

      TH1F * data = new TH1F( Form( "data_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );
      TH1F * sign = new TH1F( Form( "sign_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );
      TH1F * bckg = new TH1F( Form( "bckg_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );


      data_tree->Draw( Form( "%s>>data_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str() ) );
      sign_tree->Draw( Form( "%s>>sign_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str() ) );
      bckg_tree->Draw( Form( "%s>>bckg_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str() ) );

      pos_hists( data, sign, bckg );
      TH1F * sign_scaled = (TH1F *) sign->Clone();
      TH1F * bckg_scaled = (TH1F *) bckg->Clone(); 
      TH1F * data_cloned = (TH1F *) data->Clone(); 
      TH1F * output_sign = (TH1F *) sign->Clone();
      TH1F * output_bckg = (TH1F *) bckg->Clone(); 
      TH1F * output_data = (TH1F *) data->Clone(); 

      style_hist( output_data, data_style );
      style_hist( output_sign, sign_style );
      style_hist( output_bckg, bckg_style );

      measure fitting = produce_measurement( sign, bckg, data );
      RooFitResult * fr = fitting.pdf->fitTo( *(fitting.data), Strategy(2), Save(1), PrintLevel( -1 ) );

      std::string output_string = Form( "%s_hf_99_pos_%s_%s-%i_%s", spectator_bound.get_var().c_str(), mass.c_str(), 
                                        analysis_bound.get_var().c_str(), (int) cut_no+1, unique.c_str() );

      fit_stats = fit_results( fr, output_sign, output_bckg, output_data );
      chi2_v1_hist->SetBinContent( cut_no+1, fit_stats.chi2_v1 );
      chi2_v2_hist->SetBinContent( cut_no+1, fit_stats.chi2_v2 );
      dof_hist->SetBinContent( cut_no+1, fit_stats.ndof );

      TH1F * errh = output_histogram( fr, output_sign, output_bckg, output_data, output_string, fit_stats );

      ana_bin = cut_no + 1;


      RooRealVar * mu = ( RooRealVar *) fr->floatParsFinal().find( "mu" );
      RooRealVar * pp = ( RooRealVar *) fr->floatParsFinal().find( "ScalingPP" );

      sign_scaled->Scale( mu->getVal() );
      bckg_scaled->Scale( pp->getVal() );      

      TH1F * fit = (TH1F *) sign_scaled->Clone();
      fit->Reset();
      fit->Add( sign_scaled, bckg_scaled, 1.0, 1.0 );
      
      //integral_store->cd();
      diff_file->cd();
      std::string input_hist_name      = std::string{ mass + "_" + (cut_no+1) };
      data_cloned->Write( Form( "data_%s", input_hist_name.c_str() ) );
      sign_scaled->Write( Form( "sign_%s", input_hist_name.c_str() ) );
      bckg_scaled->Write( Form( "bckg_%s", input_hist_name.c_str() ) );
      errh->Write( Form( "errh_%s", input_hist_name.c_str() ) );

      std::vector< std::string > cut_series = spectator_bound.get_cut_series();

      TH1F * sign_weight_hist = new TH1F( Form( "sign_weight_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spectator_bound.get_bins(), 0, spectator_bound.get_bins() );
      TH1F * bckg_weight_hist = new TH1F( Form( "bckg_weight_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spectator_bound.get_bins(), 0, spectator_bound.get_bins() );
      
      for ( int bin = 1; bin <= spectator_bound.get_bins(); bin++ ){

        float sign_bin_weight = sign_scaled->GetBinContent( bin )/fit->GetBinContent( bin );
        float bckg_bin_weight = bckg_scaled->GetBinContent( bin )/fit->GetBinContent( bin );
        if (sign_scaled->GetBinContent( bin ) == 0 || fit->GetBinContent( bin ) == 0 ){ sign_bin_weight = 0; }
        if (bckg_scaled->GetBinContent( bin ) == 0 || fit->GetBinContent( bin ) == 0 ){ bckg_bin_weight = 0; }
        sign_weight_hist->SetBinContent( bin, sign_bin_weight );
        bckg_weight_hist->SetBinContent( bin, bckg_bin_weight );


        // current writeout process
        if ( mass.find( "Q12" ) == std::string::npos ){ continue; }
        std::string bdt_cut = cut_series[ bin-1 ];
        TChain chain( "tree" );
        chain.Add( files.at(0).c_str() );
        ROOT::RDataFrame frame( chain );
        std::string current_cut = bdt_cut + "&&" + mass_cut + "&&" + ana_cut;
        auto process_frame = frame.Filter( current_cut.c_str(), "filt" );
        auto sign_weight_lambda = [ &sign_bin_weight ](){ return sign_bin_weight; };
        auto bckg_weight_lambda = [ &bckg_bin_weight ](){ return bckg_bin_weight; };
        auto chi2_v1_lambda = [ &fit_stats ](){ return fit_stats.chi2_v1; };
        auto chi2_v2_lambda = [ &fit_stats ](){ return fit_stats.chi2_v2; };
        process_frame = process_frame.Define( "hf_sign_weight", sign_weight_lambda );
        process_frame = process_frame.Define( "hf_bckg_weight", bckg_weight_lambda );
        process_frame = process_frame.Define( "chi2_v1", chi2_v1_lambda );
        process_frame = process_frame.Define( "chi2_v2", chi2_v2_lambda );
        process_frame.Snapshot( "tree", Form("./df/%s_A-%i_S-%i_output.root", unique.c_str(), (int) cut_no+1, bin ), "" );

      }

      if ( mass.find( "Q12" ) == std::string::npos ){ continue; }
      stat_file->cd();
      sign_weight_hist->Write();
      bckg_weight_hist->Write();

      stats_tree->Fill(); 
    }
  
    stat_file->cd();
    chi2_v1_hist->Write();
    chi2_v2_hist->Write();
    dof_hist->Write();
    stats_tree->Write();
  }
  

  // merge trees
  TChain chain( "tree" );
  for ( size_t cut_no = 0; cut_no < ana_cuts.size(); cut_no++ ){
    for ( int bin = 1; bin <= spectator_bound.get_bins(); bin++ ){

      TFile * check = new TFile( Form("./df/%s_A-%i_S-%i_output.root", unique.c_str(), (int) cut_no+1, bin ), "READ" );
      TTree * check_tree = (TTree *) check->Get( "tree" );
      int entries = check_tree->GetEntries();
      check->Close();
      if ( entries <= 0 ){ continue; }
      chain.Add( Form("./df/%s_A-%i_S-%i_output.root", unique.c_str(), (int) cut_no+1, bin ) );
    }
  }

  ROOT::RDataFrame full_frame( chain );
  full_frame.Snapshot(  "tree", Form("./eval/weight_%s.root", unique.c_str() )  );

}
