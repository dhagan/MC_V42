#include <func_hf.hxx>

#ifndef safe_hf_hxx
#define safe_hf_hxx

void safe_hf( std::string & input_file, std::string & abin_var, std::string & spec_var,
         std::string & cuts, std::string & unique, bound_mgr * selections );

#endif
