#!/bin/bash

executable=${ANA_IP}/nn/build/nn
disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
spec="ActIpX:AvgIpX:AbsdY:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0"
hpar="!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5%:PruneMethod=NoPruning"

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" )

for qtb in ${!ranges_list[@]}; do


	number=$(($qtb + 1))
	output_unique="qtb_split-${number}"
	input_unique="qtb_split-${number}"
	range="${ranges_list[$qtb]}"
	log=${LOG_PATH}/nn/${ouput_unique}.txt
	nnxml=${OUT_PATH}/nn/${output_unique}/tmva_dataloader/weights/tmva_classification_DNN.weights.xml

	data=${OUT_PATH}/trees/gen_${input_unique}/data/data_${input_unique}.root
	sign=${OUT_PATH}/trees/gen_${input_unique}/sign/sign_${input_unique}.root
	bckg=${OUT_PATH}/trees/gen_${input_unique}/bckg/bckg_${input_unique}.root
	bbbg=${OUT_PATH}/trees/gen_${input_unique}/bbbg/bbbg_${input_unique}.root


	touch ${log}
	mkdir -p ${OUT_PATH}/nn/${output_unique}
	mkdir -p ${OUT_PATH}/nn/${output_unique}/eval
	
	pushd ${OUT_PATH}/nn/${output_unique} >> /dev/null
	
	## train tree
	${executable} -s ${sign} -b ${bckg} -d ${disc} -v ${spec} -m "train" -h ${hpar} -u ${output_unique} 2>&1 | tee ${log}
	
	## eval samples
	$executable -s ${data} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${nnxml} -u ${output_unique} -t "data" 2>&1 | tee -a ${log}
	$executable -s ${sign} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${nnxml} -u ${output_unique} -t "sign" 2>&1 | tee -a ${log}
	$executable -s ${bckg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${nnxml} -u ${output_unique} -t "bckg" 2>&1 | tee -a ${log}
	$executable -s ${bbbg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${nnxml} -u ${output_unique} -t "bbbg" 2>&1 | tee -a ${log}

	popd >> /dev/null

done
