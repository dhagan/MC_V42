cmake_minimum_required( VERSION 3.14.0 )

## Project details
project( nn )

## Set sources and lib paths.
set( sources src/main.cxx src/train.cxx src/eval.cxx )

## Find root package
find_package( ROOT CONFIG REQUIRED COMPONENTS TMVA )
##target_compile_definitions( ROOT::Core INTERFACE ${ROOT_DEFINITIONS} )

## Prepare executable
add_compile_options( -std=c++17 -Wall -Werror -Wextra -pedantic -O2 )
add_executable( nn ${sources} )
target_include_directories( nn PUBLIC inc ${CMAKE_SOURCE_DIR}/lib/analysis_utils/inc )

## Link libraries
target_link_libraries( nn PRIVATE ROOT::Core ${ROOT_LIBRARIES} )
target_link_libraries( nn PRIVATE analysis_utils  )

## Enable export.
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
