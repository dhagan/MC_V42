
#include <main.hxx>
#include <train.hxx>
#include <eval.hxx>

// converting scripts from old version

// no mode equivalent in this 
// nominal -n -> signal -s
// background -v -> bckg -b
// training frac -f -> -f, unused
// number folds -p -> -n
// hyperparams -H -> h
// discriminators -l -> disc -d
// spec_vars -s -> -v
// xml path -x -> -x
// unique_str ? -> -u
// type ? -> -t


int help(){
	  return 0;
}

int main( int argc, char *argv[] ) {

  std::string mode;
  std::string sign_file, bckg_file;
  std::string hyperparams;
  std::string disc_vars, spec_vars;
  std::string xml_path;
  std::string type, unique;

  static struct option long_options[] = {
    { "mode",           required_argument,  0,        'm'},
    { "sign",           required_argument,  0,        's'},
    { "bckg",           required_argument,  0,        'b'},
    { "hyperparams",    required_argument,  0,        'h'},
    { "disc_vars ",     required_argument,  0,        'd'},
    { "spec_vars",      required_argument,  0,        'v'},
    { "xml_path",       required_argument,  0,        'x'},
    { "type",           required_argument,  0,        't'},
    { "unique",         required_argument,  0,        'u'},
    { "help",           no_argument,        0,        '?'},
    { 0,                0,                  0,        0}
  };

  int option_index{0}, option{0};
  do {
    option = getopt_long( argc, argv, "m:s:b:h:d:v:x:t:u:?", long_options, &option_index);
    switch ( option ){
      case 'm':
        mode          =   std::string( optarg );
        break;
      case 's':
        sign_file =   std::string( optarg );
        break;
      case 'b': 
        bckg_file =   std::string( optarg );
        break;
      case 'h':
        hyperparams   =   std::string( optarg );
        break;
      case 'd':
        disc_vars  =   std::string( optarg );
        break;
      case 'v':
        spec_vars  =   std::string( optarg );
        break;
      case 'x':
        xml_path      =   std::string( optarg );
        break;
      case 't':
        type      =   std::string( optarg );
        break;
      case 'u':
        unique    =   std::string( optarg );
        break;
      case '?':
        return help();
        break;
    }
  } while ( option != -1 );


  if ( mode.find("train") != std::string::npos ){
    train( sign_file, bckg_file, hyperparams, disc_vars, spec_vars, unique );
  }

  if ( mode.find("eval") != std::string::npos ){
    eval( sign_file, disc_vars, spec_vars, type, unique, xml_path );
  }

}
