
#include <main.hxx>
#include <train.hxx>

void train( std::string sign_file, std::string bckg_file, std::string hyperparams, std::string disc_vars,
    std::string spec_vars, std::string unique ){


  if ( !hyperparams.empty() ){ std::cout << hyperparams << std::endl; }

  std::vector<std::string> sign_vec, bckg_vec;
  split_strings( sign_vec, sign_file, ":" );
  split_strings( bckg_vec, bckg_file, ":" );

  TFile * input_sign_file = new TFile( sign_vec.at(0).c_str(), "READ" );
  TFile * input_bckg_file = new TFile( bckg_vec.at(0).c_str(), "READ" );
  TTree * sign_tree = (TTree*) input_sign_file->Get( sign_vec.at(1).c_str() );
  TTree * bckg_tree = (TTree*) input_bckg_file->Get( bckg_vec.at(1).c_str() );
  
  TFile * tmva_train_output = new TFile( Form( "./tmva_train_%s.root", unique.c_str() ), "RECREATE" );
  TMVA::Factory * tmva_factory = new TMVA::Factory( "tmva_classification", tmva_train_output, "!V:!Silent:Color:AnalysisType=Classification");

  double sign_weight = 1.0;
  double bckg_weight = 1.0;

  TMVA::DataLoader * tmva_dataloader = new TMVA::DataLoader( "tmva_dataloader" );
  
  tmva_dataloader->AddTree( sign_tree, "Signal", sign_weight, "(EventNumber%2)==0", TMVA::Types::kTraining);
  tmva_dataloader->AddTree( sign_tree, "Signal", sign_weight, "(EventNumber%2)==1", TMVA::Types::kTesting);
  tmva_dataloader->AddTree( bckg_tree, "Background", bckg_weight, "(EventNumber%2)==0", TMVA::Types::kTraining);
  tmva_dataloader->AddTree( bckg_tree, "Background", bckg_weight, "(EventNumber%2)==1", TMVA::Types::kTesting);

  std::vector<std::string> disc_var_vec, spec_var_vec;
  split_strings( disc_var_vec, disc_vars, ":" );
  split_strings( spec_var_vec, spec_vars, ":" );

  for ( std::string & disc : disc_var_vec ){
    tmva_dataloader->AddVariable( disc.c_str(), 'F' );
  }

  for ( std::string & spec : spec_var_vec ){
    tmva_dataloader->AddSpectator( spec.c_str(), 'F' );
  }

  tmva_dataloader->PrepareTrainingAndTestTree( "", "NormMode=EqualNumEvents:!V" );

  std::string options = "Architecture=CPU:ErrorStrategy=CROSSENTROPY:WeightInitialization=XAVIER:";
  options += Form( "InputLayout=1|1|%i:", (int) disc_var_vec.size() );
  options += "Layout=TANH|16,RELU|64,SYMMRELU|64,TANH|32,TANH|16,TANH:";
  options += "TrainingStrategy=LearningRate=1e-4|Momentum=0.99|";
  options += "ConvergenceSteps=10|BatchSize=32|TestRepetitions=1|";
  options += "MaxEpochs=30|WeightDecay=1e-3|Regularization=NL2|";
  options += "Optimizer=ADAM|ADAM_beta1=0.9|ADAM_beta2=0.999|ADAM_eps=1.E-7|";

  //options += "DropConfig=0.3+0.5+0.5+0.5+0.3+0.0";
  std::cout << options << std::endl;


  //tmva_factory->BookMethod( tmva_dataloader, TMVA::Types::kBDT, "BDT", hyperparams.c_str() );
  tmva_factory->BookMethod( tmva_dataloader, TMVA::Types::kDL, "DNN", options.c_str() );

  tmva_factory->TrainAllMethods();
  tmva_factory->TestAllMethods();
  tmva_factory->EvaluateAllMethods();

  //double roc_integral = tmva_factory->GetROCIntegral( tmva_dataloader, "BDT");
  //std::cout << roc_integral << std::endl;

  tmva_train_output->Close();

}
