#include "data_sim_mix.hxx"

void sample_mixing(int sign_fraction_int = 0){

  TFile sign_file{"./signal_bound-0/split0/signal_00_mu4000_P5000_bound-0.root","READ"}; 
  TFile bckg_file{"./pp_bound-0/split0/pp_00_mu4000_P5000_bound-0.root","READ"}; 

  TTree * sign_tree = (TTree*) sign_file.Get("TreeD");
  TTree * bckg_tree = (TTree*) bckg_file.Get("TreeD");
  
  TFile * combined_bckg_file = new TFile(Form("./bckg_bound-0/bckg_00_mu4000_P5000_bound-0.root"),"RECREATE"); 
  TTree * combined_bckg_tree = new TTree{"TreeD","TreeD"};

  float sign_AbsCosTheta,sign_AbsdPhi,sign_AbsPhi,sign_DPhi,sign_DY,sign_Phi,sign_costheta,sign_EventNumber,sign_Lambda,sign_qTSquared,sign_DiMuonMass,
        sign_DiMuonTau,sign_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_DiMuonPt,sign_PhotonPt,sign_AbsdY,sign_qxpsi,sign_qypsi,sign_qxgamma,sign_qygamma,
        sign_qxsum,sign_qysum,sign_qtA,sign_qtB, sign_qtL, sign_qtM;
  
  float bckg_AbsCosTheta,bckg_AbsdPhi,bckg_AbsPhi,bckg_DPhi,bckg_DY,bckg_Phi,bckg_costheta,bckg_EventNumber,bckg_Lambda,bckg_qTSquared,bckg_DiMuonMass,
        bckg_DiMuonTau,bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_DiMuonPt,bckg_PhotonPt,bckg_AbsdY,bckg_qxpsi,bckg_qypsi,bckg_qxgamma,bckg_qygamma,
        bckg_qxsum,bckg_qysum,bckg_qtA,bckg_qtB, bckg_qtL, bckg_qtM;

  float combined_bckg_AbsCosTheta,combined_bckg_AbsdPhi,combined_bckg_AbsPhi,combined_bckg_DPhi,combined_bckg_DY,combined_bckg_Phi,combined_bckg_costheta,combined_bckg_EventNumber,combined_bckg_Lambda,combined_bckg_qTSquared,combined_bckg_DiMuonMass,
        combined_bckg_DiMuonTau,combined_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,combined_bckg_DiMuonPt,combined_bckg_PhotonPt,combined_bckg_AbsdY,combined_bckg_qxpsi,combined_bckg_qypsi,combined_bckg_qxgamma,combined_bckg_qygamma,
        combined_bckg_qxsum,combined_bckg_qysum,combined_bckg_qtA,combined_bckg_qtB, combined_bckg_qtL, combined_bckg_qtM;

  
  combined_bckg_tree->Branch("AbsCosTheta",                              &combined_bckg_AbsCosTheta,                       "AbsCosTheta/F");
	combined_bckg_tree->Branch("AbsdPhi",                                  &combined_bckg_AbsdPhi,                           "AbsdPhi/F");
	combined_bckg_tree->Branch("AbsPhi",                                   &combined_bckg_AbsPhi,                            "AbsPhi/F");
	combined_bckg_tree->Branch("DPhi",                                     &combined_bckg_DPhi,                              "DPhi/F");
	combined_bckg_tree->Branch("DY",                                       &combined_bckg_DY,                                "DY/F");
	combined_bckg_tree->Branch("Phi",                                      &combined_bckg_Phi,                               "phi/F");
	combined_bckg_tree->Branch("costheta",                                 &combined_bckg_costheta,                          "CosTheta/F");
	combined_bckg_tree->Branch("EventNumber",                              &combined_bckg_EventNumber,                       "EventNumber/F");
	combined_bckg_tree->Branch("Lambda",                                   &combined_bckg_Lambda,                            "Lambda/F");
	combined_bckg_tree->Branch("qTSquared",                                &combined_bckg_qTSquared,                         "qTSquared/F");
	combined_bckg_tree->Branch("DiMuonMass",                               &combined_bckg_DiMuonMass,                        "DiMuonMass/F");
	combined_bckg_tree->Branch("DiMuonTau",                                &combined_bckg_DiMuonTau,                         "DiMuonTau/F");
	combined_bckg_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &combined_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	combined_bckg_tree->Branch("DiMuonPt",                                 &combined_bckg_DiMuonPt,                          "DiMuonPt/F");
	combined_bckg_tree->Branch("PhotonPt",                                 &combined_bckg_PhotonPt,                          "PhotonPt/F");
	combined_bckg_tree->Branch("AbsdY",                                    &combined_bckg_AbsdY,                             "AbsdY");
	combined_bckg_tree->Branch("qxpsi",                                    &combined_bckg_qxpsi,                             "qxpsi");
	combined_bckg_tree->Branch("qypsi",                                    &combined_bckg_qypsi,                             "qypsi");
	combined_bckg_tree->Branch("qxgamma",                                  &combined_bckg_qxgamma,                           "qxgamma");
	combined_bckg_tree->Branch("qygamma",                                  &combined_bckg_qygamma,                           "qygamma");
	combined_bckg_tree->Branch("qxsum",                                    &combined_bckg_qxsum,                             "qxsum");
	combined_bckg_tree->Branch("qysum",                                    &combined_bckg_qysum,                             "qysum");
	combined_bckg_tree->Branch("qtA",                                      &combined_bckg_qtA,                               "qtA");
	combined_bckg_tree->Branch("qtB",                                      &combined_bckg_qtB,                               "qtB");
  combined_bckg_tree->Branch("qtL",                                      &combined_bckg_qtL,                               "qtL");
	combined_bckg_tree->Branch("qtM",                                      &combined_bckg_qtM,                               "qtM");

  
  sign_tree->SetBranchAddress("AbsCosTheta", &sign_AbsCosTheta);
	sign_tree->SetBranchAddress("AbsdPhi", &sign_AbsdPhi);
	sign_tree->SetBranchAddress("AbsPhi", &sign_AbsPhi);
	sign_tree->SetBranchAddress("DPhi", &sign_DPhi);
	sign_tree->SetBranchAddress("DY", &sign_DY);
	sign_tree->SetBranchAddress("Phi", &sign_Phi);
	sign_tree->SetBranchAddress("costheta", &sign_costheta);
	sign_tree->SetBranchAddress("EventNumber", &sign_EventNumber);
	sign_tree->SetBranchAddress("Lambda", &sign_Lambda);
	sign_tree->SetBranchAddress("qTSquared", &sign_qTSquared);
	sign_tree->SetBranchAddress("DiMuonMass", &sign_DiMuonMass);
	sign_tree->SetBranchAddress("DiMuonTau", &sign_DiMuonTau);
	sign_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",&sign_Trigger_HLT_2mu4_bJpsimumu_noL2);
	sign_tree->SetBranchAddress("DiMuonPt", &sign_DiMuonPt);
	sign_tree->SetBranchAddress("PhotonPt", &sign_PhotonPt);
	sign_tree->SetBranchAddress("AbsdY", &sign_AbsdY);
	sign_tree->SetBranchAddress("qxpsi", &sign_qxpsi);
	sign_tree->SetBranchAddress("qypsi", &sign_qypsi);
	sign_tree->SetBranchAddress("qxgamma", &sign_qxgamma);
	sign_tree->SetBranchAddress("qygamma", &sign_qygamma);
	sign_tree->SetBranchAddress("qxsum",  &sign_qxsum);
	sign_tree->SetBranchAddress("qysum",  &sign_qysum);
	sign_tree->SetBranchAddress("qtA",  &sign_qtA);
	sign_tree->SetBranchAddress("qtB",  &sign_qtB);
  sign_tree->SetBranchAddress("qtL",  &sign_qtL);
	sign_tree->SetBranchAddress("qtM",  &sign_qtM);

  bckg_tree->SetBranchAddress("AbsCosTheta", &bckg_AbsCosTheta);
	bckg_tree->SetBranchAddress("AbsdPhi", &bckg_AbsdPhi);
	bckg_tree->SetBranchAddress("AbsPhi", &bckg_AbsPhi);
	bckg_tree->SetBranchAddress("DPhi", &bckg_DPhi);
	bckg_tree->SetBranchAddress("DY", &bckg_DY);
	bckg_tree->SetBranchAddress("Phi", &bckg_Phi);
	bckg_tree->SetBranchAddress("costheta", &bckg_costheta);
	bckg_tree->SetBranchAddress("EventNumber", &bckg_EventNumber);
	bckg_tree->SetBranchAddress("Lambda", &bckg_Lambda);
	bckg_tree->SetBranchAddress("qTSquared", &bckg_qTSquared);
	bckg_tree->SetBranchAddress("DiMuonMass", &bckg_DiMuonMass);
	bckg_tree->SetBranchAddress("DiMuonTau", &bckg_DiMuonTau);
	bckg_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",&bckg_Trigger_HLT_2mu4_bJpsimumu_noL2);
	bckg_tree->SetBranchAddress("DiMuonPt", &bckg_DiMuonPt);
	bckg_tree->SetBranchAddress("PhotonPt", &bckg_PhotonPt);
	bckg_tree->SetBranchAddress("AbsdY", &bckg_AbsdY);
	bckg_tree->SetBranchAddress("qxpsi", &bckg_qxpsi);
	bckg_tree->SetBranchAddress("qypsi", &bckg_qypsi);
	bckg_tree->SetBranchAddress("qxgamma", &bckg_qxgamma);
	bckg_tree->SetBranchAddress("qygamma", &bckg_qygamma);
	bckg_tree->SetBranchAddress("qxsum",  &bckg_qxsum);
	bckg_tree->SetBranchAddress("qysum",  &bckg_qysum);
	bckg_tree->SetBranchAddress("qtA",  &bckg_qtA);
	bckg_tree->SetBranchAddress("qtB",  &bckg_qtB);
  bckg_tree->SetBranchAddress("qtL",  &bckg_qtL);
	bckg_tree->SetBranchAddress("qtM",  &bckg_qtM);



  for (int sign_idx = 0; sign_idx < sign_tree->GetEntries(); sign_idx++){

    sign_tree->GetEntry(sign_idx);
    combined_bckg_AbsCosTheta                             = sign_AbsCosTheta;
    combined_bckg_AbsdPhi                                 = sign_AbsdPhi;
    combined_bckg_AbsPhi                                  = sign_AbsPhi;
    combined_bckg_DPhi                                    = sign_DPhi;
    combined_bckg_DY                                      = sign_DY;
    combined_bckg_Phi                                     = sign_Phi;
    combined_bckg_costheta                                = sign_costheta;
    combined_bckg_EventNumber                             = sign_EventNumber;
    combined_bckg_Lambda                                  = sign_Lambda;
    combined_bckg_qTSquared                               = sign_qTSquared;
    combined_bckg_DiMuonMass                              = sign_DiMuonMass;
    combined_bckg_DiMuonTau                               = sign_DiMuonTau;
    combined_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
    combined_bckg_DiMuonPt                                = sign_DiMuonPt;
    combined_bckg_PhotonPt                                = sign_PhotonPt;
    combined_bckg_AbsdY                                   = sign_AbsdY;
    combined_bckg_qxpsi                                   = sign_qxpsi;
    combined_bckg_qypsi                                   = sign_qypsi;
    combined_bckg_qxgamma                                 = sign_qxgamma;
    combined_bckg_qygamma                                 = sign_qygamma;
    combined_bckg_qxsum                                   = sign_qxsum;
    combined_bckg_qysum                                   = sign_qysum;
    combined_bckg_qtA                                     = sign_qtA;
    combined_bckg_qtB                                     = sign_qtB;
    combined_bckg_qtL                                     = sign_qtM;
    combined_bckg_tree->Fill();

  }

  for (int bckg_idx = 0; bckg_idx < bckg_tree->GetEntries(); bckg_idx++){

    bckg_tree->GetEntry(bckg_idx);
    combined_bckg_AbsCosTheta                             = bckg_AbsCosTheta;
    combined_bckg_AbsdPhi                                 = bckg_AbsdPhi;
    combined_bckg_AbsPhi                                  = bckg_AbsPhi;
    combined_bckg_DPhi                                    = bckg_DPhi;
    combined_bckg_DY                                      = bckg_DY;
    combined_bckg_Phi                                     = bckg_Phi;
    combined_bckg_costheta                                = bckg_costheta;
    combined_bckg_EventNumber                             = bckg_EventNumber;
    combined_bckg_Lambda                                  = bckg_Lambda;
    combined_bckg_qTSquared                               = bckg_qTSquared;
    combined_bckg_DiMuonMass                              = bckg_DiMuonMass;
    combined_bckg_DiMuonTau                               = bckg_DiMuonTau;
    combined_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
    combined_bckg_DiMuonPt                                = bckg_DiMuonPt;
    combined_bckg_PhotonPt                                = bckg_PhotonPt;
    combined_bckg_AbsdY                                   = bckg_AbsdY;
    combined_bckg_qxpsi                                   = bckg_qxpsi;
    combined_bckg_qypsi                                   = bckg_qypsi;
    combined_bckg_qxgamma                                 = bckg_qxgamma;
    combined_bckg_qygamma                                 = bckg_qygamma;
    combined_bckg_qxsum                                   = bckg_qxsum;
    combined_bckg_qysum                                   = bckg_qysum;
    combined_bckg_qtA                                     = bckg_qtA;
    combined_bckg_qtB                                     = bckg_qtB;
    combined_bckg_qtL                                     = bckg_qtM;
    combined_bckg_tree->Fill();

  }

  combined_bckg_file->cd();
  combined_bckg_tree->Write();
  combined_bckg_file->Close();
  
}

int main(int argc, char * argv[]){
    sample_mixing(atoi(argv[1]));
}
