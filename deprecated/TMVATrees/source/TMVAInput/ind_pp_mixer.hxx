
#include <TCanvas.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TLorentzVector.h>
#include <TH2.h>
#include <TStyle.h>
#include <TChain.h>
#include <TString.h>
#include <TAxis.h>
#include <TRandom3.h>
#include <TVector3.h>
#include <TPaveText.h>

#include "math.h"
#include "assert.h"
#include "stdio.h"
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <cassert>
#include <stdlib.h>

#include "treeReader.h"

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"

bool VatoCS(const TLorentzVector &mupl,
	    const TLorentzVector &mumi,
	    const TLorentzVector &gamma,
	    TVector3 &CSAxis,
	    TVector3 &xAxis,
	    TVector3 &yAxis,
	    double &cosTheta_dimu,
	    double &cosTheta_gamma,
	    double &phi_dimu,
	    double &phi_gamma);

struct Jpsi
{
  Float_t DiMuonMass;
  Float_t DiMuonTau;
  Float_t DiMuonPt;
  Float_t DiLept_Y;
  Float_t DiLept_Phi;
  Float_t Trigger_HLT_2mu4_bJpsimumu_noL2;
  Float_t EventNumber;
  TLorentzVector mupl, mumi;
  TLorentzVector p4;
  Float_t old_lambda, old_qt2;
  Float_t old_csPhi, old_csCosTheta;
  // Int_t   JpsiIndex;
};
struct Photon
{
  Float_t PhotonPt;
  Float_t Photon_Eta;
  Float_t Photon_Phi;
  TLorentzVector p4;
  // Int_t  PhotonIndex;
};

bool VatoCS(const TLorentzVector &mupl,
	    const TLorentzVector &mumi,
	    const TLorentzVector &gamma,
	    TVector3 &CSAxis,
	    TVector3 &xAxis,
	    TVector3 &yAxis,
	    double &cosTheta_dimu,
	    double &cosTheta_gamma,
	    double &phi_dimu,
	    double &phi_gamma)
{

  TLorentzVector dimu = mupl + mumi;
  TLorentzVector higgs = dimu + gamma;

  double ProtonMass = 938.272;  // MeV
  double BeamEnergy = 6500000.; // MeV

  // 1)
  TLorentzVector p1, p2;
  p1.SetPxPyPzE(0., 0., BeamEnergy,
		TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass));
  p2.SetPxPyPzE(0., 0., -1 * BeamEnergy,
		TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass));

  // 2)
  TLorentzVector dimu_hf, mupl_hf, mumi_hf, gamma_hf, higgs_hf, p1_hf, p2_hf;

  dimu_hf = dimu;
  mupl_hf = mupl;
  mumi_hf = mumi;
  gamma_hf = gamma;
  higgs_hf = higgs;
  p1_hf = p1;
  p2_hf = p2;

  dimu_hf.Boost(-higgs.BoostVector());
  mupl_hf.Boost(-higgs.BoostVector());
  mumi_hf.Boost(-higgs.BoostVector());
  gamma_hf.Boost(-higgs.BoostVector());
  higgs_hf.Boost(-higgs.BoostVector());
  p1_hf.Boost(-higgs.BoostVector());
  p2_hf.Boost(-higgs.BoostVector());

  // 3)
  CSAxis = (p1_hf.Vect().Unit() - p2_hf.Vect().Unit()).Unit();
  yAxis = (p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);
  xAxis = xAxis.Unit();

  //4)

  phi_dimu = atan2((dimu_hf.Vect() * yAxis), (dimu_hf.Vect() * xAxis));
  cosTheta_dimu = dimu_hf.Vect().Dot(CSAxis) / (dimu_hf.Vect().Mag());

  // cross-check
  cosTheta_gamma = gamma_hf.Vect().Dot(CSAxis) / (gamma_hf.Vect().Mag());
  phi_gamma = atan2((gamma_hf.Vect() * yAxis), (gamma_hf.Vect() * xAxis));

  if (fabs(cosTheta_dimu + cosTheta_gamma) > 0.001)
    {
      std::cout << "VatoCS: cosThetaCS sum, beyond tolerance: " << cosTheta_dimu << " " << cosTheta_gamma << std::endl;
    }
  if (fabs(sin(phi_dimu - phi_gamma)) > 0.001)
    {
      std::cout << "VatoCS: phiCS difference, beyond tolerance: " << phi_dimu << " " << phi_gamma << std::endl;
    }
  return true;
}
