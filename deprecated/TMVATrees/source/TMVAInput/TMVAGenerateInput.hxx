
#include <TCanvas.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TLorentzVector.h>
#include <TH2.h>
#include <TStyle.h>
#include <TChain.h>
#include <TString.h>
#include <TAxis.h>
#include <TRandom3.h>
#include <TVector3.h>
#include <TPaveText.h>
#include <TPaveStats.h>
#include <TEfficiency.h>

#include "math.h"
#include "assert.h"
#include "stdio.h"
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <cassert>

#include "treeReader.h"

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


typedef std::vector<std::pair<double,double> > Ranges;

class cutflow {

  public:
    

    double var;
    std::string var_str, output_str;
    TH1F * cutflow_hist;
    std::vector< TH1F* > cut_distributions;
    std::vector< std::string > cut_names{ "Full",  "n_photons>1", "PhotonPt", "mu-Pt", "mu+PT", "trigger", 
                                          "lambda-lower", "qtb-bound", "photon-quality", "signal-dr-0.12", 
                                          "cos2thetaCS", "lambda-bound", "qt2-bound", "qta-bound", "phiCS", 
                                          "dimuon-mass", "lifetime"};
    const int cuts = cut_names.size();

    cutflow(){};

    cutflow( std::string cut_var, std::pair<double,double> limits, std::string input_type, std::string unique ){
      var_str = cut_var; 
      output_str = var_str + "_" + input_type + "_" + unique;
      for ( int cut = 1; cut <= cuts; cut++ ){
        cut_distributions.push_back( new TH1F(Form("%s_cut_%i",var_str.c_str(),cut), "", 50, limits.first, limits.second) );
      }
      cutflow_hist = new TH1F("Cutflow", "", cuts, 0, cuts);
    }

    void fill( int cut ){
      cutflow_hist->Fill(cut-1);
      cut_distributions.at(cut-1)->Fill(var);
    }
    void set_var( double loop_var ){ var = loop_var; }

    void write(){

      TLatex ATLAS;
      ATLAS.SetNDC(); ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
      TLatex wip; 
      wip.SetNDC(); wip.SetTextFont(42); wip.SetTextSize(0.038); wip.SetTextColor(1);
      
      for ( int cut_number = 1; cut_number < cuts; cut_number++ ){

        TCanvas * var_canvas = new TCanvas(Form("var_canv_%i",cut_number),"",600,600,1500,1500);
        TH1F * var_hist = cut_distributions.at(cut_number-1);
        var_canvas->Divide(1);
        var_canvas->cd(1);
        var_hist->Draw("HIST");
        var_hist->SetLineColor(4);
        var_hist->GetYaxis()->SetRangeUser(0,1.5*var_hist->GetMaximum()); 
        var_hist->GetXaxis()->SetTitle(var_str.c_str()); 
        var_hist->GetYaxis()->SetTitle(Form("Entries/%.2f",var_hist->GetBinWidth(1))); 
        gPad->Update(); gPad->Modified();
        TPaveStats * var_stats = (TPaveStats*) var_hist->FindObject("stats");
        var_stats->SetX1NDC(0.55); var_stats->SetX2NDC(0.83);
        var_stats->SetY1NDC(0.65); var_stats->SetY2NDC(0.79);
        var_stats->SetFillStyle(0); var_stats->SetTextFont(42);
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
        TLatex var_title_ltx;
        var_title_ltx.SetTextSize(0.03);
        var_title_ltx.DrawLatexNDC(0.2,0.825,Form("Cut %i, %s, %s",cut_number, cut_names[cut_number-1].c_str(), var_str.c_str()));  
        var_canvas->SaveAs(Form("cutflow/%s_cut-%i.png",output_str.c_str(),cut_number));
        delete var_canvas;

      }

      TCanvas * cutflow_canvas = new TCanvas("cutflow_canvas","",600,600,1500,1500);
      cutflow_canvas->Divide(1);
      cutflow_canvas->cd(1);
      cutflow_hist->Draw("HIST");
      TAxis * cutflow_axis = (TAxis*) cutflow_hist->GetXaxis();
      for (int bin_no = 1; bin_no <= ((int) cut_names.size()); ++bin_no){
        cutflow_axis->SetBinLabel(bin_no,(cut_names[bin_no-1]).c_str());
        cutflow_axis->ChangeLabel(bin_no,-90.0);
      }
      cutflow_hist->GetYaxis()->SetTitle("Events passed/cut");
      cutflow_hist->GetXaxis()->SetTitle("Cut");
      cutflow_hist->GetYaxis()->SetRangeUser(1.0,cutflow_hist->GetMaximum()*1.5);
      gPad->SetLogy();
      gPad->Update(); gPad->Modified();
      TPaveStats * cf_stats = (TPaveStats*) cutflow_hist->FindObject("stats");
      cf_stats->SetX1NDC(0.55); cf_stats->SetX2NDC(0.83);
      cf_stats->SetY1NDC(0.65); cf_stats->SetY2NDC(0.79);
      cf_stats->SetFillStyle(0); cf_stats->SetTextFont(42);
      ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
      wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
      TLatex cf_title_ltx;
      cf_title_ltx.SetTextSize(0.03);
      cf_title_ltx.DrawLatexNDC(0.2,0.825,Form("%s Cutflow",output_str.c_str()));  
      cutflow_canvas->SaveAs(Form("cutflow/%s_cutflow.png",output_str.c_str()));
      cutflow_canvas->Close();
      delete cutflow_canvas;


    }
};





bool VatoCS(const TLorentzVector &mupl,
	    const TLorentzVector &mumi,
	    const TLorentzVector &gamma,
	    TVector3 &CSAxis,
	    TVector3 &xAxis,
	    TVector3 &yAxis,
	    double &cosTheta_dimu,
	    double &cosTheta_gamma,
	    double &phi_dimu,
	    double &phi_gamma);

struct Jpsi
{
  Float_t DiMuonMass;
  Float_t DiMuonTau;
  Float_t DiMuonPt;
  Float_t DiLept_Y;
  Float_t DiLept_Phi;
  Float_t Trigger_HLT_2mu4_bJpsimumu_noL2;
  Float_t EventNumber;
  TLorentzVector mupl, mumi;
  TLorentzVector p4;
  Float_t old_lambda, old_qt2;
  Float_t old_csPhi, old_csCosTheta;
  // Int_t   JpsiIndex;
};
struct Photon
{
  Float_t PhotonPt;
  Float_t Photon_Eta;
  Float_t Photon_Phi;
  TLorentzVector p4;
  // Int_t  PhotonIndex;
};

bool VatoCS(const TLorentzVector &mupl,
	    const TLorentzVector &mumi,
	    const TLorentzVector &gamma,
	    TVector3 &CSAxis,
	    TVector3 &xAxis,
	    TVector3 &yAxis,
	    double &cosTheta_dimu,
	    double &cosTheta_gamma,
	    double &phi_dimu,
	    double &phi_gamma)
{

  TLorentzVector dimu = mupl + mumi;
  TLorentzVector higgs = dimu + gamma;

  double ProtonMass = 938.272;  // MeV
  double BeamEnergy = 6500000.; // MeV

  // 1)
  TLorentzVector p1, p2;
  p1.SetPxPyPzE(0., 0., BeamEnergy,
		TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass));
  p2.SetPxPyPzE(0., 0., -1 * BeamEnergy,
		TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass));

  // 2)
  TLorentzVector dimu_hf, mupl_hf, mumi_hf, gamma_hf, higgs_hf, p1_hf, p2_hf;

  dimu_hf = dimu;
  mupl_hf = mupl;
  mumi_hf = mumi;
  gamma_hf = gamma;
  higgs_hf = higgs;
  p1_hf = p1;
  p2_hf = p2;

  dimu_hf.Boost(-higgs.BoostVector());
  mupl_hf.Boost(-higgs.BoostVector());
  mumi_hf.Boost(-higgs.BoostVector());
  gamma_hf.Boost(-higgs.BoostVector());
  higgs_hf.Boost(-higgs.BoostVector());
  p1_hf.Boost(-higgs.BoostVector());
  p2_hf.Boost(-higgs.BoostVector());

  // 3)
  CSAxis = (p1_hf.Vect().Unit() - p2_hf.Vect().Unit()).Unit();
  yAxis = (p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);
  xAxis = xAxis.Unit();

  //4)

  phi_dimu = atan2((dimu_hf.Vect() * yAxis), (dimu_hf.Vect() * xAxis));
  cosTheta_dimu = dimu_hf.Vect().Dot(CSAxis) / (dimu_hf.Vect().Mag());

  // cross-check
  cosTheta_gamma = gamma_hf.Vect().Dot(CSAxis) / (gamma_hf.Vect().Mag());
  phi_gamma = atan2((gamma_hf.Vect() * yAxis), (gamma_hf.Vect() * xAxis));

  if (fabs(cosTheta_dimu + cosTheta_gamma) > 0.001)
    {
      std::cout << "VatoCS: cosThetaCS sum, beyond tolerance: " << cosTheta_dimu << " " << cosTheta_gamma << std::endl;
    }
  if (fabs(sin(phi_dimu - phi_gamma)) > 0.001)
    {
      std::cout << "VatoCS: phiCS difference, beyond tolerance: " << phi_dimu << " " << phi_gamma << std::endl;
    }
  return true;
}
