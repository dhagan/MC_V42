#include "TMVAGenerateInput.hxx"
#include <numeric>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

void split_strings(std::vector<std::string> & vec_split, std::string str_split, std::string delim){
  std::istringstream str_stream{str_split};
  std::string token;
  while( getline( str_stream, token, *(delim.c_str()) ) ){
    vec_split.push_back(token);
  }
}

void TMVAGenerateInput( const std::string file, const std::string input_type, const std::string cuts, 
    const std::string unique, const std::string ranges, const std::string cutflow_var, std::string eff_bins,
    std::string weight_var, bool eff=false, bool mig=false, bool truth=false, bool reco=false ){
    
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat("ourmen");
  gStyle->SetOptFit(1);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  int file_mode = 0;
  int pp_mix=0, ppmix_batch=0, train_int=0;

  if ( input_type.find("data") != std::string::npos ){ file_mode = 0; }
  if ( input_type.find("bckg") != std::string::npos ){ file_mode = 1; }
  if ( input_type.find("sign") != std::string::npos ){ file_mode = 2; }
  if ( input_type.find("bbbg") != std::string::npos ){ file_mode = 3; }

  TRandom3 * rand;
  bool mixing{false};
  if (file_mode == 1 && pp_mix == 1){
    std::cout << "mixing mode" << std::endl;
    mixing = true;
    rand = new TRandom3(5432);
  }
  std::string mix_file_str;
 
  std::vector<int> MassIndex = {0};
  std::vector<int> LambdaIndex = {0};
  std::vector<int> qTSquaredIndex = {0};
  std::vector<int> qTAIndex = {0};
  std::vector<int> PhiIndex = {0};
  std::vector<int> LifetimeIndex = {0};
  std::vector<int> CosThetaIndex = {0};

  Ranges MassRange =      {{2700.0,   3500.0}};     //J/psi mass intervals
  Ranges LambdaRange =    {{15.0,      200.0}};     //Lambda intervals
  Ranges qTSquaredRange = {{0.0,      400.0}};      //qtSquared intervals
  Ranges qtARange =       {{-5.0,     15.0}};       //qtA intervals  
  Ranges qtBRange =       {{0.0,      20.0}};       //qtB intervals
  Ranges LifetimeRange =  {{-5.0,     15.0}};       //lifetime ranges
  Ranges CosThetaRange =  {{0.0,      1.0}};        //cosThetaRange
  Ranges PhiRange =       {{0.0,      M_PI}};       //phi ranges
  Ranges PhotonPtRange =  {{5000.0,   DBL_MAX}};    //PhotonPt ranges
  Ranges MuonPtRange =    {{4000.0,   DBL_MAX}};    //MuonPt ranges
  Ranges CutflowRange =   {{-5.0,     15.0}};
  Ranges JpsiRange =      {{8.0,      30.0}};
  Ranges PhotRange =      {{5.0,      25.0}};
  Ranges DPhiRange =      {{0.0,      M_PI}};
  Ranges DiMuDZRange =    {{0,        DBL_MAX}};
  Ranges ActIpXRange =    {{0,        DBL_MAX}};
  Ranges DiMuonPtRange =  {{0,        DBL_MAX}};

  std::map< std::string, int > eff_bin;
  eff_bin["qtA"]  = 10;
  eff_bin["qtB"]  = 15;
  eff_bin["Phi"]  = 8;
  eff_bin["DPhi"] = 8;
  eff_bin["Phot"]  = 10;
  eff_bin["Jpsi"]  = 10;
  
  if ( !eff_bins.empty() ){
    std::vector< std::string > bin_vec;
    split_strings( bin_vec, eff_bins, ":");
    for ( std::string bin_str : bin_vec ){
      std::vector< std::string > split_bin;
      split_strings( split_bin, bin_str, "_");
      std::map< std::string, int >::iterator eff_bin_itr = eff_bin.find(split_bin.at( 0 ));
      eff_bin_itr->second = std::atoi( split_bin.at( 1 ).c_str() );
    }
  }

  double lambda_bins[6] = {0, 15, 25, 50, 100, 200};

  std::vector<std::string> full_range_vec;
  split_strings(full_range_vec, ranges,":");
  for ( std::string range_str : full_range_vec ){
    std::vector<std::string> var_vec, bounds;
    split_strings(var_vec, range_str,"_");
    double upper{DBL_MAX}, lower{-DBL_MAX};
    if ( var_vec.size() > 1 ){
      split_strings(bounds, var_vec.at(1),",");
      lower = std::stod(bounds.at(0));
      upper = ( bounds.size() < 2 ) ? DBL_MAX : std::stod(bounds.at(1));
    }
    if ( range_str.find("DiMuonMass") != std::string::npos ){
      MassRange.clear();
      MassRange.push_back({lower,upper});
    }
    if ( range_str.find("qtA") != std::string::npos ){
      qtARange.clear();
      qtARange.push_back({lower,upper});
    }
    if ( range_str.find("tau") != std::string::npos ){
      LifetimeRange.clear();
      LifetimeRange.push_back({lower,upper});
    }
    if ( range_str.find("qtB") != std::string::npos ){
      qtBRange.clear();
      qtBRange.push_back({lower,upper});
    }
    if ( range_str.find("PhotonPt") != std::string::npos ){
      PhotonPtRange.clear();
      PhotonPtRange.push_back({lower,upper});
    }
    //if ( range_str.find("MuonPt") != std::string::npos ){
    //  MuonPtRange.clear();
    //  MuonPtRange.push_back({lower,upper});
    //}
    if ( range_str.find("SingleMuonPt") != std::string::npos ){
      MuonPtRange.clear();
      MuonPtRange.push_back({lower,upper});
    }
    if ( range_str.find("Lambda") != std::string::npos ){
      LambdaRange.clear();
      LambdaRange.push_back({lower,upper});
    }
    if ( range_str.find("Cutflow") != std::string::npos ){
      CutflowRange.clear();
      CutflowRange.push_back({lower,upper});
    }
    if ( range_str.find("DZ") != std::string::npos ){
      DiMuDZRange.clear();
      DiMuDZRange.push_back({lower,upper});
    }
    if ( range_str.find("ActIpX") != std::string::npos ){
      ActIpXRange.clear();
      ActIpXRange.push_back({lower,upper});
    }
    if ( range_str.find( "DiMuonPt" ) != std::string::npos ){
      DiMuonPtRange.clear();
      DiMuonPtRange.push_back( { lower, upper } );
    }
  }

  //std::cout << "" << std::endl;
  //std::cout << DiMuonPtRange.at(0).first << std::endl;
  //std::cout << DiMuonPtRange.at(0).second << std::endl;
  //std::cout << MuonPtRange.at(0).first << std::endl;
  //std::cout << MuonPtRange.at(0).second << std::endl;
  //std::cout << "" << std::endl;


  cutflow cutflow_main(cutflow_var, CutflowRange.at(0), input_type, unique );

  if (!mixing){

    for (int i=0; i < PhotonPtRange.size(); i++){

      double photon_lower = std::round(PhotonPtRange.at(i).first);
      double photon_upper = std::round(PhotonPtRange.at(i).second);
      double muon_lower = std::round(MuonPtRange.at(i).first);
      double muon_upper = std::round(MuonPtRange.at(i).second);

      TChain *ch = new TChain("tree");
      ch->Add(file.c_str());
      treeReader *t = new treeReader(ch);
 
      Float_t AbsCosTheta, AbsdPhi, AbsdY, DPhi, DY, Phi, costheta, AbsPhi, EventNumber, Lambda, qTSquared, DiMuonMass, DiMuonTau,
              DiMuonPt, PhotonPt, Trigger_HLT_2mu4_bJpsimumu_noL2, qxpsi, qypsi, qxgamma, qygamma, qxsum, qysum, qtA,
              qtB, qtL, qtM, phot_dR;


      Float_t ActIpX, AvgIpX, JPsi_Eta, Phot_Eta, DeltaZ0;

      Float_t tr_DPhi, tr_Phi, tr_costheta,  tr_Lambda, tr_qTSquared, tr_DiMuonMass, tr_DiMuonTau,
              tr_DiMuonPt, tr_PhotonPt, tr_Trigger_HLT_2mu4_bJpsimumu_noL2, tr_qtA, tr_qtB, tr_JPsi_Eta, tr_Phot_Eta;

      Float_t tr_AbsdPhi, tr_DY, tr_AbsDY, tr_AbsPhi, tr_AbsCosTheta, tr_qxpsi, tr_qypsi, tr_qxgamma, tr_qygamma, tr_qxsum,
              tr_qysum, tr_qtL, tr_qtM;

      Float_t tr_ActIpX, tr_AvgIpX, tr_EventNumber, tr_DeltaZ0, tr_phot_dR;



      for (int l = 0; l < LambdaIndex.size(); l++){ // Loop through Lambda Indices
        for (int q = 0; q < qTAIndex.size(); q++){ // Loop through qTA Indices.  //##########################################################################
	        //          DECLARE THE VARIABLES AND LOAD THEM IN
	        //##########################################################################
	        UInt_t          run;
	        UInt_t          evt;
	        UInt_t          lb;
	        Float_t         actIpX;
	        Float_t         avgIpX;
          Float_t         pu_weight;
	        UInt_t          HLT_2mu4_bJpsimumu;
	        UInt_t          HLT_mu6_mu4_bJpsimumu;
	        UInt_t          HLT_2mu6_bJpsimumu;
	        UInt_t          HLT_mu10_mu6_bJpsimumu;
	        UInt_t          HLT_2mu10_bJpsimumu;
	        UInt_t          HLT_2mu4_bJpsimumu_delayed;
	        UInt_t          HLT_mu6_mu4_bJpsimumu_delayed;
	        UInt_t          HLT_2mu6_bJpsimumu_delayed;
	        UInt_t          HLT_mu10_mu6_bJpsimumu_delayed;
	        UInt_t          HLT_2mu10_bJpsimumu_delayed;
	        UInt_t          HLT_2mu4_bJpsimumu_noL2;
	        UInt_t          HLT_mu6_mu4_bJpsimumu_noL2;
	        UInt_t          HLT_2mu6_bJpsimumu_noL2;
	        UInt_t          HLT_mu10_mu6_bJpsimumu_noL2;
	        UInt_t          HLT_2mu10_bJpsimumu_noL2;
          std::vector<unsigned int> *Match_2mu4_bJpsimumu;
	        std::vector<unsigned int> *Match_mu6_mu4_bJpsimumu;
	        std::vector<unsigned int> *Match_2mu6_bJpsimumu;
	        std::vector<unsigned int> *Match_mu10_mu6_bJpsimumu;
	        std::vector<unsigned int> *Match_2mu10_bJpsimumu;
	        std::vector<unsigned int> *Match_2mu4_bJpsimumu_delayed;
	        std::vector<unsigned int> *Match_mu6_mu4_bJpsimumu_delayed;
	        std::vector<unsigned int> *Match_2mu6_bJpsimumu_delayed;
	        std::vector<unsigned int> *Match_mu10_mu6_bJpsimumu_delayed;
	        std::vector<unsigned int> *Match_2mu10_bJpsimumu_delayed;
	        std::vector<unsigned int> *Match_2mu4_bJpsimumu_noL2;
	        std::vector<unsigned int> *Match_mu6_mu4_bJpsimumu_noL2;
	        std::vector<unsigned int> *Match_2mu6_bJpsimumu_noL2;
	        std::vector<unsigned int> *Match_mu10_mu6_bJpsimumu_noL2;
	        std::vector<unsigned int> *Match_2mu10_bJpsimumu_noL2;
	        std::vector<float>   *MuPlus_Pt;
	        std::vector<float>   *MuPlus_Eta;
	        std::vector<float>   *MuPlus_Phi;
	        std::vector<float>   *MuPlus_E;
	        std::vector<float>   *MuPlus_M;
	        std::vector<float>   *mcMuPlus_Pt;
	        std::vector<float>   *mcMuPlus_Eta;
	        std::vector<float>   *mcMuPlus_Phi;
	        std::vector<float>   *mcMuPlus_M;
	        std::vector<int>     *MuPlus_quality;
	        std::vector<int>     *MuPlus_author;
	        std::vector<int>     *MuPlus_AODindex;
	        std::vector<float>   *MuPlus_z0;
	        std::vector<float>   *MuMinus_Pt;
	        std::vector<float>   *MuMinus_Eta;
	        std::vector<float>   *MuMinus_Phi;
	        std::vector<float>   *MuMinus_E;
	        std::vector<float>   *MuMinus_M;
	        std::vector<float>   *mcMuMinus_Pt;
	        std::vector<float>   *mcMuMinus_Eta;
	        std::vector<float>   *mcMuMinus_Phi;
	        std::vector<float>   *mcMuMinus_M;
	        std::vector<int>     *MuMinus_quality;
	        std::vector<int>     *MuMinus_author;
	        std::vector<int>     *MuMinus_AODindex;
	        std::vector<float>   *MuMinus_z0;
	        std::vector<float>   *Photon_Pt;
	        std::vector<float>   *Photon_Eta;
	        std::vector<float>   *Photon_Phi;
	        std::vector<float>   *Photon_E;
	        std::vector<float>   *Photon_M;
	        std::vector<float>   *mcPhoton_Pt;
	        std::vector<float>   *mcPhoton_Eta;
	        std::vector<float>   *mcPhoton_Phi;
	        std::vector<float>   *mcPhoton_M;
	        std::vector<float>   *Photon_TrkIso;
	        std::vector<float>   *Photon_CaloIso;
	        std::vector<float>   *Photon_CaloIso30;
	        std::vector<float>   *Photon_CaloIso40;
	        std::vector<int>     *Photon_quality;
	        std::vector<int>     *Photon_author;
	        std::vector<int>     *Photon_AODindex;
	        std::vector<float>   *Photon_etas0;
	        std::vector<float>   *Photon_etas1;
	        std::vector<float>   *Photon_etas2;
	        std::vector<float>   *TruthJpsi_Spx;
	        std::vector<float>   *TruthJpsi_Spy;
	        std::vector<float>   *TruthJpsi_Spz;
	        std::vector<float>   *TruthJpsi_Sm;
	        std::vector<float>   *DiLept_Pt;
	        std::vector<float>   *DiLept_Eta;
	        std::vector<float>   *DiLept_Phi;
	        std::vector<float>   *DiLept_Y;
	        std::vector<float>   *DiLept_E;
	        std::vector<float>   *DiLept_M;
	        std::vector<float>   *mcDiLept_Pt;
	        std::vector<float>   *mcDiLept_Eta;
	        std::vector<float>   *mcDiLept_Phi;
	        //std::vector<float>   *mcDiLept_Y;
	        std::vector<float>   *mcDiLept_M;
	        std::vector<float>   *mcDiLept_Dr;
	        std::vector<float>   *mcDiLeptFSR_Pt;
	        std::vector<float>   *mcDiLeptFSR_Eta;
	        std::vector<float>   *mcDiLeptFSR_Phi;
	        std::vector<float>   *mcDiLeptFSR_M;
	        std::vector<float>   *DiLept_Photon_Dphi;
	        std::vector<float>   *DiLept_TrkIso_ptcone20;
	        std::vector<float>   *DiLept_TrkIso_ptcone30;
	        std::vector<float>   *DiLept_TrkIso_ptcone40;
	        std::vector<float>   *DiLept_TrkIso_ptVarCone20;
	        std::vector<float>   *DiLept_TrkIso_ptVarCone30;
	        std::vector<float>   *DiLept_TrkIso_ptVarCone40;
	        std::vector<float>   *DiLept_CaloIso_etcone20;
	        std::vector<float>   *DiLept_CaloIso_etcone30;
	        std::vector<float>   *PosTrack_TrkIso_ptcone20;
	        std::vector<float>   *PosTrack_TrkIso_ptcone30;
	        std::vector<float>   *PosTrack_TrkIso_ptVarCone20;
	        std::vector<float>   *PosTrack_TrkIso_ptVarCone30;
	        std::vector<float>   *NegTrack_TrkIso_ptcone20;
	        std::vector<float>   *NegTrack_TrkIso_ptcone30;
	        std::vector<float>   *NegTrack_TrkIso_ptVarCone20;
	        std::vector<float>   *NegTrack_TrkIso_ptVarCone30;
	        std::vector<float>   *DiLept_dR;
	        std::vector<float>   *CosTheta;
	        std::vector<float>   *MuMuY_Pt;
	        std::vector<float>   *MuMuY_Eta;
	        std::vector<float>   *MuMuY_Phi;
	        std::vector<float>   *MuMuY_Y;
	        std::vector<float>   *MuMuY_E;
	        std::vector<float>   *MuMuY_M;
	        std::vector<float>   *mcMuMuY_Pt;
	        std::vector<float>   *mcMuMuY_Eta;
	        std::vector<float>   *mcMuMuY_Phi;
	        std::vector<float>   *mcMuMuY_M;
	        std::vector<float>   *MuMuY_dR;
	        std::vector<float>   *MuMuY_dPhi;
	        std::vector<float>   *mcMuMuY_dR;
	        std::vector<float>   *mcMuMuY_dPhi;
	        std::vector<float>   *DiMuonVertex_x;
	        std::vector<float>   *DiMuonVertex_y;
	        std::vector<float>   *DiMuonVertex_z;
	        std::vector<float>   *DiMuonVertex_ChiSq;
	        std::vector<float>   *DiMuonVertexxy;
	        std::vector<float>   *DiMuonVertexxy_Err;
	        std::vector<float>   *DiMuonVertex_Tau;
	        std::vector<float>   *DiMuonVertex_Tau_Err;
	        std::vector<float>   *DiMuonVertex_MinA0;
	        std::vector<float>   *DiMuonVertex_Err_MinA0;
	        std::vector<float>   *DiMuonVertex_Tau_MinA0;
	        std::vector<float>   *DiMuonVertex_Tau_Err_MinA0;
	        std::vector<float>   *DiMuonVertex_Mass;
	        std::vector<float>   *DiMuonVertex_Mass_Err;
	        std::vector<float>   *DiMuonVertex_Muon0_Px;
	        std::vector<float>   *DiMuonVertex_Muon0_Py;
	        std::vector<float>   *DiMuonVertex_Muon0_Pz;
	        std::vector<float>   *DiMuonVertex_Muon1_Px;
	        std::vector<float>   *DiMuonVertex_Muon1_Py;
	        std::vector<float>   *DiMuonVertex_Muon1_Pz;
	        std::vector<float>   *DiMuon_DeltaZ0;
	        std::vector<float>   *MuMuGamma_CS_CosTheta;
	        std::vector<float>   *MuMuGamma_CS_Phi;
	        std::vector<float>   *MuMuGamma_CSOLD_CosTheta;
	        std::vector<float>   *MuMuGamma_CSOLD_Phi;
	        std::vector<float>   *MuMu_CS_CosTheta;
	        std::vector<float>   *MuMu_CS_Phi;
	        std::vector<float>   *MuMuGamma_CosTheta;
	        std::vector<float>   *MuMuGamma_Phi;
	        std::vector<float>   *MuMu_CosTheta;
	        std::vector<float>   *MuMu_Phi;
	        std::vector<float>   *MuMuGamma_CosThetaOLD;
	        std::vector<float>   *MuMuGamma_PhiOLD;
	        std::vector<float>   *MuMu_CosThetaOLD;
	        std::vector<float>   *MuMu_PhiOLD;
	        std::vector<float>   *Truth_MuMuGamma_CS_CosTheta;
	        std::vector<float>   *Truth_MuMuGamma_CS_Phi;
	        std::vector<float>   *Truth_MuMuGamma_CSOLD_CosTheta;
	        std::vector<float>   *Truth_MuMuGamma_CSOLD_Phi;
	        std::vector<float>   *Truth_MuMu_CS_CosTheta;
	        std::vector<float>   *Truth_MuMu_CS_Phi;
	        std::vector<float>   *Truth_MuMuGamma_CosTheta;
	        std::vector<float>   *Truth_MuMuGamma_Phi;
	        std::vector<float>   *Truth_MuMu_CosTheta;
	        std::vector<float>   *Truth_MuMu_Phi;
	        std::vector<float>   *Truth_MuMuGamma_CosThetaOLD;
	        std::vector<float>   *Truth_MuMuGamma_PhiOLD;
	        std::vector<float>   *Truth_MuMu_CosThetaOLD;
	        std::vector<float>   *Truth_MuMu_PhiOLD;
	        std::vector<int>     *permIdx;
	        std::vector<int>     *jpsiIdx;
	        std::vector<int>     *photonIdx;


	        //###########################################
	        //            OPEN MAIN FOR LOOP
	        //###########################################
            
	        if (t->fChain == 0) return;

	        Long64_t nentries = t->fChain->GetEntries();
          std::cout << "Looping over " << nentries << " events" << std::endl;
          std::string dir = input_type;
          dir = dir + "_" +  unique;

          TFile * MC_Plots = new TFile (Form("./%s/%s_%s.root",input_type.c_str(),input_type.c_str(), unique.c_str()), "RECREATE");

          TFile * objstore;
          TTree * objtree;

          Float_t obj_EventNumber,obj_DiMuonPt, obj_DiMuonMass, obj_DiMuonTau, obj_HLT_2mu4_bJpsimumu_noL2, obj_Lambda, obj_qTsquaredPreSelection, obj_MuMuGamma_CS_Phi, obj_MuMuGamma_CS_CosTheta, obj_DiLept_Y,
          obj_DiLept_Ph, obj_DiMuonVertex_Muon0_Px, obj_DiMuonVertex_Muon0_Py, obj_DiMuonVertex_Muon0_Pz, obj_MuPlus_M, obj_DiMuonVertex_Muon1_Px, obj_DiMuonVertex_Muon1_Py, obj_DiMuonVertex_Muon1_Pz,
          obj_MuMinus_M, obj_DiLept_Pt, obj_DiLept_Eta, obj_DiLept_Phi, obj_DiLept_M, obj_Photon_Phi, obj_Photon_E, obj_Photon_Pt ,obj_Photon_Eta;

          

          
          if (file_mode == 1){

            objstore = new TFile(Form("./objstore/%s_objstore_%s.root",input_type.c_str(),unique.c_str()), "RECREATE");
	          objtree = new TTree("objtree", "tree");
            objtree->Branch("obj_EventNumber",                            &obj_EventNumber);
            objtree->Branch("obj_DiMuonPt",                               &obj_DiMuonPt);
            objtree->Branch("obj_DiMuonMass",                             &obj_DiMuonMass);
            objtree->Branch("obj_DiMuonTau",                              &obj_DiMuonTau);
            objtree->Branch("obj_HLT_2mu4_bJpsimumu_noL2",                &obj_HLT_2mu4_bJpsimumu_noL2);
            objtree->Branch("obj_Lambda",                                 &obj_Lambda);
            objtree->Branch("obj_qTsquaredPreSelection",                  &obj_qTsquaredPreSelection);
            objtree->Branch("obj_MuMuGamma_CS_Phi",                       &obj_MuMuGamma_CS_Phi);
            objtree->Branch("obj_MuMuGamma_CS_CosTheta",                  &obj_MuMuGamma_CS_CosTheta);
            objtree->Branch("obj_DiLept_Y",                               &obj_DiLept_Y);
            objtree->Branch("obj_DiLept_Phi",                             &obj_DiLept_Phi);
            objtree->Branch("obj_DiMuonVertex_Muon0_Px",                  &obj_DiMuonVertex_Muon0_Px);
            objtree->Branch("obj_DiMuonVertex_Muon0_Py",                  &obj_DiMuonVertex_Muon0_Py);
            objtree->Branch("obj_DiMuonVertex_Muon0_Pz",                  &obj_DiMuonVertex_Muon0_Pz);
            objtree->Branch("obj_MuPlus_M",                               &obj_MuPlus_M);
            objtree->Branch("obj_DiMuonVertex_Muon1_Px",                  &obj_DiMuonVertex_Muon1_Px);
            objtree->Branch("obj_DiMuonVertex_Muon1_Py",                  &obj_DiMuonVertex_Muon1_Py);
            objtree->Branch("obj_DiMuonVertex_Muon1_Pz",                  &obj_DiMuonVertex_Muon1_Pz);
            objtree->Branch("obj_MuMinus_M",                              &obj_MuMinus_M);
            objtree->Branch("obj_DiLept_Pt",                              &obj_DiLept_Pt);
            objtree->Branch("obj_DiLept_Eta",                             &obj_DiLept_Eta);
            objtree->Branch("obj_DiLept_Phi",                             &obj_DiLept_Phi);
            objtree->Branch("obj_DiLept_M",                               &obj_DiLept_M);
            objtree->Branch("obj_Photon_E",                               &obj_Photon_E);               
            objtree->Branch("obj_Photon_Pt",                              &obj_Photon_Pt);
            objtree->Branch("obj_Photon_Eta",                             &obj_Photon_Eta);            
            objtree->Branch("obj_Photon_Phi",                             &obj_Photon_Phi);           

          }

	        MC_Plots->cd();  //Selects this new root file for use.

	        TTree *tr = new TTree("TreeD", "tree");
	        tr->Branch("ActIpX", &ActIpX, "ActIpX/F");
	        tr->Branch("AvgIpX", &AvgIpX, "AvgIpX/F");
	        tr->Branch("AbsCosTheta", &AbsCosTheta, "AbsCosTheta/F");
	        tr->Branch("AbsdPhi", &AbsdPhi, "AbsdPhi/F");
	        tr->Branch("AbsPhi", &AbsPhi, "AbsPhi/F");
	        tr->Branch("DPhi", &DPhi, "DPhi/F");
	        tr->Branch("DY", &DY, "DY/F");
	        tr->Branch("Phi", &Phi, "phi/F");
	        tr->Branch("costheta", &costheta, "CosTheta/F");
	        tr->Branch("EventNumber", &EventNumber, "EventNumber/F");
	        tr->Branch("Lambda", &Lambda, "Lambda/F");
	        tr->Branch("qTSquared", &qTSquared, "qTSquared/F");
	        tr->Branch("DiMuonMass", &DiMuonMass, "DiMuonMass/F");
	        tr->Branch("DiMuonTau", &DiMuonTau, "DiMuonTau/F");
	        tr->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	        tr->Branch("DiMuonPt", &DiMuonPt, "DiMuonPt/F");
	        tr->Branch("PhotonPt", &PhotonPt, "PhotonPt/F");
	        tr->Branch("AbsdY", &AbsdY, "AbsdY");
	        tr->Branch("qxpsi", &qxpsi, "qxpsi");
	        tr->Branch("qypsi", &qypsi, "qypsi");
	        tr->Branch("qxgamma", &qxgamma, "qxgamma");
	        tr->Branch("qygamma", &qygamma, "qygamma");
	        tr->Branch("qxsum",  &qxsum, "qxsum");
	        tr->Branch("qysum",  &qysum, "qysum");
	        tr->Branch("qtA",  &qtA, "qtA");
	        tr->Branch("qtB",  &qtB, "qtB");
	        tr->Branch("qtL",  &qtL, "qtL");
	        tr->Branch("qtM",  &qtM, "qtM");
	        tr->Branch("phot_dR",  &phot_dR, "phot_dR");
	        tr->Branch("JPsi_Eta",  &JPsi_Eta, "JPsi_Eta");
	        tr->Branch("Phot_Eta",  &Phot_Eta, "Phot_Eta");
          tr->Branch("DeltaZ0", &DeltaZ0, "DeltaZ0" );

          Float_t rw_ActIpX, rw_AvgIpX, rw_DiMuonPt, rw_PhotonPt, rw_JPsi_Eta, rw_Phot_Eta, rw_Lambda, rw_costheta, rw_Phi;
          TFile * raw_weights = new TFile( Form( "./rweight_%s_%s.root", input_type.c_str(), unique.c_str() ), "RECREATE");
          TTree * rw_tr = new TTree("TreeD", "tree");
	        rw_tr->Branch("ActIpX",   &rw_ActIpX,    "ActIpX/F");
	        rw_tr->Branch("AvgIpX",   &rw_AvgIpX,    "ActIpX/F");
          rw_tr->Branch("DiMuonPt", &rw_DiMuonPt,  "DiMuonPt/F");
	        rw_tr->Branch("PhotonPt", &rw_PhotonPt,  "PhotonPt/F");
          rw_tr->Branch("JPsi_Eta", &rw_JPsi_Eta,  "JPsi_Eta");
	        rw_tr->Branch("Phot_Eta", &rw_Phot_Eta,  "Phot_Eta");
	        rw_tr->Branch("Lambda",   &rw_Lambda,    "Lambda/F");
	        rw_tr->Branch("costheta", &rw_costheta,  "CosTheta/F");
	        rw_tr->Branch("Phi",      &rw_Phi,       "phi/F");
          

	        Long64_t nbytes = 0, nb = 0;

          if (truth && input_type.find("sign") != std::string::npos){
            
            float truth_qtA, truth_qtB, truth_qt2;
            std::string truth_file_name;
            TFile * tr_file = new TFile( Form("./truth_sign/truth_sign_%s.root", unique.c_str()), "RECREATE");
            tr_file->cd();
            TTree *tr_tr = new TTree("TreeT", "tree");
              
	          tr_tr->Branch("ActIpX", &tr_ActIpX, "ActIpX");
	          tr_tr->Branch("AvgIpX", &tr_AvgIpX, "AvgIpX");
	          tr_tr->Branch("AbsCosTheta", &tr_AbsCosTheta, "AbsCosTheta");
            tr_tr->Branch("AbsdPhi", &tr_AbsdPhi, "AbsdPhi/F");
	          tr_tr->Branch("AbsPhi", &tr_AbsPhi, "AbsPhi/F");
            tr_tr->Branch("DPhi", &tr_DPhi, "DPhi/F");
	          tr_tr->Branch("DY", &tr_DY, "DY/F");
	          tr_tr->Branch("Phi", &tr_Phi, "phi/F");
	          tr_tr->Branch("costheta", &tr_costheta, "costheta/F");
	          tr_tr->Branch("EventNumber", &tr_EventNumber, "EventNumber" );
	          tr_tr->Branch("Lambda", &tr_Lambda, "Lambda/F");
	          tr_tr->Branch("qTSquared", &tr_qTSquared, "qTSquared/F");
	          tr_tr->Branch("DiMuonMass", &tr_DiMuonMass, "DiMuonMass/F");
            tr_tr->Branch("DiMuonTau", &tr_DiMuonTau, "DiMuonTau" );
	          tr_tr->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &tr_Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	          tr_tr->Branch("DiMuonPt", &tr_DiMuonPt, "DiMuonPt/F");
	          tr_tr->Branch("PhotonPt", &tr_PhotonPt, "PhotonPt/F");
            tr_tr->Branch("AbsdY", &tr_AbsDY, "AbsdY" );
            tr_tr->Branch("qxpsi", &tr_qxpsi, "qxpsi");
	          tr_tr->Branch("qypsi", &tr_qypsi, "qypsi");
	          tr_tr->Branch("qxgamma", &tr_qxgamma, "qxgamma");
	          tr_tr->Branch("qygamma", &tr_qygamma, "qygamma");
	          tr_tr->Branch("qxsum",  &tr_qxsum, "qxsum");
	          tr_tr->Branch("qysum",  &tr_qysum, "qysum");
	          tr_tr->Branch("qtA",  &tr_qtA, "qtA");
	          tr_tr->Branch("qtB",  &tr_qtB, "qtB");
            tr_tr->Branch("qtL",  &tr_qtL, "qtL");
	          tr_tr->Branch("qtM",  &tr_qtM, "qtM");
            tr_tr->Branch("phot_dR", &tr_phot_dR, "phot_dR");
	          tr_tr->Branch("JPsi_Eta",  &tr_JPsi_Eta, "JPsi_Eta");
	          tr_tr->Branch("Phot_Eta",  &tr_Phot_Eta, "Phot_Eta");
            tr_tr->Branch("DeltaZ0", &tr_DeltaZ0, "tr_DeltaZ0" );

            for (Long64_t jentry = 0; jentry < nentries; jentry++){

	            Long64_t ientry = t->LoadTree(jentry);
              if (ientry < 0) break;
	            nb = t->fChain->GetEntry(jentry);   nbytes += nb;

              tr_ActIpX = 0;
              tr_AvgIpX = 0;
              tr_EventNumber = 0;
              tr_DiMuonTau = 0;
              tr_DeltaZ0 = 0;
              tr_phot_dR = 0;

              if ( t->HLT_2mu4_bJpsimumu_noL2  ==  0 ){ continue; } 
              if ( t->mcPhoton_Pt->size()      ==  0 ){ continue; }
              if ( t->mcMuMinus_Pt->size()     ==  0 ){ continue; }
	            if ( t->mcMuPlus_Pt->size()      ==  0 ){ continue; }
              if ( t->mcPhoton_Pt->at(0) < photon_lower || t->mcPhoton_Pt->at(0) > photon_upper ) continue;
	            if ( t->mcMuMinus_Pt->at(0) < muon_lower || t->mcMuMinus_Pt->at(0) > muon_upper ) continue;
	            if ( t->mcMuPlus_Pt->at(0) < muon_lower || t->mcMuPlus_Pt->at(0) > muon_upper ) continue;

              float lambda = (t->mcMuMuY_M->at(0)/3097.0) * (t->mcMuMuY_M->at(0)/3097.0);
              if ( lambda < LambdaRange.at(0).first || lambda > LambdaRange.at(0).second  ){ continue; }
              if (t->mcDiLept_M->at(0) < MassRange.at(0).first || t->mcDiLept_M->at(0) > MassRange.at(0).second) continue;

              double abs_phi = abs(t->Truth_MuMuGamma_CS_Phi->at(0));
              if ( abs_phi < PhiRange.at(0).first || abs_phi > PhiRange.at(0).second ) continue;

	            double mcdPhi = (t->mcDiLept_Phi->at(0) - t->mcPhoton_Phi->at(0));
              while(  mcdPhi > M_PI ){ mcdPhi -= 2*M_PI; }
	            while(  mcdPhi < -M_PI ){ mcdPhi += 2*M_PI; }

              truth_qtA = (t->mcDiLept_Pt->at(0)/1000.0 - t->mcPhoton_Pt->at(0)/1000.0);
              truth_qtB = sqrt( t->mcDiLept_Pt->at(0)/1000.0 * t->mcPhoton_Pt->at(0)/1000.0) * sin(mcdPhi);
              truth_qt2 = (t->mcMuMuY_Pt->at(0)/1000.0)*(t->mcMuMuY_Pt->at(0)/1000.0);
              if ( abs(truth_qtB) < qtBRange.at(0).first || abs(truth_qtB) > qtBRange.at(0).second ){ continue; }
              if ( truth_qtA < qtARange.at(0).first || truth_qtA > qtARange.at(0).second ){ continue; }
              if ( truth_qt2 < qTSquaredRange.at(0).first || truth_qt2 > qTSquaredRange.at(0).second ){ continue; }
              if ( t->mcDiLept_Pt->at(0) < DiMuonPtRange.at(0).first || t->mcDiLept_Pt->at(0) > DiMuonPtRange.at(0).second ){ continue; }

              tr_DPhi = mcdPhi;
              tr_AbsdPhi = abs( mcdPhi );
              tr_Lambda = lambda;
              tr_qTSquared = truth_qt2;
              tr_DiMuonMass = t->mcDiLept_M->at(0);
              tr_DiMuonPt = t->mcDiLept_Pt->at(0);
              tr_PhotonPt = t->mcPhoton_Pt->at(0);
              tr_Trigger_HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;
              tr_qtA = truth_qtA;
              tr_qtB = truth_qtB;
              tr_JPsi_Eta = t->mcDiLept_Eta->at(0);
              tr_Phot_Eta = t->mcPhoton_Eta->at(0);

              tr_AbsPhi = abs_phi;
              tr_Phi = (t->Truth_MuMuGamma_CS_Phi->at(0) >= 0 ? (t->Truth_MuMuGamma_CS_Phi->at(0) - M_PI ) : (t->Truth_MuMuGamma_CS_Phi->at(0) + M_PI));
	        	  tr_costheta = t->Truth_MuMuGamma_CS_CosTheta->at(0);
              tr_AbsCosTheta = abs( tr_costheta ); 
	        	  tr_DY = t->mcDiLept_Eta->at(0) - t->mcPhoton_Eta->at(0);
	        	  tr_AbsDY = abs( tr_DY );

	        	  tr_qxpsi    = tr_DiMuonPt/1000 + (tr_PhotonPt/1000)*cos(tr_DPhi);
	        	  tr_qypsi    = (tr_PhotonPt/1000)*sin(tr_DPhi);
	        	  tr_qxgamma  = tr_PhotonPt/1000 + (tr_DiMuonPt/1000)*cos(tr_DPhi);
	        	  tr_qygamma  = (tr_DiMuonPt/1000) *sin(tr_DPhi);
	        	  tr_qxsum    = (tr_DiMuonPt/1000 + tr_PhotonPt/1000)*(1+cos(tr_DPhi));
	        	  tr_qysum    = (tr_DiMuonPt/1000 + tr_PhotonPt/1000)*(sin(tr_DPhi));

	        	  tr_qtL = ((tr_DiMuonPt/1000) - (tr_PhotonPt/1000))*cos((M_PI-abs(tr_DPhi))/2.0);
	        	  tr_qtM = ((tr_DiMuonPt/1000) - (tr_PhotonPt/1000))*sin((M_PI-abs(tr_DPhi))/2.0);


              tr_tr->Fill();
            }

            tr_tr->Write();

            MC_Plots->cd();

          }


          if (mig && input_type.find("sign") != std::string::npos){
            float reco_qtA, reco_qtB, truth_qtA, truth_qtB, truth_qt2, reco_qt2;
            std::vector<TH2F> qta_migration_vec;
            std::vector<TH2F> qtb_migration_vec;
            std::vector<TH2F> lambda_migration_vec;
            std::vector<int> mass_bins = { 0, 3, 4, 5, 12, 0, 3, 4, 5, 12 };  
            int mass_count = 0;
            std::string mass_char = "Q";
            for (int & bin_no : mass_bins){            
              if ( mass_count > 5 ){
                mass_char = "T";
              }
              qta_migration_vec.push_back( TH2F( Form( "mig_qtA_%s%i", mass_char.c_str(), bin_no ), "",
                    60, qtARange.at(0).first, qtARange.at(0).second,
                    60, qtARange.at(0).first, qtARange.at(0).second ) );
              qtb_migration_vec.push_back( TH2F( Form( "mig_qtB_%s%i", mass_char.c_str(), bin_no ), "",
                    60, qtBRange.at(0).first, qtBRange.at(0).second,
                    60, qtBRange.at(0).first, qtBRange.at(0).second ) );
              lambda_migration_vec.push_back( TH2F( Form( "mig_lambda_%s%i", mass_char.c_str(), bin_no ), "", 5,  lambda_bins, 5, lambda_bins ) );
              mass_count++;
            }
            for ( int i = 0;  i < (int) qta_migration_vec.size(); i++ ){ 
              qta_migration_vec.at(i).Sumw2();
              qtb_migration_vec.at(i).Sumw2();
              lambda_migration_vec.at(i).Sumw2();
            }
            
            for (Long64_t jentry = 0; jentry < nentries; jentry++){
	            Long64_t ientry = t->LoadTree( jentry );
              if ( ientry < 0 ) break;
	            nb = t->fChain->GetEntry( jentry ); nbytes += nb;

              if (t->HLT_2mu4_bJpsimumu_noL2  ==  0 ){ continue; }
              if (t->Photon_Pt->size()      ==  0   ){ continue; }
              if (t->MuMinus_Pt->size()     ==  0   ){ continue; }
	            if (t->MuPlus_Pt->size()      ==  0   ){ continue; }
              if ( t->Photon_Pt->at(0) < photon_lower || t->Photon_Pt->at(0) > photon_upper ) continue;
	            if ( t->MuMinus_Pt->at(0) < muon_lower || t->MuMinus_Pt->at(0) > muon_upper )   continue;
	            if ( t->MuPlus_Pt->at(0) < muon_lower || t->MuPlus_Pt->at(0) > muon_upper )   continue;

              float lambda = (t->MuMuY_M->at(0)/3097.0) * (t->MuMuY_M->at(0)/3097.0);
              if ( lambda < LambdaRange.at(0).first || lambda > LambdaRange.at(0).second  ){ continue; }
              if ( t->DiLept_M->at(0) < MassRange.at(0).first || t->DiLept_M->at(0) > MassRange.at(0).second ){ continue; }

              double abs_phi = abs(t->MuMuGamma_CS_Phi->at(0));
              if ( abs_phi < PhiRange.at(0).first || abs_phi > PhiRange.at(0).second ) continue;

	            double dPhi = (t->DiLept_Phi->at(0) - t->Photon_Phi->at(0));
              while(  dPhi   >    M_PI ){  dPhi   -=  2*M_PI;   }
	            while(  dPhi   <   -M_PI ){  dPhi   +=  2*M_PI;   }
              if ( t->DiMuonVertex_Tau->at(0) < LifetimeRange.at(0).first ){ continue; }
              if ( t->DiMuonVertex_Tau->at(0) > LifetimeRange.at(0).second ){ continue; }
              reco_qtA = (t->DiLept_Pt->at(0)/1000.0 - t->Photon_Pt->at(0)/1000.0);
              reco_qtB = sqrt( t->DiLept_Pt->at(0)/1000.0 * t->Photon_Pt->at(0)/1000.0) * sin(dPhi);
              reco_qt2 = (t->MuMuY_Pt->at(0)/1000.0)*(t->MuMuY_Pt->at(0)/1000.0);
              //if ( abs(reco_qtB) < qtBRange.at(0).first || abs(reco_qtB) > qtBRange.at(0).second ){ continue; }
              //if ( reco_qtA < qtARange.at(0).first || reco_qtA > qtARange.at(0).second ){ continue; }
              //if ( reco_qt2 < qTSquaredRange.at(0).first || reco_qt2 > qTSquaredRange.at(0).second ){ continue; }

              if ( file_mode == 2 ){
                if ( t->Photon_quality->at(0) > 1) continue;
                TLorentzVector tlv_truth_photon,tlv_reco_photon,tlv_truth_muplus,tlv_reco_muplus;
                tlv_reco_photon.SetPtEtaPhiE(t->Photon_Pt ->at(0),t->Photon_Eta->at(0),t->Photon_Phi->at(0),t->Photon_E->at(0));
                tlv_truth_photon.SetPtEtaPhiM(t->mcPhoton_Pt ->at(0),t->mcPhoton_Eta->at(0),t->mcPhoton_Phi->at(0),0.);
                Float_t PhotonDRTruthReco  = tlv_reco_photon.DeltaR(tlv_truth_photon) ;
                if (PhotonDRTruthReco > 0.12) continue;

              }
              if ( abs(t->DiMuon_DeltaZ0->at(0)) < DiMuDZRange.at(0).first || abs(t->DiMuon_DeltaZ0->at(0) ) > DiMuDZRange.at(0).second ){ continue; }
              std::vector<int> write_bins = {};
              if ( lambda > 15  && lambda < 200 ){ write_bins.push_back(0); }
              if ( lambda > 25  && lambda < 50  ){ write_bins.push_back(1); }
              if ( lambda > 50  && lambda < 100 ){ write_bins.push_back(2); }
              if ( lambda > 100 && lambda < 200 ){ write_bins.push_back(3); }
              if ( lambda > 25  && lambda < 200 ){ write_bins.push_back(4); }
              if ( lambda > 15  ){ write_bins.push_back(5); }
              if ( lambda > 25  ){ write_bins.push_back(6); }
              if ( lambda > 50  ){ write_bins.push_back(7); }
              if ( lambda > 100 ){ write_bins.push_back(8); }
              if ( lambda > 25  ){ write_bins.push_back(9); }

              
              truth_qtA = (t->mcDiLept_Pt->at(0)/1000.0 - t->mcPhoton_Pt->at(0)/1000.0);
              double mcdPhi = (t->mcDiLept_Phi->at(0) - t->mcPhoton_Phi->at(0));
              while(  mcdPhi > M_PI ){ mcdPhi -= 2*M_PI; }
	            while(  mcdPhi < -M_PI ){ mcdPhi += 2*M_PI; }
              truth_qtB = sqrt( t->mcDiLept_Pt->at(0)/1000.0 * t->mcPhoton_Pt->at(0)/1000.0) * sin(mcdPhi);

              for ( int & bin : write_bins){
                qta_migration_vec.at(bin).Fill(truth_qtA, reco_qtA );
                qtb_migration_vec.at(bin).Fill(truth_qtB, reco_qtB );
                lambda_migration_vec.at(bin).Fill( (t->mcMuMuY_M->at(0)/3097.0) * (t->mcMuMuY_M->at(0)/3097.0), lambda );
              }
            }
            TFile * mig_file = new TFile(Form("./Migrations_%s.root",input_type.c_str()),"RECREATE");
            for (int mass_idx = 0; mass_idx < qta_migration_vec.size(); mass_idx++){
               qta_migration_vec[mass_idx].Write();
               qtb_migration_vec[mass_idx].Write();
               lambda_migration_vec[mass_idx].Write();
            }
            delete mig_file;
            return;
          }
          
          nbytes = 0; nb = 0;
          if (eff && input_type.find("sign") != std::string::npos){
            float reco_qtA, reco_qtB, truth_qtA, truth_qtB, truth_qt2, reco_qt2;

            std::vector<TH1F> hist_truth_qta_vec;
            std::vector<TH1F> hist_truth_qtb_vec;
            std::vector<TH1F> hist_truth_jpsi_pt_vec;
            std::vector<TH1F> hist_truth_phot_pt_vec;
            std::vector<TH1F> hist_truth_dphi_vec;
            std::vector<TH1F> hist_truth_phi_vec;
            std::vector<TH1F> hist_truth_phi_low_vec;
            std::vector<TH1F> hist_truth_phi_high_vec;
            std::vector<TH1F> hist_reco_qta_vec;
            std::vector<TH1F> hist_reco_qtb_vec;
            std::vector<TH1F> hist_reco_jpsi_pt_vec;
            std::vector<TH1F> hist_reco_phot_pt_vec;
            std::vector<TH1F> hist_reco_dphi_vec;
            std::vector<TH1F> hist_reco_phi_vec;
            std::vector<TH1F> hist_reco_phi_low_vec;
            std::vector<TH1F> hist_reco_phi_high_vec;

            //std::vector<int> mass_bins = {0,3,4,5,12};  
            std::vector<int> mass_bins = { 0, 3, 4, 5, 12, 0, 3, 4, 5, 12 };  
  
            TFile * weight_file;
            std::vector<TH1F*> weight_hists;
            std::vector< std::string > weight_strs;

            if ( !weight_var.empty() ){
              split_strings( weight_strs, weight_var, "_" );
              if ( weight_strs.size() == 1 ){
                weight_file = new TFile("/home/atlas/dhagan/analysis/analysisFork/445"
                                            "/study/weight_sys/build/base/weights.root","READ");
                weight_var = weight_strs.at(0);
              } else if ( weight_strs.size() == 2 ){
                weight_file = new TFile( Form("/home/atlas/dhagan/analysis/analysisFork/445"
                                            "/study/weight_sys/build/%s/weights.root", weight_strs.at(0).c_str() ),"READ");

                weight_var = weight_strs.at(1);
              }
              int mass_count = 0;
              for ( int & mass : mass_bins ){
                if (mass_count < 5 ){
                  weight_hists.push_back( (TH1F*) weight_file->Get( Form( "%s_Q%i_weight", weight_var.c_str(), mass ) ) ); 
                }
                if (mass_count >= 5){
                  weight_hists.push_back( (TH1F*) weight_file->Get( Form( "%s_T%i_weight", weight_var.c_str(), mass ) ) ); 
                }
              }
            }
   

            int mass_count = 0;
            std::string mass_char{"Q"};
            for (int & bin_no : mass_bins){            
              if ( mass_count >= 5){
                mass_char = "T";
              }
              mass_count++;

              hist_reco_qta_vec.push_back(             TH1F(   Form("reco_qtA_%s%i", mass_char.c_str(),       bin_no ), "",
                    eff_bin.find("qtA")->second, qtARange.at(0).first, qtARange.at(0).second ) );
              hist_truth_qta_vec.push_back(            TH1F(   Form("truth_qtA_%s%i", mass_char.c_str(),      bin_no ), "",
                    eff_bin.find("qtA")->second, qtARange.at(0).first, qtARange.at(0).second ) );
              hist_reco_qtb_vec.push_back(             TH1F(   Form("reco_qtB_%s%i", mass_char.c_str(),       bin_no ), "",
                    eff_bin.find("qtB")->second, qtBRange.at(0).first, qtBRange.at(0).second ) );
              hist_truth_qtb_vec.push_back(            TH1F(   Form("truth_qtB_%s%i", mass_char.c_str(),      bin_no ), "",
                    eff_bin.find("qtB")->second, qtBRange.at(0).first, qtBRange.at(0).second ) );
              hist_reco_jpsi_pt_vec.push_back(         TH1F(   Form("reco_DiMuonPt_%s%i", mass_char.c_str(),   bin_no ), "",
                    eff_bin.find("Jpsi")->second, JpsiRange.at(0).first, JpsiRange.at(0).second ) );
              hist_truth_jpsi_pt_vec.push_back(        TH1F(   Form("truth_DiMuonPt_%s%i", mass_char.c_str(),  bin_no ), "",
                    eff_bin.find("Jpsi")->second, JpsiRange.at(0).first, JpsiRange.at(0).second ) );
              hist_reco_phot_pt_vec.push_back(         TH1F(   Form("reco_PhotonPt_%s%i", mass_char.c_str(),   bin_no ), "",
                    eff_bin.find("Phot")->second, PhotRange.at(0).first, PhotRange.at(0).second ) );
              hist_truth_phot_pt_vec.push_back(        TH1F(   Form("truth_PhotonPt_%s%i", mass_char.c_str(),  bin_no ), "",
                    eff_bin.find("Phot")->second, PhotRange.at(0).first, PhotRange.at(0).second ) );
              hist_reco_dphi_vec.push_back(            TH1F(   Form("reco_DPhi_%s%i", mass_char.c_str(),      bin_no ), "",
                    eff_bin.find("DPhi")->second, DPhiRange.at(0).first, DPhiRange.at(0).second ) );
              hist_truth_dphi_vec.push_back(           TH1F(   Form("truth_DPhi_%s%i", mass_char.c_str(),     bin_no ), "",
                    eff_bin.find("DPhi")->second, DPhiRange.at(0).first, DPhiRange.at(0).second ) );
              hist_reco_phi_vec.push_back(             TH1F(   Form("reco_Phi_%s%i", mass_char.c_str(),       bin_no ), "",
                    eff_bin.find("Phi")->second, PhiRange.at(0).first, PhiRange.at(0).second ) );
              hist_truth_phi_vec.push_back(            TH1F(   Form("truth_Phi_%s%i", mass_char.c_str(),      bin_no ), "",
                    eff_bin.find("Phi")->second, PhiRange.at(0).first, PhiRange.at(0).second ) );
              hist_reco_phi_low_vec.push_back(         TH1F(   Form("reco_Phi_low_%s%i", mass_char.c_str(),   bin_no ), "",
                    eff_bin.find("Phi")->second, PhiRange.at(0).first, PhiRange.at(0).second ) );
              hist_truth_phi_low_vec.push_back(        TH1F(   Form("truth_Phi_low_%s%i", mass_char.c_str(),  bin_no ), "",
                    eff_bin.find("Phi")->second, PhiRange.at(0).first, PhiRange.at(0).second ) );
              hist_reco_phi_high_vec.push_back(        TH1F(   Form("reco_Phi_high_%s%i", mass_char.c_str(),  bin_no ), "",
                    eff_bin.find("Phi")->second, PhiRange.at(0).first, PhiRange.at(0).second ) );
              hist_truth_phi_high_vec.push_back(       TH1F(   Form("truth_Phi_high_%s%i", mass_char.c_str(), bin_no ), "",
                    eff_bin.find("Phi")->second, PhiRange.at(0).first, PhiRange.at(0).second ) );
            }

            for ( int i = 0;  i < (int) hist_reco_qta_vec.size(); i++ ){ 
              hist_reco_qta_vec.at(i).Sumw2();
              hist_truth_qta_vec.at(i).Sumw2();
              hist_reco_qtb_vec.at(i).Sumw2();
              hist_truth_qtb_vec.at(i).Sumw2();
              hist_reco_jpsi_pt_vec.at(i).Sumw2();
              hist_truth_jpsi_pt_vec.at(i).Sumw2();
              hist_reco_phot_pt_vec.at(i).Sumw2();
              hist_truth_phot_pt_vec.at(i).Sumw2();
              hist_reco_dphi_vec.at(i).Sumw2();
              hist_truth_dphi_vec.at(i).Sumw2();
              hist_reco_phi_vec.at(i).Sumw2();
              hist_truth_phi_vec.at(i).Sumw2();
              hist_reco_phi_low_vec.at(i).Sumw2();
              hist_truth_phi_low_vec.at(i).Sumw2();
              hist_reco_phi_high_vec.at(i).Sumw2();
              hist_truth_phi_high_vec.at(i).Sumw2();
            }
          
	          for (Long64_t jentry = 0; jentry < nentries; jentry++){

	            Long64_t ientry = t->LoadTree(jentry);
              if (ientry < 0) break;
	            nb = t->fChain->GetEntry(jentry);   nbytes += nb;

              if ( t->HLT_2mu4_bJpsimumu_noL2  ==  0 ){ continue; } 
              if ( t->mcPhoton_Pt->size()      ==  0 ){ continue; }
              if ( t->mcMuMinus_Pt->size()     ==  0 ){ continue; }
	            if ( t->mcMuPlus_Pt->size()      ==  0 ){ continue; }
              if ( t->mcPhoton_Pt->at(0) < photon_lower || t->mcPhoton_Pt->at(0) > photon_upper ) continue;
	            if ( t->mcMuMinus_Pt->at(0) < muon_lower || t->mcMuMinus_Pt->at(0) > muon_upper ) continue;
	            if ( t->mcMuPlus_Pt->at(0) < muon_lower || t->mcMuPlus_Pt->at(0) > muon_upper ) continue;

              float lambda = (t->mcMuMuY_M->at(0)/3097.0) * (t->mcMuMuY_M->at(0)/3097.0);
              if ( lambda < LambdaRange.at(0).first || lambda > LambdaRange.at(0).second  ){ continue; }
              if (t->mcDiLept_M->at(0) < MassRange.at(0).first || t->mcDiLept_M->at(0) > MassRange.at(0).second) continue;

              double abs_phi = abs(t->Truth_MuMuGamma_CS_Phi->at(0));
              if ( abs_phi < PhiRange.at(0).first || abs_phi > PhiRange.at(0).second ) continue;

	            double mcdPhi = (t->mcDiLept_Phi->at(0) - t->mcPhoton_Phi->at(0));
              while(  mcdPhi > M_PI ){ mcdPhi -= 2*M_PI; }
	            while(  mcdPhi < -M_PI ){ mcdPhi += 2*M_PI; }

              truth_qtA = (t->mcDiLept_Pt->at(0)/1000.0 - t->mcPhoton_Pt->at(0)/1000.0);
              truth_qtB = sqrt( t->mcDiLept_Pt->at(0)/1000.0 * t->mcPhoton_Pt->at(0)/1000.0) * sin(mcdPhi);
              truth_qt2 = (t->mcMuMuY_Pt->at(0)/1000.0)*(t->mcMuMuY_Pt->at(0)/1000.0);
              if ( abs(truth_qtB) < qtBRange.at(0).first || abs(truth_qtB) > qtBRange.at(0).second ){ continue; }
              if ( truth_qtA < qtARange.at(0).first || truth_qtA > qtARange.at(0).second ){ continue; }
              if ( truth_qt2 < qTSquaredRange.at(0).first || truth_qt2 > qTSquaredRange.at(0).second ){ continue; }

              std::vector<int> write_bins = {};
              if ( lambda > 15  && lambda < 200 ){ write_bins.push_back(0); }
              if ( lambda > 25  && lambda < 50  ){ write_bins.push_back(1); }
              if ( lambda > 50  && lambda < 100 ){ write_bins.push_back(2); }
              if ( lambda > 100 && lambda < 200 ){ write_bins.push_back(3); }
              if ( lambda > 25  && lambda < 200 ){ write_bins.push_back(4); }
              if ( lambda > 15  ){ write_bins.push_back(5); }
              if ( lambda > 25  ){ write_bins.push_back(6); }
              if ( lambda > 50  ){ write_bins.push_back(7); }
              if ( lambda > 100 ){ write_bins.push_back(8); }
              if ( lambda > 25  ){ write_bins.push_back(9); }


              

              for ( int & bin : write_bins ){

                double weight = 1.0;
                if ( weight_var.empty() ){
                  weight = 1.0;
                } else if ( weight_var.find( "DiMuonPt" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->mcDiLept_Pt->at(0) ) );
                } else if ( weight_var.find( "Mass" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin(
                        t->mcMuMuY_M->at(0)/1000.0 ) );
                } else if ( weight_var.find( "PhotonPt" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->mcPhoton_Pt->at(0) ) );
                } else if ( weight_var.find( "z" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->Truth_MuMuGamma_CS_CosTheta->at(0) ) );
                } else if ( weight_var.find( "Phi" ) != std::string::npos ){
                  double w_Phi = (t->Truth_MuMuGamma_CS_Phi->at(0) >= 0 ? (t->Truth_MuMuGamma_CS_Phi->at(0) - M_PI ) : (t->Truth_MuMuGamma_CS_Phi->at(0) + M_PI));
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( abs(w_Phi) ) );
                } else if ( weight_var.find( "JPsi_Eta" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->mcDiLept_Eta->at(0) ) );
                } else if ( weight_var.find( "Phot_Eta" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->mcPhoton_Eta->at(0) ) );
                } else if ( weight_var.find( "ActIpX" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->actIpX ) );
                } else if ( weight_var.find( "AvgIpX" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->avgIpX ) );
                } else if ( weight_var.find( "PU" ) != std::string::npos ){
                  weight = t->pu_weight;
                }

                hist_truth_qta_vec.at(bin).Fill(truth_qtA, weight );
                hist_truth_qtb_vec.at(bin).Fill(abs(truth_qtB), weight );
                hist_truth_jpsi_pt_vec.at(bin).Fill(t->mcDiLept_Pt->at(0)/1000.0, weight );
                hist_truth_phot_pt_vec.at(bin).Fill(t->mcPhoton_Pt->at(0)/1000.0, weight );
                hist_truth_dphi_vec.at(bin).Fill(abs(mcdPhi), weight);
                hist_truth_phi_vec.at(bin).Fill(abs(t->Truth_MuMuGamma_CS_Phi->at(0)),weight );
                
                if ( (t->Truth_MuMuGamma_CS_CosTheta->at(0) * t->Truth_MuMuGamma_CS_CosTheta->at(0)) < 0.1 ){
                  hist_truth_phi_low_vec.at(bin).Fill(abs(t->Truth_MuMuGamma_CS_Phi->at(0)),weight);
                } else if ( (t->Truth_MuMuGamma_CS_CosTheta->at(0) * t->Truth_MuMuGamma_CS_CosTheta->at(0)) > 0.1 ){
                  hist_truth_phi_high_vec.at(bin).Fill(abs(t->Truth_MuMuGamma_CS_Phi->at(0)),weight);
                }
              }
            }

            for (Long64_t jentry = 0; jentry < nentries; jentry++){
	            Long64_t ientry = t->LoadTree( jentry );
              if ( ientry < 0 ) break;
	            nb = t->fChain->GetEntry( jentry ); nbytes += nb;

              if (t->HLT_2mu4_bJpsimumu_noL2  ==  0 ){ continue; }
              if (t->Photon_Pt->size()      ==  0   ){ continue; }
              if (t->MuMinus_Pt->size()     ==  0   ){ continue; }
	            if (t->MuPlus_Pt->size()      ==  0   ){ continue; }
              if ( t->Photon_Pt->at(0) < photon_lower || t->Photon_Pt->at(0) > photon_upper ) continue;
	            if ( t->MuMinus_Pt->at(0) < muon_lower || t->MuMinus_Pt->at(0) > muon_upper )   continue;
	            if ( t->MuPlus_Pt->at(0) < muon_lower || t->MuPlus_Pt->at(0) > muon_upper )   continue;

              float lambda = (t->MuMuY_M->at(0)/3097.0) * (t->MuMuY_M->at(0)/3097.0);
              if ( lambda < LambdaRange.at(0).first || lambda > LambdaRange.at(0).second  ){ continue; }
              if ( t->DiLept_M->at(0) < MassRange.at(0).first || t->DiLept_M->at(0) > MassRange.at(0).second ){ continue; }

              double abs_phi = abs(t->MuMuGamma_CS_Phi->at(0));
              if ( abs_phi < PhiRange.at(0).first || abs_phi > PhiRange.at(0).second ) continue;

	            double dPhi = (t->DiLept_Phi->at(0) - t->Photon_Phi->at(0));
              while(  dPhi   >    M_PI ){  dPhi   -=  2*M_PI;   }
	            while(  dPhi   <   -M_PI ){  dPhi   +=  2*M_PI;   }
              
              if ( t->DiMuonVertex_Tau->at(0) < LifetimeRange.at(0).first ){ continue; }
              if ( t->DiMuonVertex_Tau->at(0) > LifetimeRange.at(0).second ){ continue; }

              reco_qtA = (t->DiLept_Pt->at(0)/1000.0 - t->Photon_Pt->at(0)/1000.0);
              reco_qtB = sqrt( t->DiLept_Pt->at(0)/1000.0 * t->Photon_Pt->at(0)/1000.0) * sin(dPhi);
              reco_qt2 = (t->MuMuY_Pt->at(0)/1000.0)*(t->MuMuY_Pt->at(0)/1000.0);
              if ( abs(reco_qtB) < qtBRange.at(0).first || abs(reco_qtB) > qtBRange.at(0).second ){ continue; }
              if ( reco_qtA < qtARange.at(0).first || reco_qtA > qtARange.at(0).second ){ continue; }
              if ( reco_qt2 < qTSquaredRange.at(0).first || reco_qt2 > qTSquaredRange.at(0).second ){ continue; }

              if ( file_mode == 2 ){
                if ( t->Photon_quality->at(0) > 1) continue;
                TLorentzVector tlv_truth_photon,tlv_reco_photon,tlv_truth_muplus,tlv_reco_muplus;
                tlv_reco_photon.SetPtEtaPhiE(t->Photon_Pt ->at(0),t->Photon_Eta->at(0),t->Photon_Phi->at(0),t->Photon_E->at(0));
                tlv_truth_photon.SetPtEtaPhiM(t->mcPhoton_Pt ->at(0),t->mcPhoton_Eta->at(0),t->mcPhoton_Phi->at(0),0.);
                Float_t PhotonDRTruthReco  = tlv_reco_photon.DeltaR(tlv_truth_photon) ;
                if (PhotonDRTruthReco > 0.12) continue;
              }

              if ( abs(t->DiMuon_DeltaZ0->at(0)) < DiMuDZRange.at(0).first || abs(t->DiMuon_DeltaZ0->at(0) ) > DiMuDZRange.at(0).second ){ continue; }

              std::vector<int> write_bins = {};
              if ( lambda > 15  && lambda < 200 ){ write_bins.push_back(0); }
              if ( lambda > 25  && lambda < 50  ){ write_bins.push_back(1); }
              if ( lambda > 50  && lambda < 100 ){ write_bins.push_back(2); }
              if ( lambda > 100 && lambda < 200 ){ write_bins.push_back(3); }
              if ( lambda > 25  && lambda < 200 ){ write_bins.push_back(4); }
              if ( lambda > 15  ){ write_bins.push_back(5); }
              if ( lambda > 25  ){ write_bins.push_back(6); }
              if ( lambda > 50  ){ write_bins.push_back(7); }
              if ( lambda > 100 ){ write_bins.push_back(8); }
              if ( lambda > 25  ){ write_bins.push_back(9); }


              for ( int & bin : write_bins){

                double weight = 1.0;
                if ( weight_var.empty() ){
                  weight = 1.0;
                } else if ( weight_var.find( "DiMuonPt" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->DiLept_Pt->at(0) ) );
                } else if ( weight_var.find( "Mass" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin(
                        t->MuMuY_M->at(0)/1000.0 ) );
                } else if ( weight_var.find( "PhotonPt" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->Photon_Pt->at(0) ) );
                } else if ( weight_var.find( "z" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->MuMuGamma_CS_CosTheta->at(0) ) );
                } else if ( weight_var.find( "Phi" ) != std::string::npos ){
                  double w_Phi = (t->MuMuGamma_CS_Phi->at(0) >= 0 ? (t->MuMuGamma_CS_Phi->at(0) - M_PI ) : (t->MuMuGamma_CS_Phi->at(0) + M_PI));
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( abs(w_Phi) ) );
                } else if ( weight_var.find( "JPsi_Eta" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->DiLept_Eta->at(0) ) );
                } else if ( weight_var.find( "Phot_Eta" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->Photon_Eta->at(0) ) );
                } else if ( weight_var.find( "AvgIpX" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->avgIpX ) );
                } else if ( weight_var.find( "ActIpX" ) != std::string::npos ){
                  weight = weight_hists.at(bin)->GetBinContent( weight_hists.at( bin )->FindBin( t->actIpX ) );
                } else if ( weight_var.find( "PU" ) != std::string::npos ){
                  weight = t->pu_weight;
                }

                hist_reco_qta_vec.at(bin).Fill(reco_qtA,weight);
                hist_reco_qtb_vec.at(bin).Fill(abs(reco_qtB),weight);
                hist_reco_jpsi_pt_vec.at(bin).Fill(t->DiLept_Pt->at(0)/1000.0,weight);
                hist_reco_phot_pt_vec.at(bin).Fill(t->Photon_Pt->at(0)/1000.0,weight);
                hist_reco_dphi_vec.at(bin).Fill(abs(dPhi),weight);
                hist_reco_phi_vec.at(bin).Fill(abs(t->MuMuGamma_CS_Phi->at(0)),weight);

                if ( (t->MuMuGamma_CS_CosTheta->at(0) * t->MuMuGamma_CS_CosTheta->at(0)) < 0.1 ){
                  hist_reco_phi_low_vec.at(bin).Fill(abs(t->MuMuGamma_CS_Phi->at(0)), weight);
                } else if ( (t->MuMuGamma_CS_CosTheta->at(0) * t->MuMuGamma_CS_CosTheta->at(0)) > 0.1 ){
                  hist_reco_phi_high_vec.at(bin).Fill(abs(t->MuMuGamma_CS_Phi->at(0)), weight );
                }

              }
            }

            TFile * eff_file = new TFile(Form("./Efficiencies_%s.root",input_type.c_str()),"RECREATE");
            
            for (int mass_idx = 0; mass_idx < hist_reco_qta_vec.size(); mass_idx++){

              int mass_bin = mass_bins[mass_idx]; 
              std::string mass_char = "Q";
              if ( mass_idx >= 5 ){ mass_char = "T"; };
              TH1F reco_qta       = hist_reco_qta_vec[mass_idx];
              TH1F reco_qtb       = hist_reco_qtb_vec[mass_idx];
              TH1F reco_jpsi      = hist_reco_jpsi_pt_vec[mass_idx];
              TH1F reco_phot      = hist_reco_phot_pt_vec[mass_idx];
              TH1F reco_dphi      = hist_reco_dphi_vec[mass_idx];
              TH1F reco_phi       = hist_reco_phi_vec[mass_idx];
              TH1F reco_phi_low   = hist_reco_phi_low_vec[mass_idx];
              TH1F reco_phi_high  = hist_reco_phi_high_vec[mass_idx];
              TH1F truth_qta      = hist_truth_qta_vec[mass_idx];
              TH1F truth_qtb      = hist_truth_qtb_vec[mass_idx];
              TH1F truth_jpsi     = hist_truth_jpsi_pt_vec[mass_idx];
              TH1F truth_phot     = hist_truth_phot_pt_vec[mass_idx];
              TH1F truth_dphi     = hist_truth_dphi_vec[mass_idx];
              TH1F truth_phi     = hist_truth_phi_vec[mass_idx];
              TH1F truth_phi_low   = hist_truth_phi_low_vec[mass_idx];
              TH1F truth_phi_high  = hist_truth_phi_high_vec[mass_idx];
 
              TH1F * qta_div  = new TH1F(Form("eff_qtA_%s%i", mass_char.c_str(),mass_bin),"",             eff_bin.find("qtA")->second,    
                  qtARange.at(0).first, qtARange.at(0).second );
              TH1F * qtb_div  = new TH1F(Form("eff_qtB_%s%i", mass_char.c_str(),mass_bin),"",             eff_bin.find("qtB")->second,
                  qtBRange.at(0).first, qtBRange.at(0).second );
              TH1F * jpsi_div = new TH1F(Form("eff_DiMuonPt_%s%i", mass_char.c_str(),mass_bin),"",         eff_bin.find("Jpsi")->second,   
                  JpsiRange.at(0).first, JpsiRange.at(0).second );
              TH1F * phot_div = new TH1F(Form("eff_PhotonPt_%s%i", mass_char.c_str(),mass_bin),"",         eff_bin.find("Phot")->second,   
                  PhotRange.at(0).first, PhotRange.at(0).second );
              TH1F * dphi_div = new TH1F(Form("eff_DPhi_%s%i", mass_char.c_str(),mass_bin),"",         eff_bin.find("DPhi")->second,   
                  DPhiRange.at(0).first, DPhiRange.at(0).second );
              TH1F * phi_div = new TH1F(Form("eff_Phi_%s%i", mass_char.c_str(),mass_bin),"",           eff_bin.find("Phi")->second,    
                  PhiRange.at(0).first, PhiRange.at(0).second );
              TH1F * phi_low_div = new TH1F(Form("eff_Phi_low_%s%i", mass_char.c_str(),mass_bin),"",   eff_bin.find("Phi")->second,    
                  PhiRange.at(0).first, PhiRange.at(0).second );
              TH1F * phi_high_div = new TH1F(Form("eff_Phi_high_%s%i", mass_char.c_str(), mass_bin),"", eff_bin.find("Phi")->second,    
                  PhiRange.at(0).first, PhiRange.at(0).second );

              qta_div->Divide(&reco_qta,&truth_qta,1.0,1.0,"B"); 
              qtb_div->Divide(&reco_qtb,&truth_qtb,1.0,1.0,"B"); 
              jpsi_div->Divide(&reco_jpsi,&truth_jpsi,1.0,1.0,"B"); 
              phot_div->Divide(&reco_phot,&truth_phot,1.0,1.0,"B"); 
              dphi_div->Divide(&reco_dphi,&truth_dphi,1.0,1.0,"B"); 
              phi_div->Divide(&reco_phi,&truth_phi,1.0,1.0,"B"); 
              phi_low_div->Divide(&reco_phi_low,&truth_phi_low,1.0,1.0,"B"); 
              phi_high_div->Divide(&reco_phi_high,&truth_phi_high,1.0,1.0,"B"); 

              eff_file->cd();
              truth_qta.Write();
              truth_qtb.Write();
              truth_jpsi.Write();
              truth_phot.Write();
              truth_dphi.Write();
              truth_phi.Write();
              truth_phi_low.Write();
              truth_phi_high.Write();
              reco_qta.Write();
              reco_qtb.Write();
              reco_jpsi.Write();
              reco_phot.Write();
              reco_dphi.Write();
              reco_phi.Write();
              reco_phi_low.Write();
              reco_phi_high.Write();
              qta_div->Write(); 
              qtb_div->Write();
              jpsi_div->Write(); 
              phot_div->Write();
              dphi_div->Write();
              phi_div->Write();
              phi_low_div->Write();
              phi_high_div->Write();

            }
            delete eff_file;
            std::cout << "eff generated" << std::endl;
          }

          TH1F * dr_temp = new TH1F( "dr_temp", "dr_temp", 140, 0, 7 );
	        //Loop over every event in the file:
          if ( !reco ) return;
	        nbytes = 0, nb = 0;
	        for (Long64_t jentry=0; jentry<nentries;jentry++){

	          Long64_t ientry = t->LoadTree(jentry);
	          if (ientry < 0) break;
	          nb = t->fChain->GetEntry(jentry);   nbytes += nb;

	          evt= t->evt;
	          Truth_MuMuGamma_CS_CosTheta = t->Truth_MuMuGamma_CS_CosTheta;	  
	          Photon_quality=t->Photon_quality;
	          MuMuGamma_CS_CosTheta = t -> MuMuGamma_CS_CosTheta;
	          mcPhoton_Pt = t->mcPhoton_Pt;
	          Photon_Pt= t->Photon_Pt;
	          Photon_Phi = t->Photon_Phi;
	          mcMuMinus_Pt = t->mcMuMinus_Pt;
	          MuMinus_Pt = t->MuMinus_Pt;
	          mcMuPlus_Pt = t->mcMuPlus_Pt;
	          MuPlus_Pt = t->MuPlus_Pt;
	          MuMuY_M = t->MuMuY_M;
	          mcMuMuY_M = t->mcMuMuY_M;
	          MuMuY_Pt = t->MuMuY_Pt;
	          mcMuMuY_Pt = t->mcMuMuY_Pt;
	          MuMuGamma_CS_Phi = t->MuMuGamma_CS_Phi;
	          Truth_MuMuGamma_CS_Phi = t->Truth_MuMuGamma_CS_Phi;
	          DiLept_M = t->DiLept_M;
	          DiLept_Y = t->DiLept_Y;
	          DiLept_Phi = t->DiLept_Phi;
	          DiLept_Pt = t->DiLept_Pt;
	          DiMuonVertex_Tau = t->DiMuonVertex_Tau;
	          Photon_TrkIso = t->Photon_TrkIso;
	          DiLept_TrkIso_ptcone20 = t->DiLept_TrkIso_ptcone20;
	          DiLept_TrkIso_ptcone30 = t->DiLept_TrkIso_ptcone30;
	          DiLept_TrkIso_ptcone40 = t->DiLept_TrkIso_ptcone40;
	          DiLept_TrkIso_ptVarCone20 = t->DiLept_TrkIso_ptVarCone20;
	          DiLept_TrkIso_ptVarCone30 = t->DiLept_TrkIso_ptVarCone30;
	          DiLept_TrkIso_ptVarCone40 = t->DiLept_TrkIso_ptVarCone40;
	          HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;
	          Photon_quality = t->Photon_quality;
            
           
            double calc_dimuon_pt,calc_photon_pt,calc_dphi,calc_phiCS;
            if (t->DiLept_Pt->size() == 0){ calc_dimuon_pt = 0.0; }
            else{ calc_dimuon_pt = t->DiLept_Pt->at(0)/1000.0;}
            if (t->MuMuGamma_CS_Phi->size() == 0){ calc_phiCS = 0.0;}
            else { calc_phiCS = t->MuMuGamma_CS_Phi->at(0); }
            if (t->Photon_Pt->size() == 0){
              calc_photon_pt = 0.0;
              calc_dphi = 0.0; 
            } else {
              calc_photon_pt = t->Photon_Pt->at(0)/1000.0;    
              calc_dphi = (t->DiLept_Phi->at(0) - t->Photon_Phi->at(0));
            }

            if ( cutflow_var.find("qtA") != std::string::npos ){
              cutflow_main.set_var(calc_dimuon_pt - calc_photon_pt);
            } else if ( cutflow_var.find("qtB") != std::string::npos ){
              cutflow_main.set_var(sqrt(calc_dimuon_pt * calc_photon_pt) * sin(calc_dphi));
            } else if ( cutflow_var.find("Phi") != std::string::npos ){
              cutflow_main.set_var(calc_phiCS);
            }

            // 445 Cuts
            cutflow_main.fill(1);
	          if ( t->Photon_Pt->size() == 0 ) continue; 

            rw_DiMuonPt = t->DiLept_Pt->at(0); 
            rw_PhotonPt = t->Photon_Pt->at(0);
            rw_Lambda = (t->MuMuY_M->at(0)/3097.0) * (t->MuMuY_M->at(0)/3097.0);
	        	rw_Phi = (t->MuMuGamma_CS_Phi->at(0) >= 0 ? (t->MuMuGamma_CS_Phi->at(0) - M_PI ) : (t->MuMuGamma_CS_Phi->at(0) + M_PI));
            rw_JPsi_Eta = t->DiLept_Eta->at(0); 
            rw_Phot_Eta = t->Photon_Eta->at(0);
            rw_ActIpX = t->actIpX;
            rw_AvgIpX = t->avgIpX;
	        	rw_costheta = t->MuMuGamma_CS_CosTheta->at(0);
            rw_tr->Fill();

            cutflow_main.fill(2);
            if ( t->Photon_Pt->at(0) < photon_lower || t->Photon_Pt->at(0) > photon_upper ) continue;
            cutflow_main.fill(3);
	          if ( t->MuMinus_Pt->at(0) < muon_lower || t->MuMinus_Pt->at(0) > muon_upper ) continue;
            cutflow_main.fill(4);
	          if ( t->MuPlus_Pt->at(0) < muon_lower || t->MuPlus_Pt->at(0) > muon_upper ) continue;
            cutflow_main.fill(5);

            double dimuonpt = t->DiLept_Pt->at(0); 
            double photonpt = t->Photon_Pt->at(0);    
	          double dPhi = (t->DiLept_Phi->at(0) - t->Photon_Phi->at(0));

	          double lambdaPreSelection       = (t->MuMuY_M->at(0)/3097.0) * (t->MuMuY_M->at(0)/3097.0);
	          double qTsquaredPreSelection    = (t->MuMuY_Pt->at(0)/1000.0)  * (t->MuMuY_Pt->at(0)/1000.0 );
	          double qtAPreSelection          =  (dimuonpt/1000) - (photonpt/1000);
	          double qtBPreSelection          =  sqrt((dimuonpt/1000) * (photonpt/1000)) * sin(dPhi);
	          double dimuonmassPreSelection   = t->DiLept_M->at(0);
	          double dimuontauPreSelection    = t->DiMuonVertex_Tau->at(0);
	          double dimuonptPreSelection     = t->DiLept_Pt->at(0);
	          double photonptPreSelection     = t->Photon_Pt->at(0);
            double cosThetaCS2PreSelection  = (t->MuMuGamma_CS_CosTheta->at(0) * t->MuMuGamma_CS_CosTheta->at(0));
            double phiCSPreSelection        = sqrt(t->MuMuGamma_CS_Phi->at(0) * t->MuMuGamma_CS_Phi->at(0) );

	          while( dPhi > M_PI ){ dPhi -= 2*M_PI; }
	          while( dPhi < -M_PI ){ dPhi += 2*M_PI; }
	          double dY =( t->DiLept_Y->at(0) - t->Photon_Eta->at(0)) ; 
	          double absdY = sqrt(dY * dY);
	          //Declare the variables
	          double Lr =  (t->MuMuY_M->at(0)/3097.0) * (t->MuMuY_M->at(0)/3097.0);
	          double Qr = (t->MuMuY_Pt->at(0)/1000.0)  * (t->MuMuY_Pt->at(0)/1000.0 );
          
	          if (t->HLT_2mu4_bJpsimumu_noL2 == 0) continue;
            cutflow_main.fill(6);
            if (lambdaPreSelection < LambdaRange.at(0).first) continue;
            cutflow_main.fill(7);
            if ( abs(qtBPreSelection) < qtBRange.at(0).first || abs(qtBPreSelection) > qtBRange.at(0).second ) continue;
            cutflow_main.fill(8);
	          if (t->Photon_quality->at(0) > 1) continue;
            cutflow_main.fill(9);
            
              
            if ( abs(t->DiLept_Pt->at(0)) < DiMuonPtRange.at(0).first || abs(t->DiLept_Pt->at(0)) > DiMuonPtRange.at(0).second ){ continue; }
            
            phot_dR = 0;
            if (file_mode == 2){
              TLorentzVector tlv_truth_photon,tlv_reco_photon,tlv_truth_muplus,tlv_reco_muplus;
              tlv_reco_photon.SetPtEtaPhiE(t->Photon_Pt ->at(0),t->Photon_Eta->at(0),t->Photon_Phi->at(0),t->Photon_E->at(0));
              tlv_truth_photon.SetPtEtaPhiM(t->mcPhoton_Pt ->at(0),t->mcPhoton_Eta->at(0),t->mcPhoton_Phi->at(0),0.);
              Float_t PhotonDRTruthReco  = tlv_reco_photon.DeltaR(tlv_truth_photon) ;
              dr_temp->Fill( PhotonDRTruthReco );
              phot_dR = PhotonDRTruthReco;
              if (PhotonDRTruthReco > 0.12) continue;
            }
            cutflow_main.fill(10);

            if ( t->actIpX < ActIpXRange.at(0).first || t->actIpX > ActIpXRange.at(0).second ) continue;

            if ( abs(t->DiMuon_DeltaZ0->at(0)) < DiMuDZRange.at(0).first || abs(t->DiMuon_DeltaZ0->at(0)) > DiMuDZRange.at(0).second ){ continue; }



	          //Loop over the various indices and ranges and fill the created histograms based on that.
	          for (unsigned int iC=0; iC<CosThetaRange.size(); iC++){
	            if( cosThetaCS2PreSelection <= CosThetaRange.at(iC).first || cosThetaCS2PreSelection > CosThetaRange.at(iC).second) continue;
              cutflow_main.fill(11);
	            for (unsigned int iL=0; iL<LambdaRange.size(); iL++){  //CS loop
                if( lambdaPreSelection <= LambdaRange.at(iL).first || lambdaPreSelection > LambdaRange.at(iL).second) continue; 
                cutflow_main.fill(12);
                for (unsigned int iQ=0; iQ<qTSquaredRange.size(); iQ++){ 
                  if(( Qr <= qTSquaredRange.at(iQ).first || Qr > qTSquaredRange.at(iQ).second) ) continue; // THIS HAD BEEN MISSED
                  cutflow_main.fill(13);
                  if( (qtAPreSelection <= qtARange.at(iQ).first || qtAPreSelection > qtARange.at(iQ).second) ) continue;
                  cutflow_main.fill(14);
                  for (unsigned int iP = 0; iP < PhiRange.size(); iP++){
                    if (phiCSPreSelection <= PhiRange.at(iP).first || phiCSPreSelection > PhiRange.at(iP).second) continue;
                    cutflow_main.fill(15);
	        	        for (unsigned int iM=0; iM<MassRange.size(); iM++){
	        	          if( dimuonmassPreSelection <= MassRange.at(iM).first || dimuonmassPreSelection > MassRange.at(iM).second) continue;
                      cutflow_main.fill(16);
	        	          for (unsigned int iT=0; iT<LifetimeRange.size(); iT++){
	       	              if( dimuontauPreSelection <= LifetimeRange.at(iT).first || dimuontauPreSelection > LifetimeRange.at(iT).second) continue;
                        cutflow_main.fill(17);

                        EventNumber = t->evt;
	        	            Trigger_HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;

	        	            Lambda = lambdaPreSelection;
	        	            DiMuonMass = dimuonmassPreSelection;
	        	            DiMuonTau = dimuontauPreSelection;

	        	            qTSquared = qTsquaredPreSelection;
	        	            DiMuonPt = dimuonpt;

                        AbsPhi = phiCSPreSelection;
	        	            Phi = (t->MuMuGamma_CS_Phi->at(0) >= 0 ? (t->MuMuGamma_CS_Phi->at(0) - M_PI ) : (t->MuMuGamma_CS_Phi->at(0) + M_PI));
	        	
	        	            costheta = t->MuMuGamma_CS_CosTheta->at(0);
                        AbsCosTheta = (sqrt((t->MuMuGamma_CS_CosTheta->at(0) * t->MuMuGamma_CS_CosTheta->at(0))));

	        	            DY = dY;
	        	            AbsdY = absdY;
	        	            DPhi = dPhi;
	        	            AbsdPhi = (sqrt(dPhi * dPhi));
	        	            PhotonPt = photonpt;

	        	            qxpsi = dimuonpt/1000 + (photonpt/1000)*cos(dPhi);
	        	            qypsi = (photonpt/1000)*sin(dPhi);
	        		      
	        	            qxgamma = photonpt/1000 + (dimuonpt/1000)*cos(dPhi);
	        	            qygamma = (dimuonpt/1000) *sin(dPhi);

	        	            qxsum = (dimuonpt/1000 + photonpt/1000)*(1+cos(dPhi));
	        	            qysum = (dimuonpt/1000 + photonpt/1000)*(sin(dPhi));

	        	            qtA = (dimuonpt/1000) - (photonpt/1000);
	        	            qtB = sqrt((dimuonpt/1000) * (photonpt/1000)) * sin(dPhi);

	        	            qtL = ((dimuonpt/1000) - (photonpt/1000))*cos((M_PI-abs(dPhi))/2.0);
	        	            qtM = ((dimuonpt/1000) - (photonpt/1000))*sin((M_PI-abs(dPhi))/2.0);

                        JPsi_Eta = t->DiLept_Eta->at(0); 
                        Phot_Eta = t->Photon_Eta->at(0);
                        ActIpX = t->actIpX;
                        AvgIpX = t->avgIpX;
                        DeltaZ0 = t->DiMuon_DeltaZ0->at(0);
              

	        	            if ( iL == l && iQ == q ){

                          if (file_mode == 1){
	  	                      obj_EventNumber                             = EventNumber;
	  	                      obj_DiMuonPt                                = DiMuonPt;
	  	                      obj_DiMuonMass                              = DiMuonMass;
	  	                      obj_DiMuonTau                               = DiMuonTau;
	  	                      obj_HLT_2mu4_bJpsimumu_noL2                 = t->HLT_2mu4_bJpsimumu_noL2;
	  	                      obj_Lambda                                  = Lambda;
	  	                      obj_qTsquaredPreSelection                   = qTsquaredPreSelection;
	  	                      obj_MuMuGamma_CS_Phi                        = t->MuMuGamma_CS_Phi->at(0);
                            obj_MuMuGamma_CS_CosTheta                   = t->MuMuGamma_CS_CosTheta->at(0);
	  	                      obj_DiLept_Y                                = t->DiLept_Y->at(0);
	  	                      obj_DiLept_Phi                              = t->DiLept_Phi->at(0);
	  	                      obj_DiMuonVertex_Muon0_Px                   = t->DiMuonVertex_Muon0_Px->at(0);
	  		                    obj_DiMuonVertex_Muon0_Py                   = t->DiMuonVertex_Muon0_Py->at(0);
	  		                    obj_DiMuonVertex_Muon0_Pz                   = t->DiMuonVertex_Muon0_Pz->at(0);
	  		                    obj_MuPlus_M                                = t->MuPlus_M->at(0);
	  	                      obj_DiMuonVertex_Muon1_Px                   = t->DiMuonVertex_Muon1_Px->at(0);
	  		                    obj_DiMuonVertex_Muon1_Py                   = t->DiMuonVertex_Muon1_Py->at(0);
	  		                    obj_DiMuonVertex_Muon1_Pz                   = t->DiMuonVertex_Muon1_Pz->at(0);
	  		                    obj_MuMinus_M                               = t->MuMinus_M->at(0);
	  	                      obj_DiLept_Pt                               = t->DiLept_Pt->at(0);
	  		                    obj_DiLept_Eta                              = t->DiLept_Eta->at(0);
	  		                    obj_DiLept_Phi                              = t->DiLept_Phi->at(0);
	  		                    obj_DiLept_M                                = t->DiLept_M->at(0);
                            obj_Photon_Pt                               = t->Photon_Pt->at(0);
	  		                    obj_Photon_Eta                              = t->Photon_Eta->at(0);
	  		                    obj_Photon_Phi                              = t->Photon_Phi->at(0);
	  		                    obj_Photon_E                                = t->Photon_E->at(0);
	  	                      obj_Photon_Pt                               = t->Photon_Pt->at(0);
	  	                      obj_Photon_Eta                              = t->Photon_Eta->at(0);
	  	                      obj_Photon_Phi                              = t->Photon_Phi->at(0);
                            objtree->Fill();
                          }
                          tr->Fill();
                        }
	        	          } //Closing iT loop
	        	        }   //Closing iM loop
	        	      }     //Closing iP loop 
	              }       //Closing iQ loop	 
              }         //Closing iL loop
	          }           //Closing iC loop
	        } //closing loop over entries

	        MC_Plots->cd();
	        tr->Write();
          std::cout << "written" << std::endl;
	        MC_Plots->ls();
	        MC_Plots->Close();

          raw_weights->cd();
          rw_tr->Write();
          raw_weights->Close();

          if (file_mode == 1){
            objstore->cd();
            objtree->Write();
            std::cout << "written store" << std::endl;
          }

          if ( file_mode == 2 ){
            TCanvas * dr_canv = new TCanvas( "dR", "dR", 100, 100, 1000, 1000 );
            dr_canv->Divide( 1 ); dr_canv->cd(1);
            dr_canv->SetLogy();
            dr_temp->Draw( "HIST" ); dr_temp->SetMarkerStyle(21); dr_temp->SetLineColor(1);
            dr_temp->GetYaxis()->SetRangeUser( 1, dr_temp->GetMaximum()*1.5 );
            dr_temp->GetYaxis()->SetTitle( Form( "counts" ));
            dr_temp->GetXaxis()->SetTitle( Form( "dR" ));
            dr_temp->GetYaxis()->SetLabelSize(0.035); dr_temp->GetYaxis()->SetTitleSize(0.035);
            dr_temp->GetXaxis()->SetLabelSize(0.035); dr_temp->GetXaxis()->SetTitleSize(0.035);
            dr_canv->Update();
            TLine * line12 = new TLine( 0.12, 0, 0.12, dr_temp->GetMaximum()*1.5 );
            line12->SetLineColor( kRed + 1 );
            line12->Draw( "SAME" );
            dr_canv->SaveAs( "./cutflow/dr.png" );
          }

        } // momentum index loop close
      }   // mass index loop close
    }     // photon momentum loop close
    cutflow_main.write(); 
  }
    
  return;

} //Closing brace


int help(){
  std::cout << "-t input type             --    type of input, data, signal, etc" << std::endl;
  std::cout << "-i input                  --    path and tree name of input file    --   : separated list" << std::endl;
  std::cout << "-c apply cuts             --    cut(s) on the dataset, formatted    --   : separated list" << std::endl;
  std::cout << "-u input name             --    unique name for this type" << std::endl;
  std::cout << "-f cutflow variable       --    cutflow variable for this run" << std::endl;
  std::cout << "-r ranges                 --    allows setting of a multitude of variable limits in the tree creation" << std::endl;
  std::cout << "                          --    Use a colon separated list of variables, to change from defaults" << std::endl;
  std::cout << "                          --    Underscore to separate variable name and limits, use comma for limits" << std::endl;
  std::cout << "                          --    Supplying only lower limit will set upper limit to DBL_MAX" << std::endl;
  std::cout << "                          --    Supplying variable name and no limits will set range to /pm DBL_MAX" << std::endl;
  std::cout << "-h help                   --    this readout" << std::endl;
  return 0;
}

 
int main(int argc, char ** argv){
  //if (argc == 4){TMVAGenerateInput(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));}
  int option_index{0},option{0};
  static struct option long_options[] = {
      { "help",           no_argument,          0,      'h'},
      { "input",          required_argument,    0,      'i'},
      { "type",           required_argument,    0,      't'},
      { "cuts",           required_argument,    0,      'c'},
      { "unique",         required_argument,    0,      'u'},
      { "ranges",         required_argument,    0,      'r'},
      { "cutflow",        required_argument,    0,      'f'},
      { "efficiency",     no_argument,          0,      'e'},
      { "migration",      no_argument,          0,      'm'},
      { "truth",          no_argument,          0,      'v'},
      { "bins",           required_argument,    0,      'b'},
      { "weigh",          required_argument,    0,      'w'},
      { "reco",           no_argument,          0,      'o'},
      { 0,                0,                    0,      0}
  };

  std::string input_file, input_type;
  std::string cuts{""}, unique{""};
  std::string ranges{""};
  std::string cutflow_var{"qtA"};
  std::string eff_bins;
  std::string weight_var{""};
  bool eff = false, mig = false, truth = false, reco = false;
  
  do {
    option = getopt_long(argc, argv, "i:t:c:u:r:f:b:w:mevoh", long_options, &option_index);
    switch (option){
      case 'i':
        input_file = std::string(optarg);
        break;
      case 't':
        input_type = std::string(optarg);
        break;
      case 'c':
        cuts = std::string(optarg);
        break;
      case 'u':
        unique = std::string(optarg);
        break;
      case 'e':
        eff = true;
        break;
      case 'r':
        ranges = std::string(optarg);
        break;
      case 'b':
        eff_bins    = std::string( optarg );
        break;
      case 'w':
        weight_var    = std::string( optarg );
        break;
      case 'm':
        mig = true;
        break;
      case 'v':
        truth = true;
        break;
      case 'o':
        reco = true;
        break;
      case 'h':
        return help();
    }
  } while ( option != -1);

  TMVAGenerateInput( input_file, input_type, cuts, unique, ranges, cutflow_var, eff_bins,  weight_var, eff, mig, truth,
                    reco );

}
