#include "ind_pp_mixer.hxx"
#include <numeric>

void cutflow(int cut, double var,TH1F* &cutflow_integral, std::map<int,TH1F*> &cutflow_variable_map, std::map<int,TH1F*> &cutflow_abs_variable_map){
  cutflow_integral->Fill(cut);
  cutflow_variable_map[cut]->Fill(var);
  cutflow_abs_variable_map[cut]->Fill(abs(var));
} 


void TMVAGenerateInput(int file_mode=0,int bound_mode=0,int cutflow_mode=0, int pp_mix=0, int ppmix_batch=0, int train_int=0){
  // enumerating the modes
  // FILE
  // 0 data
  // 1 pp
  // 2 signal
  // 3 bb
  // BOUNDING
  // 0 bounded qta and qtb
  // 1 unbounded qta and qtb
  // 2 unbounded qta qtb and qt2
  // CUTFLOW
  // 0 cutflow qta var
  // 1 cutflow qtb var
  // 2 cutflow phi var
  // THE INPUTS FOR THE BDT MAY REMAIN THE SAME UNTIL THE 
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);

  TRandom3 * rand_root;
  bool mixing;
  if (file_mode == 1 && pp_mix == 1){
    std::cout << "mixing mode" << std::endl;
    mixing = true;
    rand_root = new TRandom3(5432);
  }
  std::string mix_file_str;

  std::vector<std::string> photonCut = {"5000.0"};
  std::vector<std::string> muonCut = {"4000.0"};
  std::vector<std::string> bound_modes_vec{"bound-0","bound-1","bound-2"};
  std::string cut_var_str;
  if (cutflow_mode == 0) cut_var_str = Form("qta_%s",bound_modes_vec[bound_mode].c_str());
  if (cutflow_mode == 1) cut_var_str = Form("qtb_%s",bound_modes_vec[bound_mode].c_str());
  if (cutflow_mode == 2) cut_var_str = Form("phi_%s",bound_modes_vec[bound_mode].c_str());
  int cutflow_var_bins = 50;
  double cutflow_min_bound = (cutflow_mode <=1) ? -20 : 0;
  double cutflow_max_bound = (cutflow_mode <=1) ?  20 : M_PI;
  TH1F * cutflow_integral = new TH1F("cutflow_integral","cutflow_integral",20,0,20);
  std::map<int,TH1F*> cutflow_variable_map;
  std::map<int,TH1F*> cutflow_abs_variable_map;
  int total_cuts = 17;
  for (int cut=1; cut <=total_cuts;cut++){
    cutflow_variable_map[cut] = new TH1F(Form("cutflow_variable_%i",cut),Form("cutflow_variable_%i",cut),cutflow_var_bins,cutflow_min_bound,cutflow_max_bound);
    cutflow_abs_variable_map[cut] = new TH1F(Form("cutflow_abs_variable_%i",cut),Form("cutflow_abs_variable_%i",cut),cutflow_var_bins/2,0,cutflow_max_bound);
  }

  std::vector<int> MassIndex = {0};
  std::vector<int> LambdaIndex = { 0};
  std::vector<int> qTSquaredIndex = {0};
  std::vector<int> qTAIndex = {0};
  std::vector<int> PhiIndex = {0};
  std::vector<int> LifetimeIndex = {0};
  std::vector<int> CosThetaIndex = {0};
  
  typedef std::vector<std::pair<double,double> > Ranges;
  Ranges MassRange =      {{2700.0, 3500.0}}; //J/psi mass intervals
  Ranges LambdaRange =    {{0.0, 200.0}};     //Lambda intervals
  Ranges qTSquaredRange = {{0.0, 400.0}};     //qT Squared intervals
  Ranges qTARange =       {{-5.0, 15.0}};     //qT A intervals  
  Ranges qtBRange =       {{-20.0, 20.0}};    //qT B intervals
  Ranges LifetimeRange =  {{-5.0, 15.0}};     //lifetime ranges
  Ranges CosThetaRange =  {{0.0, 1.0}};       //cosThetaRange
  Ranges PhiRange =       {{0.0, 3.142}};     //phi ranges
  
  bool j_mode = false;
  bool test_ten = false;
  bool thousand = false;
  bool focussed = true;
  bool train = (train_int == 1);
  
  if (mixing){

    mix_file_str = std::string("00_mu4000_P5000_bound-0.root");
    TFile * pp_original_file = new TFile("./pp_bound-0/split0_objstore/pp_objstore_00_mu4000_P5000_bound-0.root", "READ");
    TTree * pp_original_tree = (TTree*) pp_original_file->Get("objtree");
    Float_t pp_EventNumber,pp_DiMuonPt, pp_DiMuonMass, pp_DiMuonTau, pp_HLT_2mu4_bJpsimumu_noL2, pp_Lambda, pp_qTsquaredPreSelection, pp_MuMuGamma_CS_Phi, pp_MuMuGamma_CS_CosTheta, pp_DiLept_Y,
            pp_DiLept_Ph, pp_DiMuonVertex_Muon0_Px, pp_DiMuonVertex_Muon0_Py, pp_DiMuonVertex_Muon0_Pz, pp_MuPlus_M, pp_DiMuonVertex_Muon1_Px, pp_DiMuonVertex_Muon1_Py, pp_DiMuonVertex_Muon1_Pz,
            pp_MuMinus_M, pp_DiLept_Pt, pp_DiLept_Eta, pp_DiLept_Phi, pp_DiLept_M, pp_Photon_Phi, pp_Photon_E, pp_Photon_Pt ,pp_Photon_Eta;
    
    pp_original_tree->SetBranchAddress("obj_EventNumber",                            &pp_EventNumber);
	  pp_original_tree->SetBranchAddress("obj_DiMuonPt",                               &pp_DiMuonPt);
	  pp_original_tree->SetBranchAddress("obj_DiMuonMass",                             &pp_DiMuonMass);
	  pp_original_tree->SetBranchAddress("obj_DiMuonTau",                              &pp_DiMuonTau);
	  pp_original_tree->SetBranchAddress("obj_HLT_2mu4_bJpsimumu_noL2",                &pp_HLT_2mu4_bJpsimumu_noL2);
	  pp_original_tree->SetBranchAddress("obj_Lambda",                                 &pp_Lambda);
	  pp_original_tree->SetBranchAddress("obj_qTsquaredPreSelection",                  &pp_qTsquaredPreSelection);
	  pp_original_tree->SetBranchAddress("obj_MuMuGamma_CS_Phi",                       &pp_MuMuGamma_CS_Phi);
	  pp_original_tree->SetBranchAddress("obj_MuMuGamma_CS_CosTheta",                  &pp_MuMuGamma_CS_CosTheta);
	  pp_original_tree->SetBranchAddress("obj_DiLept_Y",                               &pp_DiLept_Y);
	  pp_original_tree->SetBranchAddress("obj_DiLept_Phi",                             &pp_DiLept_Phi);
	  pp_original_tree->SetBranchAddress("obj_DiMuonVertex_Muon0_Px",                  &pp_DiMuonVertex_Muon0_Px);
	  pp_original_tree->SetBranchAddress("obj_DiMuonVertex_Muon0_Py",                  &pp_DiMuonVertex_Muon0_Py);
	  pp_original_tree->SetBranchAddress("obj_DiMuonVertex_Muon0_Pz",                  &pp_DiMuonVertex_Muon0_Pz);
	  pp_original_tree->SetBranchAddress("obj_MuPlus_M",                               &pp_MuPlus_M);
	  pp_original_tree->SetBranchAddress("obj_DiMuonVertex_Muon1_Px",                  &pp_DiMuonVertex_Muon1_Px);
    pp_original_tree->SetBranchAddress("obj_DiMuonVertex_Muon1_Py",                  &pp_DiMuonVertex_Muon1_Py);
    pp_original_tree->SetBranchAddress("obj_DiMuonVertex_Muon1_Pz",                  &pp_DiMuonVertex_Muon1_Pz);
    pp_original_tree->SetBranchAddress("obj_MuMinus_M",                              &pp_MuMinus_M);
    pp_original_tree->SetBranchAddress("obj_DiLept_Pt",                              &pp_DiLept_Pt);
    pp_original_tree->SetBranchAddress("obj_DiLept_Eta",                             &pp_DiLept_Eta);
    pp_original_tree->SetBranchAddress("obj_DiLept_Phi",                             &pp_DiLept_Phi);
    pp_original_tree->SetBranchAddress("obj_DiLept_M",                               &pp_DiLept_M);
    pp_original_tree->SetBranchAddress("obj_Photon_E",                               &pp_Photon_E);               
    pp_original_tree->SetBranchAddress("obj_Photon_Pt",                              &pp_Photon_Pt);
    pp_original_tree->SetBranchAddress("obj_Photon_Eta",                             &pp_Photon_Eta);            
    pp_original_tree->SetBranchAddress("obj_Photon_Phi",                             &pp_Photon_Phi);           

    std::vector<Jpsi> allJpsi;
    std::vector<Photon> allPhoton;

    for (int entry = 0; entry < pp_original_tree->GetEntries(); entry++){
      pp_original_tree->GetEntry(entry);
      Jpsi jpsi;
		  Photon photon;
		  jpsi.EventNumber                         = pp_EventNumber;
		  jpsi.DiMuonPt                            = pp_DiMuonPt;
		  jpsi.DiMuonMass                          = pp_DiMuonMass;
		  jpsi.DiMuonTau                           = pp_DiMuonTau;
		  jpsi.Trigger_HLT_2mu4_bJpsimumu_noL2     = pp_HLT_2mu4_bJpsimumu_noL2;
		  jpsi.old_lambda                          = pp_Lambda;
		  jpsi.old_qt2                             = pp_qTsquaredPreSelection;
		  jpsi.old_csPhi                           = pp_MuMuGamma_CS_Phi;
		  jpsi.old_csCosTheta                      = pp_MuMuGamma_CS_CosTheta;
		  jpsi.DiLept_Y                            = pp_DiLept_Y;
		  jpsi.DiLept_Phi                          = pp_DiLept_Phi;
		  jpsi.mupl.SetXYZM(                       pp_DiMuonVertex_Muon0_Px, pp_DiMuonVertex_Muon0_Py, pp_DiMuonVertex_Muon0_Pz,pp_MuPlus_M);
		  jpsi.mumi.SetXYZM(                       pp_DiMuonVertex_Muon1_Px, pp_DiMuonVertex_Muon1_Py,pp_DiMuonVertex_Muon1_Pz,pp_MuMinus_M);
		  jpsi.p4.SetPtEtaPhiM(                    pp_DiLept_Pt, pp_DiLept_Eta, pp_DiLept_Phi, pp_DiLept_M);
		  photon.p4.SetPtEtaPhiE(                  pp_Photon_Pt, pp_Photon_Eta, pp_Photon_Phi, pp_Photon_E);
		  photon.PhotonPt                        = pp_Photon_Pt;
		  photon.Photon_Eta                      = pp_Photon_Eta;
		  photon.Photon_Phi                      = pp_Photon_Phi;
      allPhoton.push_back(photon); 
      allJpsi.push_back(jpsi);
    }

    std::cout << allPhoton.size() << std::endl;
    std::cout << allJpsi.size() << std::endl;

    std::vector<int>    n_vec;//           = {0,1,2,5,10,25,50,100,125,150,175,200}; 
    std::vector<double> keep_vec;//        = {0.01,0.05,0.1,0.12,0.2,0.33,0.5,0.75,1.0}; 
    std::vector<int>    keep_vec_perc;//   = {1,5,10,12,20,33,50,75,100}; 
  
    int k_min_idx, k_max_idx, n_min_idx, n_max_idx;

    if (!focussed){
      n_vec           = {0,1,2,5,10,25,50,100,125,150,175,200}; 
      keep_vec        = {0.01,0.05,0.1,0.12,0.2,0.33,0.5,0.75,1.0}; 
      keep_vec_perc   = {1,5,10,12,20,33,50,75,100}; 
    } else if (focussed){
      n_vec = {100,200}; 
      keep_vec = {0.01,1.0}; 
      keep_vec_perc = {1,100}; 
    }

    if (j_mode){
      k_min_idx = 1; k_max_idx = 2; n_min_idx = 7; n_max_idx = 8;
    } else if (test_ten){
      k_min_idx = 4; k_max_idx = 5; n_min_idx = 1; n_max_idx = 2;
    } else if (thousand) {
      k_min_idx = 0; k_max_idx = 9; n_min_idx = 0; n_max_idx = 12;
    } else if (focussed){
      k_min_idx = 0; k_max_idx = 2; n_min_idx = 0; n_max_idx = 2;
    } else {
      k_min_idx = 0; k_max_idx = 8; n_min_idx = 0; n_max_idx = 12;
    }

    for (int n_idx = n_min_idx; n_idx < n_max_idx; n_idx++){

      std::vector<TH1F*> ppmix_cutflow_vector;
      int n_count = n_vec[n_idx];

      for (int k_idx = k_min_idx; k_idx < k_max_idx; k_idx++){
        int k_int = keep_vec_perc[k_idx];
        ppmix_cutflow_vector.push_back(new TH1F(Form("ppmix_cutflow_n%i_k%i",n_count,k_int),Form("ppmix_cutflow_n%i_k%i",n_count,k_int),16,1,17));
      }

      for (int k_idx = k_min_idx; k_idx < k_max_idx; k_idx++){

        bool use_n;
        double k = keep_vec[k_idx];
        int k_int = keep_vec_perc[k_idx];
        
        TVector3 CSAxis, xAxis,yAxis;
        double cosTheta_dimu, cosTheta_gamma, phi_dimu, phi_gamma;
        TLorentzVector mumuy;

        Float_t AbsCosTheta, AbsdPhi, AbsdY, DPhi, DY, Phi, costheta, AbsPhi, EventNumber, Lambda, qTSquared, DiMuonMass, DiMuonTau,
                DiMuonPt, PhotonPt, Trigger_HLT_2mu4_bJpsimumu_noL2, qxpsi, qypsi, qxgamma, qygamma, qxsum, qysum, qtA, qtB, qtL,qtM;
        TH1F * ppmix_cutflow = ppmix_cutflow_vector.at(k_idx);

        TFile * mix_file;
        TTree * mix_tree;
        if (j_mode){ 
          std::cout << "jmode" << std::endl;
          mix_file = new TFile (Form("./pp-mix_bound-0/jmode/pp-mix_n%i_k%i_jmode_%s",n_count,k_int,mix_file_str.c_str()), "RECREATE");
	        mix_file->cd();
          mix_tree = new TTree("TreeD", "tree");
        } else if (test_ten){
          std::cout << "tt" << std::endl;
          mix_file = new TFile (Form("./pp-mix_bound-0/jmode/pp-mix_n%i_k%i_tt_%s",n_count,k_int,mix_file_str.c_str()), "RECREATE");
	        mix_file->cd();
          mix_tree = new TTree("TreeD", "tree");
        } else if (thousand){
          std::cout << "thousand" << std::endl;
          mix_file = new TFile (Form("./pp-mix_bound-0/n%i_k%i/pp-mix_n%i_k%i_%s",n_count,k_int,n_count,k_int,mix_file_str.c_str()), "RECREATE");
	        mix_file->cd();
          mix_tree = new TTree("TreeThousand","tree");
        } else if (focussed && !train){
          mix_file = new TFile (Form("./pp-mix_bound-0/rand/n%i_k%i/pp-mix_n%i_k%i_%s",n_count,k_int,n_count,k_int,mix_file_str.c_str()), "RECREATE");
	        mix_file->cd();
          mix_tree = new TTree("TreeD", "tree");
        } else if (focussed && train){
          mix_file = new TFile (Form("./pp-mix_bound-0/rand/n%i_k%i_train/pp-train-mix_n%i_k%i_%s",n_count,k_int,n_count,k_int,mix_file_str.c_str()), "RECREATE");
	        mix_file->cd();
          mix_tree = new TTree("TreeD", "tree");
        } else {
          mix_file = new TFile (Form("./pp-mix_bound-0/n%i_k%i/pp-mix_n%i_k%i_%s",n_count,k_int,n_count,k_int,mix_file_str.c_str()), "RECREATE");
	        mix_file->cd();
          mix_tree = new TTree("TreeD", "tree");
        }

	      mix_tree->Branch("AbsCosTheta", &AbsCosTheta, "AbsCosTheta/F");
	      mix_tree->Branch("AbsdPhi", &AbsdPhi, "AbsdPhi/F");
	      mix_tree->Branch("AbsPhi", &AbsPhi, "AbsPhi/F");
	      mix_tree->Branch("DPhi", &DPhi, "DPhi/F");
	      mix_tree->Branch("DY", &DY, "DY/F");
	      mix_tree->Branch("Phi", &Phi, "phi/F");
	      mix_tree->Branch("costheta", &costheta, "CosTheta/F");
	      mix_tree->Branch("EventNumber", &EventNumber, "EventNumber/F");
	      mix_tree->Branch("Lambda", &Lambda, "Lambda/F");
	      mix_tree->Branch("qTSquared", &qTSquared, "qTSquared/F");
	      mix_tree->Branch("DiMuonMass", &DiMuonMass, "DiMuonMass/F");
	      mix_tree->Branch("DiMuonTau", &DiMuonTau, "DiMuonTau/F");
	      mix_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	      mix_tree->Branch("DiMuonPt", &DiMuonPt, "DiMuonPt/F");
	      mix_tree->Branch("PhotonPt", &PhotonPt, "PhotonPt/F");
	      mix_tree->Branch("AbsdY", &AbsdY, "AbsdY");
	      mix_tree->Branch("qxpsi", &qxpsi, "qxpsi");
	      mix_tree->Branch("qypsi", &qypsi, "qypsi");
	      mix_tree->Branch("qxgamma", &qxgamma, "qxgamma");
	      mix_tree->Branch("qygamma", &qygamma, "qygamma");
	      mix_tree->Branch("qxsum",  &qxsum, "qxsum");
	      mix_tree->Branch("qysum",  &qysum, "qysum");
	      mix_tree->Branch("qtA",  &qtA, "qtA");
	      mix_tree->Branch("qtB",  &qtB, "qtB");
	      mix_tree->Branch("qtL",  &qtL, "qtL");
	      mix_tree->Branch("qtM",  &qtM, "qtM");
        
        int photon_uses[(int) allPhoton.size()] = {};
        int dimuon_uses[(int) allJpsi.size()] = {};
        int photon_considered[(int) allPhoton.size()] = {};
        int dimuon_considered[(int) allJpsi.size()] = {};
        int events_entered =0;
        bool quit = false;
        bool force_inclusion = true;
        
        if (n_count == 0){
          use_n = false;
        } else {
          use_n = true;
        }

        std::cout << "mixing initiated" << std::endl;
        std::cout << "n - " << n_count << std::endl;
        std::cout << "k - " << k << std::endl;
        if (use_n){ std::cout << "object use capped at " << n_count << std::endl;}
        else if (!use_n){ std::cout << "object use uncapped" << std::endl;}
        std::cout << "event pass count: " << std::endl;

        //std::cout << std::endl;
        int jphoton_cap;
        int jphoton_min;
        int ijpsi_cap = allJpsi.size();
        int ijpsi_min = 0;

        if (n_count == 1){
          jphoton_cap = 0; jphoton_min = -1; 
        } else if (j_mode){
          jphoton_cap = allPhoton.size(); jphoton_min = 0; 
        } else if (test_ten){
          jphoton_cap = 10; jphoton_min = 0; ijpsi_cap = 10; ijpsi_min = 0; 
        } else {
          //ijpsi_min = 0; ijpsi_cap = allJpsi.size(); jphoton_min = -1; jphoton_cap = allPhoton.size();
          ijpsi_min = 0; ijpsi_cap = 1000; jphoton_min = -1; jphoton_cap = 1000;
        }

        double start{0}, pass{0};

        int ijpsi;
        int jphoton;

        //for (int ijpsi = ijpsi_min; ijpsi < ijpsi_cap; ijpsi++){
        while (true){
            //if (quit) break;
          //for (int jphoton = jphoton_min; jphoton < jphoton_cap; jphoton++){
            if (quit) break;

            int dimu_total_uses = 0;
            int phot_total_uses = 0;
            std::accumulate(dimuon_uses, dimuon_uses+jphoton_cap, dimu_total_uses);
            std::accumulate(photon_uses, photon_uses+ijpsi_cap, phot_total_uses);

            if (dimu_total_uses >= n_count*ijpsi_cap*0.9) { quit = true; }
            if (phot_total_uses >= n_count*jphoton_cap*0.9) { quit = true; }
            if (events_entered >= 99000) {quit = true;}


            //if (events_entered == 0){
            //  jphoton = -1;
            //  ijpsi = -1;
            //}else {
              bool jpsi_selected = false; 
              bool phot_selected = false; 
              while (!jpsi_selected){
                ijpsi = rand() % ijpsi_cap;
                if (dimuon_uses[ijpsi] < n_count) jpsi_selected = true;
              }
              //std::cout << "jpsi " << ijpsi << std::endl;
              while (!phot_selected){
                jphoton = rand() % jphoton_cap;
                if (photon_uses[jphoton] < n_count) phot_selected = true;
                //std::cout << "photon " << jphoton << std::endl;
              }
            //}

            
            //std::cout << "327" << std::endl;

            ppmix_cutflow->Fill(1);
            
            int jpsi_idx = ijpsi;
            int phot_idx = jphoton;
            if (jphoton == -1){ phot_idx = ijpsi; } 
            start+=1;

            if ((jphoton != -1) && (rand_root->Rndm() > k)){ 
              continue;
            }
            ppmix_cutflow->Fill(2);
            //std::cout << "340" << std::endl;

            if (phot_idx == jpsi_idx && jphoton != -1 && (!test_ten || j_mode) ){ continue; }
            ppmix_cutflow->Fill(3);

            //std::cout << "345" << std::endl;

            if (n_count == 1 && (phot_idx != jpsi_idx)){ continue; }
            ppmix_cutflow->Fill(4);
            //std::cout << "349" << std::endl;
            //std::cout << phot_idx << std::endl;
            if (use_n && photon_uses[phot_idx] >= n_count){
              continue;
            }
            //std::cout << "353" << std::endl;
            ppmix_cutflow->Fill(5);
            if (use_n && dimuon_uses[jpsi_idx] >= n_count){
              break;
            }
            ppmix_cutflow->Fill(6);

            photon_considered[phot_idx] += 1;
            dimuon_considered[jpsi_idx] += 1;
            
            auto &jpsi = allJpsi.at(jpsi_idx);
	          auto &photon = allPhoton.at(phot_idx);

	          mumuy = jpsi.mupl + jpsi.mumi + photon.p4;

            // this needs called as it does a lot of work and preps a tonne of variables
            bool res = VatoCS(jpsi.mupl, jpsi.mumi, photon.p4, CSAxis, xAxis, yAxis, cosTheta_dimu, cosTheta_gamma, phi_dimu, phi_gamma);

            double lambda = pow(mumuy.M() / 3097.0, 2);
	          double qTsquared = pow(mumuy.Pt() / 1000., 2);
              
            double dY = (jpsi.DiLept_Y - photon.Photon_Eta);
	          double dPhi = (jpsi.DiLept_Phi - photon.Photon_Phi);
	          while (dPhi >  M_PI){ dPhi -= 2 * M_PI;}
	          while (dPhi < -M_PI){ dPhi += 2 * M_PI;}

	          AbsCosTheta = sqrt(cosTheta_dimu * cosTheta_dimu);
	          AbsdPhi = (sqrt(dPhi * dPhi));
	          DPhi = dPhi;
	          DY = dY;
	          AbsdY = fabs(dY);
	          Phi = (phi_dimu >= 0 ? (phi_dimu - M_PI) : (phi_dimu + M_PI));
	          costheta = cosTheta_dimu;
	          AbsPhi = sqrt(phi_dimu * phi_dimu);
	          EventNumber = jpsi.EventNumber;
	          Lambda = lambda;
	          qTSquared = qTsquared;
	          DiMuonMass = jpsi.DiMuonMass;
	          DiMuonTau = jpsi.DiMuonTau;
	          DiMuonPt = jpsi.DiMuonPt;
	          PhotonPt = photon.PhotonPt;
	          Trigger_HLT_2mu4_bJpsimumu_noL2 = jpsi.Trigger_HLT_2mu4_bJpsimumu_noL2;
	          qxpsi = jpsi.DiMuonPt/1000 + (photon.PhotonPt/1000)*cos(dPhi);
	          qypsi = (photon.PhotonPt/1000)*sin(dPhi);
	          qxgamma = photon.PhotonPt/1000 + (jpsi.DiMuonPt/1000)*cos(dPhi);
	          qygamma = (jpsi.DiMuonPt/1000) *sin(dPhi);
	          qxsum = (jpsi.DiMuonPt/1000 + photon.PhotonPt/1000)*(1+cos(dPhi));
	          qysum = (jpsi.DiMuonPt/1000 + photon.PhotonPt/1000)*(sin(dPhi));
	          qtA = (jpsi.DiMuonPt/1000) - (photon.PhotonPt/1000);
	          qtB = sqrt((jpsi.DiMuonPt/1000) * (photon.PhotonPt/1000)) * sin(dPhi);
	          qtL = ((jpsi.DiMuonPt/1000) - (photon.PhotonPt/1000))*cos((M_PI-abs(dPhi))/2.0);
	          qtM = ((jpsi.DiMuonPt/1000) - (photon.PhotonPt/1000))*sin((M_PI-abs(dPhi))/2.0);

	          double Qr = qTsquared;
	          double Cr = costheta * costheta;
	          double Mr = DiMuonMass;
	          double Tr = DiMuonTau;
	          double Pr = AbsPhi;
	          double Lr = lambda;



            for (int l = 0; l < LambdaIndex.size(); l++){ // Loop through Lambda Indices
              //for (int q = 0; q < qTSquaredIndex.size(); q++) // Loop through qT2 Indices.
              for (int q = 0; q < qTAIndex.size(); q++){ // Loop through qTA Indices.

                if (!train){
                  if (lambda < 15) continue;
                }

                ppmix_cutflow->Fill(7);
                if (abs(qtB) > 20 && bound_mode < 1) continue;
                ppmix_cutflow->Fill(8);
                pass+=1;

                for (unsigned int iC=0; iC<CosThetaRange.size(); iC++){

	                if( Cr <= CosThetaRange.at(iC).first || Cr > CosThetaRange.at(iC).second) continue;
                  ppmix_cutflow->Fill(9);
	                for (unsigned int iL=0; iL<LambdaRange.size(); iL++){  //CS loop

                    if( Lambda <= LambdaRange.at(iL).first || Lambda > LambdaRange.at(iL).second) continue; 
                    ppmix_cutflow->Fill(10);
                    for (unsigned int iQ=0; iQ<qTSquaredRange.size(); iQ++){ 

                      if(( Qr <= qTSquaredRange.at(iQ).first || Qr > qTSquaredRange.at(iQ).second) && bound_mode < 2) continue; // THIS HAD BEEN MISSED
                      ppmix_cutflow->Fill(11);
                      if( (qtA <= qTARange.at(iQ).first || qtA > qTARange.at(iQ).second) && bound_mode < 1 ) continue;
                      ppmix_cutflow->Fill(12);
                      for (unsigned int iP = 0; iP < PhiRange.size(); iP++){
                  
                        // Phi cs definition in earlier loop is different, shouldn't make a differenfe
                        if (Pr <= PhiRange.at(iP).first || Pr > PhiRange.at(iP).second) continue;
                        ppmix_cutflow->Fill(13);
	          	          for (unsigned int iM=0; iM<MassRange.size(); iM++){

                          // Don't really need to check this
	          	            if( Mr <= MassRange.at(iM).first || Mr > MassRange.at(iM).second) continue;
                          ppmix_cutflow->Fill(14);
	          	            for (unsigned int iT=0; iT<LifetimeRange.size(); iT++){

	         	                if( DiMuonTau <= LifetimeRange.at(iT).first || DiMuonTau > LifetimeRange.at(iT).second) continue;
                            ppmix_cutflow->Fill(15);
	                          if ( iL == l && iQ == q){ 
                              
                              mix_tree->Fill();
                              ppmix_cutflow->Fill(16);

                              std::cout << dimuon_uses[jpsi_idx] << std::endl;
                              std::cout << events_entered << std::endl;

                              events_entered++;
                              dimuon_uses[jpsi_idx] += 1;
                              photon_uses[phot_idx] += 1;
                              if (events_entered%1000 == 0){
                                std::cout << events_entered << " ";
                              }
                              //if (events_entered%10000 == 0){
                              //  std::cout << events_entered << " ";
                              //} 
                              if (events_entered == (4.5)*1e6){
                                std::cout << "4.5m event threshold, finishing" << std::endl;
                                quit = true;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
        //  }
        }
        //mix_file->cd();
	      mix_tree->Write();
	      mix_file->Close();

        std::cout << "Threshold or events used to cap, finishing" << std::endl;
        std::cout << "Events considered - " << start << std::endl;
        std::cout << "Events skipped (rand) - " << start - pass << std::endl;
        std::cout << "Events kept (rand) - " << pass << std::endl;
        std::cout << "calculated keep fraction - " << pass/start << std::endl;
        int use_bins = 200;

    
        TH1F * hist_photon_uses = new TH1F("h_photon_uses","photon uses",use_bins,-0.5,use_bins-0.5);
        TH1F * hist_dimuon_uses = new TH1F("h_dimuon_uses","dimuon uses",use_bins,-0.5,use_bins-0.5);
        TH1F * hist_photon_considered = new TH1F("h_photon_considered","photons considered",use_bins,-0.5,use_bins-0.5);
        TH1F * hist_dimuon_considered = new TH1F("h_dimuon_considered","dimuons considered",use_bins,-0.5,use_bins-0.5);
        TH1F * hist_photon_fail = new TH1F("h_photon_fails","photons failed",use_bins,-0.5,use_bins-0.5);
        TH1F * hist_dimuon_fail = new TH1F("h_dimuon_fails","dimuons failed",use_bins,-0.5,use_bins-0.5);


        for ( int i = 0; i < allJpsi.size(); i++){
          hist_dimuon_uses->Fill(dimuon_uses[i]);
          hist_dimuon_considered->Fill(dimuon_considered[i]);
          hist_dimuon_fail->Fill((dimuon_considered[i]-dimuon_uses[i]));
        }
        for ( int i = 0; i < allPhoton.size(); i++){
          hist_photon_uses->Fill(photon_uses[i]);
          hist_photon_considered->Fill(photon_considered[i]);
          hist_photon_fail->Fill((photon_considered[i]-photon_uses[i]));
        }


        TCanvas * uses = new TCanvas("uses","uses",400,600,3000,1000);
        uses->Divide(3,1);
        uses->cd(1);
        hist_photon_uses->SetLineColor(2);
        hist_dimuon_uses->SetLineColor(4);
        hist_dimuon_uses->SetLineStyle(2);
        hist_photon_uses->Draw();
        hist_dimuon_uses->Draw("SAME");
        hist_photon_uses->GetYaxis()->SetTitle("Object uses");
        hist_photon_uses->GetYaxis()->SetRangeUser(1,std::max(hist_photon_uses->GetMaximum(),hist_dimuon_uses->GetMaximum())*1.3);
        uses->SetLogy(true);
        TLegend * uses_legend = new TLegend(0.7,0.8,0.925,0.925);
        uses_legend->AddEntry(hist_photon_uses,Form("Photon uses - int %i",(int) hist_photon_uses->Integral()));
        uses_legend->AddEntry(hist_dimuon_uses,Form("Dimuon uses - int %i",(int) hist_dimuon_uses->Integral()));
        uses_legend->Draw();
        
        uses->cd(2);
        hist_photon_considered->SetLineColor(2);
        hist_dimuon_considered->SetLineColor(4);
        hist_dimuon_considered->SetLineStyle(2);
        hist_photon_considered->Draw();
        hist_dimuon_considered->Draw("SAME");
        hist_photon_considered->GetYaxis()->SetTitle("times considered");
        hist_photon_considered->GetYaxis()->SetRangeUser(1,std::max(hist_photon_considered->GetMaximum(),hist_dimuon_considered->GetMaximum())*1.3);
        uses->SetLogy(true);
        TLegend * consider_legend = new TLegend(0.6,0.8,0.925,0.925);
        consider_legend->AddEntry(hist_photon_considered,Form("Photon considerations - int %i",(int) hist_photon_considered->Integral()));
        consider_legend->AddEntry(hist_dimuon_considered,Form("Dimuon considerations - int %i",(int) hist_dimuon_considered->Integral()));
        consider_legend->Draw();

        uses->cd(3);
        hist_photon_fail->SetLineColor(2);
        hist_dimuon_fail->SetLineColor(4);
        hist_dimuon_fail->SetLineStyle(2);
        hist_photon_fail->Draw();
        hist_dimuon_fail->Draw("SAME");
        hist_photon_fail->GetYaxis()->SetTitle("Selection fails");
        hist_photon_fail->GetYaxis()->SetRangeUser(1,std::max(hist_photon_fail->GetMaximum(),hist_dimuon_fail->GetMaximum())*1.3);
        uses->SetLogy(true);
        TLegend * fails_legend = new TLegend(0.6,0.8,0.925,0.925);
        fails_legend->AddEntry(hist_photon_fail,Form("Photon fails selection - int %i",(int) hist_photon_fail->Integral()));
        fails_legend->AddEntry(hist_dimuon_fail,Form("Dimuon fails selection - int %i",(int) hist_dimuon_fail->Integral()));
        fails_legend->Draw();



        if (!thousand){ uses->SaveAs(Form("stats/stats_rand_n%i_k%i.png",n_count,k_int));}
        else { uses->SaveAs(Form("stats/thousand_stats_rand_n%i_k%i.png",n_count,k_int));}

        delete hist_photon_uses;
        delete hist_dimuon_uses;
        delete hist_photon_considered;
        delete hist_dimuon_considered;
        delete hist_photon_fail;
        delete hist_dimuon_fail;
        delete uses;
        std::cout << std::endl;
      }
      if (n_count != 1){
        std::vector<std::string> ppmix_cutflow_names{"No cuts","k","rndesMd1","non-mix mode","photon n","jpsi n","lambda low","qtb","cos theta","lambda range","qt2","qta","phi","mass","tau","filled"};
        TCanvas * ppmix_cutflow_canv = new TCanvas(Form("ppmix_cutflow_canv_n%i",n_count),Form("ppmix_cutflow_canv_n%i",n_count),600,600,2000,1500);
        ppmix_cutflow_canv->Divide(1);
        ppmix_cutflow_canv->cd(1);
        TLegend * ppmix_cutflow_legend = new TLegend(0.225,0.225,0.4,0.5);
        ppmix_cutflow_legend->SetBorderSize(0);
        ppmix_cutflow_legend->SetTextFont(42);
        ppmix_cutflow_legend->SetFillStyle(0);
        ppmix_cutflow_legend->SetTextSize(0.015);
        for (int hist_idx = 0; hist_idx < ppmix_cutflow_vector.size(); hist_idx++){
          ppmix_cutflow_legend->AddEntry(ppmix_cutflow_vector[hist_idx],Form("k%i, %i created events",keep_vec_perc[hist_idx],(int) ppmix_cutflow_vector[hist_idx]->GetBinContent(16)));
          if (hist_idx == 0){
            ppmix_cutflow_vector[hist_idx]->Draw("HIST");
          } else {
            ppmix_cutflow_vector[hist_idx]->Draw("HIST SAME");
          }
          ppmix_cutflow_vector[hist_idx]->SetLineColorAlpha(hist_idx+1,1-(0.05*hist_idx));
          if (hist_idx == 0){
            TAxis * ppmix_cutflow_axis = (TAxis*) ppmix_cutflow_vector[hist_idx]->GetXaxis();
            for (int bin_no = 1; bin_no <= ((int) ppmix_cutflow_names.size()); ++bin_no){
              ppmix_cutflow_axis->SetBinLabel(bin_no,(ppmix_cutflow_names[bin_no-1]).c_str());
              ppmix_cutflow_axis->ChangeLabel(bin_no,-80.0);
            }
            ppmix_cutflow_vector[hist_idx]->GetYaxis()->SetTitle("Events/stage");
            ppmix_cutflow_vector[hist_idx]->GetYaxis()->SetRangeUser(1e2,1e8);
            ppmix_cutflow_vector[hist_idx]->GetXaxis()->SetTitle("Stage");
            gPad->SetLogy();
          }
        }
        ppmix_cutflow_legend->Draw();
        TPaveText * ppmix_cutflow_text = new TPaveText(0.8,0.8,0.925,0.925,"nbNDC");
        ppmix_cutflow_text->AddText(Form("n - %i",n_count));
        ppmix_cutflow_text->SetBorderSize(0);
        ppmix_cutflow_text->SetFillStyle(0);
        ppmix_cutflow_text->SetFillColor(0);
        ppmix_cutflow_text->SetTextFont(42);
        ppmix_cutflow_text->SetTextSize(0.025);
        ppmix_cutflow_text->SetTextAlign(31);
        ppmix_cutflow_text->Draw();

        if (!thousand) {ppmix_cutflow_canv->SaveAs(Form("stats/ppmix_rand_cutflow_n%i.png",n_count));}
        else { ppmix_cutflow_canv->SaveAs(Form("stats/thousand_ppmix_rand_cutflow_n%i.png",n_count));}
        TFile * cutflow_file = new TFile(Form("cutflow_n%i.root",n_count),"RECREATE");
        cutflow_file->cd();
        for (int hist_idx = 0; hist_idx < ppmix_cutflow_vector.size(); hist_idx++){
          ppmix_cutflow_vector[hist_idx]->Write();
        } 
        cutflow_file->Close();
        ppmix_cutflow_canv->Close();
        delete ppmix_cutflow_canv;
        delete cutflow_file;
      }
    }
  }
}//Closing brace
 
int main(int argc, char ** argv){

  if (argc == 4){TMVAGenerateInput(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));}
  if (argc == 5){ std::cout << "this one" << std::endl; TMVAGenerateInput(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]),atoi(argv[4]));}
  if (argc == 6){TMVAGenerateInput(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]),atoi(argv[4]),atoi(argv[5]));}
  if (argc == 7){TMVAGenerateInput(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]),atoi(argv[4]),atoi(argv[5]),atoi(argv[6]));}
  return 0;
}
//Jpsi jpsi;
		                      //jpsi.EventNumber = EventNumber;
		                      //jpsi.DiMuonPt = DiMuonPt;
		                      //jpsi.DiMuonMass = DiMuonMass;
		                      //jpsi.DiMuonTau = DiMuonTau;
		                      //jpsi.Trigger_HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;
		                      //jpsi.old_lambda = Lambda;
		                      //jpsi.old_qt2 = qTsquaredPreSelection;
		                      //jpsi.old_csPhi = t->MuMuGamma_CS_Phi->at(0);
		                      //jpsi.old_csCosTheta = t->MuMuGamma_CS_CosTheta->at(0);
		                      //jpsi.DiLept_Y = t->DiLept_Y->at(0);
		                      //jpsi.DiLept_Phi = t->DiLept_Phi->at(0);
		                      //jpsi.mupl.SetXYZM(t->DiMuonVertex_Muon0_Px->at(0),
			                    //	    t->DiMuonVertex_Muon0_Py->at(0),
			                    //	    t->DiMuonVertex_Muon0_Pz->at(0),
			                    //	    t->MuPlus_M->at(0));
		                      //jpsi.mumi.SetXYZM(t->DiMuonVertex_Muon1_Px->at(0),
			                    //	    t->DiMuonVertex_Muon1_Py->at(0),
			                    //	    t->DiMuonVertex_Muon1_Pz->at(0),
			                    //	    t->MuMinus_M->at(0));
		                      //jpsi.p4.SetPtEtaPhiM(t->DiLept_Pt->at(0),
			                    //	       t->DiLept_Eta->at(0),
			                    //	       t->DiLept_Phi->at(0),
			                    //	       t->DiLept_M->at(0));

		                      //Photon photon;
		                      //photon.p4.SetPtEtaPhiE(t->Photon_Pt->at(0),
			                    //		 t->Photon_Eta->at(0),
			                    //		 t->Photon_Phi->at(0),
			                    //		 t->Photon_E->at(0));
		                      //photon.PhotonPt = t->Photon_Pt->at(0);
		                      //photon.Photon_Eta = t->Photon_Eta->at(0);
		                      //photon.Photon_Phi = t->Photon_Phi->at(0);

                          //allPhoton.push_back(photon); 
                          //allJpsi.push_back(jpsi);



	      		            //  if(jentry < nentries/2){
	      		            //MC_Plots->cd();

