//  root -l Plots.cxx+g
//   . /afs/cern.ch/user/a/atee/root_6_script.sh

// IF THERE IS NO MC IN THE NAME IT IS A RECO VARIABLE!!!!

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TLorentzVector.h"
#include "RooCategory.h"
#include "Roo1DTable.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCBShape.h"
#include "RooAbsReal.h"
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooPolynomial.h"
#include "math.h"
#include "assert.h"
#include "TLegend.h"
#include "stdio.h"

//#ifdef __MAKECINT__
//#pragma link C++ class vector<float>+;
//#pragma link C++ class vector<vector<int> >+;
//#endif

#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <math.h>

// use this order for safety on library loading
using namespace RooFit;
using namespace RooStats;
using namespace std;

#include "treeReader.C"
//#include "treeReader.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>
#include "TString.h"
#include <math.h>

#include <cassert>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TRandom3.h"

bool VatoCS(const TLorentzVector &mupl,
	    const TLorentzVector &mumi,
	    const TLorentzVector &gamma,
	    TVector3 &CSAxis,
	    TVector3 &xAxis,
	    TVector3 &yAxis,
	    double &cosTheta_dimu,
	    double &cosTheta_gamma,
	    double &phi_dimu,
	    double &phi_gamma);

struct Jpsi
{
  Float_t DiMuonMass;
  Float_t DiMuonTau;
  Float_t DiMuonPt;
  Float_t DiLept_Y;
  Float_t DiLept_Phi;
  Float_t Trigger_HLT_2mu4_bJpsimumu_noL2;
  Float_t EventNumber;
  TLorentzVector mupl, mumi;
  TLorentzVector p4;
  Float_t old_lambda, old_qt2;
  Float_t old_csPhi, old_csCosTheta;
  // Int_t   JpsiIndex;
};
struct Photon
{
  Float_t PhotonPt;
  Float_t Photon_Eta;
  Float_t Photon_Phi;
  TLorentzVector p4;
  // Int_t  PhotonIndex;
};

void ppEventMix()
{ //OPENING BRACE
  bool doMixing =true;
  double keepFraction = 0.05; // what fraction of mixed events should we write out   this was set to 0.05
    
  TRandom3 * rand = new TRandom3(5432);
    
  std::vector<Jpsi> allJpsi;
  std::vector<Photon> allPhoton;

  std::vector<std::string> photonCut = {"5000.0"};
  Float_t AbsCosTheta, AbsdPhi, AbsdY, DPhi, DY, Phi, costheta, AbsPhi, EventNumber, Lambda, qTSquared, DiMuonMass, DiMuonTau, DiMuonPt, PhotonPt, Trigger_HLT_2mu4_bJpsimumu_noL2, qxpsi, qypsi, qxgamma, qygamma, qxsum, qysum, qtA, qtB;
  Int_t PhotonIndex, JpsiIndex;
  std::map<TString, TFile *> files;
  std::map<TString, TTree *> trees;

  
  std::map<TString, Long64_t > counters;


  typedef std::vector<std::pair<double, double>> Ranges;

  Ranges MassRange = {{2700.0, 3500.0}};																																						// Various J/psi mass intervals
  Ranges LambdaRange = {{0.0, 200.0}};																				//Various Lambda intervals
  Ranges qTSquaredRange = {{0.0, 400.0}}; //Various qT Squared intervals  //Various qT Squared intervals
  Ranges LifetimeRange = {{-5.0, 15.0}};																																						//various lifetime ranges
  Ranges CosThetaRange{{0.0, 1.0}};
  Ranges PhiRange = {{0.0, 3.142}}u;

     //Creating a chain of all the trees in the various ntuples
      TChain *ch = new TChain("tree");

      //Adding the ntuples to the chain
      ch->Add("/home/atlas/jww/forAmy/EventMixing/pp_ntuple.root");

      //declare a new reader
      treeReader *t = new treeReader(ch);
       Long64_t nentries = t->fChain->GetEntries();
       std::cout << "nentries: " << nentries << std::endl;

       int evtCounter = 0;

  for (int i = 0; i < photonCut.size(); i++)
    {

      int pCut = std::stoi(photonCut.at(i));
      std::cout << pCut << std::endl;

      //Creating a chain of all the trees in the various ntuples
      //   TChain *ch = new TChain("tree");

      //Adding the ntuples to the chain
      ///  ch->Add("/home/atlas/jww/forAmy/EventMixing/pp_ntuple.root");

      //declare a new reader
      // treeReader *t = new treeReader(ch);
      //  Long64_t nentries = t->fChain->GetEntries();



      //Identify each slice with an index. More indices than slices to create empty histograms that will be used for background subtraction
      std::vector<int> MassIndex = {0};
      std::vector<int> LambdaIndex = {0};
      std::vector<int> qTSquaredIndex = {0};
      std::vector<int> PhiIndex = {0};
      std::vector<int> LifetimeIndex = {0};
      std::vector<int> CosThetaIndex = {0};

      // build trees
      for (int l = 0; l < LambdaIndex.size(); l++)
	{ // Loop through Lambda Indices
	  for (int q = 0; q < qTSquaredIndex.size(); q++)
	    { // Loop through qT2 Indices.
	      TFile *MC_Plots = new TFile(Form("pp_%i%i_P%i.root", LambdaIndex.at(l), qTSquaredIndex.at(q), pCut), "RECREATE");
	    MC_Plots->cd(); //Selects this new root file for use.
 files[Form("%i%i", LambdaIndex.at(l), qTSquaredIndex.at(q))] = MC_Plots;

      TTree *tr = new TTree("TreeB", "tree");
	      trees[Form("%i%i", LambdaIndex.at(l), qTSquaredIndex.at(q))] = tr;

//TFile *MC_Plots2 = new TFile(Form("pp_%i%i_P%i_2.root", LambdaIndex.at(l), qTSquaredIndex.at(q), pCut), "RECREATE");
	     // MC_Plots2->cd(); //Selects this new root file for use.
	
	     // files[Form("%i%i_2", LambdaIndex.at(l), qTSquaredIndex.at(q))] = MC_Plots2;


	    // TTree *tr2 = new TTree("TreeB", "tree");
	    //  trees[Form("%i%i_2", LambdaIndex.at(l), qTSquaredIndex.at(q))] = tr2;
////
	        
	      tr->Branch("JpsiIndex",&JpsiIndex,"JpsiIndex/I");
	      tr->Branch("PhotonIndex",&PhotonIndex,"PhotonIndex/I");

	   //  tr2->Branch("JpsiIndex",&JpsiIndex,"JpsiIndex/I");
	   //  tr2->Branch("PhotonIndex",&PhotonIndex,"PhotonIndex/I");
//
				  
	      tr->Branch("AbsCosTheta", &AbsCosTheta, "AbsCosTheta/F");
	      tr->Branch("AbsdPhi", &AbsdPhi, "AbsdPhi/F");
	      tr->Branch("AbsPhi", &AbsPhi, "AbsPhi/F");
	      tr->Branch("DPhi", &DPhi, "DPhi/F");
	      tr->Branch("DY", &DY, "DY/F");
	      tr->Branch("Phi", &Phi, "phi/F");
	      tr->Branch("costheta", &costheta, "CosTheta/F");
	      tr->Branch("EventNumber", &EventNumber, "EventNumber/F");
	      tr->Branch("Lambda", &Lambda, "Lambda/F");
	      tr->Branch("qTSquared", &qTSquared, "qTSquared/F");
	      tr->Branch("DiMuonMass", &DiMuonMass, "DiMuonMass/F");
	      tr->Branch("DiMuonTau", &DiMuonTau, "DiMuonTau/F");
	      tr->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	      tr->Branch("DiMuonPt", &DiMuonPt, "DiMuonPt/F");
	      tr->Branch("PhotonPt", &PhotonPt, "PhotonPt/F");
	      tr->Branch("AbsdY", &AbsdY, "AbsdY");
	      tr->Branch("qxpsi", &qxpsi, "qxpsi");
	      tr->Branch("qypsi", &qypsi, "qypsi");
	      tr->Branch("qxgamma", &qxgamma, "qxgamma");
	      tr->Branch("qygamma", &qygamma, "qygamma");
	      tr->Branch("qxsum",  &qxsum, "qxsum");
	      tr->Branch("qysum",  &qysum, "qysum");
	      tr->Branch("qtA",  &qtA, "qtA");
	      tr->Branch("qtB",  &qtB, "qtB");


	  /*   tr2->Branch("AbsCosTheta", &AbsCosTheta, "AbsCosTheta/F");
	      tr2->Branch("AbsdPhi", &AbsdPhi, "AbsdPhi/F");
	      tr2->Branch("AbsPhi", &AbsPhi, "AbsPhi/F");
	      tr2->Branch("DPhi", &DPhi, "DPhi/F");
	      tr2->Branch("DY", &DY, "DY/F");
	      tr2->Branch("Phi", &Phi, "phi/F");
	      tr2->Branch("costheta", &costheta, "CosTheta/F");
	      tr2->Branch("EventNumber", &EventNumber, "EventNumber/F");
	      tr2->Branch("Lambda", &Lambda, "Lambda/F");
	      tr2->Branch("qTSquared", &qTSquared, "qTSquared/F");
	      tr2->Branch("DiMuonMass", &DiMuonMass, "DiMuonMass/F");
	      tr2->Branch("DiMuonTau", &DiMuonTau, "DiMuonTau/F");
	      tr2->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	      tr2->Branch("DiMuonPt", &DiMuonPt, "DiMuonPt/F");
	      tr2->Branch("PhotonPt", &PhotonPt, "PhotonPt/F");
	      tr2->Branch("AbsdY", &AbsdY, "AbsdY");
	      tr2->Branch("qxpsi", &qxpsi, "qxpsi");
	      tr2->Branch("qypsi", &qypsi, "qypsi");
	      tr2->Branch("qxgamma", &qxgamma, "qxgamma");
	      tr2->Branch("qygamma", &qygamma, "qygamma");
	      tr2->Branch("qxsum",  &qxsum, "qxsum");
	      tr2->Branch("qysum",  &qysum, "qysum");
	      tr2->Branch("qtA",  &qtA, "qtA");
	      tr2->Branch("qtB",  &qtB, "qtB");*/
	    }
	}

      //Loop through Cos Theta Indices
      for (int l = 0; l < LambdaIndex.size(); l++)
	{ // Loop through Lambda Indices
	  for (int q = 0; q < qTSquaredIndex.size(); q++)
	    { // Loop through qT2 Indices.

	      if (l != 0)
		continue;
	      if (q != 0)
		continue;

	      // //Declare a new root file in which to save future histograms
	      // TFile *MC_Plots = new TFile(Form("pp_%i%i_P%i.root", LambdaIndex.at(l), qTSquaredIndex.at(q), pCut), "RECREATE");
	      // MC_Plots->cd(); //Selects this new root file for use.

	      // TTree *tr = new TTree("TreeB", "tree");

	      // tr->Branch("AbsCosTheta", &AbsCosTheta, "AbsCosTheta/F");
	      // tr->Branch("AbsdPhi", &AbsdPhi, "AbsdPhi/F");
	      // tr->Branch("AbsPhi", &AbsPhi, "AbsPhi/F");
	      // tr->Branch("DPhi", &DPhi, "DPhi/F");
	      // tr->Branch("DY", &DY, "DY/F");
	      // tr->Branch("Phi", &Phi, "phi/F");
	      // tr->Branch("costheta", &costheta, "CosTheta/F");
	      // tr->Branch("EventNumber", &EventNumber, "EventNumber/F");
	      // tr->Branch("Lambda", &Lambda, "Lambda/F");
	      // tr->Branch("qTSquared", &qTSquared, "qTSquared/F");
	      // tr->Branch("DiMuonMass", &DiMuonMass, "DiMuonMass/F");
	      // tr->Branch("DiMuonTau", &DiMuonTau, "DiMuonTau/F");
	      // tr->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	      // tr->Branch("DiMuonPt", &DiMuonPt, "DiMuonPt/F");
	      // tr->Branch("PhotonPt", &PhotonPt, "PhotonPt/F");

	      //###########################################
	      //            OPEN MAIN FOR LOOP
	      //###########################################

	      if (t->fChain == 0)
		return;

	     
	  
	      //Loop over every event in the file:

	      Long64_t nbytes = 0, nb = 0;
	      for (Long64_t jentry = 0; jentry < nentries; jentry++)
		{ //Read in this event:
		  Long64_t ientry = t->LoadTree(jentry);
		  if (ientry < 0)
		    break;
		  nb = t->fChain->GetEntry(jentry);
		  nbytes += nb;

		//if (jentry == 100000 )break;

		  //  std::cout << "boop" << std::endl;
		  

		  //##################################################
		  //                FILL HISTOGRAMS
		  //##################################################


		  if (t->Photon_Pt->size() == 0)
		    continue;

		  if (t->Photon_Pt->at(0) < pCut)
		    continue;
		  if (t->MuMinus_Pt->at(0) < (4000))
		    continue;
		  if (t->MuPlus_Pt->at(0) < (4000))
		    continue;

		  if (t->HLT_2mu4_bJpsimumu_noL2 == 0)
		    continue;
		  if (t->Photon_quality->at(0) > 1)
		    continue;

		  double dimuonmass = t->DiLept_M->at(0);
		  double dimuontau = t->DiMuonVertex_Tau->at(0);
		  double dimuonpt = t->DiLept_Pt->at(0);
		  double photonpt = t->Photon_Pt->at(0);

		  //UInt_t eventnumber = evt;
		  DiMuonPt = dimuonpt;
		  EventNumber = t->evt;
		  DiMuonMass = dimuonmass;
		  DiMuonTau = dimuontau;
		  Trigger_HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;
		  PhotonPt = photonpt;

		  double lambda = ((t->MuMuY_M->at(0)) / 3097.0 * (t->MuMuY_M->at(0)) / 3097.0);
		  double qTsquared = (t->MuMuY_Pt->at(0) / 1000.0) * (t->MuMuY_Pt->at(0) / 1000.0);

		  Jpsi jpsi;
		  Photon photon;

		  jpsi.EventNumber = EventNumber;
		  jpsi.DiMuonPt = DiMuonPt;
		  jpsi.DiMuonMass = DiMuonMass;
		  jpsi.DiMuonTau = DiMuonTau;
		  jpsi.Trigger_HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;
		  jpsi.old_lambda = lambda;
		  jpsi.old_qt2 = qTsquared;
		  jpsi.old_csPhi = t->MuMuGamma_CS_Phi->at(0);
		  jpsi.old_csCosTheta = t->MuMuGamma_CS_CosTheta->at(0);
		  jpsi.DiLept_Y = t->DiLept_Y->at(0);
		  jpsi.DiLept_Phi = t->DiLept_Phi->at(0);

		  // jpsi.mupl.SetPtEtaPhiM(t->MuPlus_Pt ->at(0),
		  // 					 t->MuPlus_Eta  ->at(0),
		  // 					 t->MuPlus_Phi  ->at(0),
		  // 					 t->MuPlus_M    ->at(0));
		  // jpsi.mumi.SetPtEtaPhiM(t->MuMinus_Pt->at(0),
		  // 					 t->MuMinus_Eta ->at(0),
		  // 					 t->MuMinus_Phi ->at(0),
		  // 					 t->MuMinus_M   ->at(0));

		  jpsi.mupl.SetXYZM(t->DiMuonVertex_Muon0_Px->at(0),
				    t->DiMuonVertex_Muon0_Py->at(0),
				    t->DiMuonVertex_Muon0_Pz->at(0),
				    t->MuPlus_M->at(0));
		  jpsi.mumi.SetXYZM(t->DiMuonVertex_Muon1_Px->at(0),
				    t->DiMuonVertex_Muon1_Py->at(0),
				    t->DiMuonVertex_Muon1_Pz->at(0),
				    t->MuMinus_M->at(0));

		  jpsi.p4.SetPtEtaPhiM(t->DiLept_Pt->at(0),
				       t->DiLept_Eta->at(0),
				       t->DiLept_Phi->at(0),
				       t->DiLept_M->at(0));

		  photon.p4.SetPtEtaPhiE(t->Photon_Pt->at(0),
					 t->Photon_Eta->at(0),
					 t->Photon_Phi->at(0),
					 t->Photon_E->at(0));
		  photon.PhotonPt = t->Photon_Pt->at(0);
		  photon.Photon_Eta = t->Photon_Eta->at(0);
		  photon.Photon_Phi = t->Photon_Phi->at(0);

	     

		  allPhoton.push_back(photon);
		  allJpsi.push_back(jpsi);

		  if (fabs(t->MuMuGamma_CS_Phi->at(0)) > M_PI) {
		    cout << "X: " << t->MuMuGamma_CS_Phi->at(0) << endl;
		  }



		  ++evtCounter;

		} //closing loop over entries

		        
	    }
	}
    }

  std::cout << "events after cuts:" << evtCounter << std::endl;
  
  TVector3 CSAxis;
  TVector3 xAxis;
  TVector3 yAxis;
  double cosTheta_dimu;
  double cosTheta_gamma;
  double phi_dimu;
  double phi_gamma;
  TLorentzVector mumuy;

  cout << "Start event mixing" << endl;
  cout << "NJpsi:   " << allJpsi.size() << endl;
  cout << "NPhoton: " << allPhoton.size() << endl;

  for (unsigned int ijpsi = 0; ijpsi != allJpsi.size(); ++ijpsi)
    {
      auto &jpsi = allJpsi.at(ijpsi);

      for (unsigned int jphoton = 0; jphoton != allPhoton.size(); ++jphoton)
	{
	  //if (ijpsi != jphoton) continue; // no mixing
	  //if (jphoton > 0) continue;
	  //jphoton = ijpsi; // ! for testing
	  if (doMixing == false) {
	    if (ijpsi != jphoton) continue;
	  } else if ((ijpsi != jphoton) && (keepFraction < 1.0) ) {
	    double b = rand->Rndm();
	    if (b > keepFraction) continue;
	  }
            
            
	  auto &photon = allPhoton.at(jphoton);

	  mumuy = jpsi.mupl + jpsi.mumi + photon.p4;

	  bool res = VatoCS(jpsi.mupl,
			    jpsi.mumi,
			    photon.p4,
			    CSAxis,
			    xAxis,
			    yAxis,
			    cosTheta_dimu,
			    cosTheta_gamma,
			    phi_dimu,
			    phi_gamma);

	  // jpsi.mupl.Dump();
	  // jpsi.mumi.Dump();
	  // photon.p4.Dump();

	  double lambda = pow(mumuy.M() / 3097.0, 2);
	  double qTsquared = pow(mumuy.Pt() / 1000., 2);


	  if (lambda < 15)
	    continue;


	  if (ijpsi == jphoton) {
	    cout << "\n\nEvent: " << ijpsi << " " << jphoton << endl;
	    cout << "CS: " << jpsi.old_csPhi << " " << phi_dimu << endl;
	    cout << "CS: " << jpsi.old_csCosTheta << " " << cosTheta_dimu << endl;
	    cout << "L : " << jpsi.old_lambda << " " << lambda << endl;
	    cout << "P : " << jpsi.old_qt2 << " " << qTsquared << endl;
	  }

	  // Phi = (t->MuMuGamma_CS_Phi->at(0) >= 0 ? (t->MuMuGamma_CS_Phi->at(0) - M_PI) : (t->MuMuGamma_CS_Phi->at(0) + M_PI));
	  // costheta = t->MuMuGamma_CS_CosTheta->at(0);

	  // DY = dY;
	  // AbsCosTheta = (sqrt((t->MuMuGamma_CS_CosTheta->at(0) * t->MuMuGamma_CS_CosTheta->at(0))));
	  // DPhi = dPhi;
	  // AbsdPhi = (sqrt(dPhi * dPhi));
	  // double Pr = sqrt(t->MuMuGamma_CS_Phi->at(0) * t->MuMuGamma_CS_Phi->at(0));
	  // AbsPhi = Pr;
	  double dY = (jpsi.DiLept_Y - photon.Photon_Eta);
	  //Declare the variables
	  double dPhi = (jpsi.DiLept_Phi - photon.Photon_Phi);
	  while (dPhi > M_PI)
	    {
	      dPhi -= 2 * M_PI;
	    }
	  while (dPhi < -M_PI)
	    {
	      dPhi += 2 * M_PI;
	    }

	  AbsCosTheta = sqrt(cosTheta_dimu * cosTheta_dimu);
	  AbsdPhi = (sqrt(dPhi * dPhi));
	  DPhi = dPhi;
	  DY = dY;
	  AbsdY = fabs(dY);
	  Phi = (phi_dimu >= 0 ? (phi_dimu - M_PI) : (phi_dimu + M_PI));
	  costheta = cosTheta_dimu;
	  AbsPhi = sqrt(phi_dimu * phi_dimu);
	  EventNumber = jpsi.EventNumber;
	  Lambda = lambda;
	  qTSquared = qTsquared;
	  DiMuonMass = jpsi.DiMuonMass;
	  DiMuonTau = jpsi.DiMuonTau;
	  DiMuonPt = jpsi.DiMuonPt;
	  PhotonPt = photon.PhotonPt;
	  Trigger_HLT_2mu4_bJpsimumu_noL2 = jpsi.Trigger_HLT_2mu4_bJpsimumu_noL2;

	       
	  qxpsi = jpsi.DiMuonPt/1000 + (photon.PhotonPt/1000)*cos(dPhi);
	  qypsi = (photon.PhotonPt/1000)*sin(dPhi);
			
	  qxgamma = photon.PhotonPt/1000 + (jpsi.DiMuonPt/1000)*cos(dPhi);
	  qygamma = (jpsi.DiMuonPt/1000) *sin(dPhi);

	  qxsum = (jpsi.DiMuonPt/1000 + photon.PhotonPt/1000)*(1+cos(dPhi));
	  qysum = (jpsi.DiMuonPt/1000 + photon.PhotonPt/1000)*(sin(dPhi));
			
	  qtA = (jpsi.DiMuonPt/1000) - (photon.PhotonPt/1000);
	  qtB = sqrt((jpsi.DiMuonPt/1000) * (photon.PhotonPt/1000)) * sin(dPhi);


	  JpsiIndex = ijpsi;
	  PhotonIndex = jphoton;

	  double Qr = qTsquared;
	  double Cr = costheta * costheta;
	  double Mr = DiMuonMass;
	  double Tr = DiMuonTau;
	  double Pr = AbsPhi;
	  double Lr = lambda;

	  // //Loop over the various indices and ranges and fill hte created histograms based on that.

	  for (unsigned int iC = 0; iC < CosThetaRange.size(); iC++)
	    {
	      if (Cr <= CosThetaRange.at(iC).first || Cr > CosThetaRange.at(iC).second)
		continue;

	      for (unsigned int iL = 0; iL < LambdaRange.size(); iL++)
		{ //CS loop
		  if (Lr <= LambdaRange.at(iL).first || Lr > LambdaRange.at(iL).second)
		    continue;

		  for (unsigned int iQ = 0; iQ < qTSquaredRange.size(); iQ++)
		    {
		      if (Qr <= qTSquaredRange.at(iQ).first || Qr > qTSquaredRange.at(iQ).second)
			continue;

		      for (unsigned int iP = 0; iP < PhiRange.size(); iP++)
			{
			  if (Pr <= PhiRange.at(iP).first || Pr > PhiRange.at(iP).second)
			    continue;

			  for (unsigned int iM = 0; iM < MassRange.size(); iM++)
			    {
			      if (Mr <= MassRange.at(iM).first || Mr > MassRange.at(iM).second)
				continue;

			      for (unsigned int iT = 0; iT < LifetimeRange.size(); iT++)
				{
				  if (Tr <= LifetimeRange.at(iT).first || Tr > LifetimeRange.at(iT).second)
				    continue;
 
				  
				  ++counters[Form("%i%i", iL, iQ)];

				//  if(ijpsi< allJpsi.size()/2){
	
				
				    trees[Form("%i%i", iL, iQ)]->Fill();
				
				// }
				// else{
	        
					
				// trees[Form("%i%i_2", iL, iQ)]->Fill();
     
				//
				// }
				  //    }
				  //
				} //Closing iT loop
			    }	  //closing iM loop
			}		  //closing iQ loop
		    }			  //Closing iL loop
		}				  //Closing iC loop
	    }
	  //tr->Fill();

	} // jphoton
    }	  // ijpsi
  cout << "\n\n\n\nCounters:" << endl;
  for (auto x: counters) {  //If using more than one tree, change to loop over files
    cout << x.first << " : " << x.second << endl;

    files[x.first]->cd();
    trees[x.first]->Write();
    files[x.first]->Close();

  //   }
  }
  cout << endl;

  cout << "NJpsi:   " << allJpsi.size() << endl;
  cout << "NPhoton: " << allPhoton.size() << endl;

} //Closing brace

bool VatoCS(const TLorentzVector &mupl,
	    const TLorentzVector &mumi,
	    const TLorentzVector &gamma,
	    TVector3 &CSAxis,
	    TVector3 &xAxis,
	    TVector3 &yAxis,
	    double &cosTheta_dimu,
	    double &cosTheta_gamma,
	    double &phi_dimu,
	    double &phi_gamma)
{

  TLorentzVector dimu = mupl + mumi;
  TLorentzVector higgs = dimu + gamma;

  double ProtonMass = 938.272;  // MeV
  double BeamEnergy = 6500000.; // MeV

  // 1)
  TLorentzVector p1, p2;
  p1.SetPxPyPzE(0., 0., BeamEnergy,
		TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass));
  p2.SetPxPyPzE(0., 0., -1 * BeamEnergy,
		TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass));

  // 2)
  TLorentzVector dimu_hf, mupl_hf, mumi_hf, gamma_hf, higgs_hf, p1_hf, p2_hf;

  dimu_hf = dimu;
  mupl_hf = mupl;
  mumi_hf = mumi;
  gamma_hf = gamma;
  higgs_hf = higgs;
  p1_hf = p1;
  p2_hf = p2;

  dimu_hf.Boost(-higgs.BoostVector());
  mupl_hf.Boost(-higgs.BoostVector());
  mumi_hf.Boost(-higgs.BoostVector());
  gamma_hf.Boost(-higgs.BoostVector());
  higgs_hf.Boost(-higgs.BoostVector());
  p1_hf.Boost(-higgs.BoostVector());
  p2_hf.Boost(-higgs.BoostVector());

  // 3)
  CSAxis = (p1_hf.Vect().Unit() - p2_hf.Vect().Unit()).Unit();
  yAxis = (p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()));
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross(CSAxis);
  xAxis = xAxis.Unit();

  //4)

  phi_dimu = atan2((dimu_hf.Vect() * yAxis), (dimu_hf.Vect() * xAxis));
  cosTheta_dimu = dimu_hf.Vect().Dot(CSAxis) / (dimu_hf.Vect().Mag());

  // cross-check
  cosTheta_gamma = gamma_hf.Vect().Dot(CSAxis) / (gamma_hf.Vect().Mag());
  phi_gamma = atan2((gamma_hf.Vect() * yAxis), (gamma_hf.Vect() * xAxis));

  if (fabs(cosTheta_dimu + cosTheta_gamma) > 0.001)
    {
      cout << "VatoCS: cosThetaCS sum, beyond tolerance: " << cosTheta_dimu << " " << cosTheta_gamma << endl;
    }
  if (fabs(sin(phi_dimu - phi_gamma)) > 0.001)
    {
      cout << "VatoCS: phiCS difference, beyond tolerance: " << phi_dimu << " " << phi_gamma << endl;
    }
  return true;
}
