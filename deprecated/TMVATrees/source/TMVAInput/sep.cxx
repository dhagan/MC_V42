#include "data_sim_mix.hxx"

void sample_mixing(int sign_fraction_int = 0){

  TFile sign_file{"./signal_bound-0/split0/signal_00_mu4000_P5000_bound-0.root","READ"}; 
  TFile bckg_file{"./pp_bound-0/split0/pp_00_mu4000_P5000_bound-0.root","READ"}; 

  TTree * sign_tree = (TTree*) sign_file.Get("TreeD");

  TTree * bckg_tree = (TTree*) bckg_file.Get("TreeD");
  
  TFile * sim_data_file = new TFile(Form("./sim_data_bound-0/s%i/sim_data_s%i_00_mu4000_P5000_bound-0.root",sign_fraction_int,sign_fraction_int),"RECREATE"); 
  TTree * sim_data_tree = new TTree{"TreeD","TreeD"};

  TFile * sim_sign_file = new TFile(Form("./sim_signal_bound-0/s%i/sim_signal_s%i_00_mu4000_P5000_bound-0.root",sign_fraction_int,sign_fraction_int),"RECREATE"); 
  TTree * sim_sign_tree = new TTree{"TreeD","TreeD"};

  TFile * sim_bckg_file = new TFile(Form("./sim_pp_bound-0/s%i/sim_pp_s%i_00_mu4000_P5000_bound-0.root",sign_fraction_int,sign_fraction_int),"RECREATE"); 
  TTree * sim_bckg_tree = new TTree{"TreeD","TreeD"};

  float sign_AbsCosTheta,sign_AbsdPhi,sign_AbsPhi,sign_DPhi,sign_DY,sign_Phi,sign_costheta,sign_EventNumber,sign_Lambda,sign_qTSquared,sign_DiMuonMass,
        sign_DiMuonTau,sign_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_DiMuonPt,sign_PhotonPt,sign_AbsdY,sign_qxpsi,sign_qypsi,sign_qxgamma,sign_qygamma,
        sign_qxsum,sign_qysum,sign_qtA,sign_qtB, sign_qtL, sign_qtM;
  
  float bckg_AbsCosTheta,bckg_AbsdPhi,bckg_AbsPhi,bckg_DPhi,bckg_DY,bckg_Phi,bckg_costheta,bckg_EventNumber,bckg_Lambda,bckg_qTSquared,bckg_DiMuonMass,
        bckg_DiMuonTau,bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_DiMuonPt,bckg_PhotonPt,bckg_AbsdY,bckg_qxpsi,bckg_qypsi,bckg_qxgamma,bckg_qygamma,
        bckg_qxsum,bckg_qysum,bckg_qtA,bckg_qtB, bckg_qtL, bckg_qtM;

  float mix_AbsCosTheta,mix_AbsdPhi,mix_AbsPhi,mix_DPhi,mix_DY,mix_Phi,mix_costheta,mix_EventNumber,mix_Lambda,mix_qTSquared,mix_DiMuonMass,
        mix_DiMuonTau,mix_Trigger_HLT_2mu4_bJpsimumu_noL2,mix_DiMuonPt,mix_PhotonPt,mix_AbsdY,mix_qxpsi,mix_qypsi,mix_qxgamma,mix_qygamma,
        mix_qxsum,mix_qysum,mix_qtA,mix_qtB, mix_qtL, mix_qtM;

  float sim_sign_AbsCosTheta,sim_sign_AbsdPhi,sim_sign_AbsPhi,sim_sign_DPhi,sim_sign_DY,sim_sign_Phi,sim_sign_costheta,sim_sign_EventNumber,sim_sign_Lambda,sim_sign_qTSquared,sim_sign_DiMuonMass,
        sim_sign_DiMuonTau,sim_sign_Trigger_HLT_2mu4_bJpsimumu_noL2,sim_sign_DiMuonPt,sim_sign_PhotonPt,sim_sign_AbsdY,sim_sign_qxpsi,sim_sign_qypsi,sim_sign_qxgamma,sim_sign_qygamma,
        sim_sign_qxsum,sim_sign_qysum,sim_sign_qtA,sim_sign_qtB,sim_sign_qtL,sim_sign_qtM;

  float sim_bckg_AbsCosTheta,sim_bckg_AbsdPhi,sim_bckg_AbsPhi,sim_bckg_DPhi,sim_bckg_DY,sim_bckg_Phi,sim_bckg_costheta,sim_bckg_EventNumber,sim_bckg_Lambda,sim_bckg_qTSquared,sim_bckg_DiMuonMass,
        sim_bckg_DiMuonTau,sim_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,sim_bckg_DiMuonPt,sim_bckg_PhotonPt,sim_bckg_AbsdY,sim_bckg_qxpsi,sim_bckg_qypsi,sim_bckg_qxgamma,sim_bckg_qygamma,
        sim_bckg_qxsum,sim_bckg_qysum,sim_bckg_qtA,sim_bckg_qtB,sim_bckg_qtL,sim_bckg_qtM;

  sim_data_tree->Branch("AbsCosTheta",                              &mix_AbsCosTheta,                       "AbsCosTheta/F");
	sim_data_tree->Branch("AbsdPhi",                                  &mix_AbsdPhi,                           "AbsdPhi/F");
	sim_data_tree->Branch("AbsPhi",                                   &mix_AbsPhi,                            "AbsPhi/F");
	sim_data_tree->Branch("DPhi",                                     &mix_DPhi,                              "DPhi/F");
	sim_data_tree->Branch("DY",                                       &mix_DY,                                "DY/F");
	sim_data_tree->Branch("Phi",                                      &mix_Phi,                               "phi/F");
	sim_data_tree->Branch("costheta",                                 &mix_costheta,                          "CosTheta/F");
	sim_data_tree->Branch("EventNumber",                              &mix_EventNumber,                       "EventNumber/F");
	sim_data_tree->Branch("Lambda",                                   &mix_Lambda,                            "Lambda/F");
	sim_data_tree->Branch("qTSquared",                                &mix_qTSquared,                         "qTSquared/F");
	sim_data_tree->Branch("DiMuonMass",                               &mix_DiMuonMass,                        "DiMuonMass/F");
	sim_data_tree->Branch("DiMuonTau",                                &mix_DiMuonTau,                         "DiMuonTau/F");
	sim_data_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &mix_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	sim_data_tree->Branch("DiMuonPt",                                 &mix_DiMuonPt,                          "DiMuonPt/F");
	sim_data_tree->Branch("PhotonPt",                                 &mix_PhotonPt,                          "PhotonPt/F");
	sim_data_tree->Branch("AbsdY",                                    &mix_AbsdY,                             "AbsdY");
	sim_data_tree->Branch("qxpsi",                                    &mix_qxpsi,                             "qxpsi");
	sim_data_tree->Branch("qypsi",                                    &mix_qypsi,                             "qypsi");
	sim_data_tree->Branch("qxgamma",                                  &mix_qxgamma,                           "qxgamma");
	sim_data_tree->Branch("qygamma",                                  &mix_qygamma,                           "qygamma");
	sim_data_tree->Branch("qxsum",                                    &mix_qxsum,                             "qxsum");
	sim_data_tree->Branch("qysum",                                    &mix_qysum,                             "qysum");
	sim_data_tree->Branch("qtA",                                      &mix_qtA,                               "qtA");
	sim_data_tree->Branch("qtB",                                      &mix_qtB,                               "qtB");
  sim_data_tree->Branch("qtL",                                      &mix_qtL,                               "qtL");
	sim_data_tree->Branch("qtM",                                      &mix_qtM,                               "qtM");

  sim_sign_tree->Branch("AbsCosTheta",                              &sim_sign_AbsCosTheta,                       "AbsCosTheta/F");
	sim_sign_tree->Branch("AbsdPhi",                                  &sim_sign_AbsdPhi,                           "AbsdPhi/F");
	sim_sign_tree->Branch("AbsPhi",                                   &sim_sign_AbsPhi,                            "AbsPhi/F");
	sim_sign_tree->Branch("DPhi",                                     &sim_sign_DPhi,                              "DPhi/F");
	sim_sign_tree->Branch("DY",                                       &sim_sign_DY,                                "DY/F");
	sim_sign_tree->Branch("Phi",                                      &sim_sign_Phi,                               "phi/F");
	sim_sign_tree->Branch("costheta",                                 &sim_sign_costheta,                          "CosTheta/F");
	sim_sign_tree->Branch("EventNumber",                              &sim_sign_EventNumber,                       "EventNumber/F");
	sim_sign_tree->Branch("Lambda",                                   &sim_sign_Lambda,                            "Lambda/F");
	sim_sign_tree->Branch("qTSquared",                                &sim_sign_qTSquared,                         "qTSquared/F");
	sim_sign_tree->Branch("DiMuonMass",                               &sim_sign_DiMuonMass,                        "DiMuonMass/F");
	sim_sign_tree->Branch("DiMuonTau",                                &sim_sign_DiMuonTau,                         "DiMuonTau/F");
	sim_sign_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &sim_sign_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	sim_sign_tree->Branch("DiMuonPt",                                 &sim_sign_DiMuonPt,                          "DiMuonPt/F");
	sim_sign_tree->Branch("PhotonPt",                                 &sim_sign_PhotonPt,                          "PhotonPt/F");
	sim_sign_tree->Branch("AbsdY",                                    &sim_sign_AbsdY,                             "AbsdY");
	sim_sign_tree->Branch("qxpsi",                                    &sim_sign_qxpsi,                             "qxpsi");
	sim_sign_tree->Branch("qypsi",                                    &sim_sign_qypsi,                             "qypsi");
	sim_sign_tree->Branch("qxgamma",                                  &sim_sign_qxgamma,                           "qxgamma");
	sim_sign_tree->Branch("qygamma",                                  &sim_sign_qygamma,                           "qygamma");
	sim_sign_tree->Branch("qxsum",                                    &sim_sign_qxsum,                             "qxsum");
	sim_sign_tree->Branch("qysum",                                    &sim_sign_qysum,                             "qysum");
	sim_sign_tree->Branch("qtA",                                      &sim_sign_qtA,                               "qtA");
	sim_sign_tree->Branch("qtB",                                      &sim_sign_qtB,                               "qtB");
  sim_sign_tree->Branch("qtL",                                      &sim_sign_qtL,                               "qtL");
	sim_sign_tree->Branch("qtM",                                      &sim_sign_qtM,                               "qtM");

  sim_bckg_tree->Branch("AbsCosTheta",                              &sim_bckg_AbsCosTheta,                       "AbsCosTheta/F");
	sim_bckg_tree->Branch("AbsdPhi",                                  &sim_bckg_AbsdPhi,                           "AbsdPhi/F");
	sim_bckg_tree->Branch("AbsPhi",                                   &sim_bckg_AbsPhi,                            "AbsPhi/F");
	sim_bckg_tree->Branch("DPhi",                                     &sim_bckg_DPhi,                              "DPhi/F");
	sim_bckg_tree->Branch("DY",                                       &sim_bckg_DY,                                "DY/F");
	sim_bckg_tree->Branch("Phi",                                      &sim_bckg_Phi,                               "phi/F");
	sim_bckg_tree->Branch("costheta",                                 &sim_bckg_costheta,                          "CosTheta/F");
	sim_bckg_tree->Branch("EventNumber",                              &sim_bckg_EventNumber,                       "EventNumber/F");
	sim_bckg_tree->Branch("Lambda",                                   &sim_bckg_Lambda,                            "Lambda/F");
	sim_bckg_tree->Branch("qTSquared",                                &sim_bckg_qTSquared,                         "qTSquared/F");
	sim_bckg_tree->Branch("DiMuonMass",                               &sim_bckg_DiMuonMass,                        "DiMuonMass/F");
	sim_bckg_tree->Branch("DiMuonTau",                                &sim_bckg_DiMuonTau,                         "DiMuonTau/F");
	sim_bckg_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2",          &sim_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,   "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	sim_bckg_tree->Branch("DiMuonPt",                                 &sim_bckg_DiMuonPt,                          "DiMuonPt/F");
	sim_bckg_tree->Branch("PhotonPt",                                 &sim_bckg_PhotonPt,                          "PhotonPt/F");
	sim_bckg_tree->Branch("AbsdY",                                    &sim_bckg_AbsdY,                             "AbsdY");
	sim_bckg_tree->Branch("qxpsi",                                    &sim_bckg_qxpsi,                             "qxpsi");
	sim_bckg_tree->Branch("qypsi",                                    &sim_bckg_qypsi,                             "qypsi");
	sim_bckg_tree->Branch("qxgamma",                                  &sim_bckg_qxgamma,                           "qxgamma");
	sim_bckg_tree->Branch("qygamma",                                  &sim_bckg_qygamma,                           "qygamma");
	sim_bckg_tree->Branch("qxsum",                                    &sim_bckg_qxsum,                             "qxsum");
	sim_bckg_tree->Branch("qysum",                                    &sim_bckg_qysum,                             "qysum");
	sim_bckg_tree->Branch("qtA",                                      &sim_bckg_qtA,                               "qtA");
	sim_bckg_tree->Branch("qtB",                                      &sim_bckg_qtB,                               "qtB");
  sim_bckg_tree->Branch("qtL",                                      &sim_bckg_qtL,                               "qtL");
	sim_bckg_tree->Branch("qtM",                                      &sim_bckg_qtM,                               "qtM");

  
  sign_tree->SetBranchAddress("AbsCosTheta", &sign_AbsCosTheta);
	sign_tree->SetBranchAddress("AbsdPhi", &sign_AbsdPhi);
	sign_tree->SetBranchAddress("AbsPhi", &sign_AbsPhi);
	sign_tree->SetBranchAddress("DPhi", &sign_DPhi);
	sign_tree->SetBranchAddress("DY", &sign_DY);
	sign_tree->SetBranchAddress("Phi", &sign_Phi);
	sign_tree->SetBranchAddress("costheta", &sign_costheta);
	sign_tree->SetBranchAddress("EventNumber", &sign_EventNumber);
	sign_tree->SetBranchAddress("Lambda", &sign_Lambda);
	sign_tree->SetBranchAddress("qTSquared", &sign_qTSquared);
	sign_tree->SetBranchAddress("DiMuonMass", &sign_DiMuonMass);
	sign_tree->SetBranchAddress("DiMuonTau", &sign_DiMuonTau);
	sign_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",&sign_Trigger_HLT_2mu4_bJpsimumu_noL2);
	sign_tree->SetBranchAddress("DiMuonPt", &sign_DiMuonPt);
	sign_tree->SetBranchAddress("PhotonPt", &sign_PhotonPt);
	sign_tree->SetBranchAddress("AbsdY", &sign_AbsdY);
	sign_tree->SetBranchAddress("qxpsi", &sign_qxpsi);
	sign_tree->SetBranchAddress("qypsi", &sign_qypsi);
	sign_tree->SetBranchAddress("qxgamma", &sign_qxgamma);
	sign_tree->SetBranchAddress("qygamma", &sign_qygamma);
	sign_tree->SetBranchAddress("qxsum",  &sign_qxsum);
	sign_tree->SetBranchAddress("qysum",  &sign_qysum);
	sign_tree->SetBranchAddress("qtA",  &sign_qtA);
	sign_tree->SetBranchAddress("qtB",  &sign_qtB);
  sign_tree->SetBranchAddress("qtL",  &sign_qtL);
	sign_tree->SetBranchAddress("qtM",  &sign_qtM);

  bckg_tree->SetBranchAddress("AbsCosTheta", &bckg_AbsCosTheta);
	bckg_tree->SetBranchAddress("AbsdPhi", &bckg_AbsdPhi);
	bckg_tree->SetBranchAddress("AbsPhi", &bckg_AbsPhi);
	bckg_tree->SetBranchAddress("DPhi", &bckg_DPhi);
	bckg_tree->SetBranchAddress("DY", &bckg_DY);
	bckg_tree->SetBranchAddress("Phi", &bckg_Phi);
	bckg_tree->SetBranchAddress("costheta", &bckg_costheta);
	bckg_tree->SetBranchAddress("EventNumber", &bckg_EventNumber);
	bckg_tree->SetBranchAddress("Lambda", &bckg_Lambda);
	bckg_tree->SetBranchAddress("qTSquared", &bckg_qTSquared);
	bckg_tree->SetBranchAddress("DiMuonMass", &bckg_DiMuonMass);
	bckg_tree->SetBranchAddress("DiMuonTau", &bckg_DiMuonTau);
	bckg_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",&bckg_Trigger_HLT_2mu4_bJpsimumu_noL2);
	bckg_tree->SetBranchAddress("DiMuonPt", &bckg_DiMuonPt);
	bckg_tree->SetBranchAddress("PhotonPt", &bckg_PhotonPt);
	bckg_tree->SetBranchAddress("AbsdY", &bckg_AbsdY);
	bckg_tree->SetBranchAddress("qxpsi", &bckg_qxpsi);
	bckg_tree->SetBranchAddress("qypsi", &bckg_qypsi);
	bckg_tree->SetBranchAddress("qxgamma", &bckg_qxgamma);
	bckg_tree->SetBranchAddress("qygamma", &bckg_qygamma);
	bckg_tree->SetBranchAddress("qxsum",  &bckg_qxsum);
	bckg_tree->SetBranchAddress("qysum",  &bckg_qysum);
	bckg_tree->SetBranchAddress("qtA",  &bckg_qtA);
	bckg_tree->SetBranchAddress("qtB",  &bckg_qtB);
  bckg_tree->SetBranchAddress("qtL",  &bckg_qtL);
	bckg_tree->SetBranchAddress("qtM",  &bckg_qtM);

  int size = 20000;
  double sign_fraction = sign_fraction_int / 100.0;
  double sign_size = size * (sign_fraction);
  double bckg_size = size * (1.0-sign_fraction);

  std::vector<int> sign_ins_idx, bckg_ins_idx;

  int sign_inserts,bckg_inserts; 
  int original_sign_entries = sign_tree->GetEntries();
  int original_bckg_entries = bckg_tree->GetEntries();

  std::cout << "mixing samples to simulate data" << std::endl;
  std::cout << "events in mixed sample - " << size << std::endl;
  std::cout << "signal fraction - " << sign_fraction << std::endl;
  std::cout << "signal events - " << sign_size << std::endl;
  std::cout << "background fraction - " << (1.0-sign_fraction) << std::endl;
  std::cout << "background events - " << bckg_size << std::endl;


  while (sign_inserts < sign_size){
    
    int rand_event = rand() % original_sign_entries;  
    if (std::find(sign_ins_idx.begin(),sign_ins_idx.end(),rand_event) != sign_ins_idx.end()) continue;
    sign_ins_idx.push_back(rand_event);

    sign_tree->GetEntry(rand_event);
    mix_AbsCosTheta                             = sign_AbsCosTheta;
    mix_AbsdPhi                                 = sign_AbsdPhi;
    mix_AbsPhi                                  = sign_AbsPhi;
    mix_DPhi                                    = sign_DPhi;
    mix_DY                                      = sign_DY;
    mix_Phi                                     = sign_Phi;
    mix_costheta                                = sign_costheta;
    mix_EventNumber                             = sign_EventNumber;
    mix_Lambda                                  = sign_Lambda;
    mix_qTSquared                               = sign_qTSquared;
    mix_DiMuonMass                              = sign_DiMuonMass;
    mix_DiMuonTau                               = sign_DiMuonTau;
    mix_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
    mix_DiMuonPt                                = sign_DiMuonPt;
    mix_PhotonPt                                = sign_PhotonPt;
    mix_AbsdY                                   = sign_AbsdY;
    mix_qxpsi                                   = sign_qxpsi;
    mix_qypsi                                   = sign_qypsi;
    mix_qxgamma                                 = sign_qxgamma;
    mix_qygamma                                 = sign_qygamma;
    mix_qxsum                                   = sign_qxsum;
    mix_qysum                                   = sign_qysum;
    mix_qtA                                     = sign_qtA;
    mix_qtB                                     = sign_qtB;
    mix_qtL                                     = sign_qtL;
    mix_qtM                                     = sign_qtM;
    sim_data_tree->Fill(); 
    sign_inserts++;
    if (sign_inserts % 1000 == 0) { std::cout << sign_inserts << " signal events" << std::endl; }

  } 
  std::cout << "signal inserts complete" << std::endl;

  while (bckg_inserts < bckg_size){

    int rand_event = rand() % original_bckg_entries;  
    if (std::find(bckg_ins_idx.begin(),bckg_ins_idx.end(),rand_event) != bckg_ins_idx.end()) continue;
    bckg_ins_idx.push_back(rand_event);

    bckg_tree->GetEntry(rand_event); 
    mix_AbsCosTheta                             = bckg_AbsCosTheta;
    mix_AbsdPhi                                 = bckg_AbsdPhi;
    mix_AbsPhi                                  = bckg_AbsPhi;
    mix_DPhi                                    = bckg_DPhi;
    mix_DY                                      = bckg_DY;
    mix_Phi                                     = bckg_Phi;
    mix_costheta                                = bckg_costheta;
    mix_EventNumber                             = bckg_EventNumber;
    mix_Lambda                                  = bckg_Lambda;
    mix_qTSquared                               = bckg_qTSquared;
    mix_DiMuonMass                              = bckg_DiMuonMass;
    mix_DiMuonTau                               = bckg_DiMuonTau;
    mix_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
    mix_DiMuonPt                                = bckg_DiMuonPt;
    mix_PhotonPt                                = bckg_PhotonPt;
    mix_AbsdY                                   = bckg_AbsdY;
    mix_qxpsi                                   = bckg_qxpsi;
    mix_qypsi                                   = bckg_qypsi;
    mix_qxgamma                                 = bckg_qxgamma;
    mix_qygamma                                 = bckg_qygamma;
    mix_qxsum                                   = bckg_qxsum;
    mix_qysum                                   = bckg_qysum;
    mix_qtA                                     = bckg_qtA;
    mix_qtB                                     = bckg_qtB;
    mix_qtL                                     = bckg_qtM;
    sim_data_tree->Fill();
    bckg_inserts++;
    if (bckg_inserts % 1000 == 0) { std::cout << bckg_inserts << " background events" << std::endl; }
      
  }
  std::cout << "background inserts complete" << std::endl;

  for (int sign_idx = 0; sign_idx < sign_tree->GetEntries(); sign_idx++){

    if (std::find(sign_ins_idx.begin(),sign_ins_idx.end(),sign_idx) != sign_ins_idx.end()) continue;
    sign_tree->GetEntry(sign_idx);
    sim_sign_AbsCosTheta                             = sign_AbsCosTheta;
    sim_sign_AbsdPhi                                 = sign_AbsdPhi;
    sim_sign_AbsPhi                                  = sign_AbsPhi;
    sim_sign_DPhi                                    = sign_DPhi;
    sim_sign_DY                                      = sign_DY;
    sim_sign_Phi                                     = sign_Phi;
    sim_sign_costheta                                = sign_costheta;
    sim_sign_EventNumber                             = sign_EventNumber;
    sim_sign_Lambda                                  = sign_Lambda;
    sim_sign_qTSquared                               = sign_qTSquared;
    sim_sign_DiMuonMass                              = sign_DiMuonMass;
    sim_sign_DiMuonTau                               = sign_DiMuonTau;
    sim_sign_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
    sim_sign_DiMuonPt                                = sign_DiMuonPt;
    sim_sign_PhotonPt                                = sign_PhotonPt;
    sim_sign_AbsdY                                   = sign_AbsdY;
    sim_sign_qxpsi                                   = sign_qxpsi;
    sim_sign_qypsi                                   = sign_qypsi;
    sim_sign_qxgamma                                 = sign_qxgamma;
    sim_sign_qygamma                                 = sign_qygamma;
    sim_sign_qxsum                                   = sign_qxsum;
    sim_sign_qysum                                   = sign_qysum;
    sim_sign_qtA                                     = sign_qtA;
    sim_sign_qtB                                     = sign_qtB;
    sim_sign_qtL                                     = sign_qtM;
    sim_sign_tree->Fill();

  }

  for (int bckg_idx = 0; bckg_idx < bckg_tree->GetEntries(); bckg_idx++){

    if (std::find(bckg_ins_idx.begin(),bckg_ins_idx.end(),bckg_idx) != bckg_ins_idx.end()) continue;
    bckg_tree->GetEntry(bckg_idx);
    sim_bckg_AbsCosTheta                             = bckg_AbsCosTheta;
    sim_bckg_AbsdPhi                                 = bckg_AbsdPhi;
    sim_bckg_AbsPhi                                  = bckg_AbsPhi;
    sim_bckg_DPhi                                    = bckg_DPhi;
    sim_bckg_DY                                      = bckg_DY;
    sim_bckg_Phi                                     = bckg_Phi;
    sim_bckg_costheta                                = bckg_costheta;
    sim_bckg_EventNumber                             = bckg_EventNumber;
    sim_bckg_Lambda                                  = bckg_Lambda;
    sim_bckg_qTSquared                               = bckg_qTSquared;
    sim_bckg_DiMuonMass                              = bckg_DiMuonMass;
    sim_bckg_DiMuonTau                               = bckg_DiMuonTau;
    sim_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
    sim_bckg_DiMuonPt                                = bckg_DiMuonPt;
    sim_bckg_PhotonPt                                = bckg_PhotonPt;
    sim_bckg_AbsdY                                   = bckg_AbsdY;
    sim_bckg_qxpsi                                   = bckg_qxpsi;
    sim_bckg_qypsi                                   = bckg_qypsi;
    sim_bckg_qxgamma                                 = bckg_qxgamma;
    sim_bckg_qygamma                                 = bckg_qygamma;
    sim_bckg_qxsum                                   = bckg_qxsum;
    sim_bckg_qysum                                   = bckg_qysum;
    sim_bckg_qtA                                     = bckg_qtA;
    sim_bckg_qtB                                     = bckg_qtB;
    sim_bckg_qtL                                     = bckg_qtM;
    sim_bckg_tree->Fill();

  }

  sim_data_file->cd();
  sim_data_tree->Write();
  sim_data_file->Close();

  sim_sign_file->cd();
  sim_sign_tree->Write();
  sim_sign_file->Close();

  sim_bckg_file->cd();
  sim_bckg_tree->Write();
  sim_bckg_file->Close();

  std::cout << "file written, mixing finished" << std::endl;
  std::cout << " " << std::endl;
  
}

int main(int argc, char * argv[]){
    sample_mixing(atoi(argv[1]));
}
