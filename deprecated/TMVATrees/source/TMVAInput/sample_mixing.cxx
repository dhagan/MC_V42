#include "sample_mixing.hxx"

int even_entry_gen(){
  static int i{-2};
  return i+=2; 
}

int odd_entry_gen(){
  static int i{-1};
  return i+=2; 
}


void sample_mixing(int mode, int split_type = 0){

  std::cout << mode << std::endl; 
  std::string output_mode;
  if (mode == 0){
    output_mode = "both";
  } else if (mode == 1){
    output_mode = "even";
  } else if (mode == 2){
    output_mode = "odd";
  }






  TFile sign_file{"./signal_bound-0/signal_00_mu4000_P5000_bound-0.root","READ"}; 
  TFile bckg_file{"./pp_bound-0/pp_00_mu4000_P5000_bound-0.root","READ"}; 
  TTree * sign_tree = (TTree*) sign_file.Get("TreeD");
  TTree * bckg_tree = (TTree*) bckg_file.Get("TreeD");
  
  TFile * mini_sign_file = new TFile(Form("./mini-sign_bound-0/%s/split-%i/mini-sign_bound-0_events-%s_split-%i.root",output_mode.c_str(),split_type,output_mode.c_str(),split_type),"RECREATE"); 
  TTree * mini_sign_tree = new TTree{"mini_sign_tree","mini_sign_tree"};

  TFile * mini_bckg_file = new TFile(Form("./mini-bckg_bound-0/%s/split-%i/mini-bckg_bound-0_events-%s_split-%i.root",output_mode.c_str(),split_type,output_mode.c_str(),split_type),"RECREATE"); 
  TTree * mini_bckg_tree = new TTree{"mini_bckg_tree","mini_bckg_tree"};

  TFile * alt_bckg_file = new TFile(Form("./alt-bckg_bound-0/alt-bckg_bound-0_events_split-%i.root",split_type),"RECREATE"); 
  TTree * alt_bckg_tree = new TTree{"alt_bckg_tree","alt_bckg_tree"};

  TFile * alt_sign_file = new TFile(Form("./alt-sign_bound-0/alt-sign_bound-0_events_split-%i.root",split_type),"RECREATE"); 
  TTree * alt_sign_tree = new TTree{"alt_sign_tree","alt_sign_tree"};

  TFile * mix_file = new TFile(Form("./sign-bckg_mix_bound-0/%s/split-%i/sign-bckg_mix_bound-0_events-%s_split-%i.root",output_mode.c_str(),split_type,output_mode.c_str(),split_type),"RECREATE"); 
  TTree * mix_tree = new TTree{"mix_tree","mix_tree"};


  float mix_AbsCosTheta,mix_AbsdPhi,mix_AbsPhi,mix_DPhi,mix_DY,mix_Phi,mix_costheta,mix_EventNumber,mix_Lambda,mix_qTSquared,mix_DiMuonMass,
        mix_DiMuonTau,mix_Trigger_HLT_2mu4_bJpsimumu_noL2,mix_DiMuonPt,mix_PhotonPt,mix_AbsdY,mix_qxpsi,mix_qypsi,mix_qxgamma,mix_qygamma,
        mix_qxsum,mix_qysum,mix_qtA,mix_qtB, mix_qtL, mix_qtM;

  float mini_sign_AbsCosTheta,mini_sign_AbsdPhi,mini_sign_AbsPhi,mini_sign_DPhi,mini_sign_DY,mini_sign_Phi,mini_sign_costheta,mini_sign_EventNumber,mini_sign_Lambda,mini_sign_qTSquared,mini_sign_DiMuonMass,
        mini_sign_DiMuonTau,mini_sign_Trigger_HLT_2mu4_bJpsimumu_noL2,mini_sign_DiMuonPt,mini_sign_PhotonPt,mini_sign_AbsdY,mini_sign_qxpsi,mini_sign_qypsi,mini_sign_qxgamma,mini_sign_qygamma,
        mini_sign_qxsum,mini_sign_qysum,mini_sign_qtA,mini_sign_qtB;

  float mini_bckg_AbsCosTheta,mini_bckg_AbsdPhi,mini_bckg_AbsPhi,mini_bckg_DPhi,mini_bckg_DY,mini_bckg_Phi,mini_bckg_costheta,mini_bckg_EventNumber,mini_bckg_Lambda,mini_bckg_qTSquared,mini_bckg_DiMuonMass,
        mini_bckg_DiMuonTau,mini_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,mini_bckg_DiMuonPt,mini_bckg_PhotonPt,mini_bckg_AbsdY,mini_bckg_qxpsi,mini_bckg_qypsi,mini_bckg_qxgamma,mini_bckg_qygamma,
        mini_bckg_qxsum,mini_bckg_qysum,mini_bckg_qtA,mini_bckg_qtB;
  
  float alt_bckg_AbsCosTheta,alt_bckg_AbsdPhi,alt_bckg_AbsPhi,alt_bckg_DPhi,alt_bckg_DY,alt_bckg_Phi,alt_bckg_costheta,alt_bckg_EventNumber,alt_bckg_Lambda,alt_bckg_qTSquared,alt_bckg_DiMuonMass,
        alt_bckg_DiMuonTau,alt_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,alt_bckg_DiMuonPt,alt_bckg_PhotonPt,alt_bckg_AbsdY,alt_bckg_qxpsi,alt_bckg_qypsi,alt_bckg_qxgamma,alt_bckg_qygamma,
        alt_bckg_qxsum,alt_bckg_qysum,alt_bckg_qtA,alt_bckg_qtB, alt_bckg_qtL, alt_bckg_qtM;

  float alt_sign_AbsCosTheta,alt_sign_AbsdPhi,alt_sign_AbsPhi,alt_sign_DPhi,alt_sign_DY,alt_sign_Phi,alt_sign_costheta,alt_sign_EventNumber,alt_sign_Lambda,alt_sign_qTSquared,alt_sign_DiMuonMass,
        alt_sign_DiMuonTau,alt_sign_Trigger_HLT_2mu4_bJpsimumu_noL2,alt_sign_DiMuonPt,alt_sign_PhotonPt,alt_sign_AbsdY,alt_sign_qxpsi,alt_sign_qypsi,alt_sign_qxgamma,alt_sign_qygamma,
        alt_sign_qxsum,alt_sign_qysum,alt_sign_qtA,alt_sign_qtB, alt_sign_qtL, alt_sign_qtM;

  float sign_AbsCosTheta,sign_AbsdPhi,sign_AbsPhi,sign_DPhi,sign_DY,sign_Phi,sign_costheta,sign_EventNumber,sign_Lambda,sign_qTSquared,sign_DiMuonMass,
        sign_DiMuonTau,sign_Trigger_HLT_2mu4_bJpsimumu_noL2,sign_DiMuonPt,sign_PhotonPt,sign_AbsdY,sign_qxpsi,sign_qypsi,sign_qxgamma,sign_qygamma,
        sign_qxsum,sign_qysum,sign_qtA,sign_qtB, sign_qtL, sign_qtM;
  
  float bckg_AbsCosTheta,bckg_AbsdPhi,bckg_AbsPhi,bckg_DPhi,bckg_DY,bckg_Phi,bckg_costheta,bckg_EventNumber,bckg_Lambda,bckg_qTSquared,bckg_DiMuonMass,
        bckg_DiMuonTau,bckg_Trigger_HLT_2mu4_bJpsimumu_noL2,bckg_DiMuonPt,bckg_PhotonPt,bckg_AbsdY,bckg_qxpsi,bckg_qypsi,bckg_qxgamma,bckg_qygamma,
        bckg_qxsum,bckg_qysum,bckg_qtA,bckg_qtB, bckg_qtL, bckg_qtM;

  mix_tree->Branch("AbsCosTheta", &mix_AbsCosTheta, "AbsCosTheta/F");
	mix_tree->Branch("AbsdPhi", &mix_AbsdPhi, "AbsdPhi/F");
	mix_tree->Branch("AbsPhi", &mix_AbsPhi, "AbsPhi/F");
	mix_tree->Branch("DPhi", &mix_DPhi, "DPhi/F");
	mix_tree->Branch("DY", &mix_DY, "DY/F");
	mix_tree->Branch("Phi", &mix_Phi, "phi/F");
	mix_tree->Branch("costheta", &mix_costheta, "CosTheta/F");
	mix_tree->Branch("EventNumber", &mix_EventNumber, "EventNumber/F");
	mix_tree->Branch("Lambda", &mix_Lambda, "Lambda/F");
	mix_tree->Branch("qTSquared", &mix_qTSquared, "qTSquared/F");
	mix_tree->Branch("DiMuonMass", &mix_DiMuonMass, "DiMuonMass/F");
	mix_tree->Branch("DiMuonTau", &mix_DiMuonTau, "DiMuonTau/F");
	mix_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &mix_Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	mix_tree->Branch("DiMuonPt", &mix_DiMuonPt, "DiMuonPt/F");
	mix_tree->Branch("PhotonPt", &mix_PhotonPt, "PhotonPt/F");
	mix_tree->Branch("AbsdY", &mix_AbsdY, "AbsdY");
	mix_tree->Branch("qxpsi", &mix_qxpsi, "qxpsi");
	mix_tree->Branch("qypsi", &mix_qypsi, "qypsi");
	mix_tree->Branch("qxgamma", &mix_qxgamma, "qxgamma");
	mix_tree->Branch("qygamma", &mix_qygamma, "qygamma");
	mix_tree->Branch("qxsum",  &mix_qxsum, "qxsum");
	mix_tree->Branch("qysum",  &mix_qysum, "qysum");
	mix_tree->Branch("qtA",  &mix_qtA, "qtA");
	mix_tree->Branch("qtB",  &mix_qtB, "qtB");
  mix_tree->Branch("qtL",  &mix_qtL, "qtL");
	mix_tree->Branch("qtM",  &mix_qtM, "qtM");

 
  mini_sign_tree->Branch("AbsCosTheta", &mini_sign_AbsCosTheta);
	mini_sign_tree->Branch("AbsdPhi", &mini_sign_AbsdPhi);
	mini_sign_tree->Branch("AbsPhi", &mini_sign_AbsPhi);
	mini_sign_tree->Branch("DPhi", &mini_sign_DPhi, "DPhi/F");
	mini_sign_tree->Branch("DY", &mini_sign_DY, "DY/F");
	mini_sign_tree->Branch("Phi", &mini_sign_Phi, "phi/F");
	mini_sign_tree->Branch("costheta", &mini_sign_costheta, "CosTheta/F");
	mini_sign_tree->Branch("EventNumber", &mini_sign_EventNumber, "EventNumber/F");
	mini_sign_tree->Branch("Lambda", &mini_sign_Lambda, "Lambda/F");
	mini_sign_tree->Branch("qTSquared", &mini_sign_qTSquared, "qTSquared/F");
	mini_sign_tree->Branch("DiMuonMass", &mini_sign_DiMuonMass, "DiMuonMass/F");
	mini_sign_tree->Branch("DiMuonTau", &mini_sign_DiMuonTau, "DiMuonTau/F");
	mini_sign_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &mini_sign_Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	mini_sign_tree->Branch("DiMuonPt", &mini_sign_DiMuonPt, "DiMuonPt/F");
	mini_sign_tree->Branch("PhotonPt", &mini_sign_PhotonPt, "PhotonPt/F");
	mini_sign_tree->Branch("AbsdY", &mini_sign_AbsdY, "AbsdY");
	mini_sign_tree->Branch("qxpsi", &mini_sign_qxpsi, "qxpsi");
	mini_sign_tree->Branch("qypsi", &mini_sign_qypsi, "qypsi");
	mini_sign_tree->Branch("qxgamma", &mini_sign_qxgamma, "qxgamma");
	mini_sign_tree->Branch("qygamma", &mini_sign_qygamma, "qygamma");
	mini_sign_tree->Branch("qxsum",  &mini_sign_qxsum, "qxsum");
	mini_sign_tree->Branch("qysum",  &mini_sign_qysum, "qysum");
	mini_sign_tree->Branch("qtA",  &mini_sign_qtA, "qtA");
	mini_sign_tree->Branch("qtB",  &mini_sign_qtB, "qtB");

  mini_bckg_tree->Branch("AbsCosTheta", &mini_bckg_AbsCosTheta, "AbsCosTheta/F");
	mini_bckg_tree->Branch("AbsdPhi", &mini_bckg_AbsdPhi, "AbsdPhi/F");
	mini_bckg_tree->Branch("AbsPhi", &mini_bckg_AbsPhi, "AbsPhi/F");
	mini_bckg_tree->Branch("DPhi", &mini_bckg_DPhi, "DPhi/F");
	mini_bckg_tree->Branch("DY", &mini_bckg_DY, "DY/F");
	mini_bckg_tree->Branch("Phi", &mini_bckg_Phi, "phi/F");
	mini_bckg_tree->Branch("costheta", &mini_bckg_costheta, "CosTheta/F");
	mini_bckg_tree->Branch("EventNumber", &mini_bckg_EventNumber, "EventNumber/F");
	mini_bckg_tree->Branch("Lambda", &mini_bckg_Lambda, "Lambda/F");
	mini_bckg_tree->Branch("qTSquared", &mini_bckg_qTSquared, "qTSquared/F");
	mini_bckg_tree->Branch("DiMuonMass", &mini_bckg_DiMuonMass, "DiMuonMass/F");
	mini_bckg_tree->Branch("DiMuonTau", &mini_bckg_DiMuonTau, "DiMuonTau/F");
	mini_bckg_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &mini_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	mini_bckg_tree->Branch("DiMuonPt", &mini_bckg_DiMuonPt, "DiMuonPt/F");
	mini_bckg_tree->Branch("PhotonPt", &mini_bckg_PhotonPt, "PhotonPt/F");
	mini_bckg_tree->Branch("AbsdY", &mini_bckg_AbsdY, "AbsdY");
	mini_bckg_tree->Branch("qxpsi", &mini_bckg_qxpsi, "qxpsi");
	mini_bckg_tree->Branch("qypsi", &mini_bckg_qypsi, "qypsi");
	mini_bckg_tree->Branch("qxgamma", &mini_bckg_qxgamma, "qxgamma");
	mini_bckg_tree->Branch("qygamma", &mini_bckg_qygamma, "qygamma");
	mini_bckg_tree->Branch("qxsum",  &mini_bckg_qxsum, "qxsum");
	mini_bckg_tree->Branch("qysum",  &mini_bckg_qysum, "qysum");
	mini_bckg_tree->Branch("qtA",  &mini_bckg_qtA, "qtA");
	mini_bckg_tree->Branch("qtB",  &mini_bckg_qtB, "qtB");

  alt_bckg_tree->Branch("AbsCosTheta", &alt_bckg_AbsCosTheta, "AbsCosTheta/F");
	alt_bckg_tree->Branch("AbsdPhi", &alt_bckg_AbsdPhi, "AbsdPhi/F");
	alt_bckg_tree->Branch("AbsPhi", &alt_bckg_AbsPhi, "AbsPhi/F");
	alt_bckg_tree->Branch("DPhi", &alt_bckg_DPhi, "DPhi/F");
	alt_bckg_tree->Branch("DY", &alt_bckg_DY, "DY/F");
	alt_bckg_tree->Branch("Phi", &alt_bckg_Phi, "phi/F");
	alt_bckg_tree->Branch("costheta", &alt_bckg_costheta, "CosTheta/F");
	alt_bckg_tree->Branch("EventNumber", &alt_bckg_EventNumber, "EventNumber/F");
	alt_bckg_tree->Branch("Lambda", &alt_bckg_Lambda, "Lambda/F");
	alt_bckg_tree->Branch("qTSquared", &alt_bckg_qTSquared, "qTSquared/F");
	alt_bckg_tree->Branch("DiMuonMass", &alt_bckg_DiMuonMass, "DiMuonMass/F");
	alt_bckg_tree->Branch("DiMuonTau", &alt_bckg_DiMuonTau, "DiMuonTau/F");
	alt_bckg_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &alt_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	alt_bckg_tree->Branch("DiMuonPt", &alt_bckg_DiMuonPt, "DiMuonPt/F");
	alt_bckg_tree->Branch("PhotonPt", &alt_bckg_PhotonPt, "PhotonPt/F");
	alt_bckg_tree->Branch("AbsdY", &alt_bckg_AbsdY, "AbsdY");
	alt_bckg_tree->Branch("qxpsi", &alt_bckg_qxpsi, "qxpsi");
	alt_bckg_tree->Branch("qypsi", &alt_bckg_qypsi, "qypsi");
	alt_bckg_tree->Branch("qxgamma", &alt_bckg_qxgamma, "qxgamma");
	alt_bckg_tree->Branch("qygamma", &alt_bckg_qygamma, "qygamma");
	alt_bckg_tree->Branch("qxsum",  &alt_bckg_qxsum, "qxsum");
	alt_bckg_tree->Branch("qysum",  &alt_bckg_qysum, "qysum");
	alt_bckg_tree->Branch("qtA",  &alt_bckg_qtA, "qtA");
	alt_bckg_tree->Branch("qtB",  &alt_bckg_qtB, "qtB");
  alt_bckg_tree->Branch("qtA",  &alt_bckg_qtA, "qtL");
	alt_bckg_tree->Branch("qtB",  &alt_bckg_qtB, "qtM");

  
  alt_sign_tree->Branch("AbsCosTheta", &alt_sign_AbsCosTheta, "AbsCosTheta/F");
	alt_sign_tree->Branch("AbsdPhi", &alt_sign_AbsdPhi, "AbsdPhi/F");
	alt_sign_tree->Branch("AbsPhi", &alt_sign_AbsPhi, "AbsPhi/F");
	alt_sign_tree->Branch("DPhi", &alt_sign_DPhi, "DPhi/F");
	alt_sign_tree->Branch("DY", &alt_sign_DY, "DY/F");
	alt_sign_tree->Branch("Phi", &alt_sign_Phi, "phi/F");
	alt_sign_tree->Branch("costheta", &alt_sign_costheta, "CosTheta/F");
	alt_sign_tree->Branch("EventNumber", &alt_sign_EventNumber, "EventNumber/F");
	alt_sign_tree->Branch("Lambda", &alt_sign_Lambda, "Lambda/F");
	alt_sign_tree->Branch("qTSquared", &alt_sign_qTSquared, "qTSquared/F");
	alt_sign_tree->Branch("DiMuonMass", &alt_sign_DiMuonMass, "DiMuonMass/F");
	alt_sign_tree->Branch("DiMuonTau", &alt_sign_DiMuonTau, "DiMuonTau/F");
	alt_sign_tree->Branch("Trigger_HLT_2mu4_bJpsimumu_noL2", &alt_sign_Trigger_HLT_2mu4_bJpsimumu_noL2, "Trigger_HLT_2mu4_bJpsimumu_noL2/F");
	alt_sign_tree->Branch("DiMuonPt", &alt_sign_DiMuonPt, "DiMuonPt/F");
	alt_sign_tree->Branch("PhotonPt", &alt_sign_PhotonPt, "PhotonPt/F");
	alt_sign_tree->Branch("AbsdY", &alt_sign_AbsdY, "AbsdY");
	alt_sign_tree->Branch("qxpsi", &alt_sign_qxpsi, "qxpsi");
	alt_sign_tree->Branch("qypsi", &alt_sign_qypsi, "qypsi");
	alt_sign_tree->Branch("qxgamma", &alt_sign_qxgamma, "qxgamma");
	alt_sign_tree->Branch("qygamma", &alt_sign_qygamma, "qygamma");
	alt_sign_tree->Branch("qxsum",  &alt_sign_qxsum, "qxsum");
	alt_sign_tree->Branch("qysum",  &alt_sign_qysum, "qysum");
	alt_sign_tree->Branch("qtA",  &alt_sign_qtA, "qtA");
	alt_sign_tree->Branch("qtB",  &alt_sign_qtB, "qtB");
  alt_sign_tree->Branch("qtL",  &alt_sign_qtA, "qtL");
	alt_sign_tree->Branch("qtM",  &alt_sign_qtB, "qtM");


  sign_tree->SetBranchAddress("AbsCosTheta", &sign_AbsCosTheta);
	sign_tree->SetBranchAddress("AbsdPhi", &sign_AbsdPhi);
	sign_tree->SetBranchAddress("AbsPhi", &sign_AbsPhi);
	sign_tree->SetBranchAddress("DPhi", &sign_DPhi);
	sign_tree->SetBranchAddress("DY", &sign_DY);
	sign_tree->SetBranchAddress("Phi", &sign_Phi);
	sign_tree->SetBranchAddress("costheta", &sign_costheta);
	sign_tree->SetBranchAddress("EventNumber", &sign_EventNumber);
	sign_tree->SetBranchAddress("Lambda", &sign_Lambda);
	sign_tree->SetBranchAddress("qTSquared", &sign_qTSquared);
	sign_tree->SetBranchAddress("DiMuonMass", &sign_DiMuonMass);
	sign_tree->SetBranchAddress("DiMuonTau", &sign_DiMuonTau);
	sign_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",&sign_Trigger_HLT_2mu4_bJpsimumu_noL2);
	sign_tree->SetBranchAddress("DiMuonPt", &sign_DiMuonPt);
	sign_tree->SetBranchAddress("PhotonPt", &sign_PhotonPt);
	sign_tree->SetBranchAddress("AbsdY", &sign_AbsdY);
	sign_tree->SetBranchAddress("qxpsi", &sign_qxpsi);
	sign_tree->SetBranchAddress("qypsi", &sign_qypsi);
	sign_tree->SetBranchAddress("qxgamma", &sign_qxgamma);
	sign_tree->SetBranchAddress("qygamma", &sign_qygamma);
	sign_tree->SetBranchAddress("qxsum",  &sign_qxsum);
	sign_tree->SetBranchAddress("qysum",  &sign_qysum);
	sign_tree->SetBranchAddress("qtA",  &sign_qtA);
	sign_tree->SetBranchAddress("qtB",  &sign_qtB);
  sign_tree->SetBranchAddress("qtL",  &sign_qtL);
	sign_tree->SetBranchAddress("qtM",  &sign_qtM);


  bckg_tree->SetBranchAddress("AbsCosTheta", &bckg_AbsCosTheta);
	bckg_tree->SetBranchAddress("AbsdPhi", &bckg_AbsdPhi);
	bckg_tree->SetBranchAddress("AbsPhi", &bckg_AbsPhi);
	bckg_tree->SetBranchAddress("DPhi", &bckg_DPhi);
	bckg_tree->SetBranchAddress("DY", &bckg_DY);
	bckg_tree->SetBranchAddress("Phi", &bckg_Phi);
	bckg_tree->SetBranchAddress("costheta", &bckg_costheta);
	bckg_tree->SetBranchAddress("EventNumber", &bckg_EventNumber);
	bckg_tree->SetBranchAddress("Lambda", &bckg_Lambda);
	bckg_tree->SetBranchAddress("qTSquared", &bckg_qTSquared);
	bckg_tree->SetBranchAddress("DiMuonMass", &bckg_DiMuonMass);
	bckg_tree->SetBranchAddress("DiMuonTau", &bckg_DiMuonTau);
	bckg_tree->SetBranchAddress("Trigger_HLT_2mu4_bJpsimumu_noL2",&bckg_Trigger_HLT_2mu4_bJpsimumu_noL2);
	bckg_tree->SetBranchAddress("DiMuonPt", &bckg_DiMuonPt);
	bckg_tree->SetBranchAddress("PhotonPt", &bckg_PhotonPt);
	bckg_tree->SetBranchAddress("AbsdY", &bckg_AbsdY);
	bckg_tree->SetBranchAddress("qxpsi", &bckg_qxpsi);
	bckg_tree->SetBranchAddress("qypsi", &bckg_qypsi);
	bckg_tree->SetBranchAddress("qxgamma", &bckg_qxgamma);
	bckg_tree->SetBranchAddress("qygamma", &bckg_qygamma);
	bckg_tree->SetBranchAddress("qxsum",  &bckg_qxsum);
	bckg_tree->SetBranchAddress("qysum",  &bckg_qysum);
	bckg_tree->SetBranchAddress("qtA",  &bckg_qtA);
	bckg_tree->SetBranchAddress("qtB",  &bckg_qtB);
  bckg_tree->SetBranchAddress("qtL",  &bckg_qtL);
	bckg_tree->SetBranchAddress("qtM",  &bckg_qtM);


  std::vector<int> entry_vec(10000);
  if (mode == 0){ // first 10000 events
    std::iota(std::begin(entry_vec), std::end(entry_vec),0);
  } else if (mode == 1){ // even events
    std::generate(entry_vec.begin(), entry_vec.end(),even_entry_gen);
  } else if (mode == 2){ // odd events
    std::generate(entry_vec.begin(), entry_vec.end(),odd_entry_gen);
  }

  for (int i = 0; i < 10000; i++){ 
    std::cout << entry_vec[i] << " "; 
  }

  double split_fraction_sign, split_fraction_bckg;
  if (split_type == 0){
    // full
    split_fraction_sign = 1;
    split_fraction_bckg = 1;
  } else if (split_type == 1){
    // 80/20
    split_fraction_bckg = 1;
    split_fraction_sign = 0.25;
  }

  int sign_events = (int) 10000*split_fraction_sign;

  for (int i = 0; i < sign_events; i++){ 

      sign_tree->GetEntry(entry_vec[i]);
      mix_AbsCosTheta                             = sign_AbsCosTheta;
      mix_AbsdPhi                                 = sign_AbsdPhi;
      mix_AbsPhi                                  = sign_AbsPhi;
      mix_DPhi                                    = sign_DPhi;
      mix_DY                                      = sign_DY;
      mix_Phi                                     = sign_Phi;
      mix_costheta                                = sign_costheta;
      mix_EventNumber                             = sign_EventNumber;
      mix_Lambda                                  = sign_Lambda;
      mix_qTSquared                               = sign_qTSquared;
      mix_DiMuonMass                              = sign_DiMuonMass;
      mix_DiMuonTau                               = sign_DiMuonTau;
      mix_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
      mix_DiMuonPt                                = sign_DiMuonPt;
      mix_PhotonPt                                = sign_PhotonPt;
      mix_AbsdY                                   = sign_AbsdY;
      mix_qxpsi                                   = sign_qxpsi;
      mix_qypsi                                   = sign_qypsi;
      mix_qxgamma                                 = sign_qxgamma;
      mix_qygamma                                 = sign_qygamma;
      mix_qxsum                                   = sign_qxsum;
      mix_qysum                                   = sign_qysum;
      mix_qtA                                     = sign_qtA;
      mix_qtB                                     = sign_qtB;
      mix_qtL                                     = sign_qtL;
      mix_qtM                                     = sign_qtM;
      mix_tree->Fill(); 

      mini_sign_AbsCosTheta                             = sign_AbsCosTheta;
      mini_sign_AbsdPhi                                 = sign_AbsdPhi;
      mini_sign_AbsPhi                                  = sign_AbsPhi;
      mini_sign_DPhi                                    = sign_DPhi;
      mini_sign_DY                                      = sign_DY;
      mini_sign_Phi                                     = sign_Phi;
      mini_sign_costheta                                = sign_costheta;
      mini_sign_EventNumber                             = sign_EventNumber;
      mini_sign_Lambda                                  = sign_Lambda;
      mini_sign_qTSquared                               = sign_qTSquared;
      mini_sign_DiMuonMass                              = sign_DiMuonMass;
      mini_sign_DiMuonTau                               = sign_DiMuonTau;
      mini_sign_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
      mini_sign_DiMuonPt                                = sign_DiMuonPt;
      mini_sign_PhotonPt                                = sign_PhotonPt;
      mini_sign_AbsdY                                   = sign_AbsdY;
      mini_sign_qxpsi                                   = sign_qxpsi;
      mini_sign_qypsi                                   = sign_qypsi;
      mini_sign_qxgamma                                 = sign_qxgamma;
      mini_sign_qygamma                                 = sign_qygamma;
      mini_sign_qxsum                                   = sign_qxsum;
      mini_sign_qysum                                   = sign_qysum;
      mini_sign_qtA                                     = sign_qtA;
      mini_sign_qtB                                     = sign_qtB;
      mini_sign_tree->Fill(); 

  } 


  int bckg_events = (int) 10000*split_fraction_bckg;
  for (int i = 0; i < bckg_events; i++){ 

      bckg_tree->GetEntry(entry_vec[i]); 
      mix_AbsCosTheta                             = bckg_AbsCosTheta;
      mix_AbsdPhi                                 = bckg_AbsdPhi;
      mix_AbsPhi                                  = bckg_AbsPhi;
      mix_DPhi                                    = bckg_DPhi;
      mix_DY                                      = bckg_DY;
      mix_Phi                                     = bckg_Phi;
      mix_costheta                                = bckg_costheta;
      mix_EventNumber                             = bckg_EventNumber;
      mix_Lambda                                  = bckg_Lambda;
      mix_qTSquared                               = bckg_qTSquared;
      mix_DiMuonMass                              = bckg_DiMuonMass;
      mix_DiMuonTau                               = bckg_DiMuonTau;
      mix_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
      mix_DiMuonPt                                = bckg_DiMuonPt;
      mix_PhotonPt                                = bckg_PhotonPt;
      mix_AbsdY                                   = bckg_AbsdY;
      mix_qxpsi                                   = bckg_qxpsi;
      mix_qypsi                                   = bckg_qypsi;
      mix_qxgamma                                 = bckg_qxgamma;
      mix_qygamma                                 = bckg_qygamma;
      mix_qxsum                                   = bckg_qxsum;
      mix_qysum                                   = bckg_qysum;
      mix_qtA                                     = bckg_qtA;
      mix_qtB                                     = bckg_qtB;
      mix_qtL                                     = bckg_qtM;
      mix_tree->Fill();

      mini_bckg_AbsCosTheta                             = bckg_AbsCosTheta;
      mini_bckg_AbsdPhi                                 = bckg_AbsdPhi;
      mini_bckg_AbsPhi                                  = bckg_AbsPhi;
      mini_bckg_DPhi                                    = bckg_DPhi;
      mini_bckg_DY                                      = bckg_DY;
      mini_bckg_Phi                                     = bckg_Phi;
      mini_bckg_costheta                                = bckg_costheta;
      mini_bckg_EventNumber                             = bckg_EventNumber;
      mini_bckg_Lambda                                  = bckg_Lambda;
      mini_bckg_qTSquared                               = bckg_qTSquared;
      mini_bckg_DiMuonMass                              = bckg_DiMuonMass;
      mini_bckg_DiMuonTau                               = bckg_DiMuonTau;
      mini_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2         = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
      mini_bckg_DiMuonPt                                = bckg_DiMuonPt;
      mini_bckg_PhotonPt                                = bckg_PhotonPt;
      mini_bckg_AbsdY                                   = bckg_AbsdY;
      mini_bckg_qxpsi                                   = bckg_qxpsi;
      mini_bckg_qypsi                                   = bckg_qypsi;
      mini_bckg_qxgamma                                 = bckg_qxgamma;
      mini_bckg_qygamma                                 = bckg_qygamma;
      mini_bckg_qxsum                                   = bckg_qxsum;
      mini_bckg_qysum                                   = bckg_qysum;
      mini_bckg_qtA                                     = bckg_qtA;
      mini_bckg_qtB                                     = bckg_qtB;
      mini_bckg_tree->Fill();

  }

  mix_file->cd();
  mix_tree->Write();
  mix_file->Close();

  mini_sign_file->cd();
  mini_sign_tree->Write();
  mini_sign_file->Close();

  mini_bckg_file->cd();
  mini_bckg_tree->Write();
  mini_bckg_file->Close();


  for (int entry = 20000; entry < 30000; entry++){

    bckg_tree->GetEntry(entry); 

    alt_bckg_AbsCosTheta                             = bckg_AbsCosTheta;
    alt_bckg_AbsdPhi                                 = bckg_AbsdPhi;
    alt_bckg_AbsPhi                                  = bckg_AbsPhi;
    alt_bckg_DPhi                                    = bckg_DPhi;
    alt_bckg_DY                                      = bckg_DY;
    alt_bckg_Phi                                     = bckg_Phi;
    alt_bckg_costheta                                = bckg_costheta;
    alt_bckg_EventNumber                             = bckg_EventNumber;
    alt_bckg_Lambda                                  = bckg_Lambda;
    alt_bckg_qTSquared                               = bckg_qTSquared;
    alt_bckg_DiMuonMass                              = bckg_DiMuonMass;
    alt_bckg_DiMuonTau                               = bckg_DiMuonTau;
    alt_bckg_Trigger_HLT_2mu4_bJpsimumu_noL2        = bckg_Trigger_HLT_2mu4_bJpsimumu_noL2;
    alt_bckg_DiMuonPt                                = bckg_DiMuonPt;
    alt_bckg_PhotonPt                                = bckg_PhotonPt;
    alt_bckg_AbsdY                                   = bckg_AbsdY;
    alt_bckg_qxpsi                                   = bckg_qxpsi;
    alt_bckg_qypsi                                   = bckg_qypsi;
    alt_bckg_qxgamma                                 = bckg_qxgamma;
    alt_bckg_qygamma                                 = bckg_qygamma;
    alt_bckg_qxsum                                   = bckg_qxsum;
    alt_bckg_qysum                                   = bckg_qysum;
    alt_bckg_qtA                                     = bckg_qtA;
    alt_bckg_qtB                                     = bckg_qtB;
    alt_bckg_qtL                                     = bckg_qtL;
    alt_bckg_qtM                                     = bckg_qtM;
    alt_bckg_tree->Fill();
    
    
    sign_tree->GetEntry(entry);
    alt_sign_AbsCosTheta                             = sign_AbsCosTheta;
    alt_sign_AbsdPhi                                 = sign_AbsdPhi;
    alt_sign_AbsPhi                                  = sign_AbsPhi;
    alt_sign_DPhi                                    = sign_DPhi;
    alt_sign_DY                                      = sign_DY;
    alt_sign_Phi                                     = sign_Phi;
    alt_sign_costheta                                = sign_costheta;
    alt_sign_EventNumber                             = sign_EventNumber;
    alt_sign_Lambda                                  = sign_Lambda;
    alt_sign_qTSquared                               = sign_qTSquared;
    alt_sign_DiMuonMass                              = sign_DiMuonMass;
    alt_sign_DiMuonTau                               = sign_DiMuonTau;
    alt_sign_Trigger_HLT_2mu4_bJpsimumu_noL2         = sign_Trigger_HLT_2mu4_bJpsimumu_noL2;
    alt_sign_DiMuonPt                                = sign_DiMuonPt;
    alt_sign_PhotonPt                                = sign_PhotonPt;
    alt_sign_AbsdY                                   = sign_AbsdY;
    alt_sign_qxpsi                                   = sign_qxpsi;
    alt_sign_qypsi                                   = sign_qypsi;
    alt_sign_qxgamma                                 = sign_qxgamma;
    alt_sign_qygamma                                 = sign_qygamma;
    alt_sign_qxsum                                   = sign_qxsum;
    alt_sign_qysum                                   = sign_qysum;
    alt_sign_qtA                                     = sign_qtA;
    alt_sign_qtB                                     = sign_qtB;
    alt_sign_qtL                                     = sign_qtL;
    alt_sign_qtM                                     = sign_qtM;
    alt_sign_tree->Fill();


  }

  alt_bckg_file->cd();
  alt_bckg_tree->Write();
  alt_bckg_file->Close();

  alt_sign_file->cd();
  alt_sign_tree->Write();
  alt_sign_file->Close();

}

int main(int argc, char * argv[]){
    sample_mixing(atoi(argv[1]),atoi(argv[2]));
}
