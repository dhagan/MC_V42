#!/bin/bash

#Script Shell Variables
unique=$1
executable=${ANA_IP}/bdt/build/bdt
log=${LOG_PATH}/bdt/${unique}.txt
data=${OUT_PATH}/trees/gen_${unique}/data/data_${unique}.root:tree
sign=${OUT_PATH}/trees/gen_${unique}/sign/sign_${unique}.root:tree
bckg=${OUT_PATH}/trees/gen_${unique}/bckg/bckg_${unique}.root:tree
bbbg=${OUT_PATH}/trees/gen_${unique}/bbbg/bbbg_${unique}.root:tree
bdtxml=${OUT_PATH}/bdt/${unique}/tmva_dataloader/weights/tmva_classification_BDT.weights.xml
disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
spec="ActIpX:AvgIpX:AbsdY:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0"
hpar="!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5%:PruneMethod=NoPruning"

touch ${log}
mkdir -p ${OUT_PATH}/bdt/${unique}
mkdir -p ${OUT_PATH}/bdt/${unique}/eval

pushd ${OUT_PATH}/bdt/${unique} >> /dev/null

## train tree
${executable} -s ${sign} -b ${bckg} -d ${disc} -v ${spec} -m "train" -h ${hpar} -u ${unique} 2>&1 | tee ${log}

## eval samples
$executable -s ${data} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "data" 2>&1 | tee -a ${log}
$executable -s ${sign} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "sign" 2>&1 | tee -a ${log}
$executable -s ${bckg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "bckg" 2>&1 | tee -a ${log}
$executable -s ${bbbg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "bbbg" 2>&1 | tee -a ${log}

popd >> /dev/null
