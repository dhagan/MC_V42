#!/bin/bash

aaDPDY_disc="AbsdPhi:AbsdY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
aaDPDY_spec="DPhi:DY:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM"

faDPDY_disc="DPhi:AbsdY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
faDPDY_spec="AbsdPhi:DY:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM"

ffDPDY_disc="DPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
ffDPDY_spec="AbsdPhi:AbsdY:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM"

aaf_disc="AbsdPhi:AbsPhi:costheta:DY:Lambda:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
aaf_spec="DPhi:Phi:AbsCosTheta:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM"

afa_disc="AbsdPhi:Phi:AbsCosTheta:DY:Lambda:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
afa_spec="DPhi:AbsPhi:costheta:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM"

faa_disc="DPhi:AbsPhi:AbsCosTheta:DY:Lambda:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
faa_spec="AbsdPhi:Phi:costheta:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM"

extd_disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qtL:qtM:qTSquared"
extd_spec="DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt"

noqt2Disc_disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB"
noqt2Disc_spec="DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:qTSquared"

noL_disc="AbsdPhi:DY:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
noL_spec="DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:Lambda"

hpar="!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5%:PruneMethod=NoPruning"

aaf="${aaf_disc}#${aaf_spec}&aaf"
afa="${afa_disc}#${afa_spec}&afa"
faa="${faa_disc}#${faa_spec}&faa"
extd="${extd_disc}#${extd_spec}&extd"
noqt2Disc="${noqt2Disc_disc}#${noqt2Disc_spec}&noqt2Disc"
noL="${noL_disc}#${noL_spec}&noL"

aaDPDY="${aaDPDY_disc}#${aaDPDY_spec}&aaDPDY"
afDPDY="${afDPDY_disc}#${afDPDY_spec}&afDPDY"
faDPDY="${faDPDY_disc}#${faDPDY_spec}&faDPDY"
ffDPDY="${ffDPDY_disc}#${ffDPDY_spec}&ffDPDY"

executable=${ANA_IP}/bdt/build/bdt
data=${OUT_PATH}/trees/gen_re449/data/data_re449.root:tree
sign=${OUT_PATH}/trees/gen_re449/sign/sign_re449.root:tree
bckg=${OUT_PATH}/trees/gen_re449/bckg/bckg_re449.root:tree
bbbg=${OUT_PATH}/trees/gen_re449/bbbg/bbbg_re449.root:tree


types=( "data" "sign" "bckg" "bbbg" )

vars=( ${aaf} ${afa} ${faa} ${extd} ${noqt2Disc} ${noL} ${ffDPDY} ${aaDPDY} ${faDPDY} )


for var in ${vars[@]}
do
	disc_spec=${var%&*}

	disc=${disc_spec%#*}
	spec=${disc_spec#*#}
	unique=re449_${var#*&}

	mkdir -p ${OUT_PATH}/bdt/${unique}
	mkdir -p ${OUT_PATH}/bdt/${unique}/eval
	pushd ${OUT_PATH}/bdt/${unique} >> /dev/null

	log=${LOG_PATH}/bdt/${unique}.txt
	bdtxml=${OUT_PATH}/bdt/${unique}/tmva_dataloader/weights/tmva_classification_BDT.weights.xml

	${executable} -s ${sign} -b ${bckg} -d ${disc} -v ${spec} -m "train" -h ${hpar} -u ${unique} 2>&1 | tee ${log}

	${executable} -s ${data} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "data" 2>&1 | tee -a ${log}
	${executable} -s ${sign} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "sign" 2>&1 | tee -a ${log}
	${executable} -s ${bckg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "bckg" 2>&1 | tee -a ${log}
	${executable} -s ${bbbg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "bbbg" 2>&1 | tee -a ${log}

	popd >> /dev/null
done

