#!/bin/bash

#Script Shell Variables
unique=tr445
storage=run_${unique}
##data=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${unique}/data/,TreeD
sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${unique}/truth_sign/,TreeT
##bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${unique}/bckg/,TreeD
##bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${unique}/bbbg/,TreeD
bdtxml=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/dataset_1o2/weights/TMVA_Training.xml

disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
spec="ActIpX:AvgIpX:EventNumber:DeltaZ0:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM"
hpar="!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5%:PruneMethod=NoPruning"

mkdir -p ${storage}
cd ${storage}
mkdir -p eval
mkdir -p Evalpngs
mkdir -p dataset_1o2/weights
bdtmapper=../../../tmvacode/BDT_Trainer/source/BDTMapper

##$bdtmapper -n ${sign} -v ${bckg} -l ${disc} -s ${spec} -t Train -H ${hpar}
##mv dataset_1o2/weights/TMVAClassification_MVACategories_1o2.weights.xml ${bdtxml}
##
#### original data
##$bdtmapper -n ${data} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
##mv Evaluate.root eval/eval_data.root
##
#### original signal
$bdtmapper -n ${sign} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
mv Evaluate.root eval/eval_sign.root
##
#### original pp
##$bdtmapper -n ${bckg} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
##mv Evaluate.root eval/eval_bckg.root
##
#### original bb
##$bdtmapper -n ${bbbg} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
##mv Evaluate.root eval/eval_bbbg.root
