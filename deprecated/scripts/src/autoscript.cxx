#include "autoscript.hxx"

#include <nlohmann/json.hpp>
#include <iostream>

void autoscript(){

  std::string input_script_filepath = std::getenv( "DEFAULT_SCRIPT_JSON" );
  std::ifstream input_file_stream;
  nlohmann::json script_json;
  input_file_stream.open( input_script_filepath + ".json" );
  input_file_stream >> script_json;
  input_file_stream.close();
  std::cout << std::setw( 4 ) << script_json << std::endl;

  // need to figure out the structure of the files i'm going to use
  //
  // define the json as a reflection of the processes
  // so each one contains a list of the arguments, store a set of long and shorts
  // Within this have a scripts json structure, each scr
  



}
