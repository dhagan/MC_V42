
#include <main.hxx>
#include <autoscript.hxx>

// converting scripts from old version
int main( int argc, char *argv[] ) {

  static struct option long_options[] = {
    { "update",         no_argument,        0,        'u' },
    { 0,                0,                  0,        0   }
  };

  bool update;

  int option_index{0}, option{0};
  do {
    option = getopt_long( argc, argv, "u", long_options, &option_index);
    switch ( option ){
      case 'u':
        update = true;
        break;
    }
  } while ( option != -1 );


  if ( update ){
    autoscript();
  }
}
