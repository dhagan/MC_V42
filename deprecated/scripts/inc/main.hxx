
#include <common.hxx>

#include <TMVA/Factory.h>
#include <TMVA/DataLoader.h>
#include <TMVA/Tools.h>
#include <TMVA/TMVAGui.h>
#include <TMVA/MethodCategory.h>
#include <TMVA/Reader.h>
#include <TMVA/MethodBDT.h>
#include <TMVA/IMethod.h>

