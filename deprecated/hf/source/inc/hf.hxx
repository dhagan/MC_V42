#ifndef hf_hxx
#define hf_hxx

#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "RooCategory.h"
#include "Roo1DTable.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCBShape.h"
#include "RooAbsReal.h"
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooPolynomial.h"
#include "RooDataHist.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"

#include <common.hxx>
#include <style.hxx>
#include <plotting_utils.hxx>
#include <process_utils.hxx>
#include <bound_mgr.hxx>

void hf_fit( std::string & input_file, std::string & abin_var, std::string & spec_var,
             std::string & slice, std::string & unique, bound_mgr * selections );

#endif
