#include <common.hxx>
#include <plotting_utils.hxx>
#include <process_utils.hxx>
#include <style.hxx>

#ifndef parse_file
#define parse_file

void parse_filename( std::string & filename, std::string & avar, std::string & svar );

#endif
