# Histfactory fits.

## Description
This stage of the analysis performs fits of signal and background distribution to the data distribution. This will produce an output file with the signal and background scaled appropriately to match with the fit produced by the histfactory workspace.

## Usage
Scripts for baseline and numerous systematic variations have been written, these `runs scripts are stored in the ./share directory`. These provide proper output structure and output organisation. The selection of slice here allows choice between the subtracted and unsubtracted spectator distribution.

```
Usage:
./hf --input,-i INPUT_PATH --avar,a ANALYSIS_VAR --svar,-v SPECTATOR_VAR --unique,-u UNIQUE
     --selections,-s SELECTION_PATH [ --slice,-t SLICE ] [ --range,-r RANGE_STRING ] [ --help,-h ]

Required arguments;
  --input,-i            Colon separated list of subtraction file paths, data, sign, and bckg.
  --avar,-a             Analysis variable, baseline uses qta. This is the variable in which the 
                        spectator distribution is split by.
  --svar,-s             Distribution which in which the mass sideband and long lifetime subtraction
                        is performed in.
  --selections,-v       A filepath to a selections file. This holds the information of all the variables binning and 
                        ranges, along with the bounds and binning of variables used.
  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is 
                        necessary for bookkeeping.

Optional arguments;
  --ranges, -r          A string that will be processed into a bound for a variable, can be used to replace or augment
                        any variable definition already supplied by the selections file as a method of smaller and 
                        faster systematic variations.
  --help,-h             This message.

```
