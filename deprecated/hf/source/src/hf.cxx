#include "hf.hxx"

//' this needs to go
using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;


void hf_fit( std::string & input, std::string & abin_var, std::string & spec_var, 
             std::string & slice, std::string & unique, bound_mgr * selections ){

  prep_style();
  gROOT->SetBatch(true);

  std::vector<std::string>  mass_index_vec = { "Q0","Q3","Q4","Q5", "Q12",
                                               "T0","T3","T4","T5", "T12" };
  std::string data_type{"data"}, sign_type{"sign"}, bckg_type{"bckg"};
  std::string sub_pos_str = "_" + slice + "_pos_";

  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( abin_var );
  std::vector< std::string > abin_strs = analysis_bound.get_series_names( analysis_bound.get_bins(), true );



  std::vector< std::string > files;
  split_strings( files, input, ":" );
  TFile * tfile_data        = new TFile( files.at(0).c_str(),      "READ");
  TFile * tfile_sign        = new TFile( files.at(1).c_str(),      "READ");
  TFile * tfile_bckg        = new TFile( files.at(2).c_str(),      "READ");

  std::vector<float> data_style = { 21, 1,        1, 1, 1, 1.0,      1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> sign_style = { 1,  kRed+1,   1, 1, 1, kRed+1,   1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> bckg_style = { 1,  kBlue+1,  1, 1, 1, kBlue+1,  1.0, 1.0, 0, 0, 0 }; 
  std::vector<float> fit_style  = { 1,  kGreen+1, 1, 1, 2, kGreen+1, 1.0, 2.0, 0, 0, 0 }; 
  std::vector<float> chi_style  = { 1,  1, 1, 1, 2, 1, 1.0, 2.0, 0, 0, 0 }; 

  std::vector<double> chi2_v1_vec;
  std::vector<double> chi2_v2_vec;

  std::map<std::string,double> unscaled_signal_count,unscaled_pp_count,unscaled_data_count,fitted_signal_count,
                              fitted_PP_count, fitted_mu_value,fitted_SPP_value,rho_value,chi_squared_version1_value,
                              chi_squared_version2_value, fitted_signal_count_error,fitted_PP_count_error,
                              fitted_mu_value_error,fitted_SPP_value_error, empty_bin_count, total_bin_count;

  TPaveText * warning = new TPaveText( 0.4, 0.5, 0.6, 0.8, "NDC" );
  warning->AddText( "NO CONVERGENCE: MINUIT 4" );
  warning->SetTextFont( 42 );
  warning->SetTextSize( 0.03 );
  warning->SetTextColor( 2 );
  warning->SetFillStyle( 0 );
  warning->SetLineColor( 0 );
  warning->SetLineStyle( 0 );
  warning->SetLineWidth( 0 );
  warning->SetBorderSize( 0 );


  // structure this with a unique name etc
  std::string store_name = "./eval/";
  store_name += "hf_fit_" + unique;
  store_name += "_A-";
  store_name += abin_var;
  store_name += "_S-";
  store_name += spec_var;
  store_name += ".root";

  TFile * integral_store = new TFile(store_name.c_str(),"RECREATE");
    

  for (std::string mass_index : mass_index_vec){

    std::cout << mass_index << std::endl;

    for (std::string abin_str : abin_strs){


      std::string input_hist_name      = std::string{ spec_var + sub_pos_str + mass_index + "_" + abin_str};
      std::string canvas_name         = std::string( spec_var + "_hf" + "_" + unique + sub_pos_str + mass_index + "_" + abin_str);
      if ( !unique.empty() ){ canvas_name += ( "_" + unique ); }
      std::string output_hist_name    = std::string( spec_var + sub_pos_str + mass_index + "_" + abin_str);

      std::cout << input_hist_name << std::endl;
      TH1F * data_in = (TH1F*) tfile_data->Get( input_hist_name.c_str() );
      TH1F * sign_in = (TH1F*) tfile_sign->Get( input_hist_name.c_str() );
      TH1F * bckg_in = (TH1F*) tfile_bckg->Get( input_hist_name.c_str() );

      if ( !data_in ){ std::cout << "null hist" << std::endl; continue; }
      if ( !sign_in ){ std::cout << "null hist" << std::endl; continue; }
      if ( !bckg_in ){ std::cout << "null hist" << std::endl; continue; }

      std::cout << "#########################################################################################" << std::endl;
      std::cout << "analysis bin          : " << abin_str << std::endl;
      std::cout << "mass theta index      : " << mass_index << std::endl;

      // Create the measurement
      Measurement measurement("measurement","measurement");
      measurement.SetOutputFilePrefix(  "./results/example_UsingC"  );
      measurement.SetPOI( "mu" ); 
      measurement.AddConstantParam( "Lumi" );
      measurement.SetLumi( 1.0 );
      measurement.SetExportOnly( true );
        
      // Create a channel
      RooStats::HistFactory::Channel chan( "channel1" );
      chan.SetData( data_in );
      chan.SetStatErrorConfig( 0.05, "Poisson" ); //original
    
      // Create the signal sample
      RooStats::HistFactory::Sample signal( "signal" );
      signal.SetHisto( sign_in );
      signal.AddNormFactor( "mu", 0.2, 0.0, 5.0 );
      //signal.AddNormFactor("mu",0.2,0.005,5.0);
      signal.SetNormalizeByTheory( true );
      chan.AddSample( signal );
    
      // Background
      RooStats::HistFactory::Sample background( "pp" );
      background.SetHisto( bckg_in );
      background.AddNormFactor( "ScalingPP", 2.0, 0, 10.0 );
      background.SetNormalizeByTheory( true );
      chan.AddSample( background );
    
      measurement.AddChannel( chan );
      RooWorkspace* wk = RooStats::HistFactory::MakeModelAndMeasurementFast(measurement);
      RooAbsPdf *pd = wk->pdf( "simPdf" );
      RooAbsData *d = wk->data( "obsData" );
      RooFitResult* fr = pd->fitTo( *d, Strategy(2), Save(1), PrintLevel(3) );

      // Get the unfitted histograms.
      TH1F * unfit_data_hist = (TH1F*) tfile_data->Get( input_hist_name.c_str() );
      TH1F * signal_hist     = (TH1F*) tfile_sign->Get( input_hist_name.c_str() );
      TH1F * bckg_hist       = (TH1F*) tfile_bckg->Get( input_hist_name.c_str() );

      RooRealVar * mu = ( RooRealVar *) fr->floatParsFinal().find( "mu" );
      RooRealVar * pp = ( RooRealVar *) fr->floatParsFinal().find( "ScalingPP" );

      double mu_Value = mu->getVal();  //b
      double mu_Error = mu->getError();
      //double err_Signal;
      double n_Signal             = signal_hist->Integral(); //a
      double n_SignalError        = sqrt(signal_hist->GetSumOfWeights()); // this is already squared
      double n_FittedSignal       = signal_hist->Integral() * mu_Value; //y
      double n_FittedSignalError  = n_FittedSignal * sqrt( ((n_SignalError * n_SignalError)/(n_Signal*n_Signal)) + ((mu_Error * mu_Error)/(mu_Value*mu_Value)) );
      double ScalingPP_Value      = pp->getVal();
      double ScalingPP_Error      = pp->getError();
      //double err_pp;
      double n_pp                 = bckg_hist->Integral();
      double n_ppError            = sqrt(bckg_hist->GetSumOfWeights());
      //double n_Fittedpp = bckg_hist->Integral() * ScalingPP_Value;
      double n_FittedppError      =  (n_pp * ScalingPP_Value) * sqrt( ((n_ppError * n_ppError)/(n_pp*n_pp)) + ((ScalingPP_Error * ScalingPP_Error)/(ScalingPP_Value*ScalingPP_Value)) );
      double rho                  = fr->correlation("mu","ScalingPP");


      TH1F * original_data_in = (TH1F*) tfile_data->Get( input_hist_name.c_str() );
      TH1F * original_sign_in = (TH1F*) tfile_sign->Get( input_hist_name.c_str() );
      TH1F * original_bckg_in = (TH1F*) tfile_bckg->Get( input_hist_name.c_str() );
      
      std::cout << "data Integral before bin adjustment: "    << original_data_in->Integral() << std::endl;
      std::cout << "signal Integral before bin adjustment: "  << original_sign_in->Integral() << std::endl;
      std::cout << "pp Integral before bin adjustment: "      << original_bckg_in->Integral() << std::endl;
      std::cout << "data Integral before scaling: "           << unfit_data_hist->Integral() << std::endl;
      std::cout << "signal Integral before scaling: "         << signal_hist->Integral() << std::endl;
      std::cout << "pp Integral before scaling: "             << bckg_hist->Integral() << std::endl;
      
      unscaled_signal_count[output_hist_name]       = signal_hist->Integral();
      unscaled_pp_count[output_hist_name]           = bckg_hist->Integral();
      unscaled_data_count[output_hist_name]         = unfit_data_hist->Integral();
      fitted_signal_count[output_hist_name]         = signal_hist->Integral()*mu_Value;
      fitted_signal_count_error[output_hist_name]   = (signal_hist->Integral()*mu_Value)*sqrt(((n_SignalError*n_SignalError)/(n_Signal*n_Signal))+((mu_Error*mu_Error)/(mu_Value*mu_Value)));
      fitted_PP_count[output_hist_name]             = bckg_hist->Integral()*ScalingPP_Value;
      fitted_PP_count_error[output_hist_name]       = (bckg_hist->Integral()*ScalingPP_Value)*sqrt(((n_ppError*n_ppError)/(n_pp*n_pp))+((ScalingPP_Error*ScalingPP_Error)/(ScalingPP_Value*ScalingPP_Value)));
      fitted_mu_value[output_hist_name]             = mu_Value;
      fitted_mu_value_error[output_hist_name]       = mu_Error; 
      fitted_SPP_value[output_hist_name]            = ScalingPP_Value;
      fitted_SPP_value_error[output_hist_name]      = ScalingPP_Error; 
      rho_value[output_hist_name]                   = rho;

      double chi2_v1 = 0;
      double chi2_v2 = 0;

      //Error and chi2 on the fitted data (Blue) line:
      for (int j= 1; j < unfit_data_hist->GetNbinsX() -1 ; j++){

        double NSig_Events = signal_hist->GetBinContent(j);
        double NPP_Events = bckg_hist->GetBinContent(j);
        double NData_Events = unfit_data_hist->GetBinContent(j);
        double NSig_Error = signal_hist->GetBinError(j);
        double NPP_Error = bckg_hist->GetBinError(j);
        double NData_Error = unfit_data_hist->GetBinError(j);
        if ( (NSig_Events - NSig_Error <= 0) || (NPP_Events - NPP_Error <=0)  || (NData_Events - NData_Error <= 0) ) continue ;
        double NSig_Events2 = NSig_Events * NSig_Events ;
        double NPP_Events2 = NPP_Events * NPP_Events;
        double NSig_Events_Error2 = NSig_Error * NSig_Error;
        double Npp_Events_Error2 = NPP_Error * NPP_Error;
        double fittedData = mu_Value * NSig_Events + ScalingPP_Value * NPP_Events;
        double err_fittedData_j = (NPP_Events2 * (mu_Error*mu_Error)) + (NSig_Events2 * (ScalingPP_Error * ScalingPP_Error)) + ( (mu_Value * mu_Value) * NSig_Events_Error2) + ( (ScalingPP_Value * ScalingPP_Value) * Npp_Events_Error2) + (2 * NSig_Events * NPP_Events * mu_Error * ScalingPP_Error * rho ) ;
        double numerator_v2 = (NData_Events- fittedData) * (NData_Events - fittedData);
        double denominator_v2 =  (NData_Error * NData_Error ) + (err_fittedData_j );  //err_fittedData_i is already squard!
        double numerator_v1 = (NData_Events- fittedData) * (NData_Events - fittedData);
        double denominator_v1 = NData_Error * NData_Error;
        double  chi2_v1_j = numerator_v1 / denominator_v1 ;
        chi2_v1 += chi2_v1_j;
        double chi2_v2_j = numerator_v2 / denominator_v2;
        chi2_v2 += chi2_v2_j;

      }
      
      chi2_v1_vec.push_back( chi2_v1 );
      chi2_v2_vec.push_back( chi2_v2 );

      chi_squared_version1_value[output_hist_name]    = chi2_v1;
      chi_squared_version2_value[output_hist_name]    = chi2_v2;

      // SCALE THE HISTOGRAMS 
      signal_hist->Scale( mu_Value );
      bckg_hist->Scale( ScalingPP_Value );
  
      std::string fit_hist_name = std::string( spec_var + "_fit" + sub_pos_str + mass_index + "_" + abin_str );
      std::string err_hist_name = std::string{ spec_var + "_err" + sub_pos_str + mass_index + "_" + abin_str};

      TH1F * fit_hist = (TH1F*) signal_hist->Clone(fit_hist_name.c_str());
      TH1F * error_hist = new TH1F(err_hist_name.c_str(),err_hist_name.c_str(),3,0.0,3.0);

      fit_hist->Add(signal_hist, bckg_hist, 1., 1.);


      // Data error
      error_hist->SetBinContent( 1, 1 );
      error_hist->SetBinError(1,sqrt( unfit_data_hist->Integral() ) );
      error_hist->SetBinContent( 2, 1 );
      error_hist->SetBinError(2, n_FittedSignalError );
      error_hist->SetBinContent( 3, 1 );
      error_hist->SetBinError( 3, n_FittedppError );

      double emptyBins = 0;
      for (int p = 0; p < fit_hist->GetNbinsX(); p++){
        if (fit_hist->GetBinContent(p+1)== 0 ) { emptyBins++; }
      }
      total_bin_count[output_hist_name]      = fit_hist->GetNbinsX(); 
      empty_bin_count[output_hist_name]      = emptyBins;
      std::cout << "Number of empty bins in fit: " << emptyBins << std::endl;
      std::cout << "signal Integral after scaling: " << signal_hist->Integral() << std::endl;
      std::cout << "pp Integral after scaling: " << bckg_hist->Integral() << std::endl;
      std::cout << "fit Integral: " << fit_hist->Integral() << std::endl;
      std::cout << "ScallingPP Value: " << ScalingPP_Value << std::endl;
      std::cout << "mu Value: " << mu_Value << std::endl;
      std::cout << "Signal Error after scaling: " << n_FittedSignalError << std::endl;
      std::cout << "Rho: " <<  rho << std::endl;
      std::cout << "////////////////////////" << std::endl;


      style_hist( unfit_data_hist,  data_style );
      style_hist( signal_hist,      sign_style );
      style_hist( bckg_hist,        bckg_style );
      style_hist( fit_hist,         fit_style );

      TCanvas * canv = new TCanvas ( output_hist_name.c_str(), output_hist_name.c_str() , 600, 600,1500,1500);
      TPad * current_pad = (TPad *) canv->cd( 1 );

      
      unfit_data_hist->Draw( "E1 P" );
      hist_prep_axes( unfit_data_hist );
      add_atlas_decorations( current_pad, true );
      set_axis_labels( unfit_data_hist, "BDT Score", Form("Entries/%.2f", unfit_data_hist->GetBinWidth(8) ) );
      add_pad_title( current_pad, Form("%s_%s",mass_index.c_str(),abin_str.c_str() ) );
      signal_hist->Draw("HIST SAME");
      bckg_hist->Draw("HIST SAME");
      fit_hist->Draw("HIST SAME");
      TLegend * current_legend = below_logo_legend();
      current_legend->AddEntry( unfit_data_hist,  "data",       "lep" );
      current_legend->AddEntry( signal_hist,      "sign_extr",  "l" );
      current_legend->AddEntry( bckg_hist,        "bckg_extr",  "l" );
      current_legend->AddEntry( fit_hist,         "Fit",        "l" );
      current_legend->Draw();

      auto ltx = new TLatex();
      ltx->SetTextSize( 0.025 );
      ltx->DrawLatexNDC( 0.54, 0.77, Form("n_{sign_extr} = %.0f #pm %0.0f",signal_hist->Integral() ,n_FittedSignalError));
      ltx->DrawLatexNDC( 0.54, 0.74, Form("n_{bckg_extr} = %.0f #pm %0.0f",bckg_hist->Integral() ,n_FittedppError));
      ltx->DrawLatexNDC( 0.54, 0.71, Form("#mu = %.4f #pm %.4f",mu_Value,mu_Error));
      ltx->DrawLatexNDC( 0.54, 0.68, Form("SPP = %.4f #pm %0.4f",ScalingPP_Value,ScalingPP_Error ) );
      ltx->DrawLatexNDC( 0.54, 0.65, Form("#chi^{2}_{V1} = %.4f",chi2_v1) );
      ltx->DrawLatexNDC( 0.54, 0.61, Form("#chi^{2}_{V2} = %.4f",chi2_v2 ) );
       
      if (fr->status() == 4){
        warning->Draw();
      }
      
      std::cout << "#########################################################################################" << std::endl;

      if (fr->status() == 0){
        canv->SaveAs(Form( "./fit/%s.png", canvas_name.c_str() ));
      } else if (fr->status() == 4){
        canv->SaveAs( Form( "./fit/%s-FAILED.png", canvas_name.c_str() ) );
        canv->SaveAs( Form( "./fit_failed/%s-FAILED.png", canvas_name.c_str() ) );
      }
      canv->Close();
      delete wk;
      delete canv; 

      integral_store->cd();

      std::string data_output_name = "data_" + input_hist_name;
      std::string sign_output_name = "sign_" + input_hist_name;
      std::string bckg_output_name = "bckg_" + input_hist_name;
      std::string err_output_name = "errh_" + input_hist_name;
      std::string fit_output_name = "fit_" + input_hist_name;
      unfit_data_hist->Write( data_output_name.c_str() );
      signal_hist->Write( sign_output_name.c_str() );
      bckg_hist->Write( bckg_output_name.c_str() );
      fit_hist->Write( fit_output_name.c_str() );
      error_hist->Write( err_output_name.c_str() );
    
    }
    
    if ( chi2_v1_vec.size() == 0 ){ continue; }

    int abin_bins = analysis_bound.get_bins();
    TH1F * chi2_v1_hist = new TH1F( "chi2_v1_hist", "chi2_v1_hist", abin_bins, 0, abin_bins );
    TH1F * chi2_v2_hist = new TH1F( "chi2_v1_hist", "chi2_v1_hist", abin_bins, 0, abin_bins );
    for ( int bin = 1; bin <= abin_bins; bin++ ){
      chi2_v1_hist->SetBinContent( bin, chi2_v1_vec.at( bin-1 ) );
      chi2_v2_hist->SetBinContent( bin, chi2_v2_vec.at( bin-1 ) );
    }
    style_hist( chi2_v1_hist, chi_style );
    style_hist( chi2_v2_hist, chi_style );
    chi2_v1_hist->SetLineColor( kRed+1 );
    chi2_v2_hist->SetLineColor( kBlue+1 );


    TCanvas * chi2_canv = new TCanvas("chi_canv", "chi_canv", 200, 200, 1000, 1000);
    chi2_v1_hist->Draw( "HIST" );
    chi2_v2_hist->Draw( "HIST SAME" );
    hist_prep_axes( chi2_v1_hist );
    add_atlas_decorations( chi2_canv, true );
    set_axis_labels( chi2_v1_hist, analysis_bound.get_x_str(), Form( "#chi^{2}" ) );
    add_pad_title( chi2_canv, Form( "#chi^{2} %s", mass_index.c_str() ), false );
    TLegend * chi2_legend = below_logo_legend();
    chi2_legend->AddEntry( chi2_v1_hist, "#chi^{2}_{v1}", "l" );
    chi2_legend->AddEntry( chi2_v2_hist, "#chi^{2}_{v2}", "l" );
    chi2_legend->Draw();
    chi2_canv->SaveAs( Form( "./fit/%s_chi2_results.png", mass_index.c_str() ) ); 
    chi2_v1_hist->Write();
    chi2_v2_hist->Write();

    chi2_v1_vec.clear();
    chi2_v2_vec.clear();

  }
   

  
  integral_store->Close();
  delete integral_store;
  
  tfile_data->Close();
  tfile_sign->Close();
  tfile_bckg->Close();
  delete tfile_data;
  delete tfile_sign;
  delete tfile_bckg;

}
