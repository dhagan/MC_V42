#include <main.hxx>
#include <hf.hxx>

int help(){
  std::cout << " Usage:" << std::endl;
  std::cout << " ./hf --input,-i INPUT_PATH --avar,a ANALYSIS_VAR --svar,-v SPECTATOR_VAR --unique,-u UNIQUE" << std::endl;
  std::cout << "      --selections,-s SELECTION_PATH [ --slice,-t SLICE ] [ --range,-r RANGE_STRING ] [ --help,-h ]" << std::endl;
  std::cout << " " << std::endl;
  std::cout << " Required arguments;" << std::endl;
  std::cout << "   --input,-i            Colon separated list of subtraction file paths, data, sign, and bckg." << std::endl;
  std::cout << "   --avar,-a             Analysis variable, baseline uses qta. This is the variable in which the " << std::endl;
  std::cout << "                         spectator distribution is split by." << std::endl;
  std::cout << "   --svar,-s             Distribution which in which the mass sideband and long lifetime subtraction" << std::endl;
  std::cout << "                         is performed in." << std::endl;
  std::cout << "   --selections,-v       A filepath to a selections file. This holds the information of all the variables binning and " << std::endl;
  std::cout << "                         ranges, along with the bounds and binning of variables used." << std::endl;
  std::cout << "   --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is " << std::endl;
  std::cout << "                         necessary for bookkeeping." << std::endl;
  std::cout << " " << std::endl;
  std::cout << " Optional arguments;" << std::endl;
  std::cout << "   --ranges, -r          A string that will be processed into a bound for a variable, can be used to replace or augment" << std::endl;
  std::cout << "                         any variable definition already supplied by the selections file as a method of smaller and " << std::endl;
  std::cout << "                         faster systematic variations." << std::endl;
  std::cout << "   --help,-h             This message." << std::endl;
  return 0;
}

void parse_filename( std::string & filename, std::string & avar, std::string & svar ){
  std::vector< std::string > files;
  split_strings( files, filename, ":" );
  std::string parse = files.at(0);
  parse = parse.substr( parse.find( "A" ), parse.length() );
  avar = parse.substr( 0, parse.find( "_" ) );
  parse = parse.substr( parse.find( "S" ), parse.length() );
  svar = parse.substr( 0, parse.find( "_" ) );
};

int main( int argc, char * argv[] ){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "help",        no_argument,              0,      'h'},
      { "input",       required_argument,        0,      'i'},
      { "slice",       required_argument,        0,      't'},
      { "avar",        required_argument,        0,      'a'},
      { "svar",        required_argument,        0,      's'},
      { "unique",      required_argument,        0,      'u'},
      { "selections",  required_argument,        0,      'v'},
      { "range",       required_argument,        0,      'r'},
      {0,             0,                        0,      0}
    };

  std::string input;
  std::string avar, svar;
  std::string slice, unique, range, bounds_path;
  std::string mass{"Q"};

  do {
    option = getopt_long(argc, argv, "i:t:a:v:u:r:s:h", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'i': 
        input     = std::string(optarg);
        break;
      case 't': 
        slice         = std::string(optarg);
        break;
      case 'a': 
        avar     = std::string(optarg);
        break;
      case 's': 
        svar     = std::string(optarg);
        break;
      case 'u': 
        unique        = std::string(optarg);
        break;
      case 'r':
        range         = std::string( optarg );
        break;
      case 'v':
        bounds_path     = std::string( optarg );
        break;
    }
  } while (option != -1);

  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( bounds_path );
  if ( !range.empty() ){ 
    selections->process_bounds_string( range );
  }
  
  if ( avar.empty() || svar.empty() ){
    parse_filename( input, avar, svar );
  }
  

  hf_fit( input, avar, svar, slice, unique, selections );  

  return 0;
}
