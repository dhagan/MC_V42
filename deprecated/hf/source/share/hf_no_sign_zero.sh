#!/bin/bash

unique="no_sign_zero"
executable=${ANA_IP}/hf/build/hf
sign=${OUT_PATH}/subtraction/base/subtracted_base_sign_A-qtA_S-BDT.root
bckg=${OUT_PATH}/subtraction/base/subtracted_base_bckg_A-qtA_S-BDT.root
zero=${OUT_PATH}/subtraction/zero/subtracted_zero_sign_A-qtA_S-BDT.root
selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/subtraction/sub_${unique}.txt

touch ${log}
mkdir -p ${OUT_PATH}/hf/${unique}
mkdir -p ${OUT_PATH}/hf/${unique}/eval
mkdir -p ${OUT_PATH}/hf/${unique}/fit
mkdir -p ${OUT_PATH}/hf/${unique}/fit_failed


pushd ${OUT_PATH}/hf/${unique} >> /dev/null

$executable -i "${bckg}:${sign}:${zero}" -t 99 -a qtA -s BDT -u ${unique} -v ${selections} ##2&>1 | tee -a ${log}

popd >> /dev/null
