#!/bin/bash

executable=${ANA_IP}/hf/build/hf
unique="shiftright_0p5"
data=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_data_A-qtA_S-BDT.root
sign=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_sign_A-qtA_S-BDT.root
bckg=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_bckg_A-qtA_S-BDT.root
selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/subtraction/sub_${unique}.txt
ranges="qtA:qtA&15:-9.5:20.5"

touch ${log}
mkdir -p ${OUT_PATH}/hf/${unique}
mkdir -p ${OUT_PATH}/hf/${unique}/eval
mkdir -p ${OUT_PATH}/hf/${unique}/fit
mkdir -p ${OUT_PATH}/hf/${unique}/fit_failed


pushd ${OUT_PATH}/hf/${unique} >> /dev/null

$executable -i "${data}:${sign}:${bckg}" -t 99 -a qtA -s BDT -u ${unique} -v ${selections} -r ${ranges}

popd >> /dev/null
