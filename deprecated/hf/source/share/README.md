# Tree Generation Scripts

## List of scripts and purpose.
* hf\_base.sh - This is the baseline script.
* hf\_no\_sign\_sub.sh - This is a test using background in place of the signal, performed as a check for bias of signal introduced by histfactory fits. Uses subtraction step background as data.
* hf\_no\_sign\_zero.sh - Another check of HF fit bias, fitting signal MC to bckg MC.
* hf\_exp\_bg.sh - run the hf extraction for version where subtraction accounts for 5% more background by exp fit.


## Script Structure
Below is how the scripts are structured. All of the defined environment variables have been defined by the setup script. This setup script must be sourced before executing any of the scripts.

Variable definitions come first.
```
executable=${ANA_IP}/hf/build/hf
unique="base"
data=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_data_A-qtA_S-BDT.root
sign=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_sign_A-qtA_S-BDT.root
bckg=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_bckg_A-qtA_S-BDT.root
selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/subtraction/sub_${unique}.txt
```
Variables for executable and unique defined as usual.
Then data, sign, bckg, defined as usual.
Selections and log also defined like normal.

Create the log file for storing information from runs, similarly create the storage structure necessary for outputting files properly
```
touch ${log}
mkdir -p ${OUT_PATH}/hf/${unique}
mkdir -p ${OUT_PATH}/hf/${unique}/eval
mkdir -p ${OUT_PATH}/hf/${unique}/fit
mkdir -p ${OUT_PATH}/hf/${unique}/fit_failed
```

Enter into the output directory for executing the program
```
pushd ${OUT_PATH}/hf/${unique} >> /dev/null
```

Perform histfactory fitting
```
$executable -i "${data}:${sign}:${bckg}" -t 99 -a qtA -v BDT -u ${unique} -b ${selections} ##2&>1 | tee -a ${log}
```


Exit back to starting directory.
```
popd >> /dev/null
```
