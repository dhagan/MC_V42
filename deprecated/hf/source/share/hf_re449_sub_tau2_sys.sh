#!/bin/bash

executable=${ANA_IP}/hf/build/hf
unique="re449_sub_tau2_sys"
data=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_data_A-qtA_S-BDT.root
sign=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_sign_A-qtA_S-BDT.root
bckg=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_bckg_A-qtA_S-BDT.root
selections=${LIB_PATH}/share/hf_bounds.txt
log=${LOG_PATH}/subtraction/sub_${unique}.txt

touch ${log}
mkdir -p ${OUT_PATH}/hf/${unique}
mkdir -p ${OUT_PATH}/hf/${unique}/eval
mkdir -p ${OUT_PATH}/hf/${unique}/fit
mkdir -p ${OUT_PATH}/hf/${unique}/fit_failed


pushd ${OUT_PATH}/hf/${unique} >> /dev/null

$executable -i "${data}:${sign}:${bckg}" -t 99 -a qtA -s BDT -u ${unique} -v ${selections} ##2&>1 | tee -a ${log}

popd >> /dev/null
