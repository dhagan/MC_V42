#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/build/hf

unique=shiftr
qta_range="qtA_15"
qtb_range="abs(qtB)_10"

mkdir -p run_${unique}
mkdir -p run_${unique}/eval/
mkdir -p run_${unique}/failed_output/
mkdir -p run_${unique}/png_output/

data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT.root
sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT.root 
bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT.root
$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique} -r ${qta_range}

##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u ${unique} -r ${qtb_range}
