#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/build/hf

mkdir -p run_blnc
mkdir -p run_blnc/eval/
mkdir -p run_blnc/failed_output/
mkdir -p run_blnc/png_output/

##names=(80 85 90 95 105 110 115 120)
##names=(200 250)
#names=(225)
names=(210)
for name in ${names[@]}
do
	data_phi_high=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_high_${name}.root
	sign_phi_high=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_high_${name}.root 
	bckg_phi_high=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_high_${name}.root
	$executable -d ${data_phi_high} -s ${sign_phi_high} -b ${bckg_phi_high} -t 99 -a Phi -v BDT -u blnc:high_${name}
	
	data_phi_low=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_low_${name}.root   	
	sign_phi_low=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_low_${name}.root 
	bckg_phi_low=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_low_${name}.root
	$executable -d ${data_phi_low} -s ${sign_phi_low} -b ${bckg_phi_low} -t 99 -a Phi -v BDT -u blnc:low_${name}
done
