#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/build/hf

qta_range="qtA_15"
qtb_range="abs(qtB)_10"

##vars=( "aaf" "afa" "faa" "extd" "noqt2" "noL" )
##vars=( "aaf" )
##vars=( "aaDPDY" "afDPDY" "faDPDY" "ffDPDY" )
vars=( "aaf" "afa" "faa" "extd" "noqt2" "noL" "aaf" "aaDPDY" "afDPDY" "faDPDY" "ffDPDY" )

mkdir -p var_sys
cd var_sys

sub_path=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/var_sys/subtractions_

for var in ${vars[@]}
do
	unique=${var}
	mkdir -p run_${unique}
	mkdir -p run_${unique}/eval/
	mkdir -p run_${unique}/failed_output/
	mkdir -p run_${unique}/png_output/
	data_qta=${sub_path}${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${unique}.root
	sign_qta=${sub_path}${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${unique}.root
	bckg_qta=${sub_path}${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${unique}.root
	$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique} -r ${qta_range}
done
