#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/build/hf

unique="12alt3"
qta_range="qtA_15"
qtb_range="abs(qtB)_10"

mkdir -p run_${unique}
mkdir -p run_${unique}/eval/
mkdir -p run_${unique}/failed_output/
mkdir -p run_${unique}/png_output/

data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${unique}.root
sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${unique}.root 
bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${unique}.root
$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique} -r ${qta_range}
##
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u ${unique} -r ${qtb_range}
##
##data_phi_high=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-Phi_S-BDT_high.root
##sign_phi_high=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-Phi_S-BDT_high.root 
##bckg_phi_high=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-Phi_S-BDT_high.root
##$executable -d ${data_phi_high} -s ${sign_phi_high} -b ${bckg_phi_high} -t 99 -a Phi -v BDT -u ${unique}:high
##
##data_phi_low=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-Phi_S-BDT_low.root
##sign_phi_low=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-Phi_S-BDT_low.root 
##bckg_phi_low=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-Phi_S-BDT_low.root
##$executable -d ${data_phi_low} -s ${sign_phi_low} -b ${bckg_phi_low} -t 99 -a Phi -v BDT -u ${unique}:low

##data_phi=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-Phi_S-BDT.root
##sign_phi=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-Phi_S-BDT.root 
##bckg_phi=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-Phi_S-BDT.root
##$executable -d ${data_phi} -s ${sign_phi} -b ${bckg_phi} -t 99 -a Phi -v BDT -u ${unique}


##
##data_photonpt=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_data_A-PhotonPt_S-BDT.root
##sign_photonpt=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_sign_A-PhotonPt_S-BDT.root 
##bckg_photonpt=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_bckg_A-PhotonPt_S-BDT.root
##$executable -d ${data_photonpt} -s ${sign_photonpt} -b ${bckg_photonpt} -t 99 -a PhotonPt -v BDT -u base:base
##
##data_dimuonpt=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_data_A-DiMuonPt_S-BDT.root
##sign_dimuonpt=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_sign_A-DiMuonPt_S-BDT.root 
##bckg_dimuonpt=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_bckg_A-DiMuonPt_S-BDT.root
##$executable -d ${data_dimuonpt} -s ${sign_dimuonpt} -b ${bckg_dimuonpt} -t 99 -a DiMuonPt -v BDT -u base:base
##
##data_dphi=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_data_A-DPhi_S-BDT.root
##sign_dphi=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_sign_A-DPhi_S-BDT.root 
##bckg_dphi=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_base/eval/subtracted_base_bckg_A-DPhi_S-BDT.root
##$executable -d ${data_dphi} -s ${sign_dphi} -b ${bckg_dphi} -t 99 -a DPhi -v BDT -u base:base


