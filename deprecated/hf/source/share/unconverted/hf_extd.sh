#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/build/hf

mkdir -p run_extd
mkdir -p run_extd/eval/
mkdir -p run_extd/failed_output/
mkdir -p run_extd/png_output/

range="qtA_15"

data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_extd/eval/subtracted_extd_data_A-qtA_S-BDT.root
sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_extd/eval/subtracted_extd_sign_A-qtA_S-BDT.root 
bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_extd/eval/subtracted_extd_bckg_A-qtA_S-BDT.root
$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u extd -r ${range}

data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_extd/eval/subtracted_extd_data_A-qtB_S-BDT.root
sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_extd/eval/subtracted_extd_sign_A-qtB_S-BDT.root 
bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_extd/eval/subtracted_extd_bckg_A-qtB_S-BDT.root
$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u extd
