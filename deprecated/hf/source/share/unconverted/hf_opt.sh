#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/build/hf

unique="base"
qta_range="qtA_15"
qtb_range="abs(qtB)_10"

params=( "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=GiniIndex:DoBoostMonitor=True#AdaBoost-GiniIndex-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=GiniIndexWithLaplace:DoBoostMonitor=True#AdaBoost-GiniIndexWithLaplace-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=MisClassificationError:DoBoostMonitor=True#AdaBoost-MisClassificationError-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=GiniIndex:DoBoostMonitor=True#AdaBoost-GiniIndex-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=GiniIndexWithLaplace:DoBoostMonitor=True#AdaBoost-GiniIndexWithLaplace-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=MisClassificationError:DoBoostMonitor=True#AdaBoost-MisClassificationError-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=10.000000:nCuts=50:DoPreselection=0:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N10C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=10.000000:nCuts=50:DoPreselection=0:SeparationType=GiniIndex:DoBoostMonitor=True#AdaBoost-GiniIndex-B1T100D3N10C50P0" )
vars=( alt angular momentum maximum minimum )

##params=( "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N5C50P0")
##vars=( alt )

mkdir -p opt
cd opt

sub_path=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/opt/subtractions_

for var in ${vars[@]}
do
	for param in ${params[@]}
	do
		unique=${param#*#}_${var}
		mkdir -p run_${unique}
		mkdir -p run_${unique}/eval/
		mkdir -p run_${unique}/failed_output/
		mkdir -p run_${unique}/png_output/
		data_qta=${sub_path}${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${unique}.root
		sign_qta=${sub_path}${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${unique}.root
		bckg_qta=${sub_path}${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${unique}.root
		$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique} -r ${qta_range} &
	done
done
