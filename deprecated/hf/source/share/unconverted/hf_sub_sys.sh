#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/build/hf

unique="sub_sys"
qta_range="qtA_15"
qtb_range="abs(qtB)_10"

mkdir -p run_${unique}
mkdir -p run_${unique}/eval/
mkdir -p run_${unique}/failed_output/
mkdir -p run_${unique}/png_output/

## QTA
local_unique=DiMuonMass1_Tau0
data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtA_S-BDT_${local_unique}.root
sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtA_S-BDT_${local_unique}.root 
bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtA_S-BDT_${local_unique}.root
$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique}:${local_unique} -r ${qta_range}

local_unique=DiMuonMass0_Tau1
data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtA_S-BDT_${local_unique}.root
sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtA_S-BDT_${local_unique}.root 
bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtA_S-BDT_${local_unique}.root
$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique}:${local_unique} -r ${qta_range}

local_unique=DiMuonMass0_Tau2
data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtA_S-BDT_${local_unique}.root
sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtA_S-BDT_${local_unique}.root 
bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtA_S-BDT_${local_unique}.root
$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique}:${local_unique} -r ${qta_range}

## qtB
##local_unique=DiMuonMass1_Tau0
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtB_S-BDT_${local_unique}.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtB_S-BDT_${local_unique}.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u ${unique}:${local_unique} -r ${qtb_range}
##
##local_unique=DiMuonMass0_Tau1
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtB_S-BDT_${local_unique}.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtB_S-BDT_${local_unique}.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u ${unique}:${local_unique} -r ${qtb_range}
##
##local_unique=DiMuonMass0_Tau2
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtB_S-BDT_${local_unique}.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtB_S-BDT_${local_unique}.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u ${unique}:${local_unique} -r ${qtb_range}



##data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_SYS_DiMuonMass0_Tau1.root
##sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_SYS_DiMuonMass0_Tau1.root 
##bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_SYS_DiMuonMass0_Tau1.root
##$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique}:SYS_DiMuonMass0_Tau1 -r ${qta_range}
##
##data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_SYS_DiMuonMass0_Tau2.root
##sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_SYS_DiMuonMass0_Tau2.root 
##bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_SYS_DiMuonMass0_Tau2.root
##$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u ${unique}:SYS_DiMuonMass0_Tau2 -r ${qta_range}


##data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtA_S-BDT_SYS_DiMuonMass1_Tau1.root
##sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtA_S-BDT_SYS_DiMuonMass1_Tau1.root 
##bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtA_S-BDT_SYS_DiMuonMass1_Tau1.root
##$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u sub_sys:SYS_DiMuonMass1_Tau1
##
##data_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtA_S-BDT_SYS_DiMuonMass1_Tau2.root
##sign_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtA_S-BDT_SYS_DiMuonMass1_Tau2.root 
##bckg_qta=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtA_S-BDT_SYS_DiMuonMass1_Tau2.root
##$executable -d ${data_qta} -s ${sign_qta} -b ${bckg_qta} -t 99 -a qtA -v BDT -u sub_sys:SYS_DiMuonMass1_Tau2




## QTB
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass1_Tau0.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass1_Tau0.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass1_Tau0.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u sub_sys:SYS_DiMuonMass1_Tau0
##
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass1_Tau1.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass1_Tau1.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass1_Tau1.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u sub_sys:SYS_DiMuonMass1_Tau1
##
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass1_Tau2.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass1_Tau2.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass1_Tau2.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u sub_sys:SYS_DiMuonMass1_Tau2
##
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass0_Tau1.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass0_Tau1.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass0_Tau1.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u sub_sys:SYS_DiMuonMass0_Tau1
##
##data_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass0_Tau2.root
##sign_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass0_Tau2.root 
##bckg_qtb=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass0_Tau2.root
##$executable -d ${data_qtb} -s ${sign_qtb} -b ${bckg_qtb} -t 99 -a qtB -v BDT -u sub_sys:SYS_DiMuonMass0_Tau2
