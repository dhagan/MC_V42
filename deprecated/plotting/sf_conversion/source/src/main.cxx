#include <main.hxx>

int main( int argc, char * argv[] ){

  static struct option long_options[] = {
    { "input",           required_argument,          0,      'i'},
    { "unique",          required_argument,          0,      'u'},
    { 0,                  0,                        0,        0}
  };

  int option_index{0}, option{0};

  // default args
  std::string scale_path{ "/home/atlas/dhagan/other/ScaleFactory/ForTamuna/signal_00_P5000.root" };
  std::string unique{ "" };

  do {
    option = getopt_long( argc, argv, "i:u:", long_options, &option_index);
    switch ( option ){
      case 'i':
				scale_path = std::string( optarg );
				break;
      case 'u':
        unique = std::string( optarg );
        break;
		}
	} while ( option != -1 );
	
	


  TFile * scale_factor_file = new TFile( scale_path.c_str() , "READ" );
  TTree * scale_tree = (TTree *) scale_factor_file->Get( "trout" );

  TFile * output_file = new TFile( Form( "./scale_factor_%s.root", unique.c_str() ),"RECREATE" );
 
  int mass_bins[5] = { 0, 3, 4, 5, 12 };

  int bins = 15;
  float min{-10}, max{20};
  float width = (max-min)/((float) bins);

  std::map< int, std::string> lambda_cuts;
  lambda_cuts[0]             = std::string("lambda>0&&lambda<200");
  lambda_cuts[3]             = std::string("lambda>25&&lambda<50");
  lambda_cuts[4]             = std::string("lambda>50&&lambda<100");
  lambda_cuts[5]             = std::string("lambda>100&&lambda<200");
  lambda_cuts[12]            = std::string("lambda>25&&lambda<200");


  for ( int & mass_idx : mass_bins ){

    TH1F * scale_factor_mean_hist = new TH1F( Form("scale_factor_mean_Q%i", mass_idx), "", bins, min, max );
    TH1F * scale_factor_rms_hist = new TH1F( Form("scale_factor_rms_Q%i", mass_idx), "", bins, min, max );
    TH1F * scale_factor_err_hist = new TH1F( Form("scale_factor_error_Q%i", mass_idx), "", bins, min, max );
    std::string lambda_cut = lambda_cuts[mass_idx];

    for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){

      TH1F * temp_hist = new TH1F( Form( "scale_fac_Q%i_qtA%i", mass_idx, bin_idx ), "", 50, 0, 2 );
      float qta_max = min + bin_idx*width;
      float qta_min = qta_max - width;
      std::string qta_cut = std::string( Form( "qtA>%.6f&&qtA<%.6f", qta_min, qta_max ) );
      scale_tree->Draw( Form( "SF>>scale_fac_Q%i_qtA%i", mass_idx, bin_idx ), Form( "%s&&%s", lambda_cut.c_str(),
                                                                                   qta_cut.c_str() ) );
      scale_factor_mean_hist->SetBinContent( bin_idx, temp_hist->GetMean() );
      scale_factor_rms_hist->SetBinContent( bin_idx, temp_hist->GetRMS() );

    }
    scale_factor_err_hist->Divide( scale_factor_rms_hist, scale_factor_mean_hist, 1.0, 1.0 );

    output_file->cd();
    scale_factor_mean_hist->Write();
    scale_factor_rms_hist->Write();
    scale_factor_err_hist->Write();
    scale_factor_err_hist->Write( Form( "err_Q%i", mass_idx ) );

  }

  output_file->Close();

}
