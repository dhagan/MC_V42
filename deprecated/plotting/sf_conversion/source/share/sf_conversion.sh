executable=${ANA_IP}/plotting/sf_conversion/build/sf_conversion
unique="qtbsplit"

mkdir -p ${OUT_PATH}/sf_conversion/${unique}

pushd ${OUT_PATH}/sf_conversion/${unique} >> /dev/null

##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000.root -u ${unique}
##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit.root -u ${unique}
##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-1.root -u qtbsplit-1
##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-2.root -u qtbsplit-2
##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-3.root -u qtbsplit-3
##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-4.root -u qtbsplit-4
##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-5.root -u qtbsplit-5
##$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-6.root -u qtbsplit-6
##executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-7.root -u qtbsplit-7
##executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtbsplit-8.root -u qtbsplit-8

$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtb_lower.root -u qtb_lower
$executable -i ${OUT_PATH}/ScaleFactors/Signal_00_P5000_qtb_upper.root -u qtb_upper


popd >> /dev/null
