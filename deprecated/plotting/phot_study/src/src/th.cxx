#include <th.hxx>

void th( ){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(10);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42);
  wip.SetTextSize(0.038); wip.SetTextColor(1);
  TLatex sim;  sim.SetNDC(); sim.SetTextFont(42);
  sim.SetTextSize(0.038); sim.SetTextColor(1);



  TFile * sf_file = new TFile("~/other/ScaleFactory/ForTamuna/signal_00_P5000.root", "READ" );
  TTree * sf_tree = (TTree*) sf_file->Get( "trout" );
  TTree * t_sf_tree = (TTree*) sf_file->Get( "tr2out" );
  
  TFile * reco_file = new TFile("~/analysis/analysisFork/445/TMVATrees/run/gen_base/sign/sign_base.root", "READ" );
  TTree * reco_tree = (TTree*) reco_file->Get( "TreeD" );

  TFile * trth_file = new TFile("~/analysis/analysisFork/445/TMVATrees/run/gen_base/truth_sign_base.root", "READ" );
  TTree * trth_tree = (TTree*) trth_file->Get( "TreeT" );

  reco_tree->AddFriend( sf_tree );
  trth_tree->AddFriend( t_sf_tree );

  int     qta_bins    = 15;
  double  qta_min     = -10.0;
  double  qta_max     = 20.0;
  double qta_width = ( qta_max - qta_min )/( (double) qta_bins );
  std::string qta_var = "qtA";
  std::string qta_unit = "GeV";

  int     photon_bins    = 25;
  double  photon_min     = 5;
  double  photon_max     = 30;
  double photon_width = ( photon_max - photon_min )/( (double) photon_bins );
  std::string photon_var = "Photon Pt";
  std::string photon_unit = "GeV";

  int     jpsi_bins    = 25;
  double  jpsi_min     = 5;
  double  jpsi_max     = 30;
  double jpsi_width = ( jpsi_max - jpsi_min )/( (double) jpsi_bins );
  std::string jpsi_var = "JPsi Pt";
  std::string jpsi_unit = "GeV";

  int     phot_eta_bins    = 20;
  double  phot_eta_min     = 0;
  double  phot_eta_max     = 2.5;
  double phot_eta_width = ( phot_eta_max - phot_eta_min )/( (double) phot_eta_bins );
  std::string phot_eta_var = "Photon Eta";
  std::string phot_eta_unit = "GeV";

  int     jpsi_eta_bins    = 20;
  double  jpsi_eta_min     = 0;
  double  jpsi_eta_max     = 2.5;
  double jpsi_eta_width = ( jpsi_eta_max - jpsi_eta_min )/( (double) jpsi_eta_bins );
  std::string jpsi_eta_var = "JPsi Eta";
  std::string jpsi_eta_unit = " ";

  int     sf_bins    = 10;
  double  sf_min     = 0.94;
  double  sf_max     = 1.02;
  double sf_width = ( sf_max - sf_min )/( (double) sf_bins );
  std::string sf_var = "Photon SF";
  std::string sf_unit = " ";

  
  std::vector<int> mass_ranges = { 0, 3, 4, 5, 12 };
  std::map< int, std::string > lambda_cuts;
  lambda_cuts[0] =   "Lambda>15&&Lambda<200";
  lambda_cuts[3] =   "Lambda>25&&Lambda<50";
  lambda_cuts[4] =   "Lambda>50&&Lambda<100";
  lambda_cuts[5] =   "Lambda>100&&Lambda<200";
  lambda_cuts[12] =  "Lambda>25&&Lambda<200";
  

  std::vector< std::string > photon_cuts; 
  photon_cuts.push_back( Form("PhotonPt/1000.0>%f&&PhotonPt/1000.0<%f", photon_min, photon_max ));
  double phot_plot_width = ( photon_max - photon_min )/( 10.0 );
  for ( int p_ind = 0; p_ind < 10; p_ind++ ){
    double p_lower = photon_min + p_ind*phot_plot_width;     
    double p_upper = photon_min + (1+p_ind)*phot_plot_width;     
    photon_cuts.push_back( Form("PhotonPt/1000.0>%f&&PhotonPt/1000.0<%f", p_lower, p_upper ));
  }



  for ( int mass_number : mass_ranges ){

    for ( int p_ind = 0; p_ind <= 10; p_ind++ ){
      
      std::string lambda_cut = lambda_cuts[mass_number];
      std::string photon_cut = photon_cuts[p_ind];
      std::string full_cut = photon_cut + "&&" + lambda_cut;

      TH2F * tr_photpt_qta = new TH2F( Form("tr_photpt_qta_Q%i", mass_number), "", photon_bins, photon_min, photon_max,
                                          qta_bins, qta_min, qta_max );
      TH2F * re_photpt_qta = new TH2F( Form("re_photpt_qta_Q%i", mass_number), "", photon_bins, photon_min, photon_max,
                                          qta_bins, qta_min, qta_max );
      TH2F * eff_photpt_qta = new TH2F( Form("eff_photpt_qta_Q%i", mass_number), "", photon_bins, photon_min, photon_max,
                                          qta_bins, qta_min, qta_max );
      reco_tree->Draw( Form("qtA:PhotonPt/1000.0>>re_photpt_qta_Q%i", mass_number), full_cut.c_str(), "goff" );
      trth_tree->Draw( Form("qtA:PhotonPt/1000.0>>tr_photpt_qta_Q%i", mass_number), full_cut.c_str(), "goff" );
      eff_photpt_qta->Divide( re_photpt_qta, tr_photpt_qta, 1.0, 1.0, "B" );
      TCanvas * canv = new TCanvas( Form( "eff_canv_Q%i", mass_number ), "", 200, 200, 1000, 1000);
      canv->Divide( 1 );
      canv->cd( 1 );
      eff_photpt_qta->SetMinimum(0.0);
      eff_photpt_qta->SetMaximum(1.2);
      eff_photpt_qta->Draw("COLZ");
      eff_photpt_qta->SetMarkerStyle(21);
      eff_photpt_qta->SetLineColor(2);
      eff_photpt_qta->GetYaxis()->SetTitle( Form( "%s /%.2f %s", qta_var.c_str(), qta_width, qta_unit.c_str() ) );
      eff_photpt_qta->GetXaxis()->SetTitle( Form( "%s /%.2f %s", photon_var.c_str(), photon_width, photon_unit.c_str() ) );
      eff_photpt_qta->GetYaxis()->SetLabelSize(0.035);
      eff_photpt_qta->GetYaxis()->SetTitleSize(0.035);
      eff_photpt_qta->GetXaxis()->SetLabelSize(0.035);
      eff_photpt_qta->GetXaxis()->SetTitleSize(0.035);
      eff_photpt_qta->GetZaxis()->SetTitleSize(0.035);
      eff_photpt_qta->GetZaxis()->SetLabelSize(0.035);
      eff_photpt_qta->GetZaxis()->SetMaxDigits(3);
      eff_photpt_qta->GetZaxis()->ChangeLabel(7, -1., -1., -1, -1, -1, ">=1.2" );
      gPad->Update();
      TLatex eff_ltx_title;
      eff_ltx_title.SetTextSize(0.03);
      eff_ltx_title.DrawLatexNDC(0.2,0.825, Form("0-1.2 Photon Pt Eff vs qtA Eff  Q%i", mass_number ) );
      canv->SaveAs( Form( "./eff_photonpt_qta_Q%i_P%i.png", mass_number, p_ind ) );

      TCanvas * tr_canv = new TCanvas( Form( "tr_canv_Q%i", mass_number ), "", 200, 200, 1000, 1000);
      tr_canv->Divide( 1 );
      tr_canv->cd( 1 );
      tr_photpt_qta->Draw("COLZ");
      tr_photpt_qta->SetMarkerStyle(21);
      tr_photpt_qta->SetLineColor(2);
      tr_photpt_qta->GetYaxis()->SetTitle( Form( "%s /%.2f %s", qta_var.c_str(), qta_width, qta_unit.c_str() ) );
      tr_photpt_qta->GetXaxis()->SetTitle( Form( "%s /%.2f %s", photon_var.c_str(), photon_width, photon_unit.c_str() ) );
      tr_photpt_qta->GetYaxis()->SetLabelSize(0.035);
      tr_photpt_qta->GetYaxis()->SetTitleSize(0.035);
      tr_photpt_qta->GetXaxis()->SetLabelSize(0.035);
      tr_photpt_qta->GetXaxis()->SetTitleSize(0.035);
      tr_photpt_qta->GetZaxis()->SetMaxDigits(3);
      gPad->Update();
      TLatex tr_ltx_title;
      tr_ltx_title.SetTextSize(0.03);
      tr_ltx_title.DrawLatexNDC(0.2,0.825, Form("Truth Photon Pt vs qtA Q%i", mass_number ) );
      tr_canv->SaveAs( Form( "./tr_photonpt_qta_Q%i_P%i.png", mass_number, p_ind ) );
      

      TCanvas * re_canv = new TCanvas( Form( "re_canv_Q%i", mass_number ), "", 200, 200, 1000, 1000);
      re_canv->Divide( 1 );
      re_canv->cd( 1 );
      re_photpt_qta->Draw("COLZ");
      re_photpt_qta->SetMarkerStyle(21);
      re_photpt_qta->SetLineColor(2);
      re_photpt_qta->GetYaxis()->SetTitle( Form( "%s /%.2f %s", qta_var.c_str(), qta_width, qta_unit.c_str() ) );
      re_photpt_qta->GetXaxis()->SetTitle( Form( "%s /%.2f %s", photon_var.c_str(), photon_width, photon_unit.c_str() ) );
      re_photpt_qta->GetYaxis()->SetLabelSize(0.035);
      re_photpt_qta->GetYaxis()->SetTitleSize(0.035);
      re_photpt_qta->GetXaxis()->SetLabelSize(0.035);
      re_photpt_qta->GetXaxis()->SetTitleSize(0.035);
      re_photpt_qta->GetZaxis()->SetMaxDigits(3);
      gPad->Update();
      TLatex re_ltx_title;
      re_ltx_title.SetTextSize(0.03);
      re_ltx_title.DrawLatexNDC(0.2,0.825, Form("Reco Photon Pt vs qtA Q%i", mass_number ) );
      re_canv->SaveAs( Form( "./re_photonpt_qta_Q%i_P%i.png", mass_number, p_ind ) );

      TCanvas * sf_phot_canv = new TCanvas( Form( "sf_canv_Q%i", mass_number ), "", 200, 200, 1000, 1000);
      TH2F * photpt_sf = new TH2F( Form("photpt_sf_Q%i", mass_number), "", photon_bins, photon_min, photon_max, sf_bins, sf_min, sf_max );
      reco_tree->Draw( Form("photon_id_sf:PhotonPt/1000.0>>photpt_sf_Q%i", mass_number), full_cut.c_str(), "goff" );
      sf_phot_canv->Divide( 1 );
      sf_phot_canv->cd( 1 );
      photpt_sf->Draw("COLZ");
      photpt_sf->SetMarkerStyle(21);
      photpt_sf->SetLineColor(2);
      photpt_sf->GetYaxis()->SetTitle( Form( "%s /%.2f %s", sf_var.c_str(), sf_width, sf_unit.c_str() ) );
      photpt_sf->GetXaxis()->SetTitle( Form( "%s /%.2f %s", photon_var.c_str(), photon_width, photon_unit.c_str() ) );
      photpt_sf->GetYaxis()->SetLabelSize(0.035);
      photpt_sf->GetYaxis()->SetTitleSize(0.035);
      photpt_sf->GetXaxis()->SetLabelSize(0.035);
      photpt_sf->GetXaxis()->SetTitleSize(0.035);
      photpt_sf->GetZaxis()->SetMaxDigits(3);
      gPad->Update();
      TLatex sfphot_ltx_title;
      sfphot_ltx_title.SetTextSize(0.03);
      sfphot_ltx_title.DrawLatexNDC(0.2,0.825, Form(" Photon Pt vs Photon ID SF - Q%i", mass_number ) );
      sf_phot_canv->SaveAs( Form( "./photonpt_sf_Q%i_P%i.png", mass_number, p_ind  ) );



      TCanvas * sf_qta_canv = new TCanvas( Form( "sf_qta_canv_Q%i_P%i", mass_number, p_ind ), "", 200, 200, 1000, 1000);
      TH2F * qta_sf = new TH2F( Form("qta_sf_Q%i", mass_number), "", qta_bins, qta_min, qta_max, sf_bins, sf_min, sf_max );
      reco_tree->Draw( Form("photon_id_sf:qtA>>qta_sf_Q%i", mass_number), full_cut.c_str(), "goff" );
      sf_qta_canv->Divide( 1 );
      sf_qta_canv->cd( 1 );
      qta_sf->Draw("COLZ");
      qta_sf->SetMarkerStyle(21);
      qta_sf->SetLineColor(2);
      qta_sf->GetYaxis()->SetTitle( Form( "%s /%.2f %s", sf_var.c_str(), sf_width, sf_unit.c_str() ) );
      qta_sf->GetXaxis()->SetTitle( Form( "%s /%.2f %s", qta_var.c_str(), qta_width, qta_unit.c_str() ) );
      qta_sf->GetYaxis()->SetLabelSize(0.035);
      qta_sf->GetYaxis()->SetTitleSize(0.035);
      qta_sf->GetXaxis()->SetLabelSize(0.035);
      qta_sf->GetXaxis()->SetTitleSize(0.035);
      qta_sf->GetZaxis()->SetMaxDigits(3);
      gPad->Update();
      TLatex sfqta_ltx_title;
      sfqta_ltx_title.SetTextSize(0.03);
      sfqta_ltx_title.DrawLatexNDC(0.2,0.825, Form(" qtA vs Photon ID SF - Q%i", mass_number ) );
      sf_qta_canv->SaveAs( Form( "./qta_sf_Q%i_P%i.png", mass_number, p_ind ) );



      TCanvas * eff_sf_qta_canv = new TCanvas( Form( "eff_sf_qta_canv_Q%i_P%i", mass_number, p_ind ), "", 200, 200, 1000, 1000);
      TH2F * eff_qta_sf = new TH2F( Form("eff_qta_sf_Q%i", mass_number), "", qta_bins, qta_min, qta_max,
                                          sf_bins, sf_min, sf_max );
      TH2F * re_qta_sf = new TH2F( Form("re_qta_sf_Q%i", mass_number), "", qta_bins, qta_min, qta_max,
                                          sf_bins, sf_min, sf_max );
      TH2F * tr_qta_sf = new TH2F( Form("tr_qta_sf_Q%i", mass_number), "", qta_bins, qta_min, qta_max,
                                          sf_bins, sf_min, sf_max );
      reco_tree->Draw( Form("photon_id_sf:qtA>>re_qta_sf_Q%i", mass_number), full_cut.c_str(), "goff" );
      trth_tree->Draw( Form("t_photon_id_sf:qtA>>tr_qta_sf_Q%i", mass_number), full_cut.c_str(), "goff" );
      eff_qta_sf->Divide( re_qta_sf, tr_qta_sf, 1.0, 1.0, "B" );
      eff_sf_qta_canv->Divide( 1 );
      eff_sf_qta_canv->cd( 1 );
      eff_qta_sf->SetMinimum(0.0);
      eff_qta_sf->SetMaximum(1.2);
      eff_qta_sf->Draw("COLZ");
      eff_qta_sf->SetMarkerStyle(21);
      eff_qta_sf->SetLineColor(2);
      eff_qta_sf->GetYaxis()->SetTitle( Form( "%s /%.2f %s", sf_var.c_str(), sf_width, sf_unit.c_str() ) );
      eff_qta_sf->GetXaxis()->SetTitle( Form( "%s /%.2f %s", qta_var.c_str(), qta_width, qta_unit.c_str() ) );
      eff_qta_sf->GetYaxis()->SetLabelSize(0.035);
      eff_qta_sf->GetYaxis()->SetTitleSize(0.035);
      eff_qta_sf->GetXaxis()->SetLabelSize(0.035);
      eff_qta_sf->GetXaxis()->SetTitleSize(0.035);
      eff_qta_sf->GetZaxis()->SetMaxDigits(3);
      eff_qta_sf->GetZaxis()->SetTitleSize(0.035);
      eff_qta_sf->GetZaxis()->SetLabelSize(0.035);
      eff_qta_sf->GetZaxis()->ChangeLabel(7, -1., -1., -1, -1, -1, ">=1.2" );
      gPad->Update();
      TLatex eff_sfqta_ltx_title;
      eff_sfqta_ltx_title.SetTextSize(0.03);
      eff_sfqta_ltx_title.DrawLatexNDC(0.2,0.825, Form("weird eff qtA vs Photon ID SF - Q%i", mass_number ) );
      eff_sf_qta_canv->SaveAs( Form( "./eff_qta_sf_Q%i_P%i.png", mass_number, p_ind ) );
      
      TCanvas * eff_sf_photon_canv = new TCanvas( Form( "eff_sf_photon_canv_Q%i_P%i", mass_number, p_ind ), "", 200, 200, 1000, 1000);
      TH2F * eff_photon_sf = new TH2F( Form("eff_photon_sf_Q%i", mass_number), "", photon_bins, photon_min, photon_max,
                                          sf_bins, sf_min, sf_max );
      TH2F * re_photon_sf = new TH2F( Form("re_photon_sf_Q%i", mass_number), "", photon_bins, photon_min, photon_max,
                                          sf_bins, sf_min, sf_max );
      TH2F * tr_photon_sf = new TH2F( Form("tr_photon_sf_Q%i", mass_number), "", photon_bins, photon_min, photon_max,
                                          sf_bins, sf_min, sf_max );
      reco_tree->Draw( Form("photon_id_sf:PhotonPt/1000.0>>re_photon_sf_Q%i", mass_number), full_cut.c_str(), "goff" );
      trth_tree->Draw( Form("t_photon_id_sf:PhotonPt/1000.0>>tr_photon_sf_Q%i", mass_number), full_cut.c_str(), "goff" );
      eff_photon_sf->Divide( re_photon_sf, tr_photon_sf, 1.0, 1.0, "B" );
      eff_sf_photon_canv->Divide( 1 );
      eff_sf_photon_canv->cd( 1 );
      eff_photon_sf->SetMinimum(0.0);
      eff_photon_sf->SetMaximum(1.2);
      eff_photon_sf->Draw("COLZ");
      eff_photon_sf->SetMarkerStyle(21);
      eff_photon_sf->SetLineColor(2);
      eff_photon_sf->GetYaxis()->SetTitle( Form( "%s /%.2f %s", sf_var.c_str(), sf_width, sf_unit.c_str() ) );
      eff_photon_sf->GetXaxis()->SetTitle( Form( "%s /%.2f %s", photon_var.c_str(), photon_width, photon_unit.c_str() ) );
      eff_photon_sf->GetYaxis()->SetLabelSize(0.035);
      eff_photon_sf->GetYaxis()->SetTitleSize(0.035);
      eff_photon_sf->GetXaxis()->SetLabelSize(0.035);
      eff_photon_sf->GetXaxis()->SetTitleSize(0.035);
      eff_photon_sf->GetZaxis()->SetMaxDigits(3);
      eff_photon_sf->GetZaxis()->SetTitleSize(0.035);
      eff_photon_sf->GetZaxis()->SetLabelSize(0.035);
      eff_photon_sf->GetZaxis()->ChangeLabel(7, -1., -1., -1, -1, -1, ">=1.2" );
      gPad->Update();
      TLatex eff_sfphoton_ltx_title;
      eff_sfphoton_ltx_title.SetTextSize(0.03);
      eff_sfphoton_ltx_title.DrawLatexNDC(0.2,0.825, Form("weird eff Photon Pt vs Photon ID SF - Q%i", mass_number ) );
      eff_sf_photon_canv->SaveAs( Form( "./eff_photon_sf_Q%i_P%i.png", mass_number, p_ind ) );

      TH1F * re_qta = new TH1F( Form("re_qta_Q%i", mass_number), "", qta_bins, qta_min, qta_max );
      TH1F * tr_qta = new TH1F( Form("tr_qta_Q%i", mass_number), "", qta_bins, qta_min, qta_max );
      TH1F * ef_qta = new TH1F( Form("ef_qta_Q%i", mass_number), "", qta_bins, qta_min, qta_max );
      reco_tree->Draw( Form("qtA>>re_qta_Q%i", mass_number), full_cut.c_str(), "goff" );
      trth_tree->Draw( Form("qtA>>tr_qta_Q%i", mass_number), full_cut.c_str(), "goff" );
      ef_qta->Divide( re_qta, tr_qta, 1.0, 1.0, "B" );
      TCanvas * qta_eff_canv = new TCanvas( Form( "qta_eff_canv_Q%i_P%i", mass_number, p_ind ), "", 200, 200, 1000, 1000);
      ef_qta->Draw("E1");
      ef_qta->Draw("HIST SAME");
      ef_qta->SetMarkerStyle(21);
      ef_qta->SetLineColor(1);
      ef_qta->GetYaxis()->SetRangeUser(0,(double) std::ceil(ef_qta->GetMaximum()*1.3));
      ef_qta->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", qta_width, qta_unit.c_str()));
      ef_qta->GetXaxis()->SetTitle( Form("%s (%s)", qta_var.c_str(), qta_unit.c_str()));
      ef_qta->GetYaxis()->SetLabelSize(0.035);
      ef_qta->GetYaxis()->SetTitleSize(0.035);
      ef_qta->GetXaxis()->SetLabelSize(0.035);
      ef_qta->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      TLatex ef_qta_pad_ltx;
      ef_qta_pad_ltx.SetTextSize( 0.03 );
      ef_qta_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency, Q%i, Photon %i", qta_var.c_str(), mass_number, p_ind ) );
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      qta_eff_canv->SaveAs( Form( "./qta_eff_Q%i_P%i.png", mass_number, p_ind ) );

      TH1F * re_jpsi = new TH1F( Form("re_jpsi_Q%i", mass_number), "", jpsi_bins, jpsi_min, jpsi_max );
      TH1F * tr_jpsi = new TH1F( Form("tr_jpsi_Q%i", mass_number), "", jpsi_bins, jpsi_min, jpsi_max );
      TH1F * ef_jpsi = new TH1F( Form("ef_jpsi_Q%i", mass_number), "", jpsi_bins, jpsi_min, jpsi_max );
      reco_tree->Draw( Form("DiMuonPt/1000.0>>re_jpsi_Q%i", mass_number), full_cut.c_str(), "goff" );
      trth_tree->Draw( Form("DiMuonPt/1000.0>>tr_jpsi_Q%i", mass_number), full_cut.c_str(), "goff" );
      ef_jpsi->Divide( re_jpsi, tr_jpsi, 1.0, 1.0, "B" );
      TCanvas * jpsi_eff_canv = new TCanvas( Form( "jpsi_eff_canv_Q%i_P%i", mass_number, p_ind ), "", 200, 200, 1000, 1000);
      ef_jpsi->Draw("E1");
      ef_jpsi->Draw("HIST SAME");
      ef_jpsi->SetMarkerStyle(21);
      ef_jpsi->SetLineColor(1);
      ef_jpsi->GetYaxis()->SetRangeUser(0, (double) std::ceil(ef_jpsi->GetMaximum()*1.3) );
      ef_jpsi->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", jpsi_width, jpsi_unit.c_str()));
      ef_jpsi->GetXaxis()->SetTitle( Form("%s (%s)", jpsi_var.c_str(), jpsi_unit.c_str()));
      ef_jpsi->GetYaxis()->SetLabelSize(0.035);
      ef_jpsi->GetYaxis()->SetTitleSize(0.035);
      ef_jpsi->GetXaxis()->SetLabelSize(0.035);
      ef_jpsi->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      TLatex ef_jpsi_pad_ltx;
      ef_jpsi_pad_ltx.SetTextSize( 0.03 );
      ef_jpsi_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency, Q%i, Photon %i", jpsi_var.c_str(), mass_number, p_ind ) );
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      jpsi_eff_canv->SaveAs( Form( "./jpsi_eff_Q%i_P%i.png", mass_number, p_ind ) );

      
      TH1F * re_phot_eta = new TH1F( Form("re_phot_eta_Q%i", mass_number), "", phot_eta_bins, phot_eta_min, phot_eta_max);
      TH1F * tr_phot_eta = new TH1F( Form("tr_phot_eta_Q%i", mass_number), "", phot_eta_bins, phot_eta_min, phot_eta_max);  
      TH1F * ef_phot_eta = new TH1F( Form("ef_phot_eta_Q%i", mass_number), "", phot_eta_bins, phot_eta_min, phot_eta_max);
      reco_tree->Draw( Form("abs(Phot_Eta)>>re_phot_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      trth_tree->Draw( Form("abs(Phot_Eta)>>tr_phot_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      ef_phot_eta->Divide( re_phot_eta, tr_phot_eta, 1.0, 1.0, "B" );
      TCanvas * phot_eta_eff_canv = new TCanvas( Form( "phot_eta_eff_canv_Q%i_P%i", mass_number, p_ind ), "", 200, 200, 1000, 1000);
      ef_phot_eta->Draw("E1");
      ef_phot_eta->Draw("HIST SAME");
      ef_phot_eta->SetMarkerStyle(21);
      ef_phot_eta->SetLineColor(1);
      ef_phot_eta->GetYaxis()->SetRangeUser(0, (double) std::ceil(ef_phot_eta->GetMaximum()*1.3) );
      ef_phot_eta->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", phot_eta_width, phot_eta_unit.c_str()));
      ef_phot_eta->GetXaxis()->SetTitle( Form("%s (%s)", phot_eta_var.c_str(), phot_eta_unit.c_str()));
      ef_phot_eta->GetYaxis()->SetLabelSize(0.035);
      ef_phot_eta->GetYaxis()->SetTitleSize(0.035);
      ef_phot_eta->GetXaxis()->SetLabelSize(0.035);
      ef_phot_eta->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      TLatex ef_phot_eta_pad_ltx;
      ef_phot_eta_pad_ltx.SetTextSize( 0.03 );
      ef_phot_eta_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency, Q%i, Photon %i", phot_eta_var.c_str(), mass_number, p_ind ) );
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      phot_eta_eff_canv->SaveAs( Form( "./phot_eta_eff_Q%i_P%i.png", mass_number, p_ind ) );

      TH1F * re_jpsi_eta = new TH1F( Form("re_jpsi_eta_Q%i", mass_number), "", jpsi_eta_bins, jpsi_eta_min, jpsi_eta_max); 
      TH1F * tr_jpsi_eta = new TH1F( Form("tr_jpsi_eta_Q%i", mass_number), "", jpsi_eta_bins, jpsi_eta_min,jpsi_eta_max);
      TH1F * ef_jpsi_eta = new TH1F( Form("ef_jpsi_eta_Q%i", mass_number), "",jpsi_eta_bins,jpsi_eta_min,jpsi_eta_max);
      reco_tree->Draw( Form("abs(JPsi_Eta)>>re_jpsi_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      trth_tree->Draw( Form("abs(JPsi_Eta)>>tr_jpsi_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      ef_jpsi_eta->Divide( re_jpsi_eta, tr_jpsi_eta, 1.0, 1.0, "B" );
      TCanvas * jpsi_eta_eff_canv = new TCanvas( Form( "jpsi_eta_eff_canv_Q%i_P%i", mass_number, p_ind), "", 200, 200, 1000, 1000);
      ef_jpsi_eta->Draw("E1");
      ef_jpsi_eta->Draw("HIST SAME");
      ef_jpsi_eta->SetMarkerStyle(21);
      ef_jpsi_eta->SetLineColor(1);
      ef_jpsi_eta->GetYaxis()->SetRangeUser(0, (double) std::ceil(ef_jpsi_eta->GetMaximum()*1.3) );
      ef_jpsi_eta->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", jpsi_eta_width, jpsi_eta_unit.c_str()));
      ef_jpsi_eta->GetXaxis()->SetTitle( Form("%s (%s)", jpsi_eta_var.c_str(), jpsi_eta_unit.c_str()));
      ef_jpsi_eta->GetYaxis()->SetLabelSize(0.035);
      ef_jpsi_eta->GetYaxis()->SetTitleSize(0.035);
      ef_jpsi_eta->GetXaxis()->SetLabelSize(0.035);
      ef_jpsi_eta->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      TLatex ef_jpsi_eta_pad_ltx;
      ef_jpsi_eta_pad_ltx.SetTextSize( 0.03 );
      ef_jpsi_eta_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency, Q%i, Photon %i", jpsi_eta_var.c_str(), mass_number, p_ind ) );
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      jpsi_eta_eff_canv->SaveAs( Form( "./jpsi_eta_eff_Q%i_P%i.png", mass_number, p_ind ) );


      TH1F * sf = new TH1F( Form("sf_Q%i", mass_number), "", sf_bins, sf_min, sf_max);
      sf_tree->Draw( Form("photon_id_sf>>sf_Q%i", mass_number), full_cut.c_str(), "goff" );
      TCanvas * sf_canv = new TCanvas( Form( "sf_canv_Q%i_P%i", mass_number, p_ind), "", 200, 200, 1000, 1000);
      sf->Draw("E1");
      sf->Draw("HIST SAME");
      sf->SetMarkerStyle(21);
      sf->SetLineColor(1);
      sf->GetYaxis()->SetRangeUser(0, (double) std::ceil(sf->GetMaximum()*1.3) );
      sf->GetYaxis()->SetTitle( Form("SF /%.3f %s", sf_width, sf_unit.c_str()));
      sf->GetXaxis()->SetTitle( Form("%s (%s)", sf_var.c_str(), sf_unit.c_str()));
      sf->GetYaxis()->SetLabelSize(0.035);
      sf->GetYaxis()->SetTitleSize(0.035);
      sf->GetXaxis()->SetLabelSize(0.035);
      sf->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      TLatex sf_pad_ltx;
      sf_pad_ltx.SetTextSize( 0.03 );
      sf_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s, Q%i, Photon %i", sf_var.c_str(), mass_number, p_ind ) );
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      sf_canv->SaveAs( Form( "./sf_Q%i_P%i.png", mass_number, p_ind ) );

      TH1F * jpsi_eta = new TH1F( Form("jpsi_eta_Q%i", mass_number), "", jpsi_eta_bins, jpsi_eta_min, jpsi_eta_max); 
      reco_tree->Draw( Form("abs(JPsi_Eta)>>jpsi_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      TCanvas * jpsi_eta_canv = new TCanvas( Form( "jpsi_eta_eff_canv_Q%i_P%i", mass_number, p_ind), "", 200, 200, 1000, 1000);
      jpsi_eta->Draw("E1");
      jpsi_eta->Draw("HIST SAME");
      jpsi_eta->SetMarkerStyle(21);
      jpsi_eta->SetLineColor(1);
      jpsi_eta->GetYaxis()->SetRangeUser(0, jpsi_eta->GetMaximum()*1.3 );
      jpsi_eta->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", jpsi_eta_width, jpsi_eta_unit.c_str()));
      jpsi_eta->GetXaxis()->SetTitle( Form("%s (%s)", jpsi_eta_var.c_str(), jpsi_eta_unit.c_str()));
      jpsi_eta->GetYaxis()->SetLabelSize(0.035);
      jpsi_eta->GetYaxis()->SetTitleSize(0.035);
      jpsi_eta->GetXaxis()->SetLabelSize(0.035);
      jpsi_eta->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      TLatex jpsi_eta_pad_ltx;
      jpsi_eta_pad_ltx.SetTextSize( 0.03 );
      jpsi_eta_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s, Q%i, Photon %i", jpsi_eta_var.c_str(), mass_number, p_ind ) );
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      jpsi_eta_canv->SaveAs( Form( "./jpsi_eta_Q%i_P%i.png", mass_number, p_ind ) );

      TH1F * jpsi_pt = new TH1F( Form("jpsi_eta_Q%i", mass_number), "", jpsi_bins, jpsi_min, jpsi_max); 
      reco_tree->Draw( Form("DiMuonPt/1000.0>>jpsi_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      TCanvas * jpsi_pt_canv = new TCanvas( Form( "jpsi_eta_eff_canv_Q%i_P%i", mass_number, p_ind), "", 200, 200, 1000, 1000);
      jpsi_pt->Draw("E1");
      jpsi_pt->Draw("HIST SAME");
      jpsi_pt->SetMarkerStyle(21);
      jpsi_pt->SetLineColor(1);
      //jpsi_pt->GetYaxis()->SetRangeUser(0, (double) std::ceil(jpsi_eta->GetMaximum()*1.3) );
      jpsi_pt->GetYaxis()->SetRangeUser(0, jpsi_pt->GetMaximum()*1.3 );
      jpsi_pt->GetYaxis()->SetTitle( Form("%s/%.3f %s", jpsi_var.c_str(), jpsi_width, jpsi_unit.c_str()));
      jpsi_pt->GetXaxis()->SetTitle( Form("%s (%s)", jpsi_var.c_str(), jpsi_unit.c_str()));
      jpsi_pt->GetYaxis()->SetLabelSize(0.035);
      jpsi_pt->GetYaxis()->SetTitleSize(0.035);
      jpsi_pt->GetXaxis()->SetLabelSize(0.035);
      jpsi_pt->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      TLatex jpsi_pt_pad_ltx;
      jpsi_pt_pad_ltx.SetTextSize( 0.03 );
      jpsi_pt_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s, Q%i, Photon %i", jpsi_var.c_str(), mass_number, p_ind ) );
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      jpsi_pt_canv->SaveAs( Form( "./jpsi_pt_Q%i_P%i.png", mass_number, p_ind ) );

      //TH1F * sf_phot = new TH1F( Form("sf_phot_Q%i", mass_number), "",sf_bins,sf_min, sf_max);
      //sf_tree->Draw( Form("photon_id_sf>>sf_phot_Q%i", mass_number), full_cut.c_str(), "goff" );
      //TCanvas * phot_eff_canv = new TCanvas( Form( "phot_eff_canv_Q%i_P%i", mass_number, p_ind), "", 200, 200, 1000, 1000);
      //sf_phot->Draw("E1");
      //sf_phot->Draw("HIST SAME");
      //sf_phot->SetMarkerStyle(21);
      //sf_phot->SetLineColor(1);
      //sf_phot->GetYaxis()->SetRangeUser(0, (double) std::ceil(ef_phot->GetMaximum()*1.3) );
      //sf_phot->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", phot_width, phot_unit.c_str()));
      //sf_phot->GetXaxis()->SetTitle( Form("%s (%s)", phot_var.c_str(), phot_unit.c_str()));
      //sf_phot->GetYaxis()->SetLabelSize(0.035);
      //sf_phot->GetYaxis()->SetTitleSize(0.035);
      //sf_phot->GetXaxis()->SetLabelSize(0.035);
      //sf_phot->GetXaxis()->SetTitleSize(0.035);
      //gPad->Update();
      //TLatex ef_phot_pad_ltx;
      //ef_phot_pad_ltx.SetTextSize( 0.03 );
      //ef_phot_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency, Q%i, Photon %i", phot_var.c_str(), mass_number, p_ind ) );
      //ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      //wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      //phot_eff_canv->SaveAs( Form( "./phot_eff_Q%i_P%i.png", mass_number, p_ind ) );

      //TH1F * sf_jpsi_eta = new TH1F( Form("ef_jpsi_eta_Q%i", mass_number), "",jpsi_eta_bins,jpsi_eta_min,jpsi_eta_max);
      //reco_tree->Draw( Form("abs(JPsi_Eta)>>re_jpsi_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      //trth_tree->Draw( Form("abs(JPsi_Eta)>>tr_jpsi_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      //ef_jpsi_eta->Divide( re_jpsi_eta, tr_jpsi_eta, 1.0, 1.0, "B" );
      //TCanvas * jpsi_eta_eff_canv = new TCanvas( Form( "jpsi_eta_eff_canv_Q%i_P%i", mass_number, p_ind), "", 200, 200, 1000, 1000);
      //ef_jpsi_eta->Draw("E1");
      //ef_jpsi_eta->Draw("HIST SAME");
      //ef_jpsi_eta->SetMarkerStyle(21);
      //ef_jpsi_eta->SetLineColor(1);
      //ef_jpsi_eta->GetYaxis()->SetRangeUser(0, (double) std::ceil(ef_jpsi_eta->GetMaximum()*1.3) );
      //ef_jpsi_eta->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", jpsi_eta_width, jpsi_eta_unit.c_str()));
      //ef_jpsi_eta->GetXaxis()->SetTitle( Form("%s (%s)", jpsi_eta_var.c_str(), jpsi_eta_unit.c_str()));
      //ef_jpsi_eta->GetYaxis()->SetLabelSize(0.035);
      //ef_jpsi_eta->GetYaxis()->SetTitleSize(0.035);
      //ef_jpsi_eta->GetXaxis()->SetLabelSize(0.035);
      //ef_jpsi_eta->GetXaxis()->SetTitleSize(0.035);
      //gPad->Update();
      //TLatex ef_jpsi_eta_pad_ltx;
      //ef_jpsi_eta_pad_ltx.SetTextSize( 0.03 );
      //ef_jpsi_eta_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency, Q%i, Photon %i", jpsi_eta_var.c_str(), mass_number, p_ind ) );
      //ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      //wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      //jpsi_eta_eff_canv->SaveAs( Form( "./jpsi_eta_eff_Q%i_P%i.png", mass_number, p_ind ) );

      //TH1F * sf_phot_eta = new TH1F( Form("ef_phot_eta_Q%i", mass_number), "",phot_eta_bins,phot_eta_min,phot_eta_max);
      //reco_tree->Draw( Form("abs(JPsi_Eta)>>re_phot_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      //trth_tree->Draw( Form("abs(JPsi_Eta)>>tr_phot_eta_Q%i", mass_number), full_cut.c_str(), "goff" );
      //ef_phot_eta->Divide( re_phot_eta, tr_phot_eta, 1.0, 1.0, "B" );
      //TCanvas * phot_eta_eff_canv = new TCanvas( Form( "phot_eta_eff_canv_Q%i_P%i", mass_number, p_ind), "", 200, 200, 1000, 1000);
      //ef_phot_eta->Draw("E1");
      //ef_phot_eta->Draw("HIST SAME");
      //ef_phot_eta->SetMarkerStyle(21);
      //ef_phot_eta->SetLineColor(1);
      //ef_phot_eta->GetYaxis()->SetRangeUser(0, (double) std::ceil(ef_phot_eta->GetMaximum()*1.3) );
      //ef_phot_eta->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", phot_eta_width, phot_eta_unit.c_str()));
      //ef_phot_eta->GetXaxis()->SetTitle( Form("%s (%s)", phot_eta_var.c_str(), phot_eta_unit.c_str()));
      //ef_phot_eta->GetYaxis()->SetLabelSize(0.035);
      //ef_phot_eta->GetYaxis()->SetTitleSize(0.035);
      //ef_phot_eta->GetXaxis()->SetLabelSize(0.035);
      //ef_phot_eta->GetXaxis()->SetTitleSize(0.035);
      //gPad->Update();
      //TLatex ef_phot_eta_pad_ltx;
      //ef_phot_eta_pad_ltx.SetTextSize( 0.03 );
      //ef_phot_eta_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency, Q%i, Photon %i", phot_eta_var.c_str(), mass_number, p_ind ) );
      //ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      //wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
      //phot_eta_eff_canv->SaveAs( Form( "./phot_eta_eff_Q%i_P%i.png", mass_number, p_ind ) );

    }
  }

  //for ( int mass_number : mass_ranges ){
  //    
  //  std::string lambda_cut = lambda_cuts[mass_number];
  //  TCanvas 

  //  for ( int p_ind = 0; p_ind <= 10; p_ind++ ){
  //    

  //    std::string photon_cut = photon_cuts[p_ind];
  //    std::string full_cut = photon_cut + "&&" + lambda_cut;


  //  }
  //}
}




