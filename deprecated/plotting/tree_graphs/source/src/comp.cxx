#include <comp.hxx>


void comp( std::string files, std::string var, std::string spec, std::string cond, std::string type, std::string unique ){

  prep_style();

  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( "/home/atlas/dhagan/libs/configs/selection_bounds.txt" );

  std::vector< std::string > mass_bins = { "Q0", "Q12" };

  std::vector< std::string > uni_vec, file_vec, var_vec, spec_vec, cond_vec;
  split_strings( uni_vec, unique, ":" );
  split_strings( file_vec, files, "#" );
  split_strings( var_vec, var, ":" );



  std::string unique_str =  "comp_" + uni_vec.at(0) + "-" + uni_vec.at(1);
  std::string base_unique = uni_vec.at(0);
  std::string comp_unique = uni_vec.at(1);
  std::string title_cond{ "" };

  bool log{ cond.find("log") != std::string::npos };
  bool norm{ cond.find("norm") != std::string::npos };
  
  if ( log ){
    std::cout << "logarithm applied" << std::endl;
    title_cond += "logarithm ";
  }
  if ( norm ){ 
    std::cout << "normalisation applied" << std::endl;
    title_cond += "normalised ";
  }

  std::vector< std::string > base_file_vec, comp_file_vec;

  split_strings( base_file_vec, file_vec.at(0), ":" );
  split_strings( comp_file_vec, file_vec.at(1), ":" );

  TFile * file_base = new TFile( base_file_vec.at(0).c_str(), "READ" );
  TFile * file_comp = new TFile( comp_file_vec.at(0).c_str(), "READ" );

  TTree * tree_base = (TTree*) file_base->Get( base_file_vec.at(1).c_str() );
  TTree * tree_comp = (TTree*) file_comp->Get( comp_file_vec.at(1).c_str() );

  std::vector<std::string> spec_cuts, spec_strs;
  if ( !spec.empty() ){
    split_strings( spec_vec, spec, ":" );
    for ( std::string current_spec : spec_vec ){
      bound spec_bound = selections->get_bound( current_spec );
      spec_cuts.push_back( spec_bound.get_cut() );
      spec_strs.push_back( Form("%s-0", current_spec.c_str()) );
      std::vector<std::string> cut_vec = spec_bound.get_cut_series();
      std::vector<std::string> str_vec = spec_bound.get_series_names();
      spec_cuts.insert( spec_cuts.end(), cut_vec.begin(), cut_vec.end() ); 
      spec_strs.insert( spec_strs.end(), str_vec.begin(), str_vec.end() ); 
    }
  } else {
    spec_cuts.push_back("");
    spec_strs.push_back("");
  }

  for ( std::string current_var : var_vec ){

    bound var_bound = selections->get_bound( current_var );
    int bins =  var_bound.get_bins();
    double min = var_bound.get_min();
    double max = var_bound.get_max();

    std::vector<float> base_style = { 1, 0, 0, 0, 1, kRed+1, 1.0, 1.0, 0, 0, 0 };
    std::vector<float> comp_style = { 1, 0, 0, 0, 2, kBlue+1, 1.0, 1.0, 0, 0, 0 };
    gStyle->SetOptStat( "rmen" );

    for ( int cut_idx = 0; cut_idx < (int) spec_cuts.size(); cut_idx++ ){
      
      for ( std::string & mass : mass_bins ){
          
        bound mass_bound = selections->get_bound( mass );


        std::string cut;
        if ( !spec_cuts[cut_idx].empty() ){
          cut = spec_cuts[cut_idx] + "&&" + mass_bound.get_cut();
        } else {
          cut = mass_bound.get_cut();
        }


        TH1F * base_hist  = new TH1F( Form( "%s", base_unique.c_str() ), "base", bins ,min, max );
        TH1F * comp_hist  = new TH1F( Form( "%s", comp_unique.c_str() ), "comp", bins, min, max );

        tree_base->Draw( Form("%s>>%s", current_var.c_str(), base_unique.c_str() ), cut.c_str(), "goff" ); 
        tree_comp->Draw( Form("%s>>%s", current_var.c_str(), comp_unique.c_str() ), cut.c_str(), "goff" );


        TCanvas * canv = new TCanvas( "c", "", 300, 300, 1000, 1000 );

        double draw_min = 0;
        if ( log ){ 
          draw_min = 1;
          canv->SetLogy();  
        }
        if ( norm ){
          base_hist->Scale( 1.0/base_hist->Integral() );
          comp_hist->Scale( 1.0/comp_hist->Integral() );
        }

        TPad * current_pad = (TPad*) canv->cd(1);
        base_hist->Draw("HIST E1");
        comp_hist->Draw("SAMES HIST E1");
        style_hist( base_hist, base_style );
        style_hist( comp_hist, comp_style );
        hist_prep_axes( base_hist );
        base_hist->GetYaxis()->SetRangeUser( draw_min, std::max(base_hist->GetMaximum(), comp_hist->GetMaximum())*(1.5 +
                                            log*8.5) );
        set_axis_labels( base_hist, var_bound.get_x_str(), Form( "Yield%s", var_bound.get_y_str().c_str() ) );
        TLegend * legend = below_logo_legend();
        legend->AddEntry(base_hist, Form( "%s", uni_vec.at(0).c_str() ), "LP");
        legend->AddEntry(comp_hist, Form( "%s", uni_vec.at(1).c_str() ), "LP");
        legend->Draw();
        std::string pad_title{ Form( "%s & %s - %s %s %s", uni_vec.at(0).c_str(), uni_vec.at(1).c_str(), 
                                     mass.c_str() , spec_strs.at( cut_idx ).c_str(), title_cond.c_str() ) };
        add_pad_title( current_pad, pad_title, !norm );
        add_atlas_decorations( current_pad, true, false );
        TPaveStats * base_stats = make_stats( base_hist, true, false );
        base_stats->Draw();
        TPaveStats * comp_stats = make_stats( comp_hist, true, true );
        comp_stats->Draw();
        
        std::string output_str = std::string( Form( "%s_%s_%s_%s_%s", current_var.c_str(), type.c_str(), uni_vec.at(0).c_str(),
            uni_vec.at(1).c_str(), mass.c_str() ) );
        if ( spec_cuts.size() > 1 ){ output_str += "_" + spec_strs.at(cut_idx); }
        if ( log ){ output_str += "_Clog"; }
        if ( norm ){ output_str += "_Cnorm"; }
        output_str += ".png";
        canv->SaveAs( output_str.c_str() );
        delete canv;
        delete base_hist;
        delete comp_hist;

      }
    }
  }
}


//std::map< std::string, std::vector<double> > bins;
  //bins["qtA"]                     =  std::vector<double>{15, -10, 20};
  //bins["BDT"]                     =  std::vector<double>{20, -1.0, 1.0};
  //bins["abs(qtB)"]                =  std::vector<double>{30, 0, 15};
  //bins["qtB"]                     =  std::vector<double>{30, -15, 15};
  //bins["abs(Phi)"]                =  std::vector<double>{40, 0, M_PI};
  //bins["DPhi"]                    =  std::vector<double>{30, 0, M_PI};
  //bins["DiMuonPt"]                =  std::vector<double>{30, 5, 30};
  //bins["PhotonPt"]                =  std::vector<double>{30, 5, 25};
  //bins["Lambda"]                  =  std::vector<double>{50, 0, 200};
  //bins["DeltaZ0"]                 =  std::vector<double>{60, -4, 4};
  //bins["DiMuonMass"]              =  std::vector<double>{50, 2700, 3500};
  //bins["DiMuonTau"]               =  std::vector<double>{50, -5, 15};


//if ( !range.empty() ){
  //  std::vector< std::string > range_vec;
  //  split_strings( range_vec, range, "#" );
  //  for ( std::string range_str : range_vec ){
  //    std::vector< std::string > name_range_pair, bin_vec;
  //    split_strings( name_range_pair, range_str, "_" );
  //    split_strings( bin_vec, name_range_pair.at(1), "," );
  //    std::map< std::string, std::vector<double> >::iterator bin_map_itr = bins.find( name_range_pair.at( 0 ).c_str() );
  //    if ( bin_map_itr != bins.end() ){
  //      bin_map_itr->second = { std::stod(bin_vec.at(0)), std::stod(bin_vec.at(1)), std::stod(bin_vec.at(2)) };
  //    } else {
  //      bins[ name_range_pair.at(0).c_str() ] = std::vector<double>{ std::stod(bin_vec.at(0)), std::stod(bin_vec.at(1)),
  //        std::stod(bin_vec.at(2)) };
  //    }
  //  }  
  //}


