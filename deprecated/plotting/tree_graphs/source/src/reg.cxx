#include <comp.hxx>


void reg( std::string files, std::string range, std::string var, std::string spec, std::string cond, std::string type, std::string unique ){

  //SetAtlasStyle();
  gROOT->SetBatch( true );
  gStyle->SetOptFit( 0 );
  gStyle->SetEndErrorSize( 8 );

  gStyle->SetPadTopMargin( 0.19 );
  gStyle->SetPadRightMargin( 0.16 );
  gStyle->SetPadBottomMargin( 0.16 );
  gStyle->SetPadLeftMargin( 0.16 );

  std::map< std::string, std::vector<double> > bins;
  bins["qtA"]                     =  std::vector<double>{15, -10, 20};
  bins["BDT"]                     =  std::vector<double>{20, -1.0, 1.0};
  bins["abs(qtB)"]                =  std::vector<double>{30, 0, 15};
  bins["qtB"]                     =  std::vector<double>{30, -15, 15};
  bins["abs(Phi)"]                =  std::vector<double>{40, 0, M_PI};
  bins["DPhi"]                    =  std::vector<double>{30, 0, M_PI};
  bins["DiMuonPt"]                =  std::vector<double>{30, 5, 30};
  bins["PhotonPt"]                =  std::vector<double>{30, 5, 25};
  bins["DeltaZ0"]                 =  std::vector<double>{60, -4, 4};
  bins["DiMuonMass"]              =  std::vector<double>{50, 2700, 3500};
  bins["DiMuonTau"]               =  std::vector<double>{50, -5, 15};
  bins["ActIpX"]                  =  std::vector<double>{50, 0, 50};

  if ( !range.empty() ){
    std::vector< std::string > range_vec;
    split_strings( range_vec, range, "#" );
    for ( std::string range_str : range_vec ){
      std::vector< std::string > name_range_pair, bin_vec;
      split_strings( name_range_pair, range_str, "_" );
      split_strings( bin_vec, name_range_pair.at(1), "," );
      std::map< std::string, std::vector<double> >::iterator bin_map_itr = bins.find( name_range_pair.at( 0 ).c_str() );
      if ( bin_map_itr != bins.end() ){
        bin_map_itr->second = { std::stod(bin_vec.at(0)), std::stod(bin_vec.at(1)), std::stod(bin_vec.at(2)) };
      } else {
        bins[ name_range_pair.at(0).c_str() ] = std::vector<double>{ std::stod(bin_vec.at(0)), std::stod(bin_vec.at(1)),
          std::stod(bin_vec.at(2)) };
      }
    }  
  }

  std::map<std::string,std::string> units;
  units["qtA"]                =  "(GeV)";
  units["BDT"]                =  "(Value)";
  units["abs(qtB)"]           =  "(GeV)";
  units["qtB"]                =  "(GeV)";
  units["abs(Phi)"]           =  "(rad)";
  units["DPhi"]               =  "(rad)";
  units["DiMuonPt"]           =  "(GeV)";
  units["PhotonPt"]           =  "(GeV)";

  std::vector< int > mass_bins = { 0, 3, 4, 5, 12 };
  std::map< int, std::string > mass_ranges;
  mass_ranges[0]             = std::string("Lambda>0&&Lambda<200");
  mass_ranges[3]             = std::string("Lambda>25&&Lambda<50");
  mass_ranges[4]             = std::string("Lambda>50&&Lambda<100");
  mass_ranges[5]             = std::string("Lambda>100&&Lambda<200");
  mass_ranges[12]            = std::string("Lambda>25&&Lambda<200");

  std::vector< std::string > var_vec;
  split_strings( var_vec, var, ":" );
  std::string title_cond{ "" };
  bool log{ cond.find("log") != std::string::npos };
  bool norm{ cond.find("norm") != std::string::npos };
  
  if ( log ){
    std::cout << "logarithm applied" << std::endl;
    title_cond += "logarithm ";
  }
  if ( norm ){ 
    std::cout << "normalisation applied" << std::endl;
    title_cond += "normalised ";
  }



  std::vector< std::string > file_vec;
  split_strings( file_vec, files, ":" );

  TFile * file_base = new TFile( file_vec.at(0).c_str(), "READ" );
  TTree * tree_base = (TTree*) file_base->Get( file_vec.at(1).c_str() );

  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont( 72 ); ATLAS.SetTextColor( 1 );
  TLatex wip; wip.SetNDC();
  wip.SetTextFont( 42 ); wip.SetTextSize( 0.038 );
  wip.SetTextColor( 1 );


  for ( std::string current_var : var_vec ){

    std::vector<double> bin_vec = bins[current_var]; 
    int bin_count   = bin_vec.at(0);
    double bin_min  = bin_vec.at(1);
    double bin_max  = bin_vec.at(2);
    double width    = ( bin_max - bin_min )/( (double) bin_count );
    std::string unit;
    if ( units.find(current_var) != units.end() ){
      unit = units[current_var];
    } else { unit = ""; }

    std::vector<std::string> spec_cuts, spec_strs, spec_vec;
    if ( !spec.empty() ){
      split_strings( spec_vec, spec, ":" );
      for ( std::string current_spec : spec_vec ){
        std::vector<double> spec_bins = bins[current_spec];
        int spec_count   = spec_bins.at(0);
        double spec_min  = spec_bins.at(1);
        double spec_max  = spec_bins.at(2);
        double spec_width = ( spec_max - spec_min )/( (double) spec_count );
        spec_cuts.push_back( Form("%s>%f&&%s<%f", current_spec.c_str(), spec_min, current_spec.c_str(), spec_max ) );
        spec_strs.push_back( Form("%s-0", current_spec.c_str()) );
        for ( int spec_bin = 1; spec_bin <= spec_count; spec_bin++){
          double bin_upper, bin_lower;
          bin_upper = spec_min + (spec_bin * spec_width);
          bin_lower = bin_upper - spec_width;
          spec_cuts.push_back( Form( "%s>%f&&%s<%f", current_spec.c_str(), bin_lower, current_spec.c_str(), bin_upper ) );
          spec_strs.push_back( Form( "%s-%i", current_spec.c_str(), spec_bin ) );
          std::cout << spec_cuts.back() << std::endl;
        }
      }
    } else {
      spec_cuts.push_back("");
      spec_strs.push_back("");
    }
    for ( int cut_idx = 0; cut_idx < (int) spec_cuts.size(); cut_idx++ ){
      
      for ( int mass_idx : mass_bins ){
        
        std::string cut = spec_cuts[cut_idx] + "&&" + mass_ranges[mass_idx];

        TH1F * base_hist  = new TH1F( Form( "base_Q%i_C%i", mass_idx, cut_idx), "", bin_count, bin_min, bin_max );

        tree_base->Draw( Form("%s>>base_Q%i_C%i", current_var.c_str(), mass_idx, cut_idx ), cut.c_str(), "goff" ); 
      
        TCanvas * canv = new TCanvas( "c", "", 300, 300, 1000, 1000 );

        double draw_min = 0;
        if ( log ){ 
          draw_min = 1;
          canv->SetLogy();  
        }
        if ( norm ){
          base_hist->Scale( 1.0/base_hist->Integral() );
        }

        double median = 0;
        const double prob = 0.5;
        base_hist->GetQuantiles( 1, &median, &prob  );
        std::cout << median << std::endl;



        canv->cd(1);
        base_hist->Draw("E1");
        base_hist->Draw("SAME HIST");
        base_hist->SetMarkerStyle(0);
        base_hist->SetLineColorAlpha(kRed+1,0.9);
        base_hist->GetYaxis()->SetRangeUser( draw_min, base_hist->GetMaximum()*1.3 );
        base_hist->GetYaxis()->SetTitle( Form( "%s/%.3f %s", current_var.c_str(), width, unit.c_str() ) );
        base_hist->GetXaxis()->SetTitle( Form( "%s (%s)", current_var.c_str(), unit.c_str() ) );
        base_hist->GetYaxis()->SetLabelSize(0.035);
        base_hist->GetYaxis()->SetTitleSize(0.035);
        base_hist->GetXaxis()->SetLabelSize(0.035);
        base_hist->GetXaxis()->SetTitleSize(0.035);
        base_hist->GetYaxis()->SetMaxDigits(3);
        gPad->Update();
        TLegend * legend = new TLegend(0.65,0.65,0.83,0.8);
        legend->SetBorderSize(0);
        legend->SetFillColor(0);
        legend->SetFillStyle(0);
        legend->SetTextFont(42);
        legend->SetTextSize(0.025);
        legend->AddEntry(base_hist, unique.c_str(), "LP");
        legend->Draw();
        TLatex title_ltx;
        title_ltx.SetTextSize(0.03);
        title_ltx.DrawLatexNDC(0.3,0.825,Form( "%s - Q%i %s %s", unique.c_str(), mass_idx, spec_strs.at(cut_idx).c_str(), title_cond.c_str() ));  
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");


        std::string output_str = std::string( Form( "%s_%s_%s_Q%i", current_var.c_str(), type.c_str(), unique.c_str(), mass_idx ) );
        if ( spec_cuts.size() > 1 ){ output_str += "_" + spec_strs.at(cut_idx); }
        if ( log ){ output_str += "_Clog"; }
        if ( norm ){ output_str += "_Cnorm"; }
        output_str += ".png";
        canv->SaveAs( output_str.c_str() );
        delete canv;

      }
    }
  }
}
