#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include <TF1.h>
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include "TROOT.h"
#include "TGraphErrors.h"
#include "TGaxis.h"
#include "TEfficiency.h"
#include <TStyle.h>
#include <TH2F.h>

#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <iostream>
#include <math.h>

#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasLabels.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasUtils.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasStyle.h"

void eff(const std::string filepath);
