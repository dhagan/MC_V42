#include <reco.hxx>
#include <split_string.hxx>

void reco(std::string filepath, std::string hf_path, std::string abin_vars, std::string spec_vars, std::string type, std::string slice, std::string unique){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  int mass_bins[5] = {0,3,4,5,12};
  TFile * eff_file        = new TFile(filepath.c_str(),   "READ");

  std::vector<std::string> file_vec;
  split_strings(file_vec, hf_path, ":");
  std::string file1 = file_vec.at(0);

  TFile * integral_file, *integral_file_alt{0};
  integral_file = new TFile(file1.c_str(),   "READ");

  bool split{false};
  if ( file_vec.size() == 2 ){
    split = true;
    std::string file2 = file_vec.at(1);
    integral_file_alt = new TFile(file2.c_str(),   "READ");
  }   

  TLatex ATLAS;
  ATLAS.SetNDC();
  ATLAS.SetTextFont(72);
  ATLAS.SetTextColor(1);
  TLatex wip; 
  wip.SetNDC();
  wip.SetTextFont(42);
  wip.SetTextSize(0.038);
  wip.SetTextColor(1);
  TLatex sim; 
  sim.SetNDC();
  sim.SetTextFont(42);
  sim.SetTextSize(0.038);
  sim.SetTextColor(1);

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  
  std::map<std::string,std::string> eff_div_str_pre, eff_div_str_suff, eff_str_pre, width_str;
  eff_div_str_pre["qtA"]          = "qtA_Q";
  eff_div_str_pre["qtB"]          = "qtB_Q";
  eff_div_str_pre["abs(qtB)"]          = "qtB_Q";
  eff_div_str_pre["DiMuonPt"]     = "jpsi_Q";
  eff_div_str_pre["PhotonPt"]     = "phot_Q";
  eff_div_str_pre["DPhi"]         = "dPhi_Q";
  eff_div_str_pre["Phi"]          = "Phi_Q";
  eff_div_str_suff["qtA"]         = "_div";
  eff_div_str_suff["qtB"]         = "_div";
  eff_div_str_suff["abs(qtB)"]         = "_div";
  eff_div_str_suff["DiMuonPt"]    = "_pt_div";
  eff_div_str_suff["PhotonPt"]    = "_pt_div";
  eff_div_str_suff["DPhi"]        = "_pt_div";
  eff_div_str_suff["Phi"]         = "_pt_div";
  eff_str_pre["qtA"]              = "qtA_Q";
  eff_str_pre["qtB"]              = "qtB_Q";
  eff_str_pre["abs(qtB)"]              = "qtB_Q";
  eff_str_pre["DiMuonPt"]         = "jpsi_pt_Q";
  eff_str_pre["PhotonPt"]         = "phot_pt_Q";
  eff_str_pre["DPhi"]             = "dPhi_Q";
  eff_str_pre["Phi"]              = "phi_Q";
  width_str["qtA"]                = "2.0GeV";
  width_str["qtB"]                = "1.0GeV";
  width_str["abs(qtB)"]                = "1.0GeV";
  width_str["PhotonPt"]           = "2.5GeV";
  width_str["DiMuonPt"]           = "2.5GeV";
  width_str["DPhi"]               = "1/8^{th}\\pi rad";
  width_str["Phi"]                = "1/8^{th}\\pi rad";

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DPhi"]          =  std::vector<double>{8,0,M_PI};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{10,5,30};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5,25};
  
  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["BDT"]           =  "Value";
  units["abs(qtB)"]      =  "GeV";
  units["qtB"]           =  "GeV";
  units["Phi"]           =  "rad";
  units["DPhi"]          =  "rad";
  units["DiMuonPt"]      =  "GeV";
  units["PhotonPt"]      =  "GeV";

  TF1 * fit_sg = new TF1("fit_sg"," [0]*exp( -((x-[1])^2)/(2*[2]^2))" ); 
  fit_sg->SetParName(0,"C"); fit_sg->SetParName(1,"X"); fit_sg->SetParName(2,"Sigma");
  fit_sg->SetParNames("C","X","SIG");
  fit_sg->SetParLimits(0,0,1e6);
  fit_sg->SetParLimits(2,1,10);
  fit_sg->SetLineColor(6);

  TF1 * fit_dg_cc = new TF1("fit_dg_cc"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) + [3]*exp( -((x-[1])^2)/(2*[4]^2)) " );
  fit_dg_cc->SetParNames("C1","X","SIG","C2","SIG2");
  fit_dg_cc->SetParLimits(0,0,1e6);
  fit_dg_cc->SetParLimits(2,1,10);
  fit_dg_cc->SetParLimits(3,0,1e6);
  fit_dg_cc->SetParLimits(4,1,10);
  fit_dg_cc->SetLineColor(6);
  

  TF1 * fit_cs_four = new TF1("fit_cs_four"," [0]*(1 + [1]*cos(x) + [2]*cos(2*x) + [3]*cos(3*x) + [4]*cos(4*x) )" );
  TF1 * fit_cs_six  = new TF1("fit_cs_six"," [0]*(1 + [1]*cos(x) + [2]*cos(2*x) + [3]*cos(3*x) + [4]*cos(4*x) + "
                                            "[5]*cos(5*x) + [6]*cos(6*x) )" );

  std::map<std::string,std::vector<std::string>> fit_strings;
  fit_strings["qtA"] = {"Single gaussian fit", "Double gaussian fit"};
  fit_strings["qtB"] = {"Single gaussian fit", "Double gaussian fit"};
  fit_strings["Phi"] = {"Four term fit", "six term fit"};

  std::map<std::string,std::vector<TF1*>> fit_funcs;
  fit_funcs["qtA"] = {fit_sg, fit_dg_cc};
  fit_funcs["qtB"] = {fit_sg, fit_dg_cc};
  fit_funcs["Phi"] = {fit_cs_four, fit_cs_six};
  
  
  int hf_error_bin = 0;
  std::string full_type;
  if (type.find("data") != std::string::npos){ hf_error_bin = 1; full_type = "data";}  
  if (type.find("sign") != std::string::npos){ hf_error_bin = 2; full_type = "signal";}
  if (type.find("bckg") != std::string::npos){ hf_error_bin = 3; full_type = "background";}

  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";



  for ( int & mass_index : mass_bins){
    
    char mass_char[100];        
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
    for (std::string & abin_var : vec_abin_vars){
        
      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)"; }
      if (abin_var.find("_low") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      if (abin_var.find("_high") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)    bins.at(0);
      double abin_min   = (double) bins.at(1);
      double abin_max   = (double) bins.at(2);

      std::string div_pre   = eff_div_str_pre[abin_var.c_str()];
      std::string div_suff  = eff_div_str_suff[abin_var.c_str()];
      std::string hist_pre  = eff_str_pre[abin_var.c_str()];
      std::string width     = width_str[abin_var.c_str()];
      if ( unique.find("low") != std::string::npos ){ div_pre = "Phi_low_Q"; hist_pre = "phi_low_Q"; }
      if ( unique.find("high") != std::string::npos ){ div_pre = "Phi_high_Q"; hist_pre = "phi_high_Q"; }


      for (std::string spec_var : vec_spec_vars){
        
        TH1F * ex_reco      = new TH1F(   Form("ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * ex_reco_low  = new TH1F(   Form("ex_signal_reco_low_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * ex_truth     = new TH1F(   Form("signal_truth_Q%i",mass_index),   "", abin_bins, abin_min, abin_max);
        TH1F * eff      = (TH1F*) eff_file->Get(Form("%s%i%s",      div_pre.c_str(),  mass_index, div_suff.c_str()));
        TH1F * reco     = (TH1F*) eff_file->Get(Form("reco_%s%i",   hist_pre.c_str(),  mass_index));
        TH1F * truth    = (TH1F*) eff_file->Get(Form("truth_%s%i",  hist_pre.c_str(),  mass_index));

        TH1F * eff_low{0}, * eff_high{0};
        if ( split ){
           eff_low  = (TH1F*) eff_file->Get(Form("Phi_low_Q%i_pt_div",  mass_index ));
           eff_high = (TH1F*) eff_file->Get(Form("Phi_high_Q%i_pt_div", mass_index ));
        }



        bool error_available = true;
        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
           
          char bin_hist_name[150], error_name[150];
          sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);
          TH1F * integral_hist  = (TH1F*) integral_file->Get(bin_hist_name);
          TH1F * error_hist     = (TH1F*) integral_file->Get(error_name);
  
          ex_reco->SetBinContent(abin_index, integral_hist->Integral());
          if (error_hist != NULL){ 
            ex_reco->SetBinError(abin_index,error_hist->GetBinError(hf_error_bin));
          } else {error_available = false;}
          
          if ( abin_var.find("Phi") != std::string::npos && split){
            integral_hist = (TH1F*) integral_file_alt->Get(bin_hist_name);
            error_hist = (TH1F*) integral_file_alt->Get(error_name);
            ex_reco_low->SetBinContent(abin_index, integral_hist->Integral());
            if (error_hist != NULL){ 
              ex_reco_low->SetBinError(abin_index,error_hist->GetBinError(hf_error_bin));
            } else {error_available = false;}
          }

        }
        if ( !error_available ){ ex_reco->Sumw2();}


        if ( abin_var.find("Phi") != std::string::npos && split){
          if ( !error_available ){ ex_reco_low->Sumw2();}
          TH1F * ex_reco_high = (TH1F*) ex_reco->Clone();
          ex_reco->Divide(ex_reco_low, ex_reco_high,1.0,1.0);
          TH1F * ex_truth_high = new TH1F(Form("truth_h_Q%i",mass_index),"",abin_bins,abin_min,abin_max);
          TH1F * ex_truth_low = new TH1F(Form("truth_l_Q%i",mass_index),"",abin_bins,abin_min,abin_max);
          ex_truth_high->Divide(ex_reco_high,eff_high,1.0,1.0);
          ex_truth_low->Divide(ex_reco_low,eff_low,1.0,1.0);
          ex_truth->Divide(ex_truth_low,ex_truth_high,1.0,1.0);
          delete ex_truth_high;
          delete ex_truth_low;
          eff->Divide(eff_low,eff_high,1.0,1.0);
        }

        std::string unit = units[abin_var];
        TCanvas * mass_canv;
        if (fit_strings[abin_var].size() != 0){
          mass_canv  = new TCanvas(Form("canv_Q%i",mass_index), "eff_canv",100,100,2000,2000);
          mass_canv->Divide(2,2);

        } else {
          mass_canv  = new TCanvas(Form("canv_Q%i",mass_index), "eff_canv",100,100,2000,1000);
          mass_canv->Divide(2);
        }


        mass_canv->cd(1);
        double axis_lim = std::ceil(eff->GetMaximum());
        eff->Draw("E1");
        eff->SetMarkerStyle(21);
        eff->SetLineColor(1);
        eff->GetYaxis()->SetRangeUser(0,axis_lim);
        eff->GetYaxis()->SetTitle(Form("Efficiency/ %s",width.c_str()));
        eff->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        eff->GetYaxis()->SetLabelSize(0.035);
        eff->GetYaxis()->SetTitleSize(0.035);
        eff->GetXaxis()->SetLabelSize(0.035);
        eff->GetXaxis()->SetTitleSize(0.035);
        gPad->Update();
        gPad->SetTicks(1,0);
        int pad1_pre_scale = truth->Integral()/axis_lim;
        truth->Scale(1.0/pad1_pre_scale);
        reco->Scale(1.0/pad1_pre_scale);    
        truth->SetLineWidth(0);
        truth->SetFillColorAlpha(kRed+1,0.5);
        reco->SetLineWidth(0);
        reco->SetFillColorAlpha(kBlue+1,0.5);
        truth->Draw("HIST SAME");
        reco->Draw("HIST SAME");
        TGaxis * pad1_axis = new TGaxis(abin_max,0,abin_max,axis_lim,0,pad1_pre_scale,510,"+L");
        pad1_axis->SetLabelSize(0.035);             
        pad1_axis->SetLabelFont(42);                
        pad1_axis->SetTitleSize(0.035);
        pad1_axis->SetTitleOffset(1.25);
        pad1_axis->SetMaxDigits(4);
        pad1_axis->SetTitleFont(42);                
        pad1_axis->SetTitle(Form("Entries/%s",width.c_str()));
        pad1_axis->Draw();
        TLegend * pad1_legend = new TLegend(0.65,0.65,0.83,0.8);
        pad1_legend->SetBorderSize(0);
        pad1_legend->SetFillColor(0);
        pad1_legend->SetFillStyle(0);
        pad1_legend->SetTextFont(42);
        pad1_legend->SetTextSize(0.025);
        pad1_legend->AddEntry(eff,"Efficiency","LP");
        pad1_legend->AddEntry(truth,"Truth","F");
        pad1_legend->AddEntry(reco,"Reco","F");
        pad1_legend->Draw();
        TLatex pad1_ltx;
        pad1_ltx.SetTextSize(0.03);
        pad1_ltx.DrawLatexNDC(0.2,0.825,Form("%s Efficiency - Mass Q%i",abin_var.c_str(),mass_index));  
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
        

        mass_canv->cd(2);
        eff->Draw("E1");
        eff->SetMarkerStyle(21);
        eff->SetLineColor(1);
        eff->GetYaxis()->SetTitle(Form("Efficiency/%s",width.c_str()));
        eff->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        eff->GetYaxis()->SetLabelSize(0.035);
        eff->GetYaxis()->SetTitleSize(0.035);
        eff->GetXaxis()->SetLabelSize(0.035);
        eff->GetXaxis()->SetTitleSize(0.035);
        gPad->Update();
        gPad->SetTicks(1,0);
        if ( !split ){ ex_truth->Sumw2(); ex_truth->Divide(ex_reco,eff,1.0,1.0); }
        TH1F * fit_truth_1  = (TH1F*) ex_truth->Clone(Form("truth_fit_1_Q%i",mass_index));
        TH1F * fit_truth_2  = (TH1F*) ex_truth->Clone(Form("truth_fit_1_Q%i",mass_index));
        TH1F * fit_reco     = (TH1F*) ex_reco->Clone(Form("reco_fit_1_Q%i",mass_index)); 
        int pad2_pre_scale = ex_truth->Integral()/axis_lim;
        ex_truth->Scale(1.0/pad2_pre_scale);
        ex_reco->Scale(1.0/pad2_pre_scale);    
        ex_truth->SetLineWidth(1);
        ex_truth->SetLineColorAlpha(kRed+1,1.0);
        ex_reco->SetLineWidth(1);
        ex_reco->SetLineColorAlpha(kBlue+1,1.0);
        ex_truth->Draw("E1 SAME");
        ex_reco->Draw("E1 SAME");
        ex_truth->Draw("HIST SAME");
        ex_reco->Draw("HIST SAME");
        gPad->Update();
        TGaxis * pad2_axis = new TGaxis(abin_max,0,abin_max,axis_lim,0,pad2_pre_scale,510,"+L");
        pad2_axis->SetLabelSize(0.035);
        pad2_axis->SetLabelFont(42);
        pad2_axis->SetMaxDigits(4);
        pad2_axis->SetTitleOffset(1.25);
        pad2_axis->SetTitleFont(42);
        pad2_axis->SetTitle(Form("Entries/%s",width.c_str()));
        pad2_axis->Draw();
        TLegend * pad2_legend = new TLegend(0.45,0.65,0.83,0.8);
        pad2_legend->SetBorderSize(0);
        pad2_legend->SetFillColor(0);
        pad2_legend->SetFillStyle(0);
        pad2_legend->SetTextFont(42);
        pad2_legend->SetTextSize(0.025);
        pad2_legend->AddEntry(eff,"efficiency","LP");
        pad2_legend->AddEntry(ex_truth,"Efficiency adjusted signal","LP");
        pad2_legend->AddEntry(ex_reco,"HF Extracted signal","LP");
        pad2_legend->Draw();
        TLatex pad2_ltx;
        pad2_ltx.SetTextSize(0.03);
        pad2_ltx.DrawLatexNDC(0.2,0.825,Form("%s Truth recovery - Mass Q%i - RMS - %.4f",abin_var.c_str(),mass_index,ex_truth->GetRMS()));  
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");


        if (fit_strings[abin_var].size() < 1){
          mass_canv->SaveAs(Form("./reco_Q%i_A-%s_%s.png",mass_index,abin_var.c_str(),unique.c_str()));
          continue;
        }
        mass_canv->cd(3);
        fit_truth_1->Draw("E1");
        fit_truth_1->SetLineWidth(3);
        fit_truth_1->SetLineColorAlpha(2,1.0);
        fit_truth_1->GetYaxis()->SetRangeUser(0,1.7*fit_truth_1->GetMaximum());
        fit_truth_1->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        fit_truth_1->GetYaxis()->SetMaxDigits(4);
        fit_truth_1->GetXaxis()->SetLabelSize(0.035);
        fit_truth_1->GetXaxis()->SetTitleSize(0.035);
        fit_truth_1->GetYaxis()->SetLabelSize(0.035);
        fit_truth_1->GetYaxis()->SetTitleSize(0.035);
        fit_truth_1->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        eff->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        fit_funcs[abin_var].at(0)->SetParameter("X",fit_truth_1->GetMean());
        if (std::string(fit_funcs[abin_var].at(0)->GetParName(1)).find("SIG") != std::string::npos){
          fit_funcs[abin_var].at(0)->SetParameter(1,fit_truth_1->GetStdDev());
        } 
        fit_truth_1->Fit(fit_funcs[abin_var].at(0),"MQ","",abin_min,abin_max);
        fit_funcs[abin_var].at(0)->Draw("SAME");
        fit_truth_1->Draw("SAME HIST");
        fit_reco->SetLineColorAlpha(4,0.5);
        fit_reco->SetLineStyle(2);
        fit_reco->Draw("E1 SAME");
        fit_reco->Draw("HIST SAME");
        fit_reco->SetLineColor(kBlue+1);
        gPad->Modified(); gPad->Update();
        TPaveStats * pad3_stats = (TPaveStats*) fit_truth_1->FindObject("stats");
        pad3_stats->SetX1NDC(0.55); pad3_stats->SetX2NDC(0.83);
        pad3_stats->SetY1NDC(0.65); pad3_stats->SetY2NDC(0.79);
        pad3_stats->SetTextFont(42);
        pad3_stats->SetFillStyle(0);
        TLegend * pad3_legend = new TLegend(0.2,0.58,0.6,0.68);
        pad3_legend->SetBorderSize(0);
        pad3_legend->SetFillColor(0);
        pad3_legend->SetFillStyle(0);
        pad3_legend->SetTextFont(42);
        pad3_legend->SetTextSize(0.02);
        pad3_legend->AddEntry(fit_truth_1,"Efficiency adjusted signal","LP");
        pad3_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        pad3_legend->AddEntry(fit_funcs[abin_var].at(0),fit_strings[abin_var].at(0).c_str(),"L");
        pad3_legend->Draw();
        TLatex pad3_ltx;
        pad3_ltx.SetTextSize(0.03);
        pad3_ltx.DrawLatexNDC(0.25,0.825,Form("%s Truth fit - Mass Q%i",abin_var.c_str(),mass_index));  
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        if (fit_strings[abin_var].size() < 2){
          mass_canv->SaveAs(Form("./reco_Q%i_A-%s_%s.png",mass_index,abin_var.c_str(),unique.c_str()));
          continue;
        }
        mass_canv->cd(4);
        fit_truth_2->Draw("E1");
        fit_truth_2->SetLineWidth(3);
        fit_truth_2->SetLineColorAlpha(2,1.0);
        fit_truth_2->GetYaxis()->SetRangeUser(0,1.7*fit_truth_2->GetMaximum());
        fit_truth_2->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        fit_truth_2->GetYaxis()->SetMaxDigits(4);
        fit_truth_2->GetXaxis()->SetLabelSize(0.035);
        fit_truth_2->GetXaxis()->SetTitleSize(0.035);
        fit_truth_2->GetYaxis()->SetLabelSize(0.035);
        fit_truth_2->GetYaxis()->SetTitleSize(0.035);
        fit_truth_2->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        eff->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        fit_funcs[abin_var].at(1)->SetParameter("X",fit_truth_2->GetMean());
        fit_truth_2->Fit(fit_funcs[abin_var].at(1),"MQ","",abin_min,abin_max);
        fit_funcs[abin_var].at(1)->Draw("SAME");
        fit_truth_2->Draw("SAME HIST");
        fit_reco->Draw("E1 SAME");
        fit_reco->Draw("HIST SAME");
        fit_reco->SetLineColor(kBlue+1);
        gPad->Modified(); gPad->Update();
        TPaveStats * pad4_stats = (TPaveStats*) fit_truth_2->FindObject("stats");
        pad4_stats->SetX1NDC(0.55); pad4_stats->SetX2NDC(0.83);
        pad4_stats->SetY1NDC(0.65); pad4_stats->SetY2NDC(0.79);
        pad4_stats->SetTextFont(42);
        pad4_stats->SetFillStyle(0);
        TLegend * pad4_legend = new TLegend(0.2,0.58,0.6,0.68);
        pad4_legend->SetBorderSize(0);
        pad4_legend->SetFillColor(0);
        pad4_legend->SetFillStyle(0);
        pad4_legend->SetTextFont(42);
        pad4_legend->SetTextSize(0.02);
        pad4_legend->AddEntry(fit_truth_2,"Efficiency adjusted signal","LP");
        pad4_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        pad4_legend->AddEntry(fit_funcs[abin_var].at(1),fit_strings[abin_var].at(1).c_str(),"L");
        pad4_legend->Draw();
        TLatex pad4_ltx;
        pad4_ltx.SetTextSize(0.03);
        pad4_ltx.DrawLatexNDC(0.25,0.825,Form("%s Truth fit - Mass Q%i",abin_var.c_str(),mass_index));  
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
        
        mass_canv->SaveAs(Form("./reco_Q%i_A-%s_%s.png",mass_index,abin_var.c_str(),unique.c_str()));
        
        //ex_truth_cmp->Scale(1.0/ex_truth_cmp->Integral());
        //truth_cmp->Scale(1.0/truth_cmp->Integral());
        //mass_canv->cd(3);
        //ex_truth_cmp->Draw("E1");
        //truth_cmp->Draw("E1 SAME");
        //ex_truth_cmp->Draw("HIST SAME");
        //truth_cmp->Draw("HIST SAME");
        //ex_truth_cmp->SetLineColorAlpha(kRed+1,1.0);
        //truth_cmp->SetLineColorAlpha(kRed+1,0.5);
        //truth_cmp->SetLineStyle(2);
        //ex_truth_cmp->GetYaxis()->SetRangeUser(0,1.0);
        //ex_truth_cmp->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //ex_truth_cmp->GetXaxis()->SetTitle(abin_var.c_str());
        //gPad->Update();
        //gPad->SetTicks(1,0);
        //TLegend * pad3_legend = new TLegend(0.6,0.75,0.925,0.925);
        //pad3_legend->SetBorderSize(0);
        //pad3_legend->SetFillColor(0);
        //pad3_legend->SetFillStyle(0);
        //pad3_legend->SetTextFont(42);
        //pad3_legend->SetTextSize(0.025);
        //pad3_legend->AddEntry(ex_truth_cmp,"Extracted truth signal","LP");
        //pad3_legend->AddEntry(truth_cmp,"Original Truth","LP");
        //pad3_legend->Draw();
        //TLatex pad3_ltx;
        //pad3_ltx.SetTextSize(0.03);
        //pad3_ltx.DrawLatexNDC(0.2,0.97,Form("%s Normalised Truth comparison - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.845,"Work In Progress");


        //mass_canv->cd(4);
        //ex_reco_cmp->Scale(1.0/ex_reco_cmp->Integral());
        //reco_cmp->Scale(1.0/reco_cmp->Integral());
        //ex_reco_cmp->Draw("E1");
        //reco_cmp->Draw("E1 SAME");
        //ex_reco_cmp->Draw("HIST SAME");
        //reco_cmp->Draw("HIST SAME");
        //ex_reco_cmp->SetLineColorAlpha(kBlue+1,1.0);
        //reco_cmp->SetLineColorAlpha(kBlue+1,0.5);
        //reco_cmp->SetLineStyle(2);
        //ex_reco_cmp->GetYaxis()->SetRangeUser(0,1.0);
        //ex_reco_cmp->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //ex_reco_cmp->GetXaxis()->SetTitle(abin_var.c_str());
        //gPad->Update();
        //gPad->SetTicks(1,0);
        //TLegend * pad4_legend = new TLegend(0.6,0.75,0.925,0.925);
        //pad4_legend->SetBorderSize(0);
        //pad4_legend->SetFillColor(0);
        //pad4_legend->SetFillStyle(0);
        //pad4_legend->SetTextFont(42);
        //pad4_legend->SetTextSize(0.025);
        //pad4_legend->AddEntry(ex_reco_cmp,"Extracted reco signal","LP");
        //pad4_legend->AddEntry(reco_cmp,"Original reco","LP");
        //pad4_legend->Draw();
        //TLatex pad4_ltx;
        //pad4_ltx.SetTextSize(0.03);
        //pad4_ltx.DrawLatexNDC(0.2,0.97,Form("%s Normalised Reco comparison - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.845,"Work In Progress");







      }
    }
  }
}
