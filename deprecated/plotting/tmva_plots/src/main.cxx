#include <main.hxx>
#include <comp.hxx>


int help(){
  std::cout << "h     --    help      --  output help" << std::endl;
  std::cout << "t     --    files     --  colon separated list of tmvatrees" << std::endl;
  std::cout << "m     --    mode      --  mode" << std::endl;
  std::cout << "u     --    uniq      --  unique string attached to this run" << std::endl;
  std::cout << "                      --  COMP - Comparison of TMVATree output" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "help",       no_argument,              0,      'h'},
      { "trees",      required_argument,        0,      't'},
      { "mode",       required_argument,        0,      'm'},
      { "uniq",       required_argument,        0,      'u'},
      { 0,            0,                        0,      0}
    };

  std::string files, mode, unique;

  do {
    option = getopt_long( argc, argv, "t:m:u:h", long_options, &option_index );
    switch ( option ){
      case 'h':
        return help();
      case 't': 
        files   = std::string( optarg );
        break;
      case 'u':
        unique  = std::string( optarg );
        break;
      case 'm':
        mode    = std::string( optarg );
        break;
    }
  } while (option != -1);
  

  if ( mode.empty() ){ 
    std::cout << "no mode provided" << std::endl; 
    return help(); 
  }

  if ( mode.find( "COMP" ) != std::string::npos ){
    comp( files, unique );
    return 0;
  }

  std::cout << "Mode provided does not match those available" << std::endl;
  return help();
}

