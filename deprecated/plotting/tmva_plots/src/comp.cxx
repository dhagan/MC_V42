#include <comp.hxx>


void comp( std::string files, std::string unique ){

  SetAtlasStyle();
  gROOT->SetBatch( true );
  gStyle->SetOptStat( "ourmeni"  );
  gStyle->SetOptFit( 0 );
  gStyle->SetEndErrorSize( 8 );

  gStyle->SetPadTopMargin( 0.19 );
  gStyle->SetPadRightMargin( 0.16 );
  gStyle->SetPadBottomMargin( 0.16 );
  gStyle->SetPadLeftMargin( 0.16 );

  std::map< std::string, std::vector<double> > bins;
  bins["qtA"]                 =  std::vector<double>{30,-5,15};
  bins["BDT"]                 =  std::vector<double>{30,-1.0,1.0};
  bins["abs(qtB)"]            =  std::vector<double>{30,0,15};
  bins["qtB"]                 =  std::vector<double>{30,-15,15};
  bins["abs(Phi)"]                 =  std::vector<double>{40,0,M_PI};
  bins["DPhi"]                =  std::vector<double>{30,0,M_PI};
  bins["DiMuonPt"]            =  std::vector<double>{30,5,30};
  bins["PhotonPt"]            =  std::vector<double>{30,5,25};
  bins["abs(costheta)"]            =  std::vector<double>{20,0,1.0};
  bins["costheta*costheta"]   =  std::vector<double>{40,0,1.0};


  std::map< std::string, std::string > names;
  names["costheta*costheta"]   =  "cos2theta";
  names["qtA"]                 =  "qtA";
  names["BDT"]                 =  "BDT";
  names["abs(qtB)"]            =  "abs_qtB";
  names["qtB"]                 =  "qtB";
  names["abs(Phi)"]            =  "Phi";
  names["DPhi"]                =  "DPhi";
  names["DiMuonPt"]            =  "DiMuonPt";
  names["PhotonPt"]            =  "PhotonPt";
  names["abs(costheta)"]       =  "abs_costheta";


  std::map< std::string, std::vector< std::string > > cuts;
  cuts["costheta*costheta"]   = {"no_cut:","low:costheta*costheta<0.1","high:costheta*costheta>0.1"};
  cuts["abs(costheta)"]       = {"no_cut:"};
  cuts["abs(Phi)"]            = {"no_cut:"};
  cuts["qtA"]                 = {"no_cut:"};
  cuts["abs(qtB)"]            = {"no_cut:"};
  cuts["qtB"]                 = {"no_cut:"};

  std::map<std::string,std::string> units;
  units["qtA"]                =  "(GeV)";
  units["BDT"]                =  "(Value)";
  units["abs(qtB)"]           =  "(GeV)";
  units["qtB"]                =  "(GeV)";
  units["abs(Phi)"]           =  "(rad)";
  units["DPhi"]               =  "(rad)";
  units["DiMuonPt"]           =  "(GeV)";
  units["PhotonPt"]           =  "(GeV)";
  units["abs(costheta)"]           =  "";
  units["costheta*costheta"]  =  "";

  std::vector< std::string > uni_vec;
  split_string( uni_vec, unique, ":" );

  std::string unique_str{ uni_vec.at(2) }, base_uni{ uni_vec.at(0) }, comp_uni{ uni_vec.at(1) };
  
  std::vector<std::string> file_vec;
  split_string( file_vec, files, ":" );

  TFile * file_base = new TFile( file_vec.at(0).c_str(), "READ" );
  TFile * file_comp = new TFile( file_vec.at(1).c_str(), "READ" );

  TTree * tree_base = (TTree*) file_base->Get( "TreeD" );
  TTree * tree_comp = (TTree*) file_comp->Get( "TreeD" );

  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont( 72 ); ATLAS.SetTextColor( 1 );
  TLatex wip; wip.SetNDC();
  wip.SetTextFont( 42 ); wip.SetTextSize( 0.038 );
  wip.SetTextColor( 1 );

  std::map< std::string, std::vector<double> >::iterator var_itr;
  for ( var_itr = bins.begin(); var_itr != bins.end(); var_itr++ ){

    int bin_count = var_itr->second.at(0);
    double bin_min = var_itr->second.at(1);
    double bin_max = var_itr->second.at(2);
    double width = ( bin_max - bin_min )/( (double) bin_count );
    std::string var = var_itr->first;
    std::string var_name = names[var];

    if ( !cuts.count(var) ){ continue; }

    for ( std::string full_cut : cuts[var] ){

      std::vector< std::string > cut_vec;
      split_string( cut_vec, full_cut, ":" );

      std::string cut_name = cut_vec.at( 0 );
      std::string cut = "";
      if ( cut_vec.size() == 2 ){ cut = cut_vec.at( 1 ); }

      std::string name = var + "_" + cut_name;
      std::string name_base =  "base_" + var_name;
      std::string name_comp =  "comp_" + var_name;

      TH1F * hist_base = new TH1F( name_base.c_str(), "", bin_count, bin_min, bin_max );
      TH1F * hist_comp = new TH1F( name_comp.c_str(), "", bin_count, bin_min, bin_max );

      tree_base->Draw( Form( "%s>>%s",var.c_str(),name_base.c_str() ), cut.c_str(), "goff" );
      tree_comp->Draw( Form( "%s>>%s",var.c_str(),name_comp.c_str() ), cut.c_str(), "goff" );
      
      TCanvas * var_canv = new TCanvas( "var_canv", "var_canv", 200, 200, 2000, 1000 );

      var_canv->Divide( 2, 1 );

      var_canv->cd( 1 );
      hist_base->Draw( "E1" );
      hist_base->GetYaxis()->SetRangeUser( 0.0, hist_base->GetMaximum()*1.5 );
      hist_base->SetLineColorAlpha( kRed+1, 1.0 );
      hist_base->GetYaxis()->SetTitle( Form( "Entries/ %.2f %s", width,  units[var].c_str() ) );
      hist_base->GetYaxis()->SetTitleOffset( 1.4 );
      hist_base->GetYaxis()->SetMaxDigits( 3 );
      hist_base->GetYaxis()->SetLabelFont( 42 );
      hist_base->GetYaxis()->SetLabelSize( 0.03 );
      hist_base->GetYaxis()->SetTitleFont( 42 );
      hist_base->GetYaxis()->SetTitleSize( 0.03 );
      hist_base->GetXaxis()->SetTitle( Form( "%s %s", var_name.c_str(), units[var].c_str() ) );
      hist_base->GetXaxis()->SetTitleOffset( 1.4 );
      hist_base->GetXaxis()->SetLabelFont( 42 );
      hist_base->GetXaxis()->SetLabelSize( 0.03 );
      hist_base->GetXaxis()->SetTitleFont( 42 );
      hist_base->GetXaxis()->SetTitleSize( 0.03 );
      hist_base->Draw( "HIST SAME" );
      gPad->Update(); gPad->Modified();
      TPaveStats * base_stats = (TPaveStats*) hist_base->FindObject( "stats" );
      base_stats->SetX1NDC( 0.55 ); base_stats->SetX2NDC( 0.83 );
      base_stats->SetY1NDC( 0.65 ); base_stats->SetY2NDC( 0.79 );
      base_stats->SetFillStyle( 0 );
      base_stats->SetTextFont( 42 );
      TLegend * base_legend = new TLegend( 0.18, 0.58, 0.6, 0.68 );
      base_legend->SetBorderSize( 0 );
      base_legend->SetFillColor( 0 );
      base_legend->SetFillStyle( 0 );
      base_legend->SetTextFont( 42 );
      base_legend->SetTextSize( 0.025 );
      base_legend->AddEntry(hist_base, base_uni.c_str(), "LP");
      base_legend->Draw();
      TLatex base_ltx;
      base_ltx.SetTextSize( 0.03 );
      base_ltx.DrawLatexNDC( 0.3, 0.825, Form( "%s - %s - %s", var_name.c_str(), cut_name.c_str(), unique_str.c_str() ) );  
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7,"Work In Progress" );

      var_canv->cd( 2 );
      hist_comp->Draw( "E1" );
      hist_comp->GetYaxis()->SetRangeUser( 0.0, hist_comp->GetMaximum()*1.5 );
      hist_comp->SetLineColorAlpha( kBlue+1, 1.0);
      hist_comp->GetYaxis()->SetTitle( Form( "Entries/ %.2f %s", width,  units[var].c_str() ) );
      hist_comp->GetYaxis()->SetTitleOffset( 1.4 );
      hist_comp->GetYaxis()->SetMaxDigits( 3 );
      hist_comp->GetYaxis()->SetLabelFont( 42 );
      hist_comp->GetYaxis()->SetLabelSize( 0.03 );
      hist_comp->GetYaxis()->SetTitleFont( 42 );
      hist_comp->GetYaxis()->SetTitleSize( 0.03 );
      hist_comp->GetXaxis()->SetTitle( Form( "%s %s", var_name.c_str(), units[var].c_str() ) );
      hist_comp->GetXaxis()->SetTitleOffset( 1.4 );
      hist_comp->GetXaxis()->SetLabelFont( 42 );
      hist_comp->GetXaxis()->SetLabelSize( 0.03 );
      hist_comp->GetXaxis()->SetTitleFont( 42 );
      hist_comp->GetXaxis()->SetTitleSize( 0.03 );
      hist_comp->Draw( "HIST SAME" );
      gPad->Update(); gPad->Modified();
      TPaveStats * comp_stats = (TPaveStats*) hist_comp->FindObject("stats");
      comp_stats->SetX1NDC( 0.55 ); comp_stats->SetX2NDC( 0.83 );
      comp_stats->SetY1NDC( 0.65 ); comp_stats->SetY2NDC( 0.79 );
      comp_stats->SetFillStyle( 0 );
      comp_stats->SetTextFont( 42 );
      TLegend * comp_legend = new TLegend( 0.18, 0.58, 0.6, 0.68 );
      comp_legend->SetBorderSize( 0 );
      comp_legend->SetFillColor( 0 );
      comp_legend->SetFillStyle( 0 );
      comp_legend->SetTextFont( 42 );
      comp_legend->SetTextSize( 0.025 );
      comp_legend->AddEntry(hist_comp, comp_uni.c_str(), "LP");
      comp_legend->Draw();
      TLatex comp_ltx;
      comp_ltx.SetTextSize( 0.03 );
      comp_ltx.DrawLatexNDC( 0.3, 0.825, Form( "%s - %s - %s", var_name.c_str(), cut_name.c_str(), unique_str.c_str() ) );  
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7,"Work In Progress" );




      var_canv->SaveAs( Form( "%s_%s_%s.png", var_name.c_str(), cut_name.c_str(), unique_str.c_str()  ));
      var_canv->Close();
      delete var_canv;
      delete hist_base;
      delete hist_comp;

    } 

  }

}
