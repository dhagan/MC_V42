
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  std::vector<std::string> tree_variables = {"BDT","DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};//,"qta-by-BDT"};
  std::vector<double> low_edges{-1.0,-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};//,-5};
  std::vector<double> high_edges{1.0, M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};//,15};

  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};
  std::vector<int> n_vec = {0,1,2,5,10,25,50,100,125,150,175,200};

  for (int q_idx = 0; q_idx <= 10; q_idx++){
    
    int qta_lower; 
    int qta_upper;
    
    if (q_idx == 0){
      qta_lower = 0;
      qta_upper = 15; 
    } else {
      qta_lower = (2*(q_idx-1)) - 5;
      qta_upper = (2*(q_idx-1)) - 3;
    }
    //std::cout << "qta upper " << qta_upper << std::endl;
    //std::cout << "qta lower " << qta_lower << std::endl;

    for (int n_idx = 0; n_idx < n_vec.size(); n_idx++){

      int current_n = n_vec[n_idx];

      TFile pp_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_pp.root","READ"};
      TFile sig_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_signal/Evaluate_signal.root","READ"};


      TFile mix1_file{ Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,1),"READ"};
      TFile mix5_file{ Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,5),"READ"};
      TFile mix10_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,10),"READ"};
      TFile mix12_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,12),"READ"};
      TFile mix20_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,20),"READ"};
      TFile mix33_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,33),"READ"};
      TFile mix50_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,50),"READ"};
      TFile mix75_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,75),"READ"};
      TFile mix100_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",current_n,100),"READ"};

      TTree * pp_tree =  (TTree*) pp_file.Get("pp_00_mu4000_P5000_bound-0");
      TTree * mix1_tree = (TTree*) mix1_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,1));
      TTree * mix5_tree = (TTree*) mix5_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,5));
      TTree * mix10_tree = (TTree*) mix10_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,10));
      TTree * mix12_tree = (TTree*) mix12_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,12));
      TTree * mix20_tree = (TTree*) mix20_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,20));
      TTree * mix33_tree = (TTree*) mix33_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,33));
      TTree * mix50_tree = (TTree*) mix50_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,50));
      TTree * mix75_tree = (TTree*) mix75_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,75));
      TTree * mix100_tree = (TTree*) mix100_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",current_n,100));
      TTree * sig_tree =  (TTree*) sig_file.Get("signal_00_mu4000_P5000_bound-0");

      for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 
      
        char cut[200];
        sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
        std::cout << cut << std::endl;

        std::vector<TH1F> pp_hist_vector;
        std::vector<TH1F> sig_hist_vector;
        std::vector<TH1F> mix1_hist_vector;
        std::vector<TH1F> mix5_hist_vector;
        std::vector<TH1F> mix10_hist_vector;
        std::vector<TH1F> mix12_hist_vector;
        std::vector<TH1F> mix20_hist_vector;
        std::vector<TH1F> mix33_hist_vector;
        std::vector<TH1F> mix50_hist_vector;
        std::vector<TH1F> mix75_hist_vector;
        std::vector<TH1F> mix100_hist_vector;

        for (int variable_index = 0; variable_index<tree_variables.size(); variable_index++){

          const char * variable = tree_variables[variable_index].c_str();
          char pp_name[20],sig_name[20],temp_name[20],hist_title[100];
          char mix1_name[20], mix5_name[20], mix10_name[20], mix12_name[20],mix20_name[20],mix33_name[20],mix50_name[20],mix75_name[20],mix100_name[20];
          sprintf(pp_name,"pp_%i_%i_%i",variable_index,current_n,q_idx);
          sprintf( mix1_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,1);
          sprintf( mix5_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,5);
          sprintf(mix10_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,10);
          sprintf(mix12_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,12);
          sprintf(mix20_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,20);
          sprintf(mix33_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,33);
          sprintf(mix50_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,50);
          sprintf(mix75_name,"mix_%i_%i_%i_%i",variable_index,current_n,q_idx,75);
          sprintf(mix100_name,"mix_%i_%i_%i_%i",variable_index,current_n,100);
          sprintf(sig_name,"sig_%i_%i_%i",variable_index,current_n,q_idx);
          sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());
          
          int bin_count = 50;
          sig_hist_vector.push_back(   TH1F(sig_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          pp_hist_vector.push_back(    TH1F(pp_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix1_hist_vector.push_back(  TH1F( mix1_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix5_hist_vector.push_back(  TH1F( mix5_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix10_hist_vector.push_back( TH1F(mix10_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix12_hist_vector.push_back( TH1F(mix12_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix20_hist_vector.push_back( TH1F(mix20_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix33_hist_vector.push_back( TH1F(mix33_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix50_hist_vector.push_back( TH1F(mix50_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix75_hist_vector.push_back( TH1F(mix75_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          mix100_hist_vector.push_back(TH1F(mix100_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));

          sig_tree->Draw(Form("%s>>%s",variable,sig_name),cut,"goff");
          pp_tree->Draw(Form("%s>>%s",variable,pp_name),cut,"goff");
          mix1_tree->Draw(Form("%s>>%s",variable,mix1_name),cut,"goff");
          mix5_tree->Draw(Form("%s>>%s",variable,mix5_name),cut,"goff");
          mix10_tree->Draw(Form("%s>>%s",variable,mix10_name),cut,"goff");
          mix12_tree->Draw(Form("%s>>%s",variable,mix12_name),cut,"goff");
          mix20_tree->Draw(Form("%s>>%s",variable,mix20_name),cut,"goff");
          mix33_tree->Draw(Form("%s>>%s",variable,mix33_name),cut,"goff");
          mix50_tree->Draw(Form("%s>>%s",variable,mix50_name),cut,"goff");
          mix75_tree->Draw(Form("%s>>%s",variable,mix75_name),cut,"goff");
          mix100_tree->Draw(Form("%s>>%s",variable,mix100_name),cut,"goff");

        } 

        TCanvas canv{Form("canv%i_%i",q_idx,n_idx),"",200,100,5000,4000};
        canv.Divide(5,4);

        std::vector<TPaveText> text_vector;
        std::vector<TLegend> legend_vector;

        for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){
          
          const char * variable = tree_variables[variable_index].c_str();

          canv.cd(variable_index+1);

          double pp_int     = pp_hist_vector[variable_index].Integral();
          double sig_int    = sig_hist_vector[variable_index].Integral();
          double mix1_int   = mix1_hist_vector[variable_index].Integral();
          double mix5_int   = mix5_hist_vector[variable_index].Integral();
          double mix10_int  = mix10_hist_vector[variable_index].Integral();
          double mix12_int  = mix12_hist_vector[variable_index].Integral();
          double mix20_int  = mix20_hist_vector[variable_index].Integral();
          double mix33_int  = mix33_hist_vector[variable_index].Integral();
          double mix50_int  = mix50_hist_vector[variable_index].Integral();
          double mix75_int  = mix75_hist_vector[variable_index].Integral();
          double mix100_int  = mix100_hist_vector[variable_index].Integral();

          pp_hist_vector[variable_index].Scale(1.0/pp_hist_vector[variable_index].Integral());
          sig_hist_vector[variable_index].Scale(1.0/sig_hist_vector[variable_index].Integral());
          mix1_hist_vector[variable_index].Scale(1.0/mix1_hist_vector[variable_index].Integral());
          mix5_hist_vector[variable_index].Scale(1.0/mix5_hist_vector[variable_index].Integral());
          mix10_hist_vector[variable_index].Scale(1.0/mix10_hist_vector[variable_index].Integral());
          mix12_hist_vector[variable_index].Scale(1.0/mix12_hist_vector[variable_index].Integral());
          mix20_hist_vector[variable_index].Scale(1.0/mix20_hist_vector[variable_index].Integral());
          mix33_hist_vector[variable_index].Scale(1.0/mix33_hist_vector[variable_index].Integral());
          mix50_hist_vector[variable_index].Scale(1.0/mix50_hist_vector[variable_index].Integral());
          mix75_hist_vector[variable_index].Scale(1.0/mix75_hist_vector[variable_index].Integral());
          mix100_hist_vector[variable_index].Scale(1.0/mix100_hist_vector[variable_index].Integral());
           

          sig_hist_vector[variable_index].Draw("HIST");
          pp_hist_vector[variable_index].Draw("HIST SAME");
          mix1_hist_vector[variable_index].Draw("HIST SAME");
          mix5_hist_vector[variable_index].Draw("HIST SAME");
          mix10_hist_vector[variable_index].Draw("HIST SAME");
          mix12_hist_vector[variable_index].Draw("HIST SAME");
          mix20_hist_vector[variable_index].Draw("HIST SAME");
          mix33_hist_vector[variable_index].Draw("HIST SAME");
          mix50_hist_vector[variable_index].Draw("HIST SAME");
          mix75_hist_vector[variable_index].Draw("HIST SAME");
          mix100_hist_vector[variable_index].Draw("HIST SAME");

          sig_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,2*std::max(pp_hist_vector[variable_index].GetMaximum(),sig_hist_vector[variable_index].GetMaximum()));

          pp_hist_vector[variable_index].SetLineColor(4);
          pp_hist_vector[variable_index].SetLineStyle(1);

          sig_hist_vector[variable_index].SetLineColor(2);
          sig_hist_vector[variable_index].SetLineStyle(1);
          
          mix1_hist_vector[variable_index].SetLineColorAlpha(1,1.0);
          mix1_hist_vector[variable_index].SetLineStyle(2);

          mix5_hist_vector[variable_index].SetLineColorAlpha(1,0.9);
          mix5_hist_vector[variable_index].SetLineStyle(2);

          mix10_hist_vector[variable_index].SetLineColorAlpha(1,0.8);
          mix10_hist_vector[variable_index].SetLineStyle(2);
         
          mix10_hist_vector[variable_index].SetLineColorAlpha(1,0.7);
          mix10_hist_vector[variable_index].SetLineStyle(2);

          mix20_hist_vector[variable_index].SetLineColorAlpha(1,0.6);
          mix20_hist_vector[variable_index].SetLineStyle(2);

          mix33_hist_vector[variable_index].SetLineColorAlpha(1,0.5);
          mix33_hist_vector[variable_index].SetLineStyle(2);

          mix50_hist_vector[variable_index].SetLineColorAlpha(1,0.4);
          mix50_hist_vector[variable_index].SetLineStyle(2);

          mix75_hist_vector[variable_index].SetLineColorAlpha(1,0.3);
          mix75_hist_vector[variable_index].SetLineStyle(2);

          mix100_hist_vector[variable_index].SetLineColorAlpha(1,0.2);
          mix100_hist_vector[variable_index].SetLineStyle(2);

          TPaveText * cap_text = new TPaveText(0.75,0.5,0.925,0.925,"nbNDC");
          cap_text->AddText(Form("Object use cap - %i",current_n));
          cap_text->AddText(Form("QTA - %i",q_idx));
          cap_text->AddText(Form("pp Integral - %f", pp_int));
          cap_text->AddText(Form("sig Integral - %f", sig_int));
          cap_text->AddText(Form("mix k1 Integral - %f", mix1_int));
          cap_text->AddText(Form("mix k5 Integral - %f", mix5_int));
          cap_text->AddText(Form("mix k10 Integral - %f", mix10_int));
          cap_text->AddText(Form("mix k12 Integral - %f", mix12_int));
          cap_text->AddText(Form("mix k20 Integral - %f", mix20_int));
          cap_text->AddText(Form("mix k33 Integral - %f", mix33_int));
          cap_text->AddText(Form("mix k50 Integral - %f", mix50_int));
          cap_text->AddText(Form("mix k75 Integral - %f", mix75_int));
          cap_text->AddText(Form("mix k100 Integral - %f", mix100_int));
          //cap_text->AddText(Form("Chi 2 - %.7f",pp_hist_vector[variable_index].Chi2Test(&mix_hist_vector[variable_index],"UU NORM") ));
          //cap_text->AddText(Form("Chi 2/ndf - %.7f",pp_hist_vector[variable_index].Chi2Test(&mix_hist_vector[variable_index],"UU NORM CHI2/NDF") ));
          cap_text->SetBorderSize(0);
          cap_text->SetFillStyle(0);
          cap_text->SetFillColor(0);
          cap_text->SetTextFont(42);
          cap_text->SetTextSize(0.015);
          cap_text->SetTextAlign(31);
          cap_text->Draw();
          
          TLegend * current_legend = new TLegend(0.225,0.6,0.4,0.925,variable);
          current_legend->AddEntry(&pp_hist_vector[variable_index],"pp");
          current_legend->AddEntry(&mix1_hist_vector[variable_index],"mix1");
          current_legend->AddEntry(&mix5_hist_vector[variable_index],"mix5");
          current_legend->AddEntry(&mix10_hist_vector[variable_index],"mix10");
          current_legend->AddEntry(&mix12_hist_vector[variable_index],"mix12");
          current_legend->AddEntry(&mix20_hist_vector[variable_index],"mix20");
          current_legend->AddEntry(&mix33_hist_vector[variable_index],"mix33");
          current_legend->AddEntry(&mix50_hist_vector[variable_index],"mix50");
          current_legend->AddEntry(&mix75_hist_vector[variable_index],"mix75");
          current_legend->AddEntry(&mix100_hist_vector[variable_index],"mix100");
          current_legend->AddEntry(&sig_hist_vector[variable_index],"sig");
          current_legend->SetBorderSize(1);
          current_legend->SetTextFont(42);
          current_legend->SetFillStyle(0);
          current_legend->SetTextSize(0.02);
          current_legend->Draw(); 
          //legend_vector.push_back(current_legend);

        }
        canv.SaveAs(Form("./pp-ppmix_qta%i_n%i_%s_shape_check.png",q_idx,current_n,mass_cuts[mass_range].c_str()));
      }
    }
  }
}

int main(){
  check(); 
}
