
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  std::vector<std::string> tree_variables = {"BDT","DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};//,"qta-by-BDT"};
  std::vector<double> low_edges{-1.0,-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};//,-5};
  std::vector<double> high_edges{1.0, M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};//,15};

  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};


  for (int q_idx = 0; q_idx <= 10; q_idx++){

    int qta_lower; 
    int qta_upper;
    
    if (q_idx == 0){
      qta_lower = -5;
      qta_upper = 15; 
    } else {
      qta_lower = (2*(q_idx-1)) - 5;
      qta_upper = (2*(q_idx-1)) - 3;
    }
    std::cout << "qta upper " << qta_upper << std::endl;
    std::cout << "qta lower " << qta_lower << std::endl;

    TFile sig_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_signal/Evaluate_signal.root","READ"};
    TFile pp_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_pp.root","READ"};
    TFile n200_k12_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",200,12),"READ"};
    TFile n100_k100_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n%i_k%i.root",100,100),"READ"};

    TTree * sig_tree        =  (TTree*) sig_file.Get("signal_00_mu4000_P5000_bound-0");
    TTree * pp_tree         =  (TTree*) pp_file.Get("pp_00_mu4000_P5000_bound-0");
    TTree * n200_k12_tree   = (TTree*)  n200_k12_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",200,12));
    TTree * n100_k100_tree  = (TTree*) n100_k100_file.Get(Form("pp-mix_n%i_k%i_00_mu4000_P5000_bound-0",100,100));

    for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 

      std::vector<TH1F> sig_hist_vector;
      std::vector<TH1F> pp_hist_vector;
      std::vector<TH1F> n200_k12_hist_vector;
      std::vector<TH1F> n100_k100_hist_vector;

      char cut[200];
      sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      std::cout << cut << std::endl;

      for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){

        const char * variable = tree_variables[variable_index].c_str();
        char pp_name[20],sig_name[20],temp_name[20],hist_title[100];
        char n200_k12_name[20],n100_k100_name[20];
        sprintf(sig_name,"sig_%i_%i",variable_index,q_idx);
        sprintf(pp_name,"pp_%i_%i",variable_index,q_idx);
        sprintf(n200_k12_name,"mix_%i_%i_%i",variable_index,200,12);
        sprintf(n100_k100_name,"mix_%i_%i_%i",variable_index,100,100);
        sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());
        
        int bin_count = 50;
        
        sig_hist_vector.push_back(   TH1F(sig_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        pp_hist_vector.push_back(    TH1F(pp_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        n200_k12_hist_vector.push_back( TH1F(n200_k12_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        n100_k100_hist_vector.push_back(TH1F(n100_k100_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));

        sig_tree->Draw(Form("%s>>%s",variable,sig_name),cut,"goff");
        pp_tree->Draw(Form("%s>>%s",variable,pp_name),cut,"goff");
        n200_k12_tree->Draw(Form("%s>>%s",variable,n200_k12_name),cut,"goff");
        n100_k100_tree->Draw(Form("%s>>%s",variable,n100_k100_name),cut,"goff");
        
      } 
      
      TCanvas canv{Form("canv%i",q_idx),"",200,100,5000,4000};
      canv.Divide(5,4);

      std::vector<TPaveText> text_vector;
      std::vector<TLegend> legend_vector;

      for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){
        
        const char * variable = tree_variables[variable_index].c_str();

        canv.cd(variable_index+1);

        double pp_int       = pp_hist_vector[variable_index].Integral();
        double sig_int      = sig_hist_vector[variable_index].Integral();
        double n200_k12_int    = n200_k12_hist_vector[variable_index].Integral();
        double n100_k100_int   = n100_k100_hist_vector[variable_index].Integral();

        pp_hist_vector[variable_index].Scale(1.0/pp_hist_vector[variable_index].Integral());
        sig_hist_vector[variable_index].Scale(1.0/sig_hist_vector[variable_index].Integral());
        n200_k12_hist_vector[variable_index].Scale(1.0/n200_k12_hist_vector[variable_index].Integral());
        n100_k100_hist_vector[variable_index].Scale(1.0/n100_k100_hist_vector[variable_index].Integral());
         

        sig_hist_vector[variable_index].Draw("HIST");
        pp_hist_vector[variable_index].Draw("HIST SAME");
        n200_k12_hist_vector[variable_index].Draw("HIST SAME");
        n100_k100_hist_vector[variable_index].Draw("HIST SAME");

        sig_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,2*std::max(pp_hist_vector[variable_index].GetMaximum(),sig_hist_vector[variable_index].GetMaximum()));

        pp_hist_vector[variable_index].SetLineColor(4);
        pp_hist_vector[variable_index].SetLineStyle(1);

        sig_hist_vector[variable_index].SetLineColor(2);
        sig_hist_vector[variable_index].SetLineStyle(1);
        
        n200_k12_hist_vector[variable_index].SetLineColorAlpha(1,0.8);
        n200_k12_hist_vector[variable_index].SetLineStyle(2);
        
        n100_k100_hist_vector[variable_index].SetLineColorAlpha(1,0.3);
        n100_k100_hist_vector[variable_index].SetLineStyle(1);

        
        TPaveText * cap_text = new TPaveText(0.75,0.5,0.925,0.925,"nbNDC");
        cap_text->AddText(Form("pp Integral - %f", pp_int));
        cap_text->AddText(Form("sig Integral - %f", sig_int));
        cap_text->AddText(Form("n200 k12 Integral - %f", n200_k12_int));
        cap_text->AddText(Form("n100 k100 Integral - %f", n100_k100_int));
        cap_text->SetBorderSize(0);
        cap_text->SetFillStyle(0);
        cap_text->SetFillColor(0);
        cap_text->SetTextFont(42);
        cap_text->SetTextSize(0.015);
        cap_text->SetTextAlign(31);
        cap_text->Draw();
        
        TLegend * current_legend = new TLegend(0.225,0.6,0.4,0.925,variable);
        current_legend->AddEntry(&sig_hist_vector[variable_index],"sig");
        current_legend->AddEntry(&pp_hist_vector[variable_index],"pp");
        current_legend->AddEntry(&n200_k12_hist_vector[variable_index],"n200_k12");
        current_legend->AddEntry(&n100_k100_hist_vector[variable_index],"n100_k100");
        current_legend->SetBorderSize(1);
        current_legend->SetTextFont(42);
        current_legend->SetFillStyle(0);
        current_legend->SetTextSize(0.02);
        current_legend->Draw(); 
        //legend_vector.push_back(current_legend);

      }
      canv.SaveAs(Form("./ppmix_focussed_qta%i_%s_shape_check.png",q_idx,mass_cuts[mass_range].c_str()));
    }

  }

}

int main(){
  check(); 
}
