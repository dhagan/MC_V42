#include <split_string.hxx>

void split_string(std::vector<std::string> & vec_split, std::string str_split, std::string delim){
  std::istringstream str_stream{str_split};
  std::string token;
  while( getline( str_stream, token, *(delim.c_str()) ) ){
    vec_split.push_back(token);
  }
}

