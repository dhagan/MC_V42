
#include <TH1.h>
#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLatex.h>

#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <string>
#include <TROOT.h>
#include <TStyle.h>
#include <cmath>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"

void bdt_overlay(){

  SetAtlasStyle();
   
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);


  TFile sign_file{"../../../TMVA/run/Eval_signal/Evaluate_signal.root","READ"}; 
  TFile bckg_file{"../../../TMVA/run/Eval_pp/Evaluate_pp.root","READ"}; 


  TTree * sign_tree = (TTree*) sign_file.Get("signal_00_mu4000_P5000_bound-0");
  TTree * bckg_tree = (TTree*) bckg_file.Get("pp_00_mu4000_P5000_bound-0");

  TH1F * sign_hist = new TH1F("sign_hist","",20,-1.0,1.0);
  TH1F * bckg_hist = new TH1F("bckg_hist","",20,-1.0,1.0);

  sign_tree->Draw("BDT>>sign_hist","","goff");
  bckg_tree->Draw("BDT>>bckg_hist","","goff");

  TCanvas BDT_Canv{"BDT_Canv","BDT_Canv",600,600,1000,1000};
  BDT_Canv.Divide(1);


  sign_hist->Scale(1.0/sign_hist->Integral());
  bckg_hist->Scale(1.0/bckg_hist->Integral());


  sign_hist->Draw("HIST"); 
  bckg_hist->Draw("HIST SAME"); 
  sign_hist->SetLineColor(2);
  bckg_hist->SetLineColor(4);
  sign_hist->GetYaxis()->SetRangeUser(0,1.1*std::max(bckg_hist->GetMaximum(),sign_hist->GetMaximum()));
  gPad->Modified(); gPad->Update();
  TLegend * current_legend = new TLegend(0.25,0.8,0.6,0.925,"BDT");
  current_legend->AddEntry(sign_hist,"signal score");
  current_legend->AddEntry(bckg_hist,"background score");
  current_legend->SetBorderSize(1);
  current_legend->SetTextFont(42);
  current_legend->SetFillStyle(0);
  current_legend->SetTextSize(0.03);
  current_legend->Draw();
  sign_hist->GetXaxis()->SetTitle("BDT score");
  
  TLatex ATLAS;
  ATLAS.SetNDC();
  ATLAS.SetTextFont(72);
  ATLAS.SetTextColor(1);
  ATLAS.DrawLatex(0.65,0.89,"ATLAS");
  TLatex wip; 
  wip.SetNDC();
  wip.SetTextFont(42);
  wip.SetTextSize(0.038);
  wip.SetTextColor(1);
  wip.DrawLatex(0.65,0.85,"Work In Progress");


  
  BDT_Canv.SaveAs("BDTOverlay.png"); 

  return;

}

int main( int argc, char * argv[]){
  bdt_overlay();
}
