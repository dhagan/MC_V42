
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"

void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  std::vector<std::string> tree_variables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};//,"qta-by-BDT"};
  int hist_bins = 50;
  std::vector<double> low_edges{-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};//,-5};
  std::vector<double> high_edges{M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};//,15};
  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};
  
  TFile n200_k1_rand_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/ppmix_bound-0/rand/n200_k1/pp-mix_n200_k1_00_mu4000_P5000.root","READ"};
  TFile n100_k100_rand_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/ppmix_bound-0/rand/n100_k100/pp-mix_n100_k100_00_mu4000_P5000.root","READ"};
  TFile n200_k1_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/ppmix_bound-0/f/n200_k1/pp-mix_n200_k1_00_mu4000_P5000.root","READ"};
  TFile n100_k100_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/ppmix_bound-0/f/n100_k100/pp-mix_n100_k100_00_mu4000_P5000.root","READ"};


  TTree * n200_k1_rand_tree     = (TTree*) n200_k1_rand_file.Get("TreeD");
  TTree * n100_k100_rand_tree          = (TTree*) n100_k100_rand_file.Get("TreeD");
  TTree * n200_k1_tree           = (TTree*) n200_k1_file.Get("TreeD");
  TTree * n100_k100_tree          = (TTree*) n100_k100_file.Get("TreeD");


  for (int q_idx = 0; q_idx <= 10; q_idx++){
                                             
    int qta_lower; 
    int qta_upper;
    if (q_idx == 0){
      qta_lower = -5;
      qta_upper = 15; 
    } else {
      qta_lower = (2*(q_idx-1)) - 5;
      qta_upper = (2*(q_idx-1)) - 3;
    }

    for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 

        std::vector<TH1F*> n200_k1_rand_hist_vector;
        std::vector<TH1F*> n100_k100_rand_hist_vector;
        std::vector<TH1F*> n200_k1_hist_vector;
        std::vector<TH1F*> n100_k100_hist_vector;


        for (int variable_index = 0; variable_index<tree_variables.size(); variable_index++){

            const char * variable = tree_variables[variable_index].c_str();
            char cut[200];
            sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
            n200_k1_rand_hist_vector.push_back(new TH1F(Form("n200k1_v%i_Q%i_qta%i",variable_index,mass_range,q_idx),hist_bins,low_edges[variable_index],high_edges[variable_index]));
            n100_k100_rand_hist_vector.push_back(new TH1F(Form("n100k100_v%i_Q%i_qta%i",variable_index,mass_range,q_idx),hist_bins,low_edges[variable_index],high_edges[variable_index]));

            n200_k1_hist_vector.push_back(new TH1F(Form("n200k1_v%i_Q%i_qta%i",variable_index,mass_range,q_idx),hist_bins,low_edges[variable_index],high_edges[variable_index]));
            n100_k100_hist_vector.push_back(new TH1F(Form("n100k100_v%i_Q%i_qta%i",variable_index,mass_range,q_idx),hist_bins,low_edges[variable_index],high_edges[variable_index]));

            n200_k1_rand_tree->Draw(Form("%s>>n200k1_v%i_Q%i_qta%i",variable,variable_index,mass_range,q_idx),cut,"goff");
            n100_k100_rand_tree->Draw(Form("%s>>n100k100_v%i_Q%i_qta%i",variable,variable_index,mass_range,q_idx),cut,"goff");
            n200_k1_tree->Draw(Form("%s>>n200k1_v%i_Q%i_qta%i",variable,variable_index,mass_range,q_idx),cut,"goff");
            n100_k100_tree->Draw(Form("%s>>n100k100_v%i_Q%i_qta%i",variable,variable_index,mass_range,q_idx),cut,"goff");

        }

        TCanvas * canv = new TCanvas(Form("canv%i%i",mass_range,q_idx),"",200,100,5000,5000);
        canv->Divide(5,5);

        for (int variable_index = 0; variable_index<tree_variables.size(); variable_index++){
            
          const char * variable = tree_variables[variable_index].c_str();

          canv->cd(variable_index+1);
          n200_k1_rand_hist_vector[variable_index]->Draw("HIST");
          n100_k100_rand_hist_vector[variable_index]->Draw("SAME HIST");
          n200_k1_hist_vector[variable_index]->Draw("HIST");
          n100_k100_hist_vector[variable_index]->Draw("SAME HIST");
          n200_k1_rand_hist_vector[variable_index]->GetYaxis()->SetRangeUser(0,n200_k1_hist_vector[variable_index]->GetMaximum());
          n200_k1_rand_hist_vector[variable_index]->GetXaxis()->SetTitle("BDT Score");
          n200_k1_rand_hist_vector[variable_index]->GetYaxis()->SetTitle("Entries / 0.04");
          n200_k1_rand_hist_vector[variable_index]->SetLineColorAlpha(1,0.8);
          n100_k100_rand_hist_vector[variable_index]->SetLineColor(1);
          n100_k100_rand_hist_vector[variable_index]->SetLineStyle(3);
          n200_k1_hist_vector[variable_index]->SetLineColor(2);
          n100_k100_hist_vector[variable_index]->SetLineColor(4);

          TPaveText * current_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          current_text->AddText(variable);
          current_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          current_text->AddText(Form("qtA%i",q_idx));
          current_text->SetBorderSize(0);
          current_text->SetFillStyle(0);
          current_text->SetFillColor(0);
          current_text->SetTextFont(42);
          current_text->SetTextSize(0.03);
          current_text->SetTextAlign(31);
          current_text->Draw();
          
          TLegend * current_legend = new TLegend(0.225,0.8,0.7,0.925,variable);
          current_legend->AddEntry(n200_k1_rand_hist_vector[variable_index],"n200 k1 rand");
          current_legend->AddEntry(n100_k100_rand_hist_vector[variable_index],"n100 k100 rand");
          current_legend->SetBorderSize(1);
          current_legend->SetTextFont(42);
          current_legend->SetFillStyle(0);
          current_legend->SetTextSize(0.02);
          current_legend->Draw(); 
        
        }

        canv->SaveAs(Form("./random_generation_n%i_k%i_%s_qta%i.png",n,k,mass_cuts[mass_range].c_str(),q_idx));

    }
  }
}

int main(){
  check(); 
}
