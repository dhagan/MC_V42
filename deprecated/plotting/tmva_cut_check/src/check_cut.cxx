
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


void check(int ll_mode = 0, int cut_mode = 0){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  std::vector<std::string> tree_variables = {"BDT","DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};//,"qta-by-BDT"};
  std::vector<double> low_edges{-1.0,-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};//,-5};
  std::vector<double> high_edges{1.0, M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};//,15};

  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};


  for (int q_idx = 0; q_idx <= 10; q_idx++){

    int qta_lower; 
    int qta_upper;
    
    if (q_idx == 0){
      qta_lower = -5;
      qta_upper = 15; 
    } else {
      qta_lower = (2*(q_idx-1)) - 5;
      qta_upper = (2*(q_idx-1)) - 3;
    }
    std::cout << "qta upper " << qta_upper << std::endl;
    std::cout << "qta lower " << qta_lower << std::endl;

    TFile * sign_file;       
    TFile * bckg_file;       
    TFile * data_file;       

    if ( ll_mode == 0) {
      sign_file = new TFile("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_signal_ll_train_reg.root","READ");
      bckg_file = new TFile("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_pp_ll_train_reg.root","READ");
      data_file = new TFile("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_data_ll_train_reg.root","READ");
    } else if ( ll_mode == 1){
      sign_file = new TFile("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_signal_ll_train_ll.root","READ");
      bckg_file = new TFile("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_pp_ll_train_ll.root","READ");
      data_file = new TFile("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_data_ll_train_ll.root","READ");
    }

    TTree * sign_tree = (TTree*) sign_file->Get("signal_00_lowlambda_mu4000_P5000_bound-0");
    TTree * bckg_tree = (TTree*) bckg_file->Get("pp_00_lowlambda_mu4000_P5000_bound-0");
    TTree * data_tree = (TTree*) data_file->Get("data_00_lowlambda_mu4000_P5000_bound-0");

    for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 

      std::vector<TH1F> sign_hist_vector;
      std::vector<TH1F> bckg_hist_vector;
      std::vector<TH1F> data_hist_vector;

      char cut[200];
      //sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      if (cut_mode == 0){
        std::cout << "no cut" << std::endl;
        sprintf(cut,"%s&&qtA>%i&&qtA<%i&&Lambda>15",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      } else if (cut_mode == 1){
        std::cout << "eps cut" << std::endl;
        sprintf(cut,"%s&&qtA>%i&&qtA<%i&&Lambda>15&&((3.141593-abs(DPhi))/2.0)<0.1",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      } else if (cut_mode == 2){
        std::cout << "qtb cut" << std::endl;
        sprintf(cut,"%s&&qtA>%i&&qtA<%i&&Lambda>15&&abs(qtB)<4",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      } else if (cut_mode == 3){
        std::cout << "eps qtb cut" << std::endl;
        sprintf(cut,"%s&&qtA>%i&&qtA<%i&&Lambda>15&&abs(qtB)<4&&((3.141593-abs(DPhi))/2.0)<0.1",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      }

      std::cout << cut << std::endl;

      for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){

        const char * variable = tree_variables[variable_index].c_str();
        char bckg_name[40],sign_name[40],data_name[40],hist_title[100];

        sprintf(sign_name,"sign_%i_%i_%i",variable_index,q_idx,mass_range);
        sprintf(bckg_name,"bckg_%i_%i_%i",variable_index,q_idx,mass_range);
        sprintf(data_name,"data_%i_%i_%i",variable_index,q_idx,mass_range);

        sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());
        
        int bin_count = 50;
        
        sign_hist_vector.push_back(TH1F(sign_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        bckg_hist_vector.push_back(TH1F(bckg_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        data_hist_vector.push_back(TH1F(data_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));

        sign_tree->Draw(Form("%s>>%s",variable,sign_name),cut,"goff");
        bckg_tree->Draw(Form("%s>>%s",variable,bckg_name),cut,"goff");
        data_tree->Draw(Form("%s>>%s",variable,data_name),cut,"goff");

      } 
      
      TCanvas canv{Form("canv%i",q_idx),"",200,100,5000,4000};
      canv.Divide(5,4);

      for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){
        
        const char * variable = tree_variables[variable_index].c_str();
        canv.cd(variable_index+1);

        double sign_int           = sign_hist_vector[variable_index].Integral();
        double bckg_int           = bckg_hist_vector[variable_index].Integral();
        double data_int           = bckg_hist_vector[variable_index].Integral();

        sign_hist_vector[variable_index].Scale(1.0/sign_hist_vector[variable_index].Integral());
        bckg_hist_vector[variable_index].Scale(1.0/bckg_hist_vector[variable_index].Integral());
        data_hist_vector[variable_index].Scale(1.0/data_hist_vector[variable_index].Integral());

        sign_hist_vector[variable_index].Draw("HIST");
        bckg_hist_vector[variable_index].Draw("HIST SAME");
        data_hist_vector[variable_index].Draw("HIST SAME");

        sign_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,1.5*std::max(sign_hist_vector[variable_index].GetMaximum(),bckg_hist_vector[variable_index].GetMaximum()));
        sign_hist_vector[variable_index].SetLineColor(2);
        sign_hist_vector[variable_index].SetLineStyle(1);
        bckg_hist_vector[variable_index].SetLineColor(4);
        bckg_hist_vector[variable_index].SetLineStyle(1);
        data_hist_vector[variable_index].SetLineColor(1);
        data_hist_vector[variable_index].SetLineStyle(1);


        TPaveText * cap_text = new TPaveText(0.75,0.5,0.925,0.925,"nbNDC");
        cap_text->AddText(Form("sign Integral - %f",    bckg_int));
        cap_text->AddText(Form("bckg Integral - %f",    sign_int));
        cap_text->AddText(Form("data Integral - %f",    data_int));
        cap_text->SetBorderSize(0);
        cap_text->SetFillStyle(0);
        cap_text->SetFillColor(0);
        cap_text->SetTextFont(42);
        cap_text->SetTextSize(0.015);
        cap_text->SetTextAlign(31);
        cap_text->Draw();
        
        TLegend * current_legend = new TLegend(0.225,0.6,0.4,0.925,variable);
        current_legend->AddEntry(&sign_hist_vector[variable_index],"sign");
        current_legend->AddEntry(&bckg_hist_vector[variable_index],"bckg");
        current_legend->AddEntry(&data_hist_vector[variable_index],"data");
        current_legend->SetBorderSize(1);
        current_legend->SetTextFont(42);
        current_legend->SetFillStyle(0);
        current_legend->SetTextSize(0.02);
        current_legend->Draw(); 

      }
      std::vector<std::string> cut_types = {"nc","eps","qtb","epsqtb"};
      std::vector<std::string> file_types = {"rg","ll"};
      canv.SaveAs(Form("./tmva_ll_check_qta%i_Q%s_%s_train%s.png",q_idx,mass_cuts[mass_range].c_str(),cut_types[cut_mode].c_str(),file_types[ll_mode].c_str()));
    }
  }
}

int main(int arc, char ** argv){
  std::cout << argv[1] << std::endl;
  std::cout << argv[2] << std::endl;
  if (arc == 3){
    std::cout << "works" << std::endl;
    check(atoi(argv[1]),atoi(argv[2])); 
  }
}
