#include <tr.hxx>
#include <split_string.hxx>

void tr( std::string sign, std::string data, std::string bin_str, std::string unique ){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);


  std::map< int ,std::string> mass_ranges;
  mass_ranges[0]             = std::string("Lambda>0&&Lambda<200");
  mass_ranges[3]             = std::string("Lambda>25&&Lambda<50");
  mass_ranges[4]             = std::string("Lambda>50&&Lambda<100");
  mass_ranges[5]             = std::string("Lambda>100&&Lambda<200");
  mass_ranges[12]            = std::string("Lambda>25&&Lambda<200");

  std::vector< std::string > var_vec = { "DiMuonPt", "ActIpX", "Mass", "PhotonPt", "Phi", "z",
                                         "Phot_Eta", "JPsi_Eta", "AvgIpX" };

  std::map< std::string, std::vector<double> > bin_map;
  bin_map["DiMuonPt"] = { 25, 8000, 33000 };
  bin_map["PhotonPt"] = { 22 , 5000, 27000 };
  bin_map["ActIpX"] = { 50, 0, 50 };
  bin_map["AvgIpX"] = { 50, 0, 50 };
  bin_map["Mass"] = { 16 , 12, 44 };
  bin_map["Phi"] = { 15 , 0, M_PI };
  bin_map["z"] = { 8 , 0, 1 };
  bin_map["JPsi_Eta"] = { 20 , -2.5, 2.5 };
  bin_map["Phot_Eta"] = { 20 , -2.5, 2.5 };
  bin_map["qtA"] = { 15 , -10.0, 20.0 };

  if ( !bin_str.empty() ){
    std::vector<std::string> full_range_vec;
    split_strings(full_range_vec, bin_str, "#" );
    for ( std::string range_str : full_range_vec ){
      std::vector<std::string> var_vec, pairs;
      split_strings(var_vec, range_str,"_");
      split_strings(pairs, var_vec.at(1), ":");
      std::vector<std::string> bounds; 

      if ( bin_map.find( var_vec.at(0) ) != bin_map.end() ){
        for ( std::string pair : pairs ){
          split_strings(bounds, pair, ",");
          std::map< std::string, std::vector<double> >::iterator itr;
          itr = bin_map.find( var_vec.at(0) );
          itr->second = { std::stod( bounds.at(0) ), std::stod( bounds.at(1) ), std::stod( bounds.at(2) ) };
          break;
        }
      }
    }
  }

  std::map< std::string, std::string > draw_vars;
  draw_vars["DiMuonPt"]  = "DiMuonPt";
  draw_vars["PhotonPt"]  = "PhotonPt";
  draw_vars["ActIpX"]    = "ActIpX";
  draw_vars["AvgIpX"]    = "AvgIpX";
  draw_vars["Mass"]      = "3.096*sqrt(Lambda)";
  draw_vars["Phi"]       = "abs(Phi)";
  draw_vars["z"]         = "abs(costheta)";
  draw_vars["JPsi_Eta"]  = "JPsi_Eta";
  draw_vars["Phot_Eta"]  = "Phot_Eta";
  draw_vars["qtA"]  = "qtA";
  
  std::map< std::string, std::string > output_names;
  output_names["DiMuonPt"] = "JPsi_Pt";
  output_names["ActIpX"] = "ActIpX";
  output_names["AvgIpX"] = "AvgIpX";
  output_names["Mass"] = "MInv";
  output_names["PhotonPt"] = "PhotonPt";
  output_names["Phi"]   = "PhiCS";
  output_names["z"]   = "costheta";
  output_names["JPsi_Eta"]   = "JPsi_Eta";
  output_names["Phot_Eta"]   = "Phot_Eta";
  output_names["qtA"]   = "qtA";


  std::map<std::string,std::string> units;
  units["qtA"]            =  "GeV";
  units["DiMuonPt"]       =  "GeV";
  units["abs(qtB)"]       =  "GeV";
  units["Phi"]            =  "rad";
  units["ActIpX"]         =  "no.";
  units["AvgIpX"]         =  "no.";
  units["MInv"]           =  "GeV";
  units["PhotonPt"]       =  "GeV";
  units["z"]              =  "rad";
  units["JPsi_Eta"]       =  "rap";
  units["Phot_Eta"]       =  "rap";
  units["qtA"]       =  "GeV";

  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip;  wip.SetNDC(); wip.SetTextFont(42);
  wip.SetTextSize(0.038); wip.SetTextColor(1);

  std::vector< std::string > sign_vec;
  std::vector< std::string > data_vec;
  split_strings( sign_vec, sign, ":" );
  split_strings( data_vec, data, ":" );
  std::string sign_file_str = sign_vec.at(0);
  std::string sign_tree_str = sign_vec.at(1);
  std::string data_file_str = data_vec.at(0);
  std::string data_tree_str = data_vec.at(1);


  TFile * sign_file = new TFile( sign_file_str.c_str(), "READ" ); 
  TFile * data_file = new TFile( data_file_str.c_str(), "READ" ); 

  TTree * sign_tree = (TTree*) sign_file->Get( sign_tree_str.c_str() );
  TTree * data_tree = (TTree*) data_file->Get( data_tree_str.c_str() );

  TFile * weight_file = new TFile( Form( "./%s/weights.root", unique.c_str() ), "RECREATE" );


  std::vector< int > masses = { 0, 3, 4, 5, 12 }; 
  
  for ( int & mass : masses ){
    
    std::string & mass_range = mass_ranges.find( mass )->second;

    for ( std::string & var : var_vec ){
    
      int bins        = (int) bin_map[var].at(0); 
      double bin_min  = bin_map[var].at(1);
      double bin_max  = bin_map[var].at(2);
      double width =  (double) (bin_max-bin_min)/((double) bins);
      std::string unit = units[var];
      std::string safe_name = output_names[var];
      std::string out_name = output_names[var];
      std::string draw_var = draw_vars[var];

      std::string sign_hist_name = "sign_Q" + std::to_string(mass) + "_"  + out_name;
      std::string data_hist_name = "data_Q" + std::to_string(mass) + "_"  + out_name;
      TH1F * sign_hist = new TH1F( sign_hist_name.c_str(), out_name.c_str(), bins, bin_min, bin_max );
      TH1F * data_hist = new TH1F( data_hist_name.c_str(), out_name.c_str(), bins, bin_min, bin_max );

      int   qta_bins        = (int) bin_map["qtA"].at(0); 
      double qta_min  = bin_map["qtA"].at(1);
      double qta_max  = bin_map["qtA"].at(2);
      double qta_width =  (double) (qta_max-qta_min)/((double) qta_bins);
      std::string qta_hist_name = "qta_Q" + std::to_string(mass) + "_"  + out_name;
      TH1F * qta_hist = new TH1F( qta_hist_name.c_str(), out_name.c_str(), qta_bins, qta_min, qta_max );

      for ( int qta_bin = 1; qta_bin <= qta_bins; qta_bin++ ){
        std::string temp_hist_name = "temp_Q" + std::to_string(mass) + "_" + out_name + "_qtA-" +
          std::to_string(qta_bin);
        TH1F * temp_weight_hist = new TH1F( temp_hist_name.c_str(), out_name.c_str(), bins, bin_min, bin_max ); 
        double qta_lower = qta_min+((qta_bin-1.0)*qta_width);
        double qta_upper = qta_lower + qta_width;
        std::string qta_cut = Form( "qtA>%f&&qtA<%f&&%s", qta_lower, qta_upper, mass_range.c_str() );
        sign_tree->Draw( Form("%s>>%s",draw_var.c_str(), sign_hist_name.c_str()), qta_cut.c_str(), "goff" );
        data_tree->Draw( Form("%s>>%s",draw_var.c_str(), data_hist_name.c_str()), qta_cut.c_str(), "goff" );
        temp_weight_hist->Divide( data_hist, sign_hist, 1.0, 1.0 );
        double mean{0}, rms{0};
        for ( int t_bin = 1; t_bin <= bins; t_bin++ ){
          mean += temp_weight_hist->GetBinContent( t_bin );
          rms += pow( temp_weight_hist->GetBinContent( t_bin ), 2 );
        }
        mean = mean/((double) bins);
        rms = rms/((double) bins);
        rms = std::sqrt(rms);
        qta_hist->SetBinContent( qta_bin, mean );
        qta_hist->SetBinError( qta_bin, rms );
      }

      sign_tree->Draw( Form("%s>>%s",draw_var.c_str(), sign_hist_name.c_str()), mass_range.c_str(), "goff" );
      data_tree->Draw( Form("%s>>%s",draw_var.c_str(), data_hist_name.c_str()), mass_range.c_str(), "goff" );
      sign_hist->Sumw2();
      data_hist->Sumw2();
      TH1F * weight_hist = new TH1F( Form("%s_Q%i_weight", var.c_str(), mass), "", bins, bin_min, bin_max );

      double data_int = data_hist->Integral();
      double sign_int = sign_hist->Integral();
      data_hist->Scale(1.0/data_int);
      sign_hist->Scale(1.0/sign_int);
      weight_hist->Divide( data_hist, sign_hist, 1.0, 1.0 );
      data_hist->Scale(data_int);
      sign_hist->Scale(sign_int);
      double axis_lim = std::ceil( weight_hist->GetMaximum()*1.3 );
      double r_axis_bound = 1.3*std::max( data_hist->GetMaximum(), sign_hist->GetMaximum() );
      double r_scale = axis_lim/r_axis_bound;
      data_hist->Scale(r_scale);
      sign_hist->Scale(r_scale);


      TCanvas * canv = new TCanvas("canv","", 100, 100, 2000, 2000 );
      canv->Divide(1,1);
      canv->cd(1);

      weight_hist->Draw("E1");
      weight_hist->SetMarkerStyle(21);
      weight_hist->SetLineColor(1);
      weight_hist->GetYaxis()->SetRangeUser(0,axis_lim);
      weight_hist->GetYaxis()->SetTitle( Form("Data/MC Weight/%.3f %s", width, unit.c_str()));
      weight_hist->GetXaxis()->SetTitle( Form("%s (%s)", var.c_str(), unit.c_str()));
      weight_hist->GetYaxis()->SetLabelSize(0.035);
      weight_hist->GetYaxis()->SetTitleSize(0.035);
      weight_hist->GetXaxis()->SetLabelSize(0.035);
      weight_hist->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      gPad->SetTicks(1,0);

      sign_hist->SetLineWidth(0);
      sign_hist->SetFillColorAlpha(kRed+1,0.5);
      data_hist->SetLineWidth(0);
      data_hist->SetFillColorAlpha(kBlue+1,0.5);
      sign_hist->Draw("HIST SAME");
      data_hist->Draw("HIST SAME");
      //TGaxis * pad_axis = new TGaxis(bin_max,0,bin_max,axis_lim,0,pad_scale,510,"+L");
      TGaxis * pad_axis = new TGaxis(bin_max,0,bin_max,axis_lim,0,r_axis_bound,510,"+L");
      pad_axis->SetLabelSize( 0.035 );             
      pad_axis->SetLabelFont( 42 );                
      pad_axis->SetTitleSize( 0.035 );
      pad_axis->SetTitleOffset( 1.25 );
      pad_axis->SetMaxDigits( 4 );
      pad_axis->SetTitleFont( 42 );                
      pad_axis->SetTitle( Form( "Entries/%.3f %s", width, unit.c_str() ));
      pad_axis->Draw();
      TLegend * pad_legend = new TLegend( 0.65, 0.65, 0.83, 0.8 );
      pad_legend->SetBorderSize( 0 );
      pad_legend->SetFillColor( 0 );
      pad_legend->SetFillStyle( 0 );
      pad_legend->SetTextFont( 42 );
      pad_legend->SetTextSize( 0.025 );
      pad_legend->AddEntry( weight_hist, "Weight", "LP");
      pad_legend->AddEntry( sign_hist, "Signal", "F");
      pad_legend->AddEntry( data_hist, "Data", "F");
      pad_legend->Draw();
      TLatex pad_ltx;
      pad_ltx.SetTextSize( 0.03 );
      pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Weight - Mass Q%i", safe_name.c_str(), mass ));  
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

      std::string save_string;
      canv->SaveAs( Form( "./%s/%s_Q%i_weight.png", unique.c_str(), out_name.c_str(), mass ));

      weight_file->cd();
      weight_hist->Write();
      qta_hist->Write();

      delete canv;
      delete weight_hist;
    }
  }
  weight_file->Close();
}



