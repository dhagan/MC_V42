
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"

void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  std::vector<std::string> tree_variables = {"BDT"};
  std::vector<double> low_edges{-1.0,-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};//,-5};
  std::vector<double> high_edges{1.0, M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};//,15};

  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};

  std::vector<int> k_vec = {1,100};
  std::vector<int> n_vec = {100,200};
  
  TFile sign_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_signal/Evaluate_signal.root","READ"};
  TFile bckg_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_pp.root","READ"};
  TFile data_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_data/Evaluate_data.root","READ"};
  TFile n200_k1_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n200_k1.root","READ"};
  TFile n100_k100_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_ppmix_n100_k100.root","READ"};


  TTree * sign_tree               = (TTree*) sign_file.Get("signal_00_mu4000_P5000_bound-0");
  TTree * bckg_tree               = (TTree*) bckg_file.Get("pp_00_mu4000_P5000_bound-0");
  TTree * data_tree               = (TTree*) data_file.Get("data_00_mu4000_P5000_bound-0");
  TTree * n200_k1_tree           = (TTree*) n200_k1_file.Get("pp-mix_n200_k1_00_mu4000_P5000_bound-0");
  TTree * n100_k100_tree          = (TTree*) n100_k100_file.Get("pp-mix_n100_k100_00_mu4000_P5000_bound-0");

  int n,k;    
  for (int n_idx = 0; n_idx < n_vec.size(); n_idx++){
    for (int k_idx = 0; k_idx < k_vec.size(); k_idx++){

      n = n_vec[n_idx];
      k = k_vec[k_idx];

      TFile new_sign_file           {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_signal_train_n%ik%i.root",n,k,n,k),"READ"};
      TFile new_bckg_file           {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_pp_train_n%ik%i.root",n,k,n,k),"READ"};
      TFile new_data_file           {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_data_train_n%ik%i.root",n,k,n,k),"READ"};
      TFile new_n200_k1_file       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_ppmix_n200_k1_train_n%i_k%i.root",n,k,n,k),"READ"};
      TFile new_n100_k100_file      {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_ppmix_n100_k100_train_n%i_k%i.root",n,k,n,k),"READ"};

      TFile new_sign_file_ll           {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_signal_train_ll_n%ik%i.root",n,k,n,k),"READ"};
      TFile new_bckg_file_ll           {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_pp_train_ll_n%ik%i.root",n,k,n,k),"READ"};
      TFile new_data_file_ll           {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_data_train_ll_n%ik%i.root",n,k,n,k),"READ"};
      TFile new_n200_k1_file_ll       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_ppmix_n200_k1_train_ll_n%i_k%i.root",n,k,n,k),"READ"};
      TFile new_n100_k100_file_ll      {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_nk/eval/n%i_k%i/Eval_ppmix_n100_k100_train_ll_n%i_k%i.root",n,k,n,k),"READ"};

      TTree * new_sign_tree = (TTree*) new_sign_file.Get("signal_00_mu4000_P5000_bound-0");
      TTree * new_bckg_tree = (TTree*) new_bckg_file.Get("pp_00_mu4000_P5000_bound-0");
      TTree * new_data_tree = (TTree*) new_data_file.Get("data_00_mu4000_P5000_bound-0");
      TTree * new_n200_k1_tree = (TTree*) new_n200_k1_file.Get("pp-mix_n200_k1_00_mu4000_P5000_bound-0");
      TTree * new_n100_k100_tree = (TTree*) new_n100_k100_file.Get("pp-mix_n100_k100_00_mu4000_P5000_bound-0");

      TTree * new_sign_tree_ll = (TTree*) new_sign_file_ll.Get("signal_00_mu4000_P5000_bound-0");
      TTree * new_bckg_tree_ll = (TTree*) new_bckg_file_ll.Get("pp_00_mu4000_P5000_bound-0");
      TTree * new_data_tree_ll = (TTree*) new_data_file_ll.Get("data_00_mu4000_P5000_bound-0");
      TTree * new_n200_k1_tree_ll = (TTree*) new_n200_k1_file_ll.Get("pp-mix_n200_k1_00_mu4000_P5000_bound-0");
      TTree * new_n100_k100_tree_ll = (TTree*) new_n100_k100_file_ll.Get("pp-mix_n100_k100_00_mu4000_P5000_bound-0");

      for (int q_idx = 0; q_idx <= 10; q_idx++){
                                                 
        int qta_lower; 
        int qta_upper;
        
        if (q_idx == 0){
          qta_lower = -5;
          qta_upper = 15; 
        } else {
          qta_lower = (2*(q_idx-1)) - 5;
          qta_upper = (2*(q_idx-1)) - 3;
        }

        for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 
          
          char cut[200];
          sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
          //std::cout << cut << std::endl;
          //std::cout << mass_ranges[mass_range] << std::endl;

          int bins = 20;

          TH1F * sign_hist            = new TH1F(Form("sign_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * bckg_hist            = new TH1F(Form("bckg_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * data_hist            = new TH1F(Form("data_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * n200_k1_hist         = new TH1F(Form("n200k1_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * n100_k100_hist       = new TH1F(Form("n100k100_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);

          TH1F * new_sign_hist        = new TH1F(Form("nsign_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_bckg_hist        = new TH1F(Form("nbckg_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_data_hist        = new TH1F(Form("ndata_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_n200_k1_hist     = new TH1F(Form("nn200k1_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_n100_k100_hist   = new TH1F(Form("nn100k100_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);

          TH1F * new_sign_hist_ll        = new TH1F(Form("nsign_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_bckg_hist_ll        = new TH1F(Form("nbckg_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_data_hist_ll        = new TH1F(Form("ndata_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_n200_k1_hist_ll     = new TH1F(Form("nn200k1_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);
          TH1F * new_n100_k100_hist_ll   = new TH1F(Form("nn100k100_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),Form("hist_n%i_k%i",n,k,mass_range),bins,-1.0,1.0);

          sign_tree->Draw(Form("BDT>>sign_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          bckg_tree->Draw(Form("BDT>>bckg_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          data_tree->Draw(Form("BDT>>data_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          n200_k1_tree->Draw(Form("BDT>>n200k1_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          n100_k100_tree->Draw(Form("BDT>>n100k100_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");

          new_sign_tree->Draw(Form("BDT>>nsign_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_bckg_tree->Draw(Form("BDT>>nbckg_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_data_tree->Draw(Form("BDT>>ndata_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_n200_k1_tree->Draw(Form("BDT>>nn200k1_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_n100_k100_tree->Draw(Form("BDT>>nn100k100_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");

          new_sign_tree_ll->Draw(Form("BDT>>nsign_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_bckg_tree_ll->Draw(Form("BDT>>nbckg_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_data_tree_ll->Draw(Form("BDT>>ndata_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_n200_k1_tree_ll->Draw(Form("BDT>>nn200k1_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");
          new_n100_k100_tree_ll->Draw(Form("BDT>>nn100k100_ll_n%i_k%i_Q%i_qta%i",n,k,mass_range,q_idx),cut,"goff");

          sign_hist            ->Sumw2(); 
          bckg_hist            ->Sumw2();
          data_hist            ->Sumw2();
          n200_k1_hist         ->Sumw2();
          n100_k100_hist       ->Sumw2();

          new_sign_hist        ->Sumw2();
          new_bckg_hist        ->Sumw2();
          new_data_hist        ->Sumw2();
          new_n200_k1_hist     ->Sumw2();
          new_n100_k100_hist   ->Sumw2();

          new_sign_hist_ll     ->Sumw2();
          new_bckg_hist_ll     ->Sumw2();
          new_data_hist_ll     ->Sumw2();
          new_n200_k1_hist_ll  ->Sumw2();
          new_n100_k100_hist_ll->Sumw2();

          for (int bin = 0; bin < bins; bin++){
            
            sign_hist            ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            bckg_hist            ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            data_hist            ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            n200_k1_hist         ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            n100_k100_hist       ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 

            new_sign_hist        ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_bckg_hist        ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_data_hist        ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_n200_k1_hist     ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_n100_k100_hist   ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 

            new_sign_hist_ll     ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_bckg_hist_ll     ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_data_hist_ll     ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_n200_k1_hist_ll  ->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 
            new_n100_k100_hist_ll->SetBinError(bin,sqrt((double) sign_hist->GetBinContent(bin))); 

          }

          TCanvas * canv = new TCanvas(Form("canv%i%i%i%i",n,k,mass_range,q_idx),"",200,100,5000,2000);
          canv->Divide(5,3);

          canv->cd(1);
          data_hist->Scale(1.0/data_hist->Integral());
          new_data_hist->Scale(1.0/new_data_hist->Integral());
          new_data_hist_ll->Scale(1.0/new_data_hist_ll->Integral());
          data_hist->Draw("HIST");
          new_data_hist->Draw("SAME HIST");
          new_data_hist_ll->Draw("SAME HIST");
          //data_hist->Draw("SAME E1");
          new_data_hist->Draw("SAME E1");
          new_data_hist_ll->Draw("SAME E1");
          //data_hist->GetYaxis()->SetRangeUser(0,1.5*std::max(data_hist->GetMaximum(),new_data_hist->GetMaximum()));
          data_hist->GetYaxis()->SetRangeUser(0,0.4);
          data_hist->GetXaxis()->SetTitle("BDT Score");
          data_hist->GetYaxis()->SetTitle("Entries / 0.04");
          data_hist->SetLineColor(1);
          new_data_hist->SetLineColorAlpha(1,0.8);
          new_data_hist->SetLineStyle(2);
          new_data_hist_ll->SetLineColorAlpha(1,0.6);
          new_data_hist_ll->SetLineStyle(3);

          
          TPaveText * data_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          data_text->AddText("Data");
          data_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          data_text->AddText(Form("qtA%i",q_idx));
          data_text->SetBorderSize(0);
          data_text->SetFillStyle(0);
          data_text->SetFillColor(0);
          data_text->SetTextFont(42);
          data_text->SetTextSize(0.03);
          data_text->SetTextAlign(31);
          data_text->Draw();
          
          TLegend * data_legend = new TLegend(0.225,0.8,0.7,0.925,"Data");
          data_legend->AddEntry(data_hist,"Data, original pp training");
          data_legend->AddEntry(new_data_hist,Form("Data, n%i_k%i training",n,k));
          data_legend->AddEntry(new_data_hist_ll,Form("Data, n%i_k%i low lambda training",n,k));
          data_legend->SetBorderSize(1);
          data_legend->SetTextFont(42);
          data_legend->SetFillStyle(0);
          data_legend->SetTextSize(0.02);
          data_legend->Draw(); 
          
          canv->cd(2);
          bckg_hist->Scale(1.0/bckg_hist->Integral());
          new_bckg_hist->Scale(1.0/new_bckg_hist->Integral());
          new_bckg_hist_ll->Scale(1.0/new_bckg_hist_ll->Integral());
          bckg_hist->Draw("HIST");
          new_bckg_hist->Draw("SAME HIST");
          new_bckg_hist_ll->Draw("SAME HIST");
          //bckg_hist->Draw("SAME E1");
          new_bckg_hist->Draw("SAME E1");
          new_bckg_hist_ll->Draw("SAME E1");
          //bckg_hist->GetYaxis()->SetRangeUser(0,1.5*std::max(bckg_hist->GetMaximum(),new_bckg_hist->GetMaximum()));
          bckg_hist->GetYaxis()->SetRangeUser(0,0.3);
          bckg_hist->GetXaxis()->SetTitle("BDT Score");
          bckg_hist->GetYaxis()->SetTitle("Entries / 0.04");
          bckg_hist->SetLineColor(4);
          new_bckg_hist->SetLineColorAlpha(4,0.8);
          new_bckg_hist->SetLineStyle(2);
          new_bckg_hist_ll->SetLineColorAlpha(4,0.6);
          new_bckg_hist_ll->SetLineStyle(3);

          
          TPaveText * bckg_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          bckg_text->AddText("Background");
          bckg_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          bckg_text->AddText(Form("qtA%i",q_idx));
          bckg_text->SetBorderSize(0);
          bckg_text->SetFillStyle(0);
          bckg_text->SetFillColor(0);
          bckg_text->SetTextFont(42);
          bckg_text->SetTextSize(0.03);
          bckg_text->SetTextAlign(31);
          bckg_text->Draw();
          
          TLegend * bckg_legend = new TLegend(0.225,0.8,0.7,0.925);
          bckg_legend->AddEntry(bckg_hist,"Bckg, original pp training");
          bckg_legend->AddEntry(new_bckg_hist,Form("Bckg, n%i_k%i training",n,k));
          bckg_legend->AddEntry(new_bckg_hist_ll,Form("Bckg, n%i_k%i low lambda training",n,k));
          bckg_legend->SetBorderSize(1);
          bckg_legend->SetTextFont(42);
          bckg_legend->SetFillStyle(0);
          bckg_legend->SetTextSize(0.02);
          bckg_legend->Draw(); 
          
          canv->cd(3);
          sign_hist->Scale(1.0/sign_hist->Integral());
          new_sign_hist->Scale(1.0/new_sign_hist->Integral());
          new_sign_hist_ll->Scale(1.0/new_sign_hist_ll->Integral());
          sign_hist->Draw("HIST");
          new_sign_hist->Draw("SAME HIST");
          new_sign_hist_ll->Draw("SAME HIST");
          //sign_hist->Draw("SAME E1");
          new_sign_hist->Draw("SAME E1");
          new_sign_hist_ll->Draw("SAME E1");
          //sign_hist->GetYaxis()->SetRangeUser(0,1.5*std::max(sign_hist->GetMaximum(),new_sign_hist->GetMaximum()));
          sign_hist->GetYaxis()->SetRangeUser(0,0.3);
          sign_hist->GetXaxis()->SetTitle("BDT Score");
          sign_hist->GetYaxis()->SetTitle("Entries / 0.04");
          sign_hist->SetLineColor(2);
          new_sign_hist->SetLineColorAlpha(2,0.8);
          new_sign_hist->SetLineStyle(2);
          new_sign_hist_ll->SetLineColorAlpha(2,0.6);
          new_sign_hist_ll->SetLineStyle(3);

          TPaveText * sign_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          sign_text->AddText("Signal");
          sign_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          sign_text->AddText(Form("qtA%i",q_idx));
          sign_text->SetBorderSize(0);
          sign_text->SetFillStyle(0);
          sign_text->SetFillColor(0);
          sign_text->SetTextFont(42);
          sign_text->SetTextSize(0.03);
          sign_text->SetTextAlign(31);
          sign_text->Draw();
          
          TLegend * sign_legend = new TLegend(0.225,0.8,0.7,0.925);
          sign_legend->AddEntry(sign_hist,"Signal, original pp training");
          sign_legend->AddEntry(new_sign_hist,Form("Signal, n%i_k%i training",n,k));
          sign_legend->AddEntry(new_sign_hist_ll,Form("Signal, n%i_k%i low lambda training",n,k));
          sign_legend->SetBorderSize(1);
          sign_legend->SetTextFont(42);
          sign_legend->SetFillStyle(0);
          sign_legend->SetTextSize(0.02);
          sign_legend->Draw(); 

          canv->cd(4);
          n200_k1_hist->Scale(1.0/n200_k1_hist->Integral());
          new_n200_k1_hist->Scale(1.0/new_n200_k1_hist->Integral());
          new_n200_k1_hist_ll->Scale(1.0/new_n200_k1_hist_ll->Integral());
          n200_k1_hist->Draw("HIST");
          new_n200_k1_hist->Draw("SAME HIST");
          new_n200_k1_hist_ll->Draw("SAME HIST");
          //n200_k1_hist->Draw("SAME E1");
          new_n200_k1_hist->Draw("SAME E1");
          new_n200_k1_hist_ll->Draw("SAME E1");
          //n200_k1_hist->GetYaxis()->SetRangeUser(0,1.5*std::max(n200_k1_hist->GetMaximum(),new_n200_k1_hist->GetMaximum()));
          n200_k1_hist->GetYaxis()->SetRangeUser(0,0.3);
          n200_k1_hist->GetXaxis()->SetTitle("BDT Score");
          n200_k1_hist->GetYaxis()->SetTitle("Entries / 0.04");
          n200_k1_hist->SetLineColor(8);
          new_n200_k1_hist->SetLineColorAlpha(8,0.8);
          new_n200_k1_hist->SetLineStyle(2);
          new_n200_k1_hist_ll->SetLineColorAlpha(8,0.6);
          new_n200_k1_hist_ll->SetLineStyle(3);                 

          TPaveText * n200_k1_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          n200_k1_text->AddText("n200_k1");
          n200_k1_text->AddText("1000 Original");
          n200_k1_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          n200_k1_text->AddText(Form("qtA%i",q_idx));
          n200_k1_text->SetBorderSize(0);
          n200_k1_text->SetFillStyle(0);
          n200_k1_text->SetFillColor(0);
          n200_k1_text->SetTextFont(42);
          n200_k1_text->SetTextSize(0.03);
          n200_k1_text->SetTextAlign(31);
          n200_k1_text->Draw();
          
          TLegend * n200_k1_legend = new TLegend(0.225,0.8,0.7,0.925);
          n200_k1_legend->AddEntry(n200_k1_hist,"n200_k1, original pp training");
          n200_k1_legend->AddEntry(new_n200_k1_hist,Form("n200_k1, n%i_k%i training",n,k));
          n200_k1_legend->AddEntry(new_n200_k1_hist_ll,Form("n200_k1, n%i_k%i low lambda training",n,k));
          n200_k1_legend->SetBorderSize(1);
          n200_k1_legend->SetTextFont(42);
          n200_k1_legend->SetFillStyle(0);
          n200_k1_legend->SetTextSize(0.02);
          n200_k1_legend->Draw(); 
        
          canv->cd(5);
          n100_k100_hist->Scale(1.0/n100_k100_hist->Integral());
          new_n100_k100_hist->Scale(1.0/new_n100_k100_hist->Integral());
          new_n100_k100_hist_ll->Scale(1.0/new_n100_k100_hist_ll->Integral());
          n100_k100_hist->Draw("HIST");
          new_n100_k100_hist->Draw("SAME HIST");
          new_n100_k100_hist_ll->Draw("SAME HIST");
          //n100_k100_hist->Draw("SAME E1");
          new_n100_k100_hist->Draw("SAME E1");
          new_n100_k100_hist_ll->Draw("SAME E1");
          //n100_k100_hist->GetYaxis()->SetRangeUser(0,1.5*std::max(n100_k100_hist->GetMaximum(),new_n100_k100_hist->GetMaximum()));
          n100_k100_hist->GetYaxis()->SetRangeUser(0,0.3);
          n100_k100_hist->GetXaxis()->SetTitle("BDT Score");
          n100_k100_hist->GetYaxis()->SetTitle("Entries / 0.04");
          n100_k100_hist->SetLineColor(6);
          new_n100_k100_hist->SetLineColorAlpha(6,0.8);
          new_n100_k100_hist->SetLineStyle(2);
          new_n100_k100_hist_ll->SetLineColorAlpha(6,0.6);
          new_n100_k100_hist_ll->SetLineStyle(3);

          TPaveText * n100_k100_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          n100_k100_text->AddText("n100_k100");
          n100_k100_text->AddText("1000 Original");
          n100_k100_text->AddText(Form("qtA%i",q_idx));
          n100_k100_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          n100_k100_text->SetBorderSize(0);
          n100_k100_text->SetFillStyle(0);
          n100_k100_text->SetFillColor(0);
          n100_k100_text->SetTextFont(42);
          n100_k100_text->SetTextSize(0.03);
          n100_k100_text->SetTextAlign(31);
          n100_k100_text->Draw();
          
          TLegend * n100_k100_legend = new TLegend(0.225,0.8,0.7,0.925);
          n100_k100_legend->AddEntry(n100_k100_hist,"n100_k100, original pp training");
          n100_k100_legend->AddEntry(new_n100_k100_hist,Form("n100_k100, n%i_k%i training",n,k));
          n100_k100_legend->AddEntry(new_n100_k100_hist_ll,Form("n100_k100, n%i_k%i low lambda training",n,k));
          n100_k100_legend->SetBorderSize(1);
          n100_k100_legend->SetTextFont(42);
          n100_k100_legend->SetFillStyle(0);
          n100_k100_legend->SetTextSize(0.02);
          n100_k100_legend->Draw(); 

          // original
          canv->cd(6);
          data_hist->Draw("HIST");
          sign_hist->Draw("SAME HIST");
          bckg_hist->Draw("SAME HIST");
          n200_k1_hist->Draw("SAME HIST");
          n100_k100_hist->Draw("SAME HIST");
          data_hist->GetYaxis()->SetRangeUser(0,0.3);
          data_hist->GetXaxis()->SetTitle("BDT Score");
          data_hist->GetYaxis()->SetTitle("Entries / 0.04");
          //data_hist->SetLineColor(1);
          //sign_hist->SetLineColor(2);
          //bckg_hist->SetLineColor(4);
          //n100_k100_hist->SetLineColorAlpha(1,0.8);
          //n100_k100_hist->SetLineStyle(2);
          //n200_k1_hist->SetLineColorAlpha(1,0.6);
          //n200_k1_hist->SetLineStyle(3);

          TPaveText * overlay_original_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          overlay_original_text->AddText("Overlay");
          overlay_original_text->AddText("Original pp training");
          overlay_original_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          overlay_original_text->AddText(Form("qtA%i",q_idx));
          overlay_original_text->SetBorderSize(0);
          overlay_original_text->SetFillStyle(0);
          overlay_original_text->SetFillColor(0);
          overlay_original_text->SetTextFont(42);
          overlay_original_text->SetTextSize(0.03);
          overlay_original_text->SetTextAlign(31);
          overlay_original_text->Draw();
          
          TLegend * overlay_original_legend = new TLegend(0.225,0.8,0.6,0.925,"Data");
          overlay_original_legend->AddEntry(data_hist,"Data");
          overlay_original_legend->AddEntry(sign_hist,"Signal");
          overlay_original_legend->AddEntry(bckg_hist,"PP");
          overlay_original_legend->AddEntry(n200_k1_hist,"n200 k1");
          overlay_original_legend->AddEntry(n100_k100_hist,"n100 k100");
          overlay_original_legend->SetBorderSize(1);
          overlay_original_legend->SetTextFont(42);
          overlay_original_legend->SetFillStyle(0);
          overlay_original_legend->SetTextSize(0.02);
          overlay_original_legend->Draw(); 

          // new training
          canv->cd(7);
          new_data_hist->Draw("HIST");
          new_sign_hist->Draw("SAME HIST");
          new_bckg_hist->Draw("SAME HIST");
          new_n200_k1_hist->Draw("SAME HIST");
          new_n100_k100_hist->Draw("SAME HIST");
          new_data_hist->GetYaxis()->SetRangeUser(0,0.3);
          new_data_hist->GetXaxis()->SetTitle("BDT Score");
          new_data_hist->GetYaxis()->SetTitle("Entries / 0.04");
          //new_data_hist->SetLineColor(1);
          //new_sign_hist->SetLineColor(2);
          //new_bckg_hist->SetLineColor(4);
          //new_n100_k100_hist->SetLineColorAlpha(1,0.8);
          //new_n100_k100_hist->SetLineStyle(2);
          //new_n200_k1_hist->SetLineColorAlpha(1,0.6);
          //new_n200_k1_hist->SetLineStyle(3);

          TPaveText * overlay_new_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          overlay_new_text->AddText("Overlay");
          overlay_new_text->AddText(Form("n%i_k%i training",n,k));
          overlay_new_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          overlay_new_text->AddText(Form("qtA%i",q_idx));
          overlay_new_text->SetBorderSize(0);
          overlay_new_text->SetFillStyle(0);
          overlay_new_text->SetFillColor(0);
          overlay_new_text->SetTextFont(42);
          overlay_new_text->SetTextSize(0.03);
          overlay_new_text->SetTextAlign(31);
          overlay_new_text->Draw();
          
          TLegend * overlay_new_legend = new TLegend(0.225,0.8,0.6,0.925,"Data");
          overlay_new_legend->AddEntry(new_data_hist,"Data");
          overlay_new_legend->AddEntry(new_sign_hist,"Signal");
          overlay_new_legend->AddEntry(new_bckg_hist,"PP");
          overlay_new_legend->AddEntry(new_n200_k1_hist,"n200 k1");
          overlay_new_legend->AddEntry(new_n100_k100_hist,"n100 k100");
          overlay_new_legend->SetBorderSize(1);
          overlay_new_legend->SetTextFont(42);
          overlay_new_legend->SetFillStyle(0);
          overlay_new_legend->SetTextSize(0.02);
          overlay_new_legend->Draw(); 

          // ll and new
          canv->cd(8);
          new_data_hist_ll->Draw("HIST");
          new_sign_hist_ll->Draw("SAME HIST");
          new_bckg_hist_ll->Draw("SAME HIST");
          new_n200_k1_hist_ll->Draw("SAME HIST");
          new_n100_k100_hist_ll->Draw("SAME HIST");
          new_data_hist_ll->GetYaxis()->SetRangeUser(0,0.3);
          new_data_hist_ll->GetXaxis()->SetTitle("BDT Score");
          new_data_hist_ll->GetYaxis()->SetTitle("Entries / 0.04");
          //new_data_hist_ll->SetLineColor(1);
          //new_sign_hist_ll->SetLineColor(2);
          //new_bckg_hist_ll->SetLineColor(4);
          //new_n100_k100_hist_ll->SetLineColorAlpha(1,0.8);
          //new_n100_k100_hist_ll->SetLineStyle(2);
          //new_n200_k1_hist_ll->SetLineColorAlpha(1,0.6);
          //new_n200_k1_hist_ll->SetLineStyle(3);

          TPaveText * overlay_ll_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          overlay_ll_text->AddText("Overlay");
          overlay_ll_text->AddText(Form("n%i_k%i training",n,k));
          overlay_ll_text->AddText("low lambda");
          overlay_ll_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          overlay_ll_text->AddText(Form("qtA%i",q_idx));
          overlay_ll_text->SetBorderSize(0);
          overlay_ll_text->SetFillStyle(0);
          overlay_ll_text->SetFillColor(0);
          overlay_ll_text->SetTextFont(42);
          overlay_ll_text->SetTextSize(0.03);
          overlay_ll_text->SetTextAlign(31);
          overlay_ll_text->Draw();
          
          TLegend * overlay_ll_legend = new TLegend(0.225,0.8,0.6,0.925,"Data");
          overlay_ll_legend->AddEntry(new_data_hist_ll,"Data");
          overlay_ll_legend->AddEntry(new_sign_hist_ll,"Signal");
          overlay_ll_legend->AddEntry(new_bckg_hist_ll,"PP");
          overlay_ll_legend->AddEntry(new_n200_k1_hist_ll,"n200 k1");
          overlay_ll_legend->AddEntry(new_n100_k100_hist_ll,"n100 k100");
          overlay_ll_legend->SetBorderSize(1);
          overlay_ll_legend->SetTextFont(42);
          overlay_ll_legend->SetFillStyle(0);
          overlay_ll_legend->SetTextSize(0.02);
          overlay_ll_legend->Draw(); 

          canv->cd(11);
          bckg_hist->Draw("HIST");
          n200_k1_hist->Draw("SAME HIST");
          n100_k100_hist->Draw("SAME HIST");
          bckg_hist->GetYaxis()->SetRangeUser(0,0.3);
          bckg_hist->GetXaxis()->SetTitle("BDT Score");
          bckg_hist->GetYaxis()->SetTitle("Entries / 0.04");

          TPaveText * overlay_bckg_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          overlay_bckg_text->AddText("Background Overlay");
          overlay_bckg_text->AddText("Original");
          overlay_bckg_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          overlay_bckg_text->AddText(Form("qtA%i",q_idx));
          overlay_bckg_text->SetBorderSize(0);
          overlay_bckg_text->SetFillStyle(0);
          overlay_bckg_text->SetFillColor(0);
          overlay_bckg_text->SetTextFont(42);
          overlay_bckg_text->SetTextSize(0.03);
          overlay_bckg_text->SetTextAlign(31);
          overlay_bckg_text->Draw();
          
          TLegend * overlay_bckg_legend = new TLegend(0.225,0.8,0.6,0.925,"Data");
          overlay_bckg_legend->AddEntry(bckg_hist,"Original");
          overlay_bckg_legend->AddEntry(n200_k1_hist,"n200 k1");
          overlay_bckg_legend->AddEntry(n100_k100_hist,"n100 k100");
          overlay_bckg_legend->SetBorderSize(1);
          overlay_bckg_legend->SetTextFont(42);
          overlay_bckg_legend->SetFillStyle(0);
          overlay_bckg_legend->SetTextSize(0.02);
          overlay_bckg_legend->Draw(); 

          canv->cd(12);
          new_bckg_hist->Draw("HIST");
          new_n200_k1_hist->Draw("SAME HIST");
          new_n100_k100_hist->Draw("SAME HIST");
          new_bckg_hist->GetYaxis()->SetRangeUser(0,0.3);
          new_bckg_hist->GetXaxis()->SetTitle("BDT Score");
          new_bckg_hist->GetYaxis()->SetTitle("Entries / 0.04");
          
          TPaveText * overlay_new_bckg_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          overlay_new_bckg_text->AddText("Background Overlay");
          overlay_new_bckg_text->AddText(Form("n%i k%i training",n,k));
          overlay_new_bckg_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          overlay_new_bckg_text->AddText(Form("qtA%i",q_idx));
          overlay_new_bckg_text->SetBorderSize(0);
          overlay_new_bckg_text->SetFillStyle(0);
          overlay_new_bckg_text->SetFillColor(0);
          overlay_new_bckg_text->SetTextFont(42);
          overlay_new_bckg_text->SetTextSize(0.03);
          overlay_new_bckg_text->SetTextAlign(31);
          overlay_new_bckg_text->Draw();
          
          TLegend * overlay_new_bckg_legend = new TLegend(0.225,0.8,0.6,0.925,"Data");
          overlay_new_bckg_legend->AddEntry(new_bckg_hist,"Original");
          overlay_new_bckg_legend->AddEntry(new_n200_k1_hist,"n200 k1");
          overlay_new_bckg_legend->AddEntry(new_n100_k100_hist,"n100 k100");
          overlay_new_bckg_legend->SetBorderSize(1);
          overlay_new_bckg_legend->SetTextFont(42);
          overlay_new_bckg_legend->SetFillStyle(0);
          overlay_new_bckg_legend->SetTextSize(0.02);
          overlay_new_bckg_legend->Draw(); 



          canv->cd(13);
          new_bckg_hist_ll->Draw("HIST");
          new_n200_k1_hist_ll->Draw("SAME HIST");
          new_n100_k100_hist_ll->Draw("SAME HIST");
          new_bckg_hist_ll->GetYaxis()->SetRangeUser(0,0.3);
          new_bckg_hist_ll->GetXaxis()->SetTitle("BDT Score");
          new_bckg_hist_ll->GetYaxis()->SetTitle("Entries / 0.04");

          TPaveText * overlay_new_ll_bckg_text = new TPaveText(0.8,0.7,0.925,0.925,"nbNDC");
          overlay_new_ll_bckg_text->AddText("Background Overlay");
          overlay_new_ll_bckg_text->AddText(Form("n%i k%i training",n,k));
          overlay_new_ll_bckg_text->AddText("Low lambda");
          overlay_new_ll_bckg_text->AddText(Form("%s",mass_cuts[mass_range].c_str()));
          overlay_new_ll_bckg_text->AddText(Form("qtA%i",q_idx));
          overlay_new_ll_bckg_text->SetBorderSize(0);
          overlay_new_ll_bckg_text->SetFillStyle(0);
          overlay_new_ll_bckg_text->SetFillColor(0);
          overlay_new_ll_bckg_text->SetTextFont(42);
          overlay_new_ll_bckg_text->SetTextSize(0.03);
          overlay_new_ll_bckg_text->SetTextAlign(31);
          overlay_new_ll_bckg_text->Draw();
          
          TLegend * overlay_new_ll_bckg_legend = new TLegend(0.225,0.8,0.6,0.925,"Data");
          overlay_new_ll_bckg_legend->AddEntry(new_bckg_hist_ll,"Original");
          overlay_new_ll_bckg_legend->AddEntry(new_n200_k1_hist_ll,"n200 k1");
          overlay_new_ll_bckg_legend->AddEntry(new_n100_k100_hist_ll,"n100 k100");
          overlay_new_ll_bckg_legend->SetBorderSize(1);
          overlay_new_ll_bckg_legend->SetTextFont(42);
          overlay_new_ll_bckg_legend->SetFillStyle(0);
          overlay_new_ll_bckg_legend->SetTextSize(0.02);
          overlay_new_ll_bckg_legend->Draw(); 

          overlay_new_bckg_text->AddText("low lambda");

          canv->cd(9);



          canv->SaveAs(Form("./ppmix_focussed_training_n%i_k%i_%s_qta%i.png",n,k,mass_cuts[mass_range].c_str(),q_idx));

        }
      }
    }
  }
}

int main(){
  check(); 
}
