//  root -l Plots.cxx+g
//   . /afs/cern.ch/user/a/atee/root_6_script.sh

// IF THERE IS NO MC IN THE NAME IT IS A RECO VARIABLE!!!!

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TLorentzVector.h"
#include "RooCategory.h"
#include "Roo1DTable.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCBShape.h"
#include "RooAbsReal.h"
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooPolynomial.h"
#include "math.h"
#include "assert.h"
#include "TLegend.h"
#include "stdio.h"


//#ifdef __MAKECINT__
//#pragma link C++ class vector<float>+;
//#pragma link C++ class vector<vector<int> >+;
//#endif

#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <math.h>

// use this order for safety on library loading
using namespace RooFit ;
using namespace RooStats ;
using namespace std;

#include "treeReader.C"
//#include "treeReader.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>
#include "TString.h"
#include <math.h>

#include <cassert>


#include "defs.h"
#include "BphysEffTool.cxx"

#include <iostream>
using namespace std;

class PhotonSF {
    public:
        PhotonSF();

        double SF(double pt, double eta) const ; // pt_in_MeV
        double SF_uncertainty(double pt, double eta) const ; // pt_in_MeV

    private:

    int getBin(double pt, double eta) const;

    TFile * m_fii_id_tight_uncov = nullptr;
    TH2*    m_h_id_tight_uncov   = nullptr;

    double m_ptmin = 10e3, m_ptmax=2e6;
    double m_etamin = 0. , m_etamax = 2.37;

    double m_crackmin = 1.37, m_crackmax = 1.52;

};

PhotonSF::PhotonSF() {
    m_fii_id_tight_uncov = TFile::Open("Summer2020_Rec_v1/efficiencySF.offline.Tight.13TeV.rel21.25ns.unc.v01.root");
    m_h_id_tight_uncov   =  dynamic_cast<TH2*>(m_fii_id_tight_uncov->Get("0_99999999999/FullSim_sf"));
}

int PhotonSF::getBin(double pt, double eta) const {
        double abseta = fabs(eta); 
    if (pt <= m_ptmin) pt = m_ptmin + 0.1;
    if (pt >=  m_ptmax) pt = m_ptmax - 0.1;

    if (abseta <= m_etamin) abseta =  m_etamin + 0.01;
    if (abseta >=  m_etamax) abseta =  m_etamax - 0.01;

    if (abseta >= m_crackmin && abseta <= m_crackmax) {
        std::cerr << "In crack" << std::endl;
        abseta = m_crackmin - 0.01;     
    }

    int bin = m_h_id_tight_uncov->FindBin(pt,abseta);
    return bin;
}




double PhotonSF::SF(double pt, double eta) const {
    int bin = getBin(pt, eta);
    return m_h_id_tight_uncov->GetBinContent(bin);
}

double PhotonSF::SF_uncertainty(double pt, double eta) const {
    int bin = getBin(pt, eta);
    return m_h_id_tight_uncov->GetBinError(bin);
}






void signalPlots() {//OPENING BRACE

  BphysEffTool* bphysEffTool = new BphysEffTool();
  PhotonSF photonSF; // for the photon scale factors



  std::vector<std::string> photonCut = {"9000.0"};

  for (int i=0; i < photonCut.size(); i++){

    int pCut = std::stoi(photonCut.at(i) );
    std::cout << pCut << std::endl;


    //Creating a chain of all the trees in the various ntuples
    TChain *ch = new TChain("tree");


    //Adding the ntuples to the chain
    ch->Add("/home/atlas/amytee/PhD_Work_Analysis_III/MC_V21/ntuples/signal_ntuple.root");
		  

    //declare a new reader
    treeReader *t = new treeReader(ch);
  
    typedef std::vector<std::pair<double,double> > Ranges;

  
    Ranges MassRange = {{2700.0, 3500.0} }; // Various J/psi mass intervals
    Ranges LambdaRange = { {0.0, 200.0}, {0.0, 15.0}, {15.0, 25.0}, {25.0, 50.0}, {50.0, 100.0}, {100.0, 200.0} }; //Various Lambda intervals
    Ranges qTSquaredRange = { {0.0, 400.0},  {0.0, 4.0}, {4.0, 16.0}, {16.0, 36.0}, {36.0, 64.0}, {64.0, 100.0}, {100.0, 144.0}, {144.0, 196.0}, {196.0, 256.0}, {256.0, 324.0}, {324.0, 400.0} };  //Various qT Squared intervals  //Various qT Squared intervals
    Ranges LifetimeRange = { {-5.0, 15.0} }; //various lifetime ranges
    Ranges CosThetaRange {{0.0, 1.0}};
    Ranges PhiRange = { {0.0, 3.142}};

    //Identify each slice with an index. More indices than slices to create empty histograms that will be used for background subtraction
    std::vector<int> MassIndex = {0};
    std::vector<int> LambdaIndex = { 0, 1, 2, 3, 4, 5};
    std::vector<int> qTSquaredIndex = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::vector<int> PhiIndex = {0};
    std::vector<int> LifetimeIndex = {0};
    std::vector<int>  CosThetaIndex = {0};


    TH1* hists[10][10];


    Float_t PhotonDRTruthReco, MuPlusDRTruthReco, MuMinusDRTruthReco;
    Float_t PhotonDPtTruthReco, MuPlusDPtTruthReco, MuMinusDPtTruthReco;
    Float_t RelPhotonDPtTruthReco, RelMuPlusDPtTruthReco, RelMuMinusDPtTruthReco;



    //Loop through Cos Theta Indices
    for (int l = 0; l < LambdaIndex.size(); l ++){ // Loop through Lambda Indices
      for (int q = 0; q < qTSquaredIndex.size(); q++){ // Loop through qT2 Indices.


	 TString slambda = Form("%i", LambdaIndex.at(l));
	TString sqt     = Form("%i", qTSquaredIndex.at(q));
            TString label   = "trigSF_" + slambda + sqt;  
            hists[l][q] = new TH1F(label,label+TString(";SF;Entries"),240,0.,1.2);
	    

	//Declare a new root file in which to save future histograms
	TFile *MC_Plots = new TFile (Form("signal_%i%i_P%i.root", LambdaIndex.at(l), qTSquaredIndex.at(q), pCut ), "RECREATE");
	MC_Plots->cd();  //Selects this new root file for use.


	   


	auto trout = new TTree("trout","tr");
	double mupl_pt   = 0.;
	double mupl_eta  = 0.;
	double mumi_pt   = 0.;
	double mumi_eta  = 0.;
	double mumu_y    = 0.;
	double dr        = 0.;

	double sfpl   = 0.;
	double sfmi   = 0.;
	double drcorr = 0.;
	double drSF   = 0.;

	double SF = 0;
	double lambda = 0;
	double qt2 = 0;

	// Photon ID SF // 
	double photon_id_sf     = 0;
	double photon_id_sf_err = 0;



	trout->Branch("mupl_pt",&mupl_pt);
	trout->Branch("mupl_eta",&mupl_eta);
	trout->Branch("mumi_pt",&mumi_pt);
	trout->Branch("mumi_eta",&mumi_eta);
	trout->Branch("mumu_y",&mumu_y);
	trout->Branch("dr",&dr);
	trout->Branch("sfpl",&sfpl);
	trout->Branch("sfmi",&sfmi);
	trout->Branch("drcorr",&drcorr);
	trout->Branch("drSF",&drSF);
	trout->Branch("SF",&SF);

	trout->Branch("lambda",&lambda);
	trout->Branch("qt2",&qt2);
  
	trout->Branch("photon_id_sf",&photon_id_sf);
	trout->Branch("photon_id_sf_err",&photon_id_sf_err);




	//##########################################################################
	//          DECLARE THE VARIABLES AND LOAD THEM IN
	//##########################################################################
  
	UInt_t          run;
	UInt_t          evt;
	UInt_t          lb;
	Float_t         actIpX;
	Float_t         avgIpX;
	UInt_t          HLT_2mu4_bJpsimumu;
	UInt_t          HLT_mu6_mu4_bJpsimumu;
	UInt_t          HLT_2mu6_bJpsimumu;
	UInt_t          HLT_mu10_mu6_bJpsimumu;
	UInt_t          HLT_2mu10_bJpsimumu;
	UInt_t          HLT_2mu4_bJpsimumu_delayed;
	UInt_t          HLT_mu6_mu4_bJpsimumu_delayed;
	UInt_t          HLT_2mu6_bJpsimumu_delayed;
	UInt_t          HLT_mu10_mu6_bJpsimumu_delayed;
	UInt_t          HLT_2mu10_bJpsimumu_delayed;
	UInt_t          HLT_2mu4_bJpsimumu_noL2;
	UInt_t          HLT_mu6_mu4_bJpsimumu_noL2;
	UInt_t          HLT_2mu6_bJpsimumu_noL2;
	UInt_t          HLT_mu10_mu6_bJpsimumu_noL2;
	UInt_t          HLT_2mu10_bJpsimumu_noL2;
	vector<unsigned int> *Match_2mu4_bJpsimumu;
	vector<unsigned int> *Match_mu6_mu4_bJpsimumu;
	vector<unsigned int> *Match_2mu6_bJpsimumu;
	vector<unsigned int> *Match_mu10_mu6_bJpsimumu;
	vector<unsigned int> *Match_2mu10_bJpsimumu;
	vector<unsigned int> *Match_2mu4_bJpsimumu_delayed;
	vector<unsigned int> *Match_mu6_mu4_bJpsimumu_delayed;
	vector<unsigned int> *Match_2mu6_bJpsimumu_delayed;
	vector<unsigned int> *Match_mu10_mu6_bJpsimumu_delayed;
	vector<unsigned int> *Match_2mu10_bJpsimumu_delayed;
	vector<unsigned int> *Match_2mu4_bJpsimumu_noL2;
	vector<unsigned int> *Match_mu6_mu4_bJpsimumu_noL2;
	vector<unsigned int> *Match_2mu6_bJpsimumu_noL2;
	vector<unsigned int> *Match_mu10_mu6_bJpsimumu_noL2;
	vector<unsigned int> *Match_2mu10_bJpsimumu_noL2;
	vector<float>   *MuPlus_Pt;
	vector<float>   *MuPlus_Eta;
	vector<float>   *MuPlus_Phi;
	vector<float>   *MuPlus_E;
	vector<float>   *MuPlus_M;
	vector<float>   *mcMuPlus_Pt;
	vector<float>   *mcMuPlus_Eta;
	vector<float>   *mcMuPlus_Phi;
	vector<float>   *mcMuPlus_M;
	vector<int>     *MuPlus_quality;
	vector<int>     *MuPlus_author;
	vector<int>     *MuPlus_AODindex;
	vector<float>   *MuPlus_z0;
	vector<float>   *MuMinus_Pt;
	vector<float>   *MuMinus_Eta;
	vector<float>   *MuMinus_Phi;
	vector<float>   *MuMinus_E;
	vector<float>   *MuMinus_M;
	vector<float>   *mcMuMinus_Pt;
	vector<float>   *mcMuMinus_Eta;
	vector<float>   *mcMuMinus_Phi;
	vector<float>   *mcMuMinus_M;
	vector<int>     *MuMinus_quality;
	vector<int>     *MuMinus_author;
	vector<int>     *MuMinus_AODindex;
	vector<float>   *MuMinus_z0;
	vector<float>   *Photon_Pt;
	vector<float>   *Photon_Eta;
	vector<float>   *Photon_Phi;
	vector<float>   *Photon_E;
	vector<float>   *Photon_M;
	vector<float>   *mcPhoton_Pt;
	vector<float>   *mcPhoton_Eta;
	vector<float>   *mcPhoton_Phi;
	vector<float>   *mcPhoton_M;
	vector<float>   *Photon_TrkIso;
	vector<float>   *Photon_CaloIso;
	vector<float>   *Photon_CaloIso30;
	vector<float>   *Photon_CaloIso40;
	vector<int>     *Photon_quality;
	vector<int>     *Photon_author;
	vector<int>     *Photon_AODindex;
	vector<float>   *Photon_etas0;
	vector<float>   *Photon_etas1;
	vector<float>   *Photon_etas2;
	vector<float>   *TruthJpsi_Spx;
	vector<float>   *TruthJpsi_Spy;
	vector<float>   *TruthJpsi_Spz;
	vector<float>   *TruthJpsi_Sm;
	vector<float>   *DiLept_Pt;
	vector<float>   *DiLept_Eta;
	vector<float>   *DiLept_Phi;
	vector<float>   *DiLept_Y;
	vector<float>   *DiLept_E;
	vector<float>   *DiLept_M;
	vector<float>   *mcDiLept_Pt;
	vector<float>   *mcDiLept_Eta;
	vector<float>   *mcDiLept_Phi;
	vector<float>   *mcDiLept_M;
	vector<float>   *mcDiLept_Dr;
	vector<float>   *mcDiLeptFSR_Pt;
	vector<float>   *mcDiLeptFSR_Eta;
	vector<float>   *mcDiLeptFSR_Phi;
	vector<float>   *mcDiLeptFSR_M;
	vector<float>   *DiLept_Photon_Dphi;
	vector<float>   *DiLept_TrkIso_ptcone20;
	vector<float>   *DiLept_TrkIso_ptcone30;
	vector<float>   *DiLept_TrkIso_ptcone40;
	vector<float>   *DiLept_TrkIso_ptVarCone20;
	vector<float>   *DiLept_TrkIso_ptVarCone30;
	vector<float>   *DiLept_TrkIso_ptVarCone40;
	vector<float>   *DiLept_CaloIso_etcone20;
	vector<float>   *DiLept_CaloIso_etcone30;
	vector<float>   *PosTrack_TrkIso_ptcone20;
	vector<float>   *PosTrack_TrkIso_ptcone30;
	vector<float>   *PosTrack_TrkIso_ptVarCone20;
	vector<float>   *PosTrack_TrkIso_ptVarCone30;
	vector<float>   *NegTrack_TrkIso_ptcone20;
	vector<float>   *NegTrack_TrkIso_ptcone30;
	vector<float>   *NegTrack_TrkIso_ptVarCone20;
	vector<float>   *NegTrack_TrkIso_ptVarCone30;
	vector<float>   *DiLept_dR;
	vector<float>   *CosTheta;
	vector<float>   *MuMuY_Pt;
	vector<float>   *MuMuY_Eta;
	vector<float>   *MuMuY_Phi;
	vector<float>   *MuMuY_Y;
	vector<float>   *MuMuY_E;
	vector<float>   *MuMuY_M;
	vector<float>   *mcMuMuY_Pt;
	vector<float>   *mcMuMuY_Eta;
	vector<float>   *mcMuMuY_Phi;
	vector<float>   *mcMuMuY_M;
	vector<float>   *MuMuY_dR;
	vector<float>   *MuMuY_dPhi;
	vector<float>   *mcMuMuY_dR;
	vector<float>   *mcMuMuY_dPhi;
	vector<float>   *DiMuonVertex_x;
	vector<float>   *DiMuonVertex_y;
	vector<float>   *DiMuonVertex_z;
	vector<float>   *DiMuonVertex_ChiSq;
	vector<float>   *DiMuonVertex_Lxy;
	vector<float>   *DiMuonVertex_Lxy_Err;
	vector<float>   *DiMuonVertex_Tau;
	vector<float>   *DiMuonVertex_Tau_Err;
	vector<float>   *DiMuonVertex_Lxy_MinA0;
	vector<float>   *DiMuonVertex_Lxy_Err_MinA0;
	vector<float>   *DiMuonVertex_Tau_MinA0;
	vector<float>   *DiMuonVertex_Tau_Err_MinA0;
	vector<float>   *DiMuonVertex_Mass;
	vector<float>   *DiMuonVertex_Mass_Err;
	vector<float>   *DiMuonVertex_Muon0_Px;
	vector<float>   *DiMuonVertex_Muon0_Py;
	vector<float>   *DiMuonVertex_Muon0_Pz;
	vector<float>   *DiMuonVertex_Muon1_Px;
	vector<float>   *DiMuonVertex_Muon1_Py;
	vector<float>   *DiMuonVertex_Muon1_Pz;
	vector<float>   *DiMuon_DeltaZ0;
	vector<float>   *MuMuGamma_CS_CosTheta;
	vector<float>   *MuMuGamma_CS_Phi;
	vector<float>   *MuMuGamma_CSOLD_CosTheta;
	vector<float>   *MuMuGamma_CSOLD_Phi;
	vector<float>   *MuMu_CS_CosTheta;
	vector<float>   *MuMu_CS_Phi;
	vector<float>   *MuMuGamma_CosTheta;
	vector<float>   *MuMuGamma_Phi;
	vector<float>   *MuMu_CosTheta;
	vector<float>   *MuMu_Phi;
	vector<float>   *MuMuGamma_CosThetaOLD;
	vector<float>   *MuMuGamma_PhiOLD;
	vector<float>   *MuMu_CosThetaOLD;
	vector<float>   *MuMu_PhiOLD;
	vector<float>   *Truth_MuMuGamma_CS_CosTheta;
	vector<float>   *Truth_MuMuGamma_CS_Phi;
	vector<float>   *Truth_MuMuGamma_CSOLD_CosTheta;
	vector<float>   *Truth_MuMuGamma_CSOLD_Phi;
	vector<float>   *Truth_MuMu_CS_CosTheta;
	vector<float>   *Truth_MuMu_CS_Phi;
	vector<float>   *Truth_MuMuGamma_CosTheta;
	vector<float>   *Truth_MuMuGamma_Phi;
	vector<float>   *Truth_MuMu_CosTheta;
	vector<float>   *Truth_MuMu_Phi;
	vector<float>   *Truth_MuMuGamma_CosThetaOLD;
	vector<float>   *Truth_MuMuGamma_PhiOLD;
	vector<float>   *Truth_MuMu_CosThetaOLD;
	vector<float>   *Truth_MuMu_PhiOLD;
	vector<int>     *permIdx;
	vector<int>     *jpsiIdx;
	vector<int>     *photonIdx;



 
	//###########################################
	//            OPEN MAIN FOR LOOP
	//###########################################
    
	if (t->fChain == 0) return;

	Long64_t nentries = t->fChain->GetEntries();
	cout << "Looping over " << nentries << " events" << endl;

	//Loop over every event in the file:

	 
	    
	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry=0; jentry<nentries;jentry++) {     //Read in this event:
	  Long64_t ientry = t->LoadTree(jentry);
	  if (ientry < 0) break;
	  nb = t->fChain->GetEntry(jentry);   nbytes += nb;

	  //if(jentry > 20000) break;//break after 200000 events
	  //if(jentry >1000) break;
	  //if(jentry%10000 ==0) 

	  evt= t->evt;
	  Truth_MuMuGamma_CS_CosTheta = t->Truth_MuMuGamma_CS_CosTheta;	  
	  Photon_quality=t->Photon_quality;
	  MuMuGamma_CS_CosTheta = t -> MuMuGamma_CS_CosTheta;
	  mcPhoton_Pt = t->mcPhoton_Pt;
	  Photon_Pt= t->Photon_Pt;
	  Photon_Phi = t->Photon_Phi;
	  mcMuMinus_Pt = t->mcMuMinus_Pt;
	  MuMinus_Pt = t->MuMinus_Pt;
	  mcMuPlus_Pt = t->mcMuPlus_Pt;
	  MuPlus_Pt = t->MuPlus_Pt;
	  MuMuY_M = t->MuMuY_M;
	  mcMuMuY_M = t->mcMuMuY_M;
	  MuMuY_Pt = t->MuMuY_Pt;
	  mcMuMuY_Pt = t->mcMuMuY_Pt;
	  MuMuGamma_CS_Phi = t->MuMuGamma_CS_Phi;
	  Truth_MuMuGamma_CS_Phi = t->Truth_MuMuGamma_CS_Phi;
	  DiLept_M = t->DiLept_M;
	  DiLept_Y = t->DiLept_Y;
	  DiLept_Phi = t->DiLept_Phi;
	  DiLept_Pt = t->DiLept_Pt;
	  DiMuonVertex_Tau = t->DiMuonVertex_Tau;
	  Photon_TrkIso = t->Photon_TrkIso;
	  DiLept_TrkIso_ptcone20 = t->DiLept_TrkIso_ptcone20;
	  DiLept_TrkIso_ptcone30 = t->DiLept_TrkIso_ptcone30;
	  DiLept_TrkIso_ptcone40 = t->DiLept_TrkIso_ptcone40;
	  DiLept_TrkIso_ptVarCone20 = t->DiLept_TrkIso_ptVarCone20;
	  DiLept_TrkIso_ptVarCone30 = t->DiLept_TrkIso_ptVarCone30;
	  DiLept_TrkIso_ptVarCone40 = t->DiLept_TrkIso_ptVarCone40;
	  HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;
Photon_quality = t->Photon_quality;

	      
	  mcPhoton_Pt = t->mcPhoton_Pt;
  
 
	  mcMuMinus_Pt = t->mcMuMinus_Pt;
	  MuMinus_Pt = t->MuMinus_Pt;
	  mcMuPlus_Pt = t->mcMuPlus_Pt;
	  MuPlus_Pt = t->MuPlus_Pt;
	  MuMuY_M = t->MuMuY_M;
	  mcMuMuY_M = t->mcMuMuY_M;
	  MuMuY_Pt = t->MuMuY_Pt;
	  mcMuMuY_Pt = t->mcMuMuY_Pt;
	  mcMuPlus_Eta = t->mcMuPlus_Eta;
	  mcMuMinus_Eta = t->mcMuMinus_Eta;
	  mcPhoton_Eta = t->mcPhoton_Eta;
	  MuPlus_Eta = t->MuPlus_Eta;
	  MuMinus_Eta = t->MuMinus_Eta;
	  Photon_Eta = t->Photon_Eta;
	  DiMuonVertex_Tau = t->DiMuonVertex_Tau;
	  HLT_2mu4_bJpsimumu_noL2 = t->HLT_2mu4_bJpsimumu_noL2;

	    
	  //	      for (unsigned int j=0; j<t->Photon_Pt->size(); ++j){

	  //##################################################
	  //                FILL HISTOGRAMS
	  //##################################################
   

	  if (t->Photon_Pt->size() == 0) continue;
	  if (t->mcPhoton_Pt->size() == 0 ) continue;


    
	  if(t->Photon_Pt->at(0) < pCut ) continue;
	  if(t->MuMinus_Pt->at(0) < (4000)) continue;
	  if(t->MuPlus_Pt->at(0) < (4000)) continue;

	  if (sqrt(t->MuPlus_Eta->at(0) * t->MuPlus_Eta->at(0)) > 2.5) continue;
	  if (sqrt(t->MuMinus_Eta->at(0) * t->MuMinus_Eta->at(0)) > 2.5) continue;
	  if (sqrt(t->Photon_Eta->at(0) * t->Photon_Eta->at(0)) > 2.5) continue;


	  double lambda = ( (t->MuMuY_M->at(0))/3097.0 * (t->MuMuY_M->at(0))/3097.0);

	  double qTsquared = (t->MuMuY_Pt->at(0)/1000.0)  * (t->MuMuY_Pt->at(0)/1000.0 );

	  double dimuonmass = t->DiLept_M->at(0);

	  double dimuontau = t->DiMuonVertex_Tau->at(0);

	  double dimuonpt = t->DiLept_Pt->at(0);

	  double photonpt = t->Photon_Pt->at(0);

     

	  //UInt_t eventnumber = evt;


	  double dPhi = (t->DiLept_Phi->at(0) - t->Photon_Phi->at(0)) ;
	  while(dPhi > M_PI){dPhi -= 2*M_PI;}
	  while(dPhi < -M_PI){dPhi += 2*M_PI;}

	  double dY =( t->DiLept_Y->at(0) - t->Photon_Eta->at(0)) ; 



	  TLorentzVector tlv_truth_photon;
	  TLorentzVector tlv_reco_photon;

	  TLorentzVector tlv_truth_muplus;
	  TLorentzVector tlv_reco_muplus;
	  TLorentzVector tlv_truth_muminus;
	  TLorentzVector tlv_reco_muminus;


	  tlv_reco_photon.SetPtEtaPhiE(t->Photon_Pt ->at(0),
				       t->Photon_Eta->at(0),
				       t->Photon_Phi->at(0),
				       t->Photon_E  ->at(0)
				       );
	  tlv_truth_photon.SetPtEtaPhiM(t->mcPhoton_Pt ->at(0),
					t->mcPhoton_Eta->at(0),
					t->mcPhoton_Phi->at(0),
					0.
					);

	  tlv_reco_muplus.SetPtEtaPhiM(t->MuPlus_Pt ->at(0),
				       t->MuPlus_Eta->at(0),
				       t->MuPlus_Phi->at(0),
				       105.6583755
				       );
	  tlv_truth_muplus.SetPtEtaPhiM(t->mcMuPlus_Pt ->at(0),
					t->mcMuPlus_Eta->at(0),
					t->mcMuPlus_Phi->at(0),
					105.6583755
					);

	  tlv_reco_muminus.SetPtEtaPhiM(t->MuMinus_Pt ->at(0),
					t->MuMinus_Eta->at(0),
					t->MuMinus_Phi->at(0),
					105.6583755
					);
	  tlv_truth_muminus.SetPtEtaPhiM(t->mcMuMinus_Pt ->at(0),
					 t->mcMuMinus_Eta->at(0),
					 t->mcMuMinus_Phi->at(0),
					 105.6583755
					 );

	  PhotonDRTruthReco  = tlv_reco_photon.DeltaR(tlv_truth_photon) ;
	  MuPlusDRTruthReco  = tlv_reco_muplus .DeltaR(tlv_truth_muplus);
	  MuMinusDRTruthReco = tlv_reco_muminus.DeltaR(tlv_truth_muminus);

	  PhotonDPtTruthReco  = tlv_reco_photon .Pt() - tlv_truth_photon .Pt();
	  MuPlusDPtTruthReco  = tlv_reco_muplus .Pt() - tlv_truth_muplus .Pt();
	  MuMinusDPtTruthReco = tlv_reco_muminus.Pt() - tlv_truth_muminus.Pt();

	  RelPhotonDPtTruthReco  = (tlv_reco_photon .Pt() - tlv_truth_photon .Pt()) /tlv_truth_photon .Pt();
	  RelMuPlusDPtTruthReco  = (tlv_reco_muplus .Pt() - tlv_truth_muplus .Pt()) / tlv_truth_muplus .Pt();
	  RelMuMinusDPtTruthReco = (tlv_reco_muminus.Pt() - tlv_truth_muminus.Pt())/ tlv_truth_muminus .Pt();

	  //Declare the variables
	  double Lr =  (t->MuMuY_M->at(0)/3097.0) * (t->MuMuY_M->at(0)/3097.0);
	  double Qr = (t->MuMuY_Pt->at(0)/1000.0)  * (t->MuMuY_Pt->at(0)/1000.0 );
	  double Cr = (t->MuMuGamma_CS_CosTheta->at(0) *t->MuMuGamma_CS_CosTheta->at(0));
	  double Mr = (t->DiLept_M->at(0));
	  double Tr = t->DiMuonVertex_Tau->at(0);
	  double Pr = sqrt (t->MuMuGamma_CS_Phi->at(0) * t->MuMuGamma_CS_Phi->at(0) );


	  if ( Cr < 0 || Cr >= 1) continue;
	  if (Lr < 0 || Lr >= 200) continue;
	  if (Qr < 0 || Qr >= 400) continue;
	  if(Pr < 0 || Pr >= 3.142) continue;
	  if (Mr < 2700.0 || Mr >= 3500.0) continue;
	  if (Tr < -5.0 || Tr >= 15.0) continue;
  

	  //Loop over the various indices and ranges and fill hte created histograms based on that.
	  if (t->HLT_2mu4_bJpsimumu_noL2 == 0) continue;
	  if(Lr < 15 ) continue;

if(t->Photon_quality->at(0) > 1) continue;

     
    
	  double maxDR = 0.12;
	  //Loop over the various indices and ranges and fill hte created histograms based on that.

	  if (PhotonDRTruthReco > maxDR) continue; 

	  for (unsigned int iC=0; iC<CosThetaRange.size(); iC++){
	    if( Cr <= CosThetaRange.at(iC).first || Cr > CosThetaRange.at(iC).second) continue;
	 
	    for (unsigned int iL=0; iL<LambdaRange.size(); iL++){  //CS loop
	      if( Lr <= LambdaRange.at(iL).first || Lr > LambdaRange.at(iL).second) continue;

	      for (unsigned int iQ=0; iQ<qTSquaredRange.size(); iQ++){
		if( Qr <= qTSquaredRange.at(iQ).first || Qr > qTSquaredRange.at(iQ).second) continue;

		for (unsigned int iP = 0; iP < PhiRange.size(); iP++){
		  if (Pr <= PhiRange.at(iP).first || Pr > PhiRange.at(iP).second) continue;

		  for (unsigned int iM=0; iM<MassRange.size(); iM++){
		    if( Mr <= MassRange.at(iM).first || Mr > MassRange.at(iM).second) continue;

		    for (unsigned int iT=0; iT<LifetimeRange.size(); iT++){
		      if( Tr <= LifetimeRange.at(iT).first || Tr > LifetimeRange.at(iT).second) continue;


		      std::cout << "A" << std::endl;
		 
		 double     mumu_y = t->DiLept_Y->at(0);
       double dr     = t->DiLept_dR->at(0);

       sfpl = bphysEffTool->getSingleMuLegSF(MuPlus_Pt->at(0) *1e-3, +1.*MuPlus_Eta->at(0)*1e-3, DATASET::Data15, SINGLEMULEG::mu4);
       std::cout << "B" << std::endl;
       sfmi = bphysEffTool->getSingleMuLegSF(MuMinus_Pt->at(0)*1e-3, -1.*MuMinus_Eta->at(0)*1e-3, DATASET::Data15, SINGLEMULEG::mu4);
       std::cout << "C" << std::endl;
       drcorr =  bphysEffTool->getDrCor(dr, mumu_y, DATASET::MC16a, DIMUTRIG::bDimu_noL2);
       std::cout << "D" << std::endl;
       drSF   =  bphysEffTool->getDrSF (dr, mumu_y, DATASET::Data15, DIMUTRIG::bDimu_noL2) ;
std::cout << "E" << std::endl;
      //Total SF is calculted like as
      // getSingleMuLegSF(mu1) * getSingleMuLegSF(mu2) * getDrSF(dimu12)
       SF =  sfpl * sfmi *  drSF;

       std::cout << "F" << std::endl;
      //cout << SF << " :    " << sfpl << "   " << sfmi << "   " <<  drcorr << endl;
  
		// photon scalefactor
		photon_id_sf     =  photonSF.SF            (t->Photon_Pt ->at(0),t->Photon_Eta ->at(0));
		photon_id_sf_err =  photonSF.SF_uncertainty(t->Photon_Pt ->at(0),t->Photon_Eta ->at(0));


	     

		      if ( iL == l && iQ == q){


			trout->Fill();
			hists[iL][iQ] -> Fill(SF);
			
	        
		      }
		      //    }
		      //
		    }//Closing iT loop
		  }//closing iM loop
		}  //closing iQ loop
	      }//Closing iL loop	 
	    }//Closing iC loop
	  }
	} //closing loop over entries


	  
            hists[l][q]->Write();
         
	  
 
	MC_Plots->cd();

	//MC_Plots->Write();
	trout->Write();
	MC_Plots->ls();
	MC_Plots->Close();

	   
      }
    }
  }
		   
}//Closing brace
 
