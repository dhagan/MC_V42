cmake_minimum_required(VERSION 3.14)
project(plt_hf)
find_package(ROOT CONFIG REQUIRED COMPONENTS) ##RooStats RooFit RooFitCore HistFactory)
target_compile_definitions(ROOT::Core INTERFACE ${ROOT_DEFINITIONS} )
include_directories(inc)
add_compile_options(-Werror -Wall -pedantic -Wextra -O3)
set(src_hf src/main.cxx src/full.cxx src/split_string.cxx src/lovl.cxx 
		src/cfit.cxx src/sfit.cxx src/dscb.cxx src/hadd.cxx
		src/fitr.cxx src/dcmp.cxx src/eff.cxx src/reco.cxx
		src/sys.cxx src/phi_fit.cxx src/syss.cxx src/diff.cxx
		)
set(style /home/atlas/dhagan/analysis/analysisFork/atlasstyle/)
#set(style /home/crucible/analysis/main/MC_V42/atlasstyle)
add_executable( plt_hf ${src_hf} ${style}/AtlasLabels.C ${style}/AtlasUtils.C ${style}/AtlasStyle.C)

#add_executable( plt_hf ${src_hf} )
target_link_libraries(plt_hf PRIVATE ROOT::Core ${ROOT_LIBRARIES} )

set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
IF( EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/compile_commands.json" )
  EXECUTE_PROCESS( COMMAND ${CMAKE_COMMAND} -E copy_if_different
          ${CMAKE_CURRENT_BINARY_DIR}/../compile_commands.json
          )
ENDIF()
