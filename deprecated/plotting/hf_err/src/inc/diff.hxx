#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TLorentzVector.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include <TStyle.h>
#include <TCanvas.h>
#include "TString.h"
#include "TROOT.h"
#include "TGaxis.h"

#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>


#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasLabels.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasUtils.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasStyle.h"

#include <split_string.hxx>

void diff( std::string eff_path, std::string hf_path, std::string abin_vars, std::string spec_vars, 
           std::string type, std::string slice, std::string unique, std::string range );
