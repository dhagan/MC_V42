#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include <TF1.h>
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include "TROOT.h"
#include "TGraphErrors.h"
#include "TGaxis.h"

#include <TStyle.h>

#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <iostream>
#include <math.h>

//#include "/home/crucible/analysis/main/MC_V42/atlasstyle/AtlasLabels.h"
//#include "/home/crucible/analysis/main/MC_V42/atlasstyle/AtlasUtils.h"
//#include "/home/crucible/analysis/main/MC_V42/atlasstyle/AtlasStyle.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasLabels.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasUtils.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasStyle.h"

void fitr(std::string filepath, std::string abin_vars, std::string spec_vars, std::string slice, std::string type, std::string unique);
