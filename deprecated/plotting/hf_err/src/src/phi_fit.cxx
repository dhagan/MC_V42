#include <phi_fit.hxx>
#include <split_string.hxx>

void phi_fit( std::string eff_path, std::string hf_path, std::string abin_vars, std::string spec_vars, std::string type,
    std::string slice, std::string unique, std::string range, std::string opstring ){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  int mass_bins[5] = {0,3,4,5,12};

  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42);
  wip.SetTextSize(0.038); wip.SetTextColor(1);
  TLatex sim;  sim.SetNDC(); sim.SetTextFont(42);
  sim.SetTextSize(0.038); sim.SetTextColor(1);

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{15,-10,20};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DPhi"]          =  std::vector<double>{8,0,M_PI};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{10,5,30};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5,25};

  if ( !range.empty() ){
    std::vector< std::string > arg_bin;
    std::vector< std::string > set_bins;
    split_strings( arg_bin, range, "_" );
    split_strings( set_bins, arg_bin.at(1), "," );
    std::map< std::string, std::vector<double> >::iterator bin_map_itr = analysis_bins.find( arg_bin.at( 0 ).c_str() );
    bin_map_itr->second = { std::stod( set_bins.at( 0 ) ), std::stod( set_bins.at( 1 ) ), std::stod( set_bins.at( 2 ) ) };
  }
  
  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["BDT"]           =  "Value";
  units["abs(qtB)"]      =  "GeV";
  units["qtB"]           =  "GeV";
  units["Phi"]           =  "rad";
  units["DPhi"]          =  "rad";
  units["DiMuonPt"]      =  "GeV";
  units["PhotonPt"]      =  "GeV";

  
  
  
  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";

  std::map< std::string, std::string > fit_opt_map;
  fit_opt_map["Phi"]  = "I";
    
  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");
  if ( vec_abin_vars.size() > 1  || vec_spec_vars.size() > 1 ){
    std::cout << "This function is designed for single variable processes only" << std::endl;
    return;
  }
  std::string abin_var = vec_abin_vars.at(0);
  std::string spec_var = vec_spec_vars.at(0);

  std::string var_name = abin_var;
  std::vector<double> bins = analysis_bins[var_name.c_str()];

  int abin_bins     = (int)    bins.at(0);
  double abin_min   = (double) bins.at(1);
  double abin_max   = (double) bins.at(2);

  double width     = (abin_max-abin_min)/((double) abin_bins) ;
  std::string unit = units[abin_var];
  std::string fit_option = "MQ" + fit_opt_map[abin_var];

  TF1 * fit_sg = new TF1("fit_sg"," [0]*exp( -((x-[1])^2)/(2*[2]^2))" ); 
  fit_sg->SetParName(0,"C"); fit_sg->SetParName(1,"X"); fit_sg->SetParName(2,"Sigma");
  fit_sg->SetParNames("C","X","SIG");
  fit_sg->SetParLimits(0,0,1e6);
  fit_sg->SetParLimits(2,1,10);
  fit_sg->SetLineColor(6);

  TF1 * fit_dg_cc = new TF1("fit_dg_cc"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) + [3]*exp( -((x-[1])^2)/(2*[4]^2)) " );
  fit_dg_cc->SetParNames("C1","X","SIG","C2","SIG2");
  fit_dg_cc->SetParLimits(0,0,1e6);
  fit_dg_cc->SetParLimits(2,1,10);
  fit_dg_cc->SetParLimits(3,0,1e6);
  fit_dg_cc->SetParLimits(4,1,10);
  fit_dg_cc->SetLineColor(6);

  TF1 * fit_cs_four = new TF1("fit_cs_four"," [0]*(1 + [1]*cos(x) + [2]*cos(2*x) + [3]*cos(3*x) + [4]*cos(4*x) )",
                              abin_min, abin_max );
  TF1 * fit_cs_six  = new TF1("fit_cs_six"," [0]*(1 + [1]*cos(x) + [2]*cos(2*x) + [3]*cos(3*x) + [4]*cos(4*x) + "
                                            "[5]*cos(5*x) + [6]*cos(6*x) )", abin_min, abin_max );


  std::map<std::string,std::vector<TF1*>> fit_funcs;
  fit_funcs["qtA"] = {fit_sg, fit_dg_cc};
  fit_funcs["qtB"] = {fit_sg, fit_dg_cc};
  fit_funcs["Phi"] = {fit_cs_four, fit_cs_six};

  std::map<std::string,std::vector<std::string>> fit_strings;
  fit_strings["qtA"] = {"Single gaussian fit", "Double gaussian fit"};
  fit_strings["qtB"] = {"Single gaussian fit", "Double gaussian fit"};
  fit_strings["Phi"] = {"Four term fit", "six term fit"};


  std::vector<std::string> file_vec, eff_vec, op_vec, type_vec;
  split_strings( file_vec, hf_path, ":" );
  split_strings( eff_vec, eff_path, ":" );
  split_strings( op_vec, opstring, ":" );
  split_strings( type_vec, type, ":" );

  if ( type_vec.size() == 1 ){
    for ( int file_ind = 1; file_ind < (int) file_vec.size(); file_ind++ ){
      type_vec.push_back( type_vec.at(0) );
    }
  }

  
  for ( int & mass_index : mass_bins ){
    
    char mass_char[100];        
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
   
    std::vector< TH1F* > extr_hists;
    std::vector< TH1F* > corr_hists;
    std::vector< TH1F* > eff_hists;
    std::vector< TH1F* > truth_hists;
    std::vector< TH1F* > reco_hists;


    for ( int file_index = 0; file_index < (int) file_vec.size(); file_index++ ){
   
      TFile * integral_file = new TFile(file_vec.at(file_index).c_str(),   "READ");
      TFile * eff_file = new TFile(eff_vec.at(file_index).c_str(),   "READ");
        
      TH1F * extr_hist      = new TH1F( Form("extr_sign_Q%i",mass_index), "", abin_bins, abin_min, abin_max );
      TH1F * corr_hist      = new TH1F( Form("corr_sign_Q%i",mass_index), "", abin_bins, abin_min, abin_max );
      TH1F * eff_hist       = (TH1F*) eff_file->Get( Form( "eff_%s_Q%i",    abin_var.c_str(),  mass_index ) );
      TH1F * reco_hist      = (TH1F*) eff_file->Get( Form( "reco_%s_Q%i",   abin_var.c_str(),  mass_index ) );
      TH1F * truth_hist     = (TH1F*) eff_file->Get( Form( "truth_%s_Q%i",  abin_var.c_str(),  mass_index ) );

      int hf_error_bin = 0;
      std::string h_type = type_vec.at( file_index );
      if (h_type.find("data") != std::string::npos){ hf_error_bin = 1; }  
      if (h_type.find("sign") != std::string::npos){ hf_error_bin = 2; }
      if (h_type.find("bckg") != std::string::npos){ hf_error_bin = 3; }
      std::cout << h_type << std::endl;


      bool error_available = true;
      for ( int abin_index = 1; abin_index <= abin_bins; abin_index++ ){
         
        char bin_hist_name[150], error_name[150];
        sprintf( bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), h_type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index );
        sprintf( error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index );
        TH1F * integral_hist  = (TH1F*) integral_file->Get( bin_hist_name );
        TH1F * error_hist     = (TH1F*) integral_file->Get( error_name );
  
        extr_hist->SetBinContent(abin_index, integral_hist->Integral());
        if (error_hist != NULL){ 
          extr_hist->SetBinError(abin_index,error_hist->GetBinError(hf_error_bin));
        } else {error_available = false;}
      }
      if ( !error_available ){ extr_hist->Sumw2();}


      std::cout << 180 << std::endl;
      corr_hist->Divide( extr_hist, eff_hist, 1.0, 1.0 );
      extr_hists.push_back( extr_hist );
      corr_hists.push_back( corr_hist );
      eff_hists.push_back( eff_hist );
      truth_hists.push_back( truth_hist );
      reco_hists.push_back( reco_hist );

    }

    std::vector< TH1F* > reg_hists;
    reg_hists.push_back( new TH1F("reg_hist_0", "", abin_bins, abin_min, abin_max ) );


    std::map< std::string, std::vector< TH1F* > > mem_hists;
    mem_hists.insert( std::make_pair( "x", extr_hists ) );
    mem_hists.insert( std::make_pair( "c", corr_hists ) );
    mem_hists.insert( std::make_pair( "e", eff_hists ) );
    mem_hists.insert( std::make_pair( "r", reg_hists ) );
    mem_hists.insert( std::make_pair( "t", truth_hists ) );
    mem_hists.insert( std::make_pair( "o", reco_hists ) );

    for ( std::string op_str : op_vec ){
      
      std::vector< std::string> op_vec;
      split_strings( op_vec, op_str, "," );
      if ( op_vec.size() < 4 ){
        std::cout << "malformed operation" << std::endl;
        return;
      }

      int result_index = std::atoi( op_vec.at(0).c_str() );
      if ( result_index + 1 == (int) reg_hists.size() ){
        reg_hists.push_back( new TH1F( Form( "reg_hist_%i",(int) ( reg_hists.size() - 1 ) ), "", abin_bins, abin_min, abin_max ) );
      }

      double op_no{1.0};
      TH1F * op_hist1, * op_hist2; 

      std::string ind{ op_vec.at(1).at(0) };
      std::map< std::string, std::vector<TH1F*> >::iterator map_itr = mem_hists.find( ind );
      ind = op_vec.at(1).at(1);
      op_hist1 = map_itr->second.at( std::stoi( ind ) );
      op_hist2 = map_itr->second.at( std::stoi( ind ) );

      if ( op_vec.at(2).find(".") == std::string::npos || op_vec.at(2).find("|") == std::string::npos ){
        ind = op_vec.at(3).at(0);
        map_itr = mem_hists.find( ind );
        ind = op_vec.at(3).at(1);
        op_hist2 = map_itr->second.at( std::stoi( ind ) );
      } else { op_no = std::stod( op_vec.at(3) ); }

      char op_char = op_vec.at(2).at(0);
      switch ( op_char ){
        case '*':
          reg_hists.at( result_index )->Multiply( op_hist1, op_hist2, 1.0, 1.0); 
          break;
        case '/':
          std::cout << 236 << std::endl;
          reg_hists.at( result_index )->Divide( op_hist1, op_hist2, 1.0, 1.0); 
          //std::cout << op_hist1->GetName() << std::endl;
          //std::cout << op_hist2->GetName() << std::endl;
          break;
        case '+':
          reg_hists.at( result_index )->Add( op_hist1, op_hist2, 1.0, 1.0); 
          break;
        case '-':
          reg_hists.at( result_index )->Add( op_hist1, op_hist2, 1.0, -1.0); 
          break;
        case '.':
          reg_hists.at( result_index )->Add( op_hist1, 1.0 ); 
          reg_hists.at( result_index )->Scale( op_no ); 
          break;
        case '|':
          reg_hists.at( result_index )->Add( op_hist1, 1.0 ); 
          reg_hists.at( result_index )->Scale( 1.0/op_no ); 
          break;
        case 'c':
          reg_hists.at( result_index ) = (TH1F*) op_hist1->Clone( Form("ref_hist_%i", result_index) );
      }
    }


    int pad_width = 2*file_vec.size();
    int pad_depth = reg_hists.size(); 

    TCanvas * mass_canv;
    mass_canv  = new TCanvas(Form("canv_Q%i",mass_index), "eff_canv", 100, 100, pad_width*1000, pad_depth*1000);
    mass_canv->Divide(pad_width,pad_depth);


    int pad = 1;
    for ( int file_index = 0; file_index < (int) file_vec.size(); file_index++ ){


      TH1F * eff    = (TH1F*) eff_hists.at( file_index )  ->Clone( Form( "e_%i", file_index ) );  
      TH1F * truth  = (TH1F*) truth_hists.at( file_index )->Clone( Form( "t_%i", file_index ) );
      TH1F * reco   = (TH1F*) reco_hists.at( file_index ) ->Clone( Form( "o_%i", file_index ) ); 
      TH1F * extr   = (TH1F*) extr_hists.at( file_index ) ->Clone( Form( "x_%i", file_index ) ); 
      TH1F * corr   = (TH1F*) corr_hists.at( file_index ) ->Clone( Form( "c_%i", file_index ) ); 


      mass_canv->cd( pad );
      double axis_lim = std::ceil(eff->GetMaximum());
      eff->Draw("E1");
      eff->SetMarkerStyle(21);
      eff->SetLineColor(1);
      eff->GetYaxis()->SetRangeUser(0,axis_lim);
      eff->GetYaxis()->SetTitle(Form("Efficiency/%.3f %s",width, unit.c_str()));
      eff->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
      eff->GetYaxis()->SetLabelSize(0.035);
      eff->GetYaxis()->SetTitleSize(0.035);
      eff->GetXaxis()->SetLabelSize(0.035);
      eff->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      gPad->SetTicks(1,0);
      int pad1_pre_scale = truth->Integral()/axis_lim;
      truth->Scale(1.0/pad1_pre_scale);
      reco->Scale(1.0/pad1_pre_scale);    
      truth->SetLineWidth(0);
      truth->SetFillColorAlpha(kRed+1,0.5);
      reco->SetLineWidth(0);
      reco->SetFillColorAlpha(kBlue+1,0.5);
      truth->Draw("HIST SAME");
      reco->Draw("HIST SAME");
      TGaxis * pad1_axis = new TGaxis(abin_max,0,abin_max,axis_lim,0,pad1_pre_scale,510,"+L");
      pad1_axis->SetLabelSize(0.035);             
      pad1_axis->SetLabelFont(42);                
      pad1_axis->SetTitleSize(0.035);
      pad1_axis->SetTitleOffset(1.25);
      pad1_axis->SetMaxDigits(4);
      pad1_axis->SetTitleFont(42);                
      pad1_axis->SetTitle(Form("Entries/%.3f %s",width, unit.c_str() ));
      pad1_axis->Draw();
      TLegend * pad1_legend = new TLegend(0.2,0.6,0.4,0.69);
      pad1_legend->SetBorderSize(0);
      pad1_legend->SetFillColor(0);
      pad1_legend->SetFillStyle(0);
      pad1_legend->SetTextFont(42);
      pad1_legend->SetTextSize(0.025);
      pad1_legend->AddEntry(eff,"Efficiency","LP");
      pad1_legend->AddEntry(truth,"Truth","F");
      pad1_legend->AddEntry(reco,"Reco","F");
      pad1_legend->Draw();
      TLatex pad1_ltx;
      pad1_ltx.SetTextSize(0.03);
      pad1_ltx.DrawLatexNDC(0.25,0.825,Form("%s Efficiency - Mass Q%i",abin_var.c_str(),mass_index));  
      ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
      wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
      pad += 1;
      

      mass_canv->cd( pad );
      eff->Draw("E1");
      eff->SetMarkerStyle(21);
      eff->SetLineColor(1);
      eff->GetYaxis()->SetTitle(Form("Efficiency/%.3f %s",width, unit.c_str()));
      eff->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
      eff->GetYaxis()->SetLabelSize(0.035);
      eff->GetYaxis()->SetTitleSize(0.035);
      eff->GetXaxis()->SetLabelSize(0.035);
      eff->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      gPad->SetTicks(1,0);
      int pad2_pre_scale = corr->Integral()/axis_lim;
      corr->Scale(1.0/pad2_pre_scale);
      extr->Scale(1.0/pad2_pre_scale);    
      corr->SetLineWidth(1);
      corr->SetLineColorAlpha(kRed+1,1.0);
      extr->SetLineWidth(1);
      extr->SetLineColorAlpha(kBlue+1,1.0);
      corr->Draw("E1 SAME");
      extr->Draw("E1 SAME");
      corr->Draw("HIST SAME");
      extr->Draw("HIST SAME");
      gPad->Update();
      TGaxis * pad2_axis = new TGaxis(abin_max,0,abin_max,axis_lim,0,pad2_pre_scale,510,"+L");
      pad2_axis->SetLabelSize(0.035);
      pad2_axis->SetLabelFont(42);
      pad2_axis->SetMaxDigits(4);
      pad2_axis->SetTitleOffset(1.25);
      pad2_axis->SetTitleFont(42);
      pad2_axis->SetTitle(Form("Entries/%.3f %s",width, unit.c_str() ));
      pad2_axis->Draw();
      TLegend * pad2_legend = new TLegend(0.2,0.6,0.4,0.69);
      pad2_legend->SetBorderSize(0);
      pad2_legend->SetFillColor(0);
      pad2_legend->SetFillStyle(0);
      pad2_legend->SetTextFont(42);
      pad2_legend->SetTextSize(0.025);
      pad2_legend->AddEntry(eff,"efficiency","LP");
      pad2_legend->AddEntry(corr,"Efficiency adjusted signal","LP");
      pad2_legend->AddEntry(extr,"HF Extracted signal","LP");
      pad2_legend->Draw();
      TLatex pad2_ltx;
      pad2_ltx.SetTextSize(0.03);
      pad2_ltx.DrawLatexNDC(0.25,0.825,Form("%s Efficiency recovery - Mass Q%i", abin_var.c_str(), mass_index ));  
      ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
      wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
      pad += 1;

    }



    for ( int reg_index = 0; reg_index < (int) reg_hists.size()-1; reg_index++ ){

      pad = pad_width+(reg_index*pad_width+1);


      TH1F * reg   = (TH1F*) reg_hists.at( reg_index )->Clone( Form( "c_%i", reg_index ) ); 
      std::string reg_string = op_vec.at(reg_index);

      mass_canv->cd( pad );
      reg->GetYaxis()->SetTitle(Form("Entries/%.3f %s",width, unit.c_str()));
      reg->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
      reg->GetYaxis()->SetRangeUser(0, reg->GetMaximum()*1.7);
      reg->GetYaxis()->SetLabelSize(0.035);
      reg->GetYaxis()->SetTitleSize(0.035);
      reg->GetXaxis()->SetLabelSize(0.035);
      reg->GetXaxis()->SetTitleSize(0.035);
      reg->GetYaxis()->SetMaxDigits(4);
      reg->SetLineWidth(1);
      reg->SetLineColorAlpha(kRed+1,1.0);
      reg->Draw("HIST");
      TLatex padr_ltx;
      padr_ltx.SetTextSize(0.03);
      padr_ltx.DrawLatexNDC(0.25,0.825,Form( "%s hist reg %i - Mass Q%i - %s", abin_var.c_str(), reg_index, mass_index,
                            reg_string.c_str()  ));  
      ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
      wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
      pad += 1;

      for ( int fit_index = 0; fit_index < (int) fit_funcs[abin_var].size(); fit_index++ ){
      
        TH1F * freg       = (TH1F*) reg_hists.at( reg_index )->Clone( Form( "c_%i_f%i", reg_index, fit_index ) ); 
        TF1 * current_fit = (TF1*) fit_funcs[abin_var].at(fit_index)->Clone( Form( "fit_%i_f%i", reg_index, fit_index ) );
        current_fit->SetRange( abin_min, abin_max );

        mass_canv->cd( pad );
        freg->GetYaxis()->SetTitle(Form("Entries/%.3f %s",width, unit.c_str()));
        freg->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        freg->GetYaxis()->SetRangeUser(0, reg->GetMaximum()*1.7);
        freg->GetYaxis()->SetMaxDigits(4);
        freg->GetYaxis()->SetLabelSize(0.035);
        freg->GetYaxis()->SetTitleSize(0.035);
        freg->GetXaxis()->SetLabelSize(0.035);
        freg->GetXaxis()->SetTitleSize(0.035);
        freg->SetLineWidth(1);
        freg->SetLineColorAlpha(kRed+1,1.0);
        freg->Draw("HIST");
        gPad->Modified(); gPad->Update();
        freg->Fit( current_fit, fit_option.c_str(),"", abin_min, abin_max );
        current_fit->Draw("SAME");
        gPad->Modified(); gPad->Update();
        TPaveStats * padf_stats = (TPaveStats*) freg->FindObject("stats");
        padf_stats->SetX1NDC(0.55); padf_stats->SetX2NDC(0.83);
        padf_stats->SetY1NDC(0.55); padf_stats->SetY2NDC(0.79);
        padf_stats->SetTextFont(42);
        padf_stats->SetFillStyle(0);
        TLegend * padf_legend = new TLegend(0.2,0.6,0.4,0.69);
        padf_legend->SetBorderSize(0);
        padf_legend->SetFillColor(0);
        padf_legend->SetFillStyle(0);
        padf_legend->SetTextFont(42);
        padf_legend->SetTextSize(0.025);
        padf_legend->AddEntry(freg,"Reg hist","LP");
        padf_legend->AddEntry( current_fit, fit_strings[abin_var].at(fit_index).c_str(), "L");
        padf_legend->Draw();
        TLatex padf_ltx;
        padf_ltx.SetTextSize(0.03);
        padf_ltx.DrawLatexNDC(0.25,0.825,Form( "%s hist freg %i - Mass Q%i", abin_var.c_str(), reg_index, mass_index ));  
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
        pad += 1;

      }

    }

    mass_canv->SaveAs(Form("./calc_Q%i_A-%s_%s.png",mass_index,abin_var.c_str(),unique.c_str()));

  }

}

        //mass_canv->cd(3);
        //fit_truth_1->Draw("E1");
        //fit_truth_1->SetLineWidth(3);
        //fit_truth_1->SetLineColorAlpha(2,1.0);
        //fit_truth_1->GetYaxis()->SetRangeUser(0,1.7*fit_truth_1->GetMaximum());
        //fit_truth_1->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_1->GetYaxis()->SetMaxDigits(4);
        //fit_truth_1->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_1->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_1->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_1->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_1->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));

        //fit_truth_1->Fit( fit_phi_cnst, fit_option.c_str(), "", abin_min, abin_max );
        //fit_phi_cnst->Draw( "SAME" );

        //fit_truth_1->Draw("SAME HIST");
        //fit_reco->SetLineColorAlpha(4,0.5);
        //fit_reco->SetLineStyle(2);
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad3_stats = (TPaveStats*) fit_truth_1->FindObject("stats");
        //pad3_stats->SetX1NDC(0.55); pad3_stats->SetX2NDC(0.83);
        //pad3_stats->SetY1NDC(0.65); pad3_stats->SetY2NDC(0.79);
        //pad3_stats->SetTextFont(42);
        //pad3_stats->SetFillStyle(0);
        //TLegend * pad3_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad3_legend->SetBorderSize(0);
        //pad3_legend->SetFillColor(0);
        //pad3_legend->SetFillStyle(0);
        //pad3_legend->SetTextFont(42);
        //pad3_legend->SetTextSize(0.02);
        //pad3_legend->AddEntry(fit_truth_1,"Efficiency adjusted signal","LP");
        //pad3_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad3_legend->AddEntry( fit_phi_cnst, "const", "L");
        //pad3_legend->Draw();
        //TLatex pad3_ltx;
        //pad3_ltx.SetTextSize(0.03);
        //pad3_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //fit_phi_cos1->FixParameter(0,fit_phi_cnst->GetParameter(0)); 
        //fit_phi_cos2->FixParameter(0,fit_phi_cnst->GetParameter(0));
        //fit_phi_cos3->FixParameter(0,fit_phi_cnst->GetParameter(0));
        //fit_phi_cos4->FixParameter(0,fit_phi_cnst->GetParameter(0));
        //fit_phi_cos5->FixParameter(0,fit_phi_cnst->GetParameter(0));
        //fit_phi_cos6->FixParameter(0,fit_phi_cnst->GetParameter(0));


        //mass_canv->cd(4);
        //fit_truth_2->Draw("E1");
        //fit_truth_2->SetLineWidth(3);
        //fit_truth_2->SetLineColorAlpha(2,1.0);
        //fit_truth_2->GetYaxis()->SetRangeUser(0,1.7*fit_truth_2->GetMaximum());
        //fit_truth_2->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_2->GetYaxis()->SetMaxDigits(4);
        //fit_truth_2->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_2->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_2->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_2->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_2->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_truth_2->Fit(fit_phi_cos1,fit_option.c_str(),"",abin_min,abin_max);
        //fit_phi_cos1->Draw("SAME");
        //fit_truth_2->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad4_stats = (TPaveStats*) fit_truth_2->FindObject("stats");
        //pad4_stats->SetX1NDC(0.55); pad4_stats->SetX2NDC(0.83);
        //pad4_stats->SetY1NDC(0.65); pad4_stats->SetY2NDC(0.79);
        //pad4_stats->SetTextFont(42);
        //pad4_stats->SetFillStyle(0);
        //TLegend * pad4_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad4_legend->SetBorderSize(0);
        //pad4_legend->SetFillColor(0);
        //pad4_legend->SetFillStyle(0);
        //pad4_legend->SetTextFont(42);
        //pad4_legend->SetTextSize(0.02);
        //pad4_legend->AddEntry(fit_truth_2,"Efficiency adjusted signal","LP");
        //pad4_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad4_legend->AddEntry(fit_phi_cos1,fit_strings[abin_var].at(1).c_str(),"L");
        //pad4_legend->Draw();
        //TLatex pad4_ltx;
        //pad4_ltx.SetTextSize(0.03);
        //pad4_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //fit_phi_cos2->FixParameter(1,fit_phi_cos1->GetParameter(1));
        //fit_phi_cos3->FixParameter(1,fit_phi_cos1->GetParameter(1));
        //fit_phi_cos4->FixParameter(1,fit_phi_cos1->GetParameter(1));
        //fit_phi_cos5->FixParameter(1,fit_phi_cos1->GetParameter(1));
        //fit_phi_cos6->FixParameter(1,fit_phi_cos1->GetParameter(1));

       
        //mass_canv->cd(5);
        //fit_truth_3->Draw("E1");
        //fit_truth_3->SetLineWidth(3);
        //fit_truth_3->SetLineColorAlpha(2,1.0);
        //fit_truth_3->GetYaxis()->SetRangeUser(0,1.7*fit_truth_3->GetMaximum());
        //fit_truth_3->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_3->GetYaxis()->SetMaxDigits(4);
        //fit_truth_3->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_3->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_3->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_3->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_3->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_truth_3->Fit(fit_phi_cos2,fit_option.c_str(),"",abin_min,abin_max);
        //fit_phi_cos2->Draw("SAME");
        //fit_truth_3->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad5_stats = (TPaveStats*) fit_truth_3->FindObject("stats");
        //pad5_stats->SetX1NDC(0.55); pad5_stats->SetX2NDC(0.83);
        //pad5_stats->SetY1NDC(0.65); pad5_stats->SetY2NDC(0.79);
        //pad5_stats->SetTextFont(42);
        //pad5_stats->SetFillStyle(0);
        //TLegend * pad5_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad5_legend->SetBorderSize(0);
        //pad5_legend->SetFillColor(0);
        //pad5_legend->SetFillStyle(0);
        //pad5_legend->SetTextFont(42);
        //pad5_legend->SetTextSize(0.02);
        //pad5_legend->AddEntry(fit_truth_3,"Efficiency adjusted signal","LP");
        //pad5_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad5_legend->AddEntry(fit_phi_cos2,fit_strings[abin_var].at(1).c_str(),"L");
        //pad5_legend->Draw();
        //TLatex pad5_ltx;
        //pad5_ltx.SetTextSize(0.03);
        //pad5_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //fit_phi_cos3->FixParameter(2,fit_phi_cos2->GetParameter(2));
        //fit_phi_cos4->FixParameter(2,fit_phi_cos2->GetParameter(2));
        //fit_phi_cos5->FixParameter(2,fit_phi_cos2->GetParameter(2));
        //fit_phi_cos6->FixParameter(2,fit_phi_cos2->GetParameter(2));


        //mass_canv->cd(6);
        //fit_truth_4->Draw("E1");
        //fit_truth_4->SetLineWidth(3);
        //fit_truth_4->SetLineColorAlpha(2,1.0);
        //fit_truth_4->GetYaxis()->SetRangeUser(0,1.7*fit_truth_4->GetMaximum());
        //fit_truth_4->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_4->GetYaxis()->SetMaxDigits(4);
        //fit_truth_4->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_4->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_4->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_4->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_4->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_truth_4->Fit(fit_phi_cos3,fit_option.c_str(),"",abin_min,abin_max);
        //fit_phi_cos3->Draw("SAME");
        //fit_truth_4->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad6_stats = (TPaveStats*) fit_truth_4->FindObject("stats");
        //pad6_stats->SetX1NDC(0.55); pad6_stats->SetX2NDC(0.83);
        //pad6_stats->SetY1NDC(0.65); pad6_stats->SetY2NDC(0.79);
        //pad6_stats->SetTextFont(42);
        //pad6_stats->SetFillStyle(0);
        //TLegend * pad6_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad6_legend->SetBorderSize(0);
        //pad6_legend->SetFillColor(0);
        //pad6_legend->SetFillStyle(0);
        //pad6_legend->SetTextFont(42);
        //pad6_legend->SetTextSize(0.02);
        //pad6_legend->AddEntry(fit_truth_4,"Efficiency adjusted signal","LP");
        //pad6_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad6_legend->AddEntry(fit_phi_cos3,fit_strings[abin_var].at(1).c_str(),"L");
        //pad6_legend->Draw();
        //TLatex pad6_ltx;
        //pad6_ltx.SetTextSize(0.03);
        //pad6_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //fit_phi_cos4->FixParameter(3,fit_phi_cos3->GetParameter(3));
        //fit_phi_cos5->FixParameter(3,fit_phi_cos3->GetParameter(3));
        //fit_phi_cos6->FixParameter(3,fit_phi_cos3->GetParameter(3));


        //mass_canv->cd(7);
        //fit_truth_5->Draw("E1");
        //fit_truth_5->SetLineWidth(3);
        //fit_truth_5->SetLineColorAlpha(2,1.0);
        //fit_truth_5->GetYaxis()->SetRangeUser(0,1.7*fit_truth_5->GetMaximum());
        //fit_truth_5->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_5->GetYaxis()->SetMaxDigits(4);
        //fit_truth_5->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_5->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_5->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_5->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_5->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_truth_5->Fit(fit_phi_cos4,fit_option.c_str(),"",abin_min,abin_max);
        //fit_phi_cos4->Draw("SAME");
        //fit_truth_5->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad7_stats = (TPaveStats*) fit_truth_5->FindObject("stats");
        //pad7_stats->SetX1NDC(0.55); pad7_stats->SetX2NDC(0.83);
        //pad7_stats->SetY1NDC(0.65); pad7_stats->SetY2NDC(0.79);
        //pad7_stats->SetTextFont(42);
        //pad7_stats->SetFillStyle(0);
        //TLegend * pad7_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad7_legend->SetBorderSize(0);
        //pad7_legend->SetFillColor(0);
        //pad7_legend->SetFillStyle(0);
        //pad7_legend->SetTextFont(42);
        //pad7_legend->SetTextSize(0.02);
        //pad7_legend->AddEntry(fit_truth_5,"Efficiency adjusted signal","LP");
        //pad7_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad7_legend->AddEntry(fit_phi_cos4,fit_strings[abin_var].at(1).c_str(),"L");
        //pad7_legend->Draw();
        //TLatex pad7_ltx;
        //pad7_ltx.SetTextSize(0.03);
        //pad7_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //fit_phi_cos5->FixParameter(4,fit_phi_cos4->GetParameter(4));
        //fit_phi_cos6->FixParameter(4,fit_phi_cos4->GetParameter(4));


        //mass_canv->cd(8);
        //fit_truth_6->Draw("E1");
        //fit_truth_6->SetLineWidth(3);
        //fit_truth_6->SetLineColorAlpha(2,1.0);
        //fit_truth_6->GetYaxis()->SetRangeUser(0,1.7*fit_truth_6->GetMaximum());
        //fit_truth_6->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_6->GetYaxis()->SetMaxDigits(4);
        //fit_truth_6->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_6->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_6->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_6->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_6->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_truth_6->Fit(fit_phi_cos5,fit_option.c_str(),"",abin_min,abin_max);
        //fit_phi_cos5->Draw("SAME");
        //fit_truth_6->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad8_stats = (TPaveStats*) fit_truth_6->FindObject("stats");
        //pad8_stats->SetX1NDC(0.55); pad8_stats->SetX2NDC(0.83);
        //pad8_stats->SetY1NDC(0.65); pad8_stats->SetY2NDC(0.79);
        //pad8_stats->SetTextFont(42);
        //pad8_stats->SetFillStyle(0);
        //TLegend * pad8_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad8_legend->SetBorderSize(0);
        //pad8_legend->SetFillColor(0);
        //pad8_legend->SetFillStyle(0);
        //pad8_legend->SetTextFont(42);
        //pad8_legend->SetTextSize(0.02);
        //pad8_legend->AddEntry(fit_truth_6,"Efficiency adjusted signal","LP");
        //pad8_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad8_legend->AddEntry(fit_phi_cos5,fit_strings[abin_var].at(1).c_str(),"L");
        //pad8_legend->Draw();
        //TLatex pad8_ltx;
        //pad8_ltx.SetTextSize(0.03);
        //pad8_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //fit_phi_cos6->FixParameter(5,fit_phi_cos5->GetParameter(5));

        //mass_canv->cd(9);
        //fit_truth_7->Draw("E1");
        //fit_truth_7->SetLineWidth(3);
        //fit_truth_7->SetLineColorAlpha(2,1.0);
        //fit_truth_7->GetYaxis()->SetRangeUser(0,1.7*fit_truth_7->GetMaximum());
        //fit_truth_7->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_7->GetYaxis()->SetMaxDigits(4);
        //fit_truth_7->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_7->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_7->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_7->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_7->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_truth_7->Fit(fit_phi_cos6,fit_option.c_str(),"",abin_min,abin_max);
        //fit_phi_cos6->Draw("SAME");
        //fit_truth_7->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad9_stats = (TPaveStats*) fit_truth_7->FindObject("stats");
        //pad9_stats->SetX1NDC(0.55); pad9_stats->SetX2NDC(0.83);
        //pad9_stats->SetY1NDC(0.65); pad9_stats->SetY2NDC(0.79);
        //pad9_stats->SetTextFont(42);
        //pad9_stats->SetFillStyle(0);
        //TLegend * pad9_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad9_legend->SetBorderSize(0);
        //pad9_legend->SetFillColor(0);
        //pad9_legend->SetFillStyle(0);
        //pad9_legend->SetTextFont(42);
        //pad9_legend->SetTextSize(0.02);
        //pad9_legend->AddEntry(fit_truth_7,"Efficiency adjusted signal","LP");
        //pad9_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad9_legend->AddEntry(fit_phi_cos6,fit_strings[abin_var].at(1).c_str(),"L");
        //pad9_legend->Draw();
        //TLatex pad9_ltx;
        //pad9_ltx.SetTextSize(0.03);
        //pad9_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //
        //fit_cs_four->SetParameter(0, fit_phi_cnst->GetParameter(0));
        //fit_cs_four->SetParameter(1, fit_phi_cos1->GetParameter(1));
        //fit_cs_four->SetParameter(2, fit_phi_cos2->GetParameter(2));
        //fit_cs_four->SetParameter(3, fit_phi_cos3->GetParameter(3));
        //fit_cs_four->SetParameter(4, fit_phi_cos4->GetParameter(4));
        //fit_cs_four->SetRange(abin_min,abin_max);

        //fit_cs_six->SetParameter(0, fit_phi_cnst->GetParameter(0));
        //fit_cs_six->SetParameter(1, fit_phi_cos1->GetParameter(1));
        //fit_cs_six->SetParameter(2, fit_phi_cos2->GetParameter(2));
        //fit_cs_six->SetParameter(3, fit_phi_cos3->GetParameter(3));
        //fit_cs_six->SetParameter(4, fit_phi_cos4->GetParameter(4));
        //fit_cs_six->SetParameter(5, fit_phi_cos5->GetParameter(5));
        //fit_cs_six->SetParameter(6, fit_phi_cos6->GetParameter(6));
        //fit_cs_six->SetRange(abin_min,abin_max);

        //mass_canv->cd(10);
        //fit_truth_8->Draw("E1");
        //fit_truth_8->SetLineWidth(3);
        //fit_truth_8->SetLineColorAlpha(2,1.0);
        //fit_truth_8->GetYaxis()->SetRangeUser(0,1.7*fit_truth_8->GetMaximum());
        //fit_truth_8->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_8->GetYaxis()->SetMaxDigits(4);
        //fit_truth_8->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_8->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_8->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_8->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_8->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_cs_four->Draw("SAME");
        //fit_truth_8->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        ////TPaveStats * pad10_stats = (TPaveStats*) fit_truth_8->FindObject("stats");
        ////pad10_stats->SetX1NDC(0.55); pad10_stats->SetX2NDC(0.83);
        ////pad10_stats->SetY1NDC(0.65); pad10_stats->SetY2NDC(0.79);
        ////pad10_stats->SetTextFont(42);
        ////pad10_stats->SetFillStyle(0);
        //TLegend * pad10_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad10_legend->SetBorderSize(0);
        //pad10_legend->SetFillColor(0);
        //pad10_legend->SetFillStyle(0);
        //pad10_legend->SetTextFont(42);
        //pad10_legend->SetTextSize(0.02);
        //pad10_legend->AddEntry(fit_truth_8,"Efficiency adjusted signal","LP");
        //pad10_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad10_legend->AddEntry(fit_cs_four,fit_strings[abin_var].at(1).c_str(),"L");
        //pad10_legend->Draw();
        //TLatex pad10_ltx;
        //pad10_ltx.SetTextSize(0.03);
        //pad10_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //mass_canv->cd(11);
        //fit_truth_9->Draw("E1");
        //fit_truth_9->SetLineWidth(3);
        //fit_truth_9->SetLineColorAlpha(2,1.0);
        //fit_truth_9->GetYaxis()->SetRangeUser(0,1.7*fit_truth_9->GetMaximum());
        //fit_truth_9->GetYaxis()->SetTitle(Form("Entries/%s",width.c_str()));
        //fit_truth_9->GetYaxis()->SetMaxDigits(4);
        //fit_truth_9->GetXaxis()->SetLabelSize(0.035);
        //fit_truth_9->GetXaxis()->SetTitleSize(0.035);
        //fit_truth_9->GetYaxis()->SetLabelSize(0.035);
        //fit_truth_9->GetYaxis()->SetTitleSize(0.035);
        //fit_truth_9->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(), unit.c_str()));
        //fit_cs_six->Draw("SAME");
        //fit_truth_9->Draw("SAME HIST");
        //fit_reco->Draw("E1 SAME");
        //fit_reco->Draw("HIST SAME");
        //fit_reco->SetLineColor(kBlue+1);
        //gPad->Modified(); gPad->Update();
        ////TPaveStats * pad11_stats = (TPaveStats*) fit_truth_9->FindObject("stats");
        ////pad11_stats->SetX1NDC(0.55); pad11_stats->SetX2NDC(0.83);
        ////pad11_stats->SetY1NDC(0.65); pad11_stats->SetY2NDC(0.79);
        ////pad11_stats->SetTextFont(42);
        ////pad11_stats->SetFillStyle(0);
        //TLegend * pad11_legend = new TLegend(0.2,0.58,0.6,0.68);
        //pad11_legend->SetBorderSize(0);
        //pad11_legend->SetFillColor(0);
        //pad11_legend->SetFillStyle(0);
        //pad11_legend->SetTextFont(42);
        //pad11_legend->SetTextSize(0.02);
        //pad11_legend->AddEntry(fit_truth_9,"Efficiency adjusted signal","LP");
        //pad11_legend->AddEntry(fit_reco,"HF extracted signal","LP");
        //pad11_legend->AddEntry(fit_cs_six,fit_strings[abin_var].at(1).c_str(),"L");
        //pad11_legend->Draw();
        //TLatex pad11_ltx;
        //pad11_ltx.SetTextSize(0.03);
        //pad11_ltx.DrawLatexNDC(0.25,0.825,Form("%s fit - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");


