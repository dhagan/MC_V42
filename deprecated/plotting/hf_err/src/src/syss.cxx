#include <sys.hxx>
#include <split_string.hxx>

void syss(std::string eff_path, std::string hf_path, std::string sys_eff_path, std::string sys_hf_path, 
    std::string abin_vars, std::string spec_vars, std::string type, std::string slice, std::string unique, 
    std::string range){


  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  TLatex ATLAS; ATLAS.SetNDC(); 
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42); 
  wip.SetTextSize(0.038); wip.SetTextColor(1);

  int mass_bins[5] = {0,3,4,5,12};
  
  TFile * base_eff_file         = new TFile(eff_path.c_str(),   "READ");
  TFile * base_integral_file    = new TFile(hf_path.c_str(),    "READ");

  TFile * sys_eff_file          = new TFile(sys_eff_path.c_str(),    "READ");
  TFile * sys_integral_file     = new TFile(sys_hf_path.c_str(),    "READ");

  TF1 * fit_single_gaussian = new TF1( "fit_double_gaussian", "[0]*exp( -((x-[1])^2)/(2*[2]^2))", -5, 15); 
  fit_single_gaussian->SetParLimits(0, 100, 50000);
  fit_single_gaussian->SetParLimits(1,-5,15); 
  fit_single_gaussian->SetParLimits(2,0,10); 

  fit_single_gaussian->SetParameter(0,25000); 
  fit_single_gaussian->SetParameter(1,5.0); 
  fit_single_gaussian->SetParameter(2,4); 

  fit_single_gaussian->SetParName(0,"C"); fit_single_gaussian->SetParName(1,"X"); fit_single_gaussian->SetParName(2,"Sigma");
  fit_single_gaussian->SetLineColor(kRed+1);
  
  TF1 * fit_double_gaussian = new TF1("fit_double_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) +"
                                      "[3]*exp(-((x-[1])^2)/(2*[4]^2)) ", -5.0, 15.0 );

  fit_double_gaussian->SetParName(0,"C1");
  fit_double_gaussian->SetParName(1,"X1");
  fit_double_gaussian->SetParName(2,"sig1");
  fit_double_gaussian->SetParName(3,"C2");
  fit_double_gaussian->SetParName(4,"sig2");
  //fit_double_gaussian->SetParLimits(0,0,3000);
  //fit_double_gaussian->SetParLimits(1,-5,15);
  //fit_double_gaussian->SetParLimits(2,2,6);
  //fit_double_gaussian->SetParLimits(3,0,3000);
  //fit_double_gaussian->SetParLimits(4,99,101);
  fit_double_gaussian->SetParameter(2,3);
  fit_double_gaussian->SetParameter(3,100);
  fit_double_gaussian->SetParameter(4,50);
  fit_double_gaussian->SetLineColor(kRed+1);
   
  
  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DPhi"]          =  std::vector<double>{8,0,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{10,5,30};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5,25};

  if ( !range.empty() ){
    std::vector< std::string > arg_bin;
    std::vector< std::string > set_bins;
    split_strings( arg_bin, range, "_" );
    split_strings( set_bins, arg_bin.at(1), "," );
    std::map< std::string, std::vector<double> >::iterator bin_map_itr = analysis_bins.find( arg_bin.at( 0 ).c_str() );
    bin_map_itr->second = { std::stod( set_bins.at( 0 ) ), std::stod( set_bins.at( 1 ) ), std::stod( set_bins.at( 2 ) ) };
    std::cout << range << std::endl;
  }
  
  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["BDT"]           =  "Value";
  units["abs(qtB)"]      =  "GeV";
  units["qtB"]           =  "GeV";
  units["Phi"]           =  "rad";
  units["DPhi"]          =  "rad";
  units["DiMuonPt"]      =  "GeV";
  units["PhotonPt"]      =  "GeV";

  int hf_error_bin = 0;
  std::string full_type;
  if (type.find("data") != std::string::npos){ hf_error_bin = 1; full_type = "data";}  
  if (type.find("sign") != std::string::npos){ hf_error_bin = 2; full_type = "signal";}
  if (type.find("bckg") != std::string::npos){ hf_error_bin = 3; full_type = "background";}

  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";


  for ( int & mass_index : mass_bins){

    char mass_char[100];        
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
    for (std::string abin_var : vec_abin_vars){
        
      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      if (abin_var.find("_low") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      if (abin_var.find("_high") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)    bins.at(0);
      double abin_min   = (double) bins.at(1);
      double abin_max   = (double) bins.at(2);
      double width      = (double) (abin_max-abin_min)/((double) abin_bins);

      TH1F * unit_hist = new TH1F( "unit_hist", "", abin_bins, abin_min, abin_max );
      for ( int unit_idx = 1; unit_idx <= abin_bins; unit_idx++ ){
        unit_hist->SetBinContent( unit_idx, 1.0 ); 
      }

      for (std::string spec_var : vec_spec_vars){
        
        TH1F * base_ex_reco = new TH1F( Form("base_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * base_eff     = (TH1F*) base_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));

        TH1F * sys_ex_reco  = new TH1F( Form("sys_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * sys_eff      = (TH1F*) sys_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));

        //TH1F * extr_corr_ratio  = new TH1F(   Form("sys_base_ratio_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * efficiency_ratio  = new TH1F(   Form("sys_base_eff_ratio_Q%i",mass_index), "", abin_bins, abin_min, abin_max);

        bool base_error_available{true}, sys_error_available{true};
        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
           
          char bin_hist_name[150], error_name[150];
          sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);

          TH1F * base_integral_hist   = (TH1F*) base_integral_file->Get(bin_hist_name);
          TH1F * base_error_hist      = (TH1F*) base_integral_file->Get(error_name);

          TH1F * sys_integral_hist    = (TH1F*) sys_integral_file->Get(bin_hist_name);
          TH1F * sys_error_hist       = (TH1F*) sys_integral_file->Get(error_name);

          base_ex_reco->SetBinContent(abin_index, base_integral_hist->Integral());
          if ( base_error_hist != NULL ){ 
            base_ex_reco->SetBinError(abin_index,base_error_hist->GetBinError(hf_error_bin));
          } else { base_error_available = false; }
          sys_ex_reco->SetBinContent(abin_index, sys_integral_hist->Integral());
          if ( sys_error_hist != NULL ){ 
            sys_ex_reco->SetBinError(abin_index,sys_error_hist->GetBinError(hf_error_bin));
          } else { sys_error_available = false; }
        }
        if ( !base_error_available ){ base_ex_reco->Sumw2(); }
        if ( !sys_error_available ){ sys_ex_reco->Sumw2(); }

        std::string unit = units[abin_var];
        
        TF1 * unit_line = new TF1( "unit_line", "1", abin_min, abin_max );
        unit_line->SetLineColorAlpha(1,0.5);
        unit_line->SetLineStyle(2);
        unit_line->SetLineWidth(1.0);

        TF1 * zero_line = new TF1( "zero_line", "0", abin_min, abin_max );
        zero_line->SetLineColorAlpha(1,0.5);
        zero_line->SetLineStyle(2);
        zero_line->SetLineWidth(1.0);

        TH1F * base_ex_corr = new TH1F( Form("base_ex_corr_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * sys_ex_corr = new TH1F( Form("sys_ex_corr_Q%i",mass_index), "", abin_bins, abin_min, abin_max);

        TH1F * rel_err_hist = new TH1F( Form( "rel_err_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
        TH1F * abs_err_hist = new TH1F( Form( "abs_err_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
        
        base_ex_corr->Divide( base_ex_reco, base_eff );
        sys_ex_corr->Divide( sys_ex_reco, sys_eff );

        abs_err_hist->Add( sys_ex_corr, base_ex_corr, 1.0, -1.0 );
        rel_err_hist->Divide( abs_err_hist, base_ex_corr, 1.0, 1.0 );
        efficiency_ratio->Divide( sys_eff, base_eff, 1.0, 1.0 );


        double sys_x[15];
        double sys_y[15];

        double rel_err_x_upper[15];
        double rel_err_x_lower[15];
        double rel_err_y_upper[15];
        double rel_err_y_lower[15];

        double abs_err_x_upper[15];
        double abs_err_x_lower[15];
        double abs_err_y_upper[15];
        double abs_err_y_lower[15];

        for ( int err_bin = 0; err_bin < abin_bins; err_bin++){ 
          sys_x[err_bin] = (abin_min+(width/2.0)) + width*((double) err_bin);
          //sys_y[err_bin] = 1.0;
          sys_y[err_bin] = 0.0;
          rel_err_x_upper[err_bin] = 0.0;
          rel_err_x_lower[err_bin] = 0.0;
          abs_err_x_upper[err_bin] = 0.0;
          abs_err_x_lower[err_bin] = 0.0;
          double rel_sys_err = rel_err_hist->GetBinContent(err_bin+1);
          double abs_sys_err = abs_err_hist->GetBinContent(err_bin+1);
          if ( rel_sys_err > 0 ){
            rel_err_y_lower[err_bin] = 0.0;
            rel_err_y_upper[err_bin] = abs(rel_sys_err);
          } else {
            rel_err_y_lower[err_bin] = abs(rel_sys_err);
            rel_err_y_upper[err_bin] = 0.0;
          }
          if ( abs_sys_err > 0 ){
            abs_err_y_lower[err_bin] = 0.0;
            abs_err_y_upper[err_bin] = abs(abs_sys_err);
          } else {
            abs_err_y_lower[err_bin] = abs(abs_sys_err);
            abs_err_y_upper[err_bin] = 0.0;
          }
        }
        TGraphAsymmErrors * rel_err_graph = new TGraphAsymmErrors( 15, sys_x, sys_y, rel_err_x_lower, rel_err_x_upper, 
                                                               rel_err_y_lower, rel_err_y_upper );
        rel_err_graph->SetFillStyle(3004);
        rel_err_graph->SetFillColorAlpha(3,1.0);
        rel_err_graph->SetLineWidth(0);

        TGraphAsymmErrors * abs_err_graph = new TGraphAsymmErrors( 15, sys_x, sys_y, abs_err_x_lower, abs_err_x_upper,
                                                                  abs_err_y_lower, abs_err_y_upper);
        abs_err_graph->SetFillStyle(3004);
        abs_err_graph->SetFillColorAlpha(3,1.0);
        abs_err_graph->SetLineWidth(0);

        TH1F * base_ex_corr_dg_fit = (TH1F*) base_ex_corr->Clone( Form("base_ex_corr_dg_fit_Q%i",mass_index) );
        base_ex_corr_dg_fit->Fit( fit_double_gaussian, "MQ", "", abin_min, abin_max );
        double dg_c1, dg_x1, dg_sig1, dg_c2, dg_sig2;
        dg_c1   = fit_double_gaussian->GetParameter(0);
        dg_x1   = fit_double_gaussian->GetParameter(1);
        dg_sig1 = fit_double_gaussian->GetParameter(2);
        dg_c2   = fit_double_gaussian->GetParameter(3);
        dg_sig2 = fit_double_gaussian->GetParameter(4);

        TH1F * base_ex_corr_sg_fit = (TH1F*) base_ex_corr->Clone( Form("base_ex_corr_sg_fit_Q%i",mass_index) );
        base_ex_corr_sg_fit->Fit( fit_single_gaussian, "MQ", "", abin_min, abin_max );
        double sg_c, sg_x, sg_s;
        sg_c   = fit_single_gaussian->GetParameter(0);
        sg_x   = fit_single_gaussian->GetParameter(1);
        sg_s   = fit_single_gaussian->GetParameter(2);


        TH1F * sys_ex_corr_dg_fit = (TH1F*) sys_ex_corr->Clone( Form("sys_ex_corr_dg_fit_Q%i",mass_index) );
        TH1F * sys_ex_corr_sg_fit = (TH1F*) sys_ex_corr->Clone( Form("sys_ex_corr_sg_fit_Q%i",mass_index) );

        TCanvas * mass_canv = new TCanvas(Form("canv_Q%i",mass_index), "eff_canv", 100, 100, 3000, 2000);
        mass_canv->Divide(3,2);

        mass_canv->cd(1);
        base_ex_corr->Draw("E1");
        base_ex_corr->Draw("HIST SAME");
        base_ex_corr->GetYaxis()->SetRangeUser( 0, base_ex_corr->GetMaximum()*1.5 );
        base_ex_corr->SetLineWidth(1);
        base_ex_corr->SetFillColorAlpha(kRed+1,0.6);
        base_ex_corr->SetLineColorAlpha(kRed+1,0.6);
        base_ex_corr->GetYaxis()->SetTitle( Form("Extracted Signal/%.3f %s", width, unit.c_str()));
        base_ex_corr->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        base_ex_corr->GetYaxis()->SetLabelSize(0.035);
        base_ex_corr->GetYaxis()->SetTitleSize(0.035);
        base_ex_corr->GetXaxis()->SetLabelSize(0.035);
        base_ex_corr->GetXaxis()->SetTitleSize(0.035);
        base_ex_corr->GetYaxis()->SetMaxDigits(3);
        sys_ex_corr->Draw("E1 SAME");
        sys_ex_corr->Draw("HIST SAME");
        sys_ex_corr->SetLineColor(1);
        sys_ex_corr->SetFillStyle(0);
        sys_ex_corr->SetLineWidth(1);
        TLegend * pad1_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
        pad1_legend->SetBorderSize( 0 );
        pad1_legend->SetFillColor( 0 );
        pad1_legend->SetFillStyle( 0 );
        pad1_legend->SetTextFont( 42 );
        pad1_legend->SetTextSize( 0.02 );
        pad1_legend->AddEntry( base_ex_corr, "Nominal Distribution", "LF");
        pad1_legend->AddEntry( sys_ex_corr, "Systematic Distribution", "L");
        pad1_legend->Draw();
        TLatex pad1_ltx;
        pad1_ltx.SetTextSize( 0.03 );
        pad1_ltx.DrawLatexNDC( 0.1, 0.85, unique.c_str() );
        pad1_ltx.DrawLatexNDC( 0.225, 0.825, Form( "%s Distributions Q%i", abin_var.c_str(), mass_index ));
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        mass_canv->cd(2);
        efficiency_ratio->Draw("E1");
        efficiency_ratio->SetMarkerStyle(21);
        efficiency_ratio->SetLineColor(1);
        efficiency_ratio->GetYaxis()->SetRangeUser( -1.0, 1.0 );
        efficiency_ratio->GetYaxis()->SetTitle(Form("Efficiency err sys/base / %.3f %s",width, unit.c_str()));
        efficiency_ratio->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        efficiency_ratio->GetYaxis()->SetLabelSize(0.035);
        efficiency_ratio->GetYaxis()->SetTitleSize(0.035);
        efficiency_ratio->GetXaxis()->SetLabelSize(0.035);
        efficiency_ratio->GetXaxis()->SetTitleSize(0.035);
        zero_line->Draw("SAME");
        TLegend * eff_pad_legend = new TLegend(0.45,0.65,0.83,0.8);
        eff_pad_legend->SetBorderSize(0);
        eff_pad_legend->SetFillColor(0);
        eff_pad_legend->SetFillStyle(0);
        eff_pad_legend->SetTextFont(42);
        eff_pad_legend->SetTextSize(0.025);
        eff_pad_legend->AddEntry(efficiency_ratio,"Efficiency Ratio","LP");
        eff_pad_legend->Draw();
        TLatex pad2_ltx;
        pad2_ltx.SetTextSize(0.03);
        pad2_ltx.DrawLatexNDC( 0.1, 0.85, unique.c_str() );
        pad2_ltx.DrawLatexNDC( 0.225, 0.825,Form("%s Efficiency ratio sys/base - Mass Q%i",abin_var.c_str(),mass_index));  
        ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        mass_canv->cd(3);
        abs_err_hist->Draw("HIST P");
        abs_err_hist->SetMarkerStyle(21);
        abs_err_hist->SetLineColor(1);
        double abs_lim = std::ceil(std::max( { abs(abs_err_hist->GetMinimum()), abs(abs_err_hist->GetMaximum()) } ) * 3);
        abs_err_hist->GetYaxis()->SetRangeUser( -abs_lim, abs_lim );
        abs_err_hist->GetYaxis()->SetTitle( Form("Abs error/%.3f %s", width, unit.c_str()));
        abs_err_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        abs_err_hist->GetYaxis()->SetLabelSize(0.035);
        abs_err_hist->GetYaxis()->SetTitleSize(0.035);
        abs_err_hist->GetXaxis()->SetLabelSize(0.035);
        abs_err_hist->GetXaxis()->SetTitleSize(0.035);
        abs_err_hist->GetYaxis()->SetMaxDigits(3);
        abs_err_graph->SetFillColorAlpha(kBlue+1,0.6);
        abs_err_graph->SetFillStyle(1);
        abs_err_graph->Draw("3 SAME");
        TLegend * pad3_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
        pad3_legend->SetBorderSize( 0 );
        pad3_legend->SetFillColor( 0 );
        pad3_legend->SetFillStyle( 0 );
        pad3_legend->SetTextFont( 42 );
        pad3_legend->SetTextSize( 0.02 );
        pad3_legend->AddEntry( abs_err_hist, "Distribution Difference", "P");
        pad3_legend->AddEntry( abs_err_graph, "Abs Sys Error", "F");
        pad3_legend->Draw();
        TLatex pad3_ltx;
        pad3_ltx.SetTextSize( 0.03 );
        pad2_ltx.DrawLatexNDC( 0.1, 0.85, unique.c_str() );
        pad3_ltx.DrawLatexNDC( 0.225, 0.825, Form( "%s Absolute Systematic Error Q%i", abin_var.c_str(), mass_index ));
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        mass_canv->cd(4);
        rel_err_hist->Draw("HIST P");
        rel_err_hist->SetMarkerStyle(21);
        rel_err_hist->SetLineColor(1);
        //double rel_lim = std::ceil(std::max( { abs(rel_err_hist->GetMinimum()), abs(rel_err_hist->GetMaximum()) } ) * 3);
        rel_err_hist->GetYaxis()->SetRangeUser( -4.0, 4.0 );
        rel_err_hist->GetYaxis()->SetTitle( Form("rel error/%.3f %s", width, unit.c_str()));
        rel_err_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        rel_err_hist->GetYaxis()->SetLabelSize(0.035);
        rel_err_hist->GetYaxis()->SetTitleSize(0.035);
        rel_err_hist->GetXaxis()->SetLabelSize(0.035);
        rel_err_hist->GetXaxis()->SetTitleSize(0.035);
        rel_err_hist->GetYaxis()->SetMaxDigits(3);
        rel_err_graph->SetFillColorAlpha(kBlue+1,0.6);
        rel_err_graph->SetFillStyle(1);
        rel_err_graph->Draw("3 SAME");
        TLegend * pad4_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
        pad4_legend->SetBorderSize( 0 );
        pad4_legend->SetFillColor( 0 );
        pad4_legend->SetFillStyle( 0 );
        pad4_legend->SetTextFont( 42 );
        pad4_legend->SetTextSize( 0.02 );
        pad4_legend->AddEntry( rel_err_hist, "Distribution Ratio", "P");
        pad4_legend->AddEntry( rel_err_graph, "Relative Sys Error", "F");
        pad4_legend->Draw();
        TLatex pad4_ltx;
        pad4_ltx.SetTextSize( 0.03 );
        pad4_ltx.DrawLatexNDC( 0.1, 0.85, unique.c_str() );
        pad4_ltx.DrawLatexNDC( 0.225, 0.825, Form( "%s Relative Systematic Error Q%i", abin_var.c_str(), mass_index ));
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        mass_canv->cd(5);
        sys_ex_corr_sg_fit->Draw("E1");
        sys_ex_corr_sg_fit->Draw("HIST SAME");
        sys_ex_corr_sg_fit->GetYaxis()->SetRangeUser( 0, sys_ex_corr_dg_fit->GetMaximum()*2.5 );
        sys_ex_corr_sg_fit->SetLineWidth(1);
        sys_ex_corr_sg_fit->GetYaxis()->SetTitle( Form("Sys Extracted Signal/%.3f %s", width, unit.c_str()));
        sys_ex_corr_sg_fit->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        sys_ex_corr_sg_fit->GetYaxis()->SetLabelSize(0.035);
        sys_ex_corr_sg_fit->GetYaxis()->SetTitleSize(0.035);
        sys_ex_corr_sg_fit->GetXaxis()->SetLabelSize(0.035);
        sys_ex_corr_sg_fit->GetXaxis()->SetTitleSize(0.035);
        sys_ex_corr_sg_fit->GetYaxis()->SetMaxDigits(3);
        sys_ex_corr_sg_fit->Fit( fit_single_gaussian, "MQ", "", abin_min, abin_max );
        gPad->Update(); gPad->Modified();
        fit_single_gaussian->SetLineColorAlpha( kRed+1, 1.0 );
        fit_single_gaussian->Draw( "SAME" );
        TPaveStats * sg_stats = (TPaveStats*) sys_ex_corr_sg_fit->FindObject("stats");
        sg_stats->SetTextFont(42);
        sg_stats->SetX1NDC(0.5); sg_stats->SetX2NDC(0.825);
        sg_stats->SetY1NDC(0.5); sg_stats->SetY2NDC(0.78);
        sg_stats->SetFillStyle(0);
        TLegend sg_legend(0.2, 0.71, 0.7,  0.78);
        sg_legend.SetTextSize(0.025);
        sg_legend.SetFillStyle(0);
        sg_legend.SetTextFont(42);
        sg_legend.SetBorderSize(0);
        sg_legend.AddEntry( sys_ex_corr_sg_fit, "sys", "l");
        sg_legend.AddEntry( fit_single_gaussian, "sg fit", "l");
        sg_legend.Draw();
        TLatex sg_ltx;
        sg_ltx.SetTextSize(0.03);
        sg_ltx.SetTextFont(42);
        sg_ltx.DrawLatexNDC( 0.21, 0.68, Form( "base x   - %.2f", sg_x) );
        sg_ltx.DrawLatexNDC( 0.21, 0.65, Form( "base c   - %.2f", sg_c) );
        sg_ltx.DrawLatexNDC( 0.21, 0.62, Form( "base sig - %.2f", sg_s) );
        TLatex sg_title_ltx;
        sg_title_ltx.SetTextSize( 0.03 );
        sg_title_ltx.DrawLatexNDC( 0.23, 0.825, Form( "%s single gaussian Q%i", abin_var.c_str(), mass_index ));


        mass_canv->cd(6);
        sys_ex_corr_dg_fit->Draw("E1");
        sys_ex_corr_dg_fit->Draw("HIST SAME");
        sys_ex_corr_dg_fit->GetYaxis()->SetRangeUser( 0, sys_ex_corr_dg_fit->GetMaximum()*2.5 );
        sys_ex_corr_dg_fit->SetLineWidth(1);
        sys_ex_corr_dg_fit->GetYaxis()->SetTitle( Form("Sys Extracted Signal/%.3f %s", width, unit.c_str()));
        sys_ex_corr_dg_fit->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        sys_ex_corr_dg_fit->GetYaxis()->SetLabelSize(0.035);
        sys_ex_corr_dg_fit->GetYaxis()->SetTitleSize(0.035);
        sys_ex_corr_dg_fit->GetXaxis()->SetLabelSize(0.035);
        sys_ex_corr_dg_fit->GetXaxis()->SetTitleSize(0.035);
        sys_ex_corr_dg_fit->GetYaxis()->SetMaxDigits(3);
        sys_ex_corr_dg_fit->Fit( fit_double_gaussian, "MQ", "", abin_min, abin_max );
        gPad->Update(); gPad->Modified();
        //fit_double_gaussian->SetLineColorAlpha( kRed+1, 1.0 );
        fit_double_gaussian->Draw( "SAME" );
        TPaveStats * dg_stats = (TPaveStats*) sys_ex_corr_dg_fit->FindObject("stats");
        dg_stats->SetX1NDC(0.5); dg_stats->SetX2NDC(0.825);
        dg_stats->SetY1NDC(0.5); dg_stats->SetY2NDC(0.78);
        dg_stats->SetTextFont(42);
        dg_stats->SetFillStyle(0);
        TLegend dg_legend(0.2, 0.71, 0.7,  0.78);
        dg_legend.SetTextSize(0.025);
        dg_legend.SetFillStyle(0);
        dg_legend.SetTextFont(42);
        dg_legend.SetBorderSize(0);
        dg_legend.AddEntry( sys_ex_corr_dg_fit, "sys", "l");
        dg_legend.AddEntry( fit_single_gaussian, "dg fit", "l");
        dg_legend.Draw();
        TLatex dg_ltx;
        dg_ltx.SetTextSize(0.03);
        dg_ltx.SetTextFont(42);
        dg_ltx.DrawLatexNDC( 0.21, 0.68, Form( "base x1  - %.2f", dg_x1) );
        dg_ltx.DrawLatexNDC( 0.21, 0.65, Form( "base c1  - %.2f", dg_c1) );
        dg_ltx.DrawLatexNDC( 0.21, 0.62, Form( "base s1  - %.2f", dg_sig1) );
        dg_ltx.DrawLatexNDC( 0.21, 0.59, Form( "base c2  - %.2f", dg_c2) );
        dg_ltx.DrawLatexNDC( 0.21, 0.56, Form( "base s2  - %.2f", dg_sig2) );

        TLatex dg_title_ltx;
        dg_title_ltx.SetTextSize( 0.03 );
        dg_title_ltx.DrawLatexNDC( 0.23, 0.825, Form( "%s double gaussian Q%i", abin_var.c_str(), mass_index ));


        
        mass_canv->SaveAs( Form( "./err_Q%i_A-%s_%s.png", mass_index,abin_var.c_str(),unique.c_str()));
        
        delete unit_hist;

      }
    }
  }
}
