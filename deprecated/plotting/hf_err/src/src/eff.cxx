#include <eff.hxx>
#include <split_string.hxx>
#include <TPaletteAxis.h>

void eff(const std::string filepath){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  int mass_bins[5] = {0,3,4,5,12};
  TFile eff_file(filepath.c_str(),"READ");

  //TEfficiency * qta_same = (TEfficiency*) eff_file.Get("qtA_same_eff"); 
  //TEfficiency * qta_full = (TEfficiency*) eff_file.Get("qtA_full_eff");
  //TEfficiency * qtb_same = (TEfficiency*) eff_file.Get("qtB_same_eff");
  //TEfficiency * qtb_full = (TEfficiency*) eff_file.Get("qtB_full_eff");
   
  TLatex ATLAS;
  ATLAS.SetNDC();
  ATLAS.SetTextFont(72);
  ATLAS.SetTextColor(1);
  TLatex wip; 
  wip.SetNDC();
  wip.SetTextFont(42);
  wip.SetTextSize(0.038);
  wip.SetTextColor(1);
  TLatex sim; 
  sim.SetNDC();
  sim.SetTextFont(42);
  sim.SetTextSize(0.038);
  sim.SetTextColor(1);

  for ( int & mass_bin : mass_bins){

    TH1F * qta_div  = (TH1F*) eff_file.Get(Form("qtA_Q%i_div",mass_bin)); 
    TH1F * qtb_div  = (TH1F*) eff_file.Get(Form("qtB_Q%i_div",mass_bin));
    TH1F * jpsi_div = (TH1F*) eff_file.Get(Form("jpsi_Q%i_pt_div",mass_bin)); 
    TH1F * phot_div = (TH1F*) eff_file.Get(Form("phot_Q%i_pt_div",mass_bin));
    TH1F * dphi_div = (TH1F*) eff_file.Get(Form("dPhi_Q%i_pt_div",mass_bin));

    TH1F * reco_qta  = (TH1F*) eff_file.Get(Form("reco_qtA_Q%i",mass_bin)); 
    TH1F * reco_qtb  = (TH1F*) eff_file.Get(Form("reco_qtB_Q%i",mass_bin));
    TH1F * reco_jpsi = (TH1F*) eff_file.Get(Form("reco_jpsi_pt_Q%i",mass_bin)); 
    TH1F * reco_phot = (TH1F*) eff_file.Get(Form("reco_phot_pt_Q%i",mass_bin));
    TH1F * reco_dphi = (TH1F*) eff_file.Get(Form("reco_dphi_Q%i",mass_bin));
    
    TH1F * truth_qta  = (TH1F*) eff_file.Get(Form("truth_qtA_Q%i",mass_bin)); 
    TH1F * truth_qtb  = (TH1F*) eff_file.Get(Form("truth_qtB_Q%i",mass_bin));
    TH1F * truth_jpsi = (TH1F*) eff_file.Get(Form("truth_jpsi_pt_Q%i",mass_bin)); 
    TH1F * truth_phot = (TH1F*) eff_file.Get(Form("truth_phot_pt_Q%i",mass_bin));
    TH1F * truth_dphi = (TH1F*) eff_file.Get(Form("truth_dphi_Q%i",mass_bin));


    TCanvas * eff_canv = new TCanvas(Form("eff_canv_Q%i",mass_bin), "eff_canv",100,100,3000,2000);
    eff_canv->Divide(3,2);

    eff_canv->cd(1);
    qta_div->Draw("E1");
    qta_div->SetMarkerStyle(21);
    qta_div->SetLineColor(kRed+1);
    qta_div->GetYaxis()->SetRangeUser(0,1.0);
    qta_div->GetYaxis()->SetTitle("Efficiency/2.0GeV");
    qta_div->GetXaxis()->SetTitle("qtA (GeV)");
    qta_div->GetYaxis()->SetLabelSize(0.035);
    qta_div->GetYaxis()->SetTitleSize(0.035);
    qta_div->GetXaxis()->SetLabelSize(0.035);
    qta_div->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    gPad->SetTicks(1,0);
    int qta_pre_scale = truth_qta->Integral();
    truth_qta->Scale(1.0/qta_pre_scale);
    reco_qta->Scale(1.0/qta_pre_scale);    
    truth_qta->SetLineWidth(0);
    truth_qta->SetFillColorAlpha(kRed+1,0.5);
    reco_qta->SetLineWidth(0);
    reco_qta->SetFillColorAlpha(kBlue+1,0.5);
    truth_qta->Draw("HIST SAME");
    reco_qta->Draw("HIST SAME");
    TGaxis * qta_axis = new TGaxis(15,0,15,1,0,qta_pre_scale,510,"+L");
    qta_axis->SetLabelSize(0.035);             
    qta_axis->SetLabelFont(42);                
    qta_axis->SetTitleOffset(1.3);            
    qta_axis->SetTitleFont(42);                
    qta_axis->SetTitleSize(0.035);
    qta_axis->SetMaxDigits(4);
    qta_axis->SetTitle("Entries/2.0GeV");
    qta_axis->Draw();
    TLegend * qta_legend = new TLegend(0.65,0.65,0.83,0.8);
    qta_legend->SetBorderSize(0);
    qta_legend->SetFillColor(0);
    qta_legend->SetFillStyle(0);
    qta_legend->SetTextFont(42);
    qta_legend->SetTextSize(0.025);
    qta_legend->AddEntry(qta_div,"efficiency","LP");
    qta_legend->AddEntry(truth_qta,"truth","F");
    qta_legend->AddEntry(reco_qta,"passed","F");
    qta_legend->Draw();
    TLatex qta_pad_ltx;
    qta_pad_ltx.SetTextSize(0.03);
    qta_pad_ltx.DrawLatexNDC(0.2,0.825,Form("qtA Efficiency - Mass Q%i",mass_bin));  
    ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
    wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
    sim.DrawLatexNDC(0.2,0.66,"Simulation");


    eff_canv->cd(2);
    qtb_div->Draw("E1");
    qtb_div->SetMarkerStyle(21);
    qtb_div->SetLineColor(kRed+1);
    qtb_div->GetYaxis()->SetRangeUser(0,1.0);
    qtb_div->GetYaxis()->SetTitle("Efficiency/1.0GeV");
    qtb_div->GetXaxis()->SetTitle("qtB (GeV)");
    qtb_div->GetYaxis()->SetLabelSize(0.035);
    qtb_div->GetYaxis()->SetTitleSize(0.035);
    qtb_div->GetXaxis()->SetLabelSize(0.035);
    qtb_div->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    gPad->SetTicks(1,0);
    int qtb_pre_scale = truth_qtb->Integral();
    truth_qtb->Scale(1.0/qtb_pre_scale);
    reco_qtb->Scale(1.0/qtb_pre_scale);    
    truth_qtb->SetLineWidth(0);
    truth_qtb->SetFillColorAlpha(kRed+1,0.5);
    reco_qtb->SetLineWidth(0);
    reco_qtb->SetFillColorAlpha(kBlue+1,0.5);
    truth_qtb->Draw("HIST SAME");
    reco_qtb->Draw("HIST SAME");
    TGaxis * qtb_axis = new TGaxis(15,0,15,1,0,qtb_pre_scale,510,"+L");
    qtb_axis->SetLabelSize(0.035);             
    qtb_axis->SetLabelFont(42);                
    qtb_axis->SetTitleOffset(1.3);
    qtb_axis->SetTitleFont(42);                
    qtb_axis->SetTitleSize(0.035);                
    qtb_axis->SetTitle("Entries/1.0GeV");
    qtb_axis->Draw();
    TLegend * qtb_legend = new TLegend(0.65,0.65,0.83,0.8);
    qtb_legend->SetBorderSize(0);
    qtb_legend->SetFillColor(0);
    qtb_legend->SetFillStyle(0);
    qtb_legend->SetTextFont(42);
    qtb_legend->SetTextSize(0.025);
    qtb_legend->AddEntry(qtb_div,"efficiency","LP");
    qtb_legend->AddEntry(truth_qtb,"truth","F");
    qtb_legend->AddEntry(reco_qtb,"passed","F");
    qtb_legend->Draw();
    TLatex qtb_pad_ltx;
    qtb_pad_ltx.SetTextSize(0.03);
    qtb_pad_ltx.DrawLatexNDC(0.2,0.825,Form("qtB Efficiency - Mass Q%i",mass_bin));  
    ATLAS.DrawLatexNDC(0.225,0.755,"ATLAS");
    wip.DrawLatexNDC(0.225,0.7,"Work In Progress");
    sim.DrawLatexNDC(0.225,0.66,"Simulation");

  
    eff_canv->cd(3);
    jpsi_div->Draw("E1");
    jpsi_div->SetMarkerStyle(21);
    jpsi_div->SetLineColor(kRed+1);
    jpsi_div->GetYaxis()->SetRangeUser(0,1.0);
    jpsi_div->GetYaxis()->SetTitle("Efficiency/2.5GeV");
    jpsi_div->GetXaxis()->SetTitle("DiMuon Pt (GeV)");
    jpsi_div->GetYaxis()->SetLabelSize(0.035);
    jpsi_div->GetYaxis()->SetTitleSize(0.035);
    jpsi_div->GetXaxis()->SetLabelSize(0.035);
    jpsi_div->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    gPad->SetTicks(1,0);
    int jpsi_pre_scale = truth_jpsi->Integral();
    truth_jpsi->Scale(1.0/jpsi_pre_scale);
    reco_jpsi->Scale(1.0/jpsi_pre_scale);    
    truth_jpsi->SetLineWidth(0);
    truth_jpsi->SetFillColorAlpha(kRed+1,0.5);
    reco_jpsi->SetLineWidth(0);
    reco_jpsi->SetFillColorAlpha(kBlue+1,0.5);
    truth_jpsi->Draw("HIST SAME");
    reco_jpsi->Draw("HIST SAME");
    TGaxis * jpsi_axis = new TGaxis(30,0,30,1,0,jpsi_pre_scale,510,"+L");
    jpsi_axis->SetLabelSize(0.035);             
    jpsi_axis->SetLabelFont(42);                
    jpsi_axis->SetTitleOffset(1.3);            
    jpsi_axis->SetTitleFont(42);              
    jpsi_axis->SetTitle("Entries/2.5GeV");
    jpsi_axis->Draw();
    TLegend * jpsi_legend = new TLegend(0.65,0.65,0.83,0.8);
    jpsi_legend->SetBorderSize(0);
    jpsi_legend->SetFillColor(0);
    jpsi_legend->SetFillStyle(0);
    jpsi_legend->SetTextFont(42);
    jpsi_legend->SetTextSize(0.025);
    jpsi_legend->AddEntry(jpsi_div,"efficiency","LP");
    jpsi_legend->AddEntry(truth_jpsi,"truth","F");
    jpsi_legend->AddEntry(reco_jpsi,"passed","F");
    jpsi_legend->Draw();
    TLatex jpsi_pad_ltx;
    jpsi_pad_ltx.SetTextSize(0.03);
    jpsi_pad_ltx.DrawLatexNDC(0.2,0.825,Form("J/\\psi P_{T} Efficiency - Mass Q%i",mass_bin));  
    ATLAS.DrawLatexNDC(0.225,0.755,"ATLAS");
    wip.DrawLatexNDC(0.225,0.7,"Work In Progress");
    sim.DrawLatexNDC(0.225,0.66,"Simulation");
    


    eff_canv->cd(4);
    phot_div->Draw("E1");
    phot_div->SetMarkerStyle(21);
    phot_div->SetLineColor(kRed+1);
    phot_div->GetYaxis()->SetRangeUser(0,1.0);
    phot_div->GetYaxis()->SetTitle("Efficiency/2.0GeV");
    phot_div->GetXaxis()->SetTitle("Photon Pt (GeV)");
    phot_div->GetYaxis()->SetLabelSize(0.035);
    phot_div->GetYaxis()->SetTitleSize(0.035);
    phot_div->GetXaxis()->SetLabelSize(0.035);
    phot_div->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    gPad->SetTicks(1,0);
    int phot_pre_scale = truth_phot->Integral();
    truth_phot->Scale(1.0/phot_pre_scale);
    reco_phot->Scale(1.0/phot_pre_scale);    
    truth_phot->SetLineWidth(0);
    truth_phot->SetFillColorAlpha(kRed+1,0.5);
    reco_phot->SetLineWidth(0);
    reco_phot->SetFillColorAlpha(kBlue+1,0.5);
    truth_phot->Draw("HIST SAME");
    reco_phot->Draw("HIST SAME");
    TGaxis * phot_axis = new TGaxis(25,0,25,1,0,phot_pre_scale,510,"+L");
    phot_axis->SetLabelSize(0.035);             
    phot_axis->SetLabelFont(42);                
    phot_axis->SetTitleOffset(1.3);            
    phot_axis->SetTitleFont(42);                
    phot_axis->SetTitle("Entries/2.0GeV");
    phot_axis->Draw();
    TLegend * phot_legend = new TLegend(0.65,0.65,0.83,0.8);
    phot_legend->SetBorderSize(0);
    phot_legend->SetFillColor(0);
    phot_legend->SetFillStyle(0);
    phot_legend->SetTextFont(42);
    phot_legend->SetTextSize(0.025);
    phot_legend->AddEntry(phot_div,"efficiency","LP");
    phot_legend->AddEntry(truth_phot,"truth","F");
    phot_legend->AddEntry(reco_phot,"passed","F");
    phot_legend->Draw();
    TLatex phot_pad_ltx;
    phot_pad_ltx.SetTextSize(0.03);
    phot_pad_ltx.DrawLatexNDC(0.2,0.825,Form("Photon P_{T} Efficiency - Mass Q%i",mass_bin));  
    ATLAS.DrawLatexNDC(0.225,0.755,"ATLAS");
    wip.DrawLatexNDC(0.225,0.7,"Work In Progress");
    sim.DrawLatexNDC(0.225,0.66,"Simulation");

    eff_canv->cd(5);
    dphi_div->Draw("E1");
    dphi_div->SetMarkerStyle(21);
    dphi_div->SetLineColor(kRed+1);
    dphi_div->GetYaxis()->SetRangeUser(0,1.0);
    dphi_div->GetYaxis()->SetTitle("Efficiency/ 1/8^{th}\\pi rad");
    dphi_div->GetXaxis()->SetTitle("dPhi (rad) ");
    dphi_div->GetYaxis()->SetLabelSize(0.035);
    dphi_div->GetYaxis()->SetTitleSize(0.035);
    dphi_div->GetXaxis()->SetLabelSize(0.035);
    dphi_div->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    gPad->SetTicks(1,0);
    int dphi_pre_scale = truth_dphi->Integral();
    truth_dphi->Scale(1.0/dphi_pre_scale);
    reco_dphi->Scale(1.0/dphi_pre_scale);    
    truth_dphi->SetLineWidth(0);
    truth_dphi->SetFillColorAlpha(kRed+1,0.5);
    reco_dphi->SetLineWidth(0);
    reco_dphi->SetFillColorAlpha(kBlue+1,0.5);
    truth_dphi->Draw("HIST SAME");
    reco_dphi->Draw("HIST SAME");
    TGaxis * dphi_axis = new TGaxis(M_PI,0,M_PI,1,0,dphi_pre_scale,510,"+L");
    dphi_axis->SetLabelSize(0.035);             
    dphi_axis->SetLabelFont(42);                
    dphi_axis->SetTitleOffset(1.3);            
    dphi_axis->SetTitleFont(42);                
    dphi_axis->SetTitle("Entries/1/8^{th}\\pi rad");
    dphi_axis->Draw();
    TLegend * dphi_legend = new TLegend(0.65,0.65,0.83,0.8);
    dphi_legend->SetBorderSize(0);
    dphi_legend->SetFillColor(0);
    dphi_legend->SetFillStyle(0);
    dphi_legend->SetTextFont(42);
    dphi_legend->SetTextSize(0.025);
    dphi_legend->AddEntry(dphi_div,"efficiency","LP");
    dphi_legend->AddEntry(truth_dphi,"truth","F");
    dphi_legend->AddEntry(reco_dphi,"passed","F");
    dphi_legend->Draw();
    TLatex dphi_pad_ltx;
    dphi_pad_ltx.SetTextSize(0.03);
    dphi_pad_ltx.DrawLatexNDC(0.2,0.825,"");  
    ATLAS.DrawLatexNDC(0.225,0.755,"ATLAS");
    wip.DrawLatexNDC(0.225,0.7,"Work In Progress");
    sim.DrawLatexNDC(0.225,0.66,"Simulation");


    eff_canv->SaveAs(Form("Efficiency_Q%i.png",mass_bin));
    delete eff_canv;
  }

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);
  //gStyle->SetPalette(62);

  for ( int & mass_bin : mass_bins){

    TH2F * div_jpsi_phot_pt = (TH2F*) eff_file.Get(Form("div_jpsi_phot_pt_Q%i",mass_bin));
    TH2F * div_qta_qtb      = (TH2F*) eff_file.Get(Form("div_qta_qtb_Q%i",mass_bin));
    TH2F * div_jpsi_qta     = (TH2F*) eff_file.Get(Form("div_jpsi_qta_Q%i",mass_bin));
    TH2F * div_phot_qta     = (TH2F*) eff_file.Get(Form("div_phot_qta_Q%i",mass_bin));
    TH2F * div_dphi_qta     = (TH2F*) eff_file.Get(Form("div_dphi_qta_Q%i",mass_bin));
    TH2F * div_jpsi_qtb     = (TH2F*) eff_file.Get(Form("div_jpsi_qtb_Q%i",mass_bin));
    TH2F * div_phot_qtb     = (TH2F*) eff_file.Get(Form("div_phot_qtb_Q%i",mass_bin));
    TH2F * div_dphi_qtb     = (TH2F*) eff_file.Get(Form("div_dphi_qtb_Q%i",mass_bin));
    TH2F * div_jpsi_dphi    = (TH2F*) eff_file.Get(Form("div_jpsi_dphi_Q%i",mass_bin));
    TH2F * div_phot_dphi    = (TH2F*) eff_file.Get(Form("div_phot_dphi_Q%i",mass_bin));

    //gStyle->SetPadTickZ(1);
  
    TCanvas * eff_canv = new TCanvas(Form("eff_2d_canv_Q%i",mass_bin), "eff_canv",100,100,4000,3000);
    eff_canv->Divide(4,3);

    //eff_canv->SetFrameFillColor(TColor::GetColorPalette(0));
    eff_canv->SetFrameFillColor(1);


    eff_canv->cd(1); 
    div_jpsi_phot_pt->Draw("COLZ");
    div_jpsi_phot_pt->GetXaxis()->SetTitle("DiMuon Pt (GeV)");
    div_jpsi_phot_pt->GetYaxis()->SetTitle("Photon Pt (GeV)");
    div_jpsi_phot_pt->GetZaxis()->SetRangeUser(0.0,1.2);
    div_jpsi_phot_pt->GetYaxis()->SetLabelSize(0.035);
    div_jpsi_phot_pt->GetYaxis()->SetTitleSize(0.035);
    div_jpsi_phot_pt->GetXaxis()->SetLabelSize(0.035);
    div_jpsi_phot_pt->GetXaxis()->SetTitleSize(0.035);
    div_jpsi_phot_pt->GetZaxis()->SetLabelSize(0.035);
    div_jpsi_phot_pt->GetZaxis()->SetTitleSize(0.035);
    TLatex pad1_ltx;
    pad1_ltx.SetTextSize(0.035);
    pad1_ltx.DrawLatexNDC(0.18,0.825,Form("Dimuon P_{T} vs Photon P_{T} Efficiency - Mass Q%i",mass_bin));  

    eff_canv->cd(2); 
    div_qta_qtb->Draw("COLZ");
    div_qta_qtb->GetXaxis()->SetTitle("qtA (GeV)");
    div_qta_qtb->GetYaxis()->SetTitle("qtB (GeV)");
    div_qta_qtb->GetZaxis()->SetRangeUser(0.0,1.2);
    div_qta_qtb->GetYaxis()->SetLabelSize(0.035);
    div_qta_qtb->GetYaxis()->SetTitleSize(0.035);
    div_qta_qtb->GetXaxis()->SetLabelSize(0.035);
    div_qta_qtb->GetXaxis()->SetTitleSize(0.035);
    div_qta_qtb->GetZaxis()->SetLabelSize(0.035);
    div_qta_qtb->GetZaxis()->SetTitleSize(0.035);
    TLatex pad2_ltx;
    pad2_ltx.SetTextSize(0.035);
    pad2_ltx.DrawLatexNDC(0.18,0.825,Form("qtA vs qtB Efficiency - Mass Q%i",mass_bin));  

    eff_canv->cd(3); 
    div_jpsi_qta->Draw("COLZ");
    div_jpsi_qta->GetXaxis()->SetTitle("DiMuon Pt (GeV)");
    div_jpsi_qta->GetYaxis()->SetTitle("qtA (GeV)");
    div_jpsi_qta->GetZaxis()->SetRangeUser(0.0,1.2);
    div_jpsi_qta->GetYaxis()->SetLabelSize(0.035);
    div_jpsi_qta->GetYaxis()->SetTitleSize(0.035);
    div_jpsi_qta->GetXaxis()->SetLabelSize(0.035);
    div_jpsi_qta->GetXaxis()->SetTitleSize(0.035);
    div_jpsi_qta->GetZaxis()->SetLabelSize(0.035);
    div_jpsi_qta->GetZaxis()->SetTitleSize(0.035);
    TLatex pad3_ltx;
    pad3_ltx.SetTextSize(0.035);
    pad3_ltx.DrawLatexNDC(0.18,0.825,Form("DiMuon P_{T} vs qtA Efficiency - Mass Q%i",mass_bin));  


    eff_canv->cd(4); 
    div_phot_qta->Draw("COLZ");
    div_phot_qta->GetXaxis()->SetTitle("Photon Pt (GeV)");
    div_phot_qta->GetYaxis()->SetTitle("qtA (GeV)");
    div_phot_qta->GetZaxis()->SetRangeUser(0.0,1.2);
    div_phot_qta->GetYaxis()->SetLabelSize(0.035);
    div_phot_qta->GetYaxis()->SetTitleSize(0.035);
    div_phot_qta->GetXaxis()->SetLabelSize(0.035);
    div_phot_qta->GetXaxis()->SetTitleSize(0.035);
    div_phot_qta->GetZaxis()->SetLabelSize(0.035);
    div_phot_qta->GetZaxis()->SetTitleSize(0.035);
    TLatex pad4_ltx;
    pad4_ltx.SetTextSize(0.035);
    pad4_ltx.DrawLatexNDC(0.18,0.825,Form("Photon P_{T} vs qtA Efficiency - Mass Q%i",mass_bin));  


    eff_canv->cd(5); 
    div_dphi_qta->Draw("COLZ");
    div_dphi_qta->GetXaxis()->SetTitle("dPhi (rad)");
    div_dphi_qta->GetYaxis()->SetTitle("qtA (GeV)");
    div_dphi_qta->GetZaxis()->SetRangeUser(0.0,1.2);
    div_dphi_qta->GetYaxis()->SetLabelSize(0.035);
    div_dphi_qta->GetYaxis()->SetTitleSize(0.035);
    div_dphi_qta->GetXaxis()->SetLabelSize(0.035);
    div_dphi_qta->GetXaxis()->SetTitleSize(0.035);
    div_dphi_qta->GetZaxis()->SetLabelSize(0.035);
    div_dphi_qta->GetZaxis()->SetTitleSize(0.035);
    TLatex pad5_ltx;
    pad5_ltx.SetTextSize(0.035);
    pad5_ltx.DrawLatexNDC(0.18,0.825,Form("dPhi vs qtA Efficiency - Mass Q%i",mass_bin));  


    eff_canv->cd(6); 
    div_jpsi_qtb->Draw("COLZ");
    div_jpsi_qtb->GetXaxis()->SetTitle("DiMuon Pt (GeV)");
    div_jpsi_qtb->GetYaxis()->SetTitle("qtB (GeV)");
    div_jpsi_qtb->GetZaxis()->SetRangeUser(0.0,1.2);
    div_jpsi_qtb->GetYaxis()->SetLabelSize(0.035);
    div_jpsi_qtb->GetYaxis()->SetTitleSize(0.035);
    div_jpsi_qtb->GetXaxis()->SetLabelSize(0.035);
    div_jpsi_qtb->GetXaxis()->SetTitleSize(0.035);
    div_jpsi_qtb->GetZaxis()->SetLabelSize(0.035);
    div_jpsi_qtb->GetZaxis()->SetTitleSize(0.035);
    TLatex pad6_ltx;
    pad6_ltx.SetTextSize(0.035);
    pad6_ltx.DrawLatexNDC(0.18,0.825,Form("DiMuon P_{T} vs qtB Efficiency - Mass Q%i",mass_bin));  


    eff_canv->cd(7); 
    div_phot_qtb->Draw("COLZ");
    div_phot_qtb->GetXaxis()->SetTitle("Photon Pt (GeV)");
    div_phot_qtb->GetYaxis()->SetTitle("qtB (GeV)");
    div_phot_qtb->GetZaxis()->SetRangeUser(0.0,1.2);
    div_phot_qtb->GetYaxis()->SetLabelSize(0.035);
    div_phot_qtb->GetYaxis()->SetTitleSize(0.035);
    div_phot_qtb->GetXaxis()->SetLabelSize(0.035);
    div_phot_qtb->GetXaxis()->SetTitleSize(0.035);
    div_phot_qtb->GetZaxis()->SetLabelSize(0.035);
    div_phot_qtb->GetZaxis()->SetTitleSize(0.035);
    TLatex pad7_ltx;
    pad7_ltx.SetTextSize(0.035);
    pad7_ltx.DrawLatexNDC(0.18,0.825,Form("Photon P_{T} vs qtB Efficiency - Mass Q%i",mass_bin));  


    eff_canv->cd(8); 
    div_dphi_qtb->Draw("COLZ");
    div_dphi_qtb->GetXaxis()->SetTitle("dPhi (rad)");
    div_dphi_qtb->GetYaxis()->SetTitle("qtB (GeV)");
    div_dphi_qtb->GetZaxis()->SetRangeUser(0.0,1.2);
    div_dphi_qtb->GetYaxis()->SetLabelSize(0.035);
    div_dphi_qtb->GetYaxis()->SetTitleSize(0.035);
    div_dphi_qtb->GetXaxis()->SetLabelSize(0.035);
    div_dphi_qtb->GetXaxis()->SetTitleSize(0.035);
    div_dphi_qtb->GetZaxis()->SetLabelSize(0.035);
    div_dphi_qtb->GetZaxis()->SetTitleSize(0.035);
    TLatex pad8_ltx;
    pad8_ltx.SetTextSize(0.035);
    pad8_ltx.DrawLatexNDC(0.18,0.825,Form("dPhi vs qtB Efficiency - Mass Q%i",mass_bin));  


    eff_canv->cd(9); 
    div_jpsi_dphi->Draw("COLZ");
    div_jpsi_dphi->GetXaxis()->SetTitle("DiMuon Pt (GeV)");
    div_jpsi_dphi->GetYaxis()->SetTitle("dPhi (rad)");
    div_jpsi_dphi->GetZaxis()->SetRangeUser(0.0,1.2);
    div_jpsi_dphi->GetYaxis()->SetLabelSize(0.035);
    div_jpsi_dphi->GetYaxis()->SetTitleSize(0.035);
    div_jpsi_dphi->GetXaxis()->SetLabelSize(0.035);
    div_jpsi_dphi->GetXaxis()->SetTitleSize(0.035);
    div_jpsi_dphi->GetZaxis()->SetLabelSize(0.035);
    div_jpsi_dphi->GetZaxis()->SetTitleSize(0.035);

    TLatex pad9_ltx;
    pad9_ltx.SetTextSize(0.035);
    pad9_ltx.DrawLatexNDC(0.18,0.825,Form("DiMuon P_{T} vs dPhi Efficiency - Mass Q%i",mass_bin));  


    eff_canv->cd(10); 
    div_phot_dphi->Draw("COLZ");
    div_phot_dphi->GetXaxis()->SetTitle("Photon Pt (GeV)");
    div_phot_dphi->GetYaxis()->SetTitle("dPhi (rad)");
    div_phot_dphi->GetZaxis()->SetRangeUser(0.0,1.2);
    div_phot_dphi->GetYaxis()->SetLabelSize(0.035);
    div_phot_dphi->GetYaxis()->SetTitleSize(0.035);
    div_phot_dphi->GetXaxis()->SetLabelSize(0.035);
    div_phot_dphi->GetXaxis()->SetTitleSize(0.035);
    div_phot_dphi->GetZaxis()->SetLabelSize(0.035);
    div_phot_dphi->GetZaxis()->SetTitleSize(0.035);
    TLatex pad10_ltx;
    pad10_ltx.SetTextSize(0.035);
    pad10_ltx.DrawLatexNDC(0.18,0.825,Form("Photon P_{T} vs dPhi Efficiency - Mass Q%i",mass_bin));  


    eff_canv->SaveAs(Form("Efficiency_2D_Q%i.png",mass_bin));
    delete eff_canv;

    
    TCanvas * proj_canv = new TCanvas(Form("eff_proj_canv_Q%i",mass_bin), "eff_canv",100,100,2000,2000);
    proj_canv->Divide(2,2);
    
    TH1F * qta_div  = (TH1F*) eff_file.Get(Form("qtA_Q%i_div",mass_bin)); 
    TH1F * qtb_div  = (TH1F*) eff_file.Get(Form("qtB_Q%i_div",mass_bin));


    TH1F * qtb_eff_proj = (TH1F*) div_qta_qtb->ProjectionY();
    TH1F * qta_eff_proj = (TH1F*) div_qta_qtb->ProjectionX();

    proj_canv->cd(1); 
    div_qta_qtb->Draw("COLZ");
    div_qta_qtb->GetXaxis()->SetTitle("qtA (GeV)");
    div_qta_qtb->GetYaxis()->SetTitle("qtB (GeV)");
    div_qta_qtb->GetZaxis()->SetRangeUser(0.0,1.2);
    div_qta_qtb->GetZaxis()->SetTitle("Efficiency");
    div_qta_qtb->GetYaxis()->SetLabelSize(0.035);
    div_qta_qtb->GetYaxis()->SetTitleSize(0.035);
    div_qta_qtb->GetXaxis()->SetLabelSize(0.035);
    div_qta_qtb->GetXaxis()->SetTitleSize(0.035);
    div_qta_qtb->GetZaxis()->SetLabelSize(0.035);
    div_qta_qtb->GetZaxis()->SetTitleSize(0.035);


    TLatex proj_2d_ltx;
    proj_2d_ltx.SetTextSize(0.035);
    proj_2d_ltx.DrawLatexNDC(0.18,0.825,Form("qtA vs qtB Efficiency - Mass Q%i",mass_bin));  


    proj_canv->cd(2);
    qtb_div->Draw("E1");
    qtb_div->SetMarkerStyle(21);
    qtb_div->SetLineColor(kRed+1);
    qtb_div->GetYaxis()->SetRangeUser(0,1.2);
    qtb_div->GetYaxis()->SetTitle("Efficiency/1.0GeV");
    qtb_div->GetXaxis()->SetTitle("qtB (GeV)");
    qtb_div->GetYaxis()->SetLabelSize(0.035);
    qtb_div->GetYaxis()->SetTitleSize(0.035);
    qtb_div->GetXaxis()->SetLabelSize(0.035);
    qtb_div->GetXaxis()->SetTitleSize(0.035);
    qtb_div->Draw("HIST SAME");
    qtb_eff_proj->Draw("E1 SAME");
    qtb_eff_proj->Draw("HIST SAME");
    gPad->Update();
    TLegend * proj_qtb_legend = new TLegend(0.55,0.65,0.83,0.8);
    proj_qtb_legend->SetBorderSize(0);
    proj_qtb_legend->SetFillColor(0);
    proj_qtb_legend->SetFillStyle(0);
    proj_qtb_legend->SetTextFont(42);
    proj_qtb_legend->SetTextSize(0.025);
    proj_qtb_legend->AddEntry(qtb_div,"1D Efficiency","LP");
    proj_qtb_legend->AddEntry(qtb_eff_proj,"Projected Efficiency","LP");
    proj_qtb_legend->Draw();
    TLatex proj_qtb_pad_ltx;
    proj_qtb_pad_ltx.SetTextSize(0.03);
    proj_qtb_pad_ltx.DrawLatexNDC(0.2,0.825,Form("qtB Efficiency - Mass Q%i",mass_bin));  
    ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
    wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
    sim.DrawLatexNDC(0.2,0.66,"Simulation");

    proj_canv->cd(3);
    qta_div->Draw("E1");
    qta_div->SetMarkerStyle(21);
    qta_div->SetLineColor(kRed+1);
    qta_div->GetYaxis()->SetRangeUser(0,1.2);
    qta_div->GetYaxis()->SetTitle("Efficiency/2.0GeV");
    qta_div->GetXaxis()->SetTitle("qtA (GeV)");
    qta_div->GetYaxis()->SetLabelSize(0.035);
    qta_div->GetYaxis()->SetTitleSize(0.035);
    qta_div->GetXaxis()->SetLabelSize(0.035);
    qta_div->GetXaxis()->SetTitleSize(0.035);
    qta_div->Draw("HIST SAME");
    qta_eff_proj->Draw("E1 SAME");
    qta_eff_proj->Draw("HIST SAME");
    gPad->Update();
    TLegend * proj_qta_legend = new TLegend(0.55,0.65,0.83,0.8);
    proj_qta_legend->SetBorderSize(0);
    proj_qta_legend->SetFillColor(0);
    proj_qta_legend->SetFillStyle(0);
    proj_qta_legend->SetTextFont(42);
    proj_qta_legend->SetTextSize(0.025);
    proj_qta_legend->AddEntry(qta_div,"1D Efficiency","LP");
    proj_qta_legend->AddEntry(qta_eff_proj,"Projected Efficiency","LP");
    proj_qta_legend->Draw();
    TLatex proj_qta_pad_ltx;
    proj_qta_pad_ltx.SetTextSize(0.03);
    proj_qta_pad_ltx.DrawLatexNDC(0.2,0.825,Form("qtA Efficiency - Mass Q%i",mass_bin));  
    ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
    wip.DrawLatexNDC(0.2,0.7,"Work In Progress");
    sim.DrawLatexNDC(0.2,0.66,"Simulation");

    eff_canv->SaveAs(Form("Efficiency_Projection_test_Q%i.png",mass_bin));
    delete eff_canv;






  }

  //std::vector<int> mass_indices = {0,3,4,5,12};
  //char hist_mass_name[100], mass_char[100];
  //for (int & mass_index : mass_indices){
  //}

}
