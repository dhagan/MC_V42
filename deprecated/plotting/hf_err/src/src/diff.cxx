#include <diff.hxx>

void diff( std::string eff_path, std::string hf_path, std::string abin_vars, std::string spec_vars, 
           std::string type, std::string slice, std::string unique, std::string range ){

  
  //std::cout << eff_path << std::endl;
  //std::cout << hf_path << std::endl;
  //std::cout << abin_vars << std::endl;
  //std::cout << spec_vars << std::endl;
  //std::cout << type << std::endl;
  //std::cout << slice << std::endl;
  //std::cout << unique << std::endl;
  //std::cout << range << std::endl;


  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);


  std::vector< std::string > unique_vec, hf_file_vec, eff_file_vec;
  split_strings( unique_vec, unique, ":" );
  split_strings( hf_file_vec, hf_path, ":" );
  split_strings( eff_file_vec, eff_path, ":" );

  TLatex ATLAS; ATLAS.SetNDC(); 
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42); 
  wip.SetTextSize(0.038); wip.SetTextColor(1);

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");


  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{15,-10,20};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{10,0,20};
  analysis_bins["qtB"]           =  std::vector<double>{10,0,20};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DPhi"]          =  std::vector<double>{8,0,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{10,5,30};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5,25};

  if ( !range.empty() ){
    std::vector< std::string > arg_bin;
    std::vector< std::string > set_bins;
    split_strings( arg_bin, range, "_" );
    split_strings( set_bins, arg_bin.at(1), "," );
    std::map< std::string, std::vector<double> >::iterator bin_map_itr = analysis_bins.find( arg_bin.at( 0 ).c_str() );
    bin_map_itr->second = { std::stod( set_bins.at( 0 ) ), std::stod( set_bins.at( 1 ) ), std::stod( set_bins.at( 2 ) ) };
  }
  
  std::cout << 63 << std::endl;

  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["BDT"]           =  "Value";
  units["abs(qtB)"]      =  "GeV";
  units["qtB"]           =  "GeV";
  units["Phi"]           =  "rad";
  units["DPhi"]          =  "rad";
  units["DiMuonPt"]      =  "GeV";
  units["PhotonPt"]      =  "GeV";

  int hf_error_bin = 0;
  std::string full_type;
  if (type.find("data") != std::string::npos){ hf_error_bin = 1; full_type = "data";}  
  if (type.find("sign") != std::string::npos){ hf_error_bin = 2; full_type = "signal";}
  if (type.find("bckg") != std::string::npos){ hf_error_bin = 3; full_type = "background";}

  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";



  TFile * base_eff_file         = new TFile( eff_file_vec.at(0).c_str(),   "READ");
  TFile * base_integral_file    = new TFile( hf_file_vec.at(0).c_str(),    "READ");
  TFile * comp_eff_file         = new TFile( eff_file_vec.at(1).c_str(),   "READ");
  TFile * comp_integral_file    = new TFile( hf_file_vec.at(1).c_str(),   "READ");

  std::cout << hf_file_vec.at(0) << std::endl;
  std::cout << hf_file_vec.at(1) << std::endl;
  std::cout << eff_file_vec.at(0) << std::endl;
  std::cout << eff_file_vec.at(1) << std::endl;

  int mass_bins[5] = {0,3,4,5,12};

  for ( int & mass_index : mass_bins){
    std::cout << mass_index << std::endl;

    char mass_char[100];        
    sprintf( mass_char, "both-theta_mass-%i", mass_index );
    
    for ( std::string abin_var : vec_abin_vars ){


      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)    bins.at(0);
      double abin_min   = (double) bins.at(1);
      double abin_max   = (double) bins.at(2);
      double width =  (double) (abin_max-abin_min)/((double) abin_bins);
      std::string unit = units[abin_var];


      for (std::string spec_var : vec_spec_vars){

        std::cout << 116 << std::endl;
        TH1F * base_ex_reco = new TH1F( Form("base_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * base_eff     = (TH1F*) base_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));
        TH1F * comp_ex_reco = new TH1F( Form("comp_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * comp_eff     = (TH1F*) comp_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));


        bool base_error_available{ true };
        bool comp_error_available{ true };
        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){

          std::cout << 127 << std::endl;
          char bin_hist_name[150], error_name[150];
          sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);
  
          TH1F * base_integral_hist   = (TH1F*) base_integral_file->Get(bin_hist_name);
          TH1F * base_error_hist      = (TH1F*) base_integral_file->Get(error_name);
          TH1F * comp_integral_hist   = (TH1F*) comp_integral_file->Get(bin_hist_name);
          TH1F * comp_error_hist      = (TH1F*) comp_integral_file->Get(error_name);
          std::cout << 137 << std::endl;
          std::cout << base_integral_hist->Integral() << std::endl;
          std::cout << 137 << std::endl;
          
          base_ex_reco->SetBinContent(abin_index, base_integral_hist->Integral());
          if ( base_error_hist != NULL ){ 
            base_ex_reco->SetBinError(abin_index,base_error_hist->GetBinError(hf_error_bin));
          } else { base_error_available = false; }
          std::cout << 143 << std::endl;
          
          comp_ex_reco->SetBinContent(abin_index, comp_integral_hist->Integral());
          if ( comp_error_hist != NULL ){ 
            comp_ex_reco->SetBinError(abin_index,comp_error_hist->GetBinError(hf_error_bin));
          } else { comp_error_available = false; }
          std::cout << 149 << std::endl;
        }
        if ( !base_error_available ){ base_ex_reco->Sumw2(); }
        if ( !comp_error_available ){ comp_ex_reco->Sumw2(); }

        std::cout << 150 << std::endl;

        TH1F * base_ex_corr = new TH1F( Form("base_ex_signal_corr_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * comp_ex_corr = new TH1F( Form("comp_ex_signal_corr_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        base_ex_corr->Divide( base_ex_reco, base_eff );
        comp_ex_corr->Divide( comp_ex_reco, comp_eff );

        TH1F * base_comp_corr_rat = new TH1F( Form("base_comp_corr_rat_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * base_comp_corr_sub = new TH1F( Form("base_comp_corr_sub_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        base_comp_corr_rat->Divide( base_ex_corr, comp_ex_corr );
        base_comp_corr_sub->Add( base_ex_corr, comp_ex_corr, 1.0, -1.0 );


        TH1F * norm_base_ex_corr = (TH1F*) base_ex_corr->Clone( Form("norm_base_ex_signal_corr_Q%i",mass_index ) );
        TH1F * norm_comp_ex_corr = (TH1F*) comp_ex_corr->Clone( Form("norm_comp_ex_signal_corr_Q%i",mass_index ) );
        TH1F * norm_base_comp_corr_rat = new TH1F( Form("norm_base_comp_corr_rat_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * norm_base_comp_corr_sub = new TH1F( Form("norm_base_comp_corr_sub_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        norm_base_ex_corr->Scale(1.0/norm_base_ex_corr->Integral() );
        norm_comp_ex_corr->Scale(1.0/norm_comp_ex_corr->Integral() );
        norm_base_comp_corr_rat->Divide( norm_base_ex_corr, norm_comp_ex_corr );
        norm_base_comp_corr_sub->Add( norm_base_ex_corr, norm_comp_ex_corr, 1.0, -1.0 );

  
        TCanvas * diff_canv = new TCanvas("sys_canv", "", 200,100,3000,2000);
        diff_canv->Divide(3,2);

        diff_canv->cd(1);
        base_ex_corr->Draw("E1");
        comp_ex_corr->Draw("E1 SAME");
        base_ex_corr->Draw("HIST SAME");
        comp_ex_corr->Draw("HIST SAME");
        base_ex_corr->GetYaxis()->SetRangeUser( 0, 
            std::max( {base_ex_corr->GetMaximum(), comp_ex_corr->GetMaximum() })*1.3 );
        base_ex_corr->SetMarkerStyle(21);
        base_ex_corr->SetLineColor(kRed+1);
        comp_ex_corr->SetMarkerStyle(20);
        comp_ex_corr->SetLineColor(kBlue+1);
        base_ex_corr->GetYaxis()->SetTitle( Form("Yield /%.3f %s", width, unit.c_str()));
        base_ex_corr->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        base_ex_corr->GetYaxis()->SetLabelSize(0.035);
        base_ex_corr->GetYaxis()->SetTitleSize(0.035);
        base_ex_corr->GetXaxis()->SetLabelSize(0.035);
        base_ex_corr->GetXaxis()->SetTitleSize(0.035);
        base_ex_corr->GetYaxis()->SetMaxDigits(3);
        TLegend * pad1_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
        pad1_legend->SetBorderSize( 0 );
        pad1_legend->SetFillColor( 0 );
        pad1_legend->SetFillStyle( 0 );
        pad1_legend->SetTextFont( 42 );
        pad1_legend->SetTextSize( 0.02 );
        pad1_legend->AddEntry( base_ex_corr, Form( "%s", unique_vec.at(0).c_str()), "LP");
        pad1_legend->AddEntry( comp_ex_corr, Form( "%s", unique_vec.at(1).c_str()), "LP");
        pad1_legend->Draw();
        TLatex pad1_ltx;
        pad1_ltx.SetTextSize( 0.03 );
        pad1_ltx.DrawLatexNDC( 0.25, 0.825, Form( "%s %s - Mass Q%i", unique_vec.at(0).c_str(), 
              unique_vec.at(1).c_str(), mass_index ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        diff_canv->cd(2);
        base_comp_corr_rat->Draw("E1");
        base_comp_corr_rat->Draw("SAME HIST");
        TLegend * pad2_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
        pad2_legend->SetBorderSize( 0 );
        pad2_legend->SetFillColor( 0 );
        pad2_legend->SetFillStyle( 0 );
        pad2_legend->SetTextFont( 42 );
        pad2_legend->SetTextSize( 0.02 );
        pad2_legend->AddEntry( base_comp_corr_rat, "Ratio", "LPE");
        pad2_legend->Draw();
        TLatex pad2_ltx;
        pad2_ltx.SetTextSize( 0.03 );
        pad2_ltx.DrawLatexNDC( 0.25, 0.825, Form( "%s %s Ratio - Mass Q%i", unique_vec.at(0).c_str(), 
              unique_vec.at(1).c_str(), mass_index ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

        diff_canv->cd(3);
        base_comp_corr_sub->Draw("E1");
        base_comp_corr_sub->Draw("SAME HIST");
        TLegend * pad3_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
        pad3_legend->SetBorderSize( 0 );
        pad3_legend->SetFillColor( 0 );
        pad3_legend->SetFillStyle( 0 );
        pad3_legend->SetTextFont( 42 );
        pad3_legend->SetTextSize( 0.02 );
        pad3_legend->AddEntry( base_comp_corr_sub,  "Difference", "LPE");
        pad3_legend->Draw();
        TLatex pad3_ltx;
        pad3_ltx.SetTextSize( 0.03 );
        pad3_ltx.DrawLatexNDC( 0.25, 0.825, Form( "%s %s Difference - Mass Q%i", unique_vec.at(0).c_str(), 
              unique_vec.at(1).c_str(), mass_index ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        diff_canv->cd(4);
        norm_base_ex_corr->Draw("E1");
        norm_comp_ex_corr->Draw("E1 SAME");
        norm_base_ex_corr->Draw("HIST SAME");
        norm_comp_ex_corr->Draw("HIST SAME");
        norm_base_ex_corr->GetYaxis()->SetRangeUser( 0, 1 );
        norm_base_ex_corr->SetMarkerStyle(21);
        norm_base_ex_corr->SetLineColor(kRed+1);
        norm_comp_ex_corr->SetMarkerStyle(20);
        norm_comp_ex_corr->SetLineColor(kBlue+1);
        norm_base_ex_corr->GetYaxis()->SetTitle( Form("Yield /%.3f %s", width, unit.c_str()));
        norm_base_ex_corr->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        norm_base_ex_corr->GetYaxis()->SetLabelSize(0.035);
        norm_base_ex_corr->GetYaxis()->SetTitleSize(0.035);
        norm_base_ex_corr->GetXaxis()->SetLabelSize(0.035);
        norm_base_ex_corr->GetXaxis()->SetTitleSize(0.035);
        norm_base_ex_corr->GetYaxis()->SetMaxDigits(3);
        TLegend * norm_pad4_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
        norm_pad4_legend->SetBorderSize( 0 );
        norm_pad4_legend->SetFillColor( 0 );
        norm_pad4_legend->SetFillStyle( 0 );
        norm_pad4_legend->SetTextFont( 42 );
        norm_pad4_legend->SetTextSize( 0.02 );
        norm_pad4_legend->AddEntry( norm_base_ex_corr, Form( "%s", unique_vec.at(0).c_str()), "LP");
        norm_pad4_legend->AddEntry( norm_comp_ex_corr, Form( "%s", unique_vec.at(1).c_str()), "LP");
        norm_pad4_legend->Draw();
        TLatex norm_pad4_ltx;
        norm_pad4_ltx.SetTextSize( 0.03 );
        norm_pad4_ltx.DrawLatexNDC( 0.25, 0.825, Form( "Norm %s %s - Mass Q%i", unique_vec.at(0).c_str(), 
              unique_vec.at(1).c_str(), mass_index ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        diff_canv->cd(5);
        norm_base_comp_corr_rat->Draw("E1");
        norm_base_comp_corr_rat->Draw("SAME HIST");
        TLegend * norm_pad5_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
        norm_pad5_legend->SetBorderSize( 0 );
        norm_pad5_legend->SetFillColor( 0 );
        norm_pad5_legend->SetFillStyle( 0 );
        norm_pad5_legend->SetTextFont( 42 );
        norm_pad5_legend->SetTextSize( 0.02 );
        norm_pad5_legend->AddEntry( base_comp_corr_rat, "Ratio", "LPE");
        norm_pad5_legend->Draw();
        TLatex norm_pad5_ltx;
        norm_pad5_ltx.SetTextSize( 0.03 );
        norm_pad5_ltx.DrawLatexNDC( 0.25, 0.825, Form( "Norm %s %s Ratio - Mass Q%i", unique_vec.at(0).c_str(), 
              unique_vec.at(1).c_str(), mass_index ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

        diff_canv->cd(6);
        norm_base_comp_corr_sub->Draw("E1");
        norm_base_comp_corr_sub->Draw("SAME HIST");
        TLegend * norm_pad6_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
        norm_pad6_legend->SetBorderSize( 0 );
        norm_pad6_legend->SetFillColor( 0 );
        norm_pad6_legend->SetFillStyle( 0 );
        norm_pad6_legend->SetTextFont( 42 );
        norm_pad6_legend->SetTextSize( 0.02 );
        norm_pad6_legend->AddEntry( norm_base_comp_corr_sub,  "Difference", "LPE");
        norm_pad6_legend->Draw();
        TLatex norm_pad6_ltx;
        norm_pad6_ltx.SetTextSize( 0.03 );
        norm_pad6_ltx.DrawLatexNDC( 0.25, 0.825, Form( "Norm %s %s Difference - Mass Q%i", unique_vec.at(0).c_str(), 
              unique_vec.at(1).c_str(), mass_index ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );



        diff_canv->SaveAs(Form("./%s_%s_diff_%s_Q%i.png", unique_vec.at(0).c_str(), unique_vec.at(1).c_str(), abin_var.c_str(), mass_index ));
        delete diff_canv;


      }
    }
  }





}
