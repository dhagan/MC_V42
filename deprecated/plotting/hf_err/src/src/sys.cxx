#include <sys.hxx>
#include <split_string.hxx>

#include <iostream>
#include <fstream>

void sys( std::string eff_path, std::string hf_path, std::string sys_eff_path, std::string sys_hf_path, 
    std::string abin_vars, std::string spec_vars, std::string type, std::string slice, std::string unique, 
    std::string range, std::string weights, std::string str_overname ){


  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  int mass_bins[5] = {0,3,4,5,12};

  TF1 * fit_single_gaussian = new TF1( "fit_double_gaussian", "[0]*exp( -((x-[1])^2)/(2*[2]^2))", -5, 15); 
  fit_single_gaussian->SetParLimits(0, 100, 50000);
  fit_single_gaussian->SetParLimits(1,-5,15); 
  fit_single_gaussian->SetParLimits(2,0,10); 

  fit_single_gaussian->SetParameter(0,25000); 
  fit_single_gaussian->SetParameter(1,5.0); 
  fit_single_gaussian->SetParameter(2,4); 

  fit_single_gaussian->SetParName(0,"C"); fit_single_gaussian->SetParName(1,"X"); fit_single_gaussian->SetParName(2,"Sigma");
  fit_single_gaussian->SetLineColor(kRed+1);
  
  TF1 * fit_double_gaussian = new TF1("fit_double_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) +"
                                      "[3]*exp(-((x-[1])^2)/(2*[4]^2)) ", -5.0, 15.0 );

  fit_double_gaussian->SetParName(0,"C1");
  fit_double_gaussian->SetParName(1,"X1");
  fit_double_gaussian->SetParName(2,"sig1");
  fit_double_gaussian->SetParName(3,"C2");
  fit_double_gaussian->SetParName(4,"sig2");
  fit_double_gaussian->SetParameter(2,3);
  fit_double_gaussian->SetParameter(3,100);
  fit_double_gaussian->SetParameter(4,50);
  //fit_double_gaussian->SetParLimits(1,-5,15);
  //fit_double_gaussian->SetParLimits(2,2,6);
  //fit_double_gaussian->SetParLimits(3,0,3000);
  fit_double_gaussian->SetParLimits(4,10,100);

  fit_double_gaussian->SetLineColor(kRed+1);

   
  TLatex ATLAS; ATLAS.SetNDC(); 
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42); 
  wip.SetTextSize(0.038); wip.SetTextColor(1);

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{15,-10,20};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{10,0,20};
  analysis_bins["qtB"]           =  std::vector<double>{10,0,20};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DPhi"]          =  std::vector<double>{8,0,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{10,5,30};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5,25};

  if ( !range.empty() ){
    std::vector< std::string > arg_bin;
    std::vector< std::string > set_bins;
    split_strings( arg_bin, range, "_" );
    split_strings( set_bins, arg_bin.at(1), "," );
    std::map< std::string, std::vector<double> >::iterator bin_map_itr = analysis_bins.find( arg_bin.at( 0 ).c_str() );
    bin_map_itr->second = { std::stod( set_bins.at( 0 ) ), std::stod( set_bins.at( 1 ) ), std::stod( set_bins.at( 2 ) ) };
  }
  
  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["BDT"]           =  "Value";
  units["abs(qtB)"]      =  "GeV";
  units["qtB"]           =  "GeV";
  units["Phi"]           =  "rad";
  units["DPhi"]          =  "rad";
  units["DiMuonPt"]      =  "GeV";
  units["PhotonPt"]      =  "GeV";

  int hf_error_bin = 0;
  std::string full_type;
  if (type.find("data") != std::string::npos){ hf_error_bin = 1; full_type = "data";}  
  if (type.find("sign") != std::string::npos){ hf_error_bin = 2; full_type = "signal";}
  if (type.find("bckg") != std::string::npos){ hf_error_bin = 3; full_type = "background";}

  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";

  TFile * base_eff_file         = new TFile(eff_path.c_str(),   "READ");
  TFile * base_integral_file    = new TFile(hf_path.c_str(),    "READ");

  
  std::vector< std::string > sys_eff_path_vec, sys_hf_path_vec, u_tmp, 
                             unique_sys_vec, unique_rw_vec, sys_rw_path_vec;
  split_strings( sys_eff_path_vec, sys_eff_path, ":" );
  split_strings( sys_hf_path_vec, sys_hf_path, ":" );
  split_strings( sys_rw_path_vec, weights, ":" );
  split_strings( u_tmp, unique, "_" );
  split_strings( unique_sys_vec, u_tmp.at(0), ":" );
  split_strings( unique_rw_vec, u_tmp.at(1), ":" );

  std::ofstream table_file;
  table_file.open( Form( "%s_tables.tex", abin_vars.c_str() ));
  table_file << "\\documentclass[a4paper, portrait]{article}\n";
  table_file << "\\usepackage[a4paper, margin=10pt, portrait]{geometry}\n";
  table_file << "\\begin{document}\n";


  
  for ( int & mass_index : mass_bins){

    char mass_char[100];        
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
    
    for (std::string abin_var : vec_abin_vars){


      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      if (abin_var.find("_low") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      if (abin_var.find("_high") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)    bins.at(0);
      double abin_min   = (double) bins.at(1);
      double abin_max   = (double) bins.at(2);
      double width =  (double) (abin_max-abin_min)/((double) abin_bins);
      std::string unit = units[abin_var];

      TF1 * unit_line = new TF1("unit_line","1", abin_min, abin_max);
      TH1F * unit_hist = new TH1F( "unit_hist", "", abin_bins, abin_min, abin_max );
      for ( int unit_idx = 1; unit_idx <= abin_bins; unit_idx++ ){ unit_hist->SetBinContent( unit_idx, 1.0 ); }
      
      std::string line_str{ "|c|" };
      for ( int bin_idx = 1; bin_idx <= abin_bins; bin_idx++ ){ line_str += "r|"; }
      table_file << "\\begin{center}\n";
      table_file << Form("\\begin{tabular}{%s}\n\\hline\n", line_str.c_str() );
      line_str.clear();
      for ( int bin_idx = 1; bin_idx <= abin_bins; bin_idx++ ){ line_str += Form("&%i", bin_idx ); }
      table_file << Form("Q%i-%s %s\\\\\n", mass_index, abin_var.c_str(), line_str.c_str() );
      table_file << "\\hline\n\\hline\n";
      line_str.clear();



      for (std::string spec_var : vec_spec_vars){

        TH1F * base_ex_reco = new TH1F( Form("base_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * base_eff     = (TH1F*) base_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));

        bool base_error_available{true};
        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
           
          char bin_hist_name[150], error_name[150];
          sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);           

          TH1F * base_integral_hist   = (TH1F*) base_integral_file->Get(bin_hist_name);
          TH1F * base_error_hist      = (TH1F*) base_integral_file->Get(error_name);

          base_ex_reco->SetBinContent(abin_index, base_integral_hist->Integral());
          if ( base_error_hist != NULL ){ 
            base_ex_reco->SetBinError(abin_index,base_error_hist->GetBinError(hf_error_bin));
          } else { base_error_available = false; }
        }
        if ( !base_error_available ){ base_ex_reco->Sumw2(); }

        for ( int bin_idx = 1; bin_idx <= abin_bins; bin_idx++ ){ 
          line_str += Form( "&%i", (int)
              std::round(base_ex_reco->GetBinError(bin_idx)/((double)base_ex_reco->GetBinContent(bin_idx))*100.0)); 
        }
        table_file << Form("Stat %s\\\\\n", line_str.c_str() );
        table_file << "\\hline\n\\hline\n";
        line_str.clear();

      
        std::map< std::string, TH1F* > sys_hist_map;
        bool base = false;
        TH1F * base_hist = new TH1F( "null_hist", "", abin_bins, abin_min, abin_max );

        for ( int sys_idx = 0; sys_idx < (int) sys_eff_path_vec.size(); sys_idx++ ){

          std::string current_sys_eff_path = sys_eff_path_vec.at( sys_idx );
          std::string current_sys_hf_path =  sys_hf_path_vec.at( sys_idx );
          std::string current_unique = unique_sys_vec.at( sys_idx );
          line_str += current_unique;

          TFile * sys_eff_file          = new TFile( current_sys_eff_path.c_str(),    "READ");
          TFile * sys_integral_file     = new TFile( current_sys_hf_path.c_str(),    "READ");
      
          TH1F * sys_ex_reco  = new TH1F( Form("sys_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
          TH1F * sys_eff      = (TH1F*) sys_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));

          bool  sys_error_available{true};
          for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
             
            char bin_hist_name[150], error_name[150];
            sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
            sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);

            TH1F * sys_integral_hist    = (TH1F*) sys_integral_file->Get(bin_hist_name);
            TH1F * sys_error_hist       = (TH1F*) sys_integral_file->Get(error_name);

            sys_ex_reco->SetBinContent(abin_index, sys_integral_hist->Integral());
            if ( sys_error_hist != NULL ){ 
              sys_ex_reco->SetBinError(abin_index,sys_error_hist->GetBinError(hf_error_bin));
            } else { sys_error_available = false; }
          }
          if ( !sys_error_available ){ sys_ex_reco->Sumw2(); }

          TH1F * base_ex_corr = new TH1F( Form("base_ex_corr_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
          TH1F * sys_ex_corr = new TH1F( Form("sys_ex_corr_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
                  
          base_ex_corr->Divide( base_ex_reco, base_eff);
          sys_ex_corr->Divide( sys_ex_reco, sys_eff );

          sys_hist_map[ current_unique.c_str() ] = (TH1F*) sys_ex_corr->Clone();
          if ( !base ){
            base_hist = (TH1F*) base_ex_corr->Clone( "base_hist" );
            base = true;
          }
          
          TH1F * err_hist = new TH1F( Form("sys_err_%s_Q%i", abin_var.c_str(), mass_index), "", abin_bins, abin_min, abin_max );
          TH1F * abs_err_hist = new TH1F( Form("abs_sys_err_%s_Q%i", abin_var.c_str(), mass_index), "", abin_bins, abin_min, abin_max );
          err_hist->Divide( sys_ex_corr, base_ex_corr, 1.0, 1.0 );
          TH1F * mod_err_hist = (TH1F*) err_hist->Clone();
          TH1F * err_hist_split = (TH1F*) err_hist->Clone();
          TH1F * base_ex_corr_split = (TH1F*) base_ex_corr->Clone();
          TH1F * sys_ex_corr_split = (TH1F*)  sys_ex_corr->Clone();
          err_hist->Add( unit_hist, -1.0 );
          abs_err_hist->Add( sys_ex_corr, base_ex_corr, 1.0, -1.0 );


          if ( !(abin_var.find("qtA") == std::string::npos) ){  
            
            double sys_x[15];
            double sys_y[15];
            double err_x_upper[15];
            double err_x_lower[15];
            double err_y_upper[15];
            double err_y_lower[15];

            double abs_err_x_upper[15];
            double abs_err_x_lower[15];
            double abs_err_y_upper[15];
            double abs_err_y_lower[15];

            for ( int err_bin = 0; err_bin < abin_bins; err_bin++){ 
              sys_x[err_bin] = (abin_min+(width/2.0)) + width*((double) err_bin);
              sys_y[err_bin] = 1.0;
              err_x_upper[err_bin] = 0.0;
              err_x_lower[err_bin] = 0.0;
              abs_err_x_upper[err_bin] = 0.0;
              abs_err_x_lower[err_bin] = 0.0;
              double sys_err = -1.0 + mod_err_hist->GetBinContent(err_bin+1);
              double abs_sys_err = -1.0 + abs_err_hist->GetBinContent(err_bin+1);
              if ( sys_err > 0 ){
                err_y_lower[err_bin] = 0.0;
                err_y_upper[err_bin] = abs(sys_err);
                abs_err_y_lower[err_bin] = 0.0;
                abs_err_y_upper[err_bin] = abs(abs_sys_err);
              } else {
                err_y_lower[err_bin] = abs(sys_err);
                err_y_upper[err_bin] = 0.0;
                abs_err_y_lower[err_bin] = abs(abs_sys_err);
                abs_err_y_upper[err_bin] = 0.0;
              }
            }
            TGraphAsymmErrors * err_graph = new TGraphAsymmErrors(15,sys_x,sys_y,err_x_lower,err_x_upper,err_y_lower,
                err_y_upper);
            err_graph->SetFillStyle(3004);
            err_graph->SetFillColorAlpha(3,1.0);
            err_graph->SetLineWidth(0);

            TGraphAsymmErrors * abs_err_graph = new TGraphAsymmErrors( 15, sys_x, sys_y, abs_err_x_lower, abs_err_x_upper,
                                                                      abs_err_y_lower, abs_err_y_upper);
            abs_err_graph->SetFillStyle(3004);
            abs_err_graph->SetFillColorAlpha(3,1.0);
            abs_err_graph->SetLineWidth(0);



            TCanvas * sys_canv = new TCanvas("sys_canv", "", 200,100,1000,1000);
            sys_canv->Divide(1,1);

            double axis_lim = std::ceil( mod_err_hist->GetMaximum()*1.3 );
            mod_err_hist->Draw("hist p");
            mod_err_hist->SetMarkerStyle(21);
            mod_err_hist->SetLineColor(1);
            mod_err_hist->GetYaxis()->SetRangeUser(0,axis_lim);
            mod_err_hist->GetYaxis()->SetTitle( Form("systematic/nominal /%.3f %s", width, unit.c_str()));
            mod_err_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
            mod_err_hist->GetYaxis()->SetLabelSize(0.035);
            mod_err_hist->GetYaxis()->SetTitleSize(0.035);
            mod_err_hist->GetXaxis()->SetLabelSize(0.035);
            mod_err_hist->GetXaxis()->SetTitleSize(0.035);
            gPad->Update();
            gPad->SetTicks(1,0);
            //double pad_scale = weight_hist->GetMaximum();
            double max_ratio = ((double)std::ceil(mod_err_hist->GetMaximum()))/((double)std::ceil(base_ex_corr->GetMaximum()));
            double eff_max =  std::ceil(base_ex_corr->GetMaximum() );
            base_ex_corr->Scale( max_ratio );
            sys_ex_corr->Scale( max_ratio );    
            base_ex_corr->SetLineWidth(0);
            base_ex_corr->SetFillColorAlpha(kRed+1,0.5);
            sys_ex_corr->SetLineWidth(0);
            sys_ex_corr->SetFillColorAlpha(kBlue+1,0.5);
            base_ex_corr->Draw("HIST SAME");
            sys_ex_corr->Draw("HIST SAME");
            unit_line->SetLineColor(kMagenta+1);
            unit_line->SetLineStyle(2);
            unit_line->SetLineWidth(3);
            err_graph->Draw("3 SAME");
            unit_line->Draw("SAME");
            TGaxis * pad_axis = new TGaxis(abin_max,0,abin_max,axis_lim,0, eff_max ,510,"+L");
            pad_axis->SetLabelSize( 0.035 );             
            pad_axis->SetLabelFont( 42 );                
            pad_axis->SetTitleSize( 0.035 );
            pad_axis->SetTitleOffset( 1.25 );
            pad_axis->SetMaxDigits( 4 );
            pad_axis->SetTitleFont( 42 );                
            pad_axis->SetTitle( Form( "Signal/%.3f %s", width, unit.c_str() ));
            pad_axis->Draw();
            TLegend * pad_legend = new TLegend( 0.6, 0.6, 0.88, 0.78 );
            pad_legend->SetBorderSize( 0 );
            pad_legend->SetFillColor( 0 );
            pad_legend->SetFillStyle( 0 );
            pad_legend->SetTextFont( 42 );
            pad_legend->SetTextSize( 0.02 );
            pad_legend->AddEntry( mod_err_hist, "Systematic/Nominal", "P");
            pad_legend->AddEntry( err_graph, "Systematic Error", "F");
            pad_legend->AddEntry( base_ex_corr, "Nominal", "F");
            pad_legend->AddEntry( sys_ex_corr, "Variation", "F");
            pad_legend->AddEntry( unit_line, "Nominal", "L");
            pad_legend->Draw();
            TLatex pad_ltx;
            pad_ltx.SetTextSize( 0.03 );
            pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s - Mass Q%i", current_unique.c_str(), mass_index ));  
            ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
            wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

            sys_canv->SaveAs(Form("./%s_%s_sys_Q%i.png", current_unique.c_str(), abin_var.c_str(), mass_index ));
            delete sys_canv;

            TCanvas * sys_canv_split = new TCanvas("sys_canv2", "", 200,100,2000,1000);
            sys_canv_split->Divide(2,1);
            sys_canv_split->cd(1);

            base_ex_corr_split->Draw("HIST");
            base_ex_corr_split->GetYaxis()->SetRangeUser( 0, base_ex_corr_split->GetMaximum()*1.5 );
            base_ex_corr_split->SetLineWidth(0);
            base_ex_corr_split->SetFillColorAlpha(kRed+1,0.6);
            base_ex_corr_split->GetYaxis()->SetTitle( Form("Extracted Signal/%.3f %s", width, unit.c_str()));
            base_ex_corr_split->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
            base_ex_corr_split->GetYaxis()->SetLabelSize(0.035);
            base_ex_corr_split->GetYaxis()->SetTitleSize(0.035);
            base_ex_corr_split->GetXaxis()->SetLabelSize(0.035);
            base_ex_corr_split->GetXaxis()->SetTitleSize(0.035);
            sys_ex_corr_split->Draw("HIST SAME");
            sys_ex_corr_split->SetLineColor(1);
            sys_ex_corr_split->SetFillStyle(0);
            sys_ex_corr_split->SetLineWidth(3);
            TLegend * pad1s_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
            pad1s_legend->SetBorderSize( 0 );
            pad1s_legend->SetFillColor( 0 );
            pad1s_legend->SetFillStyle( 0 );
            pad1s_legend->SetTextFont( 42 );
            pad1s_legend->SetTextSize( 0.02 );
            //pad1s_legend->AddEntry( err_graph, "Systematic Error", "F");
            pad1s_legend->AddEntry( base_ex_corr_split, "Nominal Distribution", "LF");
            pad1s_legend->AddEntry( sys_ex_corr_split, "Systematic Distribution", "L");
            //pad1s_legend->AddEntry( unit_line, "Nominal", "L");
            pad1s_legend->Draw();
            TLatex pad1s_ltx;
            pad1s_ltx.SetTextSize( 0.03 );
            pad1s_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Distributions Q%i - %s", abin_var.c_str(), mass_index, current_unique.c_str() ));
            ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
            wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

            sys_canv_split->cd(2);
            err_hist_split->Draw("HIST P");
            err_hist_split->SetMarkerStyle(21);
            err_hist_split->SetLineColor(1);
            err_hist_split->GetYaxis()->SetRangeUser(0,std::ceil( err_hist_split->GetMaximum() ));
            err_hist_split->GetYaxis()->SetTitle( Form("Ratio/%.3f %s", width, unit.c_str()));
            err_hist_split->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
            err_hist_split->GetYaxis()->SetLabelSize(0.035);
            err_hist_split->GetYaxis()->SetTitleSize(0.035);
            err_hist_split->GetXaxis()->SetLabelSize(0.035);
            err_hist_split->GetXaxis()->SetTitleSize(0.035);
            err_graph->SetFillColorAlpha(kBlue+1,0.6);
            err_graph->SetFillStyle(1);
            err_graph->Draw("3 SAME");
            TLegend * pad2s_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
            pad2s_legend->SetBorderSize( 0 );
            pad2s_legend->SetFillColor( 0 );
            pad2s_legend->SetFillStyle( 0 );
            pad2s_legend->SetTextFont( 42 );
            pad2s_legend->SetTextSize( 0.02 );
            pad2s_legend->AddEntry( err_hist_split, "Distribution Ratio", "P");
            pad2s_legend->AddEntry( err_graph, "Rel Sys Error", "F");
            pad2s_legend->Draw();
            TLatex pad2s_ltx;
            pad2s_ltx.SetTextSize( 0.03 );
            pad2s_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Relative Systematic Error Q%i - %s", abin_var.c_str(), mass_index, current_unique.c_str() ));
            ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
            wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
            sys_canv_split->SaveAs(Form("./%s_%s_sys_split_Q%i.png", current_unique.c_str(), abin_var.c_str(), mass_index ));

            delete sys_canv_split;

            TCanvas * abs_sys_canv_split = new TCanvas("sys_canv3", "", 200,100,2000,1000);
            abs_sys_canv_split->Divide(2,1);
            abs_sys_canv_split->cd(1);

            base_ex_corr_split->Draw("HIST");
            sys_ex_corr_split->Draw("HIST SAME");
            TLegend * pad1a_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
            pad1a_legend->SetBorderSize( 0 );
            pad1a_legend->SetFillColor( 0 );
            pad1a_legend->SetFillStyle( 0 );
            pad1a_legend->SetTextFont( 42 );
            pad1a_legend->SetTextSize( 0.02 );
            pad1a_legend->AddEntry( base_ex_corr_split, "Nominal Distribution", "LF");
            pad1a_legend->AddEntry( sys_ex_corr_split, "Systematic Distribution", "L");
            pad1a_legend->Draw();
            TLatex pad1a_ltx;
            pad1a_ltx.SetTextSize( 0.03 );
            pad1a_ltx.DrawLatexNDC( 0.25, 0.825, Form( "%s Distributions Q%i - %s", abin_var.c_str(), mass_index, current_unique.c_str() ));
            ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
            wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

            abs_sys_canv_split->cd(2);
            abs_err_hist->Draw("HIST P");
            abs_err_hist->SetMarkerStyle(21);
            abs_err_hist->SetLineColor(1);
            double lim = std::max( { abs(abs_err_hist->GetMinimum()), abs(abs_err_hist->GetMaximum()) } ) * 3;
            abs_err_hist->GetYaxis()->SetRangeUser( -lim, lim );
            abs_err_hist->GetYaxis()->SetTitle( Form("Ratio/%.3f %s", width, unit.c_str()));
            abs_err_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
            abs_err_hist->GetYaxis()->SetLabelSize(0.035);
            abs_err_hist->GetYaxis()->SetTitleSize(0.035);
            abs_err_hist->GetXaxis()->SetLabelSize(0.035);
            abs_err_hist->GetXaxis()->SetTitleSize(0.035);
            abs_err_hist->GetYaxis()->SetMaxDigits(3);
            abs_err_graph->SetFillColorAlpha(kBlue+1,0.6);
            abs_err_graph->SetFillStyle(1);
            abs_err_graph->Draw("3 SAME");
            TLegend * pad2a_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
            pad2a_legend->SetBorderSize( 0 );
            pad2a_legend->SetFillColor( 0 );
            pad2a_legend->SetFillStyle( 0 );
            pad2a_legend->SetTextFont( 42 );
            pad2a_legend->SetTextSize( 0.02 );
            pad2a_legend->AddEntry( abs_err_hist, "Distribution Difference", "P");
            pad2a_legend->AddEntry( abs_err_graph, "Abs Sys Error", "F");
            pad2a_legend->Draw();
            TLatex pad2a_ltx;
            pad2a_ltx.SetTextSize( 0.03 );
            pad2a_ltx.DrawLatexNDC( 0.25, 0.825, Form( "%s Absolute Systematic Error Q%i - %s", abin_var.c_str(), mass_index, current_unique.c_str() ));
            ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
            wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
            abs_sys_canv_split->SaveAs(Form("./%s_%s_abs_sys_split_Q%i.png", current_unique.c_str(), abin_var.c_str(), mass_index ));
            delete abs_sys_canv_split;

          };

          for ( int bin_idx = 1; bin_idx <= abin_bins; bin_idx++ ){ 
            line_str += Form( "&%i", (int) std::round(err_hist->GetBinContent(bin_idx)*100.0) ); 
          }
          line_str += "\\\\ \n\\hline\n";
          // ADD EXTRA HLINE CONDITIONALS HERE
          if ( current_unique.find("bdt14") != std::string::npos ){  line_str += "\\hline\n"; }
          if ( current_unique.find("subt2") != std::string::npos ){  line_str += "\\hline\n"; }
        }
        line_str += "\\hline\n";

        for ( int sys_idx = 0; sys_idx < (int) unique_rw_vec.size(); sys_idx++ ){

          std::string current_weight_str = sys_rw_path_vec.at( sys_idx );
          TFile * weighted_eff_file = new TFile( current_weight_str.c_str(), "READ" ); 
          TH1F * weighted_eff = (TH1F*) weighted_eff_file->Get(Form( "eff_%s_Q%i",abin_var.c_str(), mass_index ));
          TH1F * base_eff     = (TH1F*) base_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));
          TH1F * err_hist = new TH1F( Form("w_err_%s_Q%i", abin_var.c_str(), mass_index ), "", 
              abin_bins, abin_min, abin_max );
          std::string current_unique = unique_rw_vec.at( sys_idx );
          line_str += current_unique;
          err_hist->Divide( weighted_eff, base_eff, 1.0, 1.0 );
          err_hist->Add( unit_hist, -1.0 );
          for ( int bin_idx = 1; bin_idx <= abin_bins; bin_idx++ ){ 
            line_str += Form( "&%i", (int) std::round(err_hist->GetBinContent( bin_idx )*100.0) ); 
          }
          line_str += "\\\\ \n\\hline\n";
        } 
        table_file << line_str;
        table_file << "\\end{tabular}\n";
        table_file << "\\end{center}\n";


        TH1F * total_hist = new TH1F( Form( "total_hist_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
        std::vector< double > err_max, err_min, combined_sys_err;
        for ( int bin = 1; bin <= abin_bins; bin++ ){ 
          total_hist->SetBinContent( bin, base_hist->GetBinContent( bin ) );
          err_max.push_back( 0 ); err_min.push_back( DBL_MAX );
          combined_sys_err.push_back( 0 );
        }
        std::vector< int > marker_vec, color_vec;
        marker_vec = { 2, 3, 4, 5, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34 };
        color_vec = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 30, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 };
        TCanvas * full_sys_canv = new TCanvas( Form( "full_sys_canv_Q%i", mass_index ), "", 100,100, 1500,
                                                       1500);
        full_sys_canv->Divide(1);
        full_sys_canv->cd(1);

        base_hist->Draw("E1");
        base_hist->Draw("HIST SAME");

        base_hist->GetYaxis()->SetRangeUser( 0, base_hist->GetMaximum()*1.5 );
        base_hist->SetLineWidth(3);
        base_hist->SetLineColorAlpha( 1,0.6 );
        base_hist->GetYaxis()->SetTitle( Form("Extracted Signal/%.3f %s", width, unit.c_str()));
        base_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        base_hist->GetYaxis()->SetLabelSize(0.03);
        base_hist->GetYaxis()->SetTitleSize(0.03);
        base_hist->GetXaxis()->SetLabelSize(0.03);
        base_hist->GetXaxis()->SetTitleSize(0.03);
        base_hist->GetYaxis()->SetMaxDigits(3);

        TLegend * total_legend = new TLegend( 0.65, 0.5, 0.83, 0.78 );
        total_legend->SetBorderSize( 0 );
        total_legend->SetFillColor( 0 );
        total_legend->SetFillStyle( 0 );
        total_legend->SetTextFont( 42 );
        total_legend->SetTextSize( 0.02 );
        total_legend->AddEntry( base_hist, "Baseline", "LP");
        int sys_idx = 0;
        std::map< std::string, TH1F* >::iterator sys_itr;
        for ( sys_itr = sys_hist_map.begin(); sys_itr != sys_hist_map.end(); sys_itr++ ){
          std::string name = sys_itr->first;
          TH1F * sys = sys_itr->second;
          sys->SetMarkerStyle( marker_vec.at( sys_idx ) );
          sys->SetMarkerColorAlpha( color_vec.at( sys_idx ), 1.0 );
          sys->SetLineWidth( 0 );
          total_legend->AddEntry( sys, name.c_str() );
          sys->Draw( "P SAME" );
          sys_idx++;
          for ( int bin = 1; bin <= abin_bins; bin++ ){ 
            if ( sys->GetBinContent(bin) < err_min[bin-1] ){
              err_min[ bin-1 ]  = sys->GetBinContent( bin );
            }
            if ( sys->GetBinContent(bin) > err_max[bin-1] ){
              err_max[ bin-1 ]  = sys->GetBinContent( bin );
            }
          }
        }
        total_legend->Draw();
        TLatex total_ltx;
        total_ltx.SetTextSize( 0.03 );
        total_ltx.DrawLatexNDC( 0.225, 0.82, Form( "Systematics %s Q%i", abin_var.c_str(), mass_index ));
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );
        full_sys_canv->SaveAs( Form( "./complete/full_sys_Q%i.png", mass_index ) );
        
        for ( int bin = 1; bin <= abin_bins; bin++ ){ 
          combined_sys_err[ bin-1 ] = ( err_max[bin-1] - err_min[bin-1] ) / sqrt( 3.0f );
          //std::cout << combined_sys_err[ bin -1 ] << std::endl;
          total_hist->SetBinError( bin, combined_sys_err[ bin-1 ] );
        }

        TCanvas * final_canv = new TCanvas( Form( "final_Q%i", mass_index ), "", 100, 100, 3000, 3000 );
        TH1F * dg_combined_error_hist = new TH1F( Form( "dg_combined_err_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
        TH1F * sg_combined_error_hist = new TH1F( Form( "sg_combined_err_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
        for ( int bin = 1; bin <= abin_bins; bin++ ){ 
          double base_val = base_hist->GetBinContent( bin );
          double stat_err = base_hist->GetBinError( bin );
          double sys_err = total_hist->GetBinError( bin );
          double combined_sys_stat_err = base_val * sqrt( std::pow((sys_err)/(base_val), 2) + std::pow( (stat_err)/(base_val), 2) );
          dg_combined_error_hist->SetBinContent( bin, base_hist->GetBinContent( bin ) );
          dg_combined_error_hist->SetBinError( bin, combined_sys_stat_err );
          sg_combined_error_hist->SetBinContent( bin, base_hist->GetBinContent( bin ) );
          sg_combined_error_hist->SetBinError( bin, combined_sys_stat_err );
        }

        final_canv->Divide( 2, 2 );

        final_canv->cd( 1 );
        base_hist->Draw("E1");
        base_hist->Draw("HIST SAME");

        base_hist->GetYaxis()->SetRangeUser( 0, base_hist->GetMaximum()*1.5 );
        base_hist->SetLineWidth(3);
        base_hist->SetLineColorAlpha( 1,0.6 );
        base_hist->GetYaxis()->SetTitle( Form("Extracted Signal/%.3f %s", width, unit.c_str()));
        base_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        base_hist->GetYaxis()->SetLabelSize(0.03);
        base_hist->GetYaxis()->SetTitleSize(0.03);
        base_hist->GetXaxis()->SetLabelSize(0.03);
        base_hist->GetXaxis()->SetTitleSize(0.03);
        base_hist->GetYaxis()->SetMaxDigits(3);

        TLegend * all_sys_legend = new TLegend( 0.65, 0.4, 0.83, 0.78 );
        all_sys_legend->SetBorderSize( 0 );
        all_sys_legend->SetFillColor( 0 );
        all_sys_legend->SetFillStyle( 0 );
        all_sys_legend->SetTextFont( 42 );
        all_sys_legend->SetTextSize( 0.02 );
        all_sys_legend->AddEntry( base_hist, "Baseline", "LP");
        sys_idx = 0;
        std::map< std::string, TH1F* >::iterator full_sys_itr;
        for ( full_sys_itr = sys_hist_map.begin(); full_sys_itr != sys_hist_map.end(); full_sys_itr++ ){
          std::string name = full_sys_itr->first;
          TH1F * sys = full_sys_itr->second;
          sys->SetMarkerStyle( marker_vec.at( sys_idx ) );
          sys->SetMarkerColorAlpha( color_vec.at( sys_idx ), 1.0 );
          sys->SetLineWidth( 0 );
          total_legend->AddEntry( sys, name.c_str() );
          sys->Draw( "P SAME" );
          sys_idx++;
        }
        total_legend->Draw();
        TLatex full_sys_ltx;
        full_sys_ltx.SetTextSize( 0.03 );
        full_sys_ltx.DrawLatexNDC( 0.225, 0.82, Form( "Systematics %s Q%i", abin_var.c_str(), mass_index ));
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

        final_canv->cd( 2 );

        base_hist->Draw("E1");
        base_hist->Draw("HIST SAME");


        base_hist->SetLineWidth(3);
        base_hist->SetLineColorAlpha( 1,0.6 );
        base_hist->GetYaxis()->SetTitle( Form("Extracted Signal/%.3f %s", width, unit.c_str()));
        base_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        base_hist->GetYaxis()->SetLabelSize(0.03);
        base_hist->GetYaxis()->SetTitleSize(0.03);
        base_hist->GetXaxis()->SetLabelSize(0.03);
        base_hist->GetXaxis()->SetTitleSize(0.03);
        base_hist->GetYaxis()->SetMaxDigits(3);
        total_hist->Draw("E1 SAME");
        total_hist->SetLineColorAlpha( 1, 1 );
        total_hist->SetLineStyle( 2 );
        base_hist->GetYaxis()->SetRangeUser( 0, base_hist->GetMaximum()*1.5 );
        TLegend * final_legend = new TLegend( 0.65, 0.5, 0.83, 0.78 );
        final_legend->SetBorderSize( 0 );
        final_legend->SetFillColor( 0 );
        final_legend->SetFillStyle( 0 );
        final_legend->SetTextFont( 42 );
        final_legend->SetTextSize( 0.02 );
        final_legend->AddEntry( base_hist, "Stat", "LP");
        final_legend->AddEntry( total_hist, "Sys", "L");
        final_legend->Draw();
        TLatex final_ltx;
        final_ltx.SetTextSize( 0.03 );
        final_ltx.DrawLatexNDC( 0.225, 0.82, Form( "%s Q%i", abin_var.c_str(), mass_index ));
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        final_canv->cd( 3 );
        dg_combined_error_hist->Draw( "E1" );
        dg_combined_error_hist->SetLineWidth(3);
        dg_combined_error_hist->SetLineColorAlpha( 1,0.6 );
        dg_combined_error_hist->GetYaxis()->SetTitle( Form("Extracted Signal/%.3f %s", width, unit.c_str()));
        dg_combined_error_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        dg_combined_error_hist->GetYaxis()->SetLabelSize(0.03);
        dg_combined_error_hist->GetYaxis()->SetTitleSize(0.03);
        dg_combined_error_hist->GetXaxis()->SetLabelSize(0.03);
        dg_combined_error_hist->GetXaxis()->SetTitleSize(0.03);
        dg_combined_error_hist->GetYaxis()->SetMaxDigits(3);
        dg_combined_error_hist->GetYaxis()->SetRangeUser( 0, dg_combined_error_hist->GetMaximum()*2 );
        dg_combined_error_hist->Fit( fit_double_gaussian, "MQ", "", abin_min, abin_max );
        gPad->Modified(); gPad->Update();
        TPaveStats * dg_stats = (TPaveStats*) dg_combined_error_hist->FindObject("stats");
        dg_stats->SetX1NDC(0.55); dg_stats->SetX2NDC(0.825);
        dg_stats->SetY1NDC(0.55); dg_stats->SetY2NDC(0.78);
        dg_stats->SetTextFont(42);
        dg_stats->SetFillStyle(0);
        TLatex dg_ltx;
        dg_ltx.SetTextSize( 0.03 );
        dg_ltx.DrawLatexNDC( 0.225, 0.82, Form( "Double Gaussian Fit Q%i", mass_index ));
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

        final_canv->cd( 4 );
        sg_combined_error_hist->Draw( "E1" );
        sg_combined_error_hist->SetLineWidth(3);
        sg_combined_error_hist->SetLineColorAlpha( 1,0.6 );
        sg_combined_error_hist->GetYaxis()->SetTitle( Form("Extracted Signal/%.3f %s", width, unit.c_str()));
        sg_combined_error_hist->GetXaxis()->SetTitle( Form("%s (%s)", abin_var.c_str(), unit.c_str()));
        sg_combined_error_hist->GetYaxis()->SetLabelSize(0.03);
        sg_combined_error_hist->GetYaxis()->SetTitleSize(0.03);
        sg_combined_error_hist->GetXaxis()->SetLabelSize(0.03);
        sg_combined_error_hist->GetXaxis()->SetTitleSize(0.03);
        sg_combined_error_hist->GetYaxis()->SetMaxDigits(3);
        sg_combined_error_hist->GetYaxis()->SetRangeUser( 0, sg_combined_error_hist->GetMaximum()*2 );
        sg_combined_error_hist->Fit( fit_single_gaussian, "MQ", "", abin_min, abin_max );
        gPad->Modified(); gPad->Update();
        TPaveStats * sg_stats = (TPaveStats*) sg_combined_error_hist->FindObject("stats");
        sg_stats->SetX1NDC(0.55); sg_stats->SetX2NDC(0.825);
        sg_stats->SetY1NDC(0.55); sg_stats->SetY2NDC(0.78);
        sg_stats->SetTextFont(42);
        sg_stats->SetFillStyle(0);
        TLatex sg_ltx;
        sg_ltx.SetTextSize( 0.03 );
        sg_ltx.DrawLatexNDC( 0.225, 0.82, Form( "Single Gaussian Fit Q%i", mass_index ));

        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

        final_canv->SaveAs( Form( "./ttl_full_%s_sys/final_%s_Q%i.png", str_overname.c_str(), str_overname.c_str(), mass_index ) );

      }
    }
  }

  table_file << "\\end{document}\n";
  table_file.close();

}
        
        
        //TH1F * extr_err = new TH1F( Form( "extr_err_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
        //TH1F * eff_err = new TH1F( Form( "eff_err_Q%i", mass_index ), "", abin_bins, abin_min, abin_max );
        //extr_corr_ratio->Divide( sys_ex_corr, base_ex_corr, 1.0, 1.0 );
        //efficiency_ratio->Divide( sys_eff, base_eff, 1.0, 1.0 );

        
        //unit_line->SetLineColorAlpha(1,0.5);
        //unit_line->SetLineStyle(2);
        //unit_line->SetLineWidth(1.0);


        //mass_canv->cd(1);
        //double extr_max = std::ceil( extr_corr_ratio->GetMaximum()*1.3 );
        //extr_corr_ratio->Draw("P HIST");
        //extr_corr_ratio->SetMarkerStyle(21);
        //extr_corr_ratio->SetLineColor(1);
        //extr_corr_ratio->GetYaxis()->SetRangeUser(0,extr_max);
        //extr_corr_ratio->GetYaxis()->SetTitle(Form("Ratio sys/base / %.3f %s",width, unit.c_str()));
        //extr_corr_ratio->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        //extr_corr_ratio->GetYaxis()->SetLabelSize(0.035);
        //extr_corr_ratio->GetYaxis()->SetTitleSize(0.035);
        //extr_corr_ratio->GetXaxis()->SetLabelSize(0.035);
        //extr_corr_ratio->GetXaxis()->SetTitleSize(0.035);
        //gPad->Update();
        //gPad->SetTicks(1,0);
        ////double extr_pre_scale = (extr_max+0.5)*base_ex_corr->GetMaximum();
        ////base_ex_corr->Scale(extr_max/extr_pre_scale);
        ////sys_ex_corr->Scale(extr_max/extr_pre_scale);
        ////base_ex_corr->SetLineWidth(1);
        ////base_ex_corr->SetMarkerStyle(0);
        ////base_ex_corr->SetLineColorAlpha(kRed+1,1.0);
        ////sys_ex_corr->SetLineWidth(1);
        ////sys_ex_corr->SetMarkerStyle(0);
        ////sys_ex_corr->SetLineColorAlpha(kBlue+1,1.0);
        ////sys_ex_corr->Draw("E1 SAME");
        ////base_ex_corr->Draw("E1 SAME");
        ////sys_ex_corr->Draw("HIST SAME");
        ////base_ex_corr->Draw("HIST SAME");
        //unit_line->Draw("SAME");
        ////gPad->Update();
        ////TGaxis * extr_pad_axis = new TGaxis(abin_max,0,abin_max,extr_max,0,extr_pre_scale*(extr_max+0.5),510,"+L");
        ////extr_pad_axis->SetLabelSize(0.035);
        ////extr_pad_axis->SetLabelFont(42);
        ////extr_pad_axis->SetMaxDigits(4);
        ////extr_pad_axis->SetTitleOffset(1.25);
        ////extr_pad_axis->SetTitleFont(42);
        ////extr_pad_axis->SetTitle(Form("Entries/%.3f, %s",width, unit.c_str()));
        ////extr_pad_axis->Draw();
        //TLegend * extr_legend = new TLegend(0.45,0.65,0.83,0.8);
        //extr_legend->SetBorderSize(0);
        //extr_legend->SetFillColor(0);
        //extr_legend->SetFillStyle(0);
        //extr_legend->SetTextFont(42);
        //extr_legend->SetTextSize(0.025);
        //extr_legend->AddEntry(extr_corr_ratio," Sys/baseline Ratio","LP");
        ////extr_legend->AddEntry(base_ex_corr,  "Eff adjusted base signal","LP");
        ////extr_legend->AddEntry(sys_ex_corr,   "Eff adjusted sys signal","LP");
        //extr_legend->Draw();
        //TLatex pad3_ltx;
        //pad3_ltx.SetTextSize(0.03);
        //pad3_ltx.DrawLatexNDC(0.2,0.825,Form("%s Extracted ratio sys/base adjusted signal - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");


        //mass_canv->cd(2);
        ////efficiency_ratio->Sumw2();
        //double eff_max = std::ceil( efficiency_ratio->GetMaximum() )+1.0;
        //efficiency_ratio->Divide(sys_eff,base_eff,1.0,1.0);
        //efficiency_ratio->Draw("E1");
        //efficiency_ratio->SetMarkerStyle(21);
        //efficiency_ratio->SetLineColor(1);
        //efficiency_ratio->GetYaxis()->SetRangeUser(0,eff_max);
        //efficiency_ratio->GetYaxis()->SetTitle(Form("Efficiency ratio sys/base / %.3f %s",width, unit.c_str()));
        //efficiency_ratio->GetXaxis()->SetTitle(Form("%s (%s)",abin_var.c_str(),unit.c_str()));
        //efficiency_ratio->GetYaxis()->SetLabelSize(0.035);
        //efficiency_ratio->GetYaxis()->SetTitleSize(0.035);
        //efficiency_ratio->GetXaxis()->SetLabelSize(0.035);
        //efficiency_ratio->GetXaxis()->SetTitleSize(0.035);
        ////gPad->Update();
        ////gPad->SetTicks(1,0);
        ////double eff_pre_scale = base_eff->GetMaximum();
        ////base_eff->Scale(eff_max/eff_pre_scale);
        ////sys_eff->Scale(eff_max/eff_pre_scale);
        ////base_eff->SetLineWidth(1);
        ////base_eff->SetMarkerStyle(0);
        ////base_eff->SetLineColorAlpha(kRed+1,1.0);
        ////sys_eff->SetLineWidth(1);
        ////sys_eff->SetMarkerStyle(0);
        ////sys_eff->SetLineColorAlpha(kBlue+1,1.0);
        ////sys_eff->Draw("E1 SAME");
        ////base_eff->Draw("E1 SAME");
        ////sys_eff->Draw("HIST SAME");
        ////base_eff->Draw("HIST SAME");
        //unit_line->Draw("SAME");
        ////gPad->Update();
        ////TGaxis * eff_pad_axis = new TGaxis(abin_max,0,abin_max,eff_max,0,eff_pre_scale,510,"+L");
        ////eff_pad_axis->SetLabelSize(0.035);
        ////eff_pad_axis->SetLabelFont(42);
        ////eff_pad_axis->SetMaxDigits(4);
        ////eff_pad_axis->SetTitleOffset(1.25);
        ////eff_pad_axis->SetTitleFont(42);
        ////eff_pad_axis->SetTitle(Form("Entries/%.3f, %s",width, unit.c_str()));
        ////eff_pad_axis->Draw();
        //TLegend * eff_pad_legend = new TLegend(0.45,0.65,0.83,0.8);
        //eff_pad_legend->SetBorderSize(0);
        //eff_pad_legend->SetFillColor(0);
        //eff_pad_legend->SetFillStyle(0);
        //eff_pad_legend->SetTextFont(42);
        //eff_pad_legend->SetTextSize(0.025);
        //eff_pad_legend->AddEntry(efficiency_ratio,"Efficiency Ratio","LP");
        ////eff_pad_legend->AddEntry(base_eff, "Base Efficiency","LP");
        ////eff_pad_legend->AddEntry(sys_eff,   "Sys Efficiency","LP");
        //eff_pad_legend->Draw();
        //TLatex eff_pad_ltx;
        //eff_pad_ltx.SetTextSize(0.03);
        //eff_pad_ltx.DrawLatexNDC(0.2,0.825,Form("%s Efficiency ratio sys/base - Mass Q%i",abin_var.c_str(),mass_index));  
        //ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
        //wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

        //extr_err->Add( extr_corr_ratio, unit_hist, 1.0, -1.0);
        //eff_err->Add( efficiency_ratio, unit_hist, 1.0, -1.0);

        //mass_canv->SaveAs(Form("./sys_Q%i_A-%s_%s.png",mass_index,abin_var.c_str(),unique.c_str()));
       
