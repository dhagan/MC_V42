#include "hlrt.hxx"

hlrt(std::string path, std::string lin_files, std::string spec_vars, std::string slice, std::string type, std::string unique){

  gROOT->SetBatch(true);
  std::vector<std::string> vec_spec_vars;
  split_strings(vec_spec_vars, spec_vars, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};

  int hf_error_bin = 0;
  std::string full_type;
  int fit_color;
  if (type.find("data") != std::string::npos){ hf_error_bin = 1; full_type = "data"; fit_color = 1;}  
  if (type.find("sign") != std::string::npos){ hf_error_bin = 2; full_type = "signal"; fit_color = kRed+1;}
  if (type.find("bckg") != std::string::npos){ hf_error_bin = 3; full_type = "background"; fit_color = kBlue+1;}

  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";


  std::vector<int> mass_indices = {0,3,4,5,12};
  std::vector<std::string> vec_lh;
  split_strings(vec_ls, str_lin_files, ":");

  std::string high_filepath = path +  vec_lh.at(0);
  std::string low_filepath = path + vec_lh.at(1);
  std::string hl_filepath = path + unique
  TFile * high_integral_file = new TFile(high_filepath.c_str(),   "READ");
  TFile * low_integral_file = new TFile(low_filepath.c_str(),   "READ");
  TFile * hl_integral_file = new TFile(hl_filepath.c_str(), "RECREATE");
  
  int bdt_bins     = (int)      10; 
  double bdt_min   = (double) -1.0;
  double bdt_max   = (double)  1.0;

  for (int & mass_index : mass_indices){

    char mass_char[100];        
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
    for (std::string abin_var : vec_abin_vars){
        
      std::string abin_var = "Phi"
      std::string var_name = abin_var;
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)    bins.at(0);
      
      for (std::string spec_var : vec_spec_vars){

        bool error_available = true;
        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
           
          int bin_flags[10];
          std::fill_n (&bin_flags[0], sizeof(bin_flags)/sizeof(bin_flags[0]),0); // memset?

          char bin_hist_name[150], error_name[150], ratio_bin_hist_name[150];
          sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(ratio_bin_hist_name, "ratio_%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(ratio_error_name,     "ratio_%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);
          TH1F * high_integral_hist  = (TH1F*) high_integral_file->Get(bin_hist_name);
          TH1F * high_error_hist     = (TH1F*) high_integral_file->Get(error_name);
          TH1F * low_integral_hist  = (TH1F*) low_integral_file->Get(bin_hist_name);
          TH1F * low_error_hist     = (TH1F*) low_integral_file->Get(error_name);
          TH1F * ratio_hist = new TH1F(ratio_bin_hist_name,ratio_bin_hist_name,bdt_bins,bdt_min,bdt_max);
          TH1F * ratio_err_hist = new TH1F(ratio_error_name,bdt_bins,3,0,3);
          double h_int = high_integral_hist->Integral(); double l_int = low_integral_hist->Integral();
          double h_err = high_error_hist->GetBinContent(hf_error_bin); double l_err = low_error_hist->GetBinContent(hf_error_bin);
          if (high_integral_hist->Integral() == 0){
            ratio_hist = 0;
          } else {
            ratio_hist.SetBinContent(1,l_int/h_int);
          }
          ratio_err_hist->SetBinError(hf_error_bin, ratio_hist->Integral()*std::sqrt((l_err/l_int)*(l_err/l_int) + (h_err/h_int)*(h_err/h_int) ));

          delete high_integral_hist;
          delete low_integral_hist;

          TH1F * output_hist = ratio_hist->Clone(bin_hist_name);
          TH1F * output_err = ratio_err_hist->Clone(error_name);
        
          hl_integral_file->cd();
          output_hist->Write();
          output_err->Write();
          delete output_hist;
          delete output_err;

        }
      }
    }
  }
}



