#include <bdt.hxx>
#include <split_string.hxx>

void bdt(std::string eff_path, std::string hf_path, std::string sys_eff_path, std::string sys_hf_path, 
    std::string abin_vars, std::string spec_vars, std::string type, std::string slice, std::string unique, 
    std::string range){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);
                                    
  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  TLatex ATLAS; ATLAS.SetNDC(); 
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42); 
  wip.SetTextSize(0.038); wip.SetTextColor(1);
 
  //int mass_bins[5] = { 0, 3, 4, 5, 12 };
  int mass_bins[1] = { 0 };

  TFile * base_eff_file         = new TFile(eff_path.c_str(),   "READ");
  TFile * base_integral_file    = new TFile(hf_path.c_str(),    "READ");

  TFile * sys_eff_file          = new TFile(sys_eff_path.c_str(),    "READ");
  TFile * sys_integral_file     = new TFile(sys_hf_path.c_str(),    "READ");

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,0,M_PI};
  analysis_bins["DPhi"]          =  std::vector<double>{8,0,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{10,5,30};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5,25};

  if ( !range.empty() ){
    std::vector< std::string > arg_bin;
    std::vector< std::string > set_bins;
    split_strings( arg_bin, range, "_" );
    split_strings( set_bins, arg_bin.at(1), "," );
    std::map< std::string, std::vector<double> >::iterator bin_map_itr = analysis_bins.find( arg_bin.at( 0 ).c_str() );
    bin_map_itr->second = { std::stod( set_bins.at( 0 ) ), std::stod( set_bins.at( 1 ) ), std::stod( set_bins.at( 2 ) ) };
    std::cout << range << std::endl;
  }
  
  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["BDT"]           =  "Value";
  units["abs(qtB)"]      =  "GeV";
  units["qtB"]           =  "GeV";
  units["Phi"]           =  "rad";
  units["DPhi"]          =  "rad";
  units["DiMuonPt"]      =  "GeV";
  units["PhotonPt"]      =  "GeV";

  int hf_error_bin = 0;
  std::string full_type;
  if (type.find("data") != std::string::npos){ hf_error_bin = 1; full_type = "data";}  
  if (type.find("sign") != std::string::npos){ hf_error_bin = 2; full_type = "signal";}
  if (type.find("bckg") != std::string::npos){ hf_error_bin = 3; full_type = "background";}
  
  for ( int & mass_index : mass_bins ){

    char mass_char[100];        
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
    for (std::string abin_var : vec_abin_vars){
        
      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      if (abin_var.find("_low") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      if (abin_var.find("_high") != std::string::npos){ var_name = "Phi"; abin_var = "Phi"; }
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)    bins.at(0);
      double abin_min   = (double) bins.at(1);
      double abin_max   = (double) bins.at(2);
      double width      = (double) (abin_max-abin_min)/((double) abin_bins);

      for (std::string spec_var : vec_spec_vars){
        
        TH1F * base_ex_reco = new TH1F( Form("base_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * base_eff     = (TH1F*) base_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));

        TH1F * sys_ex_reco  = new TH1F( Form("sys_ex_signal_reco_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * sys_eff      = (TH1F*) sys_eff_file->Get(Form("eff_%s_Q%i", abin_var.c_str(), mass_index));

        //TH1F * extr_corr_ratio  = new TH1F(   Form("sys_base_ratio_Q%i",mass_index), "", abin_bins, abin_min, abin_max);
        TH1F * efficiency_ratio  = new TH1F(   Form("sys_base_eff_ratio_Q%i",mass_index), "", abin_bins, abin_min, abin_max);

        bool base_error_available{true}, sys_error_available{true};
        for (int abin_index = 0; abin_index <= abin_bins; abin_index++){
           
          char bin_hist_name[150], error_name[150];
          sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);

          TH1F * base_integral_hist   = (TH1F*) base_integral_file->Get(bin_hist_name);
          TH1F * base_error_hist      = (TH1F*) base_integral_file->Get(error_name);

          TH1F * sys_integral_hist    = (TH1F*) sys_integral_file->Get(bin_hist_name);
          TH1F * sys_error_hist       = (TH1F*) sys_integral_file->Get(error_name);
          
          TCanvas * score_canv = new TCanvas( Form( "%s_%i_Q%i", unique.c_str(), abin_index, mass_index ), "", 100, 100,
                                             1000, 1000 );
          score_canv->Divide( 1, 1 );

          score_canv->cd(1);

          base_integral_hist->Draw( "HIST" );




        



        }
      }
    }
  }



}

