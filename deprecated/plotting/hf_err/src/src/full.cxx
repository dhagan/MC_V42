#include <full.hxx>
#include <split_string.hxx>

//#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasLabels.C"
//#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasUtils.C"
//#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasStyle.C"


void full(std::string str_hstfc_file, std::string str_data_file, std::string str_sign_file, std::string str_bckg_file, std::string str_abin_vars, std::string str_spec_vars, std::string slice, std::string unique,std::string lin_files, std::string str_dsign_file,std::string str_dbckg_file, std::string str_sub_path ){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);

  TFile * integral_file = new TFile(str_hstfc_file.c_str(),   "READ");
  TFile * sub_data_file = new TFile(str_data_file.c_str(),    "READ");
  TFile * sub_sign_file = new TFile(str_sign_file.c_str(),    "READ");
  TFile * sub_bckg_file = new TFile(str_bckg_file.c_str(),    "READ");
  TFile * dsign_file = new TFile(str_dsign_file.c_str(),    "READ");
  TFile * dbckg_file = new TFile(str_dbckg_file.c_str(),    "READ");

  
  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, str_abin_vars, ":");
  split_strings(vec_spec_vars, str_spec_vars, ":");

  std::string lin_file_path, str_lin_suf;
  std::vector<std::string> vec_initial_split, vec_lin_suf;
  split_strings(vec_initial_split, lin_files, "?");
  lin_file_path   = vec_initial_split.at(0);
  str_lin_suf     = vec_initial_split.at(1);
  split_strings(vec_lin_suf,str_lin_suf,":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};

  // create fit objects
  TF1 * fit_single_gaussian = new TF1("fit_double_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2))" ); 
  fit_single_gaussian->SetParLimits(1,-5,15); fit_single_gaussian->SetParLimits(2,0,10); 
  fit_single_gaussian->SetParName(0,"C"); fit_single_gaussian->SetParName(1,"X"); fit_single_gaussian->SetParName(2,"Sigma");
  
  TF1 * fit_double_gaussian = new TF1("fit_double_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) + [3]*exp( -((x-[1])^2)/(2*[4]^2)) " );
  //fit_double_gaussian->SetParLimits(4,momentum_min,momentum_max); 

  fit_double_gaussian->SetParName(0,"C1");
  fit_double_gaussian->SetParName(1,"X1");
  fit_double_gaussian->SetParName(2,"sig1");
  fit_double_gaussian->SetParName(3,"C2");
  // fit_double_gaussian->SetParName(4,"X2"); 
  fit_double_gaussian->SetParName(4,"sig2");
  fit_double_gaussian->SetParLimits(0,0,3000);
  fit_double_gaussian->SetParLimits(1,-5,15);
  fit_double_gaussian->SetParLimits(2,2,6);
  fit_double_gaussian->SetParLimits(3,0,3000);
  fit_double_gaussian->SetParLimits(4,99,101);
  fit_double_gaussian->SetParameter(2,3);
  fit_double_gaussian->SetParameter(4,100);

  TF1 * fit_sc_gaussian = new TF1("fit_sc_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) + [3]" );
  fit_sc_gaussian->SetParLimits(0,0,3000);
  fit_sc_gaussian->SetParLimits(1,-5,15);
  fit_sc_gaussian->SetParLimits(2,2,6);
  fit_sc_gaussian->SetParLimits(3,0,3000);
  fit_sc_gaussian->SetParName(0,"C1");
  fit_sc_gaussian->SetParName(1,"X1");
  fit_sc_gaussian->SetParName(2,"sig1");
  fit_sc_gaussian->SetParName(2,"C2");

  std::vector<int> mass_indices = {0,3,4,5,12};
  
  std::vector <TH1F> data_hist_vec;
  std::vector <TH1F> sign_hist_vec;
  std::vector <TH1F> bckg_hist_vec;
  std::vector <TH1F> dsign_hist_vec;
  std::vector <TH1F> dbckg_hist_vec;

  std::vector <TH1F> data_sub_hist_vec;
  std::vector <TH1F> sign_sub_hist_vec;
  std::vector <TH1F> bckg_sub_hist_vec;

  std::vector <TH1F> comp_sign_hist_vec;
  std::vector <TH1F> comp_bckg_hist_vec;
  std::vector <TH1F> comp_dsign_hist_vec;
  std::vector <TH1F> comp_dbckg_hist_vec;
    
  
  int compound_index_max = mass_indices.size() * vec_abin_vars.size() * vec_spec_vars.size();
  
  std::vector<int>          mass_index_vec;
  std::vector<std::string>  abin_var_order_vec;
  std::vector<std::string>  spec_var_order_vec;

  for (int & mass_index : mass_indices){

    for (std::string abin_var : vec_abin_vars){

      for (std::string spec_var : vec_spec_vars){

        std::vector<double> bins = analysis_bins[abin_var];
        int abin_bins     = (int)     bins.at(0);
        double abin_min   = (double)  bins.at(1);
        double abin_max   = (double)  bins.at(2);

        char data_mass_name[20], sign_mass_name[20], bckg_mass_name[20],mass_char[20];
        char data_sub_mass_name[20], sign_sub_mass_name[20], bckg_sub_mass_name[20];
        char dsign_mass_name[20], dbckg_mass_name[20];

        sprintf(data_mass_name,         "data_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(sign_mass_name,         "sign_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(bckg_mass_name,         "bckg_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(data_sub_mass_name,     "data_sub_mass-%i_s%s_a%s", mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(sign_sub_mass_name,     "sign_sub_mass-%i_s%s_a%s", mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(bckg_sub_mass_name,     "bckg_sub_mass-%i_s%s_a%s", mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(dsign_mass_name,        "dsign_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(dbckg_mass_name,        "dbckg_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());

        sprintf(mass_char,"both-theta_mass-%i",mass_index);

        TH1F data_mass_hist(          data_mass_name,       data_mass_name,       abin_bins,  abin_min,   abin_max);
        TH1F sign_mass_hist(          sign_mass_name,       sign_mass_name,       abin_bins,  abin_min,   abin_max);
        TH1F bckg_mass_hist(          bckg_mass_name,       bckg_mass_name,       abin_bins,  abin_min,   abin_max);
        TH1F data_sub_mass_hist(      data_sub_mass_name,   data_sub_mass_name,   abin_bins,  abin_min,   abin_max);
        TH1F sign_sub_mass_hist(      sign_sub_mass_name,   sign_sub_mass_name,   abin_bins,  abin_min,   abin_max);
        TH1F bckg_sub_mass_hist(      bckg_sub_mass_name,   bckg_sub_mass_name,   abin_bins,  abin_min,   abin_max);
        TH1F dsign_mass_hist(         dsign_mass_name,      dsign_mass_name,      abin_bins,  abin_min,   abin_max);
        TH1F dbckg_mass_hist(         dbckg_mass_name,      dbckg_mass_name,      abin_bins,  abin_min,   abin_max);

        
        char comp_mass_sign_name[50], comp_mass_bckg_name[50], comp_mass_dsign_name[50], comp_mass_dbckg_name[50];
        sprintf(comp_mass_sign_name,       "comp_mass_sign-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(comp_mass_bckg_name,       "comp_mass_bckg-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(comp_mass_dsign_name,       "comp_mass_dsign-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(comp_mass_dbckg_name,       "comp_mass_dbckg-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        TH1F comp_mass_sign_hist(          comp_mass_sign_name, bckg_sub_mass_name, abin_bins,  abin_min,   abin_max);
        TH1F comp_mass_bckg_hist(          comp_mass_bckg_name, bckg_sub_mass_name, abin_bins,  abin_min,   abin_max);
        TH1F comp_mass_dsign_hist(          comp_mass_dsign_name, bckg_sub_mass_name, abin_bins,  abin_min,   abin_max);
        TH1F comp_mass_dbckg_hist(          comp_mass_dbckg_name, bckg_sub_mass_name, abin_bins,  abin_min,   abin_max);


        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){

          char data_hist_name[100], sign_hist_name[100],bckg_hist_name[100];
          char data_sub_name[100], sign_sub_name[100],bckg_sub_name[100];
          char error_name[100];
          char ddata_name[200];
          sprintf(data_hist_name, "%s_data_%s_pos_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(sign_hist_name, "%s_sign_%s_pos_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(bckg_hist_name, "%s_bckg_%s_pos_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_pos_%s_%s-%i", spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(data_sub_name,  "%s_data_%s_pos_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(sign_sub_name,  "%s_sign_%s_pos_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(bckg_sub_name,  "%s_bckg_%s_pos_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(ddata_name,  "%s_data_%s_npos_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);



          TH1F * data_integral_hist = (TH1F*) integral_file->Get(data_hist_name);
          TH1F * sign_integral_hist = (TH1F*) integral_file->Get(sign_hist_name);
          TH1F * bckg_integral_hist = (TH1F*) integral_file->Get(bckg_hist_name);
          TH1F * error_hist = (TH1F*) integral_file->Get(error_name);
          TH1F * data_sub_integral_hist = (TH1F*) sub_data_file->Get(data_sub_name);
          TH1F * sign_sub_integral_hist = (TH1F*) sub_sign_file->Get(sign_sub_name);
          TH1F * bckg_sub_integral_hist = (TH1F*) sub_bckg_file->Get(bckg_sub_name);

          TH1F * dsign_integral_hist = (TH1F*) dsign_file->Get(ddata_name);
          TH1F * dbckg_integral_hist = (TH1F*) dbckg_file->Get(ddata_name);

          data_mass_hist.SetBinContent(abin_index,data_integral_hist->Integral()); 
          data_mass_hist.SetBinError(abin_index,error_hist->GetBinError(1)); 
          sign_mass_hist.SetBinContent(abin_index,sign_integral_hist->Integral()); 
          sign_mass_hist.SetBinError(abin_index,error_hist->GetBinError(2)); 
          bckg_mass_hist.SetBinContent(abin_index,bckg_integral_hist->Integral()); 
          bckg_mass_hist.SetBinError(abin_index,error_hist->GetBinError(3)); 

          data_sub_mass_hist.SetBinContent(abin_index,data_sub_integral_hist->Integral()); 
          sign_sub_mass_hist.SetBinContent(abin_index,sign_sub_integral_hist->Integral()); 
          bckg_sub_mass_hist.SetBinContent(abin_index,bckg_sub_integral_hist->Integral()); 
          dsign_mass_hist.SetBinContent(abin_index,dsign_integral_hist->Integral()); 
          dbckg_mass_hist.SetBinContent(abin_index,dbckg_integral_hist->Integral()); 

          delete data_integral_hist;
          delete sign_integral_hist;
          delete bckg_integral_hist;
          
          delete data_sub_integral_hist;
          delete sign_sub_integral_hist;
          delete bckg_sub_integral_hist;
    
          delete dsign_integral_hist;
          delete dbckg_integral_hist;

          // extracted combination signal
          double lin_sign_bin_count{0}, lin_sign_bin_sq_error_sum{0}, lin_sign_bin_error;
          for ( int lin_sign_idx = 0; lin_sign_idx <(int) vec_lin_suf.size(); lin_sign_idx++){
            std::string lin_sign_file_name = lin_file_path + vec_lin_suf.at(lin_sign_idx) + ".root";
            TFile * lin_sign_file = new TFile( lin_sign_file_name.c_str(),   "READ");
            TH1F * lin_sign_data_hist = (TH1F*) lin_sign_file->Get(sign_hist_name);
            TH1F * lin_sign_error_hist = (TH1F*) lin_sign_file->Get(error_name);
            double lin_sign_integral = lin_sign_data_hist->Integral();
            double lin_sign_error = lin_sign_error_hist->GetBinError(2);
            lin_sign_bin_count += lin_sign_integral;
            lin_sign_bin_sq_error_sum += lin_sign_error*lin_sign_error;
            lin_sign_file->Close();
            delete lin_sign_file;
          }
          lin_sign_bin_error = std::sqrt(lin_sign_bin_sq_error_sum);
          comp_mass_sign_hist.SetBinContent(abin_index,lin_sign_bin_count); 
          comp_mass_sign_hist.SetBinError(abin_index,lin_sign_bin_error); 

          // extracted combination background 
          double lin_bckg_bin_count{0}, lin_bckg_bin_sq_error_sum{0}, lin_bckg_bin_error;
          for ( int lin_bckg_idx = 0; lin_bckg_idx < (int) vec_lin_suf.size(); lin_bckg_idx++){
            std::string lin_bckg_file_name = lin_file_path + vec_lin_suf.at(lin_bckg_idx) + ".root";
            TFile * lin_bckg_file = new TFile( lin_bckg_file_name.c_str(),   "READ");
            TH1F * lin_bckg_data_hist = (TH1F*) lin_bckg_file->Get(bckg_hist_name);
            TH1F * lin_bckg_error_hist = (TH1F*) lin_bckg_file->Get(error_name);
            double lin_bckg_integral = lin_bckg_data_hist->Integral();
            double lin_bckg_error = lin_bckg_error_hist->GetBinError(2);
            lin_bckg_bin_count += lin_bckg_integral;
            lin_bckg_bin_sq_error_sum += (lin_bckg_error*lin_bckg_error);
            lin_bckg_file->Close();
            delete lin_bckg_file;
          }
          lin_bckg_bin_error = std::sqrt(lin_sign_bin_sq_error_sum);
          comp_mass_bckg_hist.SetBinContent(abin_index,lin_bckg_bin_count); 
          comp_mass_bckg_hist.SetBinError(abin_index,lin_bckg_bin_error); 


          double lin_dsign_bin_count{0};// lin_dsign_bin_sq_error_sum{0}, lin_dsign_bin_error;
          for ( int lin_dsign_idx = 0; lin_dsign_idx < (int) vec_lin_suf.size(); lin_dsign_idx++){
            std::string lin_dsign_file_name = str_sub_path + "_dsign" + (vec_lin_suf.at(lin_dsign_idx)).substr(0,vec_lin_suf.at(lin_dsign_idx).size()-3) + ".root";
            TFile * lin_dsign_file = new TFile( lin_dsign_file_name.c_str(),   "READ");
            TH1F * lin_dsign_data_hist = (TH1F*) lin_dsign_file->Get(ddata_name);
            double lin_dsign_integral = lin_dsign_data_hist->Integral();
            lin_dsign_bin_count += lin_dsign_integral;
            lin_dsign_file->Close();
            delete lin_dsign_file;
          }
          comp_mass_dsign_hist.SetBinContent(abin_index,lin_dsign_bin_count); 
            
          double lin_dbckg_bin_count{0};// lin_dbckg_bin_sq_error_sum{0}, lin_dbckg_bin_error;
          for ( int lin_dbckg_idx = 0; lin_dbckg_idx < (int) vec_lin_suf.size(); lin_dbckg_idx++){
            std::string lin_dbckg_file_name = str_sub_path + "_dbckg" + (vec_lin_suf.at(lin_dbckg_idx)).substr(0,vec_lin_suf.at(lin_dbckg_idx).size()-3) + ".root";
            TFile * lin_dbckg_file = new TFile( lin_dbckg_file_name.c_str(),   "READ");
            TH1F * lin_dbckg_data_hist = (TH1F*) lin_dbckg_file->Get(ddata_name);
            double lin_dbckg_integral = lin_dbckg_data_hist->Integral();
            lin_dbckg_bin_count += lin_dbckg_integral;
            lin_dbckg_file->Close();
            delete lin_dbckg_file;
          }
          comp_mass_dbckg_hist.SetBinContent(abin_index,lin_dbckg_bin_count); 

        }

        mass_index_vec.push_back(mass_index);
        abin_var_order_vec.push_back(abin_var);
        spec_var_order_vec.push_back(spec_var);

        data_hist_vec.push_back(data_mass_hist);
        sign_hist_vec.push_back(sign_mass_hist);
        bckg_hist_vec.push_back(bckg_mass_hist);

        data_sub_hist_vec.push_back(data_sub_mass_hist);
        sign_sub_hist_vec.push_back(sign_sub_mass_hist);
        bckg_sub_hist_vec.push_back(bckg_sub_mass_hist);

        comp_sign_hist_vec.push_back(comp_mass_sign_hist);
        comp_bckg_hist_vec.push_back(comp_mass_bckg_hist);
        dsign_hist_vec.push_back(dsign_mass_hist);
        dbckg_hist_vec.push_back(dbckg_mass_hist);

        comp_dsign_hist_vec.push_back(comp_mass_dsign_hist);
        comp_dbckg_hist_vec.push_back(comp_mass_dbckg_hist);

      }
    }
  }

  for (int comp_index = 0; comp_index < compound_index_max; comp_index++){


    char canv_name[30];
    sprintf(canv_name,"canv%i",comp_index);

    TCanvas output_canvas(canv_name,canv_name,600,600,8000,2000);
    output_canvas.Divide(4,3);

    char mass_name[20];
    sprintf(mass_name,"mass%i",mass_index_vec[comp_index]);

    std::vector<double> maxs;
    output_canvas.cd(1);
    data_hist_vec[comp_index].Draw();
    sign_hist_vec[comp_index].Draw("HIST SAME");
    bckg_hist_vec[comp_index].Draw("HIST SAME");
    data_hist_vec[comp_index].SetLineColor(1);
    sign_hist_vec[comp_index].SetLineColor(2);
    bckg_hist_vec[comp_index].SetLineColor(4);
    bckg_hist_vec[comp_index].SetLineStyle(2);
    comp_sign_hist_vec[comp_index].SetLineColor(6);
    comp_sign_hist_vec[comp_index].Draw("SAME");
    maxs.push_back(data_hist_vec[comp_index].GetMaximum());
    maxs.push_back(sign_hist_vec[comp_index].GetMaximum());
    maxs.push_back(bckg_hist_vec[comp_index].GetMaximum());
    data_hist_vec[comp_index].GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad1_ltx;
    pad1_ltx.SetTextSize(0.05);
    pad1_ltx.DrawLatexNDC(0.0,0.96,mass_name);
    pad1_ltx.SetTextSize(0.03);
    pad1_ltx.DrawLatexNDC(0.2,0.97,  "After Histfactory");
    pad1_ltx.DrawLatexNDC(0.25,0.89, Form("n_{Data} = %.0f",data_hist_vec[comp_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.86, Form("n_{SignalEx} = %.0f",sign_hist_vec[comp_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.83, Form("n_{BckgEx} = %.0f",bckg_hist_vec[comp_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.80, Form("n_{linEx} = %.0f",comp_sign_hist_vec[comp_index].Integral()));
    TLegend pad1_lgnd(0.8, 0.8, 0.925,  0.925);
    pad1_lgnd.SetTextSize(0.025);
    pad1_lgnd.SetFillStyle(0);
    pad1_lgnd.SetTextFont(42);
    pad1_lgnd.AddEntry(&data_hist_vec[comp_index], "Data", "lep");
    pad1_lgnd.AddEntry(&sign_hist_vec[comp_index], "Signal",  "l");
    pad1_lgnd.AddEntry(&bckg_hist_vec[comp_index], "Bckg",  "l  ");
    pad1_lgnd.SetBorderSize(0);
    pad1_lgnd.Draw();

    output_canvas.cd(2);
    data_sub_hist_vec[comp_index].Draw();
    sign_sub_hist_vec[comp_index].Draw("HIST SAME");
    bckg_sub_hist_vec[comp_index].Draw("HIST SAME");
    data_sub_hist_vec[comp_index].SetLineColor(1);
    sign_sub_hist_vec[comp_index].SetLineColor(2);
    bckg_sub_hist_vec[comp_index].SetLineColor(4);
    bckg_sub_hist_vec[comp_index].SetLineStyle(2);
    maxs.push_back(data_sub_hist_vec[comp_index].GetMaximum());
    maxs.push_back(sign_sub_hist_vec[comp_index].GetMaximum());
    maxs.push_back(bckg_sub_hist_vec[comp_index].GetMaximum());
    data_sub_hist_vec[comp_index].GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    data_sub_hist_vec[comp_index].SetLineColor(1);
    sign_sub_hist_vec[comp_index].SetLineColor(2);
    bckg_sub_hist_vec[comp_index].SetLineColor(4);
    bckg_sub_hist_vec[comp_index].SetLineStyle(2);
    TLatex pad2_ltx;
    pad2_ltx.SetTextSize(0.03);
    pad2_ltx.DrawLatexNDC(0.2,0.97, "Before HistFactory");
    pad2_ltx.DrawLatexNDC(0.25,0.89, Form("n_{Data} = %.0f",data_sub_hist_vec[comp_index].Integral()));
    pad2_ltx.DrawLatexNDC(0.25,0.86, Form("n_{Signal} = %.0f",sign_sub_hist_vec[comp_index].Integral()));
    pad2_ltx.DrawLatexNDC(0.25,0.83, Form("n_{Bckg} = %.0f",bckg_sub_hist_vec[comp_index].Integral()));
    TLegend pad2_lgnd(0.8, 0.8, 0.925,  0.925);
    pad2_lgnd.SetTextSize(0.025);
    pad2_lgnd.SetFillStyle(0);
    pad2_lgnd.SetTextFont(42);
    pad2_lgnd.AddEntry(&data_sub_hist_vec[comp_index], "Data", "lep");
    pad2_lgnd.AddEntry(&sign_sub_hist_vec[comp_index], "Signal",  "l");
    pad2_lgnd.AddEntry(&bckg_sub_hist_vec[comp_index], "Bckg",  "l  ");
    pad2_lgnd.SetBorderSize(0);
    pad2_lgnd.Draw();

    output_canvas.cd(3);
    TH1F sign_overlay_temp(sign_hist_vec[comp_index]);
    TH1F sign_sub_overlay_temp(sign_sub_hist_vec[comp_index]);
    TH1F comp_overlay_sign_temp(comp_sign_hist_vec[comp_index]);
    sign_overlay_temp.Scale(1.0/sign_overlay_temp.Integral());
    sign_sub_overlay_temp.Scale(1.0/sign_sub_overlay_temp.Integral());
    comp_overlay_sign_temp.Scale(1.0/comp_overlay_sign_temp.Integral());
    sign_overlay_temp.Draw("HIST");
    sign_sub_overlay_temp.Draw("HIST SAME");
    comp_overlay_sign_temp.Draw("HIST SAME");
    sign_overlay_temp.SetLineColor(2);
    sign_overlay_temp.SetLineStyle(1);
    comp_overlay_sign_temp.SetLineColor(6);
    sign_sub_overlay_temp.SetLineColor(2);
    sign_sub_overlay_temp.SetLineStyle(2);
    comp_overlay_sign_temp.Draw("E1 SAME");
    maxs.push_back(sign_overlay_temp.GetMaximum());
    maxs.push_back(sign_sub_overlay_temp.GetMaximum());
    sign_overlay_temp.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad3_ltx;
    pad3_ltx.SetTextSize(0.03);
    pad3_ltx.DrawLatexNDC(0.2,0.97, "Signal, normalised, Overlain");
    TLegend pad3_lgnd(0.65, 0.8, 0.925,  0.925);
    pad3_lgnd.SetTextSize(0.025);
    pad3_lgnd.SetFillStyle(0);
    pad3_lgnd.SetTextFont(42);
    pad3_lgnd.AddEntry(&sign_sub_overlay_temp, "Subtraction Signal", "lep");
    pad3_lgnd.AddEntry(&sign_overlay_temp, "Extracted Signal",  "l");
    pad3_lgnd.AddEntry(&comp_overlay_sign_temp, Form("Linearity check - %s",unique.c_str()),  "l");
    pad3_lgnd.SetBorderSize(0);
    pad3_lgnd.Draw();


    output_canvas.cd(4);
    TH1F temp_sign_1gauss_fit(sign_hist_vec[comp_index]);
    temp_sign_1gauss_fit.Draw("E1");
    temp_sign_1gauss_fit.GetYaxis()->SetRangeUser(0,4000);
    temp_sign_1gauss_fit.SetLineColor(2);
    fit_single_gaussian->SetParameter(0,2300);
    fit_single_gaussian->SetParameter(1,3);
    fit_single_gaussian->SetParameter(2,3);
    temp_sign_1gauss_fit.Fit(fit_single_gaussian,"MQ","");
    fit_single_gaussian->Draw("SAME");
    fit_single_gaussian->SetLineColor(1);
    temp_sign_1gauss_fit.Draw("HIST SAME");
    gPad->Modified(); gPad->Update();
    TPaveStats * pad4_stats = (TPaveStats*) temp_sign_1gauss_fit.FindObject("stats");
    pad4_stats->SetX1NDC(0.7); pad4_stats->SetX2NDC(0.925);
    pad4_stats->SetY1NDC(0.7); pad4_stats->SetY2NDC(0.925);
    pad4_stats->SetFillStyle(0);
    temp_sign_1gauss_fit.GetXaxis()->SetTitle("qTA (GeV)");
    temp_sign_1gauss_fit.GetYaxis()->SetTitle("Signal Yield/2GeV");
    TLatex pad4_ltx;
    pad4_ltx.SetTextSize(0.03);
    pad4_ltx.DrawLatexNDC(0.2,0.97, "Extracted Signal, Single Gaussian Fit");
    //TLegend pad4_lgnd(0.225, 0.8, 0.4,  0.925);
    //pad4_lgnd.SetTextSize(0.025);
    //pad4_lgnd.SetFillStyle(0);
    //pad4_lgnd.SetTextFont(42);
    //pad4_lgnd.AddEntry(&temp_sign_1gauss_fit, "Signal",  "l");
    //pad4_lgnd.AddEntry(fit_double_gaussian, "Fit",  "l");
    //pad4_lgnd.SetBorderSize(0);
    //pad4_lgnd.Draw();
    TLatex qta_2_ATLAS;
    qta_2_ATLAS.SetNDC();
    qta_2_ATLAS.SetTextFont(72);
    qta_2_ATLAS.SetTextColor(1);
    qta_2_ATLAS.DrawLatex(0.225,0.89,"ATLAS");
    TLatex qta_2_wip; 
    qta_2_wip.SetNDC();
    qta_2_wip.SetTextFont(42);
    qta_2_wip.SetTextSize(0.038);
    qta_2_wip.SetTextColor(1);
    qta_2_wip.DrawLatex(0.225,0.85,"Work In Progress");


    output_canvas.cd(5);
    TH1F temp_data(data_hist_vec[comp_index]);
    TH1F temp_sign(sign_hist_vec[comp_index]);
    TH1F temp_bckg(bckg_hist_vec[comp_index]);
    temp_data.Scale(1.0/temp_data.Integral());
    temp_sign.Scale(1.0/temp_sign.Integral());
    temp_bckg.Scale(1.0/temp_bckg.Integral());
    temp_data.Draw();
    temp_sign.Draw("HIST SAME");
    temp_bckg.Draw("HIST SAME");
    maxs.push_back(temp_data.GetMaximum());
    maxs.push_back(temp_sign.GetMaximum());
    maxs.push_back(temp_bckg.GetMaximum());
    temp_data.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad5_ltx;
    pad5_ltx.SetTextSize(0.03);
    pad5_ltx.DrawLatexNDC(0.2,0.97, "Normalised by Integral");
    TLegend pad5_lgnd(0.8, 0.8, 0.925,  0.925);
    pad5_lgnd.SetTextSize(0.025);
    pad5_lgnd.SetFillStyle(0);
    pad5_lgnd.SetTextFont(42);
    pad5_lgnd.AddEntry(&temp_data, "Data", "lep");
    pad5_lgnd.AddEntry(&temp_sign, "Signal",  "l");
    pad5_lgnd.AddEntry(&temp_bckg, "Bckg",  "l  ");
    pad5_lgnd.SetBorderSize(0);
    pad5_lgnd.Draw();

    output_canvas.cd(6);
    TH1F temp_sub_data(data_sub_hist_vec[comp_index]);
    TH1F temp_sub_sign(sign_sub_hist_vec[comp_index]);
    TH1F temp_sub_bckg(bckg_sub_hist_vec[comp_index]);
    temp_sub_data.Scale(1.0/temp_sub_data.Integral());
    temp_sub_sign.Scale(1.0/temp_sub_sign.Integral());
    temp_sub_bckg.Scale(1.0/temp_sub_bckg.Integral());
    temp_sub_data.Draw();
    temp_sub_sign.Draw("HIST SAME");
    temp_sub_bckg.Draw("HIST SAME");
    maxs.push_back(temp_sub_data.GetMaximum());
    maxs.push_back(temp_sub_sign.GetMaximum());
    maxs.push_back(temp_sub_bckg.GetMaximum());
    temp_sub_data.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad6_ltx;
    pad6_ltx.SetTextSize(0.03);
    pad6_ltx.DrawLatexNDC(0.2,0.97, "Before HistFactory, Normalised by Integral");
    TLegend pad6_lgnd(0.8, 0.8, 0.925,  0.925);
    pad6_lgnd.SetTextSize(0.025);
    pad6_lgnd.SetFillStyle(0);
    pad6_lgnd.SetTextFont(42);
    pad6_lgnd.AddEntry(&temp_sub_data, "Data", "lep");
    pad6_lgnd.AddEntry(&temp_sub_sign, "Signal",  "l");
    pad6_lgnd.AddEntry(&temp_sub_bckg, "Bckg",  "l  ");
    pad6_lgnd.SetBorderSize(0);
    pad6_lgnd.Draw();

    output_canvas.cd(7); // normalised background before and after
    TH1F bckg_overlay_temp(bckg_hist_vec[comp_index]);
    TH1F bckg_sub_overlay_temp(bckg_sub_hist_vec[comp_index]);
    TH1F comp_overlay_bckg_temp(comp_bckg_hist_vec[comp_index]);
    bckg_overlay_temp.Scale(1.0/bckg_overlay_temp.Integral());
    bckg_sub_overlay_temp.Scale(1.0/bckg_sub_overlay_temp.Integral());
    comp_overlay_bckg_temp.Scale(1.0/comp_overlay_bckg_temp.Integral());
    bckg_overlay_temp.Draw("HIST");
    bckg_sub_overlay_temp.Draw("HIST SAME");
    comp_overlay_bckg_temp.Draw("HIST SAME");
    bckg_overlay_temp.SetLineColor(4);
    bckg_overlay_temp.SetLineStyle(1);
    bckg_sub_overlay_temp.SetLineColor(4);
    bckg_sub_overlay_temp.SetLineStyle(2);
    comp_overlay_bckg_temp.SetLineColor(6);
    comp_overlay_bckg_temp.SetLineStyle(1);
    comp_overlay_bckg_temp.Draw("E1 SAME");
    maxs.push_back(bckg_overlay_temp.GetMaximum());
    maxs.push_back(bckg_sub_overlay_temp.GetMaximum());
    bckg_overlay_temp.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad7_ltx;
    pad7_ltx.SetTextSize(0.03);
    pad7_ltx.DrawLatexNDC(0.2,0.97, "Background (pp), normalised, Overlain");
    TLegend pad7_lgnd(0.6, 0.8, 0.925,  0.925);
    pad7_lgnd.SetTextSize(0.025);
    pad7_lgnd.SetFillStyle(0);
    pad7_lgnd.SetTextFont(42);
    pad7_lgnd.AddEntry(&bckg_sub_overlay_temp, "Subtraction Background", "lep");
    pad7_lgnd.AddEntry(&bckg_overlay_temp, "Extracted Background",  "l");
    pad7_lgnd.AddEntry(&comp_overlay_bckg_temp,Form("Linearity check - %s",unique.c_str()),  "l");
    pad7_lgnd.SetBorderSize(0);
    pad7_lgnd.Draw();

    output_canvas.cd(8);
    TH1F temp_sign_2gauss_fit(sign_hist_vec[comp_index]);
    temp_sign_2gauss_fit.Draw();
    temp_sign_2gauss_fit.GetYaxis()->SetRangeUser(0,4000);
    temp_sign_2gauss_fit.SetLineColor(2);
    temp_sign_2gauss_fit.Fit(fit_double_gaussian,"MQ","",-5,15);
    fit_double_gaussian->Draw("SAME");
    fit_double_gaussian->SetLineColor(1);
    temp_sign_2gauss_fit.Draw("HIST SAME");
    gPad->Modified(); gPad->Update();
    TPaveStats * pad8_stats = (TPaveStats*) temp_sign_2gauss_fit.FindObject("stats");
    pad8_stats->SetX1NDC(0.7); pad8_stats->SetX2NDC(0.925);
    pad8_stats->SetY1NDC(0.7); pad8_stats->SetY2NDC(0.925);
    pad8_stats->SetFillStyle(0);
    TLatex pad8_ltx;
    pad8_ltx.SetTextSize(0.03);
    pad8_ltx.DrawLatexNDC(0.2,0.97, "Double Gaussian Fit");
    TLegend pad8_lgnd(0.225, 0.8, 0.4,  0.925);
    pad8_lgnd.SetTextSize(0.025);
    pad8_lgnd.SetFillStyle(0);
    pad8_lgnd.SetTextFont(42);
    pad8_lgnd.AddEntry(&temp_sign_2gauss_fit, "Signal",  "l");
    pad8_lgnd.AddEntry(fit_double_gaussian, "Fit",  "l");
    pad8_lgnd.SetBorderSize(0);
    pad8_lgnd.Draw();

    output_canvas.cd(9);
    TH1F ddata_p9_hist(data_hist_vec[comp_index]);
    TH1F dsign_p9_hist(dsign_hist_vec[comp_index]);
    TH1F dbckg_p9_hist(dbckg_hist_vec[comp_index]);
    ddata_p9_hist.Draw("HIST ");
    dsign_p9_hist.Draw("HIST SAME");
    dbckg_p9_hist.Draw("HIST SAME");
    dsign_p9_hist.SetLineColor(2);
    dbckg_p9_hist.SetLineColor(4);
    TLatex pad9_ltx;
    pad9_ltx.SetTextSize(0.03);
    pad9_ltx.DrawLatexNDC(0.2,0.97, "Simulated data with member MC sign and MC bckg, unnormalised");
    TLegend pad9_lgnd(0.225, 0.8, 0.4,  0.925);
    pad9_lgnd.SetTextSize(0.025);
    pad9_lgnd.SetFillStyle(0);
    pad9_lgnd.SetTextFont(42);
    pad9_lgnd.AddEntry(&ddata_p9_hist, "mc data",  "l");
    pad9_lgnd.AddEntry(&dsign_p9_hist, "mc sign",  "l");
    pad9_lgnd.AddEntry(&dbckg_p9_hist, "mc bckg",  "l");
    pad9_lgnd.SetBorderSize(0);
    pad9_lgnd.Draw();
  
    output_canvas.cd(10);
    TH1F ddata_p10_hist(data_hist_vec[comp_index]);
    TH1F dsign_p10_hist(dsign_hist_vec[comp_index]);
    TH1F dbckg_p10_hist(dbckg_hist_vec[comp_index]);
    ddata_p10_hist.Scale(1.0/ddata_p10_hist.Integral()); 
    dsign_p10_hist.Scale(1.0/dsign_p10_hist.Integral());
    dbckg_p10_hist.Scale(1.0/dbckg_p10_hist.Integral());
    ddata_p10_hist.Draw("HIST ");
    dsign_p10_hist.Draw("HIST SAME");
    dbckg_p10_hist.Draw("HIST SAME");
    dsign_p10_hist.SetLineColor(2);
    dbckg_p10_hist.SetLineColor(4);
    TLatex pad10_ltx;
    pad10_ltx.SetTextSize(0.03);
    pad10_ltx.DrawLatexNDC(0.2,0.97, "Simulated data with member MC sign and MC bckg, normalised");
    TLegend pad10_lgnd(0.225, 0.8, 0.4,  0.925);
    pad10_lgnd.SetTextSize(0.025);
    pad10_lgnd.SetFillStyle(0);
    pad10_lgnd.SetTextFont(42);
    pad10_lgnd.AddEntry(&ddata_p10_hist, "mc data",  "l");
    pad10_lgnd.AddEntry(&dsign_p10_hist, "mc sign",  "l");
    pad10_lgnd.AddEntry(&dbckg_p10_hist, "mc bckg",  "l");
    pad10_lgnd.SetBorderSize(0);
    pad10_lgnd.Draw();


    output_canvas.cd(11);
    TH1F comp_esign_p11_temp(comp_sign_hist_vec[comp_index]);
    TH1F comp_dsign_p11_temp(comp_dsign_hist_vec[comp_index]);
    comp_esign_p11_temp.SetLineColor(2);
    comp_dsign_p11_temp.SetLineColor(2);
    comp_esign_p11_temp.SetLineStyle(1);
    comp_dsign_p11_temp.SetLineStyle(2);
    comp_esign_p11_temp.Draw("HIST");
    comp_dsign_p11_temp.Draw("HIST SAME");
    TLatex pad11_ltx;
    pad11_ltx.SetTextSize(0.03);
    pad11_ltx.DrawLatexNDC(0.2,0.97, "Recombined linearity check signal input");
    TLegend pad11_lgnd(0.225, 0.8, 0.4,  0.925);
    pad11_lgnd.SetTextSize(0.025);
    pad11_lgnd.SetFillStyle(0);
    pad11_lgnd.SetTextFont(42);
    pad11_lgnd.AddEntry(&comp_esign_p11_temp, "linearity check input ",  "l");
    pad11_lgnd.AddEntry(&comp_dsign_p11_temp, "linearity check output",  "l");
    pad11_lgnd.SetBorderSize(0);
    pad11_lgnd.Draw();

    output_canvas.cd(12);
    TH1F comp_esign_p12_temp(comp_sign_hist_vec[comp_index]);
    TH1F comp_dsign_p12_temp(comp_dsign_hist_vec[comp_index]);
    comp_esign_p12_temp.SetLineColor(2);
    comp_dsign_p12_temp.SetLineColor(2);
    comp_esign_p12_temp.SetLineStyle(1);
    comp_dsign_p12_temp.SetLineStyle(2);
    comp_esign_p12_temp.Draw("HIST");
    comp_dsign_p12_temp.Draw("HIST SAME");
    TLatex pad12_ltx;
    pad12_ltx.SetTextSize(0.03);
    pad12_ltx.DrawLatexNDC(0.2,0.97, "Recombined linearity check background input");
    TLegend pad12_lgnd(0.225, 0.8, 0.4,  0.925);
    pad12_lgnd.SetTextSize(0.025);
    pad12_lgnd.SetFillStyle(0);
    pad12_lgnd.SetTextFont(42);
    pad12_lgnd.AddEntry(&comp_esign_p12_temp, "linearity check input ",  "l");
    pad12_lgnd.AddEntry(&comp_dsign_p12_temp, "linearity check output",  "l");
    pad12_lgnd.SetBorderSize(0);
    pad12_lgnd.Draw();




    char output_canvas_name[150];
    sprintf(output_canvas_name,"./%s_S-%s_fits_mass%i_%s.png",abin_var_order_vec[comp_index].c_str(),spec_var_order_vec[comp_index].c_str(),mass_index_vec[comp_index],unique.c_str());
    output_canvas.SaveAs(output_canvas_name);
  }

}
