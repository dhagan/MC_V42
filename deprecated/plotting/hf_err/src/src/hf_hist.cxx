#include <hf_hist.hxx>
#include <split_string.hxx>

class hf_hist {

  private:
    std::map<std::string, TH1F*> m_hists;
    std::string m_lin_directory_path;
    std::vector<std::string> m_lin_files;
    bool lin_mode, combination_mode;
    std::vector<TFile> m_files;
    std::vector<std::string> spectator_variables;
    std::vector<std::string> analysis_variables;
    std::string m_hist_access_type;

  public:
    
    hf_hist(bool mode){
      lin_mode = mode;  
    };

    hf_hist(std::string directory_path, std::string lin_files ){
      m_lin_directory_path = directory_path;
      split_strings(m_lin_files,lin_files,":");
      lin_mode = true;
    }

    hf_hist(std::string filepath){
      m_files.push_back(new TFile(filepath.c_str(),"READ")); lin_mode = false;
    };

    void set_file(std::string file){
      if (lin_mode){ std::cout << "In linear mode"  << std::endl; return;}
      m_files.push_back(new TFile(file.c_str(),"READ"));
    }

    void set_directory(std::string directory_path){
      if (!lin_mode){ std::cout << "Not in linear mode, cannot use directory path"  << std::endl; return;}
      else {
        m_lin_directory_path = directory_path;
      }
      return;
    };

    void set_linear_files(std::string lin_files){
      if (!lin_mode){ std::cout << "Not in linear mode, cannot set linear files"  << std::endl; return;}
      split_strings(m_lin_files,lin_files,":");
      return;
    }
  
    void set_linear_mode(bool mode){
      if (mode && lin_mode) return;
      if (!mode && !lin_mode) return;
      if (mode && !lin_mode){
        std::cout << "Switching to linearity mode" << std::endl << "Clearing all histograms, files, etc";
        lin_mode = mode;
        m_files.clear();
        m_hists.clear();
      }
    }

    void read_variable_names(){
      if (m_files.empty()){ std::cout << "No files provided" << std::endl; return;}
      std::string file_name(m_files.at(0).GetName());
      size_t avar_start = file_name.find("_A-")+3;
      size_t svar_start = file_name.find("_S-")+3;
      std::string avar_section(file_name.substr(avar_start,svar_start-avar_start-3));
      std::string end_section(file_name.substr(svar_start));
      std::string svar_section(end_section.substr(0,end_section.find("_")));
      split_string(analysis_variables,avar_section,"-");
      split_string(spectator_variables,svar_section,"-");
    }

    void set_spectator_variables(std::string svar_string){
      split_string(spectator_variables, svar_string)
    }
    void set_analysis_variables( std::string avar_string){
      split_string(analysis_variables, avar_string)
    }

    void set_type( std::string type){
      hist_access_type = type;
    }

    std::map<std::string,std::vector<double>> analysis_bins;
    analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
    analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
    analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
    analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
    analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
    analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};

    std::vector<int> mass_indices = {0,3,4,5,12};

    for (int & mass_index : mass_indices){

      for (std::string abin_var : vec_abin_vars){

        for (std::string spec_var : vec_spec_vars){

          char full_hist_name[100];

        
        }
      }
    }










};
