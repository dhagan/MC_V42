#include <tr.hxx>
#include <split_string.hxx>

void tr( std::string weight, std::string uweight, std::string range, std::string unique ){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  std::vector< std::string > var_vec = { "qtA" };//, "abs(qtB)", "abs(Phi)" };
  
  std::map< std::string, std::vector<double> > bin_map;
  bin_map["qtA"] = { 15, -10, 20 };
  bin_map["abs(qtB)"] = { 10, 0, 20 };
  bin_map["abs(Phi)"] = { 8, 0, M_PI };

  if ( !range.empty() ){
    std::vector<std::string> full_range_vec;
    split_strings(full_range_vec, range, "#" );
    for ( std::string range_str : full_range_vec ){
      std::vector<std::string> var_vec, pairs;
      split_strings(var_vec, range_str,"_");
      split_strings(pairs, var_vec.at(1), ":");
      std::vector<std::string> bounds; 

      if ( bin_map.find( var_vec.at(0) ) != bin_map.end() ){
        for ( std::string pair : pairs ){
          split_strings(bounds, pair, ",");
          std::map< std::string, std::vector<double> >::iterator itr;
          itr = bin_map.find( var_vec.at(0) );
          itr->second = { std::stod( bounds.at(0) ), std::stod( bounds.at(1) ), std::stod( bounds.at(2) ) };
          break;
        }
      }
      std::cout << range << std::endl;
    }
  }
  
  std::map< std::string, std::string > output_names;
  output_names["qtA"] = "qtA";
  output_names["abs(qtB)"] = "qtB";
  output_names["abs(Phi)"] = "Phi";
  
  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["abs(qtB)"]      =  "GeV";
  units["abs(Phi)"]           =  "rad";



  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip;  wip.SetNDC(); wip.SetTextFont(42);
  wip.SetTextSize(0.038); wip.SetTextColor(1);

  TFile * weight_file = new TFile( weight.c_str(), "READ" ); 
  TFile * uweight_file = new TFile( uweight.c_str(), "READ" ); 
  
  std::vector< int > masses = { 0, 3, 4, 5, 12 }; 
  
  for ( int & mass : masses ){
    
    for ( std::string & var : var_vec ){
    
      int bins        = (int) bin_map[var].at(0); 
      double bin_min  = bin_map[var].at(1);
      double bin_max  = bin_map[var].at(2);
      double width =  (double) (bin_max-bin_min)/((double) bins);
      std::string unit = units[var];
      
      TF1 * unit_line = new TF1("unit_line","1",bin_min,bin_max);

      std::string safe_name = output_names[var];
      std::string out_name = output_names[var];

      TH1F * weight_hist  = (TH1F*) weight_file->Get(Form( "eff_%s_Q%i",safe_name.c_str(), mass ));
      TH1F * uweight_hist = (TH1F*) uweight_file->Get(Form( "eff_%s_Q%i", safe_name.c_str(), mass ));

      TH1F * wr_hist = new TH1F( "wr", out_name.c_str(), bins, bin_min, bin_max );
      wr_hist->Divide( weight_hist, uweight_hist, 1.0, 1.0 );

      double * sys_x       = new double[bins];
      double * sys_y       = new double[bins];
      double * err_x_upper = new double[bins];
      double * err_x_lower = new double[bins];
      double * err_y_upper = new double[bins];
      double * err_y_lower = new double[bins];

      for ( int err_bin = 0; err_bin < bins; err_bin++){ 
        sys_x[err_bin] = -9 + width*((double) err_bin);
        sys_y[err_bin] = 1.0;
        err_x_upper[err_bin] = 0.0;
        err_x_lower[err_bin] = 0.0;
        double sys_err = -1.0 + wr_hist->GetBinContent(err_bin+1);
        if ( sys_err > 0 ){
          err_y_lower[err_bin] = 0.0;
          err_y_upper[err_bin] = abs(sys_err);
        } else {
          err_y_lower[err_bin] = abs(sys_err);
          err_y_upper[err_bin] = 0.0;
        }
        //std::cout << sys_err << std::endl;
      }
      TGraphAsymmErrors * err_graph = new TGraphAsymmErrors( bins, sys_x, sys_y, err_x_lower, err_x_upper, err_y_lower,
          err_y_upper);
      err_graph->SetFillStyle(3004);
      err_graph->SetFillColorAlpha(3,1.0);
      err_graph->SetLineWidth(0);
      
      TH1F * wr_hist_split = (TH1F*) wr_hist->Clone();
      TH1F * weight_hist_split = (TH1F*) weight_hist->Clone();
      TH1F * uweight_hist_split = (TH1F*) uweight_hist->Clone();

      TCanvas * canv = new TCanvas("canv","", 100, 100, 2000, 2000 );
      canv->Divide(1,1);
      canv->cd(1);
      double axis_lim = std::ceil( wr_hist->GetMaximum()*1.3 );
      wr_hist->Draw("hist p");
      wr_hist->SetMarkerStyle(21);
      wr_hist->SetLineColor(1);
      wr_hist->GetYaxis()->SetRangeUser(0,axis_lim);
      wr_hist->GetYaxis()->SetTitle( Form("Efficiency Ratio/%.3f %s", width, unit.c_str()));
      wr_hist->GetXaxis()->SetTitle( Form("%s (%s)", var.c_str(), unit.c_str()));
      wr_hist->GetYaxis()->SetLabelSize(0.035);
      wr_hist->GetYaxis()->SetTitleSize(0.035);
      wr_hist->GetXaxis()->SetLabelSize(0.035);
      wr_hist->GetXaxis()->SetTitleSize(0.035);
      gPad->Update();
      gPad->SetTicks(1,0);
      //double pad_scale = weight_hist->GetMaximum();
      double max_ratio = ((double)std::ceil(wr_hist->GetMaximum()))/((double)std::ceil(weight_hist->GetMaximum()));
      double eff_max =  std::ceil(weight_hist->GetMaximum() );
      weight_hist->Scale(  max_ratio );
      uweight_hist->Scale( max_ratio );    
      weight_hist->SetLineWidth(0);
      weight_hist->SetFillColorAlpha(kRed+1,0.5);
      uweight_hist->SetLineWidth(0);
      uweight_hist->SetFillColorAlpha(kBlue+1,0.5);
      weight_hist->Draw("HIST SAME");
      uweight_hist->Draw("HIST SAME");
      unit_line->SetLineColor(kMagenta+1);
      unit_line->SetLineStyle(2);
      unit_line->SetLineWidth(3);
      err_graph->Draw("3 SAME");
      unit_line->Draw("SAME");
      TGaxis * pad_axis = new TGaxis(bin_max,0,bin_max,axis_lim,0, eff_max ,510,"+L");
      pad_axis->SetLabelSize( 0.035 );             
      pad_axis->SetLabelFont( 42 );                
      pad_axis->SetTitleSize( 0.035 );
      pad_axis->SetTitleOffset( 1.25 );
      pad_axis->SetMaxDigits( 4 );
      pad_axis->SetTitleFont( 42 );                
      pad_axis->SetTitle( Form( "Efficiency/%.3f %s", width, unit.c_str() ));
      pad_axis->Draw();
      TLegend * pad_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
      pad_legend->SetBorderSize( 0 );
      pad_legend->SetFillColor( 0 );
      pad_legend->SetFillStyle( 0 );
      pad_legend->SetTextFont( 42 );
      pad_legend->SetTextSize( 0.02 );
      //pad_legend->AddEntry( wr_hist, "Efficiency Ratio", "P");
      pad_legend->AddEntry( err_graph, "Systematic Error", "F");
      pad_legend->AddEntry( weight_hist, "Weighted Efficiency", "F");
      pad_legend->AddEntry( uweight_hist, "Unweighted Effciency", "F");
      pad_legend->AddEntry( unit_line, "Nominal", "L");
      pad_legend->Draw();
      TLatex pad_ltx;
      pad_ltx.SetTextSize( 0.03 );
      pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Ratio - Mass Q%i, weights - %s", safe_name.c_str(), mass,
            unique.c_str() ));  
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

      std::string save_string;
      canv->SaveAs(Form("./%s/%s_eff_ratio_%s_Q%i.png", unique.c_str(), unique.c_str(), out_name.c_str(), mass));
      delete canv;
      


      TCanvas * canv_s = new TCanvas("canv2","", 100, 100, 2000, 1000 );
      canv_s->Divide(2,1);

      canv_s->cd(1);
      uweight_hist_split->Draw("HIST");
      uweight_hist_split->GetYaxis()->SetRangeUser( 0, std::ceil(weight_hist_split->GetMaximum()) );
      uweight_hist_split->SetLineWidth(0);
      uweight_hist_split->SetFillColorAlpha(kRed+1,0.6);
      uweight_hist_split->GetYaxis()->SetTitle( Form("Efficiency/%.3f %s", width, unit.c_str()));
      uweight_hist_split->GetXaxis()->SetTitle( Form("%s (%s)", var.c_str(), unit.c_str()));
      uweight_hist_split->GetYaxis()->SetLabelSize(0.035);
      uweight_hist_split->GetYaxis()->SetTitleSize(0.035);
      uweight_hist_split->GetXaxis()->SetLabelSize(0.035);
      uweight_hist_split->GetXaxis()->SetTitleSize(0.035);
      weight_hist_split->Draw("HIST SAME");
      weight_hist_split->SetLineColor(1);
      weight_hist_split->SetFillStyle(0);
      weight_hist_split->SetLineWidth(3);
      TLegend * pad1s_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
      pad1s_legend->SetBorderSize( 0 );
      pad1s_legend->SetFillColor( 0 );
      pad1s_legend->SetFillStyle( 0 );
      pad1s_legend->SetTextFont( 42 );
      pad1s_legend->SetTextSize( 0.02 );
      //pad1s_legend->AddEntry( err_graph, "Systematic Error", "F");
      pad1s_legend->AddEntry( uweight_hist_split, "Unweighted Efficiency", "LF");
      pad1s_legend->AddEntry( weight_hist_split, "Weighted Efficiency", "L");
      //pad1s_legend->AddEntry( unit_line, "Nominal", "L");
      pad1s_legend->Draw();
      TLatex pad1s_ltx;
      pad1s_ltx.SetTextSize( 0.03 );
      pad1s_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiencies - Mass Q%i, weights - %s", safe_name.c_str(), mass,
            unique.c_str() ));  
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

      canv_s->cd(2);
      wr_hist_split->Draw("HIST P");
      wr_hist_split->SetMarkerStyle(21);
      wr_hist_split->SetLineColor(1);
      wr_hist_split->GetYaxis()->SetRangeUser(0,std::ceil( wr_hist_split->GetMaximum() ));
      wr_hist_split->GetYaxis()->SetTitle( Form("Efficiency Ratio/%.3f %s", width, unit.c_str()));
      wr_hist_split->GetXaxis()->SetTitle( Form("%s (%s)", var.c_str(), unit.c_str()));
      wr_hist_split->GetYaxis()->SetLabelSize(0.035);
      wr_hist_split->GetYaxis()->SetTitleSize(0.035);
      wr_hist_split->GetXaxis()->SetLabelSize(0.035);
      wr_hist_split->GetXaxis()->SetTitleSize(0.035);
      err_graph->SetFillColorAlpha(kBlue+1,0.6);
      err_graph->SetFillStyle(1);
      err_graph->Draw("3 SAME");
      TLegend * pad2s_legend = new TLegend( 0.5, 0.6, 0.83, 0.78 );
      pad2s_legend->SetBorderSize( 0 );
      pad2s_legend->SetFillColor( 0 );
      pad2s_legend->SetFillStyle( 0 );
      pad2s_legend->SetTextFont( 42 );
      pad2s_legend->SetTextSize( 0.02 );
      pad2s_legend->AddEntry( wr_hist_split, "Efficiency ratio", "P");
      //pad2s_legend->AddEntry( err_graph, "Systematic Error", "F");
      pad2s_legend->Draw();
      TLatex pad2s_ltx;
      pad2s_ltx.SetTextSize( 0.03 );
      pad2s_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Efficiency Ratio - Mass Q%i, weights - %s", safe_name.c_str(), mass,
            unique.c_str() ));  
      ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
      wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

      canv_s->SaveAs(Form("./%s/%s_eff_ratio_split_%s_Q%i.png", unique.c_str(), unique.c_str(), out_name.c_str(), mass));
      delete canv_s;
      delete wr_hist;
      delete wr_hist_split;

    }
  }
}



