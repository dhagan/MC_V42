#include <tr.hxx>


int help(){
  std::cout << " i can't help you dude i can barely help myself" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

 int option{0}, option_index{0};
 std::string mode, weight, uweight, range, unique;

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"weight",        required_argument,        0,    'w'},
      {"uweight",        required_argument,        0,   'u'},
      {"mode",        required_argument,        0,      'm'},
      {"unique",        required_argument,        0,      'q'},
      {"range",        required_argument,        0,      'r'},
      {0,             0,                        0,      0}
  };

  do {
    option = getopt_long( argc, argv, "w:u:m:q:r:h", long_options, &option_index);
    switch (option){
      case 'h':
        return help();
      case 'm': 
        mode      = std::string( optarg );
        break;
      case 'w': 
        weight      = std::string( optarg );
        break;
      case 'u': 
        uweight      = std::string( optarg );
        break;
      case 'q': 
        unique      = std::string( optarg );
        break;
      case 'r': 
        range      = std::string( optarg );
        break;
    }
  } while ( option != -1 );

  if ( mode.find( "tree" ) != std::string::npos ){
    std::cout << unique << std::endl;
    tr( weight, uweight, range, unique );
  }

}
