#include "plot.hxx"

void plot(int momentum_mode = 0, int validation_mode = 0){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);

  int momentum_bins;
  double momentum_min, momentum_max;
  if (momentum_mode == 0){ momentum_bins = 10; momentum_min = -5.0; momentum_max = 15.0; }
  if (momentum_mode == 1){ momentum_bins = 15; momentum_min = 0.0; momentum_max = 15.0; }
  char variable[30];
  sprintf(variable,"%s",(momentum_mode == 0) ? "qTAwide" : "qTBwide");

  const char * momentum_mode_str = (momentum_mode == 0) ? "qta" : "qtb"; 
  const char * train_mode_str = (train_type == 0) ? "ll" : "reg"; 
  const char * train_mode_str2 = (train_type == 0) ? "llt" : "reg"; 

  std::string data_type, sign_type, bckg_type;
  if ( validation_mode == 0){
    data_type = "data";
    sign_type = "signal";
    bckg_type = "pp";
  }

  char sub_llt_data_filename[200];
  char sub_llt_sign_filename[200];
  char sub_llt_bckg_filename[200];
  char sub_reg_data_filename[200];
  char sub_reg_sign_filename[200];
  char sub_reg_bckg_filename[200];
  sprintf(sub_llt_data_filename,"/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions/Subtracted_ll_%s_%s_V0.root",data_type.c_str(),momentum_mode_str);
  sprintf(sub_llt_sign_filename,"/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions/Subtracted_ll_%s_%s_V0.root",sign_type.c_str(),momentum_mode_str);
  sprintf(sub_llt_bckg_filename,"/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions/Subtracted_ll_%s_%s_V0.root",bckg_type.c_str(),momentum_mode_str);
  sprintf(sub_reg_data_filename,"/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions/Subtracted_reg_%s_%s_V0.root",data_type.c_str(),momentum_mode_str);
  sprintf(sub_reg_sign_filename,"/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions/Subtracted_reg_%s_%s_V0.root",sign_type.c_str(),momentum_mode_str);
  sprintf(sub_reg_bckg_filename,"/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/subtractions/Subtracted_reg_%s_%s_V0.root",bckg_type.c_str(),momentum_mode_str);


  // create fit objects
  TF1 * fit_single_gaussian = new TF1("fit_double_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2))" ); 
  fit_single_gaussian->SetParLimits(1,momentum_min,momentum_max); fit_single_gaussian->SetParLimits(2,0,10); 
  fit_single_gaussian->SetParName(0,"C"); fit_single_gaussian->SetParName(1,"X"); fit_single_gaussian->SetParName(2,"Sigma");
  
  TF1 * fit_double_gaussian = new TF1("fit_double_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) + [3]*exp( -((x-[4])^2)/(2*[5]^2)) " );
  fit_double_gaussian->SetParLimits(1,momentum_min,momentum_max); fit_double_gaussian->SetParLimits(2,0,4);
  fit_double_gaussian->SetParLimits(4,momentum_min,momentum_max); fit_double_gaussian->SetParLimits(5,0,10);
  fit_double_gaussian->SetParName(0,"C1"); fit_double_gaussian->SetParName(1,"X1"); fit_double_gaussian->SetParName(2,"sig1");
  fit_double_gaussian->SetParName(3,"C2"); fit_double_gaussian->SetParName(4,"X2"); fit_double_gaussian->SetParName(5,"sig2");

  char integral_reg_file_str[200];
  char integral_llt_file_str[200];
  sprintf(integral_reg_file_str,"/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/run/run_ll/%s_reg_dt-data_integral-store.root",momentum_mode_str);
  sprintf(integral_llt_file_str,"/home/atlas/dhagan/analysis/analysisFork/445/HistFactory/run/run_ll/%s_llt_dt-data_integral-store.root",momentum_mode_str);
  TFile * integral_reg_file = new TFile(integral_reg_file_str,"READ");
  TFile * integral_llt_file = new TFile(integral_llt_file_str,"READ");

  TFile * sub_reg_data_file = new TFile(sub_reg_data_filename,"READ");
  TFile * sub_reg_sign_file = new TFile(sub_reg_sign_filename,"READ");
  TFile * sub_reg_bckg_file = new TFile(sub_reg_bckg_filename,"READ");
  TFile * sub_llt_data_file = new TFile(sub_llt_data_filename,"READ");
  TFile * sub_llt_sign_file = new TFile(sub_llt_sign_filename,"READ");
  TFile * sub_llt_bckg_file = new TFile(sub_llt_bckg_filename,"READ");


  std::vector<int> mass_indices = {0,3,4,5,12};
  
  std::vector <TH1F> data_reg_hist_vec;
  std::vector <TH1F> sign_reg_hist_vec;
  std::vector <TH1F> bckg_reg_hist_vec;
  std::vector <TH1F> data_llt_hist_vec;
  std::vector <TH1F> sign_llt_hist_vec;
  std::vector <TH1F> bckg_llt_hist_vec;

  std::vector <TH1F> data_reg_sub_hist_vec;
  std::vector <TH1F> sign_reg_sub_hist_vec;
  std::vector <TH1F> bckg_reg_sub_hist_vec;
  std::vector <TH1F> data_llt_sub_hist_vec;
  std::vector <TH1F> sign_llt_sub_hist_vec;
  std::vector <TH1F> bckg_llt_sub_hist_vec;


  for (const int& mass_index : mass_indices){

    char data_mass_name[20], sign_mass_name[20], bckg_mass_name[20],mass_char[20];
    char data_sub_mass_name[20], sign_sub_mass_name[20], bckg_sub_mass_name[20];
    sprintf(data_mass_name,"data_mass%i",mass_index);
    sprintf(sign_mass_name,"sign_mass%i",mass_index);
    sprintf(bckg_mass_name,"bckg_mass%i",mass_index);
    sprintf(data_sub_mass_name,"data_sub_mass%i",mass_index);
    sprintf(sign_sub_mass_name,"sign_sub_mass%i",mass_index);
    sprintf(bckg_sub_mass_name,"bckg_sub_mass%i",mass_index);
    sprintf(mass_char,"bothTheta-mass%i",mass_index);

    TH1F data_reg_mass_hist(data_mass_name,data_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F sign_reg_mass_hist(sign_mass_name,sign_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F bckg_reg_mass_hist(bckg_mass_name,bckg_mass_name,momentum_bins,momentum_min,momentum_max);

    TH1F data_llt_sub_mass_hist(data_sub_mass_name,data_sub_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F sign_llt_sub_mass_hist(sign_sub_mass_name,sign_sub_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F bckg_llt_sub_mass_hist(bckg_sub_mass_name,bckg_sub_mass_name,momentum_bins,momentum_min,momentum_max);
  
    TH1F data_reg_mass_hist(data_mass_name,data_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F sign_reg_mass_hist(sign_mass_name,sign_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F bckg_reg_mass_hist(bckg_mass_name,bckg_mass_name,momentum_bins,momentum_min,momentum_max);

    TH1F data_llt_sub_mass_hist(data_sub_mass_name,data_sub_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F sign_llt_sub_mass_hist(sign_sub_mass_name,sign_sub_mass_name,momentum_bins,momentum_min,momentum_max);
    TH1F bckg_llt_sub_mass_hist(bckg_sub_mass_name,bckg_sub_mass_name,momentum_bins,momentum_min,momentum_max);

    for (int momentum_index = 1; momentum_index <= momentum_bins; momentum_index++){

      char data_hist_name[100], sign_hist_name[100],bckg_hist_name[100];
      char data_sub_name[100], sign_sub_name[100],bckg_sub_name[100];
      char error_name[100];
      sprintf(data_hist_name,"BDT_%s_99_pos_%s_%s-%i_phiCS-0",data_type.c_str(),mass_char,variable,momentum_index);
      sprintf(sign_hist_name,"BDT_%s_99_pos_%s_%s-%i_phiCS-0",sign_type.c_str(),mass_char,variable,momentum_index);
      sprintf(bckg_hist_name,"BDT_%s_99_pos_%s_%s-%i_phiCS-0",bckg_type.c_str(),mass_char,variable,momentum_index);
      sprintf(error_name,"BDT_error_99_pos_%s_%s-%i_phiCS-0",mass_char,variable,momentum_index);
      sprintf(data_sub_name,"BDT_%s_99_pos_%s_%s-%i_phiCS-0",data_type.c_str(),mass_char,variable,momentum_index);
      sprintf(sign_sub_name,"BDT_%s_99_pos_%s_%s-%i_phiCS-0",sign_type.c_str(),mass_char,variable,momentum_index);
      sprintf(bckg_sub_name,"BDT_%s_99_pos_%s_%s-%i_phiCS-0",bckg_type.c_str(),mass_char,variable,momentum_index);

      TH1F * data_reg_integral_hist = (TH1F*) integral_reg_file->Get(data_hist_name);
      TH1F * sign_reg_integral_hist = (TH1F*) integral_reg_file->Get(sign_hist_name);
      TH1F * bckg_reg_integral_hist = (TH1F*) integral_reg_file->Get(bckg_hist_name);
      TH1F * error_reg_hist = (TH1F*) integral_reg_file->Get(error_name);
      
      TH1F * data_llt_integral_hist = (TH1F*) integral_llt_file->Get(data_hist_name);
      TH1F * sign_llt_integral_hist = (TH1F*) integral_llt_file->Get(sign_hist_name);
      TH1F * bckg_llt_integral_hist = (TH1F*) integral_llt_file->Get(bckg_hist_name);
      TH1F * error_llt_hist = (TH1F*) integral_llt_file->Get(error_name);

      TH1F * data_reg_sub_integral_hist = (TH1F*) sub_reg_data_file->Get(data_sub_name);
      TH1F * sign_reg_sub_integral_hist = (TH1F*) sub_reg_sign_file->Get(sign_sub_name);
      TH1F * bckg_reg_sub_integral_hist = (TH1F*) sub_reg_bckg_file->Get(bckg_sub_name);
  
      TH1F * data_llt_sub_integral_hist = (TH1F*) sub_llt_data_file->Get(data_sub_name);
      TH1F * sign_llt_sub_integral_hist = (TH1F*) sub_llt_sign_file->Get(sign_sub_name);
      TH1F * bckg_llt_sub_integral_hist = (TH1F*) sub_llt_bckg_file->Get(bckg_sub_name);

      
      data_reg_mass_hist.SetBinContent(momentum_index,data_reg_integral_hist->Integral()); 
      data_reg_mass_hist.SetBinError(momentum_index,  error_reg_hist->GetBinError(1)); 
      sign_reg_mass_hist.SetBinContent(momentum_index,sign_reg_integral_hist->Integral()); 
      sign_reg_mass_hist.SetBinError(momentum_index,  error_reg_hist->GetBinError(2)); 
      bckg_reg_mass_hist.SetBinContent(momentum_index,bckg_reg_integral_hist->Integral()); 
      bckg_reg_mass_hist.SetBinError(momentum_index,  error_reg_hist->GetBinError(3)); 
      
      data_llt_mass_hist.SetBinContent(momentum_index,data_llt_integral_hist->Integral()); 
      data_llt_mass_hist.SetBinError(momentum_index,  error_llt_hist->GetBinError(1)); 
      sign_llt_mass_hist.SetBinContent(momentum_index,sign_llt_integral_hist->Integral()); 
      sign_llt_mass_hist.SetBinError(momentum_index,  error_llt_hist->GetBinError(2)); 
      bckg_llt_mass_hist.SetBinContent(momentum_index,bckg_llt_integral_hist->Integral()); 
      bckg_llt_mass_hist.SetBinError(momentum_index,  error_llt_hist->GetBinError(3)); 

      data_reg_sub_mass_hist.SetBinContent(momentum_index,data_reg_sub_integral_hist->Integral()); 
      sign_reg_sub_mass_hist.SetBinContent(momentum_index,sign_reg_sub_integral_hist->Integral()); 
      bckg_reg_sub_mass_hist.SetBinContent(momentum_index,bckg_reg_sub_integral_hist->Integral()); 
      
      data_llt_sub_mass_hist.SetBinContent(momentum_index,data_llt_sub_integral_hist->Integral()); 
      sign_llt_sub_mass_hist.SetBinContent(momentum_index,sign_llt_sub_integral_hist->Integral()); 
      bckg_llt_sub_mass_hist.SetBinContent(momentum_index,bckg_llt_sub_integral_hist->Integral()); 


      delete data_reg_integral_hist;
      delete sign_reg_integral_hist;
      delete bckg_reg_integral_hist;
      delete error_llt_integral_hist;

      delete data_llt_integral_hist;
      delete sign_llt_integral_hist;
      delete bckg_llt_integral_hist;
      delete error_llt_integral_hist;

      delete data_reg_sub_integral_hist;
      delete sign_reg_sub_integral_hist;
      delete bckg_reg_sub_integral_hist;

      delete data_llt_sub_integral_hist;
      delete sign_llt_sub_integral_hist;
      delete bckg_llt_sub_integral_hist;

    }

    data_reg_hist_vec.push_back(data_reg_mass_hist);
    sign_reg_hist_vec.push_back(sign_reg_mass_hist);
    bckg_reg_hist_vec.push_back(bckg_reg_mass_hist);

    data_llt_hist_vec.push_back(data_llt_mass_hist);
    sign_llt_hist_vec.push_back(sign_llt_mass_hist);
    bckg_llt_hist_vec.push_back(bckg_llt_mass_hist);

    data_reg_sub_hist_vec.push_back(data_reg_sub_mass_hist);
    sign_reg_sub_hist_vec.push_back(sign_reg_sub_mass_hist);
    bckg_reg_sub_hist_vec.push_back(bckg_reg_sub_mass_hist);

    data_llt_sub_hist_vec.push_back(data_llt_sub_mass_hist);
    sign_llt_sub_hist_vec.push_back(sign_llt_sub_mass_hist);
    bckg_llt_sub_hist_vec.push_back(bckg_llt_sub_mass_hist);

  }


  for (int mass_index = 0; mass_index <=4; mass_index++){
    
    char canv_name[30];
    sprintf(canv_name,"canv%i",mass_index);

    TCanvas output_canvas(canv_name,canv_name,600,600,8000,2000);
    output_canvas.Divide(4,2);

    char mass_name[20];
    sprintf(mass_name,"mass%i",mass_indices[mass_index]);

    std::vector<double> maxs;
    output_canvas.cd(1);
    data_reg_hist_vec[mass_index].Draw();
    sign_reg_hist_vec[mass_index].Draw("HIST SAME");
    bckg_reg_hist_vec[mass_index].Draw("HIST SAME");
    data_llt_hist_vec[mass_index].Draw("HIST SAME);
    sign_llt_hist_vec[mass_index].Draw("HIST SAME");
    bckg_llt_hist_vec[mass_index].Draw("HIST SAME");
    data_reg_hist_vec[mass_index].SetLineColor(1);
    sign_reg_hist_vec[mass_index].SetLineColor(2);
    bckg_reg_hist_vec[mass_index].SetLineColor(4);
    data_llt_hist_vec[mass_index].SetLineColor(1);
    sign_llt_hist_vec[mass_index].SetLineColor(2);
    bckg_llt_hist_vec[mass_index].SetLineColor(4);
    data_llt_hist_vec[mass_index].SetLineStyle(2);
    sign_llt_hist_vec[mass_index].SetLineStyle(2);
    bckg_llt_hist_vec[mass_index].SetLineStyle(2);
    maxs.push_back(data_reg_hist_vec[mass_index].GetMaximum());
    maxs.push_back(sign_reg_hist_vec[mass_index].GetMaximum());
    maxs.push_back(bckg_reg_hist_vec[mass_index].GetMaximum());
    maxs.push_back(data_llt_hist_vec[mass_index].GetMaximum());
    maxs.push_back(sign_llt_hist_vec[mass_index].GetMaximum());
    maxs.push_back(bckg_llt_hist_vec[mass_index].GetMaximum());
    data_reg_hist_vec[mass_index].GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad1_ltx;
    pad1_ltx.SetTextSize(0.05);
    pad1_ltx.DrawLatexNDC(0.0,0.96,mass_name);
    pad1_ltx.SetTextSize(0.03);
    pad1_ltx.DrawLatexNDC(0.2,0.97,  "After Histfactory");
    pad1_ltx.DrawLatexNDC(0.25,0.89, Form("n_{Data reg} = %.0f",data_reg_hist_vec[mass_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.86, Form("n_{SignalEx reg} = %.0f",sign_reg_hist_vec[mass_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.83, Form("n_{BckgEx reg} = %.0f",bckg_reg_hist_vec[mass_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.80, Form("n_{Data llt} = %.0f",data_llt_hist_vec[mass_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.77, Form("n_{SignalEx llt} = %.0f",sign_llt_hist_vec[mass_index].Integral()));
    pad1_ltx.DrawLatexNDC(0.25,0.74, Form("n_{BckgEx llt} = %.0f",bckg_llt_hist_vec[mass_index].Integral()));
    TLegend pad1_lgnd(0.8, 0.8, 0.925,  0.925);
    pad1_lgnd.SetTextSize(0.025);
    pad1_lgnd.SetFillStyle(0);
    pad1_lgnd.SetTextFont(42);
    pad1_lgnd.AddEntry(&data_reg_hist_vec[mass_index], "Data reg", "lep");
    pad1_lgnd.AddEntry(&sign_reg_hist_vec[mass_index], "Signal reg",  "l");
    pad1_lgnd.AddEntry(&bckg_reg_hist_vec[mass_index], "Bckg reg",  "l  ");
    pad1_lgnd.AddEntry(&data_llt_hist_vec[mass_index], "Data llt", "lep");
    pad1_lgnd.AddEntry(&sign_llt_hist_vec[mass_index], "Signal llt",  "l");
    pad1_lgnd.AddEntry(&bckg_llt_hist_vec[mass_index], "Bckg llt",  "l  ");
    pad1_lgnd.SetBorderSize(0);
    pad1_lgnd.Draw();

    output_canvas.cd(2);
    data_reg_sub_hist_vec[mass_index].Draw();
    sign_reg_sub_hist_vec[mass_index].Draw("HIST SAME");
    bckg_reg_sub_hist_vec[mass_index].Draw("HIST SAME");
    data_reg_sub_hist_vec[mass_index].SetLineColor(1);
    sign_reg_sub_hist_vec[mass_index].SetLineColor(2);
    bckg_reg_sub_hist_vec[mass_index].SetLineColor(4);
    data_llt_sub_hist_vec[mass_index].Draw("HIST SAME");
    sign_llt_sub_hist_vec[mass_index].Draw("HIST SAME");
    bckg_llt_sub_hist_vec[mass_index].Draw("HIST SAME");
    data_llt_sub_hist_vec[mass_index].SetLineColor(1);
    sign_llt_sub_hist_vec[mass_index].SetLineColor(2);
    bckg_llt_sub_hist_vec[mass_index].SetLineColor(4);
    data_llt_sub_hist_vec[mass_index].SetLineStyle(2);
    sign_llt_sub_hist_vec[mass_index].SetLineStyle(2);
    bckg_llt_sub_hist_vec[mass_index].SetLineStyle(2);

    maxs.push_back(data_reg_sub_hist_vec[mass_index].GetMaximum());
    maxs.push_back(sign_reg_sub_hist_vec[mass_index].GetMaximum());
    maxs.push_back(bckg_reg_sub_hist_vec[mass_index].GetMaximum());
    maxs.push_back(data_llt_sub_hist_vec[mass_index].GetMaximum());
    maxs.push_back(sign_llt_sub_hist_vec[mass_index].GetMaximum());
    maxs.push_back(bckg_llt_sub_hist_vec[mass_index].GetMaximum());
    data_reg_sub_hist_vec[mass_index].GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad2_ltx;
    pad2_ltx.SetTextSize(0.03);
    pad2_ltx.DrawLatexNDC(0.2,0.97, "Before HistFactory");
    TLegend pad2_lgnd(0.8, 0.8, 0.925,  0.925);
    pad2_lgnd.SetTextSize(0.025);
    pad2_lgnd.SetFillStyle(0);
    pad2_lgnd.SetTextFont(42);
    pad2_lgnd.AddEntry(&data_reg_sub_hist_vec[mass_index], "Data reg", "lep");
    pad2_lgnd.AddEntry(&sign_reg_sub_hist_vec[mass_index], "Signal reg",  "l");
    pad2_lgnd.AddEntry(&bckg_reg_sub_hist_vec[mass_index], "Bckg reg",  "l  ");
    pad2_lgnd.AddEntry(&data_llt_sub_hist_vec[mass_index], "Data llt", "lep");
    pad2_lgnd.AddEntry(&sign_llt_sub_hist_vec[mass_index], "Signal llt",  "l");
    pad2_lgnd.AddEntry(&bckg_llt_sub_hist_vec[mass_index], "Bckg llt",  "l  ");

    pad2_lgnd.SetBorderSize(0);
    pad2_lgnd.Draw();

    output_canvas.cd(3);
    TH1F sign_reg_overlay_temp(sign_hist_vec[mass_index]);
    TH1F sign_reg_sub_overlay_temp(sign_sub_hist_vec[mass_index]);
    TH1F sign_llt_overlay_temp(sign_hist_vec[mass_index]);
    TH1F sign_llt_sub_overlay_temp(sign_sub_hist_vec[mass_index]);

    sign_reg_overlay_temp.Scale(1.0/sign_reg_overlay_temp.Integral());
    sign_reg_sub_overlay_temp.Scale(1.0/sign_reg_sub_overlay_temp.Integral());
    sign_llt_overlay_temp.Scale(1.0/sign_llt_overlay_temp.Integral());
    sign_llt_sub_overlay_temp.Scale(1.0/sign_llt_sub_overlay_temp.Integral());

    sign_overlay_temp.Draw("HIST");
    sign_sub_overlay_temp.Draw("HIST SAME");

    sign_sub_overlay_temp.SetLineColor(2);
    sign_sub_overlay_temp.SetLineStyle(1);
    sign_overlay_temp.SetLineColor(2);
    sign_overlay_temp.SetLineStyle(1);

    maxs.push_back(sign_overlay_temp.GetMaximum());
    maxs.push_back(sign_sub_overlay_temp.GetMaximum());
    sign_overlay_temp.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad3_ltx;
    pad3_ltx.SetTextSize(0.03);
    pad3_ltx.DrawLatexNDC(0.2,0.97, "Signal, normalised, Overlain");
    TLegend pad3_lgnd(0.65, 0.8, 0.925,  0.925);
    pad3_lgnd.SetTextSize(0.025);
    pad3_lgnd.SetFillStyle(0);
    pad3_lgnd.SetTextFont(42);
    pad3_lgnd.AddEntry(&sign_sub_overlay_temp, "Subtraction Signal", "lep");
    pad3_lgnd.AddEntry(&sign_overlay_temp, "Extracted Signal",  "l");
    pad3_lgnd.SetBorderSize(0);
    pad3_lgnd.Draw();


    output_canvas.cd(4);
    TH1F temp_sign_1gauss_fit(sign_hist_vec[mass_index]);
    temp_sign_1gauss_fit.Draw("E1");
    temp_sign_1gauss_fit.SetLineColor(2);
    fit_single_gaussian->SetParameter(0,2300);
    fit_single_gaussian->SetParameter(1,3);
    fit_single_gaussian->SetParameter(2,3);
    temp_sign_1gauss_fit.Fit(fit_single_gaussian,"MQ","");
    fit_single_gaussian->Draw("SAME");
    fit_single_gaussian->SetLineColor(1);
    temp_sign_1gauss_fit.Draw("HIST SAME");
    gPad->Modified(); gPad->Update();
    TPaveStats * pad4_stats = (TPaveStats*) temp_sign_1gauss_fit.FindObject("stats");
    pad4_stats->SetX1NDC(0.7); pad4_stats->SetX2NDC(0.925);
    pad4_stats->SetY1NDC(0.7); pad4_stats->SetY2NDC(0.925);
    pad4_stats->SetFillStyle(0);
    temp_sign_1gauss_fit.GetXaxis()->SetTitle("qTA (GeV)");
    temp_sign_1gauss_fit.GetYaxis()->SetTitle("Signal Yield/2GeV");
    TLatex pad4_ltx;
    pad4_ltx.SetTextSize(0.03);
    pad4_ltx.DrawLatexNDC(0.2,0.97, "Single Gaussian Fit");
    //TLegend pad4_lgnd(0.225, 0.8, 0.4,  0.925);
    //pad4_lgnd.SetTextSize(0.025);
    //pad4_lgnd.SetFillStyle(0);
    //pad4_lgnd.SetTextFont(42);
    //pad4_lgnd.AddEntry(&temp_sign_1gauss_fit, "Signal",  "l");
    //pad4_lgnd.AddEntry(fit_double_gaussian, "Fit",  "l");
    //pad4_lgnd.SetBorderSize(0);
    //pad4_lgnd.Draw();
    TLatex qta_2_ATLAS;
    qta_2_ATLAS.SetNDC();
    qta_2_ATLAS.SetTextFont(72);
    qta_2_ATLAS.SetTextColor(1);
    qta_2_ATLAS.DrawLatex(0.225,0.89,"ATLAS");
    TLatex qta_2_wip; 
    qta_2_wip.SetNDC();
    qta_2_wip.SetTextFont(42);
    qta_2_wip.SetTextSize(0.038);
    qta_2_wip.SetTextColor(1);
    qta_2_wip.DrawLatex(0.225,0.85,"Work In Progress");



    output_canvas.cd(5);
    TH1F temp_data(data_hist_vec[mass_index]);
    TH1F temp_sign(sign_hist_vec[mass_index]);
    TH1F temp_bckg(bckg_hist_vec[mass_index]);
    temp_data.Scale(1.0/temp_data.Integral());
    temp_sign.Scale(1.0/temp_sign.Integral());
    temp_bckg.Scale(1.0/temp_bckg.Integral());
    temp_data.Draw();
    temp_sign.Draw("HIST SAME");
    temp_bckg.Draw("HIST SAME");
    maxs.push_back(temp_data.GetMaximum());
    maxs.push_back(temp_sign.GetMaximum());
    maxs.push_back(temp_bckg.GetMaximum());
    temp_data.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad5_ltx;
    pad5_ltx.SetTextSize(0.03);
    pad5_ltx.DrawLatexNDC(0.2,0.97, "Normalised by Integral");
    TLegend pad5_lgnd(0.8, 0.8, 0.925,  0.925);
    pad5_lgnd.SetTextSize(0.025);
    pad5_lgnd.SetFillStyle(0);
    pad5_lgnd.SetTextFont(42);
    pad5_lgnd.AddEntry(&temp_data, "Data", "lep");
    pad5_lgnd.AddEntry(&temp_sign, "Signal",  "l");
    pad5_lgnd.AddEntry(&temp_bckg, "Bckg",  "l  ");
    pad5_lgnd.SetBorderSize(0);
    pad5_lgnd.Draw();

    output_canvas.cd(6);
    TH1F temp_sub_data(data_sub_hist_vec[mass_index]);
    TH1F temp_sub_sign(sign_sub_hist_vec[mass_index]);
    TH1F temp_sub_bckg(bckg_sub_hist_vec[mass_index]);
    temp_sub_data.Scale(1.0/temp_sub_data.Integral());
    temp_sub_sign.Scale(1.0/temp_sub_sign.Integral());
    temp_sub_bckg.Scale(1.0/temp_sub_bckg.Integral());
    temp_sub_data.Draw();
    temp_sub_sign.Draw("HIST SAME");
    temp_sub_bckg.Draw("HIST SAME");
    maxs.push_back(temp_sub_data.GetMaximum());
    maxs.push_back(temp_sub_sign.GetMaximum());
    maxs.push_back(temp_sub_bckg.GetMaximum());
    temp_sub_data.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad6_ltx;
    pad6_ltx.SetTextSize(0.03);
    pad6_ltx.DrawLatexNDC(0.2,0.97, "Before HistFactory, Normalised by Integral");
    TLegend pad6_lgnd(0.8, 0.8, 0.925,  0.925);
    pad6_lgnd.SetTextSize(0.025);
    pad6_lgnd.SetFillStyle(0);
    pad6_lgnd.SetTextFont(42);
    pad6_lgnd.AddEntry(&temp_sub_data, "Data", "lep");
    pad6_lgnd.AddEntry(&temp_sub_sign, "Signal",  "l");
    pad6_lgnd.AddEntry(&temp_sub_bckg, "Bckg",  "l  ");
    pad6_lgnd.SetBorderSize(0);
    pad6_lgnd.Draw();


    output_canvas.cd(7); // normalised background before and after
    TH1F bckg_overlay_temp(bckg_hist_vec[mass_index]);
    TH1F bckg_sub_overlay_temp(bckg_sub_hist_vec[mass_index]);
    bckg_overlay_temp.Scale(1.0/bckg_overlay_temp.Integral());
    bckg_sub_overlay_temp.Scale(1.0/bckg_sub_overlay_temp.Integral());
    bckg_overlay_temp.Draw("HIST");
    bckg_sub_overlay_temp.Draw("HIST SAME");
    bckg_overlay_temp.SetLineColor(4);
    bckg_overlay_temp.SetLineStyle(1);
    bckg_sub_overlay_temp.SetLineColor(4);
    bckg_sub_overlay_temp.SetLineStyle(2);
    maxs.push_back(bckg_overlay_temp.GetMaximum());
    maxs.push_back(bckg_sub_overlay_temp.GetMaximum());
    bckg_overlay_temp.GetYaxis()->SetRangeUser(0,1.3*(*std::max_element(maxs.begin(),maxs.end())));
    maxs.clear();
    TLatex pad7_ltx;
    pad7_ltx.SetTextSize(0.03);
    pad7_ltx.DrawLatexNDC(0.2,0.97, "Background (pp), normalised, Overlain");
    TLegend pad7_lgnd(0.6, 0.8, 0.925,  0.925);
    pad7_lgnd.SetTextSize(0.025);
    pad7_lgnd.SetFillStyle(0);
    pad7_lgnd.SetTextFont(42);
    pad7_lgnd.AddEntry(&bckg_sub_overlay_temp, "Subtraction Background", "lep");
    pad7_lgnd.AddEntry(&bckg_overlay_temp, "Extracted Background",  "l");
    pad7_lgnd.SetBorderSize(0);
    pad7_lgnd.Draw();


    output_canvas.cd(8);
    TH1F temp_sign_2gauss_fit(sign_hist_vec[mass_index]);
    temp_sign_2gauss_fit.Draw();
    temp_sign_2gauss_fit.SetLineColor(2);
    temp_sign_2gauss_fit.Fit(fit_double_gaussian,"MQ","",momentum_min,momentum_max);
    fit_double_gaussian->Draw("SAME");
    fit_double_gaussian->SetLineColor(1);
    temp_sign_2gauss_fit.Draw("HIST SAME");
    gPad->Modified(); gPad->Update();
    TPaveStats * pad8_stats = (TPaveStats*) temp_sign_2gauss_fit.FindObject("stats");
    pad8_stats->SetX1NDC(0.7); pad8_stats->SetX2NDC(0.925);
    pad8_stats->SetY1NDC(0.7); pad8_stats->SetY2NDC(0.925);
    pad8_stats->SetFillStyle(0);
    TLatex pad8_ltx;
    pad8_ltx.SetTextSize(0.03);
    pad8_ltx.DrawLatexNDC(0.2,0.97, "Double Gaussian Fit");
    TLegend pad8_lgnd(0.225, 0.8, 0.4,  0.925);
    pad8_lgnd.SetTextSize(0.025);
    pad8_lgnd.SetFillStyle(0);
    pad8_lgnd.SetTextFont(42);
    pad8_lgnd.AddEntry(&temp_sign_2gauss_fit, "Signal",  "l");
    pad8_lgnd.AddEntry(fit_double_gaussian, "Fit",  "l");
    pad8_lgnd.SetBorderSize(0);
    pad8_lgnd.Draw();

    char output_canvas_name[50];
    if (validation_mode == 0){
      sprintf(output_canvas_name,"./%s_%s_fits_mass%i.png",momentum_mode_str,train_mode_str2,mass_indices[mass_index]);
    } else if (validation_mode > 0){
      sprintf(output_canvas_name,"./%s_%s_val-%s_fits_mass%i.png",momentum_mode_str,train_mode_str2,data_type.c_str(),mass_indices[mass_index]);
    }
    output_canvas.SaveAs(output_canvas_name);
    delete output_canvas;

  }

}

int main(int argc, char * argv[]){
  plot(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
}
