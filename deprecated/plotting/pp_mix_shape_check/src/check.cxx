
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  std::vector<std::string> tree_variables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};
  std::vector<double> low_edges{-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};
  std::vector<double> high_edges{M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};

  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};
  std::vector<int> caps = {0,3,5,7,10};

  for (int cap_idx = 0; cap_idx < 5; cap_idx++){

    int current_cap = caps[cap_idx];
    TFile mix_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/pp-mix_bound-0/cap_%i/pp-mix_cap%i_00_mu4000_P5000_bound-0.root",current_cap,current_cap),"READ"};
    TFile pp_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/pp_bound-0/split0/pp_00_mu4000_P5000_bound-0.root","READ"};


    for ( int mass_range = 0; mass_range<mass_cuts.size(); mass_range++){ 

      
      TTree * pp_tree =  (TTree*) pp_file.Get("TreeD");
      TTree * mix_tree = (TTree*) mix_file.Get("TreeD");

      std::vector<TH1F> pp_hist_vector;
      std::vector<TH1F> mix_hist_vector;
      std::vector<TH1F> problem_hist_vector;
    
      std::string problem_cuts = Form("%s&&abs(DPhi)>((0.9)*3.14159)",mass_ranges[mass_range].c_str());

      for (int variable_index = 0; variable_index<tree_variables.size(); variable_index++){

        const char * variable = tree_variables[variable_index].c_str();

        char pp_name[20],mix_name[20],problem_name[20],temp_name[20],hist_title[100];
        sprintf(pp_name,"pp_%i_%i",variable_index,current_cap);
        sprintf(mix_name,"mix_%i_%i",variable_index,current_cap);
        sprintf(problem_name,"prob_%i_%i",variable_index,current_cap);
        sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());

        pp_hist_vector.push_back(TH1F(pp_name,hist_title,50,low_edges[variable_index],high_edges[variable_index]));
        mix_hist_vector.push_back(TH1F(mix_name,hist_title,50,low_edges[variable_index],high_edges[variable_index]));
        problem_hist_vector.push_back(TH1F(problem_name,hist_title,50,low_edges[variable_index],high_edges[variable_index]));

        pp_tree->Draw(Form("%s>>%s",variable,pp_name),mass_ranges[mass_range].c_str(),"goff");
        mix_tree->Draw(Form("%s>>%s",variable,mix_name),mass_ranges[mass_range].c_str(),"goff");
        mix_tree->Draw(Form("%s>>%s",variable,problem_name),problem_cuts.c_str(),"goff");

      } 
      
      TCanvas canv{"canv","",200,100,4000,3000};
      canv.Divide(5,4);

      std::vector<TPaveText> text_vector;
      std::vector<TLegend> legend_vector;

      for (int variable_index = 0; variable_index<tree_variables.size(); variable_index++){
        
        const char * variable = tree_variables[variable_index].c_str();

        canv.cd(variable_index+1);

        pp_hist_vector[variable_index].Scale(1.0/pp_hist_vector[variable_index].Integral());
        mix_hist_vector[variable_index].Scale(1.0/mix_hist_vector[variable_index].Integral());
        problem_hist_vector[variable_index].Scale(1.0/problem_hist_vector[variable_index].Integral());
        pp_hist_vector[variable_index].Draw("HIST");
        mix_hist_vector[variable_index].Draw("HIST SAME");
        problem_hist_vector[variable_index].Draw("HIST SAME");
        pp_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,2*std::max(pp_hist_vector[variable_index].GetMaximum(),mix_hist_vector[variable_index].GetMaximum()));

        pp_hist_vector[variable_index].SetLineColor(2);
        pp_hist_vector[variable_index].SetLineStyle(1);
        mix_hist_vector[variable_index].SetLineColor(4);
        mix_hist_vector[variable_index].SetLineStyle(2);
        problem_hist_vector[variable_index].SetLineStyle(1);
        problem_hist_vector[variable_index].SetLineColor(1);

        gPad->Modified(); gPad->Update();
        //TPaveText * integral_text = new TPaveText(0.65,0.75,0.90,0.85,"nbNDC");
        //integral_text->AddText(Form("pp Integral - %i",(int) pp_hist_vector[variable_index].Integral()));
        //integral_text->AddText(Form("mix Integral - %i",(int) mix_hist_vector[variable_index].Integral()));
        //integral_text->SetBorderSize(1);
        //integral_text->SetFillStyle(0);
        //integral_text->SetFillColor(0);
        //integral_text->SetTextFont(42);
        //integral_text->SetTextSize(0.02);
        //integral_text->SetTextAlign(31);
        //integral_text->Draw();
        //text_vector.push_back(integral_text);
        
        TPaveText * cap_text = new TPaveText(0.75,0.8,0.925,0.925,"nbNDC");
        cap_text->AddText(Form("Object use cap - %i",current_cap));
        //cap_text->AddText(Form("Chi 2 - %.7f",pp_hist_vector[variable_index].Chi2Test(&mix_hist_vector[variable_index],"UU NORM") ));
        //cap_text->AddText(Form("Chi 2/ndf - %.7f",pp_hist_vector[variable_index].Chi2Test(&mix_hist_vector[variable_index],"UU NORM CHI2/NDF") ));
        cap_text->SetBorderSize(0);
        cap_text->SetFillStyle(0);
        cap_text->SetFillColor(0);
        cap_text->SetTextFont(42);
        cap_text->SetTextSize(0.02);
        cap_text->SetTextAlign(31);
        cap_text->Draw();
        
        TLegend * current_legend = new TLegend(0.225,0.85,0.35,0.925,variable);
        current_legend->AddEntry(&pp_hist_vector[variable_index],"pp");
        current_legend->AddEntry(&mix_hist_vector[variable_index],"mix");
        current_legend->AddEntry(&problem_hist_vector[variable_index],"problem");
        current_legend->SetBorderSize(1);
        current_legend->SetTextFont(42);
        current_legend->SetFillStyle(0);
        current_legend->SetTextSize(0.02);
        current_legend->Draw(); 
        //legend_vector.push_back(current_legend);

      }
      canv.SaveAs(Form("./pp-ppmix_cap%i_%s_shape_check.png",current_cap,mass_cuts[mass_range].c_str()));
    }
  }
}

int main(){
  check(); 
}
