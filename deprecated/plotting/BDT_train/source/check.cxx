
#include <TH1.h>
#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <iostream>
#include <vector>
#include <string>
#include <TROOT.h>
#include <TStyle.h>
#include <cmath>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"

void check(){

  SetAtlasStyle();
   
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);

  TFile sign_file{"../../../TMVA/run/Eval_signal/Evaluate_signal.root","READ"}; 
  TFile bckg_file{"../../../TMVA/run/Eval_pp/Evaluate_pp-mix.root","READ"}; 

  TTree * sign_tree = (TTree*) sign_file.Get("signal_00_mu4000_P5000_bound-0");
  TTree * bckg_tree = (TTree*) bckg_file.Get("pp-mix_00_mu4000_P5000_bound-0");

  TCanvas var_canv{"var_canv","var_canv",600,600,2000,1000};
  var_canv.Divide(1);

  TH1F * sign_bdt_hist = new TH1F("sign_BDT_hist","BDT Score",20,-1.0,1.0);
  TH1F * bckg_bdt_hist = new TH1F("bckg_BDT_hist","BDT Score",20,-1.0,1.0);

  sign_tree->Draw("BDT>>sign_BDT_hist","","goff");
  bckg_tree->Draw("BDT>>bckg_BDT_hist","","goff");

  sign_bdt_hist->Scale(1.0/sign_bdt_hist->Integral());
  bckg_bdt_hist->Scale(1.0/bckg_bdt_hist->Integral());
  
  var_canv.cd(1);
  sign_bdt_hist->Draw("HIST"); 
  bckg_bdt_hist->Draw("HIST SAME"); 
  sign_bdt_hist->GetYaxis()->SetRangeUser(0,1.5*bckg_bdt_hist->GetMaximum());
  sign_bdt_hist->SetLineColor(2);
  bckg_bdt_hist->SetLineColor(4);
  gPad->Modified(); gPad->Update();

  sign_bdt_hist->GetYaxis()->SetTitle("Entries / 0.2");
  sign_bdt_hist->GetXaxis()->SetTitle("BDT Score");

  TLegend * current_legend = new TLegend(0.65,0.75,0.925,0.925,"Normalised BDT Score");
  current_legend->AddEntry(sign_bdt_hist,"Signal");
  current_legend->AddEntry(bckg_bdt_hist,"Background");
  current_legend->SetBorderSize(1);
  current_legend->SetTextFont(42);
  current_legend->SetFillStyle(0);
  current_legend->SetTextSize(0.04);
  current_legend->Draw();

  TLatex qta_2_ATLAS;
  qta_2_ATLAS.SetNDC();
  qta_2_ATLAS.SetTextFont(72);
  qta_2_ATLAS.SetTextColor(1);
  qta_2_ATLAS.DrawLatex(0.225,0.89,"ATLAS");
  TLatex qta_2_wip; 
  qta_2_wip.SetNDC();
  qta_2_wip.SetTextFont(42);
  qta_2_wip.SetTextSize(0.038);
  qta_2_wip.SetTextColor(1);
  qta_2_wip.DrawLatex(0.225,0.85,"Work In Progress");

  char file_str[30];
  var_canv.SaveAs("check.png"); 

}

int main( int argc, char * argv[]){
  check();
}
