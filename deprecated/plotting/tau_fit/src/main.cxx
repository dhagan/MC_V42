#include <main.hxx>
#include <tau.hxx>


int help(){
  std::cout << "h     --    help      --  output help" << std::endl;
  std::cout << "t     --    files     --  colon separated list of tmvatrees" << std::endl;
  std::cout << "m     --    mode      --  mode" << std::endl;
  std::cout << "u     --    uniq      --  unique string attached to this run" << std::endl;
  std::cout << "                      --  COMP - Comparison of TMVATree output" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "help",       no_argument,              0,      'h'},
      { "trees",      required_argument,        0,      't'},
      { "mode",       required_argument,        0,      'm'},
      { "unique",     required_argument,        0,      'u'},
      { "range",      required_argument,        0,      'r'},
      { "var",        required_argument,        0,      'v'},
      { "spec",       required_argument,        0,      's'},
      { "cond",       required_argument,        0,      'c'},
      { 0,            0,                        0,      0}
    };

  std::string files, mode, unique, range, var, spec, cond, type;

  do {
    option = getopt_long( argc, argv, "t:m:u:r:v:s:c:p:h", long_options, &option_index );
    switch ( option ){
      case 'h':
        return help();
      case 't': 
        files   = std::string( optarg );
        break;
      case 'u':
        unique  = std::string( optarg );
        break;
      case 'm':
        mode    = std::string( optarg );
        break;
      case 'r':
        range    = std::string( optarg );
        break;
      case 'v':
        var      = std::string( optarg );
        break;
      case 's':
        spec     = std::string( optarg );
        break;
      case 'c':
        cond     = std::string( optarg );
        break;
      case 'p':
        type     = std::string( optarg );
        break;
    }
  } while (option != -1);
  

  if ( mode.empty() ){ 
    std::cout << "no mode provided" << std::endl; 
    return help(); 
  }

  if ( mode.find( "TAU" ) != std::string::npos ){
    tau( files, range, var, spec, cond, type, unique );
    return 0;
  }

  std::cout << "Mode provided does not match those available" << std::endl;
  return help();
}

