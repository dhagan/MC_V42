#include <th.hxx>

void th( ){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);


  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42);
  wip.SetTextSize(0.038); wip.SetTextColor(1);
  TLatex sim;  sim.SetNDC(); sim.SetTextFont(42);
  sim.SetTextSize(0.038); sim.SetTextColor(1);

  TF1 * fit_f4f1 = new TF1("fit_f4f1"," ( (1-x^(2))^(2) )/( (-3-x^(2))^(2) ) ", 0, 0.5); 
  TF1 * fit_int_f4f1 = new TF1("fit_int_f4f1"," ((1-x^(2))^(2))/((-3-x^(2))*2*x) +"
      "(1/(sqrt(3)))*atan((x/(sqrt(3))))*(2-2*x^(2)) + 2*x - (2/(sqrt(3)))*(x^(2)+3)*atan((x/(sqrt(3)))) ", 0, 0.5); 

  TCanvas * canv = new TCanvas("canv","",200,200,2000,1000);
  canv->Divide(2,1);
  canv->cd( 1 );

  fit_f4f1->Draw();
  fit_f4f1->SetLineColor(1);
  fit_f4f1->GetYaxis()->SetTitle("f4/f1");
  fit_f4f1->GetXaxis()->SetTitle("cos\\theta_{cs}");
  fit_f4f1->GetYaxis()->SetLabelSize(0.035);
  fit_f4f1->GetYaxis()->SetTitleSize(0.035);
  fit_f4f1->GetXaxis()->SetLabelSize(0.035);
  fit_f4f1->GetXaxis()->SetTitleSize(0.035);
  fit_f4f1->GetYaxis()->SetRangeUser(0,fit_f4f1->GetMaximum()*1.5);
  gPad->Update();
  TLegend * pad1_legend = new TLegend(0.45,0.65,0.83,0.8);
  pad1_legend->SetBorderSize(0);
  pad1_legend->SetFillColor(0);
  pad1_legend->SetFillStyle(0);
  pad1_legend->SetTextFont(42);
  pad1_legend->SetTextSize(0.025);
  pad1_legend->AddEntry(fit_f4f1,"ratio f4/f1","L");
  pad1_legend->Draw();
  TLatex pad1_ltx_title;
  pad1_ltx_title.SetTextSize(0.03);
  pad1_ltx_title.DrawLatexNDC(0.2,0.825,Form("Ratio of f4/f1"));  
  TLatex pad1_ltx_eqn;
  pad1_ltx_eqn.SetTextSize(0.03);
  pad1_ltx_eqn.DrawLatexNDC(0.2,0.825,Form("Ratio of f4/f1"));  
  ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
  wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

  canv->cd( 2 );
  fit_int_f4f1->Draw();
  fit_int_f4f1->SetLineColor(1);
  fit_int_f4f1->GetYaxis()->SetTitle("f4/f1");
  fit_int_f4f1->GetXaxis()->SetTitle("cos\\theta_{cs}");
  fit_int_f4f1->GetYaxis()->SetLabelSize(0.035);
  fit_int_f4f1->GetYaxis()->SetTitleSize(0.035);
  fit_int_f4f1->GetXaxis()->SetLabelSize(0.035);
  fit_int_f4f1->GetXaxis()->SetTitleSize(0.035);
  //fit_int_f4f1->GetYaxis()->SetRangeUser(0,fit_int_f4f1->GetMaximum()*1.5);
  gPad->Update();
  TLegend * pad2_legend = new TLegend(0.45,0.65,0.83,0.8);
  pad2_legend->SetBorderSize(0);
  pad2_legend->SetFillColor(0);
  pad2_legend->SetFillStyle(0);
  pad2_legend->SetTextFont(42);
  pad2_legend->SetTextSize(0.025);
  pad2_legend->AddEntry(fit_f4f1,"integral f4/f1","L");
  pad2_legend->Draw();
  TLatex pad2_ltx_title;
  pad2_ltx_title.SetTextSize(0.03);
  pad2_ltx_title.DrawLatexNDC(0.2,0.825,Form("Integral of f4/f1"));  
  TLatex pad2_ltx_eqn;
  pad2_ltx_eqn.SetTextSize(0.03);
  pad2_ltx_eqn.DrawLatexNDC(0.2,0.825,Form("Integral of f4/f1"));  
  ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
  wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

  canv->SaveAs(Form("./th.png"));

}



