cmake_minimum_required(VERSION 3.14)
project(tree_check)
find_package(ROOT CONFIG REQUIRED COMPONENTS RooStats RooFit RooFitCore HistFactory)
target_compile_definitions(ROOT::Core INTERFACE ${ROOT_DEFINITIONS} )
set(src_sim check_sim.cxx)
add_executable( tmva_sim_check ${src_sim})
target_link_libraries(tmva_sim_check PRIVATE ROOT::Core ${ROOT_LIBRARIES} )

SET( CMAKE_EXPORT_COMPILE_COMMANDS ON)
IF( EXISTS "${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json" )
  EXECUTE_PROCESS( COMMAND ${CMAKE_COMMAND} -E copy_if_different
          ${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json
          )
ENDIF()
