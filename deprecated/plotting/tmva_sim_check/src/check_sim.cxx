
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  std::vector<std::string> tree_variables = {"BDT","DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};
  std::vector<double> low_edges{-1.0,-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};
  std::vector<double> high_edges{1.0, M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};
  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};

  TFile sign_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sim/eval/Eval_sign.root","READ"};
  TFile bckg_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sim/eval/Eval_bckg.root","READ"};
  TFile data_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sim/eval/Eval_data.root","READ"};

  TTree * sign_tree = (TTree*) sign_file.Get("signal_00_mu4000_P5000_bound-0");
  TTree * bckg_tree = (TTree*) bckg_file.Get("pp_00_mu4000_P5000_bound-0");
  TTree * data_tree = (TTree*) data_file.Get("data_00_mu4000_P5000_bound-0");

  std::vector<int> frac_ints = {5, 10, 20, 30, 40, 50};

  for ( int f_idx = 0; f_idx <= 5; f_idx++){

    int frac = frac_ints[f_idx];

    TFile simd_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sim/eval/Eval_sim-data_s%i.root",(int) frac),"READ"};
    TFile sims_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sim/eval/Eval_sim-sign_s%i.root",(int) frac),"READ"};
    TFile simb_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sim/eval/Eval_sim-bckg_s%i.root",(int) frac),"READ"};
    TTree * simd_tree = (TTree*) simd_file.Get(Form("sim_data_s%i_00_mu4000_P5000_bound-0",(int) frac));
    TTree * sims_tree = (TTree*) sims_file.Get(Form("sim_signal_s%i_00_mu4000_P5000_bound-0",(int) frac));
    TTree * simb_tree = (TTree*) simb_file.Get(Form("sim_pp_s%i_00_mu4000_P5000_bound-0",(int) frac));

    for (int q_idx = 0; q_idx <= 0; q_idx++){

      int qta_lower; 
      int qta_upper;
      
      if (q_idx == 0){
        qta_lower = -5;
        qta_upper = 15; 
      } else {
        qta_lower = (2*(q_idx-1)) - 5;
        qta_upper = (2*(q_idx-1)) - 3;
      }

      std::cout << "qta upper " << qta_upper << std::endl;
      std::cout << "qta lower " << qta_lower << std::endl;
          
      for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 

        std::vector<TH1F> sign_hist_vector;
        std::vector<TH1F> bckg_hist_vector;
        std::vector<TH1F> data_hist_vector;
        std::vector<TH1F> simd_hist_vector;
        std::vector<TH1F> sims_hist_vector;
        std::vector<TH1F> simb_hist_vector;
            
        char cut[200];
        sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
        std::cout << cut << std::endl;

        for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){

          const char * variable = tree_variables[variable_index].c_str();
          char hist_title[100];
          char bckg_name[40],sign_name[40],data_name[40],simd_name[40],sims_name[40],simb_name[40];

          sprintf(sign_name,"sign_%i_%i_%i_%i",variable_index,q_idx,mass_range,frac);
          sprintf(bckg_name,"bckg_%i_%i_%i_%i",variable_index,q_idx,mass_range,frac);
          sprintf(data_name,"data_%i_%i_%i_%i",variable_index,q_idx,mass_range,frac);
          sprintf(simd_name,"simd_%i_%i_%i_%i",variable_index,q_idx,mass_range,frac);
          sprintf(sims_name,"sims_%i_%i_%i_%i",variable_index,q_idx,mass_range,frac);
          sprintf(simb_name,"simb_%i_%i_%i_%i",variable_index,q_idx,mass_range,frac);
          sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());
          
          int bin_count = 50;
          
          sign_hist_vector.push_back(TH1F(sign_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          bckg_hist_vector.push_back(TH1F(bckg_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          data_hist_vector.push_back(TH1F(data_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          simd_hist_vector.push_back(TH1F(simd_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          sims_hist_vector.push_back(TH1F(sims_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          simb_hist_vector.push_back(TH1F(simb_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
          sign_tree->Draw(Form("%s>>%s",variable,sign_name),cut,"goff");
          bckg_tree->Draw(Form("%s>>%s",variable,bckg_name),cut,"goff");
          data_tree->Draw(Form("%s>>%s",variable,data_name),cut,"goff");
          simd_tree->Draw(Form("%s>>%s",variable,simd_name),cut,"goff");
          sims_tree->Draw(Form("%s>>%s",variable,sims_name),cut,"goff");
          simb_tree->Draw(Form("%s>>%s",variable,simb_name),cut,"goff");

        } 
          
        TCanvas canv{Form("canv%i",q_idx),"",200,100,5000,4000};
        canv.Divide(5,4);

        for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){
          
          const char * variable = tree_variables[variable_index].c_str();

          double sign_int = sign_hist_vector[variable_index].Integral();
          double bckg_int = bckg_hist_vector[variable_index].Integral();
          double data_int = data_hist_vector[variable_index].Integral();
          double simd_int = simd_hist_vector[variable_index].Integral();
          double sims_int = sims_hist_vector[variable_index].Integral();
          double simb_int = simb_hist_vector[variable_index].Integral();

          sign_hist_vector[variable_index].Scale(1.0/sign_hist_vector[variable_index].Integral());
          bckg_hist_vector[variable_index].Scale(1.0/bckg_hist_vector[variable_index].Integral());
          data_hist_vector[variable_index].Scale(1.0/data_hist_vector[variable_index].Integral());
          simd_hist_vector[variable_index].Scale(1.0/simd_hist_vector[variable_index].Integral());
          sims_hist_vector[variable_index].Scale(1.0/sims_hist_vector[variable_index].Integral());
          simb_hist_vector[variable_index].Scale(1.0/simb_hist_vector[variable_index].Integral());

          canv.cd(variable_index+1);
          sign_hist_vector[variable_index].Draw("HIST");
          bckg_hist_vector[variable_index].Draw("HIST SAME");
          data_hist_vector[variable_index].Draw("HIST SAME");
          simd_hist_vector[variable_index].Draw("HIST SAME");
          sims_hist_vector[variable_index].Draw("HIST SAME");
          simb_hist_vector[variable_index].Draw("HIST SAME");

          
          sign_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,1.5*std::max(sign_hist_vector[variable_index].GetMaximum(),bckg_hist_vector[variable_index].GetMaximum()));
          sign_hist_vector[variable_index].SetLineColor(2);
          sign_hist_vector[variable_index].SetLineStyle(1);
          bckg_hist_vector[variable_index].SetLineColor(4);
          bckg_hist_vector[variable_index].SetLineStyle(1);
          data_hist_vector[variable_index].SetLineColor(1);
          data_hist_vector[variable_index].SetLineStyle(1);
          simd_hist_vector[variable_index].SetLineColorAlpha(1,0.7);
          simd_hist_vector[variable_index].SetLineStyle(2);
          sims_hist_vector[variable_index].SetLineColorAlpha(2,0.7);
          sims_hist_vector[variable_index].SetLineStyle(2);
          simb_hist_vector[variable_index].SetLineColorAlpha(4,0.7);
          simb_hist_vector[variable_index].SetLineStyle(2);



          TPaveText * cap1_text = new TPaveText(0.75,0.7,0.925,0.925,"nbNDC");
          cap1_text->AddText(Form("sign Integral - %f", sign_int));
          cap1_text->AddText(Form("bckg Integral - %f", bckg_int));
          cap1_text->AddText(Form("data Integral - %f", data_int));
          cap1_text->AddText(Form("sim data Integral - %f", simd_int));
          cap1_text->AddText(Form("sim bckg Integral - %f", simb_int));
          cap1_text->AddText(Form("sim sign Integral - %f", sims_int));
          cap1_text->SetBorderSize(0);
          cap1_text->SetFillStyle(0);
          cap1_text->SetFillColor(0);
          cap1_text->SetTextFont(42);
          cap1_text->SetTextSize(0.015);
          cap1_text->SetTextAlign(31);
          cap1_text->Draw();
          
          TLegend * current1_legend = new TLegend(0.225,0.6,0.4,0.925,variable);
          current1_legend->AddEntry(&sign_hist_vector[variable_index],Form("sign"));
          current1_legend->AddEntry(&bckg_hist_vector[variable_index],Form("bckg"));
          current1_legend->AddEntry(&data_hist_vector[variable_index],Form("data"));
          current1_legend->AddEntry(&simd_hist_vector[variable_index],Form("sim data s%i", frac));
          current1_legend->AddEntry(&sims_hist_vector[variable_index],Form("sim sign s%i", frac));
          current1_legend->AddEntry(&simb_hist_vector[variable_index],Form("sim bckg s%i", frac));
          current1_legend->SetBorderSize(1);
          current1_legend->SetTextFont(42);
          current1_legend->SetFillStyle(0);
          current1_legend->SetTextSize(0.02);
          current1_legend->Draw(); 


        }
        canv.SaveAs(Form("./tmva_sim_check_qta%i_Q%s_s%i.png",q_idx,mass_cuts[mass_range].c_str(),(int) frac));
      }
    }
  }
}

int main(){
  check(); 
}
