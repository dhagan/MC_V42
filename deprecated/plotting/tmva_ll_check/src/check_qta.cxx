
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  std::vector<std::string> tree_variables = {"BDT","DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};//,"qta-by-BDT"};
  std::vector<double> low_edges{-1.0,-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};//,-5};
  std::vector<double> high_edges{1.0, M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};//,15};

  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};


  for (int q_idx = 0; q_idx <= 10; q_idx++){

    int qta_lower; 
    int qta_upper;
    
    if (q_idx == 0){
      qta_lower = -5;
      qta_upper = 15; 
    } else {
      qta_lower = (2*(q_idx-1)) - 5;
      qta_upper = (2*(q_idx-1)) - 3;
    }
    std::cout << "qta upper " << qta_upper << std::endl;
    std::cout << "qta lower " << qta_lower << std::endl;

    TFile sign_file       {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_signal_ll_train_reg.root","READ"};
    TFile bckg_file       {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_pp_ll_train_reg.root","READ"};
    TFile data_file       {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_data_ll_train_reg.root","READ"};
    TFile sign_ll_file    {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_signal_ll_train_ll.root","READ"};
    TFile bckg_ll_file    {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_pp_ll_train_ll.root","READ"};
    TFile data_ll_file    {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_data_ll_train_ll.root","READ"};

    TTree * sign_tree         =  (TTree*) sign_file.Get("signal_00_lowlambda_mu4000_P5000_bound-0");
    TTree * bckg_tree         =  (TTree*) bckg_file.Get("pp_00_lowlambda_mu4000_P5000_bound-0");
    TTree * data_tree         =  (TTree*) data_file.Get("data_00_lowlambda_mu4000_P5000_bound-0");
    TTree * sign_ll_tree      =  (TTree*) sign_ll_file.Get("signal_00_lowlambda_mu4000_P5000_bound-0");
    TTree * bckg_ll_tree      =  (TTree*) bckg_ll_file.Get("pp_00_lowlambda_mu4000_P5000_bound-0");
    TTree * data_ll_tree      =  (TTree*) data_ll_file.Get("data_00_lowlambda_mu4000_P5000_bound-0");



    for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 

      std::vector<TH1F> sign_hist_vector;
      std::vector<TH1F> bckg_hist_vector;
      std::vector<TH1F> data_hist_vector;
      std::vector<TH1F> sign_ll_hist_vector;
      std::vector<TH1F> bckg_ll_hist_vector;
      std::vector<TH1F> data_ll_hist_vector;

      char cut[200];
      //sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      sprintf(cut,"%s&&qtA>%i&&qtA<%i&&Lambda>15",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
      std::cout << cut << std::endl;

      for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){

        const char * variable = tree_variables[variable_index].c_str();
        char bckg_ll_name[40],sign_ll_name[40],data_ll_name[40],hist_title[100];
        char bckg_rg_name[40],sign_rg_name[40],data_rg_name[40];

        sprintf(sign_rg_name,"sign_rg_%i_%i_%i",variable_index,q_idx,mass_range);
        sprintf(bckg_rg_name,"bckg_rg_%i_%i_%i",variable_index,q_idx,mass_range);
        sprintf(data_rg_name,"data_rg_%i_%i_%i",variable_index,q_idx,mass_range);
        sprintf(sign_ll_name,"sign_ll_%i_%i_%i",variable_index,q_idx,mass_range);
        sprintf(bckg_ll_name,"bckg_ll_%i_%i_%i",variable_index,q_idx,mass_range);
        sprintf(data_ll_name,"data_ll_%i_%i_%i",variable_index,q_idx,mass_range);

        sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());
        
        int bin_count = 50;
        
        sign_hist_vector.push_back(       TH1F(sign_rg_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        bckg_hist_vector.push_back(       TH1F(bckg_rg_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        data_hist_vector.push_back(       TH1F(data_rg_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        sign_ll_hist_vector.push_back(    TH1F(sign_ll_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        bckg_ll_hist_vector.push_back(    TH1F(bckg_ll_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
        data_ll_hist_vector.push_back(    TH1F(data_ll_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));

        sign_tree->Draw(Form("%s>>%s",variable,sign_rg_name),cut,"goff");
        bckg_tree->Draw(Form("%s>>%s",variable,bckg_rg_name),cut,"goff");
        data_tree->Draw(Form("%s>>%s",variable,data_rg_name),cut,"goff");
        sign_ll_tree->Draw(Form("%s>>%s",variable,sign_ll_name),cut,"goff");
        bckg_ll_tree->Draw(Form("%s>>%s",variable,bckg_ll_name),cut,"goff");
        data_ll_tree->Draw(Form("%s>>%s",variable,data_ll_name),cut,"goff");

      } 
      
      TCanvas canv{Form("canv%i",q_idx),"",200,100,5000,4000};
      canv.Divide(5,4);

      for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){
        
        const char * variable = tree_variables[variable_index].c_str();
        canv.cd(variable_index+1);

        double sign_int           = sign_hist_vector[variable_index].Integral();
        double bckg_int           = bckg_hist_vector[variable_index].Integral();
        double data_int           = bckg_hist_vector[variable_index].Integral();
        double sign_ll_int        = sign_ll_hist_vector[variable_index].Integral();
        double bckg_ll_int        = bckg_ll_hist_vector[variable_index].Integral();
        double data_ll_int        = data_ll_hist_vector[variable_index].Integral();

        sign_hist_vector[variable_index].Scale(1.0/sign_hist_vector[variable_index].Integral());
        bckg_hist_vector[variable_index].Scale(1.0/bckg_hist_vector[variable_index].Integral());
        data_hist_vector[variable_index].Scale(1.0/data_hist_vector[variable_index].Integral());
        sign_ll_hist_vector[variable_index].Scale(1.0/sign_ll_hist_vector[variable_index].Integral());
        bckg_ll_hist_vector[variable_index].Scale(1.0/bckg_ll_hist_vector[variable_index].Integral());
        data_ll_hist_vector[variable_index].Scale(1.0/data_ll_hist_vector[variable_index].Integral());

        sign_hist_vector[variable_index].Draw("HIST");
        bckg_hist_vector[variable_index].Draw("HIST SAME");
        data_hist_vector[variable_index].Draw("HIST SAME");
        sign_ll_hist_vector[variable_index].Draw("HIST SAME");
        bckg_ll_hist_vector[variable_index].Draw("HIST SAME");
        data_ll_hist_vector[variable_index].Draw("HIST SAME");


        sign_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,1.5*std::max(sign_hist_vector[variable_index].GetMaximum(),bckg_hist_vector[variable_index].GetMaximum()));

        sign_hist_vector[variable_index].SetLineColor(2);
        sign_hist_vector[variable_index].SetLineStyle(1);
        bckg_hist_vector[variable_index].SetLineColor(4);
        bckg_hist_vector[variable_index].SetLineStyle(1);
        data_hist_vector[variable_index].SetLineColor(1);
        data_hist_vector[variable_index].SetLineStyle(1);

        sign_ll_hist_vector[variable_index].SetLineColorAlpha(2,0.6);
        sign_ll_hist_vector[variable_index].SetLineStyle(2);
        bckg_ll_hist_vector[variable_index].SetLineColorAlpha(4,0.6);
        bckg_ll_hist_vector[variable_index].SetLineStyle(2);
        data_ll_hist_vector[variable_index].SetLineColorAlpha(1,0.6);
        data_ll_hist_vector[variable_index].SetLineStyle(2);

        
        TPaveText * cap_text = new TPaveText(0.75,0.5,0.925,0.925,"nbNDC");
        cap_text->AddText(Form("sign Integral - %f",    bckg_int));
        cap_text->AddText(Form("bckg Integral - %f",    sign_int));
        cap_text->AddText(Form("data Integral - %f",    data_int));
        cap_text->AddText(Form("sign ll Integral - %f", bckg_ll_int));
        cap_text->AddText(Form("bckg ll Integral - %f", sign_ll_int));
        cap_text->AddText(Form("data ll Integral - %f", data_ll_int));
        cap_text->SetBorderSize(0);
        cap_text->SetFillStyle(0);
        cap_text->SetFillColor(0);
        cap_text->SetTextFont(42);
        cap_text->SetTextSize(0.015);
        cap_text->SetTextAlign(31);
        cap_text->Draw();
        
        TLegend * current_legend = new TLegend(0.225,0.6,0.4,0.925,variable);
        current_legend->AddEntry(&sign_hist_vector[variable_index],"sign");
        current_legend->AddEntry(&bckg_hist_vector[variable_index],"bckg");
        current_legend->AddEntry(&data_hist_vector[variable_index],"data");
        current_legend->AddEntry(&sign_ll_hist_vector[variable_index],"sign ll");
        current_legend->AddEntry(&bckg_ll_hist_vector[variable_index],"bckg ll");
        current_legend->AddEntry(&data_ll_hist_vector[variable_index],"data ll");
        current_legend->SetBorderSize(1);
        current_legend->SetTextFont(42);
        current_legend->SetFillStyle(0);
        current_legend->SetTextSize(0.02);
        current_legend->Draw(); 

      }
      canv.SaveAs(Form("./tmva_ll_check_qta%i_Q%s.png",q_idx,mass_cuts[mass_range].c_str()));
    }
  }
}

int main(){
  check(); 
}
