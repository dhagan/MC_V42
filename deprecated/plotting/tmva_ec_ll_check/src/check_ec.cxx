
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>

#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasLabels.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasUtils.C"
#include "/home/atlas/amytee/atlasstyle-00-04-02/AtlasStyle.C"


void check(){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  std::vector<std::string> tree_variables = {"BDT","DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM","qxgamma","qygamma","qxpsi","qypsi"};//,"qta-by-BDT"};
  std::vector<double> low_edges{-1.0,-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5,-15,-20,-5,-15};//,-5};
  std::vector<double> high_edges{1.0, M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15, 20, 20,20, 15};//,15};
  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};

  TFile sign_ll_file       {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_signal_ll_train_reg.root","READ"};
  TFile bckg_ll_file       {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_pp_ll_train_reg.root","READ"};
  TFile data_ll_file       {"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ll/eval/Eval_data_ll_train_reg.root","READ"};
  
  TTree * sign_ll_tree      =  (TTree*) sign_ll_file.Get("signal_00_lowlambda_mu4000_P5000_bound-0");
  TTree * bckg_ll_tree      =  (TTree*) bckg_ll_file.Get("pp_00_lowlambda_mu4000_P5000_bound-0");
  TTree * data_ll_tree      =  (TTree*) data_ll_file.Get("data_00_lowlambda_mu4000_P5000_bound-0");


  std::vector<std::string> ec_vec = {"leps","heps","lqtb","hqtb"};

  for (int q_idx = 0; q_idx <= 10; q_idx++){

    int qta_lower; 
    int qta_upper;
    
    if (q_idx == 0){
      qta_lower = -5;
      qta_upper = 15; 
    } else {
      qta_lower = (2*(q_idx-1)) - 5;
      qta_upper = (2*(q_idx-1)) - 3;
    }

    std::cout << "qta upper " << qta_upper << std::endl;
    std::cout << "qta lower " << qta_lower << std::endl;

    for (int ec1_idx = 0; ec1_idx <= 3; ec1_idx++){

      TFile sign_ec1_file       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ec/eval/Eval_signal_ll_train_C%s.root",ec_vec[ec1_idx].c_str()),"READ"};
      TFile bckg_ec1_file       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ec/eval/Eval_pp_ll_train_C%s.root",ec_vec[ec1_idx].c_str())    ,"READ"};
      TFile data_ec1_file       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ec/eval/Eval_data_ll_train_C%s.root",ec_vec[ec1_idx].c_str())  ,"READ"};
      
      TTree * sign_ec1_tree      =  (TTree*) sign_ec1_file.Get("signal_00_lowlambda_mu4000_P5000_bound-0");
      TTree * bckg_ec1_tree      =  (TTree*) bckg_ec1_file.Get("pp_00_lowlambda_mu4000_P5000_bound-0");
      TTree * data_ec1_tree      =  (TTree*) data_ec1_file.Get("data_00_lowlambda_mu4000_P5000_bound-0");


      for (int ec2_idx = 0; ec2_idx <= 3; ec2_idx++){

        TFile sign_ec2_file       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ec/eval/Eval_signal_%s_train_C%s.root",ec_vec[ec2_idx].c_str(),ec_vec[ec1_idx].c_str()),"READ"};
        TFile bckg_ec2_file       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ec/eval/Eval_pp_%s_train_C%s.root",ec_vec[ec2_idx].c_str(),ec_vec[ec1_idx].c_str())    ,"READ"};
        TFile data_ec2_file       {Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_ec/eval/Eval_data_%s_train_C%s.root",ec_vec[ec2_idx].c_str(),ec_vec[ec1_idx].c_str())  ,"READ"};
        
        TTree * sign_ec2_tree      =  (TTree*) sign_ec2_file.Get(Form("signal_00_lowlambda_mu4000_P5000_C%s_bound-0",ec_vec[ec2_idx].c_str()));
        TTree * bckg_ec2_tree      =  (TTree*) bckg_ec2_file.Get(Form(    "pp_00_lowlambda_mu4000_P5000_C%s_bound-0",ec_vec[ec2_idx].c_str()));
        TTree * data_ec2_tree      =  (TTree*) data_ec2_file.Get(Form(  "data_00_lowlambda_mu4000_P5000_C%s_bound-0",ec_vec[ec2_idx].c_str()));


        for ( int mass_range = 0; mass_range < mass_cuts.size(); mass_range++){ 

          std::vector<TH1F> sign_ll_hist_vector;
          std::vector<TH1F> bckg_ll_hist_vector;
          std::vector<TH1F> data_ll_hist_vector;
          
          std::vector<TH1F> sign_ec1_hist_vector;
          std::vector<TH1F> bckg_ec1_hist_vector;
          std::vector<TH1F> data_ec1_hist_vector;
          
          std::vector<TH1F> sign_ec2_hist_vector;
          std::vector<TH1F> bckg_ec2_hist_vector;
          std::vector<TH1F> data_ec2_hist_vector;



          char cut[200];
          //sprintf(cut,"%s&&qtA>%i&&qtA<%i",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
          sprintf(cut,"%s&&qtA>%i&&qtA<%i&&Lambda>15",mass_ranges[mass_range].c_str(),qta_lower,qta_upper);
          std::cout << cut << std::endl;

          for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){

            const char * variable = tree_variables[variable_index].c_str();
            char hist_title[100];
            char bckg_ll_name[40],sign_ll_name[40],data_ll_name[40];
            char bckg_ec1_name[40],sign_ec1_name[40],data_ec1_name[40];
            char bckg_ec2_name[40],sign_ec2_name[40],data_ec2_name[40];

            sprintf(sign_ll_name,"sign_ll_%i_%i_%i",variable_index,q_idx,mass_range);
            sprintf(bckg_ll_name,"bckg_ll_%i_%i_%i",variable_index,q_idx,mass_range);
            sprintf(data_ll_name,"data_ll_%i_%i_%i",variable_index,q_idx,mass_range);
            
            sprintf(sign_ec1_name,"sign_ec1_%i_%i_%i",variable_index,q_idx,mass_range);
            sprintf(bckg_ec1_name,"bckg_ec1_%i_%i_%i",variable_index,q_idx,mass_range);
            sprintf(data_ec1_name,"data_ec1_%i_%i_%i",variable_index,q_idx,mass_range);
            
            sprintf(sign_ec2_name,"sign_ec2_%i_%i_%i",variable_index,q_idx,mass_range);
            sprintf(bckg_ec2_name,"bckg_ec2_%i_%i_%i",variable_index,q_idx,mass_range);
            sprintf(data_ec2_name,"data_ec2_%i_%i_%i",variable_index,q_idx,mass_range);

            sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());
            
            int bin_count = 50;
            
            sign_ll_hist_vector.push_back(    TH1F(sign_ll_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            bckg_ll_hist_vector.push_back(    TH1F(bckg_ll_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            data_ll_hist_vector.push_back(    TH1F(data_ll_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            sign_ll_tree->Draw(Form("%s>>%s",variable,sign_ll_name),cut,"goff");
            bckg_ll_tree->Draw(Form("%s>>%s",variable,bckg_ll_name),cut,"goff");
            data_ll_tree->Draw(Form("%s>>%s",variable,data_ll_name),cut,"goff");
          
            sign_ec1_hist_vector.push_back(    TH1F(sign_ec1_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            bckg_ec1_hist_vector.push_back(    TH1F(bckg_ec1_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            data_ec1_hist_vector.push_back(    TH1F(data_ec1_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            sign_ec1_tree->Draw(Form("%s>>%s",variable,sign_ec1_name),cut,"goff");
            bckg_ec1_tree->Draw(Form("%s>>%s",variable,bckg_ec1_name),cut,"goff");
            data_ec1_tree->Draw(Form("%s>>%s",variable,data_ec1_name),cut,"goff");
            
            sign_ec2_hist_vector.push_back(    TH1F(sign_ec2_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            bckg_ec2_hist_vector.push_back(    TH1F(bckg_ec2_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            data_ec2_hist_vector.push_back(    TH1F(data_ec2_name,hist_title,bin_count,low_edges[variable_index],high_edges[variable_index]));
            sign_ec2_tree->Draw(Form("%s>>%s",variable,sign_ec2_name),cut,"goff");
            bckg_ec2_tree->Draw(Form("%s>>%s",variable,bckg_ec2_name),cut,"goff");
            data_ec2_tree->Draw(Form("%s>>%s",variable,data_ec2_name),cut,"goff");

          } 
          
          TCanvas canv{Form("canv%i",q_idx),"",200,100,5000,8000};
          canv.Divide(5,8);

          for (int variable_index = 0; variable_index<tree_variables.size()-1; variable_index++){
            
            const char * variable = tree_variables[variable_index].c_str();


            double sign_ll_int        = sign_ll_hist_vector[variable_index].Integral();
            double bckg_ll_int        = bckg_ll_hist_vector[variable_index].Integral();
            double data_ll_int        = data_ll_hist_vector[variable_index].Integral();

            double sign_ec1_int        = sign_ec1_hist_vector[variable_index].Integral();
            double bckg_ec1_int        = bckg_ec1_hist_vector[variable_index].Integral();
            double data_ec1_int        = data_ec1_hist_vector[variable_index].Integral();

            double sign_ec2_int        = sign_ec2_hist_vector[variable_index].Integral();
            double bckg_ec2_int        = bckg_ec2_hist_vector[variable_index].Integral();
            double data_ec2_int        = data_ec2_hist_vector[variable_index].Integral();


            sign_ll_hist_vector[variable_index].Scale(1.0/sign_ll_hist_vector[variable_index].Integral());
            bckg_ll_hist_vector[variable_index].Scale(1.0/bckg_ll_hist_vector[variable_index].Integral());
            data_ll_hist_vector[variable_index].Scale(1.0/data_ll_hist_vector[variable_index].Integral());
            
            sign_ec1_hist_vector[variable_index].Scale(1.0/sign_ec1_hist_vector[variable_index].Integral());
            bckg_ec1_hist_vector[variable_index].Scale(1.0/bckg_ec1_hist_vector[variable_index].Integral());
            data_ec1_hist_vector[variable_index].Scale(1.0/data_ec1_hist_vector[variable_index].Integral());

            sign_ec2_hist_vector[variable_index].Scale(1.0/sign_ec2_hist_vector[variable_index].Integral());
            bckg_ec2_hist_vector[variable_index].Scale(1.0/bckg_ec2_hist_vector[variable_index].Integral());
            data_ec2_hist_vector[variable_index].Scale(1.0/data_ec2_hist_vector[variable_index].Integral());

            canv.cd(variable_index+1);
            sign_ll_hist_vector[variable_index].Draw("HIST");
            bckg_ll_hist_vector[variable_index].Draw("HIST SAME");
            data_ll_hist_vector[variable_index].Draw("HIST SAME");

            sign_ec1_hist_vector[variable_index].Draw("HIST SAME");
            bckg_ec1_hist_vector[variable_index].Draw("HIST SAME");
            data_ec1_hist_vector[variable_index].Draw("HIST SAME");
            
            
            sign_ll_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,1.5*std::max(sign_ll_hist_vector[variable_index].GetMaximum(),bckg_ll_hist_vector[variable_index].GetMaximum()));
            sign_ll_hist_vector[variable_index].SetLineColor(2);
            sign_ll_hist_vector[variable_index].SetLineStyle(1);
            bckg_ll_hist_vector[variable_index].SetLineColor(4);
            bckg_ll_hist_vector[variable_index].SetLineStyle(1);
            data_ll_hist_vector[variable_index].SetLineColor(1);
            data_ll_hist_vector[variable_index].SetLineStyle(1);

            sign_ec1_hist_vector[variable_index].SetLineColorAlpha(2,0.6);
            sign_ec1_hist_vector[variable_index].SetLineStyle(2);
            bckg_ec1_hist_vector[variable_index].SetLineColorAlpha(4,0.6);
            bckg_ec1_hist_vector[variable_index].SetLineStyle(2);
            data_ec1_hist_vector[variable_index].SetLineColorAlpha(1,0.6);
            data_ec1_hist_vector[variable_index].SetLineStyle(2);
            
            
            
            TPaveText * cap1_text = new TPaveText(0.75,0.5,0.925,0.925,"nbNDC");
            cap1_text->AddText(Form("sign ll Integral - %f", bckg_ll_int));
            cap1_text->AddText(Form("bckg ll Integral - %f", sign_ll_int));
            cap1_text->AddText(Form("data ll Integral - %f", data_ll_int));
            cap1_text->AddText(Form("sign ll Integral %s train- %f", bckg_ec1_int));
            cap1_text->AddText(Form("bckg ll Integral %s train- %f", sign_ec1_int));
            cap1_text->AddText(Form("data ll Integral %s train- %f", data_ec1_int));
            cap1_text->SetBorderSize(0);
            cap1_text->SetFillStyle(0);
            cap1_text->SetFillColor(0);
            cap1_text->SetTextFont(42);
            cap1_text->SetTextSize(0.015);
            cap1_text->SetTextAlign(31);
            cap1_text->Draw();
            
            TLegend * current1_legend = new TLegend(0.225,0.6,0.4,0.925,variable);
            current1_legend->AddEntry(&sign_ll_hist_vector[variable_index],Form("sign ll"));
            current1_legend->AddEntry(&bckg_ll_hist_vector[variable_index],Form("bckg ll"));
            current1_legend->AddEntry(&data_ll_hist_vector[variable_index],Form("data ll"));
            current1_legend->AddEntry(&sign_ec1_hist_vector[variable_index],Form("sign ll, %s training",ec_vec[ec1_idx].c_str()));
            current1_legend->AddEntry(&bckg_ec1_hist_vector[variable_index],Form("bckg ll, %s training",ec_vec[ec1_idx].c_str()));
            current1_legend->AddEntry(&data_ec1_hist_vector[variable_index],Form("data ll, %s training",ec_vec[ec1_idx].c_str()));
            current1_legend->SetBorderSize(1);
            current1_legend->SetTextFont(42);
            current1_legend->SetFillStyle(0);
            current1_legend->SetTextSize(0.02);
            current1_legend->Draw(); 

            canv.cd(variable_index+21);

            sign_ll_hist_vector[variable_index].Draw("HIST");
            bckg_ll_hist_vector[variable_index].Draw("HIST SAME");
            data_ll_hist_vector[variable_index].Draw("HIST SAME");
            
            sign_ec2_hist_vector[variable_index].Draw("HIST SAME");
            bckg_ec2_hist_vector[variable_index].Draw("HIST SAME");
            data_ec2_hist_vector[variable_index].Draw("HIST SAME");

            sign_ec2_hist_vector[variable_index].SetLineColorAlpha(2,0.6);
            sign_ec2_hist_vector[variable_index].SetLineStyle(3);
            bckg_ec2_hist_vector[variable_index].SetLineColorAlpha(4,0.6);
            bckg_ec2_hist_vector[variable_index].SetLineStyle(3);
            data_ec2_hist_vector[variable_index].SetLineColorAlpha(1,0.6);
            data_ec2_hist_vector[variable_index].SetLineStyle(3);


            TPaveText * cap2_text = new TPaveText(0.75,0.5,0.925,0.925,"nbNDC");
            cap2_text->AddText(Form("sign ll Integral - %f", bckg_ll_int));
            cap2_text->AddText(Form("bckg ll Integral - %f", sign_ll_int));
            cap2_text->AddText(Form("data ll Integral - %f", data_ll_int));
            cap2_text->AddText(Form("sign %s Integral %s train- %f", bckg_ec2_int));
            cap2_text->AddText(Form("bckg %s Integral %s train- %f", sign_ec2_int));
            cap2_text->AddText(Form("data %s Integral %s train- %f", data_ec2_int));
            cap2_text->SetBorderSize(0);
            cap2_text->SetFillStyle(0);
            cap2_text->SetFillColor(0);
            cap2_text->SetTextFont(42);
            cap2_text->SetTextSize(0.015);
            cap2_text->SetTextAlign(31);
            cap2_text->Draw();
            
            TLegend * current2_legend = new TLegend(0.225,0.6,0.4,0.925,variable);
            current2_legend->AddEntry(&sign_ll_hist_vector[variable_index],Form("sign ll"));
            current2_legend->AddEntry(&bckg_ll_hist_vector[variable_index],Form("bckg ll"));
            current2_legend->AddEntry(&data_ll_hist_vector[variable_index],Form("data ll"));
            current2_legend->AddEntry(&sign_ec1_hist_vector[variable_index],Form("sign ll, %s training",ec_vec[ec1_idx].c_str()));
            current2_legend->AddEntry(&bckg_ll_hist_vector[variable_index],Form("bckg %s, %s training",ec_vec[ec2_idx].c_str(),ec_vec[ec1_idx].c_str()));
            current2_legend->AddEntry(&data_ll_hist_vector[variable_index],Form("data %s, %s training",ec_vec[ec2_idx].c_str(),ec_vec[ec1_idx].c_str()));
            current2_legend->SetBorderSize(1);
            current2_legend->SetTextFont(42);
            current2_legend->SetFillStyle(0);
            current2_legend->SetTextSize(0.02);
            current2_legend->Draw(); 




          }
          canv.SaveAs(Form("./tmva_ll_check_qta%i_Q%s_C1%s_C2-T%s.png",q_idx,mass_cuts[mass_range].c_str(),ec_vec[ec2_idx].c_str(),ec_vec[ec1_idx].c_str()));
        }
      }
    }
  }
}

int main(){
  check(); 
}
