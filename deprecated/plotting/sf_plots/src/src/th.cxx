#include <th.hxx>

void th( ){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);


  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip; wip.SetNDC(); wip.SetTextFont(42);
  wip.SetTextSize(0.038); wip.SetTextColor(1);
  TLatex sim;  sim.SetNDC(); sim.SetTextFont(42);
  sim.SetTextSize(0.038); sim.SetTextColor(1);

  std::vector< double > qta_bins = { 15, -10.0, 20 };

  TFile * sf_file = new TFile("~/other/ScaleFactory/ForTamuna/signal_00_P5000.root", "READ" );
  TTree * sf_tree = (TTree*) sf_file->Get( "trout" );

  int bins = (int) qta_bins.at( 0 );
  double bin_min = qta_bins.at( 1 );
  double bin_max = qta_bins.at( 2 );
  
  double width = ( bin_max - bin_min )/( (double) bins );
  std::string var = "qtA";
  std::string unit = "GeV";

  
  std::vector<int> mass_ranges = { 0, 3, 4, 5, 12 };
  std::map< int, std::string > lambda_cut;
  lambda_cut[0] =   "lambda>15&&lambda<200";
  lambda_cut[3] =   "lambda>25&&lambda<50";
  lambda_cut[4] =   "lambda>50&&lambda<100";
  lambda_cut[5] =   "lambda>100&&lambda<200";
  lambda_cut[12] =  "lambda>25&&lambda<200";
  
  std::ofstream table_file;
  table_file.open( Form( "%s_table.tex", var.c_str() ));
  table_file << "\\documentclass[a3paper, portrait]{article}\n";
  table_file << "\\usepackage[a3paper, margin=10pt, portrait]{geometry}\n";
  table_file << "\\begin{document}\n";
  table_file << "\\begin{center}\n";

  std::string line_str{ "|c|" };
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ line_str += "r|"; }
  table_file << Form("\\begin{tabular}{%s}\n\\hline\n", line_str.c_str() );
  line_str.clear();
  
  for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ line_str += Form("& %s-%i", var.c_str(), bin_idx ); }
  table_file << Form("SF %s \\\\ \n", line_str.c_str() );
  table_file << "\\hline\n\\hline\n";
  line_str.clear();


  for ( int mass_number : mass_ranges ){

    TH1F * rms_hist = new TH1F( Form( "qtA_rms_%i", mass_number ), "", bins, bin_min, bin_max );
    TH1F * mean_hist = new TH1F( Form( "qtA_mean_%i", mass_number ), "", bins, bin_min, bin_max );

    std::vector< TH1F* > sf_hists;

    for ( int idx = 0; idx < bins; idx++ ){
      double lo_edge = bin_min + ( idx*width ); 
      double hi_edge = bin_min + ( (1+idx)*width ); 
      std::string cut = std::string( Form("qtA>%f&&qtA<%f", lo_edge, hi_edge) );
      cut = cut + "&&" + lambda_cut[mass_number];
      TH1F * bin_hist = new TH1F( Form( "trig_SF_qta_%i", idx), "", 50, 0.5, 1.5 );
      sf_tree->Draw( Form("SF>>trig_SF_qta_%i", idx ), cut.c_str(), "" );
      mean_hist->SetBinContent( idx+1, bin_hist->GetMean()  );
      std::cout << bin_hist->Integral() << std::endl;
      rms_hist->SetBinContent( idx+1, bin_hist->GetRMS() );
      mean_hist->SetBinError( idx+1, bin_hist->GetRMS() );
      sf_hists.push_back( bin_hist );
      delete bin_hist;
    }
    
    
    for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ line_str += Form("& %.1f",
        mean_hist->GetBinContent(bin_idx)*100.0 ); }
    table_file << Form("SF Mean Q%i %s \\\\ \n", mass_number, line_str.c_str() );
    line_str.clear();
    table_file << "\\hline\n";
    for ( int bin_idx = 1; bin_idx <= bins; bin_idx++ ){ line_str += Form("& %.1f",
        rms_hist->GetBinContent(bin_idx)*100.0 ); }
    table_file << Form("SF RMS Q%i %s \\\\ \n", mass_number, line_str.c_str() );
    line_str.clear();
    
    TCanvas * trig_canv = new TCanvas("canv_trig","",200,200,2000,1000);
    trig_canv->Divide(2,1);

    trig_canv->cd( 1 );
    mean_hist->Draw("E1");
    mean_hist->Draw("HIST SAME");
    mean_hist->SetMarkerStyle(21);
    mean_hist->SetLineColor(2);
    mean_hist->SetLineWidth(2);
    mean_hist->GetYaxis()->SetRangeUser(0,1.25);
    mean_hist->GetYaxis()->SetTitle( Form("Mean SF/%.3f %s", width, unit.c_str()));
    mean_hist->GetXaxis()->SetTitle( Form("qtA (%s)", unit.c_str()));
    mean_hist->GetYaxis()->SetLabelSize(0.035);
    mean_hist->GetYaxis()->SetTitleSize(0.035);
    mean_hist->GetXaxis()->SetLabelSize(0.035);
    mean_hist->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    TLegend * mean_legend = new TLegend(0.5,0.65,0.83,0.8);
    mean_legend->SetBorderSize(0);
    mean_legend->SetFillColor(0);
    mean_legend->SetFillStyle(0);
    mean_legend->SetTextFont(42);
    mean_legend->SetTextSize(0.025);
    mean_legend->AddEntry( mean_hist,Form("Mean Trigger SF Q%i", mass_number ),"LP");
    mean_legend->Draw();
    TLatex mean_ltx_title;
    mean_ltx_title.SetTextSize(0.04);
    mean_ltx_title.DrawLatexNDC(0.2,0.825,Form("Mean Trigger Scale Factor Q%i", mass_number));  
    ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
    wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

    trig_canv->cd( 2 );
    rms_hist->Draw("HIST");
    rms_hist->SetMarkerStyle(21);
    rms_hist->SetLineColor(2);
    rms_hist->GetYaxis()->SetRangeUser(0,rms_hist->GetMaximum()*1.4);
    rms_hist->GetYaxis()->SetTitle( Form("SF RMS/%.3f %s", width, unit.c_str()));
    rms_hist->GetXaxis()->SetTitle( Form("qtA (%s)", unit.c_str()));
    rms_hist->GetYaxis()->SetLabelSize(0.035);
    rms_hist->GetYaxis()->SetTitleSize(0.035);
    rms_hist->GetXaxis()->SetLabelSize(0.035);
    rms_hist->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    TLegend * rms_legend = new TLegend(0.65,0.65,0.83,0.8);
    rms_legend->SetBorderSize(0);
    rms_legend->SetFillColor(0);
    rms_legend->SetFillStyle(0);
    rms_legend->SetTextFont(42);
    rms_legend->SetTextSize(0.025);
    rms_legend->AddEntry(rms_hist, "SF RMS","L");
    rms_legend->Draw();
    TLatex rms_ltx_title;
    rms_ltx_title.SetTextSize(0.03);
    rms_ltx_title.DrawLatexNDC(0.2,0.825,Form("SF RMS Q%i", mass_number ));  
    ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
    wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

    trig_canv->SaveAs(Form("./sf_trig_Q%i.png",mass_number));


    for ( int idx = 0; idx < bins; idx++ ){
      double lo_edge = bin_min + ( idx*width ); 
      double hi_edge = bin_min + ( (1+idx)*width ); 
      std::string cut = std::string( Form("qtA>%f&&qtA<%f", lo_edge, hi_edge) );
      cut = cut + "&&" + lambda_cut[mass_number];
      TH1F * bin_hist = new TH1F( Form( "phot_SF_qta_%i", idx), "", 50, 0.5, 1.5 );
      sf_tree->Draw( Form("photon_id_sf>>phot_SF_qta_%i", idx ), cut.c_str(), "" );
      mean_hist->SetBinContent( idx+1, bin_hist->GetMean()  );
      std::cout << bin_hist->Integral() << std::endl;
      rms_hist->SetBinContent( idx+1, bin_hist->GetRMS()  );
      mean_hist->SetBinError( idx+1, bin_hist->GetRMS()  );
      sf_hists.push_back( bin_hist );
      delete bin_hist;
    }
    TCanvas * canv = new TCanvas("canv_phot","",200,200,2000,1000);
    canv->Divide(2,1);

    canv->cd( 1 );
    mean_hist->Draw("E1");
    mean_hist->Draw("HIST SAME");
    mean_hist->SetMarkerStyle(21);
    mean_hist->SetLineColor(2);
    mean_hist->SetLineWidth(2);
    mean_hist->GetYaxis()->SetRangeUser(0,1.25);
    mean_hist->GetYaxis()->SetTitle( Form("Mean SF/%.3f %s", width, unit.c_str()));
    mean_hist->GetXaxis()->SetTitle( Form("qtA (%s)", unit.c_str()));
    mean_hist->GetYaxis()->SetLabelSize(0.035);
    mean_hist->GetYaxis()->SetTitleSize(0.035);
    mean_hist->GetXaxis()->SetLabelSize(0.035);
    mean_hist->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    TLegend * phot_mean_legend = new TLegend(0.5,0.65,0.83,0.8);
    phot_mean_legend->SetBorderSize(0);
    phot_mean_legend->SetFillColor(0);
    phot_mean_legend->SetFillStyle(0);
    phot_mean_legend->SetTextFont(42);
    phot_mean_legend->SetTextSize(0.025);
    phot_mean_legend->AddEntry( mean_hist, Form("Mean Photon SF Q%i", mass_number ),"LP");
    phot_mean_legend->Draw();
    TLatex phot_mean_ltx_title;
    phot_mean_ltx_title.SetTextSize(0.04);
    phot_mean_ltx_title.DrawLatexNDC(0.2,0.825,Form("Mean Photon Scale Factor Q%i", mass_number));  
    ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
    wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

    canv->cd( 2 );
    rms_hist->Draw("HIST");
    rms_hist->SetMarkerStyle(21);
    rms_hist->SetLineColor(2);
    rms_hist->GetYaxis()->SetRangeUser(0,rms_hist->GetMaximum()*1.4);
    rms_hist->GetYaxis()->SetTitle( Form("SF RMS/%.3f %s", width, unit.c_str()));
    rms_hist->GetXaxis()->SetTitle( Form("qtA (%s)", unit.c_str()));
    rms_hist->GetYaxis()->SetLabelSize(0.035);
    rms_hist->GetYaxis()->SetTitleSize(0.035);
    rms_hist->GetXaxis()->SetLabelSize(0.035);
    rms_hist->GetXaxis()->SetTitleSize(0.035);
    gPad->Update();
    TLegend * phot_rms_legend = new TLegend(0.65,0.65,0.83,0.8);
    phot_rms_legend->SetBorderSize(0);
    phot_rms_legend->SetFillColor(0);
    phot_rms_legend->SetFillStyle(0);
    phot_rms_legend->SetTextFont(42);
    phot_rms_legend->SetTextSize(0.025);
    phot_rms_legend->AddEntry(rms_hist, "SF RMS","L");
    phot_rms_legend->Draw();
    TLatex phot_rms_ltx_title;
    phot_rms_ltx_title.SetTextSize(0.03);
    phot_rms_ltx_title.DrawLatexNDC(0.2,0.825,Form("SF RMS Q%i", mass_number ));  
    ATLAS.DrawLatexNDC(0.2,0.75,"ATLAS");
    wip.DrawLatexNDC(0.2,0.7,"Work In Progress");

    canv->SaveAs(Form("./sf_phot_Q%i.png",mass_number));

  }
  table_file << "\\hline\n";
  table_file << "\\end{tabular}\n";
  table_file << "\\end{center}\n";
  table_file << "\\end{document}\n";
  table_file.close();

}




