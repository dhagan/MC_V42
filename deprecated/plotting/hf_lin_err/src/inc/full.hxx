#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>
#include "TString.h"
#include "TROOT.h"

#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <math.h>
#include <fstream>
#include <cassert>

#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

//#include "/home/crucible/analysis/main/MC_V42/atlasstyle/AtlasLabels.h"
//#include "/home/crucible/analysis/main/MC_V42/atlasstyle/AtlasUtils.h"
//#include "/home/crucible/analysis/main/MC_V42/atlasstyle/AtlasStyle.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasLabels.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasUtils.h"
#include "/home/atlas/dhagan/analysis/analysisFork/atlasstyle/AtlasStyle.h"



void full(std::string str_hstfc_file, std::string str_data_file, std::string str_sign_file, std::string str_bckg_file, std::string str_abin_vars, std::string str_spec_vars, std::string slice, std::string unique,std::string lin_files, std::string str_dsign_file,std::string str_dbckg_file, std::string str_sub_path );
