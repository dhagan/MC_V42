#include <fitr.hxx>
#include <split_string.hxx>
#include <dscb.hxx>



void fitr(std::string filepath, std::string abin_vars, std::string spec_vars, std::string slice, std::string type, std::string unique){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  
  std::vector<double> std_dev;
  std::vector<double> mean;

  std::vector<int> mass_indices = {0,3,4,5,12};

  std::vector<std::string> unique_vec;
  split_strings(unique_vec, unique, ":");

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};

  TFile * results_file = new TFile(filepath.c_str(),"READ");


  char mass_char[100];
  for (int & mass_index : mass_indices){
    
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
    for (std::string abin_var : vec_abin_vars){

      std::vector<double> bins = analysis_bins[abin_var];
      //int abin_bins     = (int)     bins.at(0);
      //double abin_min   = (double)  bins.at(1);
      //double abin_max   = (double)  bins.at(2);

      for (std::string spec_var : vec_spec_vars){
        TF1 * fit_sg;
        TF1 * fit_dg;
        TF1 * fit_gc;
        //TF1 * fit_dc;
        //TF1 * fit_ds;

        //std::vector<double> sg_mean(unique_vec.size(),0.0); 
        //std::vector<double> sg_sigm(unique_vec.size(),0.0);
        int n = 4;
        Double_t perc[4] = { 5,10,25,50 };
        Double_t perc_err[4] = { 0,0,0,0 };
        Double_t sg_mean[4];
        Double_t sg_sigm[4];
        Double_t sg_cnst[4];
        Double_t dg_mean[4];
        Double_t dg_sig1[4];
        Double_t dg_sig2[4];
        Double_t dg_cns1[4];
        Double_t dg_cns2[4];
        Double_t gc_mean[4];
        Double_t gc_sigm[4];
        Double_t gc_cns1[4];
        Double_t gc_cns2[4];
        //Double_t dc_mean[4];
        //Double_t dc_sigm[4];
        //Double_t dc_cnst[4];
        //Double_t ds_mean[4];
        //Double_t ds_sigm[4];
        //Double_t ds_cnst[4];
  
        Double_t sg_mean_err[4];
        Double_t sg_sigm_err[4];
        Double_t sg_cnst_err[4];
        Double_t dg_mean_err[4];
        Double_t dg_sig1_err[4];
        Double_t dg_sig2_err[4];
        Double_t dg_cns1_err[4];
        Double_t dg_cns2_err[4];
        Double_t gc_mean_err[4];
        Double_t gc_sigm_err[4];
        Double_t gc_cns1_err[4];
        Double_t gc_cns2_err[4];
        //Double_t dc_mean_err[4];
        //Double_t dc_sigm_err[4];
        //Double_t dc_cnst_err[4];
        //Double_t ds_mean_err[4];
        //Double_t ds_sigm_err[4];
        //Double_t ds_cnst_err[4];

        int point = 0;
        for (std::string unique_str : unique_vec){

          std::string suffix(Form("cfit_%s_%s_%s_Q%i_%s", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index, unique_str.c_str()));
          fit_sg      =   (TF1*) results_file->Get(("fit_sg"       + suffix ).c_str());
          fit_dg      =   (TF1*) results_file->Get(("fit_dg_cc"    + suffix ).c_str());
          fit_gc      =   (TF1*) results_file->Get(("fit_gc"       + suffix ).c_str());
          //fit_dc      =   (TF1*) results_file->Get(("fit_dscb"     + suffix ).c_str());
          //fit_ds      =   (TF1*) results_file->Get(("fit_dscb_sym" + suffix ).c_str());
    
          sg_mean[point] = fit_sg->GetParameter("X");
          sg_sigm[point] = fit_sg->GetParameter("SIG");
          sg_cnst[point] = fit_sg->GetParameter("C");
          sg_mean_err[point] = fit_sg->GetParError(1);
          sg_sigm_err[point] = fit_sg->GetParError(2);
          sg_cnst_err[point] = fit_sg->GetParError(0);

          // the 2.0 scaling is to scale the plot for the second gaussian down for the plot
          dg_mean[point] = fit_dg->GetParameter("X1");
          dg_sig1[point] = fit_dg->GetParameter("SIG1");
          dg_sig2[point] = fit_dg->GetParameter("SIG2");
          dg_cns1[point] = fit_dg->GetParameter("C1");
          dg_cns2[point] = fit_dg->GetParameter("C2");
          dg_mean_err[point] = fit_dg->GetParError(1);
          dg_sig1_err[point] = fit_dg->GetParError(2);
          dg_sig2_err[point] = fit_dg->GetParError(4);
          dg_cns1_err[point] = fit_dg->GetParError(0);
          dg_cns2_err[point] = fit_dg->GetParError(3);

          gc_mean[point] = fit_gc->GetParameter("X1");
          gc_sigm[point] = fit_gc->GetParameter("SIG1");
          gc_cns1[point] = fit_gc->GetParameter("C1");
          gc_cns2[point] = fit_gc->GetParameter("C2");
          gc_mean_err[point] = fit_gc->GetParError(1);
          gc_sigm_err[point] = fit_gc->GetParError(2);
          gc_cns1_err[point] = fit_gc->GetParError(0);
          gc_cns2_err[point] = fit_gc->GetParError(3);

          //dc_mean[point] = fit_dc->GetParameter("mean");
          //dc_sigm[point] = fit_dc->GetParameter("\\sigma");
          //dc_cnst[point] = fit_dc->GetParameter("Norm");
          //dc_mean_err[point] = fit_dc->GetParError(4);
          //dc_sigm_err[point] = fit_dc->GetParError(5);
          //dc_cnst_err[point] = fit_dc->GetParError(6);

          //ds_mean[point] = fit_ds->GetParameter("MEAN");
          //ds_sigm[point] = fit_ds->GetParameter("\\SIG");
          //ds_cnst[point] = fit_ds->GetParameter("\\SIG");
          //ds_mean_err[point] = fit_ds->GetParError(2);
          //ds_sigm_err[point] = fit_ds->GetParError(3);
          //ds_cnst_err[point] = fit_ds->GetParError(4);


          point++;
        }
        
        TLatex ATLAS;
        ATLAS.SetNDC();
        ATLAS.SetTextFont(72);
        ATLAS.SetTextColor(1);
        TLatex wip; 
        wip.SetNDC();
        wip.SetTextFont(42);
        wip.SetTextSize(0.038);
        wip.SetTextColor(1);
        TLatex sim; 
        sim.SetNDC();
        sim.SetTextFont(42);
        sim.SetTextSize(0.038);
        sim.SetTextColor(1);


        TCanvas * mean_canvas = new TCanvas(Form("mean_canv_%i",mass_index),"",200,200,3000,1000); 
        mean_canvas->Divide(3,1);

        mean_canvas->cd(1);
        TGraphErrors * sg_mean_graph = new TGraphErrors(n,perc,sg_mean, perc_err, sg_mean_err);
        sg_mean_graph->SetLineColorAlpha(kRed+1,0.9);
        sg_mean_graph->Draw("AC*");
        sg_mean_graph->SetMarkerStyle(21);
        sg_mean_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        sg_mean_graph->GetYaxis()->SetTitle("Fit Mean qTA (GeV)");
        sg_mean_graph->GetYaxis()->SetRangeUser(0,4);
        sg_mean_graph->GetXaxis()->SetRangeUser(0,75);
        sg_mean_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        TLatex pad_1_ltx;
        pad_1_ltx.SetTextSize(0.03);
        pad_1_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));  
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");

        //sg->GetYaxis->SetRangeUser();
        mean_canvas->cd(2);
        TGraphErrors * dg_mean_graph = new TGraphErrors(n,perc,dg_mean,perc_err,dg_mean_err);
        dg_mean_graph->SetLineColorAlpha(kRed+1,0.9);
        dg_mean_graph->Draw("AC*");
        dg_mean_graph->SetMarkerStyle(21);
        dg_mean_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        dg_mean_graph->GetYaxis()->SetTitle("Fit Mean qTA (GeV)");
        dg_mean_graph->GetYaxis()->SetRangeUser(0,4);
        dg_mean_graph->GetXaxis()->SetRangeUser(0,75);
        dg_mean_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        TLatex pad_2_ltx;
        pad_2_ltx.SetTextSize(0.03);
        pad_2_ltx.DrawLatexNDC(0.2,0.97,Form("Double gauss, common centre fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");

        mean_canvas->cd(3);
        TGraphErrors * gc_mean_graph = new TGraphErrors(n,perc,gc_mean,perc_err,gc_mean_err);
        gc_mean_graph->SetLineColorAlpha(kRed+1,0.9);
        gc_mean_graph->Draw("AC*");
        gc_mean_graph->SetMarkerStyle(21);
        gc_mean_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        gc_mean_graph->GetYaxis()->SetTitle("Fit Mean qTA (GeV)");
        gc_mean_graph->GetYaxis()->SetRangeUser(0,4);
        gc_mean_graph->GetXaxis()->SetRangeUser(0,75);
        gc_mean_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        TLatex pad_3_ltx;
        pad_3_ltx.SetTextSize(0.03);
        pad_3_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss with constant fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");

        mean_canvas->SaveAs(Form("fitr_mean_%s_%s_%s_Q%i.png", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        mean_canvas->Close();
        delete mean_canvas;



        TCanvas * sigm_canvas = new TCanvas(Form("sigm_canv_%i",mass_index),"",200,200,3000,1000); 
        sigm_canvas->Divide(3,1);
        sigm_canvas->cd(1);
        TGraphErrors * sg_sigm_graph = new TGraphErrors(n,perc,sg_sigm, perc_err, sg_sigm_err);
        sg_sigm_graph->Draw("AC*");
        sg_sigm_graph->SetMarkerStyle(21);
        sg_sigm_graph->SetLineColorAlpha(kRed+1,0.9);
        sg_sigm_graph->GetYaxis()->SetRangeUser(0,14);
        sg_sigm_graph->GetXaxis()->SetRangeUser(0,75);
        sg_sigm_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        sg_sigm_graph->GetYaxis()->SetTitle("Fitted Sigma qTA (GeV)");
        sg_sigm_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        pad_1_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));  
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");

        //sg->GetYaxis->SetRangeUser();
        sigm_canvas->cd(2);
        TGraphErrors * dg_sig1_graph = new TGraphErrors(n,perc,dg_sig1,perc_err,dg_sig1_err);
        TGraphErrors * dg_sig2_graph = new TGraphErrors(n,perc,dg_sig2,perc_err,dg_sig2_err);
        dg_sig1_graph->Draw("AC*");
        dg_sig1_graph->SetMarkerStyle(21);
        dg_sig1_graph->SetLineColorAlpha(kRed+1,0.9);
        dg_sig1_graph->GetYaxis()->SetRangeUser(0,14);
        dg_sig1_graph->GetXaxis()->SetLimits(0,75);
        dg_sig1_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        dg_sig1_graph->GetYaxis()->SetTitle("Double Gaussian Fitted Sigmas qTA (GeV)");
        dg_sig1_graph->GetYaxis()->SetTitleSize(0.04);
        //dg_sig1_graph->GetYaxis()->SetLabelColor(kRed+1);
        dg_sig1_graph->SetLineWidth(3);
        dg_sig2_graph->Draw("SAME C*");
        dg_sig2_graph->SetMarkerStyle(21);
        dg_sig2_graph->GetXaxis()->SetRangeUser(0,75);
        dg_sig2_graph->SetLineColorAlpha(kBlue+1,0.5);
        dg_sig2_graph->SetLineWidth(3);
        gPad->SetTicks(1,0);
        //TGaxis * gauss2_axis = new TGaxis(75,0,75,7,0,14,608,"+L");
        //gauss2_axis->SetName("gauss2_axis");
        //gauss2_axis->SetLabelColor(kBlue+1);
        //gauss2_axis->SetLabelFont(42);
        //gauss2_axis->Draw();  
        TLegend gauss2_legend(0.7, 0.8, 0.925,  0.925);
        gauss2_legend.SetTextSize(0.025);
        gauss2_legend.SetFillStyle(0);
        gauss2_legend.SetTextFont(42);
        gauss2_legend.AddEntry(dg_sig1_graph, "Narrow gaussian", "lep");
        gauss2_legend.AddEntry(dg_sig2_graph, "Wide gaussian",  "l");
        gauss2_legend.SetBorderSize(0);
        gauss2_legend.Draw();
        gPad->Modified(); gPad->Update();
        pad_2_ltx.DrawLatexNDC(0.2,0.97,Form("Double gauss, common centre fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");

        sigm_canvas->cd(3);
        TGraphErrors * gc_sigm_graph = new TGraphErrors(n,perc,gc_sigm,perc_err,gc_sigm_err);
        gc_sigm_graph->Draw("AC*");
        gc_sigm_graph->SetMarkerStyle(21);
        gc_sigm_graph->SetLineColorAlpha(kRed+1,0.9);
        gc_sigm_graph->GetYaxis()->SetRangeUser(0,14);
        gc_sigm_graph->GetXaxis()->SetRangeUser(0,75);
        gc_sigm_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        gc_sigm_graph->GetYaxis()->SetTitle("Fitted Sigma qTA (GeV)");
        gc_sigm_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        pad_3_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss with constant fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");

        sigm_canvas->SaveAs(Form("fitr_sigm_%s_%s_%s_Q%i.png", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        sigm_canvas->Close();
        delete sigm_canvas;



        
        TCanvas * cnst_canvas = new TCanvas(Form("cnst_canv_%i",mass_index),"",200,200,3000,1000); 
        cnst_canvas->Divide(3,1);
        cnst_canvas->cd(1);
        TGraphErrors * sg_cnst_graph = new TGraphErrors(n,perc,sg_cnst, perc_err, sg_cnst_err);
        sg_cnst_graph->SetLineColorAlpha(kRed+1,0.9);
        sg_cnst_graph->Draw("AC*");
        sg_cnst_graph->SetMarkerStyle(21);
        sg_cnst_graph->GetYaxis()->SetRangeUser(0,3500);
        sg_cnst_graph->GetXaxis()->SetRangeUser(0,75);
        sg_cnst_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        sg_cnst_graph->GetYaxis()->SetTitle("Fitted Normalization (GeV)");
        sg_cnst_graph->GetYaxis()->SetTitleOffset(1.65);
        sg_cnst_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        pad_1_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));  
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");

        //sg->GetYaxis->SetRangeUser();
        cnst_canvas->cd(2);
        TGraphErrors * dg_cns1_graph = new TGraphErrors(n,perc,dg_cns1,perc_err,dg_cns1_err);
        TGraphErrors * dg_cns2_graph = new TGraphErrors(n,perc,dg_cns2,perc_err,dg_cns2_err);
        dg_cns1_graph->SetLineColorAlpha(kRed+1,0.9);
        dg_cns1_graph->Draw("AC*");
        dg_cns1_graph->SetMarkerStyle(21);
        dg_cns1_graph->GetYaxis()->SetRangeUser(0,3500);
        dg_cns1_graph->GetXaxis()->SetRangeUser(0,75);
        dg_cns1_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        dg_cns1_graph->GetYaxis()->SetTitle("Fitted Normalization (GeV)");
        dg_cns1_graph->GetYaxis()->SetTitleOffset(1.65);
        dg_cns1_graph->SetLineWidth(3);
        dg_cns2_graph->SetLineColorAlpha(kBlue+1,0.9);
        dg_cns2_graph->Draw("SAME C*");
        dg_cns2_graph->SetMarkerStyle(21);
        dg_cns1_graph->SetLineWidth(3);
        dg_cns2_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        pad_2_ltx.DrawLatexNDC(0.2,0.97,Form("Double gauss, common centre fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");
        TLegend const2_legend(0.7, 0.8, 0.925,  0.925);
        const2_legend.SetTextSize(0.025);
        const2_legend.SetFillStyle(0);
        const2_legend.SetTextFont(42);
        const2_legend.AddEntry(dg_cns1_graph, "Narrow gaussian", "lep");
        const2_legend.AddEntry(dg_cns2_graph, "Wide gaussian",  "l");
        const2_legend.SetBorderSize(0);
        const2_legend.Draw();


        cnst_canvas->cd(3);
        TGraphErrors * gc_cns1_graph = new TGraphErrors(n,perc,gc_cns1,perc_err,gc_cns1_err);
        TGraphErrors * gc_cns2_graph = new TGraphErrors(n,perc,gc_cns2,perc_err,gc_cns2_err);
        gc_cns1_graph->SetLineColorAlpha(kRed+1,0.9);
        gc_cns1_graph->Draw("AC*");
        gc_cns1_graph->SetMarkerStyle(21);
        gc_cns1_graph->GetYaxis()->SetRangeUser(0,3500);
        gc_cns1_graph->GetXaxis()->SetRangeUser(0,75);
        gc_cns1_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        gc_cns1_graph->GetYaxis()->SetTitle("Fitted Normalization (GeV)");
        gc_cns1_graph->GetYaxis()->SetTitleOffset(1.65);
        gc_cns2_graph->SetMarkerStyle(21);
        gc_cns2_graph->SetLineColorAlpha(kBlue+1,0.9);
        gc_cns2_graph->Draw("SAME C*");
        gc_cns1_graph->SetLineWidth(3);
        gc_cns2_graph->SetLineWidth(3);
        gPad->Modified(); gPad->Update();
        pad_3_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss with constant fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");
        TLegend const3_legend(0.7, 0.8, 0.925,  0.925);
        const3_legend.SetTextSize(0.025);
        const3_legend.SetFillStyle(0);
        const3_legend.SetTextFont(42);
        const3_legend.AddEntry(dg_cns1_graph, "Gaussian", "lep");
        const3_legend.AddEntry(dg_cns2_graph, "Constant",  "l");
        const3_legend.SetBorderSize(0);
        const3_legend.Draw();


        cnst_canvas->SaveAs(Form("fitr_cnst_%s_%s_%s_Q%i.png", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));

        
        
        //mean_canvas->cd(4);
        //TGraphErrors * dc_mean_graph = new TGraphErrors(n,perc,dc_mean,perc_err,dc_mean_err);
        //dc_mean_graph->SetLineColorAlpha(kRed+1,0.9);
        //dc_mean_graph->Draw("AC*");
        //dc_mean_graph->SetMarkerStyle(21);
        //dc_mean_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        //dc_mean_graph->GetYaxis()->SetTitle("Fit Mean qTA (GeV)");
        //dc_mean_graph->GetYaxis()->SetRangeUser(0,7);
        //dc_mean_graph->GetXaxis()->SetRangeUser(0,75);
        //dc_mean_graph->SetLineWidth(3);
        //gPad->Modified(); gPad->Update();
        //TLatex pad_4_ltx;
        //pad_4_ltx.SetTextSize(0.03);
        //pad_4_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");

        //mean_canvas->cd(5);
        //TGraphErrors * ds_mean_graph = new TGraphErrors(n,perc,ds_mean,perc_err,ds_mean_err);
        //ds_mean_graph->SetLineColorAlpha(kRed+1,0.9);
        //ds_mean_graph->Draw("AC*");
        //ds_mean_graph->SetMarkerStyle(21);
        //ds_mean_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        //ds_mean_graph->GetYaxis()->SetTitle("Fit Mean qTA (GeV)");
        //ds_mean_graph->GetYaxis()->SetRangeUser(0,7);
        //ds_mean_graph->GetXaxis()->SetRangeUser(0,75);
        //ds_mean_graph->SetLineWidth(3);
        //gPad->Modified(); gPad->Update();
        //TLatex pad_5_ltx;
        //pad_5_ltx.SetTextSize(0.03);
        //pad_5_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB SYM %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");

        
        //sigm_canvas->cd(4);
        //TGraphErrors * dc_sigm_graph = new TGraphErrors(n,perc,dc_sigm,perc_err,dc_sigm_err);
        //dc_sigm_graph->Draw("AC*");
        //dc_sigm_graph->SetMarkerStyle(21);
        //dc_sigm_graph->SetLineColorAlpha(kRed+1,0.9);
        //dc_sigm_graph->GetYaxis()->SetRangeUser(0,7);
        //dc_sigm_graph->GetXaxis()->SetRangeUser(0,75);
        //dc_sigm_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        //dc_sigm_graph->GetYaxis()->SetTitle("Fitted Sigma qTA (GeV)");
        //dc_sigm_graph->SetLineWidth(3);
        //gPad->Modified(); gPad->Update();
        //pad_4_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");

        //sigm_canvas->cd(5);
        //TGraphErrors * ds_sigm_graph = new TGraphErrors(n,perc,ds_sigm,perc_err,ds_sigm_err);
        //ds_sigm_graph->Draw("AC*");
        //ds_sigm_graph->SetMarkerStyle(21);
        //ds_sigm_graph->SetLineColorAlpha(kRed+1,0.9);
        //ds_sigm_graph->GetYaxis()->SetRangeUser(0,7);
        //ds_sigm_graph->GetXaxis()->SetRangeUser(0,75);
        //ds_sigm_graph->GetXaxis()->SetTitle("Percentage MC Signal");
        //ds_sigm_graph->GetYaxis()->SetTitle("Fitted Sigma qTA (GeV)");
        //ds_sigm_graph->SetLineWidth(3);
        //gPad->Modified(); gPad->Update();
        //pad_5_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB SYM %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");


        //cnst_canvas->cd(4);
        //TGraphErrors * dc_cnst_graph = new TGraphErrors(n,perc,dc_cnst,perc_err,dc_cnst_err);
        //dc_cnst_graph->SetMarkerStyle(21);
        //dc_cnst_graph->SetLineColorAlpha(kRed+1,0.9);
        //dc_cnst_graph->Draw("AC");
        //gPad->Modified(); gPad->Update();
        //pad_4_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");

        //cnst_canvas->cd(5);
        //TGraphErrors * ds_cnst_graph = new TGraphErrors(n,perc,ds_cnst,perc_err,ds_cnst_err);
        //ds_cnst_graph->SetMarkerStyle(21);
        //ds_cnst_graph->SetLineColorAlpha(kRed+1,0.9);
        //ds_cnst_graph->Draw("AC");
        //gPad->Modified(); gPad->Update();
        //pad_5_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB SYM %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");



      }
    }
  }
}
