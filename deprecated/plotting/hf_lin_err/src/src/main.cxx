#include <main.hxx>
#include <full.hxx>
#include <lovl.hxx>
#include <cfit.hxx>
#include <sfit.hxx>
#include <hadd.hxx>
#include <fitr.hxx>
#include <dcmp.hxx>
#include <hlrt.hxx>

int help(){

  std::cout << "h     --    help      --  output help" << std::endl;
  std::cout << "f     --    hfin      --  path to single histfactory output" << std::endl;
  std::cout << "d     --    data      --  path to a data file" << std::endl;
  std::cout << "s     --    sign      --  path to a sign file" << std::endl;
  std::cout << "b     --    bckg      --  path to a bckg file" << std::endl;
  std::cout << "a     --    avar      --  colon separated list of analysis bin variables" << std::endl;
  std::cout << "v     --    svar      --  colon separated list of spectator variables" << std::endl;
  std::cout << "t     --    slice     --  subtraction slice of interest, default 99" << std::endl;
  std::cout << "g     --    type      --  type of interest, data, sign, bckg" << std::endl;
  std::cout << "u     --    uniq      --  unique name for run" << std::endl;
  std::cout << "p     --    path      --  path and partial filename to files for recombination" << std::endl;
  std::cout << "l     --    linf      --  colon separated list of files for combination" << std::endl;
  std::cout << "e     --    dsign     --  path to signal component of simulated data file " << std::endl;
  std::cout << "w     --    dbckg     --  path to background component of simulated data file" << std::endl;
  std::cout << "m     --    mode      --  mode of execution:" << std::endl;
  std::cout << "                      --  LOVL - overlay linear files" << std::endl;
  std::cout << "                      --  CFIT - combine and fit linear files" << std::endl;
  std::cout << "                      --  SFIT - fit a histfactory output" << std::endl;
  std::cout << "                      --  FULL - plot a histfactory run with input/output" << std::endl;
  std::cout << "                      --  HADD - Comparison of linear histogram addition methods" << std::endl;



  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"hfin",        required_argument,        0,      'f'},
      {"data",        required_argument,        0,      'd'},
      {"sign",        required_argument,        0,      's'},
      {"bckg",        required_argument,        0,      'b'},
      {"avar",        required_argument,        0,      'a'},
      {"svar",        required_argument,        0,      'v'},
      {"slice",       required_argument,        0,      't'},
      {"type",        required_argument,        0,      'g'},
      {"uniq",        required_argument,        0,      'u'},
      {"linf",        required_argument,        0,      'l'},
      {"dsign",       required_argument,        0,      'e'},
      {"dbckg",       required_argument,        0,      'w'},
      {"spath",       required_argument,        0,      'p'},
      {0,             0,                        0,      0}
    };

  std::string str_hstfc_file;
  std::string str_data_file, str_sign_file, str_bckg_file;
  std::string str_abin_vars, str_spec_vars;
  std::string str_slice{"99"}, str_unique, str_path;
  std::string str_lin_files, str_datasign_file, str_databckg_file;
  std::string str_mode, str_type;

  do {
    option = getopt_long(argc, argv, "f:d:s:b:t:a:v:u:l:e:w:p:g:m:h", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'f': 
        str_hstfc_file      = std::string(optarg);
        break;
      case 'd': 
        str_data_file       = std::string(optarg);
        break;
      case 's': 
        str_sign_file       = std::string(optarg);
        break;
      case 'b': 
        str_bckg_file       = std::string(optarg);
        break;
      case 'a': 
        str_abin_vars       = std::string(optarg);
        break;
      case 'v': 
        str_spec_vars       = std::string(optarg);
        break;
      case 't':
        str_slice           = std::string(optarg);
        break;
      case 'u':
        str_unique          = std::string(optarg);
        break;
      case 'l':
        str_lin_files       = std::string(optarg);
        break;
      case 'e':
        str_datasign_file   = std::string(optarg);
        break;
      case 'w':
        str_databckg_file   = std::string(optarg);
        break;
      case 'p':
        str_path            = std::string(optarg);
        break;
      case 'm':
        str_mode            = std::string(optarg);
        break;
      case 'g':
        str_type            = std::string(optarg);
        break;
    }
  } while (option != -1);
  

  if (str_mode.empty()) { std::cout << "no mode provided" << std::endl; return help(); }

  if (str_mode.find("FULL") != std::string::npos){
    full(str_hstfc_file, str_data_file, str_sign_file, str_bckg_file, str_abin_vars, str_spec_vars, str_slice, str_unique, str_lin_files, str_datasign_file, str_databckg_file, str_path);  
  }

  if (str_mode.find("LOVL") != std::string::npos){
    if (str_path.empty() || str_lin_files.empty() || str_abin_vars.empty() || str_spec_vars.empty() || str_type.empty()){
      std::cout << "Mode LOVL (overlay of linear files) requires the following arguments:" << std::endl;
      std::cout << "  filepath" << std::endl << "  files" << std::endl;
      std::cout << "  filepath" << std::endl <<  "  analysis vars" << std::endl;
      std::cout << "  spectator vars" << std::endl << "  type" << std::endl;
      std::cout << "  unique" << std::endl; return help();
    }
    lovl(str_path, str_lin_files,str_abin_vars, str_spec_vars, str_slice, str_type, str_unique.c_str());
    return 0;
  }

  if (str_mode.find("SFIT") != std::string::npos){
    if (str_hstfc_file.empty() || str_abin_vars.empty() || str_spec_vars.empty() || str_type.empty() || str_slice.empty() || str_unique.empty()){
      std::cout << "Mode SFIT (fitting of single file) requires the following arguments:" << std::endl;
      std::cout << "  file" << std::endl;
      std::cout << "  slice" << std::endl <<  "  analysis vars" << std::endl;
      std::cout << "  spectator vars" << std::endl << "  type" << std::endl;
      std::cout << "  unique" << std::endl; return help();
    }
    sfit(str_hstfc_file, str_abin_vars, str_spec_vars, str_type,str_slice,str_unique);
    return 0;
  }

  
  if (str_mode.find("CFIT") != std::string::npos){
    if (str_path.empty() || str_lin_files.empty() || str_abin_vars.empty() || str_spec_vars.empty() || str_type.empty() || str_unique.empty()){
      std::cout << "Mode CFIT (fitting of combined linear files) requires the following arguments:" << std::endl;
      std::cout << "  filepath" << std::endl << "  files" << std::endl;
      std::cout << "  slice" << std::endl <<  "  analysis vars" << std::endl;
      std::cout << "  spectator vars" << std::endl << "  type" << std::endl;
      std::cout << "  unique" << std::endl; return help();
    }
    cfit(str_path,str_lin_files, str_abin_vars, str_spec_vars, str_slice, str_type, str_unique);
    return 0;
  }

  if (str_mode.find("HADD") != std::string::npos){
    if (str_path.empty() || str_lin_files.empty() || str_abin_vars.empty() || str_spec_vars.empty() || str_type.empty() || str_unique.empty()){
      std::cout << "Mode HADD (comparison of histogram recombination methods) requires the following arguments:" << std::endl;
      std::cout << "  filepath" << std::endl << "  files" << std::endl;
      std::cout << "  slice" << std::endl <<  "  analysis vars" << std::endl;
      std::cout << "  spectator vars" << std::endl << "  type" << std::endl;
      std::cout << "  unique" << std::endl; return help();
    }
    hadd(str_path,str_lin_files, str_abin_vars, str_spec_vars, str_slice, str_type, str_unique);
    return 0;
  }
  
  if (str_mode.find("FITR") != std::string::npos){
    if (str_path.empty() || str_abin_vars.empty() || str_spec_vars.empty() || str_type.empty() || str_unique.empty()){
      std::cout << "Mode FITR (fitting of combined linear files) requires the following arguments:" << std::endl;
      std::cout << "  filepath" << std::endl << "  files" << std::endl;
      std::cout << "  slice" << std::endl <<  "  analysis vars" << std::endl;
      std::cout << "  spectator vars" << std::endl << "  type" << std::endl;
      std::cout << "  unique" << std::endl; return help();
    }
    fitr(str_path, str_abin_vars, str_spec_vars, str_slice, str_type, str_unique);
    return 0;
  }

  if (str_mode.find("HLRT") != std::string::npos){
    if (str_path.empty() || str_abin_vars.empty() || str_spec_vars.empty() || str_type.empty() || str_unique.empty()){
      std::cout << "Mode HLRT (ratio low/high phi) requires the following arguments:" << std::endl;
      std::cout << "  filepath" << std::endl << "  files" << std::endl;
      std::cout << "  slice" << std::endl << "  spectator vars" << std::endl;
      std::cout << "  type" << std::endl << "  unique" << std::endl; return help();
    }
    hlrt(str_path, str_lin_files, str_spec_vars, str_slice, str_type, str_unique);
    return 0;
  }

  if (str_mode.find("DCMP") != std::string::npos){
      //std::cout << "Mode CMP (fitting of combined linear files) requires the following arguments:" << std::endl;
      //std::cout << "path to data files, -d, used for signal component of data" << std::endl;

    dcmp(str_hstfc_file, str_sign_file, str_data_file, str_lin_files, str_abin_vars, str_spec_vars, str_unique );
    return 0;
  }

  std::cout << "Mode provided does not match those available" << std::endl;
  return help();
}

