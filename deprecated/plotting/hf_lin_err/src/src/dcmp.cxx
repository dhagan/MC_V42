#include <dcmp.hxx>
#include <split_string.hxx>


void dcmp(std::string hfac_path, std::string sign_path, std::string dsig_path, std::string lin_files, std::string abin_vars, std::string spec_vars, std::string unique){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(10);
  

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::vector<std::string> vec_files;
  split_strings(vec_files, lin_files, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};


  std::vector<int> mass_indices = {0,3,4,5,12};
  char hfac_mass_name[100], sign_mass_name[100], dsig_mass_name[100];
  char hfac_comp_mass_name[100], sign_comp_mass_name[100], dsig_comp_mass_name[100];
  char mass_char[100];

  for (int & mass_index : mass_indices){
    
    sprintf(mass_char,"both-theta_mass-%i",mass_index);

    for (std::string abin_var : vec_abin_vars){

      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)     bins.at(0);
      double abin_min   = (double)  bins.at(1);
      double abin_max   = (double)  bins.at(2);

      for (std::string spec_var : vec_spec_vars){

        sprintf(hfac_mass_name,         "hfac_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(sign_mass_name,         "sign_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(dsig_mass_name,         "dsig_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(hfac_comp_mass_name,         "hfac_comp_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(sign_comp_mass_name,         "sign_comp_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        sprintf(dsig_comp_mass_name,         "dsig_comp_mass-%i_s%s_a%s",     mass_index, spec_var.c_str(),   abin_var.c_str());
        TH1F hfac_hist{hfac_mass_name, hfac_mass_name, abin_bins, abin_min, abin_max};
        TH1F sign_hist{sign_mass_name, sign_mass_name, abin_bins, abin_min, abin_max};
        TH1F dsig_hist{dsig_mass_name, dsig_mass_name, abin_bins, abin_min, abin_max};
        TH1F hfac_comp_hist{hfac_comp_mass_name, hfac_comp_mass_name, abin_bins, abin_min, abin_max};
        TH1F sign_comp_hist{sign_comp_mass_name, sign_comp_mass_name, abin_bins, abin_min, abin_max};
        TH1F dsig_comp_hist{dsig_comp_mass_name, dsig_comp_mass_name, abin_bins, abin_min, abin_max};
  
        
        
        bool hfac_error_available = true;
        bool sign_error_available = true;
        bool dsig_error_available = true;
        bool hfac_comp_error_available = true;
        bool sign_comp_error_available = true;
        bool dsig_comp_error_available = true;

        std::string hfac_comp_file_path = hfac_path + "s50_f1_99.root";
        std::string sign_comp_file_path = sign_path + "s50_f1.root";
        std::string dsig_comp_file_path = dsig_path + "s50_f1.root";
        TFile * hfac_comp_file = new TFile(hfac_comp_file_path.c_str(),"READ");
        TFile * sign_comp_file = new TFile(sign_comp_file_path.c_str(),"READ");
        TFile * dsig_comp_file = new TFile(dsig_comp_file_path.c_str(),"READ");
        TH1F * hfac_comp_file_hist  = new TH1F("hfac_comp_file-1","",abin_bins,abin_min,abin_max);
        TH1F * sign_comp_file_hist  = new TH1F("sign_comp_file-1","",abin_bins,abin_min,abin_max);
        TH1F * dsig_comp_file_hist  = new TH1F("dsig_comp_file-1","",abin_bins,abin_min,abin_max);

        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){

          char hfac_bin_hist_name[150]; 
          char hfac_err_hist_name[150];
          sprintf(hfac_bin_hist_name,   "%s_sign_99_pos_%s_%s-%i",  spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(hfac_err_hist_name,   "%s_err_99_pos_%s_%s-%i",   spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);
          
          char sign_bin_hist_name[150];
          char sign_err_hist_name[150];
          sprintf(sign_bin_hist_name,   "%s_sign_99_pos_%s_%s-%i",  spec_var.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(sign_err_hist_name,   "%s_err_99_pos_%s_%s-%i",   spec_var.c_str(), mass_char,  abin_var.c_str(), abin_index);
          
          char dsig_bin_hist_name[150];
          char dsig_err_hist_name[150];
          sprintf(dsig_bin_hist_name,   "%s_data_00_npos_%s_%s-%i", spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(dsig_err_hist_name,   "%s_err_00_npos_%s_%s-%i",  spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);

          TH1F * hfac_comp_int_hist     = (TH1F*) hfac_comp_file->Get(hfac_bin_hist_name);
          TH1F * hfac_comp_err_hist     = (TH1F*) hfac_comp_file->Get(hfac_err_hist_name);
          hfac_comp_file_hist->SetBinContent(abin_index, hfac_comp_int_hist->Integral());
          if (hfac_comp_err_hist != NULL){ 
            hfac_comp_file_hist->SetBinError(abin_index,hfac_comp_err_hist->GetBinError(2));
          } else { hfac_comp_error_available = false;}


          TH1F * sign_comp_int_hist     = (TH1F*) sign_comp_file->Get(sign_bin_hist_name);
          TH1F * sign_comp_err_hist     = (TH1F*) sign_comp_file->Get(sign_err_hist_name);
          sign_comp_file_hist->SetBinContent(abin_index, sign_comp_int_hist->Integral());
          if (sign_comp_err_hist != NULL){ 
            sign_comp_file_hist->SetBinError(abin_index,sign_comp_err_hist->GetBinError(2));
          } else { sign_comp_error_available = false;}

          
          TH1F * dsig_comp_int_hist     = (TH1F*) dsig_comp_file->Get(dsig_bin_hist_name);
          TH1F * dsig_comp_err_hist     = (TH1F*) dsig_comp_file->Get(dsig_err_hist_name);
          dsig_comp_file_hist->SetBinContent(abin_index, dsig_comp_int_hist->Integral());
          if (dsig_comp_err_hist != NULL){ 
            dsig_comp_file_hist->SetBinError(abin_index,dsig_comp_err_hist->GetBinError(2));
          } else { dsig_comp_error_available = false;}

        }

        hfac_comp_hist.Add(hfac_comp_file_hist,1.0);
        sign_comp_hist.Add(sign_comp_file_hist,1.0); 
        dsig_comp_hist.Add(dsig_comp_file_hist,1.0);
        delete hfac_comp_file_hist;
        delete sign_comp_file_hist;
        delete dsig_comp_file_hist;
        hfac_comp_file->Close(); 
        sign_comp_file->Close();
        dsig_comp_file->Close();
        if (!hfac_comp_error_available){ hfac_comp_hist.Sumw2();}
        if (!sign_comp_error_available){ sign_comp_hist.Sumw2();}
        if (!dsig_comp_error_available){ dsig_comp_hist.Sumw2();}




        for ( int file_idx = 0; file_idx <(int) vec_files.size(); file_idx++){

          std::string hfac_file_path = hfac_path + vec_files[file_idx] + "_99.root";
          std::string sign_file_path = sign_path + vec_files[file_idx] + ".root";
          std::string dsig_file_path = dsig_path + vec_files[file_idx] + ".root";
          
          
          TFile * hfac_file = new TFile(hfac_file_path.c_str(),"READ");
          TFile * sign_file = new TFile(sign_file_path.c_str(),"READ");
          TFile * dsig_file = new TFile(dsig_file_path.c_str(),"READ");
          
          TH1F * hfac_file_hist       = new TH1F(Form("hfac_file-%i",file_idx),"",abin_bins,abin_min,abin_max);
          TH1F * sign_file_hist       = new TH1F(Form("sign_file-%i",file_idx),"",abin_bins,abin_min,abin_max);
          TH1F * dsig_file_hist       = new TH1F(Form("dsig_file-%i",file_idx),"",abin_bins,abin_min,abin_max);
          


          for (int abin_index = 1; abin_index <= abin_bins; abin_index++){

            char hfac_bin_hist_name[150]; 
            char hfac_err_hist_name[150];
            sprintf(hfac_bin_hist_name,   "%s_sign_99_pos_%s_%s-%i",  spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);
            sprintf(hfac_err_hist_name,   "%s_err_99_pos_%s_%s-%i",   spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);
            
            char sign_bin_hist_name[150];
            char sign_err_hist_name[150];
            sprintf(sign_bin_hist_name,   "%s_sign_99_pos_%s_%s-%i",  spec_var.c_str(), mass_char,  abin_var.c_str(), abin_index);
            sprintf(sign_err_hist_name,   "%s_err_99_pos_%s_%s-%i",   spec_var.c_str(), mass_char,  abin_var.c_str(), abin_index);
          
            char dsig_bin_hist_name[150];
            char dsig_err_hist_name[150];
            sprintf(dsig_bin_hist_name,   "%s_data_00_npos_%s_%s-%i", spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);
            sprintf(dsig_err_hist_name,   "%s_err_00_npos_%s_%s-%i",  spec_var.c_str(), mass_char, abin_var.c_str(), abin_index);

            TH1F * hfac_int_hist     = (TH1F*) hfac_file->Get(hfac_bin_hist_name);
            TH1F * hfac_err_hist     = (TH1F*) hfac_file->Get(hfac_err_hist_name);
            hfac_file_hist->SetBinContent(abin_index, hfac_int_hist->Integral());
            if (hfac_err_hist != NULL){ 
              hfac_file_hist->SetBinError(abin_index,hfac_err_hist->GetBinError(2));
            } else { hfac_error_available = false;}

            TH1F * sign_int_hist     = (TH1F*) sign_file->Get(sign_bin_hist_name);
            TH1F * sign_err_hist     = (TH1F*) sign_file->Get(sign_err_hist_name);
            sign_file_hist->SetBinContent(abin_index, sign_int_hist->Integral());
            if (sign_err_hist != NULL){ 
              sign_file_hist->SetBinError(abin_index,sign_err_hist->GetBinError(2));
            } else { sign_error_available = false;}

            TH1F * dsig_int_hist     = (TH1F*) dsig_file->Get(dsig_bin_hist_name);
            TH1F * dsig_err_hist     = (TH1F*) dsig_file->Get(dsig_err_hist_name);
            dsig_file_hist->SetBinContent(abin_index, dsig_int_hist->Integral());
            if (dsig_err_hist != NULL){ 
              dsig_file_hist->SetBinError(abin_index,dsig_err_hist->GetBinError(2));
            } else { dsig_error_available = false;}

          }

          hfac_hist.Add(hfac_file_hist,1.0);
          sign_hist.Add(sign_file_hist,1.0);
          dsig_hist.Add(dsig_file_hist,1.0);
         
          delete hfac_file_hist;
          delete sign_file_hist;
          delete dsig_file_hist;
          
          hfac_file->Close(); 
          sign_file->Close();
          dsig_file->Close();

        }

        if (!hfac_error_available){ hfac_hist.Sumw2();}
        if (!sign_error_available){ sign_hist.Sumw2();}
        if (!dsig_error_available){ dsig_hist.Sumw2();}
        
        TLatex ATLAS;
        ATLAS.SetNDC();
        ATLAS.SetTextFont(72);
        ATLAS.SetTextColor(1);
        TLatex wip; 
        wip.SetNDC();
        wip.SetTextFont(42);
        wip.SetTextSize(0.038);
        wip.SetTextColor(1);
        TLatex sim; 
        sim.SetNDC();
        sim.SetTextFont(42);
        sim.SetTextSize(0.038);
        sim.SetTextColor(1);

        TH1F * comp_clone_c1 = (TH1F*) hfac_comp_hist.Clone("comp_c1"); 
        TH1F * comp_clone_c2 = (TH1F*) dsig_comp_hist.Clone("comp_p2");
        TH1F * comp_clone_c3 = (TH1F*) sign_comp_hist.Clone("comp_p3"); 


        TCanvas * canv_c1 = new TCanvas("canv_c1","canv_c1",100,100,1000,1000);
        canv_c1->Divide(1,1);
        
        canv_c1->cd(1);
        comp_clone_c1->Draw("E1");
        comp_clone_c1->GetYaxis()->SetRangeUser(0,1.8*comp_clone_c1->GetMaximum());
        comp_clone_c1->GetYaxis()->SetTitleOffset(1.7);
        comp_clone_c1->SetMarkerStyle(21);
        comp_clone_c1->SetLineColor(1);
        comp_clone_c1->Draw("HIST SAME");
        hfac_hist.Draw("E1 SAME");
        hfac_hist.Draw("HIST SAME");
        hfac_hist.SetLineColor(kRed+1);
        comp_clone_c1->GetXaxis()->SetTitle("qTA (GeV)");
        comp_clone_c1->GetYaxis()->SetTitle("Signal Yield/2GeV");
        gPad->Modified(); gPad->Update();
        ATLAS.DrawLatexNDC(0.2,0.89,"ATLAS");
        wip.DrawLatexNDC(0.2,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.2,0.80,"Simulation");
        TLatex canv_c1_ltx;
        canv_c1_ltx.SetTextSize(0.03);
        canv_c1_ltx.DrawLatexNDC(0.2,0.97,Form("Extracted Signal %s, Q%i", abin_var.c_str(),mass_index));  
        TLegend canv_c1_legend(0.45, 0.8, 0.925,  0.925);
        canv_c1_legend.SetTextSize(0.025);
        canv_c1_legend.SetFillStyle(0);
        canv_c1_legend.SetTextFont(42);
        canv_c1_legend.AddEntry(comp_clone_c1,"Extracted signal, s50", "l");
        canv_c1_legend.AddEntry(&hfac_hist,Form("Extracted signal, recombined %s",unique.c_str()), "l");
        canv_c1_legend.SetBorderSize(0);
        canv_c1_legend.Draw();
        
        canv_c1->SaveAs(Form("dcmp_%s_Q%i_%s.png", abin_var.c_str(), mass_index, unique.c_str())); 
        delete canv_c1;
        
        TCanvas * canv_c2 = new TCanvas("canv_c2","canv_c2",100,100,1000,1000);
        canv_c2->Divide(1,1);
        
        canv_c2->cd(1);
        comp_clone_c2->Draw("E1");
        comp_clone_c2->GetYaxis()->SetRangeUser(0,1.8*comp_clone_c2->GetMaximum());
        comp_clone_c2->GetYaxis()->SetTitleOffset(1.7);
        comp_clone_c2->SetMarkerStyle(21);
        comp_clone_c2->SetLineColor(1);
        comp_clone_c2->Draw("HIST SAME");
        hfac_hist.Draw("E1 SAME");
        hfac_hist.Draw("HIST SAME");
        hfac_hist.SetLineColor(kRed+1);
        comp_clone_c2->GetXaxis()->SetTitle("qTA (GeV)");
        comp_clone_c2->GetYaxis()->SetTitle("Signal Yield/2GeV");
        gPad->Modified(); gPad->Update();
        ATLAS.DrawLatexNDC(0.2,0.89,"ATLAS");
        wip.DrawLatexNDC(0.2,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.2,0.80,"Simulation");
        TLatex canv_c2_ltx;
        canv_c2_ltx.SetTextSize(0.03);
        canv_c2_ltx.DrawLatexNDC(0.2,0.97,Form("Extracted Signal %s, Q%i", abin_var.c_str(),mass_index));  
        TLegend canv_c2_legend(0.45, 0.8, 0.925,  0.925);
        canv_c2_legend.SetTextSize(0.025);
        canv_c2_legend.SetFillStyle(0);
        canv_c2_legend.SetTextFont(42);
        canv_c2_legend.AddEntry(comp_clone_c2,"Input signal, s50", "l");
        canv_c2_legend.AddEntry(&hfac_hist,Form("Extracted signal, recombined %s",unique.c_str()), "l");
        canv_c2_legend.SetBorderSize(0);
        canv_c2_legend.Draw();
        
        canv_c2->SaveAs(Form("dcmp_in_%s_Q%i_%s.png", abin_var.c_str(), mass_index, unique.c_str())); 
        delete canv_c2;

        TCanvas * canv_c3 = new TCanvas("canv_c3","canv_c3",100,100,1000,1000);
        canv_c3->Divide(1,1);
        

        canv_c3->cd(1);
        TH1F * hfac_norm  = (TH1F*) hfac_hist.Clone("hfac_norm"); 
        comp_clone_c3->Scale(1.0/comp_clone_c3->Integral());
        hfac_norm->Scale(1.0/hfac_norm->Integral());
        comp_clone_c3->Draw("E1");
        comp_clone_c3->GetYaxis()->SetRangeUser(0,1.8*comp_clone_c3->GetMaximum());
        comp_clone_c3->GetYaxis()->SetTitleOffset(1.7);
        comp_clone_c3->SetMarkerStyle(21);
        comp_clone_c3->SetLineColor(1);
        comp_clone_c3->Draw("HIST SAME");
        hfac_norm->Draw("E1 SAME");
        hfac_norm->Draw("HIST SAME");
        hfac_norm->SetLineColor(kRed+1);
        comp_clone_c3->GetXaxis()->SetTitle("qTA (GeV)");
        comp_clone_c3->GetYaxis()->SetTitle("Signal Yield/2GeV");
        gPad->Modified(); gPad->Update();
        ATLAS.DrawLatexNDC(0.2,0.89,"ATLAS");
        wip.DrawLatexNDC(0.2,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.2,0.80,"Simulation");
        TLatex canv_c3_ltx;
        canv_c3_ltx.SetTextSize(0.03);
        canv_c3_ltx.DrawLatexNDC(0.2,0.97,Form("Extracted Signal %s, Q%i", abin_var.c_str(),mass_index));  
        TLegend canv_c3_legend(0.45, 0.8, 0.925,  0.925);
        canv_c3_legend.SetTextSize(0.025);
        canv_c3_legend.SetFillStyle(0);
        canv_c3_legend.SetTextFont(42);
        canv_c3_legend.AddEntry(comp_clone_c3,"Normalised MC signal", "l");
        canv_c3_legend.AddEntry(hfac_norm,Form("Normalised signal, recombined %s",unique.c_str()), "l");
        canv_c3_legend.SetBorderSize(0);
        canv_c3_legend.Draw();
        
        canv_c3->SaveAs(Form("dcmp_shp_%s_Q%i_%s.png", abin_var.c_str(), mass_index, unique.c_str())); 
        delete canv_c3;

        //delete hfac_hist; 
        //delete sign_hist;
        //delete dsig_hist;
        //delete hfac_comp_hist;
        //delete sign_comp_hist;
        //delete dsig_comp_hist;

      }
    }
  }
}
