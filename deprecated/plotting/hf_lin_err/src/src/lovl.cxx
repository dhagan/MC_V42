#include <lovl.hxx>
#include <split_string.hxx>

void lovl(std::string path, std::string files, std::string abin_vars, std::string spec_vars, std::string slice, std::string type, std::string unique){
  
  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);


  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::vector<std::string> vec_files;
  split_strings(vec_files, files, ":");

  int hf_error_bin = 0;
  if (type.find("data") != std::string::npos) hf_error_bin = 1;
  if (type.find("sign") != std::string::npos) hf_error_bin = 2;
  if (type.find("bckg") != std::string::npos) hf_error_bin = 3;

  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";


  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};

  std::vector<int> mass_indices = {0,3,4,5,12};
  std::vector <TH1F*> hist_vec;

  TF1 * zero_line = new TF1("zero","0",-5,15);
  zero_line->SetLineStyle(2);
  zero_line->SetLineColorAlpha(1,0.5);


  char hist_mass_name[100], mass_char[100];
  
  for (int & mass_index : mass_indices){
    
    sprintf(mass_char,"both-theta_mass-%i",mass_index);

    for (std::string abin_var : vec_abin_vars){

      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)     bins.at(0);
      double abin_min   = (double)  bins.at(1);
      double abin_max   = (double)  bins.at(2);

      for (std::string spec_var : vec_spec_vars){

        hist_vec.clear();

        int file_no = 1;
        for (std::string file : vec_files){ 

          sprintf(hist_mass_name,         "hist_mass-%i_s%s_a%s_f%i", mass_index, spec_var.c_str(), abin_var.c_str(), file_no++);
          TH1F * data_mass_hist = new TH1F( hist_mass_name, hist_mass_name, abin_bins,abin_min, abin_max);
          std::string file_path = path + file + ".root";
          TFile * current_file = new TFile(file_path.c_str(),"READ");
          
          std::cout << file_path << std::endl;
          bool error_available = true;
          for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
            
            char data_hist_name[200], error_name[200];
            sprintf(data_hist_name, "%s_%s_%s_%s_%s_%s-%i",  spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);
            sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);
            std::cout << data_hist_name << std::endl;
            TH1F * data_integral_hist = (TH1F*) current_file->Get(data_hist_name);
            TH1F * error_hist = (TH1F*) current_file->Get(error_name);
            data_mass_hist->SetBinContent(abin_index, data_integral_hist->Integral());
            if (error_hist != NULL){ 
              data_mass_hist->SetBinError(abin_index, error_hist->GetBinError(hf_error_bin));
            } else {error_available = false; }
          }
          if (!error_available){ data_mass_hist->Sumw2(); }
          hist_vec.push_back(data_mass_hist);
          current_file->Close(); 
          delete current_file;
        }
        
        std::vector<double> mean = {0,0,0,0,0,0,0,0,0,0};
        std::vector<double> rmse = {0,0,0,0,0,0,0,0,0,0};
        if (abin_var.find("qtB") != std::string::npos) {for (int i = 0; i<5; i++){mean.push_back(0); rmse.push_back(0); }}
  
        TH1F * rmse_hist = new TH1F(Form("rmse_hist-%i",mass_index),"",abin_bins,abin_min,abin_max);
        TH1F * rms_hist = new TH1F(Form("rms_hist-%i",mass_index),"",abin_bins,abin_min,abin_max);
        TH1F * sd_hist = new TH1F(Form("sd_hist-%i",mass_index),"",abin_bins,abin_min,abin_max);
        TH1F * rmse_auto_hist = new TH1F(Form("rmse_auto_hist-%i",mass_index),"",abin_bins,abin_min,abin_max);
        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
          TH1F * bin_hist = new TH1F(Form("bin-%i",abin_index),"",100,0,100000);
          for (TH1F * current_hist : hist_vec){
            mean.at(abin_index-1) += current_hist->GetBinContent(abin_index);
            bin_hist->Fill(current_hist->GetBinContent(abin_index));
          }
          mean.at(abin_index-1) = mean.at(abin_index-1)/((double) hist_vec.size());
          for (TH1F * current_hist : hist_vec){
            double me = mean.at(abin_index-1) - current_hist->GetBinContent(abin_index);
            double mse = me*me;
            rmse.at(abin_index-1) += mse;
          }
          bin_hist->Sumw2();
          rmse.at(abin_index-1) = std::sqrt(rmse.at(abin_index-1)/((double)hist_vec.size()));
          rmse_hist->SetBinContent(abin_index,mean.at(abin_index-1));
          rmse_hist->SetBinError(abin_index,rmse.at(abin_index-1));
          rms_hist->SetBinContent(abin_index,bin_hist->GetMean());
          rms_hist->SetBinError(abin_index,bin_hist->GetRMS());
          sd_hist->SetBinContent(abin_index,bin_hist->GetMean());
          sd_hist->SetBinError(abin_index,bin_hist->GetStdDev());
          rmse_auto_hist->SetBinContent(abin_index,bin_hist->GetMean());
          rmse_auto_hist->SetBinError(abin_index,bin_hist->GetRMSError());


          delete bin_hist;
        }

        TLatex ATLAS;
        ATLAS.SetNDC();
        ATLAS.SetTextFont(72);
        ATLAS.SetTextColor(1);
        TLatex wip; 
        wip.SetNDC();
        wip.SetTextFont(42);
        wip.SetTextSize(0.038);
        wip.SetTextColor(1);
        TLatex sim; 
        sim.SetNDC();
        sim.SetTextFont(42);
        sim.SetTextSize(0.038);
        sim.SetTextColor(1);


        ATLAS.DrawLatex(0.225,0.89,"ATLAS");
        wip.DrawLatex(0.225,0.85,"Work In Progress");
        wip.DrawLatex(0.225,0.80,"Simulation");



        TCanvas * current_canv = new TCanvas("current_canv","current_canv",100,100,1000,1000);
        current_canv->Divide(1);

        current_canv->cd(1);
        int iteration{0}, max{0};
        //int bars = hist_vec.size();
        for (TH1F * current_hist : hist_vec){
          //if (bars>5)current_hist->SetBarOffset(-0.3 + 0.06*iteration);
          current_hist->SetLineColorAlpha(1,1.0-(0.1*iteration++));
          current_hist->SetMarkerStyle(21);
          std::cout << current_hist->GetName() << std::endl;
          if (iteration == 0 ){ 
            current_hist->Draw("E1"); 
            current_hist->Draw("HIST SAME"); 
          } else { 
            current_hist->Draw("E1 SAME"); 
            current_hist->Draw("HIST SAME"); 
          }
          if (current_hist->GetMaximum() > max) max = current_hist->GetMaximum();
        } 
        if (unique.find("c50") == std::string::npos){
          rmse_hist->Draw("E3 SAME");
        }
        rmse_hist->SetFillColorAlpha(kRed+1,0.5);
        zero_line->Draw("SAME");
        hist_vec.at(0)->GetYaxis()->SetRangeUser(-0.12*max,1.2*max);
        hist_vec.at(0)->GetXaxis()->SetTitle("qTA (GeV)");
        hist_vec.at(0)->GetYaxis()->SetTitle("Signal Yield/2GeV");
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");
        TLatex pad1_ltx;
        pad1_ltx.SetTextSize(0.03);
        pad1_ltx.DrawLatexNDC(0.2,0.97,Form("Overlain extracted %s %s %s, Q%i rmse", abin_var.c_str(), type.c_str()
                                            ,slice.c_str(), mass_index));
        

        //current_canv->cd(2);
        //iteration = 0; max = 0;
        //bars = hist_vec.size();
        //for (TH1F * current_hist : hist_vec){
        //  if (bars>5)current_hist->SetBarOffset(-0.3 + 0.06*iteration);
        //  current_hist->SetLineColorAlpha(1,1.0-(0.1*iteration++));
        //  current_hist->SetMarkerStyle(21);
        //  if (iteration == 0 ){ 
        //    current_hist->Draw("E1"); 
        //    current_hist->Draw("HIST SAME"); 
        //  } else { 
        //    current_hist->Draw("E1 SAME"); 
        //    current_hist->Draw("HIST SAME"); 
        //  }
        //  if (current_hist->GetMaximum() > max) max = current_hist->GetMaximum();
        //} 
        //rms_hist->Draw("E3 SAME");
        //rms_hist->SetFillColorAlpha(kRed+1,0.5);
        //hist_vec.at(0)->GetYaxis()->SetRangeUser(-0.12*max,1.2*max);
        //hist_vec.at(0)->GetXaxis()->SetTitle("qTA (GeV)");
        //hist_vec.at(0)->GetYaxis()->SetTitle("Signal Yield/2GeV");
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");
        //TLatex pad2_ltx;
        //pad2_ltx.SetTextSize(0.03);
        //pad2_ltx.DrawLatexNDC(0.2,0.97,Form("Overlain extracted %s %s %s, Q%i, rms", abin_var.c_str(), type.c_str()
        //                                    ,slice.c_str(), mass_index));

        //current_canv->cd(3);
        //iteration = 0; max = 0;
        //bars = hist_vec.size();
        //for (TH1F * current_hist : hist_vec){
        //  if (bars>5)current_hist->SetBarOffset(-0.3 + 0.06*iteration);
        //  current_hist->SetLineColorAlpha(1,1.0-(0.1*iteration++));
        //  current_hist->SetMarkerStyle(21);
        //  if (iteration == 0 ){ 
        //    current_hist->Draw("E1"); 
        //    current_hist->Draw("HIST SAME"); 
        //  } else { 
        //    current_hist->Draw("E1 SAME"); 
        //    current_hist->Draw("HIST SAME"); 
        //  }
        //  if (current_hist->GetMaximum() > max) max = current_hist->GetMaximum();
        //} 
        //sd_hist->Draw("E3 SAME");
        //sd_hist->SetFillColorAlpha(kRed+1,0.5);
        //hist_vec.at(0)->GetYaxis()->SetRangeUser(-0.12*max,1.2*max);
        //hist_vec.at(0)->GetXaxis()->SetTitle("qTA (GeV)");
        //hist_vec.at(0)->GetYaxis()->SetTitle("Signal Yield/2GeV");
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");
        //TLatex pad3_ltx;
        //pad3_ltx.SetTextSize(0.03);
        //pad3_ltx.DrawLatexNDC(0.2,0.97,Form("Overlain extracted %s %s %s, Q%i, std dev", abin_var.c_str(), type.c_str()
        //                                    ,slice.c_str(), mass_index));

        //current_canv->cd(4);
        //iteration = 0; max = 0;
        //bars = hist_vec.size();
        //for (TH1F * current_hist : hist_vec){
        //  if (bars>5)current_hist->SetBarOffset(-0.3 + 0.06*iteration);
        //  current_hist->SetLineColorAlpha(1,1.0-(0.1*iteration++));
        //  current_hist->SetMarkerStyle(21);
        //  if (iteration == 0 ){ 
        //    current_hist->Draw("E1"); 
        //    current_hist->Draw("HIST SAME"); 
        //  } else { 
        //    current_hist->Draw("E1 SAME"); 
        //    current_hist->Draw("HIST SAME"); 
        //  }
        //  if (current_hist->GetMaximum() > max) max = current_hist->GetMaximum();
        //} 
        //rmse_auto_hist->Draw("E3 SAME");
        //rmse_auto_hist->SetFillColorAlpha(kRed+1,0.5);
        //hist_vec.at(0)->GetYaxis()->SetRangeUser(-0.12*max,1.2*max);
        //hist_vec.at(0)->GetXaxis()->SetTitle("qTA (GeV)");
        //hist_vec.at(0)->GetYaxis()->SetTitle("Signal Yield/2GeV");
        //ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        //wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        //sim.DrawLatexNDC(0.225,0.80,"Simulation");
        //TLatex pad4_ltx;
        //pad4_ltx.SetTextSize(0.03);
        //pad4_ltx.DrawLatexNDC(0.2,0.97,Form("Overlain extracted %s %s %s, Q%i, rmse auto", abin_var.c_str(), type.c_str()
        //                                    ,slice.c_str(), mass_index));

        current_canv->SaveAs(Form("lovl_%s_%s_%s_Q%i_%s.png", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index,unique.c_str())); 
        delete rms_hist;
        delete rmse_hist;
        delete sd_hist;
        delete rmse_auto_hist;

        delete current_canv;
      }
    }
  }

  



}
