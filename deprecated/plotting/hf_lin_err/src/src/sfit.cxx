#include <sfit.hxx>
#include <split_string.hxx>
#include <dscb.hxx>


void sfit(std::string filepath, std::string abin_vars, std::string spec_vars, std::string type, std::string slice, std::string unique){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);
        

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};
  
  int hf_error_bin = 0;
  std::string full_type;
  int fit_color;
  if (type.find("data") != std::string::npos){ hf_error_bin = 1; full_type = "data"; fit_color = 1;}  
  if (type.find("sign") != std::string::npos){ hf_error_bin = 2; full_type = "signal"; fit_color = kRed+1;}
  if (type.find("bckg") != std::string::npos){ hf_error_bin = 3; full_type = "background"; fit_color = kBlue+1;}

  std::string pos = "pos";
  if (slice.compare("00") == 0) pos = "npos";


  TF1 * fit_sg = new TF1("fit_single_gaussian"," [0]*exp( -((x-[1])^2)/(2*[2]^2))" ); 
  fit_sg->SetParName(0,"C"); fit_sg->SetParName(1,"X"); fit_sg->SetParName(2,"Sigma");
  fit_sg->SetParNames("C","X","SIG");
  //fit_sg->SetLineColor(kRed+1);
  fit_sg->SetLineColor(fit_color);
  
  TF1 * fit_dg_cc = new TF1("fit_dg_cc"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) + [3]*exp( -((x-[1])^2)/(2*[4]^2)) " );
  fit_dg_cc->SetParNames("C1","X1","SIG1","C2","SIG2");
  //fit_dg_cc->SetLineColor(kRed+1);
  fit_dg_cc->SetLineColor(fit_color);

  TF1 * fit_dgn = new TF1("fit_dgn"," [0]*exp( -((x-[1])^2)/(2*[2]^2))",-5,15); 
  fit_dgn->SetParNames("C","X","SIG");
  fit_dgn->SetLineColor(kMagenta+2);
  //fit_dgn->SetLineStyle(2);
  fit_dgn->SetLineWidth(3);

  TF1 * fit_dgw = new TF1("fit_dgw"," [0]*exp( -((x-[1])^2)/(2*[2]^2))",-5,15); 
  fit_dgw->SetParNames("C","X","SIG");
  fit_dgw->SetLineColor(kGreen+2);
  //fit_dgw->SetLineStyle(2);
  fit_dgw->SetLineWidth(3);

  TF1 * fit_gc = new TF1("fit_cg"," [0]*exp( -((x-[1])^2)/(2*[2]^2)) + [3]" );
  fit_gc->SetParNames("C1","X1","SIG1","C2");
  fit_gc->SetLineColor(fit_color);

  TF1 * fit_gcg = new TF1("fit_gcg"," [0]*exp( -((x-[1])^2)/(2*[2]^2))",-5,15); 
  fit_gcg->SetParNames("C","X","SIG");
  fit_gcg->SetLineColor(kMagenta+2);
  //fit_gcg->SetLineStyle(2);
  fit_gcg->SetLineWidth(3);
  
  TF1 * fit_gcc = new TF1("fit_gcg","[0]",-5,15); 
  fit_gcc->SetParNames("C");
  fit_gcc->SetLineColor(kGreen+2);
  //fit_gcc->SetLineStyle(2);
  fit_gcc->SetLineWidth(3);



  std::vector<int> mass_indices = {0,3,4,5,12};
  TFile * integral_file = new TFile(filepath.c_str(),   "READ");

  for (int & mass_index : mass_indices){

    char mass_char[100];        
    sprintf(mass_char,"both-theta_mass-%i",mass_index);
    for (std::string abin_var : vec_abin_vars){
        
      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)    bins.at(0);
      double abin_min   = (double) bins.at(1);
      double abin_max   = (double) bins.at(2);

      for (std::string spec_var : vec_spec_vars){
        
        char full_hist_name[150];
        TH1F full_hist( full_hist_name, full_hist_name, abin_bins, abin_min, abin_max);

        bool error_available = true;
        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
           
          char bin_hist_name[150], error_name[150];
          sprintf(bin_hist_name, "%s_%s_%s_%s_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(),pos.c_str(), mass_char, abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_%s_%s_%s-%i", spec_var.c_str(),   slice.c_str(),pos.c_str(), mass_char,  abin_var.c_str(), abin_index);
          TH1F * integral_hist  = (TH1F*) integral_file->Get(bin_hist_name);
          TH1F * error_hist     = (TH1F*) integral_file->Get(error_name);

          full_hist.SetBinContent(abin_index, integral_hist->Integral());
          if (error_hist != NULL){ 
            full_hist.SetBinError(abin_index,error_hist->GetBinError(hf_error_bin));
          } else {error_available = false;}
        }
        if ( !error_available ){ full_hist.Sumw2();}

        TH1F * hist_p1 = (TH1F*) full_hist.Clone("p1");
        TH1F * hist_p2 = (TH1F*) full_hist.Clone("p2");
        TH1F * hist_p3 = (TH1F*) full_hist.Clone("p3");

        TLatex ATLAS;
        ATLAS.SetNDC();
        ATLAS.SetTextFont(72);
        ATLAS.SetTextColor(1);
        TLatex wip; 
        wip.SetNDC();
        wip.SetTextFont(42);
        wip.SetTextSize(0.038);
        wip.SetTextColor(1);
        TLatex sim; 
        sim.SetNDC();
        sim.SetTextFont(42);
        sim.SetTextSize(0.038);
        sim.SetTextColor(1);

        TCanvas * current_canv = new TCanvas("current_canv","",200,200,3000,1000); 
        current_canv->Divide(3,1);

        current_canv->cd(1); 
        hist_p1->Draw("E1");
        //hist_p1->GetYaxis()->SetRangeUser(-0.12*hist_p1->GetMaximum(),1.2*hist_p1->GetMaximum());
        hist_p1->GetYaxis()->SetRangeUser(0,1.7*hist_p1->GetMaximum());
        hist_p1->GetYaxis()->SetTitleOffset(1.7);
        hist_p1->SetMarkerStyle(21);
        fit_sg->SetParLimits(0,0,hist_p1->Integral()*2); 
        fit_sg->SetParLimits(1,abin_min,abin_max); 
        fit_sg->SetParLimits(2,hist_p1->GetRMS()/10.0,hist_p1->GetRMS()*2.0); 
        fit_sg->SetParameters(hist_p1->GetMaximum(),hist_p1->GetMean(),hist_p1->GetRMS());
        hist_p1->Fit(fit_sg,"MQ","",abin_min,abin_max);
        fit_sg->Draw("SAME");
        hist_p1->Draw("SAME HIST");
        gPad->Modified(); gPad->Update();
        TPaveStats * pad1_stats = (TPaveStats*) hist_p1->FindObject("stats");
        pad1_stats->SetX1NDC(0.6); pad1_stats->SetX2NDC(0.925);
        pad1_stats->SetY1NDC(0.6); pad1_stats->SetY2NDC(0.925);
        pad1_stats->SetFillStyle(0);
        hist_p1->GetXaxis()->SetTitle("qTA (GeV)");
        hist_p1->GetYaxis()->SetTitle("Signal Yield/2GeV");
        TLatex pad_1_ltx;
        pad_1_ltx.SetTextSize(0.03);
        pad_1_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));  
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");
        TLegend pad1_legend(0.2, 0.65, 0.6,  0.78);
        pad1_legend.SetTextSize(0.025);
        pad1_legend.SetFillStyle(0);
        pad1_legend.SetTextFont(42);
        pad1_legend.AddEntry(hist_p1,Form("Extracted %s",full_type.c_str()), "l");
        pad1_legend.AddEntry(fit_sg, "Gaussian Fit", "l");
        pad1_legend.SetBorderSize(0);
        pad1_legend.Draw();


        current_canv->cd(2); 
        hist_p2->Draw("E1");
        //hist_p2->GetYaxis()->SetRangeUser(-0.12*hist_p2->GetMaximum(),1.2*hist_p2->GetMaximum());
        hist_p2->GetYaxis()->SetRangeUser(0,1.7*hist_p2->GetMaximum());
        hist_p2->GetYaxis()->SetTitleOffset(1.7);
        hist_p2->SetMarkerStyle(21);
        fit_dg_cc->SetParLimits(0,0,hist_p1->Integral()*2.0); 
        fit_dg_cc->SetParLimits(1,abin_min,abin_max); 
        fit_dg_cc->SetParLimits(2,hist_p2->GetRMS()/2.0,hist_p2->GetRMS()*2.0); 
        fit_dg_cc->SetParLimits(3,0,hist_p1->GetMaximum()/2.0);
        fit_dg_cc->SetParLimits(4,hist_p2->GetRMS()*2.0,hist_p2->GetRMS()*5.0); 
        fit_dg_cc->SetParameters(hist_p2->GetMaximum(),hist_p2->GetMean(),hist_p2->GetRMS(),hist_p2->GetMaximum()/5.0,hist_p2->GetRMS());
        hist_p2->Fit(fit_dg_cc,"MQ","",abin_min,abin_max);
        fit_dg_cc->Draw("SAME");
        fit_dgn->SetParameters(fit_dg_cc->GetParameter("C1"),fit_dg_cc->GetParameter("X1"),fit_dg_cc->GetParameter("SIG1"));
        fit_dgw->SetParameters(fit_dg_cc->GetParameter("C2"),fit_dg_cc->GetParameter("X1"),fit_dg_cc->GetParameter("SIG2"));
        fit_dgn->Draw("SAME");
        fit_dgw->Draw("SAME");
        hist_p2->Draw("SAME HIST");
        gPad->Modified(); gPad->Update();
        TPaveStats * pad2_stats = (TPaveStats*) hist_p2->FindObject("stats");
        pad2_stats->SetX1NDC(0.6); pad2_stats->SetX2NDC(0.925);
        pad2_stats->SetY1NDC(0.6); pad2_stats->SetY2NDC(0.925);
        pad2_stats->SetFillStyle(0);
        hist_p2->GetXaxis()->SetTitle("qTA (GeV)");
        hist_p2->GetYaxis()->SetTitle("Signal Yield/2GeV");
        TLatex pad_2_ltx;
        pad_2_ltx.SetTextSize(0.03);
        pad_2_ltx.DrawLatexNDC(0.2,0.97,Form("Double gauss, common centre fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");
        TLegend pad2_legend(0.2, 0.65, 0.6,  0.78);
        pad2_legend.SetTextSize(0.025);
        pad2_legend.SetFillStyle(0);
        pad2_legend.SetTextFont(42);
        pad2_legend.AddEntry(hist_p2,Form("Extracted %s",full_type.c_str()), "l");
        pad2_legend.AddEntry(fit_dg_cc, "Double Gaussian Fit", "l");
        pad2_legend.AddEntry(fit_dgn, "Narrow Gaussian", "l");
        pad2_legend.AddEntry(fit_dgw, "Wide Gaussian", "l");
        pad2_legend.SetBorderSize(0);
        pad2_legend.Draw();


        current_canv->cd(3); 
        hist_p3->Draw("E1");
        hist_p3->SetMarkerStyle(21);
        //hist_p3->GetYaxis()->SetRangeUser(-0.12*hist_p3->GetMaximum(),1.2*hist_p3->GetMaximum());
        hist_p3->GetYaxis()->SetRangeUser(0,1.7*hist_p3->GetMaximum());
        hist_p3->GetYaxis()->SetTitleOffset(1.7);
        fit_gc->SetParLimits(0,0,hist_p3->Integral()*2); 
        fit_gc->SetParLimits(1,abin_min,abin_max); 
        fit_gc->SetParLimits(2,hist_p3->GetRMS()/10.0,hist_p3->GetRMS()*2.0); 
        fit_gc->SetParLimits(3,0,hist_p3->GetMaximum()/2.0); 
        fit_gc->SetParameters(hist_p3->GetMaximum(),hist_p3->GetMean(),hist_p3->GetRMS(),hist_p3->GetMaximum()/5.0);
        hist_p3->Fit(fit_gc,"MQ","",abin_min,abin_max);
        fit_gc->Draw("SAME");
        fit_gcg->SetParameters(fit_gc->GetParameter("C1"),fit_gc->GetParameter("X1"),fit_gc->GetParameter("SIG1"));
        fit_gcc->SetParameter(0,fit_gc->GetParameter("C2"));
        fit_gcg->Draw("SAME");
        fit_gcc->Draw("SAME");

        hist_p3->Draw("SAME HIST");
        gPad->Modified(); gPad->Update();
        TPaveStats * pad3_stats = (TPaveStats*) hist_p3->FindObject("stats");
        pad3_stats->SetX1NDC(0.6); pad3_stats->SetX2NDC(0.925);
        pad3_stats->SetY1NDC(0.6); pad3_stats->SetY2NDC(0.925);
        pad3_stats->SetFillStyle(0);
        hist_p3->GetXaxis()->SetTitle("qTA (GeV)");
        hist_p3->GetYaxis()->SetTitle("Signal Yield/2GeV");
        TLatex pad_3_ltx;
        pad_3_ltx.SetTextSize(0.03);
        pad_3_ltx.DrawLatexNDC(0.2,0.97,Form("Single gauss with constant fit %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));
        ATLAS.DrawLatexNDC(0.225,0.89,"ATLAS");
        wip.DrawLatexNDC(0.225,0.845,"Work In Progress");
        sim.DrawLatexNDC(0.225,0.80,"Simulation");
        TLegend pad3_legend(0.2, 0.65, 0.6,  0.78);
        pad3_legend.SetTextSize(0.025);
        pad3_legend.SetFillStyle(0);
        pad3_legend.SetTextFont(42);
        pad3_legend.AddEntry(hist_p3,Form("Extracted %s",full_type.c_str()), "l");
        pad3_legend.AddEntry(fit_gc, "Gaussian with constant fit", "l");
        pad3_legend.AddEntry(fit_gcg, "Gaussian", "l");
        pad3_legend.AddEntry(fit_gcc, "Constant", "l");
        pad3_legend.SetBorderSize(0);
        pad3_legend.Draw();


        current_canv->SaveAs(Form("sfit_%s_%s_%s_Q%i_%s.png", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index,unique.c_str())); 
        delete current_canv;
        
        TFile * sfit_results;
        if (!gSystem->AccessPathName("./sfit/sfit_results.root",kFileExists)){
          sfit_results = new TFile("./sfit_results.root","CREATE");
        } else {
          sfit_results = new TFile("./sfit_results.root","UPDATE");
        }
        sfit_results->cd();

        std::string suffix(Form("sfit_%s_%s_%s_Q%i_%s", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index,unique.c_str()));
        fit_sg->Write(          ("fit_sg"       + suffix ).c_str());
        fit_dg_cc->Write(       ("fit_dg_cc"    + suffix ).c_str());
        fit_gc->Write(          ("fit_gc"       + suffix ).c_str());
        //fit_dscb->Write(        ("fit_dscb"     + suffix ).c_str());
        //fit_dscb_sym->Write(    ("fit_dscb_sym" + suffix ).c_str());
        sfit_results->Close();
        delete sfit_results;


      }
    }
  }

}


//current_canv->cd(4); 
        //hist_p4->Draw("E1");
        //hist_p4->SetMarkerStyle(21);
        //hist_p4->GetYaxis()->SetRangeUser(-0.12*hist_p4->GetMaximum(),1.2*hist_p4->GetMaximum());
        //double hist_mean = hist_p4->GetMean();
        //double hist_std = hist_p4->GetStdDev();
        //double hist_int = hist_p4->Integral();
        //fit_dscb->SetParameters(1,2,2,1,hist_mean,hist_std,hist_int); 
        //hist_p4->Fit(fit_dscb,"MQ","",abin_min,abin_max);
        //fit_dscb->Draw("SAME");
        //hist_p4->Draw("SAME HIST");
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad4_stats = (TPaveStats*) hist_p4->FindObject("stats");
        //pad4_stats->SetX1NDC(0.6); pad4_stats->SetX2NDC(0.925);
        //pad4_stats->SetY1NDC(0.6); pad4_stats->SetY2NDC(0.925);
        //pad4_stats->SetFillStyle(0);
        //hist_p4->GetXaxis()->SetTitle("qTA (GeV)");
        //hist_p4->GetYaxis()->SetTitle("Signal Yield/2GeV");
        //TLatex pad_4_ltx;
        //pad_4_ltx.SetTextSize(0.03);
        //pad_4_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));

        //current_canv->cd(5);
        //hist_p5->Draw("E1");
        //hist_p5->SetMarkerStyle(21);
        //hist_p5->GetYaxis()->SetRangeUser(-0.12*hist_p5->GetMaximum(),1.2*hist_p5->GetMaximum());
        //double hist_p5_mean = hist_p5->GetMean();
        //double hist_p5_std = hist_p5->GetStdDev();
        //double hist_p5_int = hist_p5->Integral();
        //fit_dscb_sym->SetParameters(1,1,hist_p5_mean,hist_p5_std,hist_p5_int); 
        //hist_p5->Fit(fit_dscb_sym,"MQ","",abin_min,abin_max);
        //fit_dscb_sym->Draw("SAME");
        //hist_p5->Draw("SAME HIST");
        //gPad->Modified(); gPad->Update();
        //TPaveStats * pad5_stats = (TPaveStats*) hist_p5->FindObject("stats");
        //pad5_stats->SetX1NDC(0.6); pad5_stats->SetX2NDC(0.925);
        //pad5_stats->SetY1NDC(0.6); pad5_stats->SetY2NDC(0.925);
        //pad5_stats->SetFillStyle(0);
        //hist_p5->GetXaxis()->SetTitle("qTA (GeV)");
        //hist_p5->GetYaxis()->SetTitle("Signal Yield/2GeV");
        //TLatex pad_5_ltx;
        //pad_5_ltx.SetTextSize(0.03);
        //pad_5_ltx.DrawLatexNDC(0.2,0.97,Form("DSCB SYM %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));

//TF1 * fit_dscb = new TF1("fit_dscb",DSCB,-5,15,7);
  //fit_dscb->SetParNames("\\alpha_{low}","\\alpha_{high}","n_{low}", "n_{high}", "mean", "\\sigma", "Norm");
  //fit_dscb->SetLineColor(kRed+1);

  //TF1 * fit_dscb_sym = new TF1("fit_dscb_sym",DSCB_SYM,-5,15,5);
  //fit_dscb_sym->SetParNames("\\alpha","N","MEAN", "SIG", "NORM");
  //fit_dscb_sym->SetLineColor(kRed+1);
  //fit_dscb_sym->SetParLimits(0,0.5,1.5);  // func boundaries
  //fit_dscb_sym->SetParLimits(1,0.5,3);    // power
  //fit_dscb_sym->SetParLimits(3,1,5);      // sigma

