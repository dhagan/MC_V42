#include <hadd.hxx>
#include <split_string.hxx>
#include <dscb.hxx>


void hadd(std::string path, std::string files, std::string abin_vars, std::string spec_vars, std::string slice, std::string type, std::string unique){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, abin_vars, ":");
  split_strings(vec_spec_vars, spec_vars, ":");

  std::vector<std::string> vec_files;
  split_strings(vec_files, files, ":");

  std::map<std::string,std::vector<double>> analysis_bins;
  analysis_bins["qtA"]           =  std::vector<double>{10,-5,15};
  analysis_bins["BDT"]           =  std::vector<double>{10,-1.0,1.0};
  analysis_bins["abs(qtB)"]      =  std::vector<double>{15,0,15};
  analysis_bins["Phi"]           =  std::vector<double>{8,-M_PI,M_PI};
  analysis_bins["DiMuonPt"]      =  std::vector<double>{15,5000,30000};
  analysis_bins["PhotonPt"]      =  std::vector<double>{10,5000,25000};

  int hf_error_bin = 0;
  if (type.find("data") != std::string::npos) hf_error_bin = 1;
  if (type.find("sign") != std::string::npos) hf_error_bin = 2;
  if (type.find("bckg") != std::string::npos) hf_error_bin = 3;

  std::vector<int> mass_indices = {0,3,4,5,12};
  char mass_char[100];
  char manual_hist_name[100], auto_hist_name[100];

  
  for (int & mass_index : mass_indices){
    
    sprintf(mass_char,"both-theta_mass-%i",mass_index);

    for (std::string abin_var : vec_abin_vars){

      std::string var_name = abin_var;
      if (abin_var.find("qtB") != std::string::npos){ var_name = "abs(qtB)";}
      std::vector<double> bins = analysis_bins[var_name.c_str()];
      int abin_bins     = (int)     bins.at(0);
      double abin_min   = (double)  bins.at(1);
      double abin_max   = (double)  bins.at(2);

      for (std::string spec_var : vec_spec_vars){
        
        sprintf(manual_hist_name,   "manual_hist_mass-%i_s%s_a%s",  mass_index, spec_var.c_str(), abin_var.c_str());
        sprintf(auto_hist_name,     "auto_hist_mass-%i_s%s_a%s",    mass_index, spec_var.c_str(), abin_var.c_str());
        TH1F *manual_hist = new TH1F(manual_hist_name, manual_hist_name, abin_bins, abin_min, abin_max);
        TH1F *auto_hist = new TH1F(auto_hist_name, auto_hist_name, abin_bins, abin_min, abin_max);
        manual_hist->Sumw2();
        auto_hist->Sumw2();


        for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
          char hist_name[200], error_name[200];
          sprintf(hist_name, "%s_%s_%s_pos_%s_%s-%i",  spec_var.c_str(), type.c_str(), slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(error_name,     "%s_err_%s_pos_%s_%s-%i", spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          double lin_mass_bin_count{0}, lin_mass_bin_sq_error_sum{0}, lin_mass_bin_error;
          for ( int lin_mass_idx = 0; lin_mass_idx <(int) vec_files.size(); lin_mass_idx++){
        manual_hist->SetMarkerStyle(1);
            std::string lin_mass_file_name = path + vec_files.at(lin_mass_idx) + ".root";
            TFile * lin_mass_file = new TFile( lin_mass_file_name.c_str(),   "READ");
            TH1F * lin_mass_data_hist = (TH1F*) lin_mass_file->Get(hist_name);
            TH1F * lin_mass_error_hist = (TH1F*) lin_mass_file->Get(error_name);
            double lin_mass_integral = lin_mass_data_hist->Integral();
            double lin_mass_error{0};
            if (lin_mass_error_hist != NULL){
              lin_mass_error = lin_mass_error_hist->GetBinError(hf_error_bin);
            }
            lin_mass_bin_count += lin_mass_integral;
            //lin_mass_bin_sq_error_sum +=  (lin_mass_error*lin_mass_error)/(lin_mass_integral*lin_mass_integral);
            lin_mass_bin_sq_error_sum += (lin_mass_error*lin_mass_error);
            lin_mass_file->Close();
            delete lin_mass_file;
          }
          lin_mass_bin_error = std::sqrt(lin_mass_bin_sq_error_sum);
          manual_hist->SetBinContent(abin_index,lin_mass_bin_count); 
          manual_hist->SetBinError(abin_index,lin_mass_bin_error); 
        }

        for ( int file_idx = 0; file_idx <(int) vec_files.size(); file_idx++){
          std::string file_name = path + vec_files.at(file_idx) + ".root";
          TFile * current_file = new TFile(file_name.c_str(),"READ");
          TH1F * file_hist = new TH1F(Form("File-%i",file_idx),"",abin_bins,abin_min,abin_max);
          for (int abin_index = 1; abin_index <= abin_bins; abin_index++){
            char bin_hist_name[150], error_name[150];
            sprintf(bin_hist_name, "%s_%s_%s_pos_%s_%s-%i", spec_var.c_str(), type.c_str(), slice.c_str(), mass_char, abin_var.c_str(), abin_index);
            sprintf(error_name,     "%s_err_%s_pos_%s_%s-%i", spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
            TH1F * integral_hist  = (TH1F*) current_file->Get(bin_hist_name);
            TH1F * error_hist     = (TH1F*) current_file->Get(error_name);
            file_hist->SetBinContent(abin_index, integral_hist->Integral());
            if (error_hist != NULL){ 
              file_hist->SetBinError(abin_index,error_hist->GetBinError(hf_error_bin));
            }
          }
          auto_hist->Add(file_hist,1.0); 
        }

        manual_hist->SetLineColor(kRed+1);
        auto_hist->SetLineColor(kBlue+1);
        
        manual_hist->SetMarkerStyle(21);
        auto_hist->SetMarkerStyle(21);
        manual_hist->SetBarOffset(-0.2);
        auto_hist->SetBarOffset(0.2);
        //auto_hist->SetErrorLineStyle(1);
        //auto_hist->Get

        TCanvas * current_canv = new TCanvas("temp_canv","H_Add",200,200,1500,1500);
        current_canv->Divide(1);
      
        current_canv->cd(1);
        manual_hist->Draw("E1");
        auto_hist->Draw("E1 SAME");
        manual_hist->Draw("HIST SAME");
        auto_hist->Draw("HIST SAME");


        manual_hist->GetYaxis()->SetRangeUser(0,1.2*manual_hist->GetMaximum());
        manual_hist->GetXaxis()->SetTitle("qTA (GeV)");
        manual_hist->GetYaxis()->SetTitle("Signal Yield/2GeV");


        TLegend current_legend{0.8, 0.8, 0.925,  0.925};
        current_legend.SetTextSize(0.025);
        current_legend.SetFillStyle(0);
        current_legend.SetTextFont(42);
        current_legend.AddEntry(manual_hist, "by SetBin",  "lep");
        current_legend.AddEntry(auto_hist,   "by Add",     "l");
        current_legend.SetBorderSize(0.1);
        current_legend.Draw();
        
        TLatex current_ltx;
        current_ltx.SetTextSize(0.03);
        current_ltx.DrawLatexNDC(0.2,0.97,Form("Overlain error combinations %s %s %s, Q%i", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index));  

        current_canv->SaveAs(Form("hadd_%s_%s_%s_Q%i_%s.png", abin_var.c_str(), type.c_str(),slice.c_str(), mass_index,unique.c_str())); 
        delete current_canv;
      }
    }
  }

}
