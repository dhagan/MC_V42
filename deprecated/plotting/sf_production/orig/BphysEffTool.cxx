#include "BphysEffTool.h"
#include <iostream>

#include "TFile.h"

BphysEffTool::BphysEffTool(std::string singleMuEffFileName, std::string drCorFileName) :
m_singleMuEffFileName(singleMuEffFileName),
m_drCorFileName(drCorFileName)
{
  TFile* singleMuEffFile = new TFile(m_singleMuEffFileName.c_str(),"READ");
  TFile* drCorFile = new TFile(m_drCorFileName.c_str(),"READ");
  
  // Initialize single mu SF map
  vector<DATASET> datasets = { DATASET::Data15, DATASET::Data16 };
  vector<SINGLEMULEG> mulegs = { SINGLEMULEG::mu4, SINGLEMULEG::mu6 };
  for(auto dataset : datasets) {
    for(auto muleg : mulegs) {
      std::string histName = std::string("SF_period")+datasetNames[dataset]+"AllYear_"+singlemulegNames[muleg];
      if( ! (TH2F*)(singleMuEffFile->Get(histName.c_str())) )
        std::cout << "SingleMu SF histogram " << histName << " cannot be found in " << singleMuEffFile << endl;
      else 
        singleMuSfMap[dataset][muleg] = (TH2F*)(singleMuEffFile->Get(histName.c_str()));
    }
  }
  
  // Initialize dimu dR correlation map
  datasets = { DATASET::Data15, DATASET::Data16, DATASET::MC16a };
  vector<DIMUTRIG> dimutrigs = { DIMUTRIG::bDimu, DIMUTRIG::bDimu_noL2 };
  vector<ETARANGE> etaranges = { ETARANGE::Barrel, ETARANGE::Overlap, ETARANGE::EndCap };
  for(auto dataset : datasets) {
    for(auto dimutrig : dimutrigs) {
      for(auto etarange : etaranges) {
        std::string histName = datasetNames[dataset]+"_"+dimutrigNames[dimutrig]+"_"+etarangeNames[etarange];
        if(dataset == DATASET::Data16 && dimutrig == DIMUTRIG::bDimu_noL2)
          continue; // no such triggers in 2016+
        if( ! (TF1*)(drCorFile->Get(histName.c_str())) )
          std::cout << "Dimu dR correlation histogram " << histName << " cannot be found in " << drCorFile << endl;
        else
          dimuDrCorMap[dataset][dimutrig][etarange] = (TF1*)(drCorFile->Get(histName.c_str()));
      }
    }
  }
}

double BphysEffTool::getSingleMuLegSF(double pT, double qEta, DATASET datasetData, SINGLEMULEG muleg) {
  TH2F* hist = singleMuSfMap[datasetData][muleg];
  if( !hist ) {
    std::cout << "No singleMuLeg SF hist for " << datasetNames[datasetData] << ", " << singlemulegNames[muleg] << std::endl;
    return 1.;
  }
  if(fabs(qEta) > 2.4) {
    std::cout << "eta(mu) = " << fabs(qEta) << " is beyond 2.4, will return SF = 1." << std::endl;
    return 1.;
  }
  if(pT < 4.0) {
    std::cout << "pT(mu) = " << pT << " is below 4 GeV, will return SF = 1." << std::endl;
    return 1.;
  }
  if(pT > 40.) {
    pT = 39.9;
  }
  int ibin = hist->FindBin(pT,qEta);
  double sf = hist->GetBinContent(ibin);
  return sf;
}

double BphysEffTool::getDrCor(double dR, double y, DATASET dataset, DIMUTRIG dimutrig) {
  ETARANGE etarange;
  if(fabs(y) > 2.3) {
    std::cout << "y(dimu) = " << y << " is beyond 2.3, will returt c_dr for 1.2--2.3 range." << std::endl;
    etarange = ETARANGE::EndCap;
  }
  else if(fabs(y) > 1.2 && fabs(y) <= 2.3) {
    etarange = ETARANGE::EndCap;
  }
  else if(fabs(y) > 1.0 && fabs(y) <= 1.2) {
    etarange = ETARANGE::Overlap;
  }
  else {
    etarange = ETARANGE::Barrel;
  }
  TF1* erfun = dimuDrCorMap[dataset][dimutrig][etarange];
  double drcor = erfun->Eval(dR);
  double unphysNorm = erfun->GetParameter(2);
  drcor /= unphysNorm;
  return drcor;
}

double BphysEffTool::getDrSF(double dR, double y, DATASET dataset, DIMUTRIG dimutrig) {
  DATASET datasetData = dataset;
  DATASET datasetMc;
  if(datasetData == DATASET::Data15 || datasetData == DATASET::Data16)
    datasetMc = DATASET::MC16a;
  else if(datasetData == DATASET::Data17)
    datasetMc = DATASET::MC16d;
  else if(datasetData == DATASET::Data18)
    datasetMc = DATASET::MC16d;
  else {
    std::cout << "Dataset argument should be for data; will return SF = 1." << endl;
    return 1.;
  }
  double sf = getDrCor(dR,y,datasetData,dimutrig) / getDrCor(dR,y,datasetMc,dimutrig);
  return sf;
}

