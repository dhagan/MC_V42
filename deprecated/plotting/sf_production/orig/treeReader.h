//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Apr  7 19:28:08 2020 by ROOT version 6.18/04
// from TTree tree/myTree
// found on file: data_ntuple.root
//////////////////////////////////////////////////////////

#ifndef treeReader_h
#define treeReader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class treeReader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          run;
   UInt_t          evt;
   UInt_t          lb;
   Float_t         actIpX;
   Float_t         avgIpX;
   UInt_t          HLT_2mu4_bJpsimumu;
   UInt_t          HLT_mu6_mu4_bJpsimumu;
   UInt_t          HLT_2mu6_bJpsimumu;
   UInt_t          HLT_mu10_mu6_bJpsimumu;
   UInt_t          HLT_2mu10_bJpsimumu;
   UInt_t          HLT_2mu4_bJpsimumu_delayed;
   UInt_t          HLT_mu6_mu4_bJpsimumu_delayed;
   UInt_t          HLT_2mu6_bJpsimumu_delayed;
   UInt_t          HLT_mu10_mu6_bJpsimumu_delayed;
   UInt_t          HLT_2mu10_bJpsimumu_delayed;
   UInt_t          HLT_2mu4_bJpsimumu_noL2;
   UInt_t          HLT_mu6_mu4_bJpsimumu_noL2;
   UInt_t          HLT_2mu6_bJpsimumu_noL2;
   UInt_t          HLT_mu10_mu6_bJpsimumu_noL2;
   UInt_t          HLT_2mu10_bJpsimumu_noL2;
   std::vector<unsigned int> *Match_2mu4_bJpsimumu;
   std::vector<unsigned int> *Match_mu6_mu4_bJpsimumu;
   std::vector<unsigned int> *Match_2mu6_bJpsimumu;
   std::vector<unsigned int> *Match_mu10_mu6_bJpsimumu;
   std::vector<unsigned int> *Match_2mu10_bJpsimumu;
   std::vector<unsigned int> *Match_2mu4_bJpsimumu_delayed;
   std::vector<unsigned int> *Match_mu6_mu4_bJpsimumu_delayed;
   std::vector<unsigned int> *Match_2mu6_bJpsimumu_delayed;
   std::vector<unsigned int> *Match_mu10_mu6_bJpsimumu_delayed;
   std::vector<unsigned int> *Match_2mu10_bJpsimumu_delayed;
   std::vector<unsigned int> *Match_2mu4_bJpsimumu_noL2;
   std::vector<unsigned int> *Match_mu6_mu4_bJpsimumu_noL2;
   std::vector<unsigned int> *Match_2mu6_bJpsimumu_noL2;
   std::vector<unsigned int> *Match_mu10_mu6_bJpsimumu_noL2;
   std::vector<unsigned int> *Match_2mu10_bJpsimumu_noL2;
   std::vector<float>   *MuPlus_Pt;
   std::vector<float>   *MuPlus_Eta;
   std::vector<float>   *MuPlus_Phi;
   std::vector<float>   *MuPlus_E;
   std::vector<float>   *MuPlus_M;
   std::vector<float>   *mcMuPlus_Pt;
   std::vector<float>   *mcMuPlus_Eta;
   std::vector<float>   *mcMuPlus_Phi;
   std::vector<float>   *mcMuPlus_M;
   std::vector<int>     *MuPlus_quality;
   std::vector<int>     *MuPlus_author;
   std::vector<int>     *MuPlus_AODindex;
   std::vector<float>   *MuPlus_z0;
   std::vector<float>   *MuMinus_Pt;
   std::vector<float>   *MuMinus_Eta;
   std::vector<float>   *MuMinus_Phi;
   std::vector<float>   *MuMinus_E;
   std::vector<float>   *MuMinus_M;
   std::vector<float>   *mcMuMinus_Pt;
   std::vector<float>   *mcMuMinus_Eta;
   std::vector<float>   *mcMuMinus_Phi;
   std::vector<float>   *mcMuMinus_M;
   std::vector<int>     *MuMinus_quality;
   std::vector<int>     *MuMinus_author;
   std::vector<int>     *MuMinus_AODindex;
   std::vector<float>   *MuMinus_z0;
   std::vector<float>   *Photon_Pt;
   std::vector<float>   *Photon_Eta;
   std::vector<float>   *Photon_Phi;
   std::vector<float>   *Photon_E;
   std::vector<float>   *Photon_M;
   std::vector<float>   *mcPhoton_Pt;
   std::vector<float>   *mcPhoton_Eta;
   std::vector<float>   *mcPhoton_Phi;
   std::vector<float>   *mcPhoton_M;
   std::vector<float>   *Photon_TrkIso;
   std::vector<float>   *Photon_CaloIso;
   std::vector<float>   *Photon_CaloIso30;
   std::vector<float>   *Photon_CaloIso40;
   std::vector<int>     *Photon_quality;
   std::vector<int>     *Photon_author;
   std::vector<int>     *Photon_AODindex;
   std::vector<float>   *Photon_etas0;
   std::vector<float>   *Photon_etas1;
   std::vector<float>   *Photon_etas2;
   std::vector<float>   *TruthJpsi_Spx;
   std::vector<float>   *TruthJpsi_Spy;
   std::vector<float>   *TruthJpsi_Spz;
   std::vector<float>   *TruthJpsi_Sm;
   std::vector<float>   *DiLept_Pt;
   std::vector<float>   *DiLept_Eta;
   std::vector<float>   *DiLept_Phi;
   std::vector<float>   *DiLept_Y;
   std::vector<float>   *DiLept_E;
   std::vector<float>   *DiLept_M;
   std::vector<float>   *mcDiLept_Pt;
   std::vector<float>   *mcDiLept_Eta;
   std::vector<float>   *mcDiLept_Phi;
   std::vector<float>   *mcDiLept_M;
   std::vector<float>   *mcDiLept_Y;
   std::vector<float>   *mcDiLept_Dr;
   std::vector<float>   *mcDiLeptFSR_Pt;
   std::vector<float>   *mcDiLeptFSR_Eta;
   std::vector<float>   *mcDiLeptFSR_Phi;
   std::vector<float>   *mcDiLeptFSR_M;
   std::vector<float>   *DiLept_Photon_Dphi;
   std::vector<float>   *DiLept_TrkIso_ptcone20;
   std::vector<float>   *DiLept_TrkIso_ptcone30;
   std::vector<float>   *DiLept_TrkIso_ptcone40;
   std::vector<float>   *DiLept_TrkIso_ptVarCone20;
   std::vector<float>   *DiLept_TrkIso_ptVarCone30;
   std::vector<float>   *DiLept_TrkIso_ptVarCone40;
   std::vector<float>   *DiLept_CaloIso_etcone20;
   std::vector<float>   *DiLept_CaloIso_etcone30;
   std::vector<float>   *PosTrack_TrkIso_ptcone20;
   std::vector<float>   *PosTrack_TrkIso_ptcone30;
   std::vector<float>   *PosTrack_TrkIso_ptVarCone20;
   std::vector<float>   *PosTrack_TrkIso_ptVarCone30;
   std::vector<float>   *NegTrack_TrkIso_ptcone20;
   std::vector<float>   *NegTrack_TrkIso_ptcone30;
   std::vector<float>   *NegTrack_TrkIso_ptVarCone20;
   std::vector<float>   *NegTrack_TrkIso_ptVarCone30;
   std::vector<float>   *DiLept_dR;
   std::vector<float>   *CosTheta;
   std::vector<float>   *MuMuY_Pt;
   std::vector<float>   *MuMuY_Eta;
   std::vector<float>   *MuMuY_Phi;
   std::vector<float>   *MuMuY_Y;
   std::vector<float>   *MuMuY_E;
   std::vector<float>   *MuMuY_M;
   std::vector<float>   *mcMuMuY_Pt;
   std::vector<float>   *mcMuMuY_Eta;
   std::vector<float>   *mcMuMuY_Phi;
   std::vector<float>   *mcMuMuY_M;
   std::vector<float>   *MuMuY_dR;
   std::vector<float>   *MuMuY_dPhi;
   std::vector<float>   *mcMuMuY_dR;
   std::vector<float>   *mcMuMuY_dPhi;
   std::vector<float>   *DiMuonVertex_x;
   std::vector<float>   *DiMuonVertex_y;
   std::vector<float>   *DiMuonVertex_z;
   std::vector<float>   *DiMuonVertex_ChiSq;
   std::vector<float>   *DiMuonVertex_Lxy;
   std::vector<float>   *DiMuonVertex_Lxy_Err;
   std::vector<float>   *DiMuonVertex_Tau;
   std::vector<float>   *DiMuonVertex_Tau_Err;
   std::vector<float>   *DiMuonVertex_Lxy_MinA0;
   std::vector<float>   *DiMuonVertex_Lxy_Err_MinA0;
   std::vector<float>   *DiMuonVertex_Tau_MinA0;
   std::vector<float>   *DiMuonVertex_Tau_Err_MinA0;
   std::vector<float>   *DiMuonVertex_Mass;
   std::vector<float>   *DiMuonVertex_Mass_Err;
   std::vector<float>   *DiMuonVertex_Muon0_Px;
   std::vector<float>   *DiMuonVertex_Muon0_Py;
   std::vector<float>   *DiMuonVertex_Muon0_Pz;
   std::vector<float>   *DiMuonVertex_Muon1_Px;
   std::vector<float>   *DiMuonVertex_Muon1_Py;
   std::vector<float>   *DiMuonVertex_Muon1_Pz;
   std::vector<float>   *DiMuon_DeltaZ0;
   std::vector<float>   *MuMuGamma_CS_CosTheta;
   std::vector<float>   *MuMuGamma_CS_Phi;
   std::vector<float>   *MuMuGamma_CSOLD_CosTheta;
   std::vector<float>   *MuMuGamma_CSOLD_Phi;
   std::vector<float>   *MuMu_CS_CosTheta;
   std::vector<float>   *MuMu_CS_Phi;
   std::vector<float>   *MuMuGamma_CosTheta;
   std::vector<float>   *MuMuGamma_Phi;
   std::vector<float>   *MuMu_CosTheta;
   std::vector<float>   *MuMu_Phi;
   std::vector<float>   *MuMuGamma_CosThetaOLD;
   std::vector<float>   *MuMuGamma_PhiOLD;
   std::vector<float>   *MuMu_CosThetaOLD;
   std::vector<float>   *MuMu_PhiOLD;
   std::vector<float>   *Truth_MuMuGamma_CS_CosTheta;
   std::vector<float>   *Truth_MuMuGamma_CS_Phi;
   std::vector<float>   *Truth_MuMuGamma_CSOLD_CosTheta;
   std::vector<float>   *Truth_MuMuGamma_CSOLD_Phi;
   std::vector<float>   *Truth_MuMu_CS_CosTheta;
   std::vector<float>   *Truth_MuMu_CS_Phi;
   std::vector<float>   *Truth_MuMuGamma_CosTheta;
   std::vector<float>   *Truth_MuMuGamma_Phi;
   std::vector<float>   *Truth_MuMu_CosTheta;
   std::vector<float>   *Truth_MuMu_Phi;
   std::vector<float>   *Truth_MuMuGamma_CosThetaOLD;
   std::vector<float>   *Truth_MuMuGamma_PhiOLD;
   std::vector<float>   *Truth_MuMu_CosThetaOLD;
   std::vector<float>   *Truth_MuMu_PhiOLD;
   std::vector<int>     *permIdx;
   std::vector<int>     *jpsiIdx;
   std::vector<int>     *photonIdx;
   std::vector<int>     *MCMutliJpsiEvt;
   std::vector<int>     *MCParentCCpdgId;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_evt;   //!
   TBranch        *b_lb;   //!
   TBranch        *b_actIpX;   //!
   TBranch        *b_avgIpX;   //!
   TBranch        *b_HLT_2mu4_bJpsimumu;   //!
   TBranch        *b_HLT_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_HLT_2mu6_bJpsimumu;   //!
   TBranch        *b_HLT_mu10_mu6_bJpsimumu;   //!
   TBranch        *b_HLT_2mu10_bJpsimumu;   //!
   TBranch        *b_HLT_2mu4_bJpsimumu_delayed;   //!
   TBranch        *b_HLT_mu6_mu4_bJpsimumu_delayed;   //!
   TBranch        *b_HLT_2mu6_bJpsimumu_delayed;   //!
   TBranch        *b_HLT_mu10_mu6_bJpsimumu_delayed;   //!
   TBranch        *b_HLT_2mu10_bJpsimumu_delayed;   //!
   TBranch        *b_HLT_2mu4_bJpsimumu_noL2;   //!
   TBranch        *b_HLT_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_HLT_2mu6_bJpsimumu_noL2;   //!
   TBranch        *b_HLT_mu10_mu6_bJpsimumu_noL2;   //!
   TBranch        *b_HLT_2mu10_bJpsimumu_noL2;   //!
   TBranch        *b_Match_2mu4_bJpsimumu;   //!
   TBranch        *b_Match_mu6_mu4_bJpsimumu;   //!
   TBranch        *b_Match_2mu6_bJpsimumu;   //!
   TBranch        *b_Match_mu10_mu6_bJpsimumu;   //!
   TBranch        *b_Match_2mu10_bJpsimumu;   //!
   TBranch        *b_Match_2mu4_bJpsimumu_delayed;   //!
   TBranch        *b_Match_mu6_mu4_bJpsimumu_delayed;   //!
   TBranch        *b_Match_2mu6_bJpsimumu_delayed;   //!
   TBranch        *b_Match_mu10_mu6_bJpsimumu_delayed;   //!
   TBranch        *b_Match_2mu10_bJpsimumu_delayed;   //!
   TBranch        *b_Match_2mu4_bJpsimumu_noL2;   //!
   TBranch        *b_Match_mu6_mu4_bJpsimumu_noL2;   //!
   TBranch        *b_Match_2mu6_bJpsimumu_noL2;   //!
   TBranch        *b_Match_mu10_mu6_bJpsimumu_noL2;   //!
   TBranch        *b_Match_2mu10_bJpsimumu_noL2;   //!
   TBranch        *b_MuPlus_Pt;   //!
   TBranch        *b_MuPlus_Eta;   //!
   TBranch        *b_MuPlus_Phi;   //!
   TBranch        *b_MuPlus_E;   //!
   TBranch        *b_MuPlus_M;   //!
   TBranch        *b_mcMuPlus_Pt;   //!
   TBranch        *b_mcMuPlus_Eta;   //!
   TBranch        *b_mcMuPlus_Phi;   //!
   TBranch        *b_mcMuPlus_M;   //!
   TBranch        *b_MuPlus_quality;   //!
   TBranch        *b_MuPlus_author;   //!
   TBranch        *b_MuPlus_AODindex;   //!
   TBranch        *b_MuPlus_z0;   //!
   TBranch        *b_MuMinus_Pt;   //!
   TBranch        *b_MuMinus_Eta;   //!
   TBranch        *b_MuMinus_Phi;   //!
   TBranch        *b_MuMinus_E;   //!
   TBranch        *b_MuMinus_M;   //!
   TBranch        *b_mcMuMinus_Pt;   //!
   TBranch        *b_mcMuMinus_Eta;   //!
   TBranch        *b_mcMuMinus_Phi;   //!
   TBranch        *b_mcMuMinus_M;   //!
   TBranch        *b_MuMinus_quality;   //!
   TBranch        *b_MuMinus_author;   //!
   TBranch        *b_MuMinus_AODindex;   //!
   TBranch        *b_MuMinus_z0;   //!
   TBranch        *b_Photon_Pt;   //!
   TBranch        *b_Photon_Eta;   //!
   TBranch        *b_Photon_Phi;   //!
   TBranch        *b_Photon_E;   //!
   TBranch        *b_Photon_M;   //!
   TBranch        *b_mcPhoton_Pt;   //!
   TBranch        *b_mcPhoton_Eta;   //!
   TBranch        *b_mcPhoton_Phi;   //!
   TBranch        *b_mcPhoton_M;   //!
   TBranch        *b_Photon_TrkIso;   //!
   TBranch        *b_Photon_CaloIso;   //!
   TBranch        *b_Photon_CaloIso30;   //!
   TBranch        *b_Photon_CaloIso40;   //!
   TBranch        *b_Photon_quality;   //!
   TBranch        *b_Photon_author;   //!
   TBranch        *b_Photon_AODindex;   //!
   TBranch        *b_Photon_etas0;   //!
   TBranch        *b_Photon_etas1;   //!
   TBranch        *b_Photon_etas2;   //!
   TBranch        *b_TruthJpsi_Spx;   //!
   TBranch        *b_TruthJpsi_Spy;   //!
   TBranch        *b_TruthJpsi_Spz;   //!
   TBranch        *b_TruthJpsi_Sm;   //!
   TBranch        *b_DiLept_Pt;   //!
   TBranch        *b_DiLept_Eta;   //!
   TBranch        *b_DiLept_Phi;   //!
   TBranch        *b_DiLept_Y;   //!
   TBranch        *b_DiLept_E;   //!
   TBranch        *b_DiLept_M;   //!
   TBranch        *b_mcDiLept_Pt;   //!
   TBranch        *b_mcDiLept_Eta;   //!
   TBranch        *b_mcDiLept_Phi;   //!
   TBranch        *b_mcDiLept_M;   //!
   TBranch        *b_mcDiLept_Y;   //!
   TBranch        *b_mcDiLept_Dr;   //!
   TBranch        *b_mcDiLeptFSR_Pt;   //!
   TBranch        *b_mcDiLeptFSR_Eta;   //!
   TBranch        *b_mcDiLeptFSR_Phi;   //!
   TBranch        *b_mcDiLeptFSR_M;   //!
   TBranch        *b_DiLept_Photon_Dphi;   //!
   TBranch        *b_DiLept_TrkIso_ptcone20;   //!
   TBranch        *b_DiLept_TrkIso_ptcone30;   //!
   TBranch        *b_DiLept_TrkIso_ptcone40;   //!
   TBranch        *b_DiLept_TrkIso_ptVarCone20;   //!
   TBranch        *b_DiLept_TrkIso_ptVarCone30;   //!
   TBranch        *b_DiLept_TrkIso_ptVarCone40;   //!
   TBranch        *b_DiLept_CaloIso_etcone20;   //!
   TBranch        *b_DiLept_CaloIso_etcone30;   //!
   TBranch        *b_PosTrack_TrkIso_ptcone20;   //!
   TBranch        *b_PosTrack_TrkIso_ptcone30;   //!
   TBranch        *b_PosTrack_TrkIso_ptVarCone20;   //!
   TBranch        *b_PosTrack_TrkIso_ptVarCone30;   //!
   TBranch        *b_NegTrack_TrkIso_ptcone20;   //!
   TBranch        *b_NegTrack_TrkIso_ptcone30;   //!
   TBranch        *b_NegTrack_TrkIso_ptVarCone20;   //!
   TBranch        *b_NegTrack_TrkIso_ptVarCone30;   //!
   TBranch        *b_DiLept_dR;   //!
   TBranch        *b_CosTheta;   //!
   TBranch        *b_MuMuY_Pt;   //!
   TBranch        *b_MuMuY_Eta;   //!
   TBranch        *b_MuMuY_Phi;   //!
   TBranch        *b_MuMuY_Y;   //!
   TBranch        *b_MuMuY_E;   //!
   TBranch        *b_MuMuY_M;   //!
   TBranch        *b_mcMuMuY_Pt;   //!
   TBranch        *b_mcMuMuY_Eta;   //!
   TBranch        *b_mcMuMuY_Phi;   //!
   TBranch        *b_mcMuMuY_M;   //!
   TBranch        *b_MuMuY_dR;   //!
   TBranch        *b_MuMuY_dPhi;   //!
   TBranch        *b_mcMuMuY_dR;   //!
   TBranch        *b_mcMuMuY_dPhi;   //!
   TBranch        *b_DiMuonVertex_x;   //!
   TBranch        *b_DiMuonVertex_y;   //!
   TBranch        *b_DiMuonVertex_z;   //!
   TBranch        *b_DiMuonVertex_ChiSq;   //!
   TBranch        *b_DiMuonVertex_Lxy;   //!
   TBranch        *b_DiMuonVertex_Lxy_Err;   //!
   TBranch        *b_DiMuonVertex_Tau;   //!
   TBranch        *b_DiMuonVertex_Tau_Err;   //!
   TBranch        *b_DiMuonVertex_Lxy_MinA0;   //!
   TBranch        *b_DiMuonVertex_Lxy_Err_MinA0;   //!
   TBranch        *b_DiMuonVertex_Tau_MinA0;   //!
   TBranch        *b_DiMuonVertex_Tau_Err_MinA0;   //!
   TBranch        *b_DiMuonVertex_Mass;   //!
   TBranch        *b_DiMuonVertex_Mass_Err;   //!
   TBranch        *b_DiMuonVertex_Muon0_Px;   //!
   TBranch        *b_DiMuonVertex_Muon0_Py;   //!
   TBranch        *b_DiMuonVertex_Muon0_Pz;   //!
   TBranch        *b_DiMuonVertex_Muon1_Px;   //!
   TBranch        *b_DiMuonVertex_Muon1_Py;   //!
   TBranch        *b_DiMuonVertex_Muon1_Pz;   //!
   TBranch        *b_DiMuon_DeltaZ0;   //!
   TBranch        *b_MuMuGamma_CS_CosTheta;   //!
   TBranch        *b_MuMuGamma_CS_Phi;   //!
   TBranch        *b_MuMuGamma_CSOLD_CosTheta;   //!
   TBranch        *b_MuMuGamma_CSOLD_Phi;   //!
   TBranch        *b_MuMu_CS_CosTheta;   //!
   TBranch        *b_MuMu_CS_Phi;   //!
   TBranch        *b_MuMuGamma_CosTheta;   //!
   TBranch        *b_MuMuGamma_Phi;   //!
   TBranch        *b_MuMu_CosTheta;   //!
   TBranch        *b_MuMu_Phi;   //!
   TBranch        *b_MuMuGamma_CosThetaOLD;   //!
   TBranch        *b_MuMuGamma_PhiOLD;   //!
   TBranch        *b_MuMu_CosThetaOLD;   //!
   TBranch        *b_MuMu_PhiOLD;   //!
   TBranch        *b_Truth_MuMuGamma_CS_CosTheta;   //!
   TBranch        *b_Truth_MuMuGamma_CS_Phi;   //!
   TBranch        *b_Truth_MuMuGamma_CSOLD_CosTheta;   //!
   TBranch        *b_Truth_MuMuGamma_CSOLD_Phi;   //!
   TBranch        *b_Truth_MuMu_CS_CosTheta;   //!
   TBranch        *b_Truth_MuMu_CS_Phi;   //!
   TBranch        *b_Truth_MuMuGamma_CosTheta;   //!
   TBranch        *b_Truth_MuMuGamma_Phi;   //!
   TBranch        *b_Truth_MuMu_CosTheta;   //!
   TBranch        *b_Truth_MuMu_Phi;   //!
   TBranch        *b_Truth_MuMuGamma_CosThetaOLD;   //!
   TBranch        *b_Truth_MuMuGamma_PhiOLD;   //!
   TBranch        *b_Truth_MuMu_CosThetaOLD;   //!
   TBranch        *b_Truth_MuMu_PhiOLD;   //!
   TBranch        *b_permIdx;   //!
   TBranch        *b_jpsiIdx;   //!
   TBranch        *b_photonIdx;   //!
   TBranch        *b_MCMutliJpsiEvt;   //!
   TBranch        *b_MCParentCCpdgId;   //!

   treeReader(TTree *tree=0);
   //virtual ~treeReader();
   //virtual Int_t    Cut(Long64_t entry);
   //virtual Int_t    GetEntry(Long64_t entry);
   //virtual Long64_t LoadTree(Long64_t entry);
   //virtual void     Init(TTree *tree);
   //virtual void     Loop();
   //virtual Bool_t   Notify();
   //virtual void     Show(Long64_t entry = -1);

  ~treeReader();
  Int_t    Cut(Long64_t entry);         
  Int_t    GetEntry(Long64_t entry);
  Long64_t LoadTree(Long64_t entry);
  void     Init(TTree *tree);
  void     Loop();
  Bool_t   Notify();
  void     Show(Long64_t entry = -1);


};

#endif


