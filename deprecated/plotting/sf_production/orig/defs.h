#ifndef DEFS_H
#define DEFS_H
#include <iostream>

enum DATASET { Data15, Data16, Data17, Data18, MC16a, MC16d, MC16e };
std::vector< std::string > datasetNames = { "Data15", "Data16", "Data17", "Data18", "MC16a", "MC16d", "MC16e" };
enum SINGLEMULEG { mu4, mu6, mu4_nomucomb, mu6_nomucomb };
std::vector< std::string > singlemulegNames = { "mu4", "mu6", "mu4_nomucomb", "mu6_nomucomb" };
enum DIMUTRIG { bDimu, bDimu_noL2 };
std::vector< std::string > dimutrigNames = { "bDimu", "bDimu_noL2" };
enum ETARANGE { Barrel, Overlap, EndCap };
std::vector< std::string > etarangeNames = { "Barrel", "Overlap", "EndCap" };

#endif

