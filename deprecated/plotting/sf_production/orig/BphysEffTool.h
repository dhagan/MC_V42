#ifndef BPHYSEFFTOOL_H
#define BPHYSEFFTOOL_H

#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "defs.h"

#include <map>

class BphysEffTool {
public:
  BphysEffTool(std::string singleMuEffFileName = "singleMu_SF_data1516_etabins.root", std::string drCorFileName = "c_dr_fixed.root");
  
  double getSingleMuLegSF(double pT, double qEta, DATASET datasetData, SINGLEMULEG muleg);
  double getDrCor(double dR, double y, DATASET dataset, DIMUTRIG dimutrig);
  double getDrSF(double dR, double y, DATASET dataset, DIMUTRIG dimutrig);
  
private:
  std::string m_singleMuEffFileName;
  std::string m_drCorFileName;
  
  std::map< DATASET, std::map< SINGLEMULEG, TH2F* > > singleMuSfMap;
  std::map< DATASET, std::map< DIMUTRIG, std::map<ETARANGE, TF1* > > > dimuDrCorMap;
  
};
#endif
