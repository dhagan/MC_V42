
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>


void check(){
  
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gStyle->SetOptFit(0);
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  //std::vector<std::string> variables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM"};
  //std::vector<double> low_edges{-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5};
  //std::vector<double> high_edges{M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15};
  std::vector<std::string> variables =        {"DPhi","AbsdPhi","AbsPhi","qtA","qtB","qtL","qtM","cos( (3.14159-AbsdPhi)/2.0 )"};
  std::vector<std::string> variables_outstr = {"DPhi","AbsdPhi","AbsPhi","qtA","qtB","qtL","qtM","cosEpsilonOver2"};
  std::vector<double> low_edges{-M_PI*1.1,   0,   0,-10,-20, -20,-20,-1.0};
  std::vector<double> high_edges{M_PI*1.1,M_PI,M_PI, 20, 20,  20, 20, 1.0};
  

  std::vector<std::string> file_types = {"signal","data","pp","bb"};
  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};


  for ( int mass_range = 0; mass_range<mass_cuts.size(); mass_range++){ 

    for ( int file_index = 0; file_index < file_types.size(); file_index++){

      
      const char * file_type_str = file_types[file_index].c_str();
      TFile tmva_output_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_%s/Evaluate_%s.root",file_type_str,file_type_str),"READ"};

      TTree * tmva_output_tree = (TTree*) tmva_output_file.Get(Form("%s_00_mu4000_P5000_bound-0",file_type_str));

      std::vector<TH2F> hist_vector;
      std::vector<std::string> x_variable_str;
      std::vector<std::string> y_variable_str;

      for (int xvariable_index = 0; xvariable_index<variables.size(); xvariable_index++){
        for (int yvariable_index = 0; yvariable_index<variables.size(); yvariable_index++){
          if (xvariable_index == yvariable_index) continue;

          const char * xvariable = variables[xvariable_index].c_str();
          const char * yvariable = variables[yvariable_index].c_str();

          const char * xvariable_str = variables_outstr[xvariable_index].c_str();
          const char * yvariable_str = variables_outstr[yvariable_index].c_str();

          char output_name[20],hist_title[100];
          sprintf(output_name,"output_%i_%i_%i",file_index,xvariable_index,yvariable_index);
          sprintf(hist_title,"%s_%s_%s",xvariable_str,yvariable_str,mass_cuts[mass_range].c_str());

          TH2F current_hist{hist_title,hist_title,50,low_edges[xvariable_index],high_edges[xvariable_index],50,low_edges[yvariable_index],high_edges[yvariable_index]};
          tmva_output_tree->Draw(Form("%s:%s>>%s",yvariable,xvariable,hist_title),mass_ranges[mass_range].c_str(),"goff");
          x_variable_str.push_back(variables_outstr[xvariable_index]);
          y_variable_str.push_back(variables_outstr[yvariable_index]);

          TCanvas canv{"canv","",200,100,2000,2000};
          canv.Divide(1,1);
          canv.cd(1);
          current_hist.Draw("COLZ");
          current_hist.GetXaxis()->SetTitle(variables_outstr[xvariable_index].c_str());
          current_hist.GetYaxis()->SetTitle(variables_outstr[yvariable_index].c_str());
          TPaveText * integral_text = new TPaveText(0.05,0.93,0.90,0.99,"nbNDC");
          integral_text->AddText(Form("%s vs %s, %s, %s",variables_outstr[yvariable_index].c_str(),variables_outstr[xvariable_index].c_str(),file_types[file_index].c_str(),mass_cuts[mass_range].c_str()));
          integral_text->SetBorderSize(1);
          integral_text->SetFillStyle(0);
          integral_text->SetFillColor(0);
          integral_text->SetTextFont(42);
          integral_text->SetTextSize(0.03);
          integral_text->SetTextAlign(31);
          integral_text->Draw();
          canv.SaveAs(Form("./pngs/%s_%s_xvar_%s_yvar_%s.png",file_type_str,mass_cuts[mass_range].c_str(),variables_outstr[yvariable_index].c_str(),variables_outstr[xvariable_index].c_str()));

        }
      } 

      
      
      std::vector<TPaveText> text_vector;
      std::vector<TLegend> legend_vector;

      for (int hist_index = 0; hist_index<hist_vector.size(); hist_index++){

        
      }
    }
  }
}

int main(){
  check(); 
}
