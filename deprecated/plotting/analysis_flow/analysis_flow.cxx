#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TF1.h"
#include "math.h"
#include "assert.h"
#include "TSystem.h"
#include "TLatex.h"
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <math.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>
#include "TString.h"
#include <fstream>
#include <cassert>
#include "TFile.h"
#include "TROOT.h"


void analysisFlow(){

  FILE *outfile =fopen(Form("AnalysisFlow.tex"), "w");
  std::vector<std::string> Slice;
  //Slice = {"00",
	//  			 "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "310",
	//  			 "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "410",
	//  			 "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "510"}; 
  //std::vector<std::string> MSSSlice = {"11","21","31","41","12","22","32","42","71","81","72","82","91","92","99"};
  //std::vector<std::string> cut = {"0","1","2","3","4","5","6","7","8","9","10","11"}; 
  
  fprintf(outfile,  "\\begin{figure}[h!]  \n");
  fprintf(outfile,  "\\centering  \n");
  fprintf(outfile,  "\\begin{tabular}  { |l||r|r|r|r||}  \n");
  fprintf(outfile,  "\\hline \n");
  fprintf(outfile,  "stage/cut & pp & signal & bb & data\\\\ \n");


  fprintf(outfile,  "\\hline \n");
 
  TFile *ppNtuple     = new TFile("/home/atlas/dhagan/analysisFiles/analysisFork/pp_ntuple.root");
  TFile *bbNtuple     = new TFile("/home/atlas/dhagan/analysisFiles/analysisFork/bb_ntuple.root");
  TFile *dataNtuple   = new TFile("/home/atlas/dhagan/analysisFiles/analysisFork/data_ntuple.root");
  TFile *signalNtuple = new TFile("/home/atlas/dhagan/analysisFiles/analysisFork/signal_ntuple.root");

  std::map<TFile,std::string> filemap;


  fprintf(outfile, "ntuples &");
  fprintf(outfile, Form("%i&",(TTree*)ppNtuple->Get("tree"))->GetEntries())
  fprintf(outfile, Form("%i&",(TTree*)signalNtuple->Get("tree"))->GetEntries())
  fprintf(outfile, Form("%i&",(TTree*)bbNtuple->Get("tree"))->GetEntries())
  fprintf(outfile, Form("%i&",(TTree*)dataNtuple->Get("tree"))->GetEntries())

  //fprintf(outfile, "%s &",Slice[i].c_str());
  //fprintf(outfile, "%i & ",(int) ((TH1F*)   f0pp->Get(Form("qTa_pp_%s",finalCut.c_str())))->GetEntries());


  fprintf(outfile, "\\hline \n"); //end of row
  fprintf(outfile, "\\end{tabular} \n");
  fprintf(outfile, "\\end{figure} \n");

}

