#include <tr.hxx>



int help(){
  std::cout << " i can't help you dude i can barely help myself" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

 int option{0}, option_index{0};
 std::string mode, sign, bckg;

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'b'},
      {"sign",        required_argument,        0,      's'},
      {"bckg",        required_argument,        0,      'b'},
      {"mode",        required_argument,        0,      'm'},
      {0,             0,                        0,      0}
  };

  do {
    option = getopt_long( argc, argv, "s:b:m:h", long_options, &option_index);
    switch (option){
      case 'h':
        return help();
      case 'm': 
        mode      = std::string( optarg );
        break;
      case 'b': 
        bckg      = std::string( optarg );
        break;
      case 's': 
        sign      = std::string( optarg );
        break;
    }
  } while ( option != -1 );

  if ( mode.find( "tree" ) != std::string::npos ){
    tr( sign, bckg );
  }

}
