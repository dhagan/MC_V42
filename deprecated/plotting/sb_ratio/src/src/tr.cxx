#include <tr.hxx>
#include <split_string.hxx>

void tr( std::string sign, std::string bckg){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);


  std::map< int ,std::string> mass_ranges;
  mass_ranges[0]             = std::string("Lambda>0&&Lambda<200");
  mass_ranges[3]             = std::string("Lambda>25&&Lambda<50");
  mass_ranges[4]             = std::string("Lambda>50&&Lambda<100");
  mass_ranges[5]             = std::string("Lambda>100&&Lambda<200");
  mass_ranges[12]            = std::string("Lambda>25&&Lambda<200");

  std::vector< std::string > var_vec = { "qtA", "abs(qtB)", "abs(Phi)" };
  std::map< std::string, std::vector< std::string >> var_cuts;
  var_cuts["qtA"]           = {"(qtA>-10)&&(qtA<20)"};
  var_cuts["abs(qtB)"]      = {"(abs(qtB)>0)&&(abs(qtB)<20)"};
  var_cuts["abs(Phi)"]      = {"abs(Phi)<3.141592","(abs(Phi)<3.141592)&&((costheta*costheta)<0.1)",
                              "(abs(Phi)<3.141592)&&((costheta*costheta)>0.1)"};
  
  std::map< std::string, std::vector<double> > bin_map;
  bin_map["qtA"] = { 15, -10, 20 };
  bin_map["abs(qtB)"] = { 10, 0, 20 };
  bin_map["abs(Phi)"] = { 8, 0, M_PI };
  
  std::map< std::string, std::string > output_names;
  output_names["qtA"] = "qtA";
  output_names["abs(qtB)"] = "qtB";
  output_names["abs(Phi)"] = "Phi";
  std::map< std::string, std::vector< std::string >> output_types;
  output_types["qtA"]       = { "" };
  output_types["abs(qtB)"]  = { "" };
  output_types["abs(Phi)"]  = { "", "low", "high" };

  std::map<std::string,std::string> units;
  units["qtA"]           =  "GeV";
  units["abs(qtB)"]      =  "GeV";
  units["abs(Phi)"]           =  "rad";


  TLatex ATLAS; ATLAS.SetNDC();
  ATLAS.SetTextFont(72); ATLAS.SetTextColor(1);
  TLatex wip;  wip.SetNDC(); wip.SetTextFont(42);
  wip.SetTextSize(0.038); wip.SetTextColor(1);

  std::vector< std::string > sign_vec;
  std::vector< std::string > bckg_vec;
  split_strings( sign_vec, sign, ":" );
  split_strings( bckg_vec, bckg, ":" );
  std::string sign_file_str = sign_vec.at(0);
  std::string sign_tree_str = sign_vec.at(1);
  std::string bckg_file_str = bckg_vec.at(0);
  std::string bckg_tree_str = bckg_vec.at(1);

  TFile * sign_file = new TFile( sign_file_str.c_str(), "READ" ); 
  TFile * bckg_file = new TFile( bckg_file_str.c_str(), "READ" ); 

  TTree * sign_tree = (TTree*) sign_file->Get( sign_tree_str.c_str() );
  TTree * bckg_tree = (TTree*) bckg_file->Get( bckg_tree_str.c_str() );
  
  std::vector< int > masses = { 0, 3, 4, 5, 12 }; 
  
  for ( int & mass : masses ){
    
    std::string & mass_range = mass_ranges.find( mass )->second;

    for ( std::string & var : var_vec ){
    
      int bins        = (int) bin_map[var].at(0); 
      double bin_min  = bin_map[var].at(1);
      double bin_max  = bin_map[var].at(2);
      double width =  (double) (bin_max-bin_min)/((double) bins);
      std::string unit = units[var];

      for ( int cut_no = 0; cut_no < (int) var_cuts[var].size(); cut_no++ ){
        
        std::string var_cut = var_cuts[var].at(cut_no);
        std::string safe_name = output_names[var];
        std::string cut_name = output_types[var].at( cut_no );
        std::string out_name = ( cut_name.empty() ) ? ( safe_name + "_Q" + std::to_string(mass) ) : ( safe_name + "_" + cut_name +
                                "_Q" + std::to_string(mass));

        std::string sign_hist_name = "sign_" + out_name;
        std::string bckg_hist_name = "bckg_" + out_name;
        TH1F * sign_hist = new TH1F( sign_hist_name.c_str(), out_name.c_str(), bins, bin_min, bin_max );
        TH1F * bckg_hist = new TH1F( bckg_hist_name.c_str(), out_name.c_str(), bins, bin_min, bin_max );

        TH1F * norm_sign_hist = new TH1F( ("norm_" + sign_hist_name).c_str(), out_name.c_str(), bins, bin_min, bin_max );
        TH1F * norm_bckg_hist = new TH1F( ("norm_" + bckg_hist_name).c_str(), out_name.c_str(), bins, bin_min, bin_max );

        std::string current_cut = mass_range + "&&" +  var_cut;

        sign_tree->Draw( Form("%s>>%s",var.c_str(), sign_hist_name.c_str()), current_cut.c_str(), "goff" );
        bckg_tree->Draw( Form("%s>>%s",var.c_str(), bckg_hist_name.c_str()), current_cut.c_str(), "goff" );

        TH1F * ratio_hist = new TH1F( "ratio", out_name.c_str(), bins, bin_min, bin_max );
        ratio_hist->Divide( sign_hist, bckg_hist, 1.0, 1.0 );
        TCanvas * canv = new TCanvas("canv","", 100, 100, 2000, 2000 );
        canv->Divide(1,1);
        canv->cd(1);
        double axis_lim = std::ceil( ratio_hist->GetMaximum()*1.3 );
        ratio_hist->Draw("E1");
        ratio_hist->SetMarkerStyle(21);
        ratio_hist->SetLineColor(1);
        ratio_hist->GetYaxis()->SetRangeUser(0,axis_lim);
        ratio_hist->GetYaxis()->SetTitle( Form("S/B ratio/%.3f %s", width, unit.c_str()));
        ratio_hist->GetXaxis()->SetTitle( Form("%s (%s)", var.c_str(), unit.c_str()));
        ratio_hist->GetYaxis()->SetLabelSize(0.035);
        ratio_hist->GetYaxis()->SetTitleSize(0.035);
        ratio_hist->GetXaxis()->SetLabelSize(0.035);
        ratio_hist->GetXaxis()->SetTitleSize(0.035);
        gPad->Update();
        gPad->SetTicks(1,0);
        int pad_scale = sign_hist->Integral()/axis_lim;
        sign_hist->Scale( 1.0/pad_scale );
        bckg_hist->Scale( 1.0/pad_scale );    
        sign_hist->SetLineWidth(0);
        sign_hist->SetFillColorAlpha(kRed+1,0.5);
        bckg_hist->SetLineWidth(0);
        bckg_hist->SetFillColorAlpha(kBlue+1,0.5);
        sign_hist->Draw("HIST SAME");
        bckg_hist->Draw("HIST SAME");
        TGaxis * pad_axis = new TGaxis(bin_max,0,bin_max,axis_lim,0,pad_scale,510,"+L");
        pad_axis->SetLabelSize( 0.035 );             
        pad_axis->SetLabelFont( 42 );                
        pad_axis->SetTitleSize( 0.035 );
        pad_axis->SetTitleOffset( 1.25 );
        pad_axis->SetMaxDigits( 4 );
        pad_axis->SetTitleFont( 42 );                
        pad_axis->SetTitle( Form( "Entries/%.3f %s", width, unit.c_str() ));
        pad_axis->Draw();
        TLegend * pad_legend = new TLegend( 0.65, 0.65, 0.83, 0.8 );
        pad_legend->SetBorderSize( 0 );
        pad_legend->SetFillColor( 0 );
        pad_legend->SetFillStyle( 0 );
        pad_legend->SetTextFont( 42 );
        pad_legend->SetTextSize( 0.025 );
        pad_legend->AddEntry( ratio_hist, "Ratio", "LP");
        pad_legend->AddEntry( sign_hist, "Signal", "F");
        pad_legend->AddEntry( bckg_hist, "Background", "F");
        pad_legend->Draw();
        TLatex pad_ltx;
        pad_ltx.SetTextSize( 0.03 );
        pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Ratio - Mass Q%i", safe_name.c_str(), mass ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );


        std::string save_string;
        canv->SaveAs(Form("./%s.png",out_name.c_str()));
        delete canv;
        delete ratio_hist;



        norm_sign_hist->Scale(1.0/norm_sign_hist->Integral());
        norm_bckg_hist->Scale(1.0/norm_bckg_hist->Integral());

        TH1F * norm_ratio_hist = new TH1F( "norm_ratio", out_name.c_str(), bins, bin_min, bin_max );
        norm_ratio_hist->Divide( norm_sign_hist, norm_bckg_hist, 1.0, 1.0 );
        TCanvas * norm_canv = new TCanvas("canv","", 100, 100, 2000, 2000 );
        norm_canv->Divide(1,1);
        norm_canv->cd(1);
        double norm_axis_lim = std::ceil( norm_ratio_hist->GetMaximum()*1.3 );
        norm_ratio_hist->Draw("E1");
        norm_ratio_hist->SetMarkerStyle(21);
        norm_ratio_hist->SetLineColor(1);
        norm_ratio_hist->GetYaxis()->SetRangeUser(0,norm_axis_lim);
        norm_ratio_hist->GetYaxis()->SetTitle( Form("S/B ratio/%.3f %s", width, unit.c_str()));
        norm_ratio_hist->GetXaxis()->SetTitle( Form("%s (%s)", var.c_str(), unit.c_str()));
        norm_ratio_hist->GetYaxis()->SetLabelSize(0.035);
        norm_ratio_hist->GetYaxis()->SetTitleSize(0.035);
        norm_ratio_hist->GetXaxis()->SetLabelSize(0.035);
        norm_ratio_hist->GetXaxis()->SetTitleSize(0.035);
        gPad->Update();
        gPad->SetTicks(1,0);
        int norm_pad_scale = norm_sign_hist->Integral()/norm_axis_lim;
        norm_sign_hist->Scale( 1.0/norm_pad_scale );
        norm_bckg_hist->Scale( 1.0/norm_pad_scale );    
        norm_sign_hist->SetLineWidth(0);
        norm_sign_hist->SetFillColorAlpha(kRed+1,0.5);
        norm_bckg_hist->SetLineWidth(0);
        norm_bckg_hist->SetFillColorAlpha(kBlue+1,0.5);
        norm_sign_hist->Draw("HIST SAME");
        norm_bckg_hist->Draw("HIST SAME");
        TGaxis * norm_pad_axis = new TGaxis(bin_max,0,bin_max,norm_axis_lim,0,norm_pad_scale,510,"+L");
        norm_pad_axis->SetLabelSize( 0.035 );             
        norm_pad_axis->SetLabelFont( 42 );                
        norm_pad_axis->SetTitleSize( 0.035 );
        norm_pad_axis->SetTitleOffset( 1.25 );
        norm_pad_axis->SetMaxDigits( 4 );
        norm_pad_axis->SetTitleFont( 42 );                
        norm_pad_axis->SetTitle( Form( "Entries/%.3f %s", width, unit.c_str() ));
        norm_pad_axis->Draw();
        TLegend * norm_pad_legend = new TLegend( 0.65, 0.65, 0.83, 0.8 );
        norm_pad_legend->SetBorderSize( 0 );
        norm_pad_legend->SetFillColor( 0 );
        norm_pad_legend->SetFillStyle( 0 );
        norm_pad_legend->SetTextFont( 42 );
        norm_pad_legend->SetTextSize( 0.025 );
        norm_pad_legend->AddEntry( norm_ratio_hist, "Ratio", "LP");
        norm_pad_legend->AddEntry( norm_sign_hist, "Signal", "F");
        norm_pad_legend->AddEntry( norm_bckg_hist, "Background", "F");
        norm_pad_legend->Draw();
        TLatex norm_pad_ltx;
        norm_pad_ltx.SetTextSize( 0.03 );
        norm_pad_ltx.DrawLatexNDC( 0.2, 0.825, Form( "%s Nrom Ratio - Mass Q%i", safe_name.c_str(), mass ));  
        ATLAS.DrawLatexNDC( 0.2, 0.75, "ATLAS" );
        wip.DrawLatexNDC( 0.2, 0.7, "Work In Progress" );

        norm_canv->SaveAs(Form("./norm_%s.png",out_name.c_str()));
        delete norm_canv;
        delete norm_ratio_hist;


      }

    }
  }

}



