
#include <TH1.h>
#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <iostream>
#include <vector>
#include <string>
#include <TROOT.h>
#include <TStyle.h>
#include <cmath>

void check(int mode){
   
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);

  std::string mode_string;
  if (mode == 0) mode_string = "both";
  if (mode == 1) mode_string = "even";
  if (mode == 2) mode_string = "odd";

  //TFile sign_file{"../../../TMVA/run/Eval_signal/Evaluate_signal.root","READ"}; 
  //TFile bckg_file{"../../../TMVA/run/Eval_pp/Evaluate_pp.root","READ"}; 

  TFile mix_file{Form("../../../TMVA/run/Eval_mix/Evaluate_mix%s.root",mode_string.c_str()),"READ"}; 
  TFile mini_sign_file{Form("../../../TMVA/run/Eval_minisign/Evaluate_minisign%s.root",mode_string.c_str()),"READ"}; 
  TFile mini_bckg_file{Form("../../../TMVA/run/Eval_minibckg/Evaluate_minibckg%s.root",mode_string.c_str()),"READ"}; 

  std::cout << "here" << std::endl;

  TTree * mini_sign_tree = (TTree*) mini_sign_file.Get(Form("mini-sign_bound-0_events-%s",mode_string.c_str()));
  TTree * mini_bckg_tree = (TTree*) mini_bckg_file.Get(Form("mini-bckg_bound-0_events-%s",mode_string.c_str()));
  TTree * mix_tree = (TTree*) mix_file.Get(Form("sign-bckg_mix_bound-0_events-%s",mode_string.c_str()));

  std::vector<std::string> variable_strings = {"","AbsCosTheta","AbsdPhi","AbsPhi","DPhi","DY","Phi","costheta",
                                               "EventNumber","Lambda","qTSquared","DiMuonMass","DiMuonTau","BDT",
                                               "DiMuonPt","PhotonPt","AbsdY","qxpsi","qypsi","qxgamma","qygamma",
                                               "qxsum","qysum","qtA","qtB"};

  std::cout << "here" << std::endl;
  
  std::vector<double> low_edges =   {0, 0,   0,   0,-M_PI,-4,-M_PI,-1,   0,  0,  0,2700,-5.0,-1.0,    0,     0, 0,-15,-20,-20,-20, 0,-30,-5,-15};
  std::vector<double> high_edges =  {0, 1,M_PI,M_PI, M_PI, 4, M_PI, 1, 1E7,200,400,3500, 5.0, 1.0,30000, 20000, 4, 20, 20, 20, 20,50, 30,15, 15};

  TCanvas var_canv{"var_canv","var_canv",600,600,6000,4000};
  var_canv.Divide(6,4);
  std::vector<TH1F> histogram_vec;

  for (int pad_no = 1; pad_no < 25; pad_no++){

    std::cout << "" << std::endl;
    std::cout << pad_no << std::endl;
    std::cout << variable_strings[pad_no] << std::endl;
    std::cout << low_edges[pad_no] << std::endl;
    std::cout << high_edges[pad_no] << std::endl;
    std::cout << "hist nos;" << std::endl;
    std::cout << (pad_no-1)*4 << std::endl;
    std::cout << 1+(pad_no-1)*4 << std::endl;
    std::cout << 2+(pad_no-1)*4 << std::endl;
    std::cout << 3+(pad_no-1)*4 << std::endl;
    std::cout << "" << std::endl;

    int bin_count = (pad_no == 13) ? 10 : 30;

    histogram_vec.push_back(TH1F{Form("mix_%i",pad_no),variable_strings[pad_no].c_str(),bin_count,low_edges[pad_no],high_edges[pad_no]});
    histogram_vec.push_back(TH1F{Form("sign_%i",pad_no),variable_strings[pad_no].c_str(),bin_count,low_edges[pad_no],high_edges[pad_no]});
    histogram_vec.push_back(TH1F{Form("bckg_%i",pad_no),variable_strings[pad_no].c_str(),bin_count,low_edges[pad_no],high_edges[pad_no]});
    histogram_vec.push_back(TH1F{Form("comb_%i",pad_no),variable_strings[pad_no].c_str(),bin_count,low_edges[pad_no],high_edges[pad_no]});
  
    mini_sign_tree->Draw(Form("%s>>sign_%i",variable_strings[pad_no].c_str(),pad_no),"","goff");
    mini_bckg_tree->Draw(Form("%s>>bckg_%i",variable_strings[pad_no].c_str(),pad_no),"","goff");
    mix_tree->Draw(Form("%s>>mix_%i",variable_strings[pad_no].c_str(),pad_no),"","goff");

    histogram_vec[ 3+(pad_no-1)*4].Add(&histogram_vec[1+(pad_no-1)*4],&histogram_vec[2+(pad_no-1)*4],1.0,1.0);

    std::cout << histogram_vec[0+(pad_no-1)*4].Integral() << std::endl;
    std::cout << histogram_vec[1+(pad_no-1)*4].Integral() << std::endl;
    std::cout << histogram_vec[2+(pad_no-1)*4].Integral() << std::endl;
    std::cout << histogram_vec[3+(pad_no-1)*4].Integral() << std::endl;
  
  }

  for (int pad_no = 1; pad_no < 25; pad_no++){

    var_canv.cd(pad_no);
    histogram_vec[0+(pad_no-1)*4].Draw("HIST"); 
    histogram_vec[1+(pad_no-1)*4].Draw("HIST SAME"); 
    histogram_vec[2+(pad_no-1)*4].Draw("HIST SAME"); 
    histogram_vec[3+(pad_no-1)*4].Draw("HIST SAME");
    histogram_vec[0+(pad_no-1)*4].SetLineColor(1);
    histogram_vec[1+(pad_no-1)*4].SetLineColor(2);
    histogram_vec[2+(pad_no-1)*4].SetLineColor(4);
    histogram_vec[3+(pad_no-1)*4].SetLineColor(3);
    histogram_vec[3+(pad_no-1)*4].SetLineStyle(2);
    histogram_vec[0+(pad_no-1)*4].GetYaxis()->SetRangeUser(0,1.1*(histogram_vec[-1+pad_no*4].GetMaximum()));
    gPad->Modified(); gPad->Update();
    TLegend * current_legend = new TLegend(0.14,0.75,0.35,0.85,variable_strings[pad_no].c_str());
    current_legend->AddEntry(&histogram_vec[0+(pad_no-1)*4],"mixed");
    current_legend->AddEntry(&histogram_vec[1+(pad_no-1)*4],"sign");
    current_legend->AddEntry(&histogram_vec[2+(pad_no-1)*4],"bckg");
    current_legend->AddEntry(&histogram_vec[3+(pad_no-1)*4],"combined");
    current_legend->SetBorderSize(1);
    current_legend->SetTextFont(42);
    current_legend->SetFillStyle(0);
    current_legend->SetTextSize(0.02);
    current_legend->Draw();

  } 
  
  char file_str[30];
  sprintf(file_str,"check_%s.png",mode_string.c_str());
  var_canv.SaveAs(file_str); 


//  //TTree * sign_tree = (TTree*) mini_sign_file.Get("TreeD");
//
//  double sign_dphi, sign_qta, sign_DPhi, sign_DiMuonPt, sign_PhotonPt, sign_qtl;
//  double bckg_dphi, bckg_qta, bckg_DPhi, bckg_DiMuonPt, bckg_PhotonPt, bckg_qtl;
//  TH1F signalqta{"signqta","signqta",20,-5,15};
//  TH1F signalqtl{"signqtl","signqtl",20,-5,15};
//  TH1F backgroungqta{"backgroundqta","backgroundqta",20,-5,15};
//  TH1F backgroungqtl{"backgroundqtl","backgroundqtl",20,-5,15};
//
//  mini_sign_tree->SetBranchAddress("qtA",&sign_qta);
//  mini_sign_tree->SetBranchAddress("DPhi",&sign_DPhi);
//  mini_sign_tree->SetBranchAddress("PhotonPt",&sign_PhotonPt);
//  mini_sign_tree->SetBranchAddress("DiMuonPt",&sign_DiMuonPt);
//  ///sign_tree->SetBranchAddress("",&);
//  ///sign_tree->SetBranchAddress("",&);
//
//  for (int sign_entries = 0; sign_entries < sign_tree->GetEntries(); sign_entries++){
//    sign_tree->GetEntry(sign_entries);
//    signalqta.Fill(sign_qta);
//    signalqtl.Fill((sign_qta*(cos((M_PI-sign_DPhi)/2.0))));
//  }
//  //delete sign_tree;
//  //TTree * bckg_tree = (TTree*) mini_bckg_file.Get("TreeD");
//  //for (int bckg_entries = 0; bckg_entries < bckg_file.GetEntries(); bckg_entries++){
//  //  bckg_tree->GetEntry(bckg_entries);
//  //  backgroundqta.Fill(bckg_qta);
//  //  backgroundqtl.Fill((bckg_qta*(cos((M_PI-bckg_DPhi)/2.0))));
//  //}
//  TCanvas qtAL_canvas{"qtAL_Canvas","qta-qtl",600,600,1000,1000};
//  qtAL_canvas.Divide(2,1);
//  qtAL_canvas.cd(1);
//  signalqta.Draw();
//  qtAL_canvas.cd(2);
//  signalqtl.Draw();
//  qtAL_canvas.SaveAs("qtA-qtL.png");

}

int main( int argc, char * argv[]){
  std::cout << argv[0] << std::endl;
  check(atoi(argv[1]));
}
