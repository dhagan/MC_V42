
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>


void check(int window_mode = 0){
  
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  //std::vector<std::string> tmva_variables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta"};
  std::vector<std::string> variables{"qtA","BDT","absqtB"};
  std::vector<std::string> tree_variables{"qtA","BDT","abs(qtB)"};
  std::vector<std::string> file_types{"signal","data","pp","bb"};
  std::map<std::string,std::vector<double>> var_bins;
  var_bins["BDT"] = {10,-1.0,1.0};
  var_bins["qtA"] = {10,-5.0,15.0};
  var_bins["absqtB"] = {15,0,15.0};

  const char * mom_mode_string = (window_mode == 0) ? "qta" : "qtb";
  const char * mode_string = (window_mode == 0) ? "qtA" : "absqtB";
  const char * m_wind = (window_mode == 0) ? "qTAwide" :"qTBwide";
  std::vector<std::string> momentum_analysis_windows;
  int wind_bins = (int) var_bins[mode_string].at(0);
  for (int wind = 1; wind <= wind_bins; wind++){
    char window_char[30];
    sprintf(window_char,"%s-%i",m_wind,wind);
    momentum_analysis_windows.push_back(std::string(window_char));
  }

  for ( int file_index = 0; file_index < file_types.size(); file_index++){
    
    char sub_file_name[150];
    char tmva_file_name[150];
    sprintf(tmva_file_name,"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_%s/Evaluate_%s.root",file_types[file_index].c_str(),file_types[file_index].c_str());
    sprintf(sub_file_name,"/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/run/Subtracted_%s_%s_VARS-1.root",file_types[file_index].c_str(),mom_mode_string);


    TFile tmva_file{tmva_file_name,"READ"};
    TFile sub_file{sub_file_name,"READ"};

    TTree * tmva_tree = (TTree*) tmva_file.Get(Form("%s_00_mu4000_P5000",file_types[file_index].c_str()));

    std::vector<TH1F> tmva_hist_vector;
    std::vector<TH1F*> sub_hist_vector;
    std::vector<TH1F> integral_hist_vector;

    for (int variable_index = 0; variable_index<variables.size(); variable_index++){

      const char * variable = variables[variable_index].c_str();
      int bins = (int) var_bins[variable].at(0);
      int low_edge = var_bins[variable].at(1);
      int high_edge = var_bins[variable].at(2);
      
      char tmva_name[20],sub_name[20],integral_name[20],sub_hist_name[150];
      sprintf(tmva_name,"tmva_%i_%i",file_index,variable_index);
      sprintf(integral_name,"int_%i_%i",file_index,variable_index);
      sprintf(sub_hist_name,"%s_%s_99_npos_bothTheta-mass0_qtA-0_phiCS-0",variable,file_types[file_index].c_str());

      tmva_hist_vector.push_back(TH1F(tmva_name,variable,bins,low_edge,high_edge));
      const char * limits_string = (window_mode == 0) ? "qtA>-5&&qtA<15" : "abs(qtB)>0&&abs(qtB)<15";
      tmva_tree->Draw(Form("%s>>%s",tree_variables[variable_index].c_str(),tmva_name),limits_string,"goff");
      sub_hist_vector.push_back((TH1F*)sub_file.Get(sub_hist_name));
      if (variable_index == 0) {sub_hist_vector[variable_index]->Rebin();}

      TH1F integral_hist(integral_name,variable,bins,low_edge,high_edge);
      
      // integral reconstruction;
      int bin_number = 1;
      for (std::string current_window : momentum_analysis_windows){
                
        char integral_segment_name[100];
        sprintf(integral_segment_name,"BDT_%s_99_npos_bothTheta-mass0_%s_phiCS-0",file_types[file_index].c_str(),current_window.c_str());
        TH1F * integral_segment = (TH1F*) sub_file.Get(integral_segment_name);
        integral_hist.SetBinContent(bin_number,integral_segment->Integral());
        bin_number++;
      }

      integral_hist_vector.push_back(integral_hist);

    } 
    
    TCanvas canv{"canv","",200,100,2000,1000};
    canv.Divide(variables.size());
    for (int variable_index = 0; variable_index<variables.size(); variable_index++){
      const char * variable = variables[variable_index].c_str();
      canv.cd(variable_index+1);

      tmva_hist_vector[variable_index].Draw("HIST");
      if ((variable_index == 0 && window_mode == 0)|| (variable_index==2 && window_mode == 1) ){
        integral_hist_vector[variable_index].SetMarkerStyle(8);
        integral_hist_vector[variable_index].SetMarkerColor(1);
        integral_hist_vector[variable_index].SetLineColor(1);
        integral_hist_vector[variable_index].Draw("SAME P");
      }
      sub_hist_vector[variable_index]->Draw("SAME HIST");
      tmva_hist_vector[variable_index].SetLineColor(2);
      tmva_hist_vector[variable_index].SetLineStyle(1);
      sub_hist_vector[variable_index]->SetLineColor(4);
      sub_hist_vector[variable_index]->SetLineStyle(2);
      sub_hist_vector[variable_index]->GetYaxis()->SetRangeUser(0,tmva_hist_vector[variable_index].GetMaximum()*1.3);
      gPad->Modified(); gPad->Update();

      TPaveText * integral_text = new TPaveText(0.6,0.7,0.875,0.85,"nbNDC");
      integral_text->AddText(Form("input Integral - %i",(int) tmva_hist_vector[variable_index].Integral()));
      integral_text->AddText(Form("output Integral - %i",(int) sub_hist_vector[variable_index]->Integral()));
      if ((variable_index==0&&window_mode==0)||(variable_index==2&&window_mode==1)){integral_text->AddText(Form("recombined integral - %i",(int) integral_hist_vector[variable_index].Integral()));}
      integral_text->SetBorderSize(1);
      integral_text->SetFillStyle(0);
      integral_text->SetFillColor(0);
      integral_text->SetTextFont(42);
      integral_text->SetTextSize(0.02);
      integral_text->SetTextAlign(31);
      integral_text->Draw();
      TLegend * current_legend = new TLegend(0.15,0.75,0.35,0.85,variable);
      current_legend->AddEntry(&tmva_hist_vector[variable_index],"TMVA");
      current_legend->AddEntry(sub_hist_vector[variable_index],"Subtract");
      if ((variable_index==0&&window_mode==0)||(variable_index==2&&window_mode==1)){ current_legend->AddEntry(&integral_hist_vector[variable_index],"recombined");}
      current_legend->SetBorderSize(1);
      current_legend->SetTextFont(42);
      current_legend->SetFillStyle(0);
      current_legend->SetTextSize(0.02);
      current_legend->Draw(); 
    }

    canv.SaveAs(Form("./%s-windows_%s_sub_step_check.png",mom_mode_string,file_types[file_index].c_str()));

  }
}

int main(){
  check(0); 
  check(1); 
}
