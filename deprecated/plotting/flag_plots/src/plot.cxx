#include "plot.hxx"

void split_strings(std::vector<std::string> & vec_split, std::string str_split, std::string delim){
  std::istringstream str_stream{str_split};
  std::string token;
  while( getline( str_stream, token, *(delim.c_str()) ) ){
    vec_split.push_back(token);
  }
}


void plot_flags(std::string str_data_file, std::string str_sign_file, std::string str_bckg_file, std::string str_abin_vars, std::string str_spec_vars, std::string slice, std::string unique){

  SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);

  TFile * data_file = new TFile(str_data_file.c_str(),    "READ");
  TFile * sign_file = new TFile(str_sign_file.c_str(),    "READ");
  TFile * bckg_file = new TFile(str_bckg_file.c_str(),    "READ");

  std::cout << str_data_file << std::endl;
  std::cout << str_sign_file << std::endl;
  std::cout << str_bckg_file << std::endl;

  
  std::vector<std::string> vec_abin_vars, vec_spec_vars;
  split_strings(vec_abin_vars, str_abin_vars, ":");
  split_strings(vec_spec_vars, str_spec_vars, ":");

  std::map<std::string,int> analysis_bins;
  analysis_bins["qtA"]           =  10;
  analysis_bins["BDT"]           =  10;
  analysis_bins["abs(qtB)"]      =  15;
  analysis_bins["Phi"]           =  8;
  analysis_bins["DiMuonPt"]      =  15;
  analysis_bins["PhotonPt"]      =  10;

  std::vector<int> mass_indices = {0,3,4,5,12};

  std::vector<TH1F*> data_hist_vec;
  std::vector<TH1F*> sign_hist_vec;
  std::vector<TH1F*> bckg_hist_vec;
  std::vector<std::string> bin_name;
  std::vector<int> abin_number;

  // need to make this calc a little more clever
  int compound_index_max = mass_indices.size() * vec_abin_vars.size() * vec_spec_vars.size() * (analysis_bins[vec_abin_vars.at(0)]+1);
  std::cout << compound_index_max;
  
  std::vector<int>          mass_index_vec;
  std::vector<std::string>  abin_var_order_vec;
  std::vector<std::string>  spec_var_order_vec;

  for (int & mass_index : mass_indices){

    for (std::string abin_var : vec_abin_vars){

      for (std::string spec_var : vec_spec_vars){

        int abin_bins     = (int) analysis_bins[abin_var];

        for (int abin_index = 0; abin_index <= abin_bins; abin_index++){

          char data_hist_name[300], sign_hist_name[300],bckg_hist_name[300], mass_char[200];
          sprintf(mass_char,"both-theta_mass-%i",mass_index);
          sprintf(data_hist_name, "%s_data_%s_flag_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(sign_hist_name, "%s_sign_%s_flag_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);
          sprintf(bckg_hist_name, "%s_bckg_%s_flag_%s_%s-%i",  spec_var.c_str(),   slice.c_str(), mass_char,  abin_var.c_str(), abin_index);

          data_hist_vec.push_back( (TH1F*) data_file->Get(data_hist_name) );
          sign_hist_vec.push_back( (TH1F*) sign_file->Get(sign_hist_name) );
          bckg_hist_vec.push_back( (TH1F*) bckg_file->Get(bckg_hist_name) );
          bin_name.push_back(Form("%s alteration flags %s %s %s-%i",spec_var.c_str(), slice.c_str(), mass_char, abin_var.c_str(), abin_index)); 
          mass_index_vec.push_back(mass_index);
          abin_var_order_vec.push_back(abin_var);
          spec_var_order_vec.push_back(spec_var);
          abin_number.push_back(abin_index);
          
          
        }
      }
    }
  }

  std::cout << "hists loaded" << std::endl;
  
  for (int comp_index = 0; comp_index < compound_index_max; comp_index++){

    std::cout << "comp index" << std::endl;
    char canv_name[30];
    sprintf(canv_name,"canv%i",comp_index);

    TCanvas output_canvas(canv_name,canv_name,600,600,1000,1000);
    output_canvas.Divide(1,1);

    char mass_name[20];
    sprintf(mass_name,"mass%i",mass_index_vec[comp_index]);

    output_canvas.cd(1);
    data_hist_vec[comp_index]->Scale(1.3); 
    sign_hist_vec[comp_index]->Scale(1.2);
    bckg_hist_vec[comp_index]->Scale(1.1);
    data_hist_vec[comp_index]->Draw("HIST");
    sign_hist_vec[comp_index]->Draw("HIST SAME");
    bckg_hist_vec[comp_index]->Draw("HIST SAME");
    data_hist_vec[comp_index]->SetLineColor(1);
    sign_hist_vec[comp_index]->SetLineColor(2);
    bckg_hist_vec[comp_index]->SetLineColor(4);
    data_hist_vec[comp_index]->GetYaxis()->SetRangeUser(0,1.75);
    TLatex pad2_ltx;
    pad2_ltx.SetTextSize(0.03);
    pad2_ltx.DrawLatexNDC(0.2,0.97, bin_name[comp_index].c_str());

    TLegend pad1_lgnd(0.8, 0.8, 0.925,  0.925);
    pad1_lgnd.SetTextSize(0.025);
    pad1_lgnd.SetFillStyle(0);
    pad1_lgnd.SetTextFont(42);
    pad1_lgnd.AddEntry(data_hist_vec[comp_index], "Data Flags",    "lep");
    pad1_lgnd.AddEntry(sign_hist_vec[comp_index], "Sign Flags",    "l");
    pad1_lgnd.AddEntry(bckg_hist_vec[comp_index], "Bckg Flags",    "l  ");
    pad1_lgnd.SetBorderSize(0);
    pad1_lgnd.Draw();



    char output_canvas_name[150];
    sprintf(output_canvas_name,"./%s_%s_flags_mass%i_%s_%s-%i.png",spec_var_order_vec[comp_index].c_str(),slice.c_str(),mass_index_vec[comp_index],unique.c_str(),abin_var_order_vec[comp_index].c_str(),abin_number[comp_index]);
    output_canvas.SaveAs(output_canvas_name);
  }

}

int help(){
  std::cout << "idk dude it's so late right now" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"data",        required_argument,        0,      'd'},
      {"sign",        required_argument,        0,      's'},
      {"bckg",        required_argument,        0,      'b'},
      {"avar",        required_argument,        0,      'a'},
      {"svar",        required_argument,        0,      'v'},
      {"slice",       required_argument,        0,      't'},
      {"unique",      required_argument,        0,      'u'},
      {0,             0,                        0,      0}
    };

  std::string str_hstfc_file;
  std::string str_data_file, str_sign_file, str_bckg_file;
  std::string str_abin_vars, str_spec_vars;
  std::string str_slice, str_unique;

  do {
    option = getopt_long(argc, argv, "d:s:b:t:a:v:u:h", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'd': 
        str_data_file       = std::string(optarg);
        break;
      case 's': 
        str_sign_file       = std::string(optarg);
        break;
      case 'b': 
        str_bckg_file       = std::string(optarg);
        break;
      case 'a': 
        str_abin_vars       = std::string(optarg);
        break;
      case 'v': 
        str_spec_vars       = std::string(optarg);
        break;
      case 't':
        str_slice           = std::string(optarg);
        break;
      case 'u':
        str_unique          = std::string(optarg);
        break;
    }
  } while (option != -1);

  plot_flags(str_data_file, str_sign_file, str_bckg_file, str_abin_vars, str_spec_vars, str_slice, str_unique);
  return 0;

}



