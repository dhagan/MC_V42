#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "TCanvas.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TLorentzVector.h"
#include "RooCategory.h"
#include "Roo1DTable.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCBShape.h"
#include "RooAbsReal.h"
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooPolynomial.h"
#include "math.h"
#include "assert.h"
#include "TSystem.h"
#include "TLatex.h"

#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <math.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>
#include "TString.h"
#include <fstream>
#include <cassert>

#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "TFile.h"
#include "TROOT.h"

void crosscheck(){
  
  gROOT->SetBatch(true);
  gStyle->SetOptStat("ni");
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa"};
  std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA"};

  std::vector<std::string> selectSources = {"signal","data","pp","bb"};
  
  for ( int sourceInd = 0; sourceInd < selectSources.size(); sourceInd++){
    TCanvas * canv = new TCanvas("canv","",200,100,4000,4000);
    canv->Divide(3,3);
    TFile * cutFlowFile   = new TFile(Form("../CutFlow/run/lambdaqtA-445-86-qtbRange-qt2Lim/CutFlow_00/%s_PreSelectionCuts_00.root",selectSources[sourceInd].c_str()),"READ");
    TFile * TMVAInputFile  = new TFile(Form("../TMVATrees/run/%s_bound-0/%s_00_mu4000_P5000_bound-0.root",selectSources[sourceInd].c_str(),selectSources[sourceInd].c_str()),"READ");
    TTree * TMVATree = (TTree*) TMVAInputFile->Get("TreeD");
    std::vector<TH1F*> histVec;
    std::vector<TH1F*> histVecMaxs;
    for (int varInd = 0; varInd<CutFlowVariables.size(); varInd++){
      canv->cd(varInd+1);
      TH1F * cutFlowCurrent;
      if (sourceInd != 0){
        cutFlowCurrent = (TH1F*) cutFlowFile->Get(Form("%s_%s_cut12",CutFlowVariables[varInd].c_str(),selectSources[sourceInd].c_str()));
      } else {
        cutFlowCurrent = (TH1F*) cutFlowFile->Get(Form("%s_%s_cut13",CutFlowVariables[varInd].c_str(),selectSources[sourceInd].c_str()));
      }
      TH1F * TMVAInputCurrent = (TH1F*) cutFlowCurrent->Clone(Form("TMVACloned%i",varInd));
      TMVAInputCurrent->Reset();
      TMVATree->Draw(Form("%s>>TMVACloned%i",TMVAInputVariables[varInd].c_str(),varInd),"","goff");
      int axisMax = 1.1*max(TMVAInputCurrent->GetMaximum(),cutFlowCurrent->GetMaximum());
      TMVAInputCurrent->SetLineColor(2);
      TMVAInputCurrent->SetLineStyle(1);
      cutFlowCurrent->SetLineColor(4);
      cutFlowCurrent->SetLineStyle(2);
      TLegend * leg = new TLegend(0.15,0.75,0.35,0.85,CutFlowVariables[varInd].c_str());
      TMVAInputCurrent->Draw("HIST");
      cutFlowCurrent->Draw("HIST SAME");
      TMVAInputCurrent->GetYaxis()->SetRangeUser(0,axisMax);
      cutFlowCurrent->GetYaxis()->SetRangeUser(0,axisMax);
      TPaveText * integralText = new TPaveText(0.65,0.75,0.90,0.85,"nbNDC");
      integralText->AddText(Form("TMVA Integral - %i",(int) TMVAInputCurrent->Integral()));
      integralText->AddText(Form("CutF Integral - %i",(int) cutFlowCurrent->Integral()));
      integralText->SetBorderSize(1);
      integralText->SetFillStyle(0);
      integralText->SetFillColor(0);
      integralText->SetTextFont(42);
      integralText->SetTextSize(0.02);
      integralText->SetTextAlign(31);
      integralText->Draw();
      leg->AddEntry(TMVAInputCurrent,"TMVA");
      leg->AddEntry(cutFlowCurrent,"Cutflow");
      leg->SetBorderSize(1);
      leg->SetTextFont(42);
      leg->SetFillStyle(0);
      leg->SetTextSize(0.02);
      leg->Draw(); 
    } 
    canv->SaveAs(Form("./%s_CrossCheckVars.tiff",selectSources[sourceInd].c_str()));
    delete canv;
  }
}
