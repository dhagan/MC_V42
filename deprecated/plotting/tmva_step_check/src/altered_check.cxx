
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>


void check(int norm = 0){
  
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  std::vector<std::string> tmva_variables = {"qtA","qtB","qtL","qtM"};
  std::vector<double> low_edges{-7,-20,-7,-7};
  std::vector<double> high_edges{17, 20,17,17};

  std::vector<std::string> file_types = {"signal","data","pp"};


  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};

  std::map<std::string,TTree*>  tree_map;
  TFile   data_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_data/Evaluate_data.root","READ"};
  TTree * data_tree = (TTree*) data_file.Get("data_00_mu4000_P5000_bound-0");
  TFile   sign_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_signal/Evaluate_signal.root","READ"};
  TTree * sign_tree = (TTree*) sign_file.Get("signal_00_mu4000_P5000_bound-0");
  TFile   bckg_file{"/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_pp/Evaluate_pp.root","READ"};
  TTree * bckg_tree = (TTree*) bckg_file.Get("pp_00_mu4000_P5000_bound-0");

  for ( int mass_range = 0; mass_range<mass_cuts.size(); mass_range++){ 

    TCanvas canv{"canv","",200,100,2000,2000};
    canv.Divide(2,2);
    
    canv.cd(1);
    TH1F * data_hist_qta  = new TH1F("data_hist_qta","qta",50,-7,17);
    TH1F * sign_hist_qta  = new TH1F("sign_hist_qta","qta",50,-7,17);
    TH1F * bckg_hist_qta  = new TH1F("bckg_hist_qta","qta",50,-7,17);
    data_tree->Draw(Form("%s>>%s","qtA","data_hist_qta"),mass_ranges[mass_range].c_str(),"goff");
    sign_tree->Draw(Form("%s>>%s","qtA","sign_hist_qta"),mass_ranges[mass_range].c_str(),"goff");
    bckg_tree->Draw(Form("%s>>%s","qtA","bckg_hist_qta"),mass_ranges[mass_range].c_str(),"goff");
    if (norm != 0){
      data_hist_qta->Scale(1.0/data_hist_qta->Integral());
      sign_hist_qta->Scale(1.0/sign_hist_qta->Integral());
      bckg_hist_qta->Scale(1.0/bckg_hist_qta->Integral());
    }
    data_hist_qta->Draw("HIST");
    sign_hist_qta->Draw("SAME HIST");
    bckg_hist_qta->Draw("SAME HIST");
    data_hist_qta->GetYaxis()->SetRangeUser(0,1.5*data_hist_qta->GetMaximum());
    data_hist_qta->SetLineColor(1);
    sign_hist_qta->SetLineColor(2);
    bckg_hist_qta->SetLineColor(4);
    TLegend * qta_legend = new TLegend(0.15,0.75,0.35,0.85,Form("qta %s",mass_cuts[mass_range].c_str()));
    qta_legend->AddEntry(data_hist_qta,"Data");
    qta_legend->AddEntry(sign_hist_qta,"Signal");
    qta_legend->AddEntry(bckg_hist_qta,"Background");
    qta_legend->SetBorderSize(1);
    qta_legend->SetTextFont(42);
    qta_legend->SetFillStyle(0);
    qta_legend->SetTextSize(0.02);
    qta_legend->Draw(); 
    
    canv.cd(2);
    TH1F * data_hist_qtb  = new TH1F("data_hist_qtb","qtb",50,-20,20);
    TH1F * sign_hist_qtb  = new TH1F("sign_hist_qtb","qtb",50,-20,20);
    TH1F * bckg_hist_qtb  = new TH1F("bckg_hist_qtb","qtb",50,-20,20);
    data_tree->Draw(Form("%s>>%s","qtB","data_hist_qtb"),mass_ranges[mass_range].c_str(),"goff");
    sign_tree->Draw(Form("%s>>%s","qtB","sign_hist_qtb"),mass_ranges[mass_range].c_str(),"goff");
    bckg_tree->Draw(Form("%s>>%s","qtB","bckg_hist_qtb"),mass_ranges[mass_range].c_str(),"goff");
    if (norm != 0){
      data_hist_qtb->Scale(1.0/data_hist_qtb->Integral());
      sign_hist_qtb->Scale(1.0/sign_hist_qtb->Integral());
      bckg_hist_qtb->Scale(1.0/bckg_hist_qtb->Integral());
    }
    data_hist_qtb->Draw("HIST");
    sign_hist_qtb->Draw("SAME HIST");
    bckg_hist_qtb->Draw("SAME HIST");
    data_hist_qtb->GetYaxis()->SetRangeUser(0,1.5*data_hist_qtb->GetMaximum());
    data_hist_qtb->SetLineColor(1);
    sign_hist_qtb->SetLineColor(2);
    bckg_hist_qtb->SetLineColor(4);
    TLegend * qtb_legend = new TLegend(0.15,0.75,0.35,0.85,Form("qtb %s",mass_cuts[mass_range].c_str()));
    qtb_legend->AddEntry(data_hist_qtb,"Data");
    qtb_legend->AddEntry(sign_hist_qtb,"Signal");
    qtb_legend->AddEntry(bckg_hist_qtb,"Background");
    qtb_legend->SetBorderSize(1);
    qtb_legend->SetTextFont(42);
    qtb_legend->SetFillStyle(0);
    qtb_legend->SetTextSize(0.02);
    qtb_legend->Draw(); 

    canv.cd(3);
    TH1F * data_hist_qtl  = new TH1F("data_hist_qtl","qtl",50,-7,17);
    TH1F * sign_hist_qtl  = new TH1F("sign_hist_qtl","qtl",50,-7,17);
    TH1F * bckg_hist_qtl  = new TH1F("bckg_hist_qtl","qtl",50,-7,17);
    data_tree->Draw(Form("%s>>%s","qtL","data_hist_qtl"),mass_ranges[mass_range].c_str(),"goff");
    sign_tree->Draw(Form("%s>>%s","qtL","sign_hist_qtl"),mass_ranges[mass_range].c_str(),"goff");
    bckg_tree->Draw(Form("%s>>%s","qtL","bckg_hist_qtl"),mass_ranges[mass_range].c_str(),"goff");
    if (norm != 0){
      data_hist_qtl->Scale(1.0/data_hist_qtl->Integral());
      sign_hist_qtl->Scale(1.0/sign_hist_qtl->Integral());
      bckg_hist_qtl->Scale(1.0/bckg_hist_qtl->Integral());
    }
    data_hist_qtl->Draw("HIST");
    sign_hist_qtl->Draw("SAME HIST");
    bckg_hist_qtl->Draw("SAME HIST");
    data_hist_qtl->GetYaxis()->SetRangeUser(0,1.5*data_hist_qtl->GetMaximum());
    data_hist_qtl->SetLineColor(1);
    sign_hist_qtl->SetLineColor(2);
    bckg_hist_qtl->SetLineColor(4);
    TLegend * qtl_legend = new TLegend(0.15,0.75,0.35,0.85,Form("qtl %s",mass_cuts[mass_range].c_str()));
    qtl_legend->AddEntry(data_hist_qtl,"Data");
    qtl_legend->AddEntry(sign_hist_qtl,"Signal");
    qtl_legend->AddEntry(bckg_hist_qtl,"Background");
    qtl_legend->SetBorderSize(1);
    qtl_legend->SetTextFont(42);
    qtl_legend->SetFillStyle(0);
    qtl_legend->SetTextSize(0.02);
    qtl_legend->Draw(); 

    canv.cd(4);
    TH1F * data_hist_qtm  = new TH1F("data_hist_qtm","qtm",50,-7,17);
    TH1F * sign_hist_qtm  = new TH1F("sign_hist_qtm","qtm",50,-7,17);
    TH1F * bckg_hist_qtm  = new TH1F("bckg_hist_qtm","qtm",50,-7,17);
    data_tree->Draw(Form("%s>>%s","qtM","data_hist_qtm"),mass_ranges[mass_range].c_str(),"goff");
    sign_tree->Draw(Form("%s>>%s","qtM","sign_hist_qtm"),mass_ranges[mass_range].c_str(),"goff");
    bckg_tree->Draw(Form("%s>>%s","qtM","bckg_hist_qtm"),mass_ranges[mass_range].c_str(),"goff");
    if (norm != 0){
      data_hist_qtm->Scale(1.0/data_hist_qtm->Integral());
      sign_hist_qtm->Scale(1.0/sign_hist_qtm->Integral());
      bckg_hist_qtm->Scale(1.0/bckg_hist_qtm->Integral());
    }
    data_hist_qtm->Draw("HIST");
    sign_hist_qtm->Draw("SAME HIST");
    bckg_hist_qtm->Draw("SAME HIST");
    data_hist_qtm->GetYaxis()->SetRangeUser(0,1.5*data_hist_qtm->GetMaximum());
    data_hist_qtm->SetLineColor(1);
    sign_hist_qtm->SetLineColor(2);
    bckg_hist_qtm->SetLineColor(4);
    TLegend * qtm_legend = new TLegend(0.15,0.75,0.35,0.85,Form("qtm %s",mass_cuts[mass_range].c_str()));
    qtm_legend->AddEntry(data_hist_qtm,"Data");
    qtm_legend->AddEntry(sign_hist_qtm,"Signal");
    qtm_legend->AddEntry(bckg_hist_qtm,"Background");
    qtm_legend->SetBorderSize(1);
    qtm_legend->SetTextFont(42);
    qtm_legend->SetFillStyle(0);
    qtm_legend->SetTextSize(0.02);
    qtm_legend->Draw(); 

    if (norm == 0){
      canv.SaveAs(Form("./qtAlterations_%s_check.png",mass_cuts[mass_range].c_str()));
    } else {
      canv.SaveAs(Form("./qtAlterations_normalised_%s_check.png",mass_cuts[mass_range].c_str()));
    }

     delete data_hist_qta;
     delete sign_hist_qta;
     delete bckg_hist_qta;
     delete data_hist_qtb;
     delete sign_hist_qtb;
     delete bckg_hist_qtb;
     delete data_hist_qtl;
     delete sign_hist_qtl;
     delete bckg_hist_qtl;
     delete data_hist_qtm;
     delete sign_hist_qtm;
     delete bckg_hist_qtm;
;
  }
}

int main(int argc, char * argv[]){
  if (argc == 1){
    check(); 
  } else if (argc == 2){
    check(atoi(argv[1])); 
  }
}
