
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cassert>

#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TPaveText.h>


void check(){
  
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  //std::vector<std::string> CutFlowVariables   = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","Phi","PhotonPt","qTSquared","qTa","CosTheta","DiMuonEta","DiMuonY","Lambda","MuMinusPt","MuPlusPt","PhotonEta"};
  //std::vector<std::string> TMVAInputVariables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB"};
  
  std::vector<std::string> tmva_variables = {"DPhi","DY","DiMuonMass","DiMuonPt","DiMuonTau","abs(Phi)","PhotonPt","qTSquared","qtA","qtB","Lambda","AbsCosTheta","qtL","qtM"};
  std::vector<double> low_edges{-M_PI*1.1,-5,2600, 5000,-5,       0, 4000,  0,-7,-20,  0,  0, -5,-5};
  std::vector<double> high_edges{M_PI*1.1, 5,3600,30000, 5,M_PI*1.1,30000,440,17, 20,220,1.1, 15,15};

  std::vector<std::string> file_types = {"signal","data","pp","bb"};


  std::vector<std::string> mass_cuts    = {"mass0","mass3","mass4","mass5","mass12"};
  std::vector<std::string> mass_ranges  = {"Lambda>0&&Lambda<200","Lambda>25&&Lambda<50","Lambda>50&&Lambda<100","Lambda>100&&Lambda<200","Lambda>25&&Lambda<200"};



  for ( int mass_range = 0; mass_range<mass_cuts.size(); mass_range++){ 

    for ( int file_index = 0; file_index < file_types.size(); file_index++){

      
      const char * file_type_str = file_types[file_index].c_str();
      TFile tmva_output_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run/Eval_%s/Evaluate_%s.root",file_type_str,file_type_str),"READ"};
      TFile tmva_input_file{Form("/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/%s_bound-0/%s_00_mu4000_P5000_bound-0.root",file_type_str,file_type_str),"READ"};

      TTree * tmva_input_tree =  (TTree*) tmva_input_file.Get("TreeD");
      TTree * tmva_output_tree = (TTree*) tmva_output_file.Get(Form("%s_00_mu4000_P5000_bound-0",file_type_str));

      std::vector<TH1F> input_hist_vector;
      std::vector<TH1F> output_hist_vector;

      for (int variable_index = 0; variable_index<tmva_variables.size(); variable_index++){

        const char * variable = tmva_variables[variable_index].c_str();

        char input_name[20],output_name[20],temp_name[20],hist_title[100];
        sprintf(input_name,"input_%i_%i",file_index,variable_index);
        sprintf(output_name,"output_%i_%i",file_index,variable_index);
        sprintf(hist_title,"%s %s",variable,mass_cuts[mass_range].c_str());

        input_hist_vector.push_back(TH1F(input_name,hist_title,50,low_edges[variable_index],high_edges[variable_index]));
        output_hist_vector.push_back(TH1F(output_name,hist_title,50,low_edges[variable_index],high_edges[variable_index]));

        tmva_input_tree->Draw(Form("%s>>%s",variable,input_name),mass_ranges[mass_range].c_str(),"goff");
        tmva_output_tree->Draw(Form("%s>>%s",variable,output_name),mass_ranges[mass_range].c_str(),"goff");

      } 
      
      TCanvas canv{"canv","",200,100,4000,3000};
      canv.Divide(4,4);

      std::vector<TPaveText> text_vector;
      std::vector<TLegend> legend_vector;

      for (int variable_index = 0; variable_index<tmva_variables.size(); variable_index++){
        
        const char * variable = tmva_variables[variable_index].c_str();

        canv.cd(variable_index+1);

        input_hist_vector[variable_index].Draw("HIST");
        output_hist_vector[variable_index].Draw("HIST SAME");
        input_hist_vector[variable_index].GetYaxis()->SetRangeUser(0,1.1*std::max(input_hist_vector[variable_index].GetMaximum(),output_hist_vector[variable_index].GetMaximum()));

        input_hist_vector[variable_index].SetLineColor(2);
        input_hist_vector[variable_index].SetLineStyle(1);
        output_hist_vector[variable_index].SetLineColor(4);
        output_hist_vector[variable_index].SetLineStyle(2);

        gPad->Modified(); gPad->Update();
        TPaveText * integral_text = new TPaveText(0.65,0.75,0.90,0.85,"nbNDC");
        integral_text->AddText(Form("input Integral - %i",(int) input_hist_vector[variable_index].Integral()));
        integral_text->AddText(Form("output Integral - %i",(int) output_hist_vector[variable_index].Integral()));
        integral_text->SetBorderSize(1);
        integral_text->SetFillStyle(0);
        integral_text->SetFillColor(0);
        integral_text->SetTextFont(42);
        integral_text->SetTextSize(0.02);
        integral_text->SetTextAlign(31);
        integral_text->Draw();
        //text_vector.push_back(integral_text);
        
        TLegend * current_legend = new TLegend(0.15,0.75,0.35,0.85,variable);
        current_legend->AddEntry(&input_hist_vector[variable_index],"TMVA Input");
        current_legend->AddEntry(&output_hist_vector[variable_index],"TMVA Output");
        current_legend->SetBorderSize(1);
        current_legend->SetTextFont(42);
        current_legend->SetFillStyle(0);
        current_legend->SetTextSize(0.02);
        current_legend->Draw(); 
        //legend_vector.push_back(current_legend);

      }
      canv.SaveAs(Form("./%s_%s_CrossCheckVars.png",file_type_str,mass_cuts[mass_range].c_str()));
    }
  }
}

int main(){
  check(); 
}
