#!/bin/bash

help_func()
{
   echo ""
   echo "Usage: $0 -i"
   echo -i "\tInitial execution, ensures reconfigure"
   return # Exit script after printing help
}

initial_flag=
OPTIND=1
while getopts "i" opt
do
   case "$opt" in
      i ) initial_flag=1 ;;
      ? ) help_func ;; # Print helpFunction in case parameter is non-existent
   esac
done

## Title
cat ./assets/banner
echo ""

## Initialise the submodule
git submodule update --init --recursive

## Check root and cmake are here
if ! command -v root &> /dev/null
then
    echo "No root install, exiting."
    return
fi
echo -e "$(root --version)"

if ! command -v cmake &> /dev/null
then
    echo "No cmake install, exiting."
    return
fi
echo -e $(cmake --version)
echo ""



## Prepare environment variables for cmake and compilation
analysis_install_path=$(pwd)
analysis_storage_path="/run/media/crucible/Expansion/analysis/"
export IN_PATH="$analysis_storage_path/in"
export OUT_PATH="$analysis_storage_path/out/run"
export LOG_PATH="$analysis_storage_path/out/log"
export ANA_IP="$analysis_install_path"
export LIB_PATH="$analysis_install_path/lib/analysis_utils"
export LIB_INCLUDE="$analysis_install_path/lib/analysis_utils/inc"
export LIB_BUILD="$analysis_install_path/lib/build/libanalysisutil.so"
export DEFAULT_STYLE_JSON="$analysis_install_path/lib/analysis_utils/share/default_style"
export DEFAULT_FIT_JSON="$analysis_install_path/lib/analysis_utils/share/default_fits"
export DEFAULT_SCRIPT_JSON="$analysis_install_path/lib/analysis_utils/share/default_scripts"
export SF_PATH="${IN_PATH}/sf_map/"
mkdir -p ${OUT_PATH}
mkdir -p ${LOG_PATH}

## Compile the library
echo "Building library..."
mkdir -p ./lib/build
pushd ./lib/build >> /dev/null
if [ ! -z "$initial_flag" ]; then
	cmake -S ../analysis_utils -B . -G Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS=1
fi
ninja
cd ..
ln -sf ./build/compile_commands.json
popd >> /dev/null
echo ""

## Compile main components of the analysis chain
echo "\nCompiling process steps:"
main_processes=( trees bdt nn event_subtraction event_hf mix_samples scripts systematics )
for process in "${main_processes[@]}"
do
	echo "Building ${process}..."
	mkdir -p ./${process}/build
	pushd ./${process}/build >> /dev/null
	if [ ! -z "$initial_flag" ]; then
		cmake -S ../source -B . -G Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS=1
	fi
	ninja
	cd ..
	rm -rf ./.cache
	ln -sf ./build/compile_commands.json
	popd >> /dev/null
	mkdir -p ${OUT_PATH}/${process}
	mkdir -p ${LOG_PATH}/${process}
done

## Compile plots, etc
echo "\nCompiling plotting steps:"
studies=( migration tree_check extract single_systematic exp_bg_fit sf_conversion apply_eff event_extract
	  statistics_test stages )
pushd ./plotting >> /dev/null
for plot in "${studies[@]}"
do
	echo "Building ${plot}..."
	mkdir -p ./${plot}/build
	pushd ./${plot}/build >> /dev/null
	if [ ! -z "$initial_flag" ]; then
		cmake -S ../source -B . -G Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS=1
	fi
	ninja
	cd ..
	rm -rf ./.cache
	ln -sf ./build/compile_commands.json
	popd >> /dev/null
	mkdir -p ${OUT_PATH}/${plot}
	mkdir -p ${LOG_PATH}/${plot}
done
popd >> /dev/null
