## Mass side-band and long lifetime subtraction.

## Description
This stage of the analysis takes files output from either the boosted decision tree, or the tree creation step and performs a subtraction of the j/psi mass sidebands and long lifetime events with respect to a certain specified variable. The baseline analysis chain uses the BDT score as the variable in which to perform the subtraction. The subtraction works as follows, the BDT score distribution is split into a number of bins with respect to an analysis variable, typically $`q_{T}^{A}`$, $`q_{T}^{B}`$ or $`\phi`$. In each of these $`N_{ana}`$ bins, 8 divisions are made, 4 slices across the $`J/\psi`$ mass spectrum are made and 2 across the lifetime distribution;
 Lifetime divisions            | Mass divisions                
:------------------------------:|:------------------------------:
 ![tau]( https://gitlab.cern.ch/dhagan/MC_V42/blob/local/assets/lls.png?raw=true)   | ![mass]( https://gitlab.cern.ch/dhagan/MC_V42/-/tree/local//assets/msb.png  ) 

The histograms produced by the combination of these subdivisions, indexed $N_{Mass},N_{\tau}$, are then combined with the following expression; $$` ( 21 + 31 - 41 - 11 ) - ( 22 + 32 - 42 - 12 ) `$$ This produces the distribution of score in the respective analysis bin with the long lifetime and mass sideband contributions subtracted. This should produce flat distributions with no integral for the bbbg sample. Following the division into multiple separate bins, an 'alteration' step is performed to zero out negative bins in the distribution in preparation for the next step. This is done as the upcoming fit procedure becomes numerically unstable when provided with negative entries.

## Usage
Scripts for baseline and numerous systematic variations have been written, these == runs scripts are stored in the ./share directory ==. These provide proper output structure and output organisation.

```
Usage:
./subtract --mode,-m MODE --input,-i INPUT_PATH --analysis,-a ANALYSIS_VAR --spectator,-s SPECTATOR_VAR \
           --selections,-v SELECTION_PATH --unique,-u UNIQUE [ --type,-t TYPE ] [ --ranges,-r RANGE_STRING ] \
           [ --cuts,-c EXTRA_CUT ] [ --help,-h ]

Required arguments;
  --mode,-m             Program execution mode. Subtract "sub" or alter "pos". "zero" mode is also
                        available for outputting files containing no score.
  --input,-i            Input file path, in "pos" mode, supply data, sign, and bckg paths in colon
                        separated list
  --analysis,-a         Analysis variable, baseline uses qta. This is the variable in which the 
                        spectator distribution is split by.
  --spectator,-s        Distribution which in which the mass sideband and long lifetime subtraction
                        is performed in.
  --selections,-v       A filepath to a selections file. This holds the information of all the variables binning and 
                        ranges, along with the bounds and binning of all variables 
  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is 
                        necessary for bookkeeping.

Optional arguments;
  --ranges, -r          A string that will be processed into a bound for a variable, can be used to replace or augment
                        any variable definition already supplied by the selections file as a method of smaller and 
                        faster systematic variations.
  --cuts,-c             Extra cut to be used during the subtraction.
  --help,-h             This message.
      
```
