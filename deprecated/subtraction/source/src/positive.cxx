#include <positive.hxx>

void alteration( std::string & input_path, std::string & abin_var, std::string & spec_var, 
                 std::string & selections, std::string & ranges, 
                 std::vector< std::string> & mass_vars ){

  std::vector< std::string > input_files;
  split_strings( input_files, input_path, ":" );

  TFile * data_file = new TFile( input_files.at(0).c_str(), "UPDATE" );
  TFile * sign_file = new TFile( input_files.at(1).c_str(), "UPDATE" );
  TFile * bckg_file = new TFile( input_files.at(2).c_str(), "UPDATE" );

  bound_mgr * analysis = new bound_mgr();
  bound_mgr * spectators = new bound_mgr();


  std::vector<std::string> selection_files;
  split_strings( selection_files, selections, ":" );
  if ( selection_files.size() <= 1 ){
    std::cout << "Insufficient selections supplied" << std::endl;
    std::cout << "Try finding sub_analysis_bounds.txt path or sub_spectator_bounds.txt path" << std::endl;
  }
  analysis->load_bound_mgr( selection_files.at(0) );
  spectators->load_bound_mgr( selection_files.at(1) );
  analysis->process_bounds_string( ranges );


  bound analysis_bound = analysis->get_bound( abin_var );
  bound spectator_bound = spectators->get_bound( spec_var );
  int bin_count = spectator_bound.get_bins();
  
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( 0,  true );

  std::vector< std::string > analysis_bins;
  for ( int idx = 0; idx < (int) ana_cut_names.size(); idx++ ){
    for ( std::string & mass_bin : mass_vars ){
      std::string name = mass_bin + "_" + ana_cut_names[idx];
      analysis_bins.push_back( name );
    }
  }

  std::vector<std::string> slice_strings{"99","00"};
  
  std::vector< TH1D * > data_save_hist;
  std::vector< TH1D * > sign_save_hist;
  std::vector< TH1D * > bckg_save_hist;

  for ( std::string & ana_bin : analysis_bins  ){ 

    std::cout << ana_bin << std::endl;
    for ( std::string & slice : slice_strings ){
  
      std::string input_name    = Form( "%s_%s_npos_%s", spec_var.c_str(), slice.c_str(), ana_bin.c_str() );
      const char * data_name    = "data_npos";
      const char * sign_name    = "sign_npos";
      const char * bckg_name    = "bckg_npos";
      
      const char * data_pos_name = Form( "%s_%s_pos_%s", spec_var.c_str() ,slice.c_str(), ana_bin.c_str());
      const char * sign_pos_name = Form( "%s_%s_pos_%s", spec_var.c_str() ,slice.c_str(), ana_bin.c_str());
      const char * bckg_pos_name = Form( "%s_%s_pos_%s", spec_var.c_str() ,slice.c_str(), ana_bin.c_str());
        
      const char * data_flag_name = Form( "%s_data_%s_flag_%s", spec_var.c_str(), slice.c_str(), ana_bin.c_str());
      const char * sign_flag_name = Form( "%s_sign_%s_flag_%s", spec_var.c_str(), slice.c_str(), ana_bin.c_str());
      const char * bckg_flag_name = Form( "%s_bckg_%s_flag_%s", spec_var.c_str(), slice.c_str(), ana_bin.c_str());

      TH1D * temp_hist;
      temp_hist = (TH1D*) data_file->Get( input_name.c_str() );
      TH1D * data_hist = (TH1D*) temp_hist->Clone( data_name );
      temp_hist = (TH1D*) sign_file->Get( input_name.c_str() );
      TH1D * sign_hist = (TH1D*) temp_hist->Clone( sign_name );
      temp_hist = (TH1D*) bckg_file->Get( input_name.c_str() );
      TH1D * bckg_hist = (TH1D*) temp_hist->Clone( bckg_name );

      TH1D * data_hist_pos = (TH1D*) data_hist->Clone( data_pos_name );
      TH1D * sign_hist_pos = (TH1D*) sign_hist->Clone( sign_pos_name );
      TH1D * bckg_hist_pos = (TH1D*) bckg_hist->Clone( bckg_pos_name );
      
      TH1D * data_flags = new TH1D( data_flag_name, data_flag_name, bin_count, 0, bin_count ); 
      TH1D * sign_flags = new TH1D( sign_flag_name, sign_flag_name, bin_count, 0, bin_count );
      TH1D * bckg_flags = new TH1D( bckg_flag_name, bckg_flag_name, bin_count, 0, bin_count );
      
      for (int bin_number = 1; bin_number <= data_hist_pos->GetNbinsX(); bin_number++){
        if (data_hist_pos->GetBinContent(bin_number) < 0){
          data_hist_pos->SetBinContent(bin_number,0);
          data_hist_pos->SetBinError(bin_number,0);
          data_flags->SetBinContent(bin_number,1);
          std::cout << "data flag" << std::endl;
        }
        if (sign_hist_pos->GetBinContent(bin_number) < 0){
          sign_hist_pos->SetBinContent(bin_number,0);
          sign_hist_pos->SetBinError(bin_number,0);
          sign_flags->SetBinContent(bin_number,1);
          std::cout << "sign flag" << std::endl;
        }
        if (bckg_hist_pos->GetBinContent(bin_number) < 0){
          bckg_hist_pos->SetBinContent(bin_number,0);
          bckg_hist_pos->SetBinError(bin_number,0);
          bckg_flags->SetBinContent(bin_number,1);
          std::cout << "bckg flag" << std::endl;
        }
        if ((bckg_hist_pos->GetBinContent(bin_number) <= 0) && (sign_hist_pos->GetBinContent(bin_number) <= 0)){
          data_hist_pos->SetBinContent(bin_number,0);
          data_hist_pos->SetBinError(bin_number,0);
          data_flags->SetBinContent(bin_number,1);
        }
      }
        
      data_save_hist.push_back( data_hist_pos );
      data_save_hist.push_back( data_flags );
      sign_save_hist.push_back( sign_hist_pos );
      sign_save_hist.push_back( sign_flags );
      bckg_save_hist.push_back( bckg_hist_pos );
      bckg_save_hist.push_back( bckg_flags );

    }
  }

  data_file->cd();
  for ( TH1D * data_hist : data_save_hist ){
    data_hist->Write();
  } 
  data_file->Close();

  sign_file->cd();
  for ( TH1D * sign_hist : sign_save_hist ){
    sign_hist->Write();
  } 
  sign_file->Close();

  bckg_file->cd();
  for ( TH1D * bckg_hist : bckg_save_hist ){
    bckg_hist->Write();
  } 
  bckg_file->Close();

  delete data_file;
  delete sign_file;
  delete bckg_file;

}
