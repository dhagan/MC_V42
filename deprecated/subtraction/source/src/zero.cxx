#include <zero.hxx>

void zero( std::string & type, std::string & abin_var, std::string & spec_var, 
           std::string & selections, std::string & ranges, std::string & unique ){


  std::vector<std::string> selection_files;
  split_strings( selection_files, selections, ":" );
  
  bound_mgr * analysis = new bound_mgr();
  bound_mgr * spectators = new bound_mgr();
  analysis->load_bound_mgr( selection_files.at(0) );
  spectators->load_bound_mgr( selection_files.at(1) );
  if ( !ranges.empty() ){
    analysis->process_bounds_string( ranges );
  }

  bound analysis_bound = analysis->get_bound( abin_var );
  bound spectator_bound = spectators->get_bound( spec_var );
  int spec_bins = spectator_bound.get_bins();
  double spec_min = spectator_bound.get_min();
  double spec_max = spectator_bound.get_max();


  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( 0,  true );



  std::vector< std::string > mass_vars = { "Q0", "Q3", "Q4", "Q5", "Q12",
                                           "T0", "T3", "T4", "T5", "T12" };
  
  std::string output_str = Form( "./subtracted_%s_%s_A-%s_S-%s.root", unique.c_str(), type.c_str(), 
                                 abin_var.c_str(), spec_var.c_str() );
  TFile * zero_file = new TFile( output_str.c_str(), "RECREATE" );
  zero_file->cd();

  for ( const std::string & mass : mass_vars ){
    for ( const std::string & ana_str : ana_cut_names ){

      std::string hist_name = spec_var + "_99_pos_" + mass + "_" + ana_str;
      TH1D * zero_hist = new TH1D( hist_name.c_str(), hist_name.c_str(), spec_bins, spec_min, spec_max );
      for ( int idx = 0; idx <= spec_bins; idx++ ){ 
        zero_hist->SetBinContent( idx, 0 );
        zero_hist->SetBinError( idx, 0 );
      }
      zero_hist->Write();
    }
  }

  zero_file->Close();
}
