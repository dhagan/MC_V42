#include <subtract.hxx>
#include <form_slices.hxx>

#include <ROOT/RDataFrame.hxx>

void subtraction( std::string & file_str, std::string & type, std::string & abin_var, 
                  std::string & spec_var, std::string & extra_var, std::string & selections,
                  std::string & ranges, std::vector< std::string >& mass_vars,
                  std::string & sub_consts, std::string & cf_path, std::string & y_values,
                  std::string & unique ){

  ROOT::EnableImplicitMT( );

  if ( extra_var.empty() ){ std::cout << extra_var << std::endl; }

  std::vector<std::string> file_vec, selection_files;
  split_strings( file_vec, file_str, ":" );
  split_strings( selection_files, selections, ":" );

  // rdataframes are not faster here.
  // combination of using doubles and the histogram construction
  // could probably be fixed with good reference management
  TFile * input_file = new TFile( file_vec.at(0).c_str(), "READ" );
  TTree * input_tree = (TTree *) input_file->Get( file_vec.at(1).c_str() );


  if ( selection_files.size() <= 1 ){
    std::cout << "Insufficient selections supplied" << std::endl;
    std::cout << "Try finding sub_analysis_bounds.txt path or sub_spectator_bounds.txt path" << std::endl;
  }

  bound_mgr * analysis = new bound_mgr();
  bound_mgr * spectators = new bound_mgr();
  analysis->load_bound_mgr( selection_files.at(0) );
  spectators->load_bound_mgr( selection_files.at(1) );
  if ( !ranges.empty() ){
    analysis->process_bounds_string( ranges );
  }

  bound analysis_bound = analysis->get_bound( abin_var );
  bound spectator_bound = spectators->get_bound( spec_var );
  int spec_bins = spectator_bound.get_bins();
  double spec_min = spectator_bound.get_min();
  double spec_max = spectator_bound.get_max();


  
  
  bool y_mode = !y_values.empty();
  std::vector< double > y_dbls;
  if ( y_mode ){
    std::vector< std::string > y_strs;        
    split_strings( y_strs, y_values, ":" );   
    if ( y_strs.size() != (size_t) analysis_bound.get_bins() ){
      std::cout << "y value amount mismatch" << std::endl;
    }
    y_dbls.reserve( y_strs.size() );
    std::transform( y_strs.begin(), y_strs.end(), std::back_inserter( y_dbls ), []( std::string y ){ return std::stod(y); } );
    //std::for_each( y_dbls.begin(), y_dbls.end(), []( double y ){ std::cout << y << std::endl; } );
  }

  std::vector<double> sub_const = { 1.0, 1.0, 1.0, 1.0, 1.0, -1.0 };
  std::vector< std::string > sub_const_strs;
  if ( !sub_consts.empty() ){
    split_strings( sub_const_strs, sub_consts, ":" );
    sub_const = { std::stod( sub_const_strs.at(0) ), std::stod( sub_const_strs.at(1) ),
                  std::stod( sub_const_strs.at(2) ), std::stod( sub_const_strs.at(3) ) };

  }


  cutflow * cf_sub = new cutflow();
  if ( !cf_path.empty() ){
    std::vector< std::string > sub_cuts = { "mass_subtraction", "all_subtraction" };
    std::vector< std::string > cf_vars;
    split_strings( cf_vars, cf_path, ":" );
    std::string cf_unique = Form(  "%s_%s", type.c_str(), cf_vars.at(1).c_str() );
    cf_sub->load_cutflow( cf_vars.at( 0 ), cf_unique ); 
    cf_sub->expand_cutflow( sub_cuts );
  }


  std::unordered_map< int, std::string > slice_map;
  form_slices( analysis, &slice_map );
  std::string zero_cut = "DiMuonMass>=2700&&DiMuonMass<=3500&&DiMuonTau>=-5.0&&DiMuonTau<=15.0";
  slice_map.insert( std::pair< int, std::string>{ 00, zero_cut } );

  std::vector< int > slice_strings = { 11, 21, 31, 41, 12, 22, 32, 42,
                                       71, 81, 72, 82, 91, 92, 99, 0,
                                       73, 83, 93 };
  std::vector< std::string > ana_cut_series = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( 0,  true );



  // form analysis_cuts
  std::map< std::string, std::string > analysis_cuts;
  std::map< std::string, double > y_map;
  for ( std::string & mass_bin : mass_vars ){
    bound mass_bound = analysis->get_bound( mass_bin );
    std::string mass_cut = mass_bound.get_cut();
    for ( int idx = 0; idx < (int) ana_cut_series.size(); idx++ ){
      std::string name = mass_bin + "_" + ana_cut_names[idx];
      analysis_cuts[ name ] = std::string( mass_cut + "&&" + ana_cut_series[idx] );
      if ( y_mode ){ y_map[ name ] = y_dbls[ idx ]; }
    }
    analysis_cuts[ Form( "%s-0", analysis_bound.get_var().c_str() ) ] = ( analysis_bound.get_cut() + "&&" + mass_cut );
    if ( y_mode ){ y_map[ Form( "%s-0", analysis_bound.get_var().c_str() ) ] = 1.0; }
  }

  std::map< std::string, TH1F * > store_hists;
  std::unordered_map< std::string, TH1F * > hists;
  hists.reserve( (slice_strings.size() + 1)*(analysis_cuts.size() + 1) );

  std::map< std::string, std::string >::iterator analysis_itr;
  for ( analysis_itr = analysis_cuts.begin(); analysis_itr != analysis_cuts.end(); analysis_itr++ ){ 

    std::string ana_cut_name = analysis_itr->first;
    std::string ana_cut = analysis_itr->second;
    
    for ( int & slice : slice_strings ){

      const char * hist_name = Form( "%s_%i_%s", spec_var.c_str(), slice, ana_cut_name.c_str());
      hists[hist_name] = new TH1F( hist_name, hist_name, spec_bins, spec_min, spec_max );
      hists[hist_name]->Sumw2( kTRUE );
      if ( slice_map.find( slice ) != slice_map.end() ){ 
        std::string full_cut =  "(" + slice_map.at( slice ) + ")&&(" + ana_cut + ")";
        input_tree->Draw( Form( "%s>>%s", spectator_bound.get_var().c_str(), hist_name ), full_cut.c_str(), "goff" );
      }


    }

    std::cout << ana_cut_name << std::endl;

    if ( y_mode ){
      sub_const[0] = y_map[ ana_cut_name ];
      sub_const[3] = 1.0/y_map[ ana_cut_name ];
    }


    TH1F * h11 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 11, ana_cut_name.c_str() )); 
    TH1F * h21 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 21, ana_cut_name.c_str() )); 
    TH1F * h31 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 31, ana_cut_name.c_str() ));
    TH1F * h41 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 41, ana_cut_name.c_str() ));
    TH1F * h12 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 12, ana_cut_name.c_str() ));
    TH1F * h22 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 22, ana_cut_name.c_str() ));
    TH1F * h32 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 32, ana_cut_name.c_str() ));
    TH1F * h42 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 42, ana_cut_name.c_str() ));
    TH1F * h71 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 71, ana_cut_name.c_str() ));
    TH1F * h72 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 72, ana_cut_name.c_str() ));
    TH1F * h81 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 81, ana_cut_name.c_str() ));
    TH1F * h82 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 82, ana_cut_name.c_str() ));

    h71->Add( h21, h31, sub_const.at(1), sub_const.at(2) ); 
    h72->Add( h22, h32, sub_const.at(1), sub_const.at(2) ); 
    h81->Add( h41, h11, sub_const.at(3), sub_const.at(0) );
    h82->Add( h42, h12, sub_const.at(3), sub_const.at(0) );

    TH1F * h91 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 91, ana_cut_name.c_str() ) );
    TH1F * h92 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 92, ana_cut_name.c_str() ) );
    h91->Add( h71, h81, sub_const.at(4), sub_const.at(5) );
    h92->Add( h72, h82, sub_const.at(4), sub_const.at(5) );

    TH1F * h99 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 99, ana_cut_name.c_str() ) );
    h99->Add( h91, h92, 1.0, -1.0 );

    TH1F * h73 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 73, ana_cut_name.c_str() ));
    h73->Add( h71, h72, 1.0, 1.0 );
    TH1F * h83 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 83, ana_cut_name.c_str() ));
    h83->Add( h81, h82, 1.0, 1.0 );
    TH1F * h93 = hists.at( Form( "%s_%i_%s", spec_var.c_str(), 93, ana_cut_name.c_str() ));
    h93->Add( h73, h83, 1.0, -1.0 );

    // std::move might help
    std::string h93_str{ Form( "%s_93_npos_%s", spec_var.c_str(), ana_cut_name.c_str() ) };
    store_hists[h93_str.c_str()] = h93;

    std::string h99_str{ Form( "%s_99_npos_%s", spec_var.c_str(), ana_cut_name.c_str() ) };
    store_hists[h99_str.c_str()] = h99;

    std::string h00_str{ Form( "%s_00_npos_%s", spec_var.c_str(), ana_cut_name.c_str() ) };
    store_hists[h00_str.c_str()] = hists.at( Form( "%s_%s_%s", spec_var.c_str(), "0", ana_cut_name.c_str() ));

  }

  std::string output_file_string = Form( "./subtracted_%s_%s_A-%s_S-%s.root", unique.c_str(), type.c_str(),
                                          abin_var.c_str(), spec_var.c_str() );

  std::cout << "writing output" << std::endl;
  TFile * output_file = new TFile( output_file_string.c_str(), "RECREATE" );
  output_file->cd();
  for ( std::pair< std::string, TH1F * > hist_pair : store_hists){
    hist_pair.second->Write( hist_pair.first.c_str() );
  }
  

  int current_cut = cf_sub->cuts - 2;
  int ana_bins = analysis_bound.get_bins();
  float ana_min = analysis_bound.get_min();
  float ana_max = analysis_bound.get_max();
  TH1F * mass_hist = new TH1F( "mass_hist", "", ana_bins, ana_min, ana_max );
  TH1F * full_hist = new TH1F( "full_hist", "", ana_bins, ana_min, ana_max );


  std::cout << "cutflow output" << std::endl;
  if ( !cf_path.empty() ){
    std::string ana_str = Form( "%s%s_%s-", spec_var.c_str(), "_93_npos_Q0", abin_var.c_str() );
    recombine_spectator( output_file, ana_bins, ana_str, type, false, mass_hist );
    cf_sub->set_dist( current_cut, mass_hist );
    ana_str = Form( "%s%s_%s-", spec_var.c_str(), "_99_npos_Q0", abin_var.c_str() );
    recombine_spectator( output_file, ana_bins, ana_str, type, false, full_hist );
    current_cut++;
    cf_sub->set_dist( current_cut, full_hist );
    cf_sub->write( Form( "%s_%s", type.c_str(), unique.c_str() ), analysis_bound );
  }

  output_file->Close();
  delete output_file;



}
