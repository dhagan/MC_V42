#include <main.hxx>

#include <subtract.hxx>
#include <positive.hxx>
#include <zero.hxx>

int help(){
  std::cout << "Usage: " << std::endl;
  std::cout << "./subtract --mode,-m MODE --input,-i INPUT_PATH --analysis,-a ANALYSIS_VAR --spectator,-s SPECTATOR_VAR \\ " << std::endl;
  std::cout << "           --selections,-v SELECTION_PATH --unique,-u UNIQUE [ --type,-t TYPE ] [ --ranges,-r RANGE_STRING ] \\ " << std::endl;
  std::cout << "           [ --cuts,-c EXTRA_CUT ] [ --help,-h ] " << std::endl;
  std::cout << " " << std::endl;
  std::cout << "Required arguments; " << std::endl;
  std::cout << "  --mode,-m             Program execution mode. Subtract \"sub\" or alter \"pos\". \"zero\" mode is also" << std::endl;
  std::cout << "                        available for outputting files containing no score. " << std::endl;
  std::cout << "  --input,-i            Input file path, in \"pos\" mode, supply data, sign, and bckg paths in colon " << std::endl;
  std::cout << "                        separated list " << std::endl;
  std::cout << "  --analysis,-a         Analysis variable, baseline uses qta. This is the variable in which the " << std::endl;
  std::cout << "                        spectator distribution is split by. " << std::endl;
  std::cout << "  --spectator,-s        Distribution which in which the mass sideband and long lifetime subtraction " << std::endl;
  std::cout << "                        is performed in. " << std::endl;
  std::cout << "  --selections,-v       A filepath to a selections file. This holds the information of all the variables binning and " << std::endl;
  std::cout << "                        ranges, along with the bounds and binning of all variables " << std::endl;
  std::cout << "  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is " << std::endl;
  std::cout << "                        necessary for bookkeeping. " << std::endl;
  std::cout << " " << std::endl;
  std::cout << "Optional arguments; " << std::endl;
  std::cout << "  --ranges, -r          A string that will be processed into a bound for a variable, can be used to replace or augment " << std::endl;
  std::cout << "                        any variable definition already supplied by the selections file as a method of smaller and " << std::endl;
  std::cout << "                        faster systematic variations. " << std::endl;
  std::cout << "  --cuts,-c             Extra cut to be used during the subtraction. " << std::endl;
  std::cout << "  --help,-h             This message. " << std::endl;
  return 0;
}


int main(int argc, char *argv[]){

  static struct option long_options[] = {
      { "mode",           required_argument,    0,      'm'},
      { "input",          required_argument,    0,      'i'},
      { "analysis",       required_argument,    0,      'a'},
      { "spectator",      required_argument,    0,      's'},
      { "type",           required_argument,    0,      't'},
      { "unique",         required_argument,    0,      'u'},
      { "selections",     required_argument,    0,      'v'},
      { "ranges",         required_argument,    0,      'r'},
      { "cuts",           required_argument,    0,      'c'},
      { "bg_sub",         required_argument,    0,      'b'},
      { "cf_path",        required_argument,    0,      'g'},
      { "yvals",          required_argument,    0,      'y'},
      { "Tproc",          no_argument,          0,      'f'},
      { "help",           no_argument,          0,      'h'},
      { 0,                0,                    0,      0}
  };

  int option_index{0},option{0};

  // default args
  std::string input_path;
  std::string abin_var{ "qtA" }, spec_var{ "BDT" }, extra_var{ "" };
  std::string type, name, output_dir;
  std::string mode, unique, selections;
  std::string ranges, sub_consts, cf_path;
  std::string y_values{ "" };
  bool process_t_masses = false;
  
  do {
    option = getopt_long( argc, argv, "m:i:a:s:t:u:v:r:c:b:g:y:fh", long_options, &option_index);
    switch (option){
      case 'm':
        mode                = std::string( optarg );
        break;
      case 'i':
        input_path          = std::string( optarg );
        break;
      case 'a':
        abin_var            = std::string( optarg );
        break;
      case 's':
        spec_var            = std::string( optarg );
        break;
      case 't':
        type                = std::string( optarg );
        break;
      case 'u':
        unique              = std::string( optarg );
        break;
      case 'v':
        selections          = std::string( optarg );
        break;
      case 'r':
        ranges              = std::string( optarg );
        break;
      case 'c':
        extra_var           = std::string( optarg ); 
        break;
      case 'b':
        sub_consts          = std::string( optarg ); 
        break;
      case 'g':
        cf_path             = std::string( optarg ); 
        break;
      case 'y':
        y_values            = std::string( optarg ); 
        break;
      case 'f':
        process_t_masses    = true;   
        break;
      case 'h':
        return help();

    }
  } while ( option != -1);


  if ( mode.empty() ){ std::cout << "provide mode" << std::endl; return 0; }

  std::vector< std::string > mass_vars = { "Q0", "Q3", "Q4", "Q5", "Q12" };
  if ( process_t_masses ){
    mass_vars.insert( mass_vars.end(), { "T0", "T3", "T4", "T5", "T12" } );
  }
 // mass_vars = { "Q0" };
  
 

  if ( mode.find("sub") != std::string::npos ){

    subtraction( input_path, type, abin_var, spec_var, extra_var, selections, ranges, 
                 mass_vars, sub_consts, cf_path, y_values, unique );
    return 0;
  }

  if ( mode.find("pos") != std::string::npos ){
    alteration( input_path, abin_var, spec_var, selections, ranges , mass_vars );
    return 0;
  }

  if ( mode.find("zero") != std::string::npos ){
    zero( type, abin_var, spec_var, selections, ranges, unique );
    return 0;
  }


}

//bound_mgr * analysis_selections = new bound_mgr();
//analysis_selections->load_bound_mgr( selections );
//if ( !ranges.empty() ){ analysis_selections->process_bounds_string( ranges ); }


//std::vector< std::string > selection_sources;
//
//split_strings( selection_sources, selections, ":" );
//bound_mgr * analysis_selections = new bound_mgr();
//bound_mgr * spectator_selections = new bound_mgr();
//analysis_selections->load_bound_mgr( selection_sources[0] );
//analysis_selections->process_bounds_string( ranges );
//spectator_selections->load_bound_mgr( selection_sources[1] );
//
//bound analysis_bound = analysis_selections->get_bound( abin_var );
//bound spectator_bound = spectator_selections->get_bound( spec_var );
//bound extra_bound = bound();
//if ( !extra_var.empty() ){
//  extra_bound = analysis_selections->get_bound( extra_var );
//}
//
//std::vector< bound > mass_bounds;
//for ( std::string & mass : masses ){
//  mass_bounds.push_back( analysis_selections->get_bound( mass ) );
//}
//
//TFile * output_file = new TFile( Form( "./subtracted_%s_%s_A-%s_S-%s.root", unique.c_str(), 
//                                 type.c_str(), abin_var.c_str(), spec_var.c_str() ) );
//
//subtract_file subtract_file = { new TTree( input_path.c_str(), "READ" ), &type, &unique };
//
//for ( bound & mass : mass_bounds ){
//  subtract_bounds subtract_bounds = { &analysis_bound, &spectator_bound, &mass, &extra_bound };
//  subtraction( subtract_bounds, subtract_file, sub_consts, output_file );
//}
