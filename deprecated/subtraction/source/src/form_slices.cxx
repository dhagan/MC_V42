#include <form_slices.hxx>

void form_slices( bound_mgr * analysis, std::unordered_map< int, std::string > * slice_map ){
  for ( int mass_idx = 1; mass_idx <=4; mass_idx++ ){
    for ( int tau_idx = 1; tau_idx <=2; tau_idx++ ){
      std::string mass_cut = Form( "jmass%i", mass_idx );
      std::string tau_cut = Form( "jtau%i", tau_idx );
      std::string slice_cut = analysis->merge_cut( mass_cut, tau_cut );
      int name = std::stoi( std::to_string(mass_idx) + std::to_string(tau_idx) );
      slice_map->insert( std::pair<int,std::string>{ name, slice_cut } );
    }
  }
}
