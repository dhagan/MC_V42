#!/bin/bash 

executable=${ANA_IP}/subtraction/build/subtract
unique=zero
selections=${LIB_PATH}/share/sub_analysis_bounds.txt:${LIB_PATH}/share/sub_spectator_bounds.txt
log=${LOG_PATH}/subtraction/sub_${unique}.txt

touch ${log}
mkdir -p ${OUT_PATH}/subtraction/${unique}
mkdir -p ${OUT_PATH}/subtraction/${unique}/cutflow
pushd ${OUT_PATH}/subtraction/${unique} >> /dev/null

$executable -m "zero" -a qtA -s BDT -t bbbg -v ${selections} -u ${unique} &
$executable -m "zero" -a qtA -s BDT -t sign -v ${selections} -u ${unique} &
$executable -m "zero" -a qtA -s BDT -t bckg -v ${selections} -u ${unique} &
$executable -m "zero" -a qtA -s BDT -t data -v ${selections} -u ${unique}

popd >> /dev/null
