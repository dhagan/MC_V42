#!/bin/bash 
 
executable=${ANA_IP}/subtraction/build/subtract
unique=re448
data=${OUT_PATH}/bdt/${unique}/eval/eval_data.root:data_${unique} 
sign=${OUT_PATH}/bdt/${unique}/eval/eval_sign.root:sign_${unique}
bckg=${OUT_PATH}/bdt/${unique}/eval/eval_bckg.root:bckg_${unique}
bbbg=${OUT_PATH}/bdt/${unique}/eval/eval_bbbg.root:bbbg_${unique}
selections=${LIB_PATH}/share/sub_analysis_bounds.txt:${LIB_PATH}/share/sub_spectator_bounds.txt
log=${LOG_PATH}/subtraction/sub_${unique}.txt
cf_sign_path=${OUT_PATH}/trees/gen_${unique}/cutflow/sign_${unique}_cutflow.root:${unique} 
cf_bckg_path=${OUT_PATH}/trees/gen_${unique}/cutflow/bckg_${unique}_cutflow.root:${unique}
cf_bbbg_path=${OUT_PATH}/trees/gen_${unique}/cutflow/bbbg_${unique}_cutflow.root:${unique}
cf_data_path=${OUT_PATH}/trees/gen_${unique}/cutflow/data_${unique}_cutflow.root:${unique}



touch ${log}
mkdir -p ${OUT_PATH}/subtraction/${unique}
mkdir -p ${OUT_PATH}/subtraction/${unique}/cutflow
pushd ${OUT_PATH}/subtraction/${unique} >> /dev/null

$executable -m sub -i ${bbbg} -a qtA -s BDT -t bbbg -v ${selections} -u ${unique} -g ${cf_bbbg_path} &
$executable -m sub -i ${sign} -a qtA -s BDT -t sign -v ${selections} -u ${unique} -g ${cf_sign_path} &
$executable -m sub -i ${bckg} -a qtA -s BDT -t bckg -v ${selections} -u ${unique} -g ${cf_bckg_path} &
$executable -m sub -i ${data} -a qtA -s BDT -t data -v ${selections} -u ${unique} -g ${cf_data_path} 

sub_data=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_data_A-qtA_S-BDT.root
sub_sign=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_sign_A-qtA_S-BDT.root
sub_bckg=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_bckg_A-qtA_S-BDT.root
${executable} -m pos -i "${sub_data}:${sub_sign}:${sub_bckg}" -a qtA -s BDT -v ${selections} -u ${unique}

popd >> /dev/null
