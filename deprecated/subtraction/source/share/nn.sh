#!/bin/bash 
 
executable=${ANA_IP}/subtraction/build/subtract
input_unique=nn
output_unique=nn
data=${OUT_PATH}/nn/${input_unique}/eval/eval_data.root:data_nn
sign=${OUT_PATH}/nn/${input_unique}/eval/eval_sign.root:sign_nn
bckg=${OUT_PATH}/nn/${input_unique}/eval/eval_bckg.root:bckg_nn
bbbg=${OUT_PATH}/nn/${input_unique}/eval/eval_bbbg.root:bbbg_nn
selections=${LIB_PATH}/share/sub_analysis_bounds.txt:${LIB_PATH}/share/sub_spectator_bounds.txt
log=${LOG_PATH}/subtraction/sub_${output_unique}.txt
cf_sign_path=${OUT_PATH}/trees/gen_nn/cutflow/sign_nn_cutflow.root:nn 
cf_bckg_path=${OUT_PATH}/trees/gen_nn/cutflow/bckg_nn_cutflow.root:nn
cf_bbbg_path=${OUT_PATH}/trees/gen_nn/cutflow/bbbg_nn_cutflow.root:nn
cf_data_path=${OUT_PATH}/trees/gen_nn/cutflow/data_nn_cutflow.root:nn



touch ${log}
mkdir -p ${OUT_PATH}/subtraction/${output_unique}
mkdir -p ${OUT_PATH}/subtraction/${output_unique}/cutflow
pushd ${OUT_PATH}/subtraction/${output_unique} >> /dev/null

##$executable -m sub -i ${data} -a qtA -s BDT -t data -v ${selections} -u ${unique} 2&>1 | tee ${log} &
##$executable -m sub -i ${sign} -a qtA -s BDT -t sign -v ${selections} -u ${unique} 2&>1 | tee -a ${log} &
##$executable -m sub -i ${bckg} -a qtA -s BDT -t bckg -v ${selections} -u ${unique} 2&>1 | tee -a ${log} &
##$executable -m sub -i ${bbbg} -a qtA -s BDT -t bbbg -v ${selections} -u ${unique} 2&>1 | tee -a ${log} &

$executable -m sub -i ${bbbg} -a qtA -s DNN -t bbbg -v ${selections} -u ${output_unique} -g ${cf_bbbg_path}
$executable -m sub -i ${sign} -a qtA -s DNN -t sign -v ${selections} -u ${output_unique} -g ${cf_sign_path} &
$executable -m sub -i ${bckg} -a qtA -s DNN -t bckg -v ${selections} -u ${output_unique} -g ${cf_bckg_path} &
$executable -m sub -i ${data} -a qtA -s DNN -t data -v ${selections} -u ${output_unique} -g ${cf_data_path} &

sub_data=${OUT_PATH}/subtraction/${output_unique}/subtracted_${output_unique}_data_A-qtA_S-DNN.root
sub_sign=${OUT_PATH}/subtraction/${output_unique}/subtracted_${output_unique}_sign_A-qtA_S-DNN.root
sub_bckg=${OUT_PATH}/subtraction/${output_unique}/subtracted_${output_unique}_bckg_A-qtA_S-DNN.root
${executable} -m pos -i "${sub_data}:${sub_sign}:${sub_bckg}" -a qtA -s DNN -v ${selections} -u ${output_unique} ## 2&>1 | tee -a ${log}

popd >> /dev/null
