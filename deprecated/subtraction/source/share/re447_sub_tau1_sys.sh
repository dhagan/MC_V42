#!/bin/bash 
 
executable=${ANA_IP}/subtraction/build/subtract
input_unique=re447
output_unique=re447_sub_tau1_sys
data=${OUT_PATH}/bdt/${input_unique}/eval/eval_data.root:data_${input_unique} 
sign=${OUT_PATH}/bdt/${input_unique}/eval/eval_sign.root:sign_${input_unique}
bckg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bckg.root:bckg_${input_unique}
bbbg=${OUT_PATH}/bdt/${input_unique}/eval/eval_bbbg.root:bbbg_${input_unique}
selections=${LIB_PATH}/share/sub_analysis_sub_tau1_bounds.txt:${LIB_PATH}/share/sub_spectator_bounds.txt
log=${LOG_PATH}/subtraction/sub_${output_unique}.txt
cf_sign_path=${OUT_PATH}/trees/gen_${input_unique}/cutflow/sign_${input_unique}_cutflow.root:${input_unique} 
cf_bckg_path=${OUT_PATH}/trees/gen_${input_unique}/cutflow/bckg_${input_unique}_cutflow.root:${input_unique}
cf_bbbg_path=${OUT_PATH}/trees/gen_${input_unique}/cutflow/bbbg_${input_unique}_cutflow.root:${input_unique}
cf_data_path=${OUT_PATH}/trees/gen_${input_unique}/cutflow/data_${input_unique}_cutflow.root:${input_unique}


touch ${log}
mkdir -p ${OUT_PATH}/subtraction/${output_unique}
mkdir -p ${OUT_PATH}/subtraction/${output_unique}/cutflow
pushd ${OUT_PATH}/subtraction/${output_unique} >> /dev/null

$executable -m sub -i ${bbbg} -a qtA -s BDT -t bbbg -v ${selections} -u ${output_unique} -g ${cf_bbbg_path} &
$executable -m sub -i ${sign} -a qtA -s BDT -t sign -v ${selections} -u ${output_unique} -g ${cf_sign_path} &
$executable -m sub -i ${bckg} -a qtA -s BDT -t bckg -v ${selections} -u ${output_unique} -g ${cf_bckg_path} &
$executable -m sub -i ${data} -a qtA -s BDT -t data -v ${selections} -u ${output_unique} -g ${cf_data_path} 

sub_data=${OUT_PATH}/subtraction/${output_unique}/subtracted_${output_unique}_data_A-qtA_S-BDT.root
sub_sign=${OUT_PATH}/subtraction/${output_unique}/subtracted_${output_unique}_sign_A-qtA_S-BDT.root
sub_bckg=${OUT_PATH}/subtraction/${output_unique}/subtracted_${output_unique}_bckg_A-qtA_S-BDT.root
${executable} -m pos -i "${sub_data}:${sub_sign}:${sub_bckg}" -a qtA -s BDT -v ${selections} -r ${ranges} -u ${output_unique}

popd >> /dev/null
