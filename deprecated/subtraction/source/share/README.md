## Mass sideband and lifetime subtraction scripts

## List of scripts and purpose
* base.sh       - Baseline script
* generic.sh    - basic passthrough script. Performs normal operation on a sample with a provided unique if no special accommodations are needed at this step
* jp8.sh        - perform regular subtraction on jp8 channel $` P_{T,J/\psi} > 8 GeV `$
* jp9.sh        - perform regular subtraction on jp8 channel $` P_{T,J/\psi} > 9 GeV `$
* jp10.sh       - perform regular subtraction on jp8 channel $` P_{T,J/\psi} > 10 GeV `$
* wide.sh       - subtraction for a wider qta range 
* zero.sh       - zero signal version test for hf, input bckg as data, fit sign and bckg to this.
* exp\_bg.sh    - subtraction considering background fitted to an exponential, $`\sim5\%`$ more than base fit.


## Script Structure
Below is how the scripts are structured. All of the defined environment variables have been defined by the setup script. This setup script must be sourced before executing any of the scripts.

Define variables
```
executable=${ANA_IP}/subtraction/build/subtract
unique=base
data=${OUT_PATH}/bdt/${unique}/eval/eval_data.root:data_${unique} 
sign=${OUT_PATH}/bdt/${unique}/eval/eval_sign.root:sign_${unique}
bckg=${OUT_PATH}/bdt/${unique}/eval/eval_bckg.root:bckg_${unique}
bbbg=${OUT_PATH}/bdt/${unique}/eval/eval_bbbg.root:bbbg_${unique}
selections=${LIB_PATH}/share/sub_analysis_bounds.txt:${LIB_PATH}/share/sub_spectator_bounds.txt
log=${LOG_PATH}/subtraction/sub_${unique}.txt
```

Create logs and output directories. Shift to the output directory.
```
touch ${log}
mkdir -p ${OUT_PATH}/subtraction/${unique}
pushd ${OUT_PATH}/subtraction/${unique} >> /dev/null
```

Subtract the backgrounds
```
$executable -m sub -i ${bbbg} -a qtA -s BDT -t bbbg -v ${selections} -u ${unique}
$executable -m sub -i ${sign} -a qtA -s BDT -t sign -v ${selections} -u ${unique} &
$executable -m sub -i ${bckg} -a qtA -s BDT -t bckg -v ${selections} -u ${unique} &
$executable -m sub -i ${data} -a qtA -s BDT -t data -v ${selections} -u ${unique} &
```

Remove the zeroed bins.
```
sub_data=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_data_A-qtA_S-BDT.root
sub_sign=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_sign_A-qtA_S-BDT.root
sub_bckg=${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_bckg_A-qtA_S-BDT.root
${executable} -m pos -i "${sub_data}:${sub_sign}:${sub_bckg}" -a qtA -s BDT -v ${selections} -u ${unique} ## 2&>1 | tee -a ${log}
```

Finish up.
```
popd >> /dev/null
```
