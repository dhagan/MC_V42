#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract

sign_file=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sep/eval/Eval_sign_valid.root
bckg_file=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sep/eval/Eval_bckg_valid.root
lin_data_file=/home/atlas/dhagan/analysis/analysisFork/445/sample_mixer/run/lin_data
lin_dsign_file=/home/atlas/dhagan/analysis/analysisFork/445/sample_mixer/run/lin_sign
lin_dbckg_file=/home/atlas/dhagan/analysis/analysisFork/445/sample_mixer/run/lin_bckg

mkdir -p ./subtractions_lin
mkdir -p ./subtractions_lin/logs
mkdir -p ./subtractions_lin/eval
mkdir -p ./subtractions_lin/lin_eval_gaps


fractions=(5 10 25 50)

for frac in ${fractions[@]}
do
	for ((file = 1; file<= (50/${frac}); file++ ))
	do
		cp ${sign_file} ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s${frac}_f${file}.root 
		cp ${bckg_file} ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s${frac}_f${file}.root 
		$executable -i ${lin_data_file}/s${frac}/f${file}/lin_data_s${frac}_f${file}.root:TreeD -b Phi_low -m sub -t data -n lin -u phi_low_s${frac}_f${file}
		$executable -i ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s${frac}_f${file}.root:sign_valid -b Phi_low  -m sub -t sign -n lin -u phi_low_s${frac}_f${file}
		$executable -i ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s${frac}_f${file}.root:bckg_valid -b Phi_low -m sub -t bckg -n lin -u phi_low_s${frac}_f${file}
		$executable -i ${lin_dsign_file}/s${frac}/f${file}/lin_sign_s${frac}_f${file}.root:TreeD -b Phi_low -m sub -t data -n lin -u dsign_phi_low_s${frac}_f${file}
		$executable -i ${lin_dbckg_file}/s${frac}/f${file}/lin_bckg_s${frac}_f${file}.root:TreeD -b Phi_low -m sub -t data -n lin -u dbckg_phi_low_s${frac}_f${file}

		$executable -i ${lin_data_file}/s${frac}/f${file}/lin_data_s${frac}_f${file}.root:TreeD -b Phi_high -m sub -t data -n lin -u phi_high_s${frac}_f${file}
		$executable -i ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s${frac}_f${file}.root:sign_valid -b Phi_high  -m sub -t sign -n lin -u phi_high_s${frac}_f${file}
		$executable -i ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s${frac}_f${file}.root:bckg_valid -b Phi_high -m sub -t bckg -n lin -u phi_high_s${frac}_f${file}
		$executable -i ${lin_dsign_file}/s${frac}/f${file}/lin_sign_s${frac}_f${file}.root:TreeD -b Phi_high -m sub -t data -n lin -u dsign_phi_high_s${frac}_f${file}
		$executable -i ${lin_dbckg_file}/s${frac}/f${file}/lin_bckg_s${frac}_f${file}.root:TreeD -b Phi_high -m sub -t data -n lin -u dbckg_phi_high_s${frac}_f${file}

	done
done

##cp ${sign_file} ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s60_f1.root 
##cp ${bckg_file} ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s60_f1.root 
##$executable -i ${lin_data_file}/s60/f1/lin_data_s60_f1.root:TreeD -m sub -t data -n lin -u s60_f1
##$executable -i ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s60_f1.root:sign_valid  -m sub -t sign -n lin -u s60_f1
##$executable -i ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s60_f1.root:bckg_valid  -m sub -t bckg -n lin -u s60_f1
##$executable -i ${lin_dsign_file}/s60/f1/lin_sign_s60_f1.root:TreeD -m sub -t data -n lin -u dsign_s60_f1
##$executable -i ${lin_dbckg_file}/s60/f1/lin_bckg_s60_f1.root:TreeD -m sub -t data -n lin -u dbckg_s60_f1
##
##cp ${sign_file} ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s75_f1.root 
##cp ${bckg_file} ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s75_f1.root 
##$executable -i ${lin_data_file}/s75/f1/lin_data_s75_f1.root:TreeD -m sub -t data -n lin -u s75_f1
##$executable -i ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s75_f1.root:sign_valid  -m sub -t sign -n lin -u s75_f1
##$executable -i ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s75_f1.root:bckg_valid  -m sub -t bckg -n lin -u s75_f1
##$executable -i ${lin_dsign_file}/s75/f1/lin_sign_s75_f1.root:TreeD -m sub -t data -n lin -u dsign_s75_f1
##$executable -i ${lin_dbckg_file}/s75/f1/lin_bckg_s75_f1.root:TreeD -m sub -t data -n lin -u dbckg_s75_f1

