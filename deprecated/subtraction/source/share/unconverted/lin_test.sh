#!/bin/bash

executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract

sign_file=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sep/eval/Eval_sign_valid.root
bckg_file=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_sep/eval/Eval_bckg_valid.root
lin_data_file=/home/atlas/dhagan/analysis/analysisFork/445/sample_mixer/run/lin_data
lin_dsign_file=/home/atlas/dhagan/analysis/analysisFork/445/sample_mixer/run/lin_sign
lin_dbckg_file=/home/atlas/dhagan/analysis/analysisFork/445/sample_mixer/run/lin_bckg

##mkdir -p ./subtractions_lin
##mkdir -p ./subtractions_lin/logs
##mkdir -p ./subtractions_lin/eval
##mkdir -p ./subtractions_lin/lin_eval_gaps



$executable -i ${lin_data_file}/s5/f1/lin_data_s5_f1.root:TreeD -m sub -t data -n lin -u s5_f1
##$executable -i ./subtractions_lin/lin_eval_gaps/Eval_sign_valid_s5_f1.root:sign_valid  -m sub -t sign -n lin -u s5_f1
##$executable -i ./subtractions_lin/lin_eval_gaps/Eval_bckg_valid_s5_f1.root:bckg_valid  -m sub -t bckg -n lin -u s5_f1
##$executable -i ${lin_dsign_file}/s5/f1/lin_sign_s5_f1.root:TreeD -m sub -t data -n lin -u dsign_s5_f1
##$executable -i ${lin_dbckg_file}/s5/f1/lin_bckg_s5_f1.root:TreeD -m sub -t data -n lin -u dbckg_s5_f1
