#!/bin/bash

unique=BDT
executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_sign.root
bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_bckg.root
data=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_data.root
bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_bbbg.root
tree=_base

mkdir -p ./subtractions_${unique}
mkdir -p ./subtractions_${unique}/logs
mkdir -p ./subtractions_${unique}/eval


#### qtA
##local_unique=bdt8
##range="BDT_8,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 
##
##local_unique=bdt12
##range="BDT_12,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 
##
##local_unique=bdt14
##range="BDT_14,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 

## qtB
local_unique=bdt8
range="BDT_8,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
$executable -b qtB -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 

local_unique=bdt12
range="BDT_12,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
$executable -b qtB -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 

local_unique=bdt14
range="BDT_12,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
$executable -b qtB -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 










##local_unique=bdt12
##range="BDT_12,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 
##
##local_unique=bdt14
##range="BDT_14,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 
##
##local_unique=bdt16
##range="BDT_16,-1.0,1.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 


##range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0,2740.0,3460.0#DiMuonTau_-5.0,0.91:0.91,15.0:-5.0,15.0"
##$executable -i ${data}:data${tree} -m sub -n sub_sys -t data -u SYS_DiMuonMass1_Tau1 -r ${range}
##$executable -i ${sign}:signal${tree} -m sub -n sub_sys -t sign -u SYS_DiMuonMass1_Tau1 -r ${range}
##$executable -i ${bckg}:pp${tree} -m sub -n sub_sys -t bckg -u SYS_DiMuonMass1_Tau1 -r ${range}
##$executable -i ${bbbg}:bb${tree} -m sub -n sub_sys -t bbbg -u SYS_DiMuonMass1_Tau1 -r ${range}
##sub_qta_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtA_S-BDT_SYS_DiMuonMass1_Tau1.root
##sub_qta_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtA_S-BDT_SYS_DiMuonMass1_Tau1.root
##sub_qta_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtA_S-BDT_SYS_DiMuonMass1_Tau1.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range}
##
##range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0,2740.0,3460.0#DiMuonTau_-5.0,0.81:0.81,15.0:-5.0,15.0"
##$executable -i ${data}:data${tree} -m sub -n sub_sys -t data -u SYS_DiMuonMass1_Tau2 -r ${range}
##$executable -i ${sign}:signal${tree} -m sub -n sub_sys -t sign -u SYS_DiMuonMass1_Tau2 -r ${range}
##$executable -i ${bckg}:pp${tree} -m sub -n sub_sys -t bckg -u SYS_DiMuonMass1_Tau2 -r ${range}
##$executable -i ${bbbg}:bb${tree} -m sub -n sub_sys -t bbbg -u SYS_DiMuonMass1_Tau2 -r ${range}
##sub_qta_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtA_S-BDT_SYS_DiMuonMass1_Tau2.root
##sub_qta_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtA_S-BDT_SYS_DiMuonMass1_Tau2.root
##sub_qta_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtA_S-BDT_SYS_DiMuonMass1_Tau2.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range}
##
##
#### qtB
##range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0:2740.0,3460.0"
##$executable -i ${data}:data${tree} -b qtB -m sub -n sub_sys -t data -u SYS_DiMuonMass1_Tau0 -r ${range}
##$executable -i ${sign}:signal${tree} -b qtB -m sub -n sub_sys -t sign -u SYS_DiMuonMass1_Tau0 -r ${range}
##$executable -i ${bckg}:pp${tree} -b qtB -m sub -n sub_sys -t bckg -u SYS_DiMuonMass1_Tau0 -r ${range}
##$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n sub_sys -t bbbg -u SYS_DiMuonMass1_Tau0 -r ${range}
##sub_qtb_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass1_Tau0.root
##sub_qtb_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass1_Tau0.root
##sub_qtb_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass1_Tau0.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range}
##
##range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0,2740.0,3460.0#DiMuonTau_-5.0,0.91:0.91,15.0:-5.0,15.0"
##$executable -i ${data}:data${tree} -b qtB -m sub -n sub_sys -t data -u SYS_DiMuonMass1_Tau1 -r ${range}
##$executable -i ${sign}:signal${tree} -b qtB -m sub -n sub_sys -t sign -u SYS_DiMuonMass1_Tau1 -r ${range}
##$executable -i ${bckg}:pp${tree} -b qtB -m sub -n sub_sys -t bckg -u SYS_DiMuonMass1_Tau1 -r ${range}
##$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n sub_sys -t bbbg -u SYS_DiMuonMass1_Tau1 -r ${range}
##sub_qtb_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass1_Tau1.root
##sub_qtb_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass1_Tau1.root
##sub_qtb_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass1_Tau1.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range}
##
##range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0,2740.0,3460.0#DiMuonTau_-5.0,0.81:0.81,15.0:-5.0,15.0"
##$executable -i ${data}:data${tree} -b qtB -m sub -n sub_sys -t data -u SYS_DiMuonMass1_Tau2 -r ${range}
##$executable -i ${sign}:signal${tree} -b qtB -m sub -n sub_sys -t sign -u SYS_DiMuonMass1_Tau2 -r ${range}
##$executable -i ${bckg}:pp${tree} -b qtB -m sub -n sub_sys -t bckg -u SYS_DiMuonMass1_Tau2 -r ${range}
##$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n sub_sys -t bbbg -u SYS_DiMuonMass1_Tau2 -r ${range}
##sub_qtb_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass1_Tau2.root
##sub_qtb_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass1_Tau2.root
##sub_qtb_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass1_Tau2.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range}
##
##range="DiMuonTau_-5.0,0.91:0.91,15.0:-5.0,15.0"
##$executable -i ${data}:data${tree} -b qtB -m sub -n sub_sys -t data -u SYS_DiMuonMass0_Tau1 -r ${range}
##$executable -i ${sign}:signal${tree} -b qtB -m sub -n sub_sys -t sign -u SYS_DiMuonMass0_Tau1 -r ${range}
##$executable -i ${bckg}:pp${tree} -b qtB -m sub -n sub_sys -t bckg -u SYS_DiMuonMass0_Tau1 -r ${range}
##$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n sub_sys -t bbbg -u SYS_DiMuonMass0_Tau1 -r ${range}
##sub_qtb_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass0_Tau1.root
##sub_qtb_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass0_Tau1.root
##sub_qtb_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass0_Tau1.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range}
##
##range="DiMuonTau_-5.0,0.81:0.81,15.0:-5.0,15.0"
##$executable -i ${data}:data${tree} -b qtB -m sub -n sub_sys -t data -u SYS_DiMuonMass0_Tau2 -r ${range}
##$executable -i ${sign}:signal${tree} -b qtB -m sub -n sub_sys -t sign -u SYS_DiMuonMass0_Tau2 -r ${range}
##$executable -i ${bckg}:pp${tree} -b qtB -m sub -n sub_sys -t bckg -u SYS_DiMuonMass0_Tau2 -r ${range}
##$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n sub_sys -t bbbg -u SYS_DiMuonMass0_Tau2 -r ${range}
##sub_qtb_data=./subtractions_sub_sys/eval/subtracted_sub_sys_data_A-qtB_S-BDT_SYS_DiMuonMass0_Tau2.root
##sub_qtb_sign=./subtractions_sub_sys/eval/subtracted_sub_sys_sign_A-qtB_S-BDT_SYS_DiMuonMass0_Tau2.root
##sub_qtb_bckg=./subtractions_sub_sys/eval/subtracted_sub_sys_bckg_A-qtB_S-BDT_SYS_DiMuonMass0_Tau2.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range}


#### PhotonPt
##$executable -i ${data}:data${tree} -b PhotonPt -m sub -n sys -t data
##$executable -i ${sign}:signal${tree} -b PhotonPt -m sub -n sys -t sign
##$executable -i ${bckg}:pp${tree} -b PhotonPt -m sub -n base -t bckg
##$executable -i ${bbbg}:bb${tree} -b PhotonPt -m sub -n base -t bbbg
##sub_photonpt_data=./subtractions_sub_sys/eval/subtracted_base_data_A-PhotonPt_S-BDT.root
##sub_photonpt_sign=./subtractions_sub_sys/eval/subtracted_base_sign_A-PhotonPt_S-BDT.root
##sub_photonpt_bckg=./subtractions_sub_sys/eval/subtracted_base_bckg_A-PhotonPt_S-BDT.root
##$executable -b PhotonPt -d ${sub_photonpt_data} -j ${sub_photonpt_sign} -p ${sub_photonpt_bckg} -m pos 
##
#### DiMuonPt
##$executable -i ${data}:data${tree} -b DiMuonPt -m sub -n base -t data
##$executable -i ${sign}:signal${tree} -b DiMuonPt -m sub -n base -t sign
##$executable -i ${bckg}:pp${tree} -b DiMuonPt -m sub -n base -t bckg
##$executable -i ${bbbg}:bb${tree} -b DiMuonPt -m sub -n base -t bbbg
##sub_dimuonpt_data=./subtractions_sub_sys/eval/subtracted_base_data_A-DiMuonPt_S-BDT.root
##sub_dimuonpt_sign=./subtractions_sub_sys/eval/subtracted_base_sign_A-DiMuonPt_S-BDT.root
##sub_dimuonpt_bckg=./subtractions_sub_sys/eval/subtracted_base_bckg_A-DiMuonPt_S-BDT.root
##$executable -b DiMuonPt -d ${sub_dimuonpt_data} -j ${sub_dimuonpt_sign} -p ${sub_dimuonpt_bckg} -m pos 
##
#### DPhi
##$executable -i ${data}:data${tree} -b DPhi -m sub -n base -t data
##$executable -i ${sign}:signal${tree} -b DPhi -m sub -n base -t sign
##$executable -i ${bckg}:pp${tree} -b DPhi -m sub -n base -t bckg
##$executable -i ${bbbg}:bb${tree} -b DPhi -m sub -n base -t bbbg
##sub_dphi_data=./subtractions_sub_sys/eval/subtracted_base_data_A-DPhi_S-BDT.root
##sub_dphi_sign=./subtractions_sub_sys/eval/subtracted_base_sign_A-DPhi_S-BDT.root
##sub_dphi_bckg=./subtractions_sub_sys/eval/subtracted_base_bckg_A-DPhi_S-BDT.root
##$executable -b DPhi -d ${sub_dphi_data} -j ${sub_dphi_sign} -p ${sub_dphi_bckg} -m pos 




