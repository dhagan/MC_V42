#!/bin/bash


executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
ranges="qtA_15,-10,20#qtB_10,0,20"


params=( "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=GiniIndex:DoBoostMonitor=True#AdaBoost-GiniIndex-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=GiniIndexWithLaplace:DoBoostMonitor=True#AdaBoost-GiniIndexWithLaplace-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=MisClassificationError:DoBoostMonitor=True#AdaBoost-MisClassificationError-B1T100D3N5C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=GiniIndex:DoBoostMonitor=True#AdaBoost-GiniIndex-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=GiniIndexWithLaplace:DoBoostMonitor=True#AdaBoost-GiniIndexWithLaplace-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=1:SeparationType=MisClassificationError:DoBoostMonitor=True#AdaBoost-MisClassificationError-B1T100D3N5C50P1" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=10.000000:nCuts=50:DoPreselection=0:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N10C50P0" "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=10.000000:nCuts=50:DoPreselection=0:SeparationType=GiniIndex:DoBoostMonitor=True#AdaBoost-GiniIndex-B1T100D3N10C50P0" )

##params=( "BoostType=AdaBoost:AdaBoostBeta=0.100000:NTrees=100:MaxDepth=3:MinNodeSize=5.000000:nCuts=50:DoPreselection=0:SeparationType=CrossEntropy:DoBoostMonitor=True#AdaBoost-CrossEntropy-B1T100D3N5C50P0" )
vars=( alt angular momentum maximum minimum )
##vars=( alt )


mkdir -p opt
cd opt


for var in ${vars[@]}
do
	for param in ${params[@]}
	do
		unique=${param#*#}_${var}
		mkdir -p ./subtractions_${unique}
		mkdir -p ./subtractions_${unique}/eval
		opt_path=/home/atlas/dhagan/analysis/bdt_opt/run/${unique}/eval
		$executable -i ${opt_path}/eval_data.root:data_ -b qtA -m sub -u ${unique} -t data -r ${ranges}
		$executable -i ${opt_path}/eval_sign.root:sign_ -b qtA -m sub -u ${unique} -t sign -r ${ranges}
		$executable -i ${opt_path}/eval_bckg.root:bckg_ -b qtA -m sub -u ${unique} -t bckg -r ${ranges}
		$executable -i ${opt_path}/eval_bbbg.root:bbbg_ -b qtA -m sub -u ${unique} -t bbbg -r ${ranges}
		sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${unique}.root
		sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${unique}.root
		sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${unique}.root
		$executable -b qtA -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${ranges}
	done
done

##echo ${unique}
##echo ${tree}
##echo ${opt_path}
##ls -lisah ${opt_path}/
