#!/bin/bash

original=base
unique=win2_sys
executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${original}/eval/eval_sign.root
bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${original}/eval/eval_bckg.root
data=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${original}/eval/eval_data.root
bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${original}/eval/eval_bbbg.root
tree=_${original}

mkdir -p ./subtractions_${unique}
mkdir -p ./subtractions_${unique}/logs
mkdir -p ./subtractions_${unique}/eval

## qtA
ranges="SubConst_1.5,0.667,0.667,1.5#DiMuonMass_2700.0,2860.0:2860.0,3100:3100.0,3360.0:3360.0,3500.0#qtA_15,-10,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -u ${unique} -t data -r ${ranges}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -u ${unique} -t sign -r ${ranges}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -u ${unique} -t bckg -r ${ranges}
$executable -i ${bbbg}:bbbg${tree} -b qtA -m sub -u ${unique} -t bbbg -r ${ranges}
##sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${unique}.root
##sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${unique}.root
##sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${ranges} 

##local_unique=DiMuonMass0_Tau1
##range="DiMuonTau_-5.0,0.91:0.91,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 
##
##local_unique=DiMuonMass0_Tau2
##range="DiMuonTau_-5.0,0.81:0.81,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${local_unique}.root
##sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${local_unique}.root
##sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${local_unique}.root
##$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 
##
##
#### qtA
##local_unique=DiMuonMass1_Tau0
##range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0:2740.0,3460.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range} 
##
##local_unique=DiMuonMass0_Tau1
##range="DiMuonTau_-5.0,0.91:0.91,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range} 
##
##local_unique=DiMuonMass0_Tau2
##range="DiMuonTau_-5.0,0.81:0.81,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range} 
##
