#!/bin/bash


executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
ranges="qtA_15,-10,20#qtB_10,0,20"

##vars=( "aaf" "afa" "faa" "extd" "noqt2" "noL" )
##vars=( "aaDPDY" "afDPDY" "faDPDY" "ffDPDP" )
##vars=( "ffDPDY" )
vars=( "aaf" "afa" "faa" "extd" "noqt2" "noL" "aaDPDY" "afDPDY" "faDPDY" "ffDPDP" "ffDPDY" )

mkdir -p var_sys
cd var_sys

for var in ${vars[@]}
do
	unique=${var}
	mkdir -p ./subtractions_${unique}
	mkdir -p ./subtractions_${unique}/eval
	opt_path=/home/atlas/dhagan/analysis/bdt_opt/run/${unique}/eval
	$executable -i ${opt_path}/eval_data.root:data_${unique} -b qtA -m sub -u ${unique} -t data -r ${ranges}
	$executable -i ${opt_path}/eval_sign.root:sign_${unique} -b qtA -m sub -u ${unique} -t sign -r ${ranges}
	$executable -i ${opt_path}/eval_bckg.root:bckg_${unique} -b qtA -m sub -u ${unique} -t bckg -r ${ranges}
	$executable -i ${opt_path}/eval_bbbg.root:bbbg_${unique} -b qtA -m sub -u ${unique} -t bbbg -r ${ranges}
	sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${unique}.root
	sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${unique}.root
	sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${unique}.root
	$executable -b qtA -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${ranges}
done

