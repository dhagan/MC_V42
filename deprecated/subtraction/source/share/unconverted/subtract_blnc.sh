#!/bin/bash
executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_sign.root
bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_bckg.root
data=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_data.root
bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_bbbg.root
tree=_base

mkdir -p ./subtractions_blnc
mkdir -p ./subtractions_blnc/logs
mkdir -p ./subtractions_blnc/eval


##splits=(0.080 0.085 0.090 0.095 0.105 0.110 0.115 0.120)
##names=(80 85 90 95 105 110 115 120)
##inds=(0..7)

##splits=(0.2 0.25)
##names=(200 250)
##inds=(0 1)

##splits=(0.225)
##names=(225)
##inds=(0)

splits=(0.210)
names=(210)
inds=(0)

for ind in ${inds[@]}
do
	range=${splits[$ind]}
	name=${names[$ind]}

	$executable -i ${data}:data${tree} -b Phi_low -m sub -n blnc -t data -u low_${name} -r CosSplit_${range} 
	$executable -i ${sign}:sign${tree} -b Phi_low -m sub -n blnc -t sign -u low_${name} -r CosSplit_${range}
	$executable -i ${bckg}:bckg${tree} -b Phi_low -m sub -n blnc -t bckg -u low_${name} -r CosSplit_${range}
	sub_phi_low_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_low_${name}.root
	sub_phi_low_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_low_${name}.root
	sub_phi_low_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_low_${name}.root
	$executable -b Phi_low -d ${sub_phi_low_data}  -j ${sub_phi_low_sign} -p ${sub_phi_low_bckg} -m pos
	
	$executable -i ${data}:data${tree} -b Phi_high -m sub -n blnc -t data -u high_${name} -r CosSplit_${range} 
	$executable -i ${sign}:sign${tree} -b Phi_high -m sub -n blnc -t sign -u high_${name} -r CosSplit_${range}
	$executable -i ${bckg}:bckg${tree} -b Phi_high -m sub -n blnc -t bckg -u high_${name} -r CosSplit_${range}
	sub_phi_high_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_high_${name}.root
	sub_phi_high_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_high_${name}.root
	sub_phi_high_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_high_${name}.root
	$executable -b Phi_high -d ${sub_phi_high_data}  -j ${sub_phi_high_sign} -p ${sub_phi_high_bckg} -m pos
done



##range="CosSplit_0.105"
#### Phi low
##$executable -i ${data}:data${tree} -b Phi_low -m sub -n blnc -t data -u low_5 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_low -m sub -n blnc -t sign -u low_5 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_low -m sub -n blnc -t bckg -u low_5 -r ${range}
##sub_phi_low_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_low_5.root
##sub_phi_low_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_low_5.root
##sub_phi_low_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_low_5.root
##$executable -b Phi_low -d ${sub_phi_low_data}  -j ${sub_phi_low_sign} -p ${sub_phi_low_bckg} -m pos
##
##
###### Phi high
##$executable -i ${data}:data${tree} -b Phi_high -m sub -n blnc -t data -u high_5 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_high -m sub -n blnc -t sign -u high_5 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_high -m sub -n blnc -t bckg -u high_5 -r ${range}
##sub_phi_high_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_high_5.root
##sub_phi_high_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_high_5.root
##sub_phi_high_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_high_5.root
##$executable -b Phi_high -d ${sub_phi_high_data}  -j ${sub_phi_high_sign} -p ${sub_phi_high_bckg} -m pos
##
##range="CosSplit_0.11"
###### Phi 10
##$executable -i ${data}:data${tree} -b Phi_low -m sub -n blnc -t data -u low_10 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_low -m sub -n blnc -t sign -u low_10 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_low -m sub -n blnc -t bckg -u low_10 -r ${range}
##sub_phi_low_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_low_10.root
##sub_phi_low_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_low_10.root
##sub_phi_low_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_low_10.root
##$executable -b Phi_low -d ${sub_phi_low_data}  -j ${sub_phi_low_sign} -p ${sub_phi_low_bckg} -m pos
##
##
###### Phi high
##$executable -i ${data}:data${tree} -b Phi_high -m sub -n blnc -t data -u high_10 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_high -m sub -n blnc -t sign -u high_10 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_high -m sub -n blnc -t bckg -u high_10 -r ${range}
##sub_phi_high_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_high_10.root
##sub_phi_high_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_high_10.root
##sub_phi_high_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_high_10.root
##$executable -b Phi_high -d ${sub_phi_high_data}  -j ${sub_phi_high_sign} -p ${sub_phi_high_bckg} -m pos
##
##range="CosSplit_0.115"
###### Phi 15
##$executable -i ${data}:data${tree} -b Phi_low -m sub -n blnc -t data -u low_15 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_low -m sub -n blnc -t sign -u low_15 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_low -m sub -n blnc -t bckg -u low_15 -r ${range}
##sub_phi_low_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_low_15.root
##sub_phi_low_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_low_15.root
##sub_phi_low_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_low_15.root
##$executable -b Phi_low -d ${sub_phi_low_data}  -j ${sub_phi_low_sign} -p ${sub_phi_low_bckg} -m pos
##
##
###### Phi high
##$executable -i ${data}:data${tree} -b Phi_high -m sub -n blnc -t data -u high_15 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_high -m sub -n blnc -t sign -u high_15 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_high -m sub -n blnc -t bckg -u high_15 -r ${range}
##sub_phi_high_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_high_15.root
##sub_phi_high_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_high_15.root
##sub_phi_high_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_high_15.root
##$executable -b Phi_high -d ${sub_phi_high_data}  -j ${sub_phi_high_sign} -p ${sub_phi_high_bckg} -m pos
##
##
##range="CosSplit_0.120"
###### Phi 20
##$executable -i ${data}:data${tree} -b Phi_low -m sub -n blnc -t data -u low_20 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_low -m sub -n blnc -t sign -u low_20 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_low -m sub -n blnc -t bckg -u low_20 -r ${range}
##sub_phi_low_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_low_20.root
##sub_phi_low_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_low_20.root
##sub_phi_low_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_low_20.root
##$executable -b Phi_low -d ${sub_phi_low_data}  -j ${sub_phi_low_sign} -p ${sub_phi_low_bckg} -m pos
##
##
###### Phi high
##$executable -i ${data}:data${tree} -b Phi_high -m sub -n blnc -t data -u high_20 -r ${range} 
##$executable -i ${sign}:sign${tree} -b Phi_high -m sub -n blnc -t sign -u high_20 -r ${range}
##$executable -i ${bckg}:bckg${tree} -b Phi_high -m sub -n blnc -t bckg -u high_20 -r ${range}
##sub_phi_high_data=./subtractions_blnc/eval/subtracted_blnc_data_A-Phi_S-BDT_high_20.root
##sub_phi_high_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-Phi_S-BDT_high_20.root
##sub_phi_high_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-Phi_S-BDT_high_20.root
##$executable -b Phi_high -d ${sub_phi_high_data}  -j ${sub_phi_high_sign} -p ${sub_phi_high_bckg} -m pos


###### qtA
##$executable -i ${data}:data${tree} -b qtA -m sub -n blnc -t data
##$executable -i ${sign}:sign${tree} -b qtA -m sub -n blnc -t sign
##$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n blnc -t bckg
####$executable -i ${bbbg}:bb${tree} -m sub -n blnc -t bbbg
##sub_qta_data=./subtractions_blnc/eval/subtracted_blnc_data_A-qtA_S-BDT.root
##sub_qta_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-qtA_S-BDT.root
##sub_qta_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-qtA_S-BDT.root
##$executable -b qtA -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos 
##
#### qtB
##$executable -i ${data}:data${tree} -b qtB -m sub -n blnc -t data
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n blnc -t sign
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n blnc -t bckg
####$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n blnc -t bbbg
##sub_qtb_data=./subtractions_blnc/eval/subtracted_blnc_data_A-qtB_S-BDT.root
##sub_qtb_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-qtB_S-BDT.root
##sub_qtb_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-qtB_S-BDT.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos 


#### PhotonPt
##$executable -i ${data}:data${tree} -b PhotonPt -m sub -n blnc -t data
##$executable -i ${sign}:signal${tree} -b PhotonPt -m sub -n blnc -t sign
##$executable -i ${bckg}:pp${tree} -b PhotonPt -m sub -n blnc -t bckg
##$executable -i ${bbbg}:bb${tree} -b PhotonPt -m sub -n blnc -t bbbg
##sub_photonpt_data=./subtractions_blnc/eval/subtracted_blnc_data_A-PhotonPt_S-BDT.root
##sub_photonpt_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-PhotonPt_S-BDT.root
##sub_photonpt_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-PhotonPt_S-BDT.root
##$executable -b PhotonPt -d ${sub_photonpt_data} -j ${sub_photonpt_sign} -p ${sub_photonpt_bckg} -m pos 
##
#### DiMuonPt
##$executable -i ${data}:data${tree} -b DiMuonPt -m sub -n blnc -t data
##$executable -i ${sign}:signal${tree} -b DiMuonPt -m sub -n blnc -t sign
##$executable -i ${bckg}:pp${tree} -b DiMuonPt -m sub -n blnc -t bckg
##$executable -i ${bbbg}:bb${tree} -b DiMuonPt -m sub -n blnc -t bbbg
##sub_dimuonpt_data=./subtractions_blnc/eval/subtracted_blnc_data_A-DiMuonPt_S-BDT.root
##sub_dimuonpt_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-DiMuonPt_S-BDT.root
##sub_dimuonpt_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-DiMuonPt_S-BDT.root
##$executable -b DiMuonPt -d ${sub_dimuonpt_data} -j ${sub_dimuonpt_sign} -p ${sub_dimuonpt_bckg} -m pos 
##
#### DPhi
##$executable -i ${data}:data${tree} -b DPhi -m sub -n blnc -t data
##$executable -i ${sign}:signal${tree} -b DPhi -m sub -n blnc -t sign
##$executable -i ${bckg}:pp${tree} -b DPhi -m sub -n blnc -t bckg
##$executable -i ${bbbg}:bb${tree} -b DPhi -m sub -n blnc -t bbbg
##sub_dphi_data=./subtractions_blnc/eval/subtracted_blnc_data_A-DPhi_S-BDT.root
##sub_dphi_sign=./subtractions_blnc/eval/subtracted_blnc_sign_A-DPhi_S-BDT.root
##sub_dphi_bckg=./subtractions_blnc/eval/subtracted_blnc_bckg_A-DPhi_S-BDT.root
##$executable -b DPhi -d ${sub_dphi_data} -j ${sub_dphi_sign} -p ${sub_dphi_bckg} -m pos 
