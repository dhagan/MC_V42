#!/bin/bash

unique="re448"
executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_sign.root
bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_bckg.root
data=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_data.root
bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_bbbg.root
tree=_${unique}
ranges="qtA_15,-10,20#qtB_10,0,20"

mkdir -p ./subtractions_${unique}
mkdir -p ./subtractions_${unique}/logs
mkdir -p ./subtractions_${unique}/eval
#cd subtractions_${unique}

#### qtA
$executable -i ${data}:data${tree} -b qtA -m sub -u ${unique} -t data -r ${ranges}
$executable -i ${sign}:sign${tree} -b qtA -m sub -u ${unique} -t sign -r ${ranges}
$executable -i ${bckg}:bckg${tree} -b qtA -m sub -u ${unique} -t bckg -r ${ranges}
$executable -i ${bbbg}:bbbg${tree} -b qtA -m sub -u ${unique} -t bbbg -r ${ranges}
sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT_${unique}.root
sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT_${unique}.root
sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT_${unique}.root
$executable -b qtA -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${ranges}

###### qtB
##$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -r ${ranges}
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -r ${ranges}
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -r ${ranges}
####$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n ${unique} -t bbbg
##sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT.root
##sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT.root
##sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${ranges}
##
###### Phi low
##$executable -i ${data}:data${tree} -b Phi_low -m sub -n ${unique} -t data -u low -r ${ranges}
##$executable -i ${sign}:sign${tree} -b Phi_low -m sub -n ${unique} -t sign -u low -r ${ranges}
##$executable -i ${bckg}:bckg${tree} -b Phi_low -m sub -n ${unique} -t bckg -u low -r ${ranges}
####$executable -i ${bbbg}:bb${tree} -b Phi_low -m sub -n ${unique} -t bbbg -u low
##sub_phi_low_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-Phi_S-BDT_low.root
##sub_phi_low_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-Phi_S-BDT_low.root
##sub_phi_low_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-Phi_S-BDT_low.root
##$executable -b Phi_low -d ${sub_phi_low_data}  -j ${sub_phi_low_sign} -p ${sub_phi_low_bckg} -m pos -r ${ranges}
##
###### Phi high
##$executable -i ${data}:data${tree} -b Phi_high -m sub -n ${unique} -t data -u high -r ${ranges}
##$executable -i ${sign}:sign${tree} -b Phi_high -m sub -n ${unique} -t sign -u high -r ${ranges}
##$executable -i ${bckg}:bckg${tree} -b Phi_high -m sub -n ${unique} -t bckg -u high -r ${ranges}
####$executable -i ${bbbg}:bb${tree} -b Phi_high -m sub -n ${unique} -t bbbg -u high
##sub_phi_high_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-Phi_S-BDT_high.root
##sub_phi_high_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-Phi_S-BDT_high.root
##sub_phi_high_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-Phi_S-BDT_high.root
##$executable -b Phi_high -d ${sub_phi_high_data}  -j ${sub_phi_high_sign} -p ${sub_phi_high_bckg} -m pos -r ${ranges}
##
###### Phi both
##$executable -i ${data}:data${tree} -b Phi -m sub -n ${unique} -t data -r ${ranges}
##$executable -i ${sign}:sign${tree} -b Phi -m sub -n ${unique} -t sign -r ${ranges}
##$executable -i ${bckg}:bckg${tree} -b Phi -m sub -n ${unique} -t bckg -r ${ranges}
####$executable -i ${bbbg}:bb${tree} -b Phi_high -m sub -n ${unique} -t bbbg -u high
##sub_phi_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-Phi_S-BDT.root
##sub_phi_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-Phi_S-BDT.root
##sub_phi_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-Phi_S-BDT.root
##$executable -b Phi -d ${sub_phi_data} -j ${sub_phi_sign} -p ${sub_phi_bckg} -m pos -r ${ranges}



#### PhotonPt
##$executable -i ${data}:data${tree} -b PhotonPt -m sub -n ${unique} -t data
##$executable -i ${sign}:signal${tree} -b PhotonPt -m sub -n ${unique} -t sign
##$executable -i ${bckg}:pp${tree} -b PhotonPt -m sub -n ${unique} -t bckg
##$executable -i ${bbbg}:bb${tree} -b PhotonPt -m sub -n ${unique} -t bbbg
##sub_photonpt_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-PhotonPt_S-BDT.root
##sub_photonpt_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-PhotonPt_S-BDT.root
##sub_photonpt_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-PhotonPt_S-BDT.root
##$executable -b PhotonPt -d ${sub_photonpt_data} -j ${sub_photonpt_sign} -p ${sub_photonpt_bckg} -m pos 
##
#### DiMuonPt
##$executable -i ${data}:data${tree} -b DiMuonPt -m sub -n ${unique} -t data
##$executable -i ${sign}:signal${tree} -b DiMuonPt -m sub -n ${unique} -t sign
##$executable -i ${bckg}:pp${tree} -b DiMuonPt -m sub -n ${unique} -t bckg
##$executable -i ${bbbg}:bb${tree} -b DiMuonPt -m sub -n ${unique} -t bbbg
##sub_dimuonpt_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-DiMuonPt_S-BDT.root
##sub_dimuonpt_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-DiMuonPt_S-BDT.root
##sub_dimuonpt_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-DiMuonPt_S-BDT.root
##$executable -b DiMuonPt -d ${sub_dimuonpt_data} -j ${sub_dimuonpt_sign} -p ${sub_dimuonpt_bckg} -m pos 
##
#### DPhi
##$executable -i ${data}:data${tree} -b DPhi -m sub -n ${unique} -t data
##$executable -i ${sign}:signal${tree} -b DPhi -m sub -n ${unique} -t sign
##$executable -i ${bckg}:pp${tree} -b DPhi -m sub -n ${unique} -t bckg
##$executable -i ${bbbg}:bb${tree} -b DPhi -m sub -n ${unique} -t bbbg
##sub_dphi_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-DPhi_S-BDT.root
##sub_dphi_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-DPhi_S-BDT.root
##sub_dphi_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-DPhi_S-BDT.root
##$executable -b DPhi -d ${sub_dphi_data} -j ${sub_dphi_sign} -p ${sub_dphi_bckg} -m pos 
