#!/bin/bash

unique=sub_sys
executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
mass_sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_sign.root
mass_bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_bckg.root
mass_data=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_data.root
mass_bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_bbbg.root
tau_sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_sign.root
tau_bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_bckg.root
tau_data=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_data.root
tau_bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_base/eval/eval_bbbg.root
mass_tree=_${unique}
tau_tree=_base

mkdir -p ./subtractions_${unique}
cd ./subtractions_${unique}


## qtA
local_unique=DiMuonMass1_Tau0
mkdir -p ./subtractions_${local_unique}/logs
mkdir -p ./subtractions_${local_unique}/eval
range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0:2740.0,3460.0#qtA_15,-10,20#qtB_10,0,20"
$executable -i ${mass_data}:data${mass_tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
$executable -i ${mass_sign}:sign${mass_tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
$executable -i ${mass_bckg}:bckg${mass_tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
sub_qta_data=./subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtA_S-BDT_${local_unique}.root
sub_qta_sign=./subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtA_S-BDT_${local_unique}.root
sub_qta_bckg=./subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtA_S-BDT_${local_unique}.root
$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 


local_unique=DiMuonMass0_Tau1
mkdir -p ./subtractions_${local_unique}/logs
mkdir -p ./subtractions_${local_unique}/eval
range="DiMuonTau_-5.0,0.91:0.91,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
$executable -i ${tau_data}:data${tau_tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
$executable -i ${tau_sign}:sign${tau_tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
$executable -i ${tau_bckg}:bckg${tau_tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
sub_qta_data=./subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtA_S-BDT_${local_unique}.root
sub_qta_sign=./subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtA_S-BDT_${local_unique}.root
sub_qta_bckg=./subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtA_S-BDT_${local_unique}.root
$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 


local_unique=DiMuonMass0_Tau2
mkdir -p ./subtractions_${local_unique}/logs
mkdir -p ./subtractions_${local_unique}/eval
range="DiMuonTau_-5.0,0.81:0.81,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
$executable -i ${tau_data}:data${tau_tree} -b qtA -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
$executable -i ${tau_sign}:sign${tau_tree} -b qtA -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
$executable -i ${tau_bckg}:bckg${tau_tree} -b qtA -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
sub_qta_data=./subtractions_${local_unique}/eval/subtracted_${local_unique}_data_A-qtA_S-BDT_${local_unique}.root
sub_qta_sign=./subtractions_${local_unique}/eval/subtracted_${local_unique}_sign_A-qtA_S-BDT_${local_unique}.root
sub_qta_bckg=./subtractions_${local_unique}/eval/subtracted_${local_unique}_bckg_A-qtA_S-BDT_${local_unique}.root
$executable -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${range} 


## qtB
##local_unique=DiMuonMass1_Tau0
##range="DiMuonMass_2740.0,2920.0:2920.0,3100.0:3100.0,3280.0:3280.0,3460.0:2740.0,3460.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range} 
##
##local_unique=DiMuonMass0_Tau1
##range="DiMuonTau_-5.0,0.91:0.91,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range} 
##
##local_unique=DiMuonMass0_Tau2
##range="DiMuonTau_-5.0,0.81:0.81,15.0:-5.0,15.0#qtA_15,-10,20#qtB_10,0,20"
##$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -u ${local_unique} -r ${range}
##$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -u ${local_unique} -r ${range}
##$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -u ${local_unique} -r ${range}
##sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT_${local_unique}.root
##sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT_${local_unique}.root
##$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${range} 

