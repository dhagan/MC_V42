#!/bin/bash

unique="shiftr1"
executable=/home/atlas/dhagan/analysis/analysisFork/445/Subtraction/build/subtract
sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_sign.root
bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_bckg.root
data=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_data.root
bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/eval/eval_bbbg.root
tree=_${unique}
ranges="qtA_15,-9,21#qtB_10,0,20"

mkdir -p ./subtractions_${unique}
mkdir -p ./subtractions_${unique}/logs
mkdir -p ./subtractions_${unique}/eval
#cd subtractions_${unique}

#### qtA
$executable -i ${data}:data${tree} -b qtA -m sub -n ${unique} -t data -r ${ranges}
$executable -i ${sign}:sign${tree} -b qtA -m sub -n ${unique} -t sign -r ${ranges}
$executable -i ${bckg}:bckg${tree} -b qtA -m sub -n ${unique} -t bckg -r ${ranges}
##$executable -i ${bbbg}:bb${tree} -m sub -n ${unique} -t bbbg
sub_qta_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtA_S-BDT.root
sub_qta_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtA_S-BDT.root
sub_qta_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtA_S-BDT.root
$executable -b qtA -d ${sub_qta_data} -j ${sub_qta_sign} -p ${sub_qta_bckg} -m pos -r ${ranges}

## qtB
$executable -i ${data}:data${tree} -b qtB -m sub -n ${unique} -t data -r ${ranges} 
$executable -i ${sign}:sign${tree} -b qtB -m sub -n ${unique} -t sign -r ${ranges}
$executable -i ${bckg}:bckg${tree} -b qtB -m sub -n ${unique} -t bckg -r ${ranges}
##$executable -i ${bbbg}:bb${tree} -b qtB -m sub -n ${unique} -t bbbg
sub_qtb_data=./subtractions_${unique}/eval/subtracted_${unique}_data_A-qtB_S-BDT.root 
sub_qtb_sign=./subtractions_${unique}/eval/subtracted_${unique}_sign_A-qtB_S-BDT.root
sub_qtb_bckg=./subtractions_${unique}/eval/subtracted_${unique}_bckg_A-qtB_S-BDT.root
$executable -b qtB -d ${sub_qtb_data} -j ${sub_qtb_sign} -p ${sub_qtb_bckg} -m pos -r ${ranges}

