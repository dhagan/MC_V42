#ifndef subtract_hxx
#define subtract_hxx

#include <main.hxx>
#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting_utils.hxx>
#include <process_utils.hxx>
#include <cutflow.hxx>

void subtraction( std::string & file_str, std::string & type, std::string & abin_var, 
                  std::string & spec_var, std::string & extra_var, std::string & selections,
                  std::string & ranges, std::vector<std::string> & mass_vars,
                  std::string & sub_const, std::string & cf_path, std::string & y_values,
                  std::string & unique );

#endif
//void subtraction( subtract_bounds subtract_bounds, subtract_file subtract_file,
//                  std::string & sub_const, TFile * output_file );

