#ifndef zero_hxx
#define zero_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting_utils.hxx>
#include <process_utils.hxx>

void zero( std::string & type, std::string & abin_var, std::string & spec_var, 
           std::string & selections, std::string & ranges, std::string & unique );

#endif
