#ifndef alter_hxx
#define alter_hxx

#include <main.hxx>  
#include <form_slices.hxx>
                     
void alteration( std::string & input_path, std::string & abin_var, std::string & spec_var, 
                 std::string & selections, std::string & ranges, 
                 std::vector< std::string> & mass_vars );

#endif
