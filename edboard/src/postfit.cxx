#include <postfit.hxx>
#include <sys/stat.h>

void postfit( YAML::Node & run_node, variable_set & variables ){


  std::string unique = run_node[ "unique" ].as<std::string>();

  mkdir( "./postfit", 744 );
  chdir( "./postfit" );

  std::string dir = "./" + unique;
  mkdir( dir.c_str(), 744 );
  chdir( dir.c_str() );

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";

  std::string efficiency_path = std::string( std::getenv( "OUT_PATH" ) ) + "/trees/";
  std::string efficiency_unique = run_node["eff_unique"].as<std::string>();
  efficiency_path += efficiency_unique + "/sign_efficiencies_" + efficiency_unique + ".root";

  basic_fileset * fileset = new basic_fileset();
  fileset->load_efficiency_fileset( efficiency_path, "" );

  std::vector< std::string > ana_cut_series = variables.analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names  = variables.analysis_bound.get_series_names();
  

  TH1F * sign = variables.analysis_bound.get_hist();
  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_cut_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    std::string table_path = trex_qta_path + "Tables/";
    YAML::Node table_postfit_result;
    try {
      table_postfit_result = YAML::LoadFile( table_path + "Table_postfit.yaml" );
    } catch ( std::exception & a ) {
      std::cout << " WARNING >>>>>> Fit fails in bin " << ana_idx+1 << std::endl;
      continue; 
    }
    //std::cout << table_path + "Table_postfit.yaml" << std::endl;
    double postfit_table_signal_yield = table_postfit_result[0]["Samples"][0]["Yield"].as<double>();
    double postfit_table_signal_error = table_postfit_result[0]["Samples"][0]["Error"].as<double>();

    sign->SetBinContent( ana_idx+1, postfit_table_signal_yield  );
    sign->SetBinError( ana_idx+1, postfit_table_signal_error );

  }

  prep_style();
  gStyle->SetOptStat( "krimes" );

  sign->SetLineColor( kRed+1 );
  TH1F * sign_noeff = (TH1F *) sign->Clone( "sign_noeff" ); 
  fileset->apply_sign_efficiency( sign, variables ); 

  TF1 * double_gaussian = prep_dg( -10, 20 );
  if ( run_node[ "fit" ] ){
    std::array< double, 5 > fit_initials = run_node["fit"].as<std::array<double,5>>();
    for ( size_t idx = 0; idx < 5; idx++ ){
      double_gaussian->SetParameter(idx, fit_initials[idx] );
    }
  } else {
    align_dg( double_gaussian, sign );
  }
  
  double_gaussian->SetParLimits( 1, 4.5, 5.0 );
  sign->Fit( double_gaussian, "MQ", "", -5, 20.0);
 
  TF1 * inner_gaussian = prep_sg( -10, 20 ) ;
  TF1 * outer_gaussian = prep_sg( -10, 20 ) ;
  inner_gaussian->SetParameter( 0, double_gaussian->GetParameter( 0 ) );
  inner_gaussian->SetParameter( 1, double_gaussian->GetParameter( 1 ) );
  inner_gaussian->SetParameter( 2, double_gaussian->GetParameter( 2 ) );
  outer_gaussian->SetParameter( 0, double_gaussian->GetParameter( 3 ));
  outer_gaussian->SetParameter( 1, double_gaussian->GetParameter( 1 ) );
  outer_gaussian->SetParameter( 2, double_gaussian->GetParameter( 4 ));

  double_gaussian->SetLineColorAlpha( kRed+2, 0.9 );
  inner_gaussian->SetLineColorAlpha( kBlue+1, 1.0 );
  outer_gaussian->SetLineColorAlpha( rapsberry, 1.0 );
  inner_gaussian->SetLineStyle( 2.0 );
  outer_gaussian->SetLineStyle( 2.0 );
  sign->SetLineColor( kBlack );
  sign_noeff->SetLineColor( kBlack );



  TCanvas canv( "fc", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  sign_noeff->Draw( "HIST E1 ");
  hist_prep_axes( sign_noeff );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( sign_noeff, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit", true );
  TLegend * fs1_leg = below_logo_legend();
  fs1_leg->AddEntry( sign_noeff, "Extracted Signal" );
  fs1_leg->Draw();
  TPaveStats * fsstat = make_stats( sign_noeff );
  fsstat->Draw();
  save_and_clear( canv, active_pad, "./" + unique + "_noeff" );


  active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  sign->Draw( "HIST E1 ");
  double_gaussian->Draw( "SAME" );
  inner_gaussian->Draw( "SAME" );
  outer_gaussian->Draw( "SAME" );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( sign, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Postfit, efficiency corrected", true );
  hist_prep_axes( sign );
  TLegend * fs_leg = below_logo_legend();
  fs_leg->AddEntry( sign, "Efficiency Corrected" );
  fs_leg->AddEntry( double_gaussian, "Double Gaussian Fit" );
  fs_leg->Draw();
  TPaveStats * stat = make_stats( sign );
  stat->Draw();
  save_and_clear( canv, active_pad, "./" + unique + "_fit" );

  delete sign;
  delete sign_noeff;
  delete double_gaussian;

  chdir( ".." );
  chdir( ".." );

}