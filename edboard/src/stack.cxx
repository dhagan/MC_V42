
#include <stack.hxx>
#include <sstream>
#include <sys/stat.h>
#include <THStack.h>

enum val_type{ val_sign, val_bckg, val_data, val_yield };
std::unordered_map< val_type, std::string > val_string = { {val_sign, "Samples"}, {val_bckg, "Samples" }, 
                                                           {val_data, "Data"}, { val_yield, "Total" } };
std::unordered_map< val_type, int > val_idx = { {val_sign, 0}, {val_bckg, 1 }, { val_data, 0 }, { val_yield, 0 } };
std::unordered_map< val_type, int > val_color = { {val_sign, kRed+1}, {val_bckg, kBlue+1 }, { val_data, kBlack }, { val_yield, kGreen+1} };
std::unordered_map< val_type, int > val_style= { {val_sign, 1}, {val_bckg, 1 }, { val_data, 1 }, { val_yield, 2 } };
std::unordered_map< val_type, std::string > val_name = { {val_sign, "sign"}, {val_bckg, "bckg" }, 
                                                           {val_data, "data"}, { val_yield, "fit" } };


std::vector< val > create_vals( YAML::Node & run_node, const std::vector<std::string> & val_vars, variable_set & variables ){
  std::vector< val > validations = {};
  std::transform( val_vars.cbegin(), val_vars.cend(), std::back_inserter( validations ),
    [ &variables, &run_node ]( const std::string & variable ){
      return val( variable, run_node, variables );
    }  
  );
  return validations;
}

void fill_val( val & validation, variable_set & variables, const std::string & unique, val_type type ){

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string config_path =  std::string( std::getenv("ANA_IP") ) + "/lh_summary/share/validate";
  
  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();

  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string val_path = trex_path + qta_name + "/" + unique + "/Plots";
    val_path += "/VALIDATION" + std::to_string( validation.index ) + "_postfit.yaml";
    YAML::Node val_node = YAML::LoadFile( val_path );
    std::vector< double > yields = val_node[val_string[type]][val_idx[type]]["Yield"].as<std::vector<double>>();
    std::vector< double > errors;
    TH1F * slice = static_cast<TH1F*>( validation.hist->Clone() );
    slice->Reset();
    if ( type == val_yield ){ errors = val_node[val_string[type]][val_idx[type]]["UncertaintyUp"].as<std::vector<double>>(); }
    for ( int bin_idx = 0; bin_idx < validation.hist->GetNbinsX(); bin_idx++ ){
      validation.hist->AddBinContent( bin_idx+1, yields[bin_idx] );
      slice->SetBinContent( bin_idx+1, yields[bin_idx] );
      if (type == val_yield ){
        validation.hist->SetBinError( bin_idx+1, validation.hist->GetBinError(bin_idx+1) + (errors[bin_idx]*errors[bin_idx]) );
        slice->SetBinError( bin_idx+1, std::sqrt( errors[bin_idx]*errors[bin_idx] ) );
      }
    }
    validation.slices.push_back( slice );
  }
  for ( int bin_idx = 0; bin_idx < validation.hist->GetNbinsX(); bin_idx++ ){
    validation.hist->SetBinError( bin_idx+1, std::sqrt( validation.hist->GetBinError(bin_idx+1) ) );
  }
}

void plot_stack( val & validation, variable_set & variables, const std::string & unique, val_type type ){
  
  
  THStack * val_stack = new THStack( "valstack", "" );
  std::vector<double> analysis_edges = variables.analysis_bound.get_edges();
  TH2F * val_map = new TH2F( "valmap", "", validation.binning.size()-1, &validation.binning[0], 15, &analysis_edges[0] );
  
  int min_bins = 0;
  int max_bins = variables.analysis_bound.get_bins();
  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();

  for ( int ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){
    //val_stack->GetYaxis()->SetRangeUser(1, 10e5 );
    validation.slices.at( ana_idx )->GetYaxis()->SetRangeUser( 1, 10e5 );
    validation.slices.at( ana_idx )->SetLineStyle( 0 );
    validation.slices.at( ana_idx )->SetLineWidth( 0 );
    validation.slices.at( ana_idx )->SetFillStyle( 1001 );
    validation.slices.at( ana_idx )->SetFillColorAlpha( kBlue, 0.7 - abs(ana_idx - 7)*0.05 );
    std::cout << validation.slices.at( ana_idx )->Integral() << " " << 0.7 - abs(ana_idx - 7)*0.05 << std::endl;
    val_stack->Add( validation.slices.at( ana_idx ) );
    for ( size_t val_idx = 1; val_idx <= validation.binning.size()-1; val_idx++ ){
      val_map->SetBinContent( val_idx, ana_idx,validation.slices.at( ana_idx )->GetBinContent( val_idx ) );
    }
  }
 
  TCanvas * val_canv = new TCanvas( "val_canv", "", 200,200, 1000, 1000 );
  val_canv->Divide( 1, 1 );
  TPad * active_pad = static_cast<TPad *>( val_canv->cd(1) );
  val_stack->Draw();
  gStyle->SetHistTopMargin(0);
  active_pad->SetLogy( true );
  val_stack->SetMaximum( 10e4 );
  val_stack->SetMinimum( 1.5 );
  add_atlas_decorations( active_pad, true );
  add_pad_title( active_pad, validation.hist->GetName() );
  std::string output_name = std::string( validation.var_bound.get_name() ) + "_" + unique + "_stack_" + val_name[type];
  active_pad->Update();
  save_and_clear( *val_canv, active_pad, output_name );

  active_pad = static_cast<TPad *>( val_canv->cd(1) );
  val_map->Draw( "COLZ" );
  val_canv->SetLogz( 1 );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( val_map, validation.var_bound.get_ltx(), variables.analysis_bound.get_ltx() );
  hist_prep_axes( val_map );
  val_map->GetYaxis()->SetRangeUser( -10, 20 );
  output_name = std::string( validation.var_bound.get_name() ) + "_" + unique + "_map_" + val_name[type];
  save_and_clear( *val_canv, active_pad, output_name );

  active_pad = static_cast<TPad *>( val_canv->cd(1) );
  TH1D * proj = val_map->ProjectionX();
  proj->Draw( "HIST" );
  proj->SetFillStyle(3244);
  proj->SetLineWidth( 0 );
  proj->SetFillColor( kGreen+1 );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( proj, validation.var_bound.get_ltx(), "Yield");
  hist_prep_axes( proj );
  TLegend * leg = create_atlas_legend();
  leg->AddEntry( proj, "Extracted Yield", "F" );
  leg->Draw();
  proj->GetYaxis()->SetRangeUser(0, 8e4);
  output_name = std::string( validation.var_bound.get_name() ) + "_" + unique + "_proj_" + val_name[type];
  save_and_clear( *val_canv, active_pad, output_name );


}

void stack( YAML::Node & run_node, variable_set & variables ){


  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string output_dir = "./stack_" + unique;
  if ( access( output_dir.c_str(), 744 ) != 0 ){
    mkdir( output_dir.c_str(), 744 );
  }
  if ( chdir( output_dir.c_str() ) == -1 ){
    std::cout << errno << std::endl;
  };

  std::string data_scale = "1.0";
  if ( run_node[ "data_scale" ] ){
    data_scale = run_node["data_scale"].as<std::string>();
  }

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string config_path =  std::string( std::getenv("ANA_IP") ) + "/lh_summary/share/validate";
  std::vector< std::string > val_vars = run_node[ "validation_variables" ].as<std::vector<std::string>>();

  std::string data_weight   = "(subtraction_weight*" + data_scale + ")"
                              "*((Lambda>=25.00000)&&(Lambda<=200.00000)&&(abs(qtB)>=0.00000)&&(abs(qtB)<=6.00000))";
  std::string mc_weight     = "(subtraction_weight*photon_sf*muon_sf*pu_weight)"
                              "*((Lambda>=25.00000)&&(Lambda<=200.00000)&&(abs(qtB)>=0.00000)&&(abs(qtB)<=6.00000))";

  std::string fs_unique = run_node[ "fs_unique" ].as<std::string>();
  std::string fs_path = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + fs_unique;
  basic_fileset fileset( unique ); 
  fileset.load_subtraction_fileset( fs_path, fs_unique, true );

  std::vector< val > data_vals  = create_vals( run_node, val_vars, variables );
  std::vector< val > bckg_vals  = create_vals( run_node, val_vars, variables );

  std::for_each( data_vals.begin(), data_vals.end(), [ &variables, &unique ]( val & validation ){ fill_val( validation, variables, unique, val_data ); } );
  std::for_each( bckg_vals.begin(), bckg_vals.end(), [ &variables, &unique ]( val & validation ){ fill_val( validation, variables, unique, val_bckg ); } );

  std::for_each( data_vals.begin(), data_vals.end(),
    [ &variables, &unique ]( val & validation ){
      plot_stack( validation, variables, unique, val_data );   
    }
  );

  std::for_each( bckg_vals.begin(), bckg_vals.end(),
    [ &variables, &unique ]( val & validation ){
      plot_stack( validation, variables, unique, val_bckg );   
    }
  );

}
