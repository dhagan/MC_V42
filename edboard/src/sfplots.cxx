#include <sfplots.hxx>
#include <sys/stat.h>

void sfplot( YAML::Node & run_node, variable_set & variables ){


  prep_style();

  mkdir( "./photonsf", 744 );
  chdir( "./photonsf" );

  std::string unique = run_node[ "unique" ].as<std::string>();
  

  std::string dir = "./" + unique;
  mkdir( dir.c_str(), 744 );
  chdir( dir.c_str() );

  std::string input_unique = run_node[ "input_unique" ].as<std::string>();
  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string filepath = in_path + "/scalefactors/" + input_unique; 

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( input_unique );
  fileset->load_subtraction_fileset( filepath, input_unique, true );

  bound & phot_bound = variables.bound_manager->get_bound( "PhotonPt" );
  std::vector<std::string> photon_bins = phot_bound.get_cut_series( 25 );

  TH1F * photon_sf = new TH1F( "photosf", "", 20, 5, 25 ); 
  //TH1F * photon_sf_err = new TH1F( "photosf_err", "", 20, 5, 25 ); 

  std::string sf_branch = run_node[ "scalefactor" ].as<std::string>();
  std::string sf_upper_branch = run_node[ "scalefactor_upper" ].as<std::string>();
  std::string sf_expression = sf_branch + ">>sf";
  std::string sf_err_expression = sf_upper_branch + ">>sf_err";

  TTree * sign_tree = fileset->get_tree( sign );

  for ( size_t phot_idx = 0; phot_idx < photon_bins.size(); phot_idx++ ){
    
    std::cout << photon_bins.at( phot_idx) << std::endl;

    TH1F * scalefactor  = new TH1F( "sf", "", 20, 0, 2.0 ); 
    TH1F * scalefactor_err  = new TH1F( "sf_err", "", 20, 0, 2.0 ); 
    sign_tree->Draw( sf_expression.c_str(), photon_bins.at( phot_idx).c_str(), "e goff" );
    sign_tree->Draw( sf_err_expression.c_str(), photon_bins.at( phot_idx).c_str(), "e goff" );
    photon_sf->SetBinContent( phot_idx+1, scalefactor->GetMean() );
    photon_sf->SetBinError( phot_idx+1, abs(scalefactor_err->GetMean() - scalefactor->GetMean() ) );
    //photon_sf_err->SetBinContent( phot_idx+1, scalefactor_err->GetMean() - scalefactor->GetMean() );
    delete scalefactor;
    delete scalefactor_err;
  }

  photon_sf->SetLineColor( kBlack );
  photon_sf->SetLineStyle( 1 );
  photon_sf->SetMarkerColor( kBlack );
  photon_sf->SetMarkerStyle( 20 );


  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  canv.cd( 1 );
  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  photon_sf->Draw( "HIST E1" );
  photon_sf->Draw( "P SAME" );
  photon_sf->Draw( "E1 SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( photon_sf, "p_{T}(#gamma) [GeV]", "Mean #gamma SF" );
  photon_sf->GetYaxis()->SetRangeUser( 0, 2.0 ); 
  save_and_clear( canv, active_pad, "./" + unique + "_photonsf" );

  chdir( ".." );
  chdir( ".." );


}