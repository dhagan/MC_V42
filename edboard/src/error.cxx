#include <sstream>
#include <sys/stat.h>
#include <error.hxx>
#include <stddef.h>
#include <fstream>
#include <iostream>
#include <cctype>
#include <algorithm>

void error( YAML::Node & run_node, variable_set & variables ){

  std::string unique = run_node["unique"].as<std::string>();
  std::string dir_name = "./decomposition";
  mkdir( dir_name.c_str(), 744 );
  chdir( dir_name.c_str() );

  
  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  std::vector< std::string > ana_cut_names  = variables.analysis_bound.get_series_names();

  TH1F * total = variables.analysis_bound.get_hist( "mu_tot" );
  TH1F * systematic = variables.analysis_bound.get_hist( "mu_syst" );

  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){
    
    std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
    const std::string & qta_name = ana_cut_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/Fits/" + unique;
    std::string norm_factor_result = trex_qta_path + ".txt";
    std::string np_decomposition = trex_qta_path + "_errDecomp_mu.txt";

    std::fstream np, nf;
    nf.open( norm_factor_result.c_str(), std::ios::in );
    np.open( np_decomposition, std::ios::in );

    std::string line = " ";
    std::vector<std::string> tokens = {};
    while ( getline( nf, line ) ){
      split_strings( tokens, line, " " );
      if ( tokens.size() == 0 ){ continue; }
      if ( std::regex_match( tokens.at(0), std::regex("^mu.*" ) ) ){
        total->SetBinContent( ana_idx+1, std::stof( tokens.at( 2 ) ) );
        systematic->SetBinContent( ana_idx+1, std::stof( tokens.at( 2 ) ) );
      }
      tokens.clear();
    }
    nf.close();

    line = " ";
    while ( getline( np, line ) ){
      split_strings( tokens, line, "\t" );
      if ( tokens.size() == 0 ){ continue; }
      if ( std::regex_match( line, std::regex("^TOT_ERROR.*" ) ) ){
        total->SetBinError( ana_idx+1, std::stof( tokens.at( 2 ) ) );
      }
      if ( std::regex_match( line, std::regex("^TOTSYST_ERROR.*" ) ) ){
        systematic->SetBinError( ana_idx+1, std::stof( tokens.at( 2 ) ) );
      } 
      tokens.clear();
    }
    np.close();
  
  }

  systematic->SetLineColor( kRed + 1 );
  total->SetLineColor( kBlack );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  systematic->Draw( "HIST E1" );
  total->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( systematic, "q_{T}^{A} [GeV]", "#mu" );
  //hist_prep_axes( systematic, true );
  //systematic->GetYaxis()->SetRangeUser( -1.0, 1.0 );
  active_pad->SetLogy();
  systematic->GetYaxis()->SetRangeUser( 1e-2, 1.0 );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( systematic, "systematic error" );
  legend->AddEntry( total, "total error" );
  legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "./" + unique + "_mu_values" );

  chdir( ".." );
}
