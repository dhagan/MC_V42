#include <sstream>
#include <sys/stat.h>
#include <info.hxx>


void info( YAML::Node & run_node, variable_set & variables ){

  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string output_dir = "./" + unique;
  if ( access( output_dir.c_str(), 744 ) != 0 ){
    mkdir( output_dir.c_str(), 744 );
  }
  if ( chdir( output_dir.c_str() ) == -1 ){
    std::cout << errno << std::endl;
  };

  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string path = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique;
  basic_fileset fileset( unique ); 
  fileset.load_subtraction_fileset( path, unique, true );



}