#include <main.hxx>

#include <modsf.hxx>
#include <yaml-cpp/yaml.h>
#include <stack.hxx>
#include <bdt.hxx>
#include <compare.hxx>
#include <error.hxx>
#include <postfit.hxx>
#include <sfplots.hxx>

int help(){
  std::cout << " Usage:" << std::endl;
  std::cout << " ./gen --input,-i INPUT_PATH" << std::endl;
  std::cout << " " << std::endl;
  return 0;
}
int main( int argc, char * argv[] ){

  prep_style();
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "help",        no_argument,              0,      'h'},
      { "input",       required_argument,        0,      'i'},
      {0,             0,                        0,      0}
    };

  std::string input = "";
  do {
    option = getopt_long(argc, argv, "i:ph", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'i': 
        input         = std::string( optarg );
        break;
    }
  } while (option != -1);

  if ( input.empty() ){
    std::cout << "No input, exiting..." << std::endl;
    return 0;
  }

  YAML::Node config = YAML::LoadFile( input );
  std::vector< std::string > run_list = config["general"]["runs"].as< std::vector<std::string > >();

  for ( std::string & run : run_list ){

    YAML::Node run_node = config[ run ];

    std::vector< std::string > modes = run_node[ "modes" ].as< std::vector< std::string > >();
    if ( modes.size() == 0 ){ std::cout << "No modes provided, exiting..." << std::endl; }

    std::string avar = "qtA", svar = "BDT";
    std::string range = "", bounds_path = "";
    if ( run_node[ "avar" ] ){ avar = run_node["avar"].as< std::string >(); }
    if ( run_node[ "svar" ] ){ svar = run_node["svar"].as< std::string >(); }
    if ( run_node[ "ranges" ] ){ range = run_node["ranges"].as< std::string >(); }
    if ( run_node[ "bounds_file" ] ){ bounds_path = std::string( std::getenv( "LIB_PATH" ) ) 
                                              + "/share/" 
                                              + run_node["bounds_file"].as< std::string >(); }

    std::cout << bounds_path << std::endl;
    variable_set variables = variable_set( bounds_path, range, avar, svar );

    for ( std::string & mode : modes ){

      std::cout << mode << std::endl;
      
      if ( mode.find( "modsf" ) != std::string::npos ){
        modsf( run_node, variables );
        continue;
      }
      if ( mode.find( "stack" ) != std::string::npos ){
        stack( run_node, variables );
        continue;
      }
      if ( mode.find( "bdt" ) != std::string::npos ){
        bdt( run_node );
        continue;
      }
      if ( mode.find( "compare" ) != std::string::npos ){
        compare( run_node, variables );
        continue;
      }
      if ( mode.find( "error" ) != std::string::npos ){
        error( run_node, variables );
        continue;
      }
      if ( mode.find( "postfit" ) != std::string::npos ){
        postfit( run_node, variables );
        continue;
      }
      if ( mode.find( "sfplot" ) != std::string::npos ){
        sfplot( run_node, variables );
        continue;
      }
    }
  }
  return 0;
}
