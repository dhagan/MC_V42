#include <compare.hxx>
#include <sys/stat.h>
#include <fileset.hxx>
#include <utility>


// load the fiducial or efficiency fileset
basic_fileset * load_efficiency( std::string unique, bool fiducial ){
  std::string filepath = std::string( std::getenv( "OUT_PATH" ) )
    + "/trees/" + unique + "/efficiency/sign_";
  filepath += (fiducial) ? "fiducials_" : "efficiencies_";
  filepath += unique + ".root";
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  std::cout << filepath << std::endl;
  fileset->load_efficiency_fileset( filepath, unique, fiducial );
  return fileset;
}


void compare( YAML::Node & run_node, variable_set & variables ){

  // enter output directory
  mkdir( "./compare", 744 );
  chdir( "./compare" );

  // get unique
  std::string first_unique = run_node["first_unique"].as<std::string>();
  std::string second_unique = run_node["second_unique"].as<std::string>();

  // load efficiency files
  //std::string eff_unique = run_node[ "eff_unique" ].as<std::string>();
  //basic_fileset * eff_fileset = load_efficiency( eff_unique, false );

  // get nominal, and statonly yields
  hist_group first_extracted, second_extracted;
  std::tie( std::ignore, first_extracted )= get_fit( first_unique, variables );
  std::tie( std::ignore, second_extracted ) = get_fit( second_unique, variables );

  TH1F * first_hist = first_extracted.sign_hist;
  TH1F * second_hist = second_extracted.sign_hist;

  first_hist->SetLineColor( kRed+2 );
  second_hist->SetLineColor( kBlue+2 );

  TH1F * difference = static_cast<TH1F *>( first_hist->Clone() );
  difference->Reset();
  difference->Add( second_hist, first_hist, 1.0, -1.0 );
  difference->SetLineColor( kBlack );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );

  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  first_hist->Draw( "HIST" );
  second_hist->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( first_hist, "q_{T}^{A} [GeV]", "sign yield" );
  hist_prep_axes( first_hist, true );
  TLegend * ov_legend = below_logo_legend();
  ov_legend->AddEntry( first_hist, first_unique.c_str() );
  ov_legend->AddEntry( second_hist, second_unique.c_str() );
  ov_legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "sign_overlay_" + first_unique + "_" + second_unique );

  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  difference->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( difference, "q_{T}^{A} [GeV]", "sign yield" );
  hist_prep_axes( difference, true );
  difference->GetYaxis()->SetRangeUser( -500, 500 );
  TLegend * dif_legend = below_logo_legend();
  std::string diffstr = second_unique + " - " + first_unique;
  dif_legend->AddEntry( difference, diffstr.c_str()  );
  dif_legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "sign_difference_" + first_unique + "_" + second_unique );


  //delete dif_legend;
  //delete active_pad;
  //delete first_hist;
  //delete second_hist;
  first_extracted.erase();
  second_extracted.erase();
  chdir( ".." );

}