#include <sstream>
#include <sys/stat.h>
#include <bdt.hxx>


void bdt( YAML::Node & run_node ){

  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string path = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique;
  basic_fileset fileset( unique ); 
  fileset.load_subtraction_fileset( path, unique, true );
  std::string output_dir = "./bdt_" + unique;
  if ( access( output_dir.c_str(), 744 ) != 0 ){
    mkdir( output_dir.c_str(), 744 );
  }
  if ( chdir( output_dir.c_str() ) == -1 ){
    std::cout << errno << std::endl;
  };

  TH2F * data = new TH2F( "data", "", 40, -1.0, 1.0, 40, 5, 25 );
  TH2F * sign = new TH2F( "sign", "", 40, -1.0, 1.0, 40, 5, 25 );
  TH2F * bckg = new TH2F( "bckg", "", 40, -1.0, 1.0, 40, 5, 25 );

  fileset.data_tree->Draw( "PhotonPt:BDT>>data", "", "e goff"  );
  fileset.sign_tree->Draw( "PhotonPt:BDT>>sign", "", "e goff"  );
  fileset.bckg_tree->Draw( "PhotonPt:BDT>>bckg", "", "e goff"  );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );

  data->Draw( "COLZ" );
  canv.SetLogz();
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( data, "BDT", "p_{T}(#gamma)" );
  hist_prep_axes( data );
  data->GetYaxis()->SetRangeUser( 5, 25 );
  save_and_clear( canv, active_pad, "data_bdt_photonpt" );

  sign->Draw( "COLZ" );
  canv.SetLogz();
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( sign, "BDT", "p_{T}(#gamma)" );
  hist_prep_axes( sign );
  sign->GetYaxis()->SetRangeUser( 5, 25 );
  save_and_clear( canv, active_pad, "sign_bdt_photonpt" );

  bckg->Draw( "COLZ" );
  canv.SetLogz();
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( bckg, "BDT", "p_{T}(#gamma)" );
  hist_prep_axes( bckg );
  bckg->GetYaxis()->SetRangeUser( 5, 25 );
  save_and_clear( canv, active_pad, "bckg_bdt_photonpt" );

  chdir( ".." );

}