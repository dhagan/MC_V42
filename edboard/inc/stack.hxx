#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef stack_hxx 
#define stack_hxx

void stack( YAML::Node & run_node, variable_set & variables );

struct val{
  int index;
  bound var_bound;
  std::vector<double> binning;
  TH1F * hist;
  std::vector< TH1F * > slices;
  val(){ 
    index = -1;
    binning = {}; }
  val( const std::string & variable, YAML::Node & run_node, variable_set & variables ){
    index = run_node[variable+"_idx"].as<int>();
    var_bound = variables.bound_manager->get_bound( variable );                                       
    binning = run_node[variable+"_arr"].as<std::vector<double>>();                                   
    std::string hist_name = variable + "_" + std::to_string(rand()%100000);
    hist = new TH1F( hist_name.c_str(), "", binning.size()-1, &binning[0] ); 
  }
};

#endif