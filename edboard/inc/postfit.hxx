#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef postfit_hxx 
#define postfit_hxx

void postfit( YAML::Node & run_node, variable_set & variables );

#endif

