#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef info_hxx 
#define info_hxx

void info( YAML::Node & run_node, variable_set & variables );

#endif