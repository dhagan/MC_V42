#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef sfplot_hxx 
#define sfplot_hxx

void sfplot( YAML::Node & run_node, variable_set & variables );

#endif

