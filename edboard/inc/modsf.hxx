#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef modsf_hxx 
#define modsf_hxx

void modsf( YAML::Node & run_node, variable_set & variables );

#endif

