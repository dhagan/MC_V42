#!/bin/bash
yaml="$(pwd)/run.yaml"

mkdir -p ${OUT_PATH}/edboard/${unique}/
pushd ${OUT_PATH}/edboard/${unique}/ >> /dev/null
edboard -i ${yaml}
popd >> /dev/null
