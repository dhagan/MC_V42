cmake_minimum_required(VERSION 3.14)

## Project details
project( edboard )

## Set sources and lib paths.
set( sources src/main.cxx src/funcs.cxx src/modsf.cxx src/stack.cxx src/bdt.cxx 
  src/compare.cxx src/error.cxx src/postfit.cxx src/sfplots.cxx )

## Find root package
find_package( ROOT CONFIG REQUIRED COMPONENTS RooStats RooFit RooFitCore HistFactory ROOTDataFrame MathCore MathMore )

## Prepare executable
add_compile_options( -O2 -std=c++17 -Wall -Wextra -pedantic -Werror -no-pie -static )
add_executable( edboard ${sources} )
target_include_directories( edboard PUBLIC inc ${CMAKE_SOURCE_DIR}/lib/analysis_utils/inc )

## Link libraries
target_link_libraries( edboard PRIVATE ROOT::Core ${ROOT_LIBRARIES} yaml-cpp::yaml-cpp )
target_link_libraries( edboard PUBLIC analysis_utils ${Boost_LIBRARIES} BphysEffTool)

## Enable export.
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
