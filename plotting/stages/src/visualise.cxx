#include <visualise.hxx>

#include <TGaxis.h>

void stage_map( basic_fileset * selection, basic_fileset * subtraction, basic_fileset * histfactory, 
                variable_set & variables, std::string & unique ){

  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();

  TH2F * selection_data = variables.analysis_bound.get_2d_hist( "selection_data", variables.spectator_bound );
  TH2F * selection_sign = variables.analysis_bound.get_2d_hist( "selection_sign", variables.spectator_bound );
  TH2F * selection_bckg = variables.analysis_bound.get_2d_hist( "selection_bckg", variables.spectator_bound );
  TH2F * subtraction_data = variables.analysis_bound.get_2d_hist( "subtraction_data", variables.spectator_bound );
  TH2F * subtraction_sign = variables.analysis_bound.get_2d_hist( "subtraction_sign", variables.spectator_bound );
  TH2F * subtraction_bckg = variables.analysis_bound.get_2d_hist( "subtraction_bckg", variables.spectator_bound );
  TH2F * histfac_data = variables.analysis_bound.get_2d_hist( "histfac_data", variables.spectator_bound );
  TH2F * histfac_sign = variables.analysis_bound.get_2d_hist( "histfac_sign", variables.spectator_bound );
  TH2F * histfac_bckg = variables.analysis_bound.get_2d_hist( "histfac_bckg", variables.spectator_bound );
  if ( variables.spectator_variable.find("BDT") == std::string::npos ){
    selection->data_tree->Draw( Form( "%s:%s>>selection_data", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), mass_cut.c_str() );
    selection->sign_tree->Draw( Form( "%s:%s>>selection_sign", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), mass_cut.c_str() );
    selection->bckg_tree->Draw( Form( "%s:%s>>selection_bckg", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), mass_cut.c_str() );
  }
  subtraction->data_tree->Draw( Form( "%s:%s>>subtraction_data", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), 
                          Form( "subtraction_weight*(%s)", mass_cut.c_str()) );
  subtraction->sign_tree->Draw( Form( "%s:%s>>subtraction_sign", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), 
                          Form( "subtraction_weight*(%s)", mass_cut.c_str()) );
  subtraction->bckg_tree->Draw( Form( "%s:%s>>subtraction_bckg", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), 
                          Form( "subtraction_weight*(%s)", mass_cut.c_str()) );
  histfactory->weighted_tree->Draw( Form( "%s:%s>>histfac_data", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), "subtraction_weight" );
  histfactory->weighted_tree->Draw( Form( "%s:%s>>histfac_sign", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), "subtraction_weight*hf_sign_weight" );
  histfactory->weighted_tree->Draw( Form( "%s:%s>>histfac_bckg", variables.spectator_bound.get_var().c_str(), variables.analysis_bound.get_var().c_str() ), "subtraction_weight*hf_bckg_weight" );

  TCanvas * stage_canvas = new TCanvas( "stage_canv", "", 200, 200, 3000, 3000 );
  stage_canvas->Divide( 3, 3 );

  gStyle->SetOptStat( "miner" );
  TPad * current_pad;

  // data pads, 1, 2, 3
  current_pad = (TPad *) stage_canvas->cd( 1 );
  selection_data->Draw( "COLZ" );
  set_axis_labels( selection_data, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( selection_data );
  add_pad_title( current_pad, Form( "Data after selection" ), false ); 
  TPaveStats * sel_data_stats = make_stats( selection_data );
  sel_data_stats->Draw();

  current_pad = (TPad *) stage_canvas->cd( 2 );
  subtraction_data->Draw( "COLZ" );
  set_axis_labels( subtraction_data, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( subtraction_data );
  add_pad_title( current_pad, Form( "Data after subtraction" ), false ); 
  TPaveStats * sub_data_stats = make_stats( subtraction_data );
  sub_data_stats->Draw();

  current_pad = (TPad *) stage_canvas->cd( 3 );
  histfac_data->Draw( "COLZ" );
  set_axis_labels( histfac_data, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( histfac_data );
  add_pad_title( current_pad, Form( "Data after histfac" ), false ); 
  TPaveStats * hf_data_stats = make_stats( histfac_data );
  hf_data_stats->Draw();


  // sign pads, 4, 5, 6
  current_pad = (TPad *) stage_canvas->cd( 4 );
  selection_sign->Draw( "COLZ" );
  set_axis_labels( selection_sign, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( selection_sign );
  add_pad_title( current_pad, Form( "Signal MC after selection" ), false ); 
  TPaveStats * sel_sign_stats = make_stats( selection_sign );
  sel_sign_stats->Draw();

  current_pad = (TPad *) stage_canvas->cd( 5 );
  subtraction_sign->Draw( "COLZ" );
  set_axis_labels( subtraction_sign, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( subtraction_sign );
  add_pad_title( current_pad, Form( "Signal MC after subtraction" ), false ); 
  TPaveStats * sub_sign_stats = make_stats( subtraction_sign );
  sub_sign_stats->Draw();

  current_pad = (TPad *) stage_canvas->cd( 6 );
  histfac_sign->Draw( "COLZ" );
  set_axis_labels( histfac_sign, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( histfac_sign );
  add_pad_title( current_pad, Form( "Signal Extr after histfac" ), false );
  TPaveStats * hf_sign_stats = make_stats( histfac_sign );
  hf_sign_stats->Draw();

  
  // bckg pads, 7, 8, 9
  current_pad = (TPad *) stage_canvas->cd( 7 );
  selection_bckg->Draw( "COLZ" );
  set_axis_labels( selection_bckg, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( selection_bckg );
  add_pad_title( current_pad, Form( "Background MC after selection" ), false ); 
  TPaveStats * sel_bckg_stats = make_stats( selection_bckg );
  sel_bckg_stats->Draw();


  current_pad = (TPad *) stage_canvas->cd( 8 );
  subtraction_bckg->Draw( "COLZ" );
  set_axis_labels( subtraction_bckg, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( subtraction_bckg );
  add_pad_title( current_pad, Form( "Background MC after subtraction" ), false );
  TPaveStats * sub_bckg_stats = make_stats( subtraction_bckg );
  sub_bckg_stats->Draw();


  current_pad = (TPad *) stage_canvas->cd( 9 );
  histfac_bckg->Draw( "COLZ" );
  set_axis_labels( histfac_bckg, variables.analysis_bound.get_x_str().c_str(), variables.spectator_bound.get_x_str().c_str() );
  hist_prep_text( histfac_bckg );
  add_pad_title( current_pad, Form( "Background Extr after histfac" ), false );
  TPaveStats * hf_bckg_stats = make_stats( histfac_bckg );
  hf_bckg_stats->Draw();


  stage_canvas->SaveAs( Form( "./map_%s_%s_%s.png", variables.analysis_bound.get_var().c_str(), variables.spectator_bound.get_var().c_str(), unique.c_str() ) );

}


void efficiency_plot( basic_fileset * histfactory, variable_set & variables, style_mgr * styles, std::string & unique ){

  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();

  TTree * truth_tree = (TTree *) histfactory->efficiency_file->Get( "truth_tree" );
  TTree * reco_tree =  (TTree *) histfactory->efficiency_file->Get( "reco_tree" );

  TH1F * efficiency_hist = (TH1F *) histfactory->efficiency_file->Get( Form( "eff_%s_Q12", variables.analysis_variable.c_str() ) );
  TH1F * truth_hist = variables.analysis_bound.get_hist( "truth_hist" ); 
  TH1F * reco_hist = variables.analysis_bound.get_hist( "reco_hist" ); 
  truth_tree->Draw( Form( "%s>>truth_hist", variables.analysis_bound.get_var().c_str() ), mass_cut.c_str(), "goff" );
  reco_tree->Draw(  Form( "%s>>reco_hist",  variables.analysis_bound.get_var().c_str() ) , mass_cut.c_str(), "goff" );

  std::vector< float > truth_style = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kRed+1,  0.6, 1001 };
  std::vector< float > reco_style =  { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kBlue+1, 0.6, 1001 };
  std::vector< float > eff_style =   { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0,     0.0, 0.0  };
 
  styles->set_stage( "stages" );
  styles->style_histogram( truth_hist, "eff_truth_histogram" );
  styles->style_histogram( reco_hist, "eff_reco_histogram" );
  styles->style_histogram( efficiency_hist, "efficiency_histogram" );

  int truth_maximum = truth_hist->GetMaximum()*2;
  truth_hist->Scale( 1.0/truth_maximum );
  reco_hist->Scale( 1.0/truth_maximum );    

  TCanvas * efficiency_canvas = new TCanvas( "efficiency_histogram", "", 200, 200, 1000, 1000 );
  efficiency_canvas->Divide( 1, 1 );

  TPad * efficiency_pad = (TPad *) efficiency_canvas->cd( 1 );
  efficiency_hist->Draw( "P E1" );
  hist_prep_axes( efficiency_hist );
  efficiency_hist->GetYaxis()->SetRangeUser( 0, 1 );
  set_axis_labels( efficiency_hist, variables.analysis_bound.get_x_str().c_str(), Form( "Efficiency/%s",variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( efficiency_pad, Form( "Efficiency" ), false);
  add_atlas_decorations( efficiency_pad, true, true );
  truth_hist->Draw("HIST SAME");
  reco_hist->Draw("HIST SAME");
  efficiency_pad->SetTicks(1,0);
  TGaxis * count_axis = new TGaxis( variables.analysis_bound.get_max(), 0, 
                                    variables.analysis_bound.get_max(), 1, 
                                    0, truth_maximum, 510, "+L" );
  count_axis->SetLabelSize(0.035);             
  count_axis->SetLabelFont(42);                
  count_axis->SetTitleOffset(1.3);            
  count_axis->SetTitleFont(42);                
  count_axis->SetTitleSize(0.035);
  count_axis->SetMaxDigits(4);
  count_axis->SetTitle( Form( "Count/%s",variables.analysis_bound.get_y_str().c_str() ) );
  count_axis->Draw();
  TLegend * eff_legend = below_logo_legend();
  eff_legend->AddEntry( efficiency_hist, "Signal Efficiency","PE" );
  eff_legend->AddEntry( truth_hist,"Truth", "F" );
  eff_legend->AddEntry( reco_hist, "Reco","F" );
  eff_legend->Draw();

  efficiency_canvas->SaveAs( Form( "./%s_efficiency_%s.png", unique.c_str(), variables.analysis_variable.c_str() ) );

}

void training( basic_fileset * subtraction, variable_set & variables, style_mgr * styles, std::string & unique ){

  bound score_bound = variables.bound_manager->get_bound( "BDT" );
  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();

  TH1F * sign_score = new TH1F( "sign_score", "", 50, -1.0, 1.0 ); 
  TH1F * bckg_score = new TH1F( "bckg_score", "", 50, -1.0, 1.0 ); 
  subtraction->sign_tree->Draw( "BDT>>sign_score", mass_cut.c_str(), "goff" );
  subtraction->bckg_tree->Draw( "BDT>>bckg_score", mass_cut.c_str(), "goff" );

  sign_score->Scale( 1.0/sign_score->Integral() );
  bckg_score->Scale( 1.0/bckg_score->Integral() );

  styles->style_histogram( sign_score, "extracted_sign" );
  styles->style_histogram( bckg_score, "extracted_bckg" );
  sign_score->SetFillStyle( 0 );
  bckg_score->SetFillStyle( 0 );

  TCanvas * score_canvas = new TCanvas( "score_canvas", "", 200, 200, 1000, 1000 );
  score_canvas->Divide( 1, 1 );

  TPad * score_pad = (TPad *) score_canvas->cd( 1 );

  sign_score->Draw("HIST");
  bckg_score->Draw("HIST SAME");
  hist_prep_axes( sign_score, true );
  set_axis_labels( sign_score, score_bound.get_x_str().c_str(), Form( "Score/%s",score_bound.get_y_str().c_str() ) );
  add_pad_title( score_pad, Form( "Evaluation Score" ), false);
  add_atlas_decorations( score_pad, true, true );
  TLegend * score_legend = below_logo_legend();
  score_legend->AddEntry( sign_score,"Signal", "L" );
  score_legend->AddEntry( bckg_score, "Background","L" );
  score_legend->Draw();
  
  score_canvas->SaveAs( Form( "%s_score.png", unique.c_str() ) );

}

void signal_background( basic_fileset * subtraction, variable_set & variables, style_mgr * styles, std::string & unique ){

  bound qtb_bound = variables.bound_manager->get_bound( "qtB" );
  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();

  TH1F * sign_qtb = qtb_bound.get_hist( "sign_qtb", 50 );
  TH1F * bckg_qtb = qtb_bound.get_hist( "bckg_qtb", 50 );
  subtraction->sign_tree->Draw( "qtB>>sign_qtb", mass_cut.c_str(), "goff" );
  subtraction->bckg_tree->Draw( "qtB>>bckg_qtb", mass_cut.c_str(), "goff" );

  sign_qtb->Scale( 1.0/sign_qtb->Integral() );
  bckg_qtb->Scale( 1.0/bckg_qtb->Integral() );

  styles->set_stage( "default" );
  styles->style_histogram( sign_qtb, "extracted_sign" );
  styles->style_histogram( bckg_qtb, "extracted_bckg" );
  sign_qtb->SetFillStyle( 0 );
  bckg_qtb->SetFillStyle( 0 );

  TCanvas * qtb_canvas = new TCanvas( "qtb_canvas", "", 200, 200, 1000, 1000 );
  qtb_canvas->Divide( 1, 1 );

  gStyle->SetOptStat( "krimes" );
  
  TPad * qtb_pad = (TPad *) qtb_canvas->cd( 1 );
  TH1F * ratio = ratio_pad( sign_qtb, bckg_qtb, qtb_pad );
  sign_qtb->GetYaxis()->SetRangeUser( 0, 0.095 );
  ratio->GetXaxis()->SetTitle( "q_{T}^{B} [GeV]" );
  qtb_pad = (TPad *) qtb_canvas->cd( 1 );
  add_atlas_decorations( qtb_pad, true, true );
  set_axis_labels( sign_qtb, qtb_bound.get_x_str( ).c_str(), Form( "Yield/%s",qtb_bound.get_y_str( 50 ).c_str() ) );
  add_pad_title( qtb_pad, Form( "Normalised q_{T}^{B}" ), false );
  TLegend * qtb_legend = below_logo_legend();
  qtb_legend->AddEntry( sign_qtb,"Signal", "L" );
  qtb_legend->AddEntry( bckg_qtb, "Background","L" );
  qtb_legend->AddEntry( ratio, "Ratio","L" );
  qtb_legend->Draw();

  qtb_canvas->SaveAs( Form( "%s_qtb.png", unique.c_str() ) );


}

void bdt_plots( TFile * bdt, variable_set & variables, style_mgr * styles, std::string & unique ){

  bound score_bound = variables.bound_manager->get_bound( "BDT" );

  TDirectory * dataloader = (TDirectory *) bdt->GetDirectory( "tmva_dataloader" );
  TDirectory * method_BDT = (TDirectory *) dataloader->GetDirectory( "Method_BDT" );
  TDirectory * BDT = (TDirectory *) method_BDT->GetDirectory( "BDT" );

  TH1D * training_sign = (TH1D *) BDT->Get( "MVA_BDT_Train_S" );
  TH1D * training_bckg = (TH1D *) BDT->Get( "MVA_BDT_Train_B" );
  TH1D * test_sign = (TH1D *) BDT->Get( "MVA_BDT_S" );
  TH1D * test_bckg = (TH1D *) BDT->Get( "MVA_BDT_B" ); 

  styles->set_stage( "default" );
  styles->style_histogram( training_sign, "extracted_sign" );
  styles->style_histogram( training_bckg, "extracted_bckg" );
  styles->style_histogram( test_sign, "extracted_sign" );
  styles->style_histogram( test_bckg, "extracted_bckg" );
  training_sign->SetFillStyle( 0 );
  training_bckg->SetFillStyle( 0 );
  test_sign->SetFillStyle( 0 );
  test_bckg->SetFillStyle( 0 );
  training_sign->SetLineStyle( 0 );
  training_bckg->SetLineStyle( 0 );
  test_sign->SetLineStyle( 2 );
  test_bckg->SetLineStyle( 2 );

  gStyle->SetOptStat( "krimes" );
  TCanvas * test_train_canvas = new TCanvas( "test_train_canvas", "", 200, 200, 2000, 1000 );
  test_train_canvas->Divide( 2, 1 );
  TPad * signal_pad = (TPad *) test_train_canvas->cd( 1 );
  TH1F * signal_ratio = ratio_pad( test_sign, training_sign, signal_pad );
  signal_pad = (TPad *) test_train_canvas->cd( 1 );
  test_sign->GetYaxis()->SetRangeUser( 0, 4 );
  add_atlas_decorations( signal_pad, true, true );
  set_axis_labels( test_sign, score_bound.get_x_str( ).c_str(), Form( "Yield/%s", score_bound.get_y_str( 40 ).c_str() ) );
  add_pad_title( signal_pad, "Signal BDT Score", false );
  TLegend * signal_legend = below_logo_legend();
  signal_legend->AddEntry( test_sign,"Testing", "L" );
  signal_legend->AddEntry( training_sign,"Training","L" );
  signal_legend->AddEntry( signal_ratio, "Ratio","L" );
  signal_legend->Draw();

  TPad * background_pad = (TPad *) test_train_canvas->cd( 2 );
  TH1F * background_ratio = ratio_pad( test_bckg, training_bckg, background_pad );
  background_pad = (TPad *) test_train_canvas->cd( 2 );
  add_atlas_decorations( background_pad, true, true );
  set_axis_labels( test_bckg, score_bound.get_x_str().c_str(), Form( "Yield/%s", score_bound.get_y_str( 40 ).c_str() ) );
  add_pad_title( background_pad, "Background BDT Score", false );
  TLegend * background_legend = below_logo_legend();
  background_legend->AddEntry( test_bckg,"Testing", "L" );
  background_legend->AddEntry( training_bckg,"Training","L" );
  background_legend->AddEntry( background_ratio, "Ratio","L" );
  background_legend->Draw();

  test_train_canvas->SaveAs( Form( "train_test_score_%s.png", unique.c_str() ) );

}

void bbbg_sub_proof( basic_fileset * subtraction, variable_set & variables, style_mgr * styles, std::string & unique ){

  bound qta_bound = variables.bound_manager->get_bound( "qtA" );
  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();
  
  TH1F * presub_qta = qta_bound.get_hist( "presub_qta", 50 );
  TH1F * postsub_qta = qta_bound.get_hist( "postsub_qta", 50 );
  subtraction->bbbg_tree->Draw( "qtA>>presub_qta", mass_cut.c_str(), "goff" );
  subtraction->bbbg_tree->Draw( "qtA>>postsub_qta", Form( "subtraction_weight*(%s)", mass_cut.c_str() ), "goff" );

  styles->set_stage( "default" );
  styles->style_histogram( presub_qta, "raw_data" );
  styles->style_histogram( postsub_qta, "extracted_sign" );
  presub_qta->SetFillStyle( 0 );
  postsub_qta->SetFillStyle( 0 );
  presub_qta->SetLineStyle( 1 );


  TCanvas * sub_canvas = new TCanvas( "sub_canvas", "", 200, 200, 1000, 1000 );
  sub_canvas->Divide( 1, 1 );

  TPad * sub_pad = (TPad *) sub_canvas->cd( 1 );
  presub_qta->Draw( "HIST E1" );
  postsub_qta->Draw( "HIST E1 SAME" );
  hist_prep_axes( presub_qta, true );
  set_axis_labels( presub_qta, qta_bound.get_x_str(  ).c_str(), Form( "Score/%s",qta_bound.get_y_str( 50 ).c_str() ) );
  add_pad_title( sub_pad, Form( "Non-Prompt Subtraction Effect" ), true );
  add_atlas_decorations( sub_pad, true, true );
  TLegend * sub_legend = below_logo_legend();
  sub_legend->AddEntry( presub_qta,"Before Subtraction", "L" );
  sub_legend->AddEntry( postsub_qta,"After Subtraction", "L" );
  sub_legend->Draw();
  sub_canvas->SaveAs( Form( "%s_sub.png", unique.c_str() ) );

}

void dz02( basic_fileset * subtraction, variable_set & variables, style_mgr * styles, std::string & unique ){

  bound dz_bound = variables.bound_manager->get_bound( "DeltaZ0" );
  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();
  
  TH1F * sign_dz = new TH1F( "dz", "", 50, -1, 1 );
  subtraction->sign_tree->Draw( "DeltaZ0>>dz", mass_cut.c_str(), "goff" );

  styles->set_stage( "default" );
  styles->style_histogram( sign_dz, "raw_data" );
  sign_dz->SetFillStyle( 0 );
  sign_dz->SetLineStyle( 1 );


  TCanvas * sub_canvas = new TCanvas( "sub_canvas", "", 200, 200, 1000, 1000 );
  sub_canvas->Divide( 1, 1 );

  TPad * sub_pad = (TPad *) sub_canvas->cd( 1 );
  sign_dz->Draw( "HIST E1" );
  hist_prep_axes( sign_dz, true );
  set_axis_labels( sign_dz, dz_bound.get_x_str( ).c_str(), Form( "Count/%s", dz_bound.get_y_str( 250 ).c_str() ) );
  add_pad_title( sub_pad, Form( "DeltaZ0" ), true );
  add_atlas_decorations( sub_pad, true, true );
  TLegend * sub_legend = below_logo_legend();
  sub_legend->AddEntry( sign_dz,"Signal", "L" );
  sub_legend->Draw();
  sub_canvas->SaveAs( Form( "%s_dz.png", unique.c_str() ) );

}

void bjorken_map( basic_fileset * histfactory, variable_set & variables, std::string & unique ){

  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();
  TTree * tree = (TTree *) histfactory->efficiency_file->Get( "reco_tree" );

  double bin_edges[14] = { 5.62e-5, 1e-4, 1.778e-4, 3.16e-4, 5.62e-4, 1e-3, 1.778e-3, 3.16e-3, 5.62e-3, 1e-2, 1.778e-2, 3.16e-2, 5.62e-2, 1.0e-1 };

  TFile * file = new TFile( "generic.root", "RECREATE" );

  TH2F * compare_bjorken = new TH2F( "compare_bjorken", "", 13, bin_edges, 13, bin_edges );
  TH2F * bjorken_components = new TH2F( "bjorken_components", "", 30, -3, 3, 60, 0.0005, 0.004 );
  tree->Draw( "Q/13000:Y>>bjorken_components", "abs(Y)<2.5&&(DiMuonMass>3000&&DiMuonMass<3200)" );
  tree->Draw( "(Q/13000)*exp(Y):(Q/13000)*exp(-Y)>>compare_bjorken", "abs(Y)<2.5&&(DiMuonMass>3000&&DiMuonMass<3200)" );
  TH1D * rapidity = bjorken_components->ProjectionX();
  TH1D * qs = bjorken_components->ProjectionY();


  TCanvas * bjorken_canvas = new TCanvas( "bjorken_canvas", "", 200, 200, 2000, 2000 );
  bjorken_canvas->Divide( 2, 2 );

  TPad * bjorken_pad = (TPad *) bjorken_canvas->cd( 1 );
  compare_bjorken->Draw( "COLZ" );
  bjorken_pad->SetLogx();
  bjorken_pad->SetLogy();
  hist_prep_axes( compare_bjorken, true );
  add_pad_title( bjorken_pad, "Bjorken-x" );
  set_axis_labels( compare_bjorken, "x_{2}", "x_{1}" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * bjorken_stats = make_stats( compare_bjorken, false, false );
  bjorken_stats->Draw();


  bjorken_pad = (TPad *) bjorken_canvas->cd( 2 );
  bjorken_components->Draw( "COLZ" );
  hist_prep_axes( bjorken_components, true );
  add_pad_title( bjorken_pad, "Bjorken-x components" );
  set_axis_labels( bjorken_components, "y", "Q/#sqrt{s}" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * component_stats = make_stats( bjorken_components, false, false );
  component_stats->Draw();
  
  
  bjorken_pad = (TPad *) bjorken_canvas->cd( 3 );
  rapidity->Draw( "HIST" );
  hist_prep_axes( rapidity, true );
  add_pad_title( bjorken_pad, "Rapidity" );
  set_axis_labels( rapidity, "y", "Yield" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * rapidity_stats = make_stats( rapidity, false, false );
  rapidity_stats->Draw();

  bjorken_pad = (TPad *) bjorken_canvas->cd( 4 );
  qs->Draw( "HIST" );
  hist_prep_axes( qs, true );
  add_pad_title( bjorken_pad, "Q/#sqrt{s}" );
  set_axis_labels( qs, "Q/#sqrt{s}", "Yield" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * qs_stats = make_stats( qs, false, false );
  qs_stats->Draw();



  bjorken_canvas->SaveAs( Form( "%s_bjorken.png", unique.c_str() ) );
  
  file->cd();
  compare_bjorken->Write();
  bjorken_components->Write();
  rapidity->Write();
  qs->Write();
  file->Close();




}

void visualise( basic_fileset * selection, TFile * bdt, basic_fileset * subtraction, basic_fileset * histfactory, 
                variable_set & variables, std::string & unique ){

  prep_style();
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();
  gStyle->SetOptStat( "kourmenis" );

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "default" );

  stage_map( selection, subtraction, histfactory, variables, unique );
  efficiency_plot( histfactory, variables, styles, unique );
  training( subtraction, variables, styles, unique );
  signal_background( subtraction, variables, styles, unique  );
  bdt_plots( bdt, variables, styles, unique );
  bbbg_sub_proof( subtraction, variables, styles, unique );
  dz02( subtraction, variables, styles, unique );
  bjorken_map( histfactory, variables, unique );
  //qtb_decline( subtraction, variables, unique );


  return;
}




