#include <main.hxx>
#include <visualise.hxx>

int help(){
  std::cout << "Supply 3 files for stage 1, sign, bckg, and data." << std::endl; 
  std::cout << "Supply 3 files for stage 2, sign, bckg, and data." << std::endl;
  std::cout << "Supply 1 files for stage 3, output data file from histfactory." << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"stage1",      required_argument,        0,      'a'},
      {"stage2",      required_argument,        0,      'b'},
      {"stage3",      required_argument,        0,      'c'},
      {"var",         required_argument,        0,      's'},
      {"unique",      required_argument,        0,      'u'},
      {"selections",  required_argument,        0,      'v'},
      {"range",       required_argument,        0,      'r'},
      {"mode",        required_argument,        0,      'm'},
      {0,             0,                        0,      0}
    };

  std::string trees_path, subtraction_path, histfactory_path, efficiency_path, bdt_path;
  std::string analysis_variable, spectator_variable;
  std::string unique, selections_file, ranges;
  std::string mode;

  do {
    option = getopt_long( argc, argv, "a:s:v:r:i:c:e:b:t:u:m:h", long_options, &option_index);
    switch (option){
      // variables
      case 'a': 
        analysis_variable        = std::string( optarg );
        break;
      case 's': 
        spectator_variable        = std::string( optarg );
        break;
      case 'v':
        selections_file = std::string( optarg );
        break;
      case 'r':
        ranges      = std::string( optarg );
        break;

      //input paths
      case 'i': 
        histfactory_path = std::string( optarg );
        break;
      case 'c': 
        subtraction_path = std::string( optarg );
        break;
      case 'e':
        efficiency_path = std::string( optarg );
        break;
      case 'b':
        bdt_path = std::string( optarg );
        break;
      case 't':
        trees_path = std::string( optarg );
        break;

      // others
      case 'u':
        unique      = std::string( optarg );
        break;
      case 'm':
        mode    = std::string( optarg );
        break;
      case 'h':
        return help();
    }
  } while (option != -1);



  variable_set variables( selections_file, ranges, analysis_variable, spectator_variable );
  basic_fileset * histfactory = new basic_fileset();
  if ( !histfactory_path.empty() ){ 
    histfactory->load_final_fileset( histfactory_path, efficiency_path, unique );
    
  }

  std::vector< std::string > filepath_vec;
  basic_fileset * subtraction = new basic_fileset();
  basic_fileset * selection = new basic_fileset();

  split_strings( filepath_vec, trees_path, ":" );
  selection->data_file = new TFile( filepath_vec.at( 0 ).c_str(), "READ" );
  selection->sign_file = new TFile( filepath_vec.at( 1 ).c_str(), "READ" );
  selection->bckg_file = new TFile( filepath_vec.at( 2 ).c_str(), "READ" );
  selection->data_tree = (TTree *) selection->data_file->Get( "tree" );
  selection->sign_tree = (TTree *) selection->sign_file->Get( "tree" );
  selection->bckg_tree = (TTree *) selection->bckg_file->Get( "tree" );
  selection->efficiency_file = new TFile( efficiency_path.c_str(), "READ" );


  filepath_vec.clear();
  split_strings( filepath_vec, subtraction_path, ":" );
  subtraction->data_file = new TFile( filepath_vec.at( 0 ).c_str(), "READ" );
  subtraction->sign_file = new TFile( filepath_vec.at( 1 ).c_str(), "READ" );
  subtraction->bckg_file = new TFile( filepath_vec.at( 2 ).c_str(), "READ" );
  subtraction->bbbg_file = new TFile( filepath_vec.at( 3 ).c_str(), "READ" );
  subtraction->data_tree = (TTree *) subtraction->data_file->Get( "tree" );
  subtraction->sign_tree = (TTree *) subtraction->sign_file->Get( "tree" );
  subtraction->bckg_tree = (TTree *) subtraction->bckg_file->Get( "tree" );
  subtraction->bbbg_tree = (TTree *) subtraction->bbbg_file->Get( "tree" );
  subtraction->efficiency_file = new TFile( efficiency_path.c_str(), "READ" );

  TFile * bdt = new TFile( bdt_path.c_str(), "READ" );

 
  visualise( selection, bdt, subtraction, histfactory, variables, unique );


}
