executable=stages
selections=${LIB_PATH}/share/hf_bounds.txt
unique=base
log=${LOG_PATH}/stages/${unique}.txt
sel_data="${OUT_PATH}/trees/gen_base/data/data_base.root"
sel_sign="${OUT_PATH}/trees/gen_base/sign/sign_base.root"
sel_bckg="${OUT_PATH}/trees/gen_base/bckg/bckg_base.root"
sub_data="${OUT_PATH}/event_subtraction/base/data_base.root"
sub_sign="${OUT_PATH}/event_subtraction/base/sign_base.root"
sub_bckg="${OUT_PATH}/event_subtraction/base/bckg_base.root"
sub_bbbg="${OUT_PATH}/event_subtraction/base/bbbg_base.root"
bdt="${OUT_PATH}/bdt/base/tmva_train_base.root"
histfactory="${OUT_PATH}/event_hf/base/eval/"
efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/stages/${unique}

pushd ${OUT_PATH}/stages/${unique} >> /dev/null

##touch ${log}
${executable} -t "${sel_data}:${sel_sign}:${sel_bckg}" -c "${sub_data}:${sub_sign}:${sub_bckg}:${sub_bbbg}" -i "${histfactory}" -e "${efficiency}" -v ${selections} -u ${unique} -b ${bdt} -a "qtA" -s "BDT"


popd >> /dev/null
