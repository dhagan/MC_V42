executable=${ANA_IP}/plotting/stages/build/stages
selections=${LIB_PATH}/share/hf_bounds.txt

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )


for index in {0..7}; do

	number=$(($index + 1))
	output_unique="qtbsplit-${number}"
	range="${ranges_list[$index]}"

	sel_data="${OUT_PATH}/trees/${output_unique}/data/data_${output_unique}.root"
	sel_sign="${OUT_PATH}/trees/${output_unique}/sign/sign_${output_unique}.root"
	sel_bckg="${OUT_PATH}/trees/${output_unique}/bckg/bckg_${output_unique}.root"
	sub_data="${OUT_PATH}/event_subtraction/${output_unique}/data_${output_unique}.root"
	sub_sign="${OUT_PATH}/event_subtraction/${output_unique}/sign_${output_unique}.root"
	sub_bckg="${OUT_PATH}/event_subtraction/${output_unique}/bckg_${output_unique}.root"
	sub_bbbg="${OUT_PATH}/event_subtraction/${output_unique}/bbbg_${output_unique}.root"
	bdt="${OUT_PATH}/bdt/base/tmva_train_base.root"
	histfactory="${OUT_PATH}/event_hf/${output_unique}/eval/"
	efficiency="${OUT_PATH}/trees/${output_unique}/efficiency/sign_efficiencies_${output_unique}.root"

	mkdir -p ${OUT_PATH}/stages/${output_unique}

	pushd ${OUT_PATH}/stages/${output_unique} >> /dev/null

	${executable} -t "${sel_data}:${sel_sign}:${sel_bckg}" -c "${sub_data}:${sub_sign}:${sub_bckg}:${sub_bbbg}" -i "${histfactory}" -e "${efficiency}" -v ${selections} -u ${output_unique} -b ${bdt} -a "qtA" -s "BDT" -r ${range}

	popd >> /dev/null
done
