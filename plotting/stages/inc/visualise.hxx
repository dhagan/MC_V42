#ifndef map_hxx
#define map_hxx

#include <anna.hxx>
#include <main.hxx>


void stage_map( basic_fileset * selection, basic_fileset * subtraction, basic_fileset * histfactory, 
                variable_set & variables, std::string & unique );

void visualise( basic_fileset * selection, TFile * bdt, basic_fileset * subtraction, basic_fileset * histfactory, 
                variable_set & variables, std::string & unique );
#endif
