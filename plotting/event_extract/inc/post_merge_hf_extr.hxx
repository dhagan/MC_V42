#ifndef post_hf_extr_hxx
#define post_hf_extr_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>

#include <THStack.h>

#include <hf_tables_maps.hxx>

//extern "C" class fit_results : public TObject{
//
//  public:
//    fit_results(){};
//    ~fit_results(){};
//    double mu_value, mu_error;
//    double spp_value, spp_error;
//    double sign, sign_error;
//    double bckg, bckg_error;
//    double data, data_error;
//    double sign_fit, sign_fit_error;
//    double bckg_fit, bckg_fit_error;
//    double data_fit, data_fit_error;
//    double rho, chi2_v1, chi2_v2;
//		ClassDef( fit_results, 3 );
//};

void post_hf_extr( std::string & input, std::string & abin_var, std::string & spec_var,
                   std::string & type, bound_mgr * selections, std::string unique,
                   std::string & efficiency, bool t_ranges, std::string & stats_filepath );

#endif
