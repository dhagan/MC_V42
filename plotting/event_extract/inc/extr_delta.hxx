#ifndef extr_delta_hxx
#define extr_delta_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>

void extr_delta( std::string & input, std::string & abin_var, std::string & spec_var,
                bound_mgr * selections, std::string & uniques );

#endif
