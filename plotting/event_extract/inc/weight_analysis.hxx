#ifndef weight_analysis_hxx
#define weight_analysis_hxx


#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>
#include <fit_mgr.hxx>
#include <fileset.hxx>
#include <variable_set.hxx>
#include <fit_results.hxx>

#include <THStack.h>

#include <hf_tables_maps.hxx>
#include <hf_extr_plotters.hxx>


void weight_analysis( basic_fileset * fileset, variable_set & variables, style_mgr * styles, const std::string & check_variable  );

void weight_analysis_combo( std::vector< basic_fileset *> & splits, variable_set & variables, style_mgr * styles, const std::string & check_variable, const std::string & unique );

#endif
