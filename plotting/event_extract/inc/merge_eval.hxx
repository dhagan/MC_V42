#ifndef merge_extr_hxx
#define merge_extr_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>
#include <fit_mgr.hxx>
#include <fileset.hxx>
#include <variable_set.hxx>
#include <fit_results.hxx>

#include <THStack.h>

#include <hf_tables_maps.hxx>
#include <hf_extr_plotters.hxx>

void merge_split_eval( basic_fileset * merge, std::vector< basic_fileset *> splits,  variable_set & variables, const std::string & unique );

#endif
