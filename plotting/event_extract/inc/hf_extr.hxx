#ifndef hf_extr_hxx
#define hf_extr_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>
#include <fit_mgr.hxx>
#include <fileset.hxx>
#include <variable_set.hxx>
#include <fit_results.hxx>

#include <THStack.h>

#include <hf_tables_maps.hxx>
#include <hf_extr_plotters.hxx>

void hf_extr( basic_fileset & input, variable_set & variables );

#endif
