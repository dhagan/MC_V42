#ifndef hf_tables_maps_hxx
#define hf_tables_maps_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>
#include <fit_results.hxx>


#include <iostream>
#include <fstream>

void generate_weight_maps( bound & analysis_bound, bound & spectator_bound, TDirectoryFile * stats_file, std::string & unique, TFile * output_file );
void produce_table( TTree * stats_tree );

#endif
