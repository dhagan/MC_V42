#ifndef extr_cmp_hxx
#define extr_cmp_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>

#include <THStack.h>

void extr_compare( std::string & input, std::string & abin_var, std::string & spec_var,
                   bound_mgr * selections, std::string unique );


#endif
