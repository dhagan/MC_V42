
#ifndef hf_extr_plotters_hxx
#define hf_extr_plotters_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>
#include <fit_mgr.hxx>
#include <fileset.hxx>
#include <variable_set.hxx>
#include <fit_results.hxx>

#include <THStack.h>

void fit_process( TH1F * sign_hist, TH1F * bckg_hist, const char * unique, variable_set & variables );
void sign_extraction( TH1F * sign_extracted, const char * unique, variable_set & variables );
void bckg_extraction( TH1F * bckg_extracted, const char * unique, variable_set & variables );
void shape_extraction( TH1F * sign_extracted_normalised, TH1F * bckg_extracted_normalised, const char * unique, variable_set & variables );
void data_comparison( TH1F * sign_extracted, TH1F * bckg_extracted, TH1F * data_extracted, THStack * sign_bckg_stack, TH1F * data_raw, const char * unique, variable_set & variables );
void mc_shape_comparison( TH1F * sign_extracted_normalised, TH1F * sign_mc, TH1F * bckg_extracted_normalised, TH1F * bckg_mc, const char * unique, variable_set & variables );
void ratio_plots( TH1F * sign_extracted_normalised, TH1F * sign_mc, TH1F * bckg_extracted_normalised, TH1F * bckg_mc, const char * unique, variable_set & variables );
void extraction_method_comparison( TH1F * weight_hist_in, TH1F * differential_hist_in, variable_set & variables, const char * unique, const char * first="first", const char * second="second" );
void extraction_details( basic_fileset & input, variable_set & variables );
void jpsi_gamma( basic_fileset & input, variable_set & variables );
void delta_map( basic_fileset & input, variable_set & variables, style_mgr * styles );
#endif

