#ifndef split_extr_hxx
#define split_extr_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>
#include <fit_mgr.hxx>
#include <fileset.hxx>
#include <variable_set.hxx>
#include <fit_results.hxx>

#include <THStack.h>

#include <hf_tables_maps.hxx>
#include <hf_extr_plotters.hxx>

void split_extr( std::vector< basic_fileset *> splits,  variable_set & variables, const std::string & unique );

#endif
