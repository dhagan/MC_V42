#ifndef extr_hxx
#define extr_hxx

#include <common.hxx>
#include <bound_mgr.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>

void extr( std::string & input, std::string & abin_var, std::string & spec_var,
           std::string & type, bound_mgr * selections, std::string unique, bool t_ranges, 
           bool pos_mode );

#endif
