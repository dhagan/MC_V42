executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique=srcB
input="${OUT_PATH}/event_hf/qtb/qtb_Q12_output.root"
sign="${OUT_PATH}/event_subtraction/base/sign_base.root"
bckg="${OUT_PATH}/event_subtraction/base/bckg_base.root"
stats_path="${OUT_PATH}/event_hf/qtb/eval/hf_fit_qtb_A-qtB_S-BDT.root"
log=${LOG_PATH}/extract/${unique}.qtb

efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input}:${sign}:${bckg}" -v ${selections} -u ${unique} -e ${efficiency} -a "qtA" -s "BDT" -b ${stats_path} -f
##${executable} -i "${input}" -v ${selections} -u ${unique} -e ${efficiency} -a "DiMuonPtGeV" -s "BDT" -t "sign" -f
##${executable} -i "${input}" -v ${selections} -u ${unique} -e ${efficiency} -a "PhotonPtGeV" -s "BDT" -t "sign" -f
##${executable} -i "${input}" -v ${selections} -u ${unique} -e ${efficiency} -a "qtB" -s "BDT" -t "sign" -f

popd >> /dev/null
