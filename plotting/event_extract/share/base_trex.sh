
executable=${exec_path}/event_extract
selections=${LIB_PATH}/share/hf_bounds.txt
unique="base"

hf_path="${OUT_PATH}/event_hf/"
efficiency="${OUT_PATH}/trees/"
sub="${OUT_PATH}/event_subtraction/"
trex_path="${OUT_PATH}/trex/${unique}/trex_processed_base.root"


mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

${executable} -i "${hf_path}:${efficiency}:${sub}" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -T ${trex_path} -m "trex"

popd >> /dev/null
