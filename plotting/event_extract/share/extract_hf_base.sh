##executable=${ANA_IP}/plotting/event_extract/build/extr
executable="${exec_path}/event_extract"
selections=${LIB_PATH}/share/hf_bounds.txt
unique="base"
log=${LOG_PATH}/extract/${unique}.txt
input="${OUT_PATH}/event_hf/base/eval/base_Q12_output.root"
stats_path="${OUT_PATH}/event_hf/base/eval/hf_chi2_base_A-qtA_S-BDT.root"
sign="${OUT_PATH}/event_subtraction/base/sign_base.root"
bckg="${OUT_PATH}/event_subtraction/base/bckg_base.root"
efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${OUT_PATH}/event_hf/:${OUT_PATH}/trees/:${OUT_PATH}/event_subtraction/" -v ${selections} -u ${unique} -e ${efficiency} -a "qtA" -s "BDT" -t "sign" -m "hf"
##${executable} -i "${input}" -v ${selections} -u ${unique} -e ${efficiency} -a "DiMuonPtGeV" -s "BDT" -t "sign" -f
##${executable} -i "${input}" -v ${selections} -u ${unique} -e ${efficiency} -a "PhotonPtGeV" -s "BDT" -t "sign" -f
##${executable} -i "${input}" -v ${selections} -u ${unique} -e ${efficiency} -a "qtB" -s "BDT" -t "sign" -f


popd >> /dev/null
