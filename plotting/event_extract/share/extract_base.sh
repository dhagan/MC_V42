executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/selection_bounds.txt
unique=base
log=${LOG_PATH}/extract/${unique}.txt
input="${OUT_PATH}/event_subtraction/${unique}/sign_base.root"
##efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input}" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -t "sign"

popd >> /dev/null
