executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique="qtb_upper5"
log=${LOG_PATH}/extract/${unique}.txt
input="${OUT_PATH}/event_hf/${unique}/eval/${unique}_Q12_output.root"
stats_path="${OUT_PATH}/event_hf/${unique}/eval/hf_chi2_${unique}_A-qtA_S-BDT.root"
sign="${OUT_PATH}/event_subtraction/${unique}/sign_${unique}.root"
bckg="${OUT_PATH}/event_subtraction/${unique}/bckg_${unique}.root"
efficiency="${OUT_PATH}/trees/gen_base/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input}:${sign}:${bckg}" -v ${selections} -u ${unique} -e ${efficiency} -a "qtA" -s "BDT" -t "sign" -b ${stats_path} -f 


popd >> /dev/null
