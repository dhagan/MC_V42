executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
input_unique="base"
output_unique="experimental"

weight="${OUT_PATH}/event_hf/${input_unique}/eval/weight_${input_unique}.root"
diff="${OUT_PATH}/event_hf/${input_unique}/eval/diff_${input_unique}.root"
stats="${OUT_PATH}/event_hf/${input_unique}/eval/stat_${input_unique}.root"
sign="${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root"
bckg="${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root"
efficiency="${OUT_PATH}/trees/${input_unique}/efficiency/sign_efficiencies_${input_unique}.root"

mkdir -p ${OUT_PATH}/event_extract/${output_unique}
pushd ${OUT_PATH}/event_extract/${output_unique} >> /dev/null

${executable} -i "${weight}:${diff}:${stats}:${sign}:${bckg}:${efficiency}" -v ${selections} -u ${output_unique} -a "qtA" -s "BDT" -f 

popd >> /dev/null
