executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
output_unique="extract_qtb_merge_at_end"
log=${LOG_PATH}/extract/${output_unique}.txt
input_before="${OUT_PATH}/event_extract/qtb_merge/extract.root"
input_after="${OUT_PATH}/event_extract/extract_qtb_merge_at_end/extract_qtb_merge_at_end.root"

mkdir -p ${OUT_PATH}/event_extract/${output_unique}

pushd ${OUT_PATH}/event_extract/${output_unique} >> /dev/null

touch ${log}
${executable} -i "${input_before}:${input_after}" -c -d -u "premerge:postmerge" -v ${selections} -a "qtA" -s "BDT"

popd >> /dev/null
