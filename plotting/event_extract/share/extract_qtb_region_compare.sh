executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique="qtb_region_compare"
unique_input="qtb_upper:qtb_lower"
input_upper="${OUT_PATH}/event_hf/qtb_upper/eval/qtb_upper_Q12_output.root"
input_lower="${OUT_PATH}/event_hf/qtb_lower/eval/qtb_lower_Q12_output.root"
stats_path="${OUT_PATH}/event_hf/base/eval/hf_chi2_base_A-qtA_S-BDT.root"
sign_upper="${OUT_PATH}/event_subtraction/qtb_upper/sign_qtb_upper.root"
bckg_upper="${OUT_PATH}/event_subtraction/qtb_upper/bckg_qtb_upper.root"
sign_lower="${OUT_PATH}/event_subtraction/qtb_lower/sign_qtb_lower.root"
bckg_lower="${OUT_PATH}/event_subtraction/qtb_lower/bckg_qtb_lower.root"

mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

${executable} -i "${input_upper}:${input_lower}:${sign_upper}:${sign_lower}:${bckg_upper}:${bckg_lower}" -v ${selections} -u ${unique_input} -a "qtA" -s "BDT" -c


popd >> /dev/null
