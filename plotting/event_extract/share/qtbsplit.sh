executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique=qtbsplit

mkdir -p ${OUT_PATH}/event_extract/${unique}
pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null
##${executable} -i "${OUT_PATH}/event_hf/:${OUT_PATH}/trees/" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -b 4
##${executable} -i "${OUT_PATH}/event_hf/:${OUT_PATH}/trees/" -v ${selections} -u ${unique} -a "DiMuonPt" -s "BDT" -b 6
##${executable} -i "${OUT_PATH}/event_hf/:${OUT_PATH}/trees/" -g "${OUT_PATH}/event_subtraction" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -m "split" -b 8


${executable} -i "${OUT_PATH}/event_hf/:${OUT_PATH}/trees/" -g "${OUT_PATH}/event_subtraction" -v ${selections} -u "qtb_lower:qtbsplit:lowervslower" -a "qtA" -s "BDT" -m "merge" -b 3


popd >> /dev/null

##ranges_1="qtB:abs(qtB)&1:0:2"
##ranges_2="qtB:abs(qtB)&1:2:4"
##ranges_3="qtB:abs(qtB)&1:4:6"
##ranges_4="qtB:abs(qtB)&1:6:8"
##ranges_5="qtB:abs(qtB)&1:8:10"
##ranges_6="qtB:abs(qtB)&1:10:12"
##ranges_7="qtB:abs(qtB)&1:12:14"
##ranges_8="qtB:abs(qtB)&1:14:16"
##ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )
##
##for qtb in {0..7}; do
##
##	## create specifics
##	range="${ranges_list[$qtb]}"
##	number=$(($qtb + 1))
##	output_unique="qtbsplit-${number}"
##	input_unique="qtbsplit-${number}"
##
##	weight="${OUT_PATH}/event_hf/${input_unique}/eval/weight_${input_unique}.root"
##	diff="${OUT_PATH}/event_hf/${input_unique}/eval/diff_${input_unique}.root"
##	stats="${OUT_PATH}/event_hf/${input_unique}/eval/stat_${input_unique}.root"
##	sign="${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root"
##	bckg="${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root"
##	efficiency="${OUT_PATH}/trees/${input_unique}/efficiency/sign_efficiencies_${input_unique}.root"
##
##	mkdir -p ${OUT_PATH}/event_extract/${output_unique}
##	pushd ${OUT_PATH}/event_extract/${output_unique} >> /dev/null
##
##	${executable} -i "${weight}:${diff}:${stats}:${sign}:${bckg}:${efficiency}" -v ${selections} -u ${output_unique} -a "qtA" -s "BDT" -f 
##
##	popd >> /dev/null
##done
