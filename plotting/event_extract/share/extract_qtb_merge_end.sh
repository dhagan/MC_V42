executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
output_unique="qtb_merge_end"
log=${LOG_PATH}/extract/${output_unique}.txt

input_lower="${OUT_PATH}/event_hf/qtb_lower/eval/qtb_lower_Q12_output.root"
input_upper="${OUT_PATH}/event_hf/qtb_upper/eval/qtb_upper_Q12_output.root"

stats_path="${OUT_PATH}/event_hf/base/eval/hf_chi2_base_A-qtA_S-BDT.root"
sign="${OUT_PATH}/event_subtraction/base/sign_base.root"
bckg="${OUT_PATH}/event_subtraction/base/bckg_base.root"

efficiency="${OUT_PATH}/trees/gen_base/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/event_extract/${output_unique}

pushd ${OUT_PATH}/event_extract/${output_unique} >> /dev/null

touch ${log}
${executable} -i "${input_upper}:${input_lower}:${sign}:${bckg}" -v ${selections} -e ${efficiency} -a "qtA" -s "BDT" -q -u ${output_unique} -b ${stats_path}

popd >> /dev/null
