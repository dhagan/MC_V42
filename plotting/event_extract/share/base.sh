executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique=base
log=${LOG_PATH}/extract/${unique}.txt
weight="${OUT_PATH}/event_hf/${unique}/eval/weight_${unique}.root"
diff="${OUT_PATH}/event_hf/${unique}/eval/diff_${unique}.root"
stats="${OUT_PATH}/event_hf/${unique}/eval/stat_${unique}.root"
sign="${OUT_PATH}/event_subtraction/${unique}/sign_${unique}.root"
bckg="${OUT_PATH}/event_subtraction/${unique}/bckg_${unique}.root"
efficiency="${OUT_PATH}/trees/${unique}/efficiency/sign_efficiencies_${unique}.root"

mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${weight}:${diff}:${stats}:${sign}:${bckg}:${efficiency}" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -f 

popd >> /dev/null
