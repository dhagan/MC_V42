executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique=qtbsplit
log=${LOG_PATH}/extract/${unique}.txt
split1=${OUT_PATH}/event_hf/qtbsplit-1/eval
split2=${OUT_PATH}/event_hf/qtbsplit-2/eval
split3=${OUT_PATH}/event_hf/qtbsplit-3/eval
efficiency1="${OUT_PATH}/trees/gen_qtbsplit-1/sign_efficiencies_qtbsplit-1.root"
efficiency2="${OUT_PATH}/trees/gen_qtbsplit-2/sign_efficiencies_qtbsplit-2.root"
efficiency3="${OUT_PATH}/trees/gen_qtbsplit-3/sign_efficiencies_qtbsplit-3.root"


##touch ${log}
##mkdir -p ${OUT_PATH}/event_extract/${unique}
##pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null
##
##${executable} -i "${split1}:${split2}:${split3}:${efficiency1}:${efficiency2}:${efficiency3}" -v ${selections} -a "qtA" -s "BDT" -b
##
##popd >> /dev/null


ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
ranges_list=( "${ranges_1}" "${ranges_2}" ) ##"${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )
uniques=( "lower" "upper" )

for qtb in {0..0}; do

	## create specifics
	range="${ranges_list[$qtb]}"
	output_unique="qtb_${uniques[$index]}"
	input_unique="qtb_${uniques[$index]}"

	weight="${OUT_PATH}/event_hf/${input_unique}/eval/weight_${input_unique}.root"
	diff="${OUT_PATH}/event_hf/${input_unique}/eval/diff_${input_unique}.root"
	stats="${OUT_PATH}/event_hf/${input_unique}/eval/stat_${input_unique}.root"
	sign="${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root"
	bckg="${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root"
	efficiency="${OUT_PATH}/trees/gen_${input_unique}/sign_efficiencies_${input_unique}.root"

	touch ${log}
	mkdir -p ${OUT_PATH}/event_extract/${output_unique}
	pushd ${OUT_PATH}/event_extract/${output_unique} >> /dev/null

	${executable} -i "${weight}:${diff}:${stats}:${sign}:${bckg}:${efficiency}" -v ${selections} -u ${output_unique} -a "qtA" -s "BDT" -m "hf"

	popd >> /dev/null
done
