executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" )

for qtb in ${!ranges_list[@]}; do

	number=$(($qtb + 1))
	output_unique="qtbsplit-${number}"
	input_unique="qtbsplit-${number}"
	range="${ranges_list[$qtb]}"
	log=${LOG_PATH}/extract/${output_unique}.txt

	weight_input="${OUT_PATH}/event_hf/${input_unique}/eval/${input_unique}_Q12_output.root"
	diff_input="${OUT_PATH}/event_hf/${input_unique}/eval/hf_fit_${input_unique}_A-qtA_S-BDT.root"
	stats_path="${OUT_PATH}/event_hf/${input_unique}/eval/hf_chi2_${input_unique}_A-qtA_S-BDT.root"
	sign="${OUT_PATH}/event_subtraction/${input_unique}/sign_${input_unique}.root"
	bckg="${OUT_PATH}/event_subtraction/${input_unique}/bckg_${input_unique}.root"
	efficiency="${OUT_PATH}/trees/gen_${input_unique}/sign_efficiencies_${input_unique}.root"


	touch ${log}
	mkdir -p ${OUT_PATH}/event_extract/${output_unique}
	pushd ${OUT_PATH}/event_extract/${output_unique} >> /dev/null
	
	${executable} -i "${weight_input}:${diff_input}:${stats_path}:${sign}:${bckg}:${efficiency}" -v ${selections} -u ${output_unique} -r ${range} -a "qtA" -s "BDT" -f

	${executable} -i "${split_1}${split_2}:${split_3}" -v ${selections} -u ${output_unique} -r ${range} -a "qtA" -s "BDT" -f 

	popd >> /dev/null
	
done


