executable=${ANA_IP}/plotting/event_extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique="qtb_hadd"
log=${LOG_PATH}/extract/${unique}.txt
input_upper="${OUT_PATH}/event_extrat/qtb_upper/qtb_upper_Q12_output.root"
input_lower="${OUT_PATH}/event_extrat/qtb_lower/qtb_lower_Q12_output.root"
efficiency="${OUT_PATH}/trees/gen_base/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/event_extract/${unique}

pushd ${OUT_PATH}/event_extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input_upper}:${input_lower}" -d


popd >> /dev/null
