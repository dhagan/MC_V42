#include <hf_extr.hxx>

void hf_extr( basic_fileset & input, variable_set & variables ){


  prep_style();
  gStyle->SetOptStat( "krims" );


  std::string unique = input.get_unique();
  const char * unique_c = input.get_unique();

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "event_extract_general" );

  std::vector< std::string > mass_bins = { "Q12" };

  TTree * input_tree = (TTree *) input.weighted_file->Get( "tree" );
  TTree * sign_tree = (TTree *) input.sign_file->Get( "tree" );
  TTree * bckg_tree = (TTree *) input.bckg_file->Get( "tree" );

  int ana_bins      = variables.analysis_bound.get_bins();
  float ana_min     = variables.analysis_bound.get_min(); 
  float ana_max     = variables.analysis_bound.get_max(); 
  std::string analysis_var = variables.analysis_bound.get_var();
  std::vector< std::string > ana_cuts = variables.analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = variables.analysis_bound.get_series_names( ana_bins );

	TTree * stats_tree = (TTree *) input.statistics_file->Get( "Q12" );
	produce_table( stats_tree );

  TFile * output_file = new TFile( "extract.root", "RECREATE" );

  generate_weight_maps( variables.analysis_bound, variables.spectator_bound, input.statistics_file, unique, output_file );

  delta_map( input, variables, styles ); 

  for ( std::string & mass : mass_bins ){

    variables.mass = mass;
    bound mass_bound = variables.bound_manager->get_bound( mass );
    std::string mass_cut = mass_bound.get_cut();

    TH1F * sign_extracted = new TH1F( Form( "sign_extr_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_extracted = new TH1F( Form( "bckg_extr_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_extracted = new TH1F( Form( "data_extr_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * sign_mc = new TH1F( Form( "sign_mc_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_mc = new TH1F( Form( "bckg_mc_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_raw = new TH1F( Form( "data_raw_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );

    THStack * comb_stack = new THStack( Form( "comb_stack_%s", mass.c_str() ), "" );

    input_tree->Draw( Form( "%s>>sign_extr_%s", variables.analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str() ) );

    input_tree->Draw( Form( "%s>>bckg_extr_%s", variables.analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str() ) );

    input_tree->Draw( Form( "%s>>data_raw_%s", variables.analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );

    sign_tree->Draw( Form( "%s>>sign_mc_%s", variables.analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );
    bckg_tree->Draw( Form( "%s>>bckg_mc_%s", variables.analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );

    data_extracted->Add( sign_extracted, bckg_extracted, 1.0, 1.0 ); 

    styles->style_histogram( sign_extracted, "extracted_sign" );
    styles->style_histogram( bckg_extracted, "extracted_bckg" );
    TH1F * sign_stack = (TH1F *) sign_extracted->Clone();
    TH1F * bckg_stack = (TH1F *) bckg_extracted->Clone();
    styles->style_histogram( sign_stack,  "extracted_sign_fill" );
    styles->style_histogram( bckg_stack,  "extracted_bckg_fill" );
    comb_stack->Add( bckg_stack );
    comb_stack->Add( sign_stack );
    TH1F * sign_extracted_normalised = (TH1F *) sign_extracted->Clone( Form( "sign_extr_norm_%s", mass.c_str() ) );
    TH1F * bckg_extracted_normalised = (TH1F *) bckg_extracted->Clone( Form( "bckg_extr_norm_%s", mass.c_str() ) );
    TH1F * data_extracted_normalised = (TH1F *) data_extracted->Clone( Form( "data_extr_norm_%s", mass.c_str() ) );
    sign_extracted_normalised->Scale( 1.0/sign_extracted_normalised->Integral() );
    bckg_extracted_normalised->Scale( 1.0/bckg_extracted_normalised->Integral() );
    data_extracted_normalised->Scale( 1.0/data_extracted_normalised->Integral() );
    sign_mc->Scale( 1.0/sign_mc->Integral() );
    bckg_mc->Scale( 1.0/bckg_mc->Integral() );
    styles->style_histogram( sign_mc,  "mc_sign" );
    styles->style_histogram( bckg_mc,  "mc_bckg" );
    styles->style_histogram( data_raw,  "raw_data" );
    styles->style_histogram( data_extracted, "extracted_data" );
    hist_group diff_hists_noeff = input.recreate_histograms_differential( variables, false );
    hist_group diff_hists_eff = input.recreate_histograms_differential( variables, true );
    hist_group stat_hists_eff = input.recreate_histograms_statistical( variables, true );
    hist_group weight_hists_eff = input.recreate_histograms_weights( variables, true );

    extraction_method_comparison(  stat_hists_eff.sign_hist,   diff_hists_eff.sign_hist, variables, "stats_vs_original" );
    extraction_method_comparison(  weight_hists_eff.sign_hist, diff_hists_eff.sign_hist, variables, "weight_vs_original" );
    extraction_method_comparison(  weight_hists_eff.sign_hist, stat_hists_eff.sign_hist, variables, "weight_vs_stats" );

    jpsi_gamma( input, variables );
    extraction_details( input, variables );
    data_comparison( sign_extracted, bckg_extracted, data_extracted, comb_stack, data_raw, unique_c, variables   ); 
    sign_extraction( sign_extracted, unique_c, variables );
    bckg_extraction( bckg_extracted, unique_c, variables );
    shape_extraction( sign_extracted_normalised, bckg_extracted_normalised, unique_c, variables );
    mc_shape_comparison( sign_extracted_normalised, sign_mc, bckg_extracted_normalised, bckg_mc, unique_c, variables );  
    ratio_plots( sign_extracted_normalised, sign_mc, bckg_extracted_normalised, bckg_mc, unique_c, variables );
    fit_process( sign_extracted, bckg_extracted, unique_c, variables );

    
    std::string efficiency_string = Form( "eff_%s_%s", variables.analysis_variable.c_str(), mass.c_str() );
    if ( input.efficiency_file->GetListOfKeys()->Contains( efficiency_string.c_str() ) ){
      TH1F * sign_efficiency = (TH1F *) input.efficiency_file->Get( efficiency_string.c_str() ); 
      sign_extracted->Divide( sign_extracted, sign_efficiency, 1.0, 1.0 );
      std::cout << "Signal efficiency correction \"" << efficiency_string << "\" in \"" << unique << "\" applied." << std::endl;
    } else {
      std::cout << "Warning: Efficiency \"" << efficiency_string << "\" not found. Continuing without correction." << std::endl;
    }

    //std::string eff_unique =  unique + "_eff";
    char eff_unique[50];
    sprintf( eff_unique, "%s_eff", unique_c ); 

    THStack * eff_comb_stack = new THStack( Form( "eff_comb_stack_%s", mass.c_str() ), "" );
    sign_stack = (TH1F *) sign_extracted->Clone();
    bckg_stack = (TH1F *) bckg_extracted->Clone();
    styles->style_histogram( sign_stack,  "extracted_sign_fill" );
    styles->style_histogram( bckg_stack,  "extracted_bckg_fill" );
    eff_comb_stack->Add( bckg_stack );
    eff_comb_stack->Add( sign_stack );
    data_extracted->Reset(); 
    data_extracted->Add( sign_extracted, bckg_extracted, 1.0, 1.0 ); 
    sign_extracted_normalised = (TH1F *) sign_extracted->Clone( Form( "sign_extr_norm_%s", mass.c_str() ) );
    sign_extracted_normalised->Scale( 1.0/sign_extracted_normalised->Integral() );
    data_extracted_normalised->Scale( 1.0/data_extracted_normalised->Integral() );

    
    data_comparison( sign_extracted, bckg_extracted, data_extracted, comb_stack, data_raw, eff_unique, variables   );  
    sign_extraction( sign_extracted, eff_unique, variables );
    bckg_extraction( bckg_extracted, eff_unique, variables );
    shape_extraction( sign_extracted_normalised, bckg_extracted_normalised, eff_unique, variables );
    mc_shape_comparison( sign_extracted_normalised, sign_mc, bckg_extracted_normalised, bckg_mc, eff_unique, variables );  
    ratio_plots( sign_extracted_normalised, sign_mc, bckg_extracted_normalised, bckg_mc, eff_unique, variables );
    fit_process( sign_extracted, bckg_extracted, eff_unique, variables );
        

    // noefficiency differential production.
    TH1F * sign_diff_hist = diff_hists_noeff.sign_hist; 
    TH1F * bckg_diff_hist = diff_hists_noeff.bckg_hist;
    TH1F * data_diff_hist = diff_hists_noeff.data_hist;
    THStack * diff_comb_stack = new THStack( Form( "comb_stack_%s", mass.c_str() ), "" );
    TH1F * diff_sign_stack = (TH1F *) sign_extracted->Clone();
    TH1F * diff_bckg_stack = (TH1F *) bckg_extracted->Clone();
    TH1F * diff_sign_normalised = (TH1F *) sign_diff_hist->Clone();
    TH1F * diff_bckg_normalised = (TH1F *) bckg_diff_hist->Clone();
    diff_sign_normalised->Scale( 1.0/diff_sign_normalised->Integral() );
    diff_bckg_normalised->Scale( 1.0/diff_bckg_normalised->Integral() );
    styles->style_histogram( sign_diff_hist, "extracted_sign" );
    styles->style_histogram( bckg_diff_hist, "extracted_bckg" );
    styles->style_histogram( diff_sign_normalised, "extracted_sign" );
    styles->style_histogram( diff_bckg_normalised, "extracted_bckg" );
    styles->style_histogram( diff_sign_stack,  "extracted_sign_fill" );
    styles->style_histogram( diff_bckg_stack,  "extracted_bckg_fill" );
    diff_comb_stack->Add( bckg_stack );
    diff_comb_stack->Add( sign_stack );

    char diff_noeff_unique[50];
    sprintf( diff_noeff_unique, "%s_diff_noeff", unique_c );

    data_comparison( sign_diff_hist, bckg_diff_hist, data_diff_hist, diff_comb_stack, data_raw, diff_noeff_unique, variables   );  
    sign_extraction( sign_diff_hist, diff_noeff_unique, variables );
    bckg_extraction( bckg_diff_hist, diff_noeff_unique, variables );
    shape_extraction( diff_sign_normalised, diff_bckg_normalised, diff_noeff_unique, variables );
    mc_shape_comparison( diff_sign_normalised, sign_mc, diff_bckg_normalised, bckg_mc, diff_noeff_unique, variables );  
    ratio_plots( diff_sign_normalised, sign_mc, diff_bckg_normalised, bckg_mc, diff_noeff_unique, variables );
    fit_process( sign_diff_hist, bckg_diff_hist, diff_noeff_unique, variables );


    // efficiency differential production.
    TH1F * sign_diff_eff_hist = diff_hists_eff.sign_hist; 
    TH1F * bckg_diff_eff_hist = diff_hists_eff.bckg_hist;
    TH1F * data_diff_eff_hist = diff_hists_eff.data_hist;
    THStack * diff_eff_comb_stack = new THStack( Form( "comb_stack_%s", mass.c_str() ), "" );
    TH1F * diff_eff_sign_stack = (TH1F *) sign_extracted->Clone();
    TH1F * diff_eff_bckg_stack = (TH1F *) bckg_extracted->Clone();
    TH1F * diff_eff_sign_normalised = (TH1F *) sign_diff_eff_hist->Clone();
    TH1F * diff_eff_bckg_normalised = (TH1F *) bckg_diff_eff_hist->Clone();
    diff_eff_sign_normalised->Scale( 1.0/diff_eff_sign_normalised->Integral() );
    diff_eff_bckg_normalised->Scale( 1.0/diff_eff_bckg_normalised->Integral() );
    styles->style_histogram( sign_diff_eff_hist, "extracted_sign" );
    styles->style_histogram( bckg_diff_eff_hist, "extracted_bckg" );
    styles->style_histogram( diff_eff_sign_normalised, "extracted_sign" );
    styles->style_histogram( diff_eff_bckg_normalised, "extracted_bckg" );
    styles->style_histogram( diff_eff_sign_stack,  "extracted_sign_fill" );
    styles->style_histogram( diff_eff_bckg_stack,  "extracted_bckg_fill" );
    diff_eff_comb_stack->Add( bckg_stack );
    diff_eff_comb_stack->Add( sign_stack );

    //std::string diff_eff_unique = unique + ;
    char diff_eff_unique[50];
    sprintf( diff_eff_unique, "%s_diff_eff", unique_c );

    data_comparison( sign_diff_eff_hist, bckg_diff_eff_hist, data_diff_eff_hist, diff_eff_comb_stack, data_raw, diff_eff_unique, variables   );  
    sign_extraction( sign_diff_eff_hist, diff_eff_unique, variables );
    bckg_extraction( bckg_diff_eff_hist, diff_eff_unique, variables );
    shape_extraction( diff_eff_sign_normalised, diff_eff_bckg_normalised, diff_eff_unique, variables );
    mc_shape_comparison( diff_eff_sign_normalised, sign_mc, diff_eff_bckg_normalised, bckg_mc, diff_eff_unique, variables );  
    ratio_plots( diff_eff_sign_normalised, sign_mc, diff_eff_bckg_normalised, bckg_mc, diff_eff_unique, variables );
    fit_process( sign_diff_eff_hist, bckg_diff_eff_hist, diff_eff_unique, variables );

    output_file->cd();
    sign_diff_eff_hist->Write();
    bckg_diff_eff_hist->Write();
    data_diff_eff_hist->Write();
    diff_eff_sign_normalised->Write( "sign_norm_Q12" );
    diff_eff_bckg_normalised->Write( "bckg_norm_Q12" );


  }

  output_file->Close(); 
}
