#include <weight_analysis.hxx>

static const int weight_bins=200;

TH1F * weight_histogram( std::vector< basic_fileset *> & filesets, variable_set & variables, const std::string & variable, sample_type type, bool subtraction=false ){

  bound & variable_bound = variables.bound_manager->get_bound( variable ); 
  TH1F * stat_hist = variable_bound.get_hist();
  std::vector< std::string > bin_cuts = variable_bound.get_cut_series();

  if ( type == data || type == bbbg ){ 
    std::cout << "Warning: No histfactory weight exists for type: \"" << type_c[type] << "\" returning empty" << std::endl;
  }
  const char * draw_variable = Form( (subtraction) ? "(hf_%s_weight*subtraction_weight)>>+weights" : "(hf_%s_weight)>>+weights" , type_c[type] );

  TH1F * weights = new TH1F( "weights", "", weight_bins, -1.0*subtraction, 1.0 );
  for ( int var_bin = 1; var_bin <= variable_bound.get_bins(); var_bin++ ){

    for ( basic_fileset * fileset : filesets ){ fileset->weighted_tree->Draw( draw_variable, bin_cuts.at( var_bin - 1 ).c_str(), "goff" ); } 
    if ( weights->GetEntries() == 0 ){ continue; }
    stat_hist->SetBinContent( var_bin, weights->GetMean() );
    stat_hist->SetBinError( var_bin, weights->GetRMS() );
    weights->Reset();

  }

  delete weights;
  return stat_hist;
}




void weight_analysis( basic_fileset * fileset, variable_set & variables, style_mgr * styles, const std::string & check_variable ){

  gStyle->SetOptStat( "enkrimes" );

  std::vector< basic_fileset * > bf{ fileset };

  const char * unique_c = fileset->get_unique();
  bound & variable = variables.bound_manager->get_bound( check_variable );
  TH1F * sign_weight_histogram = weight_histogram( bf, variables, check_variable, sign, false );
  TH1F * bckg_weight_histogram = weight_histogram( bf, variables, check_variable, bckg, false );
  TH1F * sign_sub_weight_histogram = weight_histogram( bf, variables, check_variable, sign, true );
  TH1F * bckg_sub_weight_histogram = weight_histogram( bf, variables, check_variable, bckg, true );
  sign_weight_histogram->SetStats( 0 );
  bckg_weight_histogram->SetStats( 0 );
  sign_sub_weight_histogram->SetStats( 0 );
  bckg_sub_weight_histogram->SetStats( 0 );
  
  styles->set_stage( "default" );
  styles->style_histogram( sign_weight_histogram, "sign_weight" );
  styles->style_histogram( bckg_weight_histogram, "bckg_weight" );
  styles->style_histogram( sign_sub_weight_histogram, "sign_weight" );
  styles->style_histogram( bckg_sub_weight_histogram, "bckg_weight" );

  TCanvas * weight_canv = new TCanvas( "weight_canvas", "", 200, 200, 2000, 1000 );
  weight_canv->Divide( 2, 1 );

  TPad * weight_pad = (TPad *) weight_canv->cd( 1 );
  sign_weight_histogram->Draw( "E3" );
  sign_weight_histogram->Draw( "SAME E1" );
  sign_weight_histogram->Draw( "SAME P" );
  bckg_weight_histogram->Draw( "SAME E3 P" );
  bckg_weight_histogram->Draw( "SAME E1" );
  hist_prep_axes( sign_weight_histogram );
  sign_weight_histogram->GetYaxis()->SetRangeUser( 0, 1.5 );
  set_axis_labels( sign_weight_histogram, variable.get_x_str(), Form( "Weight/%s", variable.get_y_str().c_str() ) );
  add_pad_title( weight_pad, Form( "Weights %s - %s", variable.get_ltx().c_str(), unique_c ), false );
  add_atlas_decorations( weight_pad, true, false );
  TLegend * weight_legend = create_atlas_legend();
  weight_legend->AddEntry( sign_weight_histogram, "sign weight", "LPF" );
  weight_legend->AddEntry( bckg_weight_histogram, "bckg weight", "LPF" );
  weight_legend->Draw( "SAME" );
  
  weight_pad = (TPad *) weight_canv->cd( 2 );
  sign_sub_weight_histogram->Draw( "E3" );
  sign_sub_weight_histogram->Draw( "SAME E1" );
  sign_sub_weight_histogram->Draw( "SAME P" );
  bckg_sub_weight_histogram->Draw( "SAME E3 P" );
  bckg_sub_weight_histogram->Draw( "SAME E1" );
  hist_prep_axes( sign_sub_weight_histogram );
  sign_sub_weight_histogram->GetYaxis()->SetRangeUser( -1, 2.0);
  set_axis_labels( sign_sub_weight_histogram, variable.get_x_str(), Form( "Weight/%s", variable.get_y_str().c_str() ) );
  add_pad_title( weight_pad, Form( "Weights w/ sub %s - %s", variable.get_ltx().c_str(), unique_c ), false );
  add_atlas_decorations( weight_pad, true, false );
  TLegend * sub_weight_legend = create_atlas_legend();
  sub_weight_legend->SetX1NDC( 0.5 );
  sub_weight_legend->AddEntry( sign_sub_weight_histogram, "sign weight", "LPF" );
  sub_weight_legend->AddEntry( bckg_sub_weight_histogram, "bckg weight", "LPF" );
  sub_weight_legend->Draw( "SAME" );


  weight_canv->SaveAs( Form( "weights_%s_%s.png", unique_c, check_variable.c_str() ) );

  delete sign_weight_histogram;
  delete bckg_weight_histogram;
  delete sign_sub_weight_histogram;
  delete bckg_sub_weight_histogram;
  delete weight_canv;

}



void weight_analysis_combo( std::vector< basic_fileset *> & splits, variable_set & variables, style_mgr * styles, const std::string & check_variable, const std::string & unique ){

  const char * unique_c = unique.c_str();
  bound & variable = variables.bound_manager->get_bound( check_variable );

  TH1F * sign_weight = weight_histogram( splits, variables, check_variable, sign, false );
  TH1F * bckg_weight = weight_histogram( splits, variables, check_variable, bckg, false );
  TH1F * sign_sub_weight = weight_histogram( splits, variables, check_variable, sign, true );
  TH1F * bckg_sub_weight = weight_histogram( splits, variables, check_variable, bckg, true );

  sign_weight->SetStats( 0 );
  bckg_weight->SetStats( 0 );
  sign_sub_weight->SetStats( 0 ); 
  bckg_sub_weight->SetStats( 0 );

  styles->set_stage( "default" );
  styles->style_histogram( sign_weight, "sign_weight" );
  styles->style_histogram( bckg_weight, "bckg_weight" );
  styles->style_histogram( sign_sub_weight, "sign_weight" );
  styles->style_histogram( bckg_sub_weight, "bckg_weight" );


  TCanvas * weight_canv = new TCanvas( "weight_canvas", "", 200, 200, 2000, 1000 );
  weight_canv->Divide( 2, 1 );

  TPad * weight_pad = (TPad *) weight_canv->cd( 1 );
  sign_weight->Draw( "E3" );
  sign_weight->Draw( "SAME E1" );
  sign_weight->Draw( "SAME P" );
  bckg_weight->Draw( "SAME E3 P" );
  bckg_weight->Draw( "SAME E1" );
  hist_prep_axes( sign_weight );
  sign_weight->GetYaxis()->SetRangeUser( 0, 1.5 );
  set_axis_labels( sign_weight, variable.get_x_str(), Form( "Weight/%s", variable.get_y_str().c_str() ) );
  add_pad_title( weight_pad, Form( "Weights %s - %s", variable.get_ltx().c_str(), unique_c ), false );
  add_atlas_decorations( weight_pad, true, false );
  TLegend * weight_legend = create_atlas_legend();
  weight_legend->AddEntry( sign_weight, "sign weight", "LPF" );
  weight_legend->AddEntry( bckg_weight, "bckg weight", "LPF" );
  weight_legend->Draw( "SAME" );
  
  weight_pad = (TPad *) weight_canv->cd( 2 );
  sign_sub_weight->Draw( "E3" );
  sign_sub_weight->Draw( "SAME E1" );
  sign_sub_weight->Draw( "SAME P" );
  bckg_sub_weight->Draw( "SAME E3 P" );
  bckg_sub_weight->Draw( "SAME E1" );
  hist_prep_axes( sign_sub_weight );
  sign_sub_weight->GetYaxis()->SetRangeUser( -1, 2.0 );
  set_axis_labels( sign_sub_weight, variable.get_x_str(), Form( "Weight/%s", variable.get_y_str().c_str() ) );
  add_pad_title( weight_pad, Form( "Weights w/ sub %s - %s", variable.get_ltx().c_str(), unique_c ), false );
  add_atlas_decorations( weight_pad, true, false );
  TLegend * sub_weight_legend = create_atlas_legend();
  sub_weight_legend->SetX1NDC( 0.5 );
  sub_weight_legend->AddEntry( sign_sub_weight, "sign weight", "LPF" );
  sub_weight_legend->AddEntry( bckg_sub_weight, "bckg weight", "LPF" );
  sub_weight_legend->Draw( "SAME" );


  weight_canv->SaveAs( Form( "weights_combo_%s_%s.png", unique_c, check_variable.c_str() ) );

  delete sign_weight;
  delete bckg_weight;
  delete sign_sub_weight;
  delete bckg_sub_weight;
  delete weight_canv;


}
