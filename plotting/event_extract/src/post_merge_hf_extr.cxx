#include <post_merge_hf_extr.hxx>

void post_hf_extr( std::string & input, std::string & abin_var, std::string & spec_var,
           std::string & type, bound_mgr * selections, std::string unique,
           std::string & efficiency, bool t_ranges, std::string & stats_filepath ){

  prep_style();
  gStyle->SetOptStat( "miner" );

  if ( efficiency.empty() ){ std::cout << "no efficiency" << std::endl; }

  //std::vector< std::string > mass_bins = { "Q3", "Q4", "Q5", "Q12" };
  std::vector< std::string > mass_bins = { "Q12" };
  if ( t_ranges ){
    mass_bins.insert( mass_bins.end(), {"T0", "T3", "T4", "T5", "T12" } );
  }

  std::vector< std::string > filenames;
  split_strings( filenames, input, ":" );
  std::string & data_path_1 = filenames.at( 0 );
  std::string & data_path_2 = filenames.at( 1 );
  std::string & sign_path = filenames.at( 2 );
  std::string & bckg_path = filenames.at( 3 );

  std::cout << data_path_1 << std::endl;
  std::cout << data_path_2 << std::endl;
  std::cout << sign_path << std::endl;
  std::cout << bckg_path << std::endl;

  TFile * input_file_1 = new TFile( data_path_1.c_str(), "READ" );
  TFile * input_file_2 = new TFile( data_path_2.c_str(), "READ" );
  TTree * input_tree_1 = (TTree *) input_file_1->Get( "tree" );
  TTree * input_tree_2 = (TTree *) input_file_2->Get( "tree" );

  TFile * sign_mc_file = new TFile( sign_path.c_str(), "READ" );
  TTree * sign_tree = (TTree *) sign_mc_file->Get( "tree" );
  TFile * bckg_mc_file = new TFile( bckg_path.c_str(), "READ" );
  TTree * bckg_tree = (TTree *) bckg_mc_file->Get( "tree" );

  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int ana_bins      = analysis_bound.get_bins();
  float ana_min     = analysis_bound.get_min(); 
  float ana_max     = analysis_bound.get_max(); 
  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( ana_bins );

  std::vector< float > eff_style =  {  21, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,      0.0, 0.0 };
  std::vector< float > sign_extr_style = { 0.0, 0.0, 0.0, 0.0, 1.0, kRed+1, 1.0, 1.0, 0.0, 0.0, 0.0 };
  std::vector< float > bckg_extr_style = { 0.0, 0.0, 0.0, 0.0, 1.0, kBlue+1, 1.0, 1.0, 0.0, 0.0, 0.0 };
  std::vector< float > data_raw_style = { 0.0, 0.0, 0.0, 0.0, 1.0, 1, 1.0, 1.0, 0.0, 0.0, 0.0 };
  std::vector< float > data_extr_style = { 0.0, 0.0, 0.0, 0.0, 2.0, kGreen+1, 1.0, 1.0, 0.0, 0.0, 0.0 };

  std::vector< float > sign_fill_style = { 1.0, 1.0, 1.0, 1.0, 1.0, kRed+1, 1.0,  0.0,  kRed+1,  0.5, 1001 };
  std::vector< float > bckg_fill_style = { 1.0, 1.0, 1.0, 1.0, 1.0, kBlue+1, 1.0, 0.0, kBlue+1, 0.5, 1001 };

  std::vector< float > sign_mc_style = { 0.0, 0.0, 0.0, 0.0, 2.0, kRed+1, 1.0, 1.0, 0.0, 0.0, 0.0 };
  std::vector< float > bckg_mc_style = { 0.0, 0.0, 0.0, 0.0, 2.0, kBlue+1, 1.0, 1.0, 0.0, 0.0, 0.0 };

  TFile * stats_file = new TFile( stats_filepath.c_str(), "READ" );
	TTree * stats_tree = (TTree *) stats_file->Get( "Q12" );

  TFile * output_file = new TFile( "extract.root", "RECREATE" );

  generate_weight_maps( analysis_bound, spectator_bound, stats_file, unique, output_file );
	produce_table( stats_tree );
 
  for ( std::string & mass : mass_bins ){

    bound mass_bound = selections->get_bound( mass );
    std::string mass_cut = mass_bound.get_cut();

    //weight_plot( input_tree, mass_bound, analysis_bound, unique );

    TH1F * sign_extracted = new TH1F( Form( "sign_extr_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_extracted = new TH1F( Form( "bckg_extr_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_extracted = new TH1F( Form( "data_extr_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * sign_mc = new TH1F( Form( "sign_mc_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_mc = new TH1F( Form( "bckg_mc_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_raw = new TH1F( Form( "data_raw_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );

    TH1F * sign_extracted_1 = new TH1F( Form( "sign_extr_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_extracted_1 = new TH1F( Form( "bckg_extr_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_raw_1 = new TH1F( Form( "data_raw_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );

    TH1F * sign_extracted_2 = new TH1F( Form( "sign_extr_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_extracted_2 = new TH1F( Form( "bckg_extr_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_raw_2 = new TH1F( Form( "data_raw_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    
    THStack * comb_stack = new THStack( Form( "comb_stack_%s", mass.c_str() ), "" );

    input_tree_1->Draw( Form( "%s>>sign_extr_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str()  ) );
    input_tree_2->Draw( Form( "%s>>sign_extr_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str()  ) );

    input_tree_1->Draw( Form( "%s>>bckg_extr_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str()  ) );
    input_tree_2->Draw( Form( "%s>>bckg_extr_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str()  ) );

    input_tree_1->Draw( Form( "%s>>data_raw_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );
    input_tree_2->Draw( Form( "%s>>data_raw_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );

    sign_tree->Draw( Form( "%s>>sign_mc_%s", analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );
    bckg_tree->Draw( Form( "%s>>bckg_mc_%s", analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );

    sign_extracted->Add( sign_extracted_1, sign_extracted_2, 1.0, 1.0 );
    bckg_extracted->Add( bckg_extracted_1, bckg_extracted_2, 1.0, 1.0 );
    data_extracted->Add( sign_extracted, bckg_extracted, 1.0, 1.0 );
    data_raw->Add( data_raw_1, data_raw_2, 1.0, 1.0 );

    std::cout << sign_extracted->Integral() << std::endl;
    std::cout << bckg_extracted->Integral() << std::endl;
    std::cout << data_extracted->Integral() << std::endl;
    std::cout << data_raw->Integral() << std::endl;



    style_hist( sign_extracted,  sign_extr_style );
    style_hist( bckg_extracted,  bckg_extr_style );
    TH1F * sign_stack = (TH1F *) sign_extracted->Clone();
    TH1F * bckg_stack = (TH1F *) bckg_extracted->Clone();
    style_hist( sign_stack,  sign_fill_style );
    style_hist( bckg_stack,  bckg_fill_style );
    comb_stack->Add( bckg_stack );
    comb_stack->Add( sign_stack );
    TH1F * sign_extr_normalised = (TH1F *) sign_extracted->Clone( Form( "sign_extr_norm_%s", mass.c_str() ) );
    TH1F * bckg_extr_normalised = (TH1F *) bckg_extracted->Clone( Form( "bckg_extr_norm_%s", mass.c_str() ) );
    TH1F * data_extr_normalised = (TH1F *) data_extracted->Clone( Form( "data_extr_norm_%s", mass.c_str() ) );

    sign_extr_normalised->Scale( 1.0/sign_extr_normalised->Integral() );
    bckg_extr_normalised->Scale( 1.0/bckg_extr_normalised->Integral() );
    data_extr_normalised->Scale( 1.0/data_extr_normalised->Integral() );
    sign_mc->Scale( 1.0/sign_mc->Integral() );
    bckg_mc->Scale( 1.0/bckg_mc->Integral() );
    style_hist( sign_mc,  sign_mc_style );
    style_hist( bckg_mc,  bckg_mc_style );
    style_hist( data_raw,  data_raw_style );
    style_hist( data_extracted, data_extr_style );

    sign_extracted->ResetStats();
    bckg_extracted->ResetStats();
    data_extracted->ResetStats();
    sign_extr_normalised->ResetStats();
    bckg_extr_normalised->ResetStats();
    data_extr_normalised->ResetStats();
    sign_mc->ResetStats();
    bckg_mc->ResetStats();
    data_raw->ResetStats();


    output_file->cd();
    sign_extracted->Write();
    bckg_extracted->Write();
    data_extracted->Write();
    comb_stack->Write();
    sign_extr_normalised->Write();
    bckg_extr_normalised->Write();
    data_extr_normalised->Write();
    sign_mc->Write();
    bckg_mc->Write();
    data_raw->Write();
  

    
    TCanvas * sign_extr_canv  = new TCanvas( Form( "sign_extr_canv_%s", mass.c_str() ), "", 100, 100, 1000, 1000 );
    sign_extr_canv->Divide( 1, 1 );

    //sign_extracted->ResetStats();
    TPad * current_pad = (TPad *) sign_extr_canv->cd( 1 );
    sign_extracted->Draw( "HIST" );
    hist_prep_axes( sign_extracted, true );
    set_axis_labels( sign_extracted, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( current_pad, Form( "Signal - Weight, %s %s %s", type.c_str(), abin_var.c_str(), mass.c_str() ) );
    add_atlas_decorations( current_pad, true, false );
    TPaveStats * sign_extracted_stats = make_stats( sign_extracted );
    sign_extracted_stats->Draw();
    sign_extr_canv->SaveAs( Form("./sign_extract_%s_%s_%s.png", mass.c_str(), abin_var.c_str(), unique.c_str()) ); 
    gPad->SetLogy();
    sign_extracted->GetYaxis()->SetRangeUser( 1, sign_extracted->GetMaximum()*100 );
    sign_extr_canv->SaveAs( Form("./sign_extract_%s_%s_%s_log.png", mass.c_str(), abin_var.c_str(), unique.c_str()) );
    delete sign_extr_canv;

    

    TCanvas * norm_canv = new TCanvas( Form( "norm_canv_%s", mass.c_str() ), "", 100, 100, 1000, 1000 );
    norm_canv->Divide( 1, 1 );
    TPad * norm_pad = (TPad *) norm_canv->cd( 1 );
    sign_extr_normalised->Draw( "HIST E1" );
    bckg_extr_normalised->Draw( "HIST E1 SAMES" );
    hist_prep_axes( sign_extr_normalised, true );
    set_axis_labels( sign_extr_normalised, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( norm_pad, Form( "%s %s %s", unique.c_str(), abin_var.c_str(), mass.c_str() ) );
    add_atlas_decorations( norm_pad, true, false );
    TLegend * norm_legend = below_logo_legend();
    norm_legend->AddEntry( sign_extr_normalised, "sign", "LP" );
    norm_legend->AddEntry( bckg_extr_normalised, "bckg", "LP" );
    norm_legend->Draw();
    TPaveStats * norm_sign_stats = make_stats( sign_extr_normalised, true, true );
    norm_sign_stats->Draw();
    TPaveStats * norm_bckg_stats = make_stats( bckg_extr_normalised, true );
    norm_bckg_stats->Draw();

    norm_canv->SaveAs( Form("./norm_%s_%s_%s.png", mass.c_str(), abin_var.c_str(), unique.c_str()) );
    gPad->SetLogy();
    delete norm_canv;
    
    gStyle->SetOptStat( "" );
    TCanvas * combination_canv = new TCanvas( Form( "combination_%s", mass.c_str() ), "", 100, 100, 2000, 1000 );
    combination_canv->Divide( 2, 1 );

    TPad * combination_pad = (TPad *) combination_canv->cd( 1 );
    
    data_raw->Draw( "HIST E1" );
    bckg_extracted->Draw( "HIST E1 SAMES" );
    sign_extracted->Draw( "HIST E1 SAMES" );
    data_extracted->Draw( "HIST E1 SAME" );
    hist_prep_axes( data_raw, true );
    set_axis_labels( data_raw, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( combination_pad, Form( "%s %s %s", unique.c_str(), abin_var.c_str(), mass.c_str() ) );
    add_atlas_decorations( combination_pad, true, false );
    TLegend * comb_legend = below_logo_legend();
    comb_legend->AddEntry( data_raw,        "data raw",   "LP" );
    comb_legend->AddEntry( sign_extracted,  "sign extr",  "LP" );
    comb_legend->AddEntry( bckg_extracted,  "bckg extr",  "LP" );
    comb_legend->AddEntry( data_extracted,  "data extr",  "LP" );
    comb_legend->Draw();

    combination_pad = (TPad *) combination_canv->cd( 2 );
    data_extracted->Draw( "HIST E1" );
    data_raw->Draw( "HIST E1 SAME" );
    comb_stack->Draw( "HIST SAME" );
    hist_prep_axes( data_extracted, true );
    set_axis_labels( data_extracted, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( combination_pad, Form( "%s %s %s", unique.c_str(), abin_var.c_str(), mass.c_str() ) );
    add_atlas_decorations( combination_pad, true, false );
    TLegend * stack_legend = below_logo_legend();
    stack_legend->AddEntry( data_raw,         "data raw",   "LP" );
    stack_legend->AddEntry( data_extracted,   "data extr",  "LP" );
    stack_legend->AddEntry( sign_stack,       "sign stack", "F" );
    stack_legend->AddEntry( bckg_stack,       "bckg stack", "F" );

    stack_legend->Draw();

    combination_canv->SaveAs( Form( "./comb_%s_%s_%s.png", mass.c_str(), abin_var.c_str(), unique.c_str() ) );

    gStyle->SetOptStat( "rmeink" );
    TCanvas * comparison_canv = new TCanvas( Form( "comparison_%s", mass.c_str() ), "", 100, 100, 2000, 1000 );
    comparison_canv->Divide( 2, 1 );

    TPad * mc_sign_pad = (TPad *) comparison_canv->cd( 1 );
    sign_extr_normalised->Draw( "HIST E1" );
    sign_mc->Draw( "HIST E1 SAMES");
    hist_prep_axes( sign_extr_normalised, true );
    set_axis_labels( sign_extr_normalised, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( mc_sign_pad, Form( "%s %s %s", unique.c_str(), abin_var.c_str(), mass.c_str() ), false );
    add_atlas_decorations( mc_sign_pad, true, false );
    TLegend * sign_comp_legend = below_logo_legend();
    sign_comp_legend->AddEntry( sign_extr_normalised,   "ex sign", "LP" );
    sign_comp_legend->AddEntry( sign_mc,                "mc sign", "LP" );
    sign_comp_legend->Draw();
    TPaveStats * sign_extr_stats = make_stats( sign_extr_normalised, true, true );
    sign_extr_stats->Draw();
    TPaveStats * sign_mc_stats = make_stats( sign_mc, true, false );
    sign_mc_stats->Draw();

    TPad * mc_bckg_pad = (TPad *) comparison_canv->cd( 2 );
    bckg_extr_normalised->Draw( "HIST E1" );
    bckg_mc->Draw( "HIST E1 SAMES");
    hist_prep_axes( bckg_extr_normalised, true );
    set_axis_labels( bckg_extr_normalised, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( mc_bckg_pad, Form( "%s %s %s", unique.c_str(), abin_var.c_str(), mass.c_str() ), false );
    add_atlas_decorations( mc_bckg_pad, true, false );
    TLegend * bckg_comp_legend = below_logo_legend();
    bckg_comp_legend->AddEntry( bckg_extr_normalised,   "ex bckg", "LP" );
    bckg_comp_legend->AddEntry( bckg_mc,                "mc bckg", "LP" );
    bckg_comp_legend->Draw();
    TPaveStats * bckg_extr_stats = make_stats( bckg_extr_normalised, true, true );
    bckg_extr_stats->Draw();
    TPaveStats * bckg_mc_stats = make_stats( bckg_mc, true, false );
    bckg_mc_stats->Draw();

    comparison_canv->SaveAs( Form( "./mc_comp_%s_%s_%s.png", mass.c_str(), abin_var.c_str(), unique.c_str() ) );

    output_file->Close(); 

  }

}
