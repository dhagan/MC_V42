#include <extr.hxx>

void extr( std::string & input, std::string & abin_var, std::string & spec_var,
           std::string & type, bound_mgr * selections, std::string unique, bool t_ranges, bool pos_mode ){

  prep_style();

  std::vector< std::string > mass_bins = { "Q0", "Q3", "Q4", "Q5", "Q12" };
  if ( t_ranges ){
    mass_bins.insert( mass_bins.end(), {"T0", "T3", "T4", "T5", "T12" } );
  }
  TFile * input_file = new TFile( input.c_str(),   "READ");
  TTree * input_tree = (TTree *) input_file->Get( "tree" );

  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int ana_bins      = analysis_bound.get_bins();
  float ana_min     = analysis_bound.get_min(); 
  float ana_max     = analysis_bound.get_max(); 
  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( ana_bins );

  int spec_bins      = spectator_bound.get_bins();
  float spec_min     = spectator_bound.get_min(); 
  float spec_max     = spectator_bound.get_max(); 


  std::string pos = ( pos_mode ) ? "_99_pos_" : "_99_npos_";
  std::vector< float > eff_style =  {  21, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,      0.0, 0.0 };
  std::vector< float > extr_style = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kRed+1,   0.5, 1001 };
  std::vector< float > corr_style = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kBlue+1,  0.5, 1001 };
  std::vector< float > bdt_style = { 0.0, 0.0, 0.0, 0.0, kRed+1, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0 };


  for ( std::string & mass : mass_bins ){

    bound mass_bound = selections->get_bound( mass );
    std::string mass_cut = mass_bound.get_cut();

    TH1F * extracted = new TH1F( Form( "extr_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );

    input_tree->Draw(  Form( "%s>>extr_%s", analysis_bound.get_var().c_str(), mass.c_str() ), Form( "subtraction_weight*(%s)", mass_cut.c_str() ) );

    TCanvas * extr_canv  = new TCanvas( Form( "extr_canv_%s", mass.c_str() ), "", 100, 100, 1000, 1000 );
    extr_canv->Divide( 1, 1 );

    style_hist( extracted,  extr_style );

    TPad * current_pad = (TPad *) extr_canv->cd(1);
    extracted->Draw( "HIST" );
    hist_prep_axes( extracted, true );
    set_axis_labels( extracted, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( current_pad, Form( "Extracted %s %s %s", type.c_str(), abin_var.c_str(), mass.c_str() ) );

    extr_canv->SaveAs( Form("./extract_%s_%s_A-%s_%s.png", type.c_str(), mass.c_str(), abin_var.c_str(), unique.c_str()) );
    delete extr_canv;

    for ( size_t cut_no = 0; cut_no < ana_cuts.size(); cut_no++ ){

      std::string & ana_cut = ana_cuts[cut_no];
      std::string & ana_cut_name = ana_cut_names[cut_no];


      
      TH1F * bdt_extracted = new TH1F( Form( "bdt_extr_%s_%s", mass.c_str(), ana_cut_name.c_str() ), "", spec_bins, spec_min, spec_max );
      input_tree->Draw(  Form( "%s>>bdt_extr_%s_%s", spectator_bound.get_var().c_str(), mass.c_str(), ana_cut_name.c_str() ), Form( "subtraction_weight*((%s)&&(%s))", mass_cut.c_str(), ana_cut.c_str() ) );

      TCanvas * bdt_canv  = new TCanvas( Form( "extr_canv_%s", mass.c_str() ), "", 100, 100, 1000, 1000 );
      bdt_canv->Divide( 1, 1 );

      current_pad = (TPad *) bdt_canv->cd(1);
      bdt_extracted->Draw( "HIST" );
      style_hist( bdt_extracted,  bdt_style );
      hist_prep_axes( bdt_extracted, true );
      set_axis_labels( bdt_extracted, spectator_bound.get_x_str(), Form( "Yield%s", spectator_bound.get_y_str().c_str() ) );
      add_pad_title( current_pad, Form( "Extracted %s %s %s", type.c_str(), spec_var.c_str(), mass.c_str() ) );

      bdt_canv->SaveAs( Form("./BDT_extract_%s_%s_A-%s_%s.png", type.c_str(), mass.c_str(), ana_cut_name.c_str(), unique.c_str()) );
      delete bdt_canv;
      delete bdt_extracted;


    }
    
  }

}
