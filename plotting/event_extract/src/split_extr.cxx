#include <split_extr.hxx>
#include <weight_analysis.hxx>



void combi_fit( TH1F * split1_eff, TH1F * split2_eff, TH1F * split3_eff, TH1F * combi_eff, variable_set & variables, std::string & mass, const std::string & unique ){

  gStyle->SetOptStat( "krims" );

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "event_extract_general_" );

  fit_mgr * fit_manager = new fit_mgr();
  fit_manager->load_fits();

  bound & analysis_bound = variables.analysis_bound;

  TF1 * split1_sg = prep_sg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * split2_sg = prep_sg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * split3_sg = prep_sg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * combi_sg = prep_sg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * split1_dg = prep_dg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * split2_dg = prep_dg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * split3_dg = prep_dg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * combi_dg = prep_dg( analysis_bound.get_min(), analysis_bound.get_max() );



  split1_sg->SetRange( -10, 13 );
  split1_sg->SetParLimits( 0, 3500, 7000 );
  split1_sg->SetParLimits( 1, 4, 6 );
  split1_sg->SetParLimits( 2, 2, 5 );
  split1_sg->SetParameter( 0, 4500 );
  split1_sg->SetParameter( 1, 5 );
  split1_sg->SetParameter( 2, 4 );

  split2_sg->SetRange( -5, 13 );
  split2_sg->SetParLimits( 0, 2000, 4000 );
  split2_sg->SetParLimits( 1, 4, 6 );
  split2_sg->SetParLimits( 2, 3, 5 );
  split2_sg->SetParameter( 0, 3000 );
  split2_sg->SetParameter( 1, 5 );
  split2_sg->SetParameter( 2, 5 );

  split3_sg->SetRange( -5, 12 );
  split3_sg->SetParLimits( 0, 2000, 4000 );
  split3_sg->SetParLimits( 1, 4, 6 );
  split3_sg->SetParLimits( 2, 3, 6 );
  split3_sg->SetParameter( 0, 3000 );
  split3_sg->SetParameter( 1, 5 );
  split3_sg->SetParameter( 2, 4 );

  combi_sg->SetRange( -5, 12 );
  combi_sg->SetParLimits( 0, 10000, 20000 );
  combi_sg->SetParLimits( 1, 4, 6 );
  combi_sg->SetParLimits( 2, 2, 6 );
  combi_sg->SetParameter( 0, 15000 );
  combi_sg->SetParameter( 1, 5 );
  combi_sg->SetParameter( 2, 3 );

  split1_dg->SetRange( -10, 13 );
  split1_dg->SetParLimits( 0, 3500, 6000 );
  split1_dg->SetParLimits( 1, 4, 6 );
  split1_dg->SetParLimits( 2, 2, 5 );
  split1_dg->SetParLimits( 3, 0, 3500 );
  split1_dg->SetParLimits( 4, 5, 15 );
  split1_dg->SetParameter( 0, 5000 );
  split1_dg->SetParameter( 1, 5 );
  split1_dg->SetParameter( 2, 4 );
  split1_dg->SetParameter( 3, 1000 );

  split2_dg->SetRange( -5, 13 );
  split2_dg->SetParLimits( 0, 2000, 4000 );
  split2_dg->SetParLimits( 1, 4, 5 );
  split2_dg->SetParLimits( 2, 2, 4 );
  split2_dg->SetParLimits( 3, 0, 2000 );
  split2_dg->SetParLimits( 4, 4, 10 );
  split2_dg->SetParameter( 0, 3000 );
  split2_dg->SetParameter( 1, 5 );
  split2_dg->SetParameter( 2, 5 );
  split2_dg->SetParameter( 3, 1000 );
  split2_dg->SetParameter( 4, 6 );

  split3_dg->SetRange( -5, 12 );
  split3_dg->SetParLimits( 0, 2000, 4000 );
  split3_dg->SetParLimits( 1, 4, 6 );
  split3_dg->SetParLimits( 2, 1, 4 );
  split3_dg->SetParLimits( 3, 0, 2000 );
  split3_dg->SetParLimits( 4, 4, 10 );
  split3_dg->SetParameter( 0, 3000 );
  split3_dg->SetParameter( 1, 5 );
  split3_dg->SetParameter( 2, 4 );
  split3_dg->SetParameter( 3, 1000 );
  split3_dg->SetParameter( 4, 10 );

  combi_dg->SetRange( -10, 14 );
  combi_dg->SetParLimits( 0, 5000, 20000 );
  combi_dg->SetParLimits( 1, 4, 6 );
  combi_dg->SetParLimits( 2, 2, 4 );
  combi_dg->SetParLimits( 3, 0, 5000 );
  combi_dg->SetParLimits( 4, 4, 10 );
  combi_dg->SetParameter( 0, 15000 );
  combi_dg->SetParameter( 1, 5 );
  combi_dg->SetParameter( 2, 5 );
  combi_dg->SetParameter( 3, 1000 );
  combi_dg->SetParameter( 4, 8 );

  TH1F * split1_sg_hist = (TH1F *) split1_eff->Clone();
  TH1F * split2_sg_hist = (TH1F *) split2_eff->Clone();
  TH1F * split3_sg_hist = (TH1F *) split3_eff->Clone();
  TH1F * combi_sg_hist  = (TH1F *) combi_eff->Clone();

  TH1F * split1_dg_hist = (TH1F *) split1_eff->Clone();
  TH1F * split2_dg_hist = (TH1F *) split2_eff->Clone();
  TH1F * split3_dg_hist = (TH1F *) split3_eff->Clone();
  TH1F * combi_dg_hist  = (TH1F *) combi_eff->Clone();

  TCanvas * combi_sg_canv = new TCanvas( "combi_sg_canv", "", 200, 200, 2000, 2000 );
  combi_sg_canv->Divide( 2, 2 );

  TPad * current_sg_pad;

  current_sg_pad = (TPad *) combi_sg_canv->cd( 1 );

  split1_sg_hist->Draw( "HIST E1" );
  split1_sg_hist->Fit( split1_sg, "MR", "" );
  split1_sg->Draw( "SAME" );
  hist_prep_axes( split1_sg_hist );
  set_axis_labels( split1_sg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_sg_pad, Form( "split1 %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_sg_pad, true, false );
  TLegend * split1_sg_legend = below_logo_legend();
  split1_sg_legend->AddEntry( split1_sg_hist, Form( "Signal, split1 %s", unique.c_str() ) );
  split1_sg_legend->AddEntry( split1_sg, "Fit, Single gaussian" );
  split1_sg_legend->Draw();
  TPaveStats * split1_sg_stats = make_stats( split1_sg_hist );
  split1_sg_stats->Draw( "SAME" );


  current_sg_pad = (TPad *) combi_sg_canv->cd( 2 ) ;
  split2_sg_hist->Draw( "HIST E1" );
  split2_sg_hist->Fit( split2_sg, "MR", "" );
  split2_sg->Draw( "SAME" );
  hist_prep_axes( split2_sg_hist );
  set_axis_labels( split2_sg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_sg_pad, Form( "split2 %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_sg_pad, true, false );
  TLegend * split2_sg_legend = below_logo_legend();
  split2_sg_legend->AddEntry( split2_sg_hist, Form( "Signal, split2 %s", unique.c_str() ) );
  split2_sg_legend->AddEntry( split2_sg, "Fit, Single gaussian" );
  split2_sg_legend->Draw();
  TPaveStats * split2_sg_stats = make_stats( split2_sg_hist );
  split2_sg_stats->Draw( "SAME" );

  current_sg_pad = (TPad *) combi_sg_canv->cd( 3 ) ;
  split3_sg_hist->Draw( "HIST E1" );
  split3_sg_hist->Fit( split3_sg, "MR", "" );
  split3_sg->Draw( "SAME" );
  hist_prep_axes( split3_sg_hist );
  set_axis_labels( split3_sg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_sg_pad, Form( "split3 %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_sg_pad, true, false );
  TLegend * split3_sg_legend = below_logo_legend();
  split3_sg_legend->AddEntry( split3_sg_hist, Form( "Signal, split3 %s", unique.c_str() ) );
  split3_sg_legend->AddEntry( split3_sg, "Fit, Single gaussian" );
  split3_sg_legend->Draw();
  TPaveStats * split3_sg_stats = make_stats( split3_sg_hist );
  split3_sg_stats->Draw( "SAME" );


  current_sg_pad = (TPad *) combi_sg_canv->cd( 4 ) ;
  combi_sg_hist->Draw( "HIST E1" );
  combi_sg_hist->Fit( combi_sg, "MR", "" );
  combi_sg->Draw( "SAME" );
  hist_prep_axes( combi_sg_hist );
  set_axis_labels( combi_sg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_sg_pad, Form( "combi %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_sg_pad, true, false );
  TLegend * combi_sg_legend = below_logo_legend();
  combi_sg_legend->AddEntry( combi_sg_hist, Form( "Signal, combi %s", unique.c_str() ) );
  combi_sg_legend->AddEntry( combi_sg, "Fit, Single gaussian" );
  combi_sg_legend->Draw();
  TPaveStats * combi_sg_stats = make_stats( combi_sg_hist );
  combi_sg_stats->Draw( "SAME" );

  combi_sg_canv->SaveAs( Form( "./combi_%s_sg.png", unique.c_str() ) );

  TCanvas * combi_dg_canv = new TCanvas( "combi_dg_canv", "", 200, 200, 2000, 2000 );
  combi_dg_canv->Divide( 2, 2 );

  TPad * current_dg_pad;

  current_dg_pad = (TPad *) combi_dg_canv->cd( 1 );

  split1_dg_hist->Draw( "HIST E1" );
  split1_dg_hist->Fit( split1_dg, "MR", "" );
  split1_dg->Draw( "SAME" );
  hist_prep_axes( split1_dg_hist );
  set_axis_labels( split1_dg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_dg_pad, Form( "split1 %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_dg_pad, true, false );
  TLegend * split1_dg_legend = below_logo_legend();
  split1_dg_legend->AddEntry( split1_dg_hist, Form( "Signal, split1 %s", unique.c_str() ) );
  split1_dg_legend->AddEntry( split1_dg, "Fit, Double gaussian" );
  split1_dg_legend->Draw();
  TPaveStats * split1_dg_stats = make_stats( split1_dg_hist );
  split1_dg_stats->Draw( "SAME" );


  current_dg_pad = (TPad *) combi_dg_canv->cd( 2 ) ;
  split2_dg_hist->Draw( "HIST E1" );
  split2_dg_hist->Fit( split2_dg, "MR", "" );
  split2_dg->Draw( "SAME" );
  hist_prep_axes( split2_dg_hist );
  set_axis_labels( split2_dg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_dg_pad, Form( "split2 %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_dg_pad, true, false );
  TLegend * split2_dg_legend = below_logo_legend();
  split2_dg_legend->AddEntry( split2_dg_hist, Form( "Signal, split2 %s", unique.c_str() ) );
  split2_dg_legend->AddEntry( split2_dg, "Fit, Double gaussian" );
  split2_dg_legend->Draw();
  TPaveStats * split2_dg_stats = make_stats( split2_dg_hist );
  split2_dg_stats->Draw( "SAME" );

  current_dg_pad = (TPad *) combi_dg_canv->cd( 3 ) ;
  split3_dg_hist->Draw( "HIST E1" );
  split3_dg_hist->Fit( split3_dg, "MR", "", analysis_bound.get_min(), analysis_bound.get_max() );
  split3_dg->Draw( "SAME" );
  hist_prep_axes( split3_dg_hist );
  set_axis_labels( split3_dg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_dg_pad, Form( "split3 %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_dg_pad, true, false );
  TLegend * split3_dg_legend = below_logo_legend();
  split3_dg_legend->AddEntry( split3_dg_hist, Form( "Signal, split3 %s", unique.c_str() ) );
  split3_dg_legend->AddEntry( split3_dg, "Fit, Double gaussian" );
  split3_dg_legend->Draw();
  TPaveStats * split3_dg_stats = make_stats( split3_dg_hist );
  split3_dg_stats->Draw( "SAME" );


  current_dg_pad = (TPad *) combi_dg_canv->cd( 4 ) ;
  combi_dg_hist->Draw( "HIST E1" );
  combi_dg_hist->Fit( combi_dg, "MR", "" );
  combi_dg->Draw( "SAME" );
  hist_prep_axes( combi_dg_hist );
  set_axis_labels( combi_dg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_dg_pad, Form( "combi %s %s %s", unique.c_str(), analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( current_dg_pad, true, false );
  TLegend * combi_dg_legend = below_logo_legend();
  combi_dg_legend->AddEntry( combi_dg_hist, Form( "Signal, combi %s", unique.c_str() ) );
  combi_dg_legend->AddEntry( combi_dg, "Fit, Double gaussian" );
  combi_dg_legend->Draw();
  TPaveStats * combi_dg_stats = make_stats( combi_dg_hist );
  combi_dg_stats->Draw( "SAME" );

  combi_dg_canv->SaveAs( Form( "./combi_%s_dg.png", unique.c_str() ) );


}


void analysis_var_overlay( std::vector< basic_fileset *> & splits,  variable_set & variables, style_mgr * styles, const std::string & unique ){
  
  uint8_t split_count = splits.size();
  if ( split_count <= 1 ){ return; }

  std::vector<TH1F *> analysis_variable_hists_sign( splits.size() );
  std::vector<TH1F *> analysis_variable_hists_bckg( splits.size() );
  for ( uint8_t split_idx = 0; split_idx < split_count; split_idx++ ){
    std::string current_unique = unique + "-" + std::to_string( split_idx );
    hist_group current_hists = splits.at( split_idx )->recreate_histograms_weights( variables, true, false );
    analysis_variable_hists_sign.at( split_idx ) = current_hists.sign_hist;
    analysis_variable_hists_bckg.at( split_idx ) = current_hists.bckg_hist;
    styles->style_histogram( analysis_variable_hists_sign.at( split_idx ), "extracted_sign" );
    styles->style_histogram( analysis_variable_hists_bckg.at( split_idx ), "extracted_bckg" );
    analysis_variable_hists_sign.at( split_idx )->SetLineColorAlpha( kRed+1,  1.0 - split_idx/10.0 );
    analysis_variable_hists_bckg.at( split_idx )->SetLineColorAlpha( kBlue+1, 1.0 - split_idx/10.0 );
    analysis_variable_hists_sign.at( split_idx )->SetLineStyle( split_idx + 1 );
    analysis_variable_hists_bckg.at( split_idx )->SetLineStyle( split_idx + 1 );
    analysis_variable_hists_sign.at( split_idx )->SetLineWidth( 2 );
    analysis_variable_hists_bckg.at( split_idx )->SetLineWidth( 2 );
  }

  TCanvas * sign_overlay = new TCanvas( "sign_overlay", "", 200, 200, 1000, 1000 );
  sign_overlay->Divide( 1, 1 );
  TPad * sign_pad = (TPad *)  sign_overlay->cd( 1 );
  analysis_variable_hists_sign.at( 0 )->Draw( "HIST" );
  hist_prep_axes( analysis_variable_hists_sign.at( 0 ), true );
  set_axis_labels( analysis_variable_hists_sign.at( 0 ), variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( sign_pad, Form( "%s %s %s", variables.analysis_bound.get_ltx().c_str(), unique.c_str(), variables.mass.c_str() ) );
  add_atlas_decorations( sign_pad, true, false );
  TLegend * sign_legend = below_logo_legend();
  sign_legend->AddEntry( analysis_variable_hists_sign.at( 0 ), Form( "%s-1", unique.c_str() ) );
  for ( size_t split_idx = 1; split_idx < splits.size(); split_idx++ ){
    analysis_variable_hists_sign.at( split_idx )->Draw( "HIST SAME" );
    sign_legend->AddEntry( analysis_variable_hists_sign.at( split_idx ), Form( "%s-%i", unique.c_str(), (int) split_idx+1 ) );
  }
  sign_legend->Draw( "SAME" );
  sign_overlay->SaveAs( Form( "sign_overlay_%s_%s.png", variables.analysis_variable.c_str(), unique.c_str() ) );

  analysis_variable_hists_sign.at( 0 )->GetYaxis()->SetRangeUser( 1, analysis_variable_hists_sign.at( 0 )->GetMaximum()*100 );
  sign_pad->SetLogy();
  sign_overlay->SaveAs( Form( "sign_overlay_%s_%s_log.png", variables.analysis_variable.c_str(), unique.c_str() ) );


  TCanvas * bckg_overlay = new TCanvas( "bckg_overlay", "", 200, 200, 1000, 1000 );
  bckg_overlay->Divide( 1, 1 );
  TPad * bckg_pad = (TPad *)  bckg_overlay->cd( 1 );
  analysis_variable_hists_bckg.at( 0 )->Draw( "HIST" );
  hist_prep_axes( analysis_variable_hists_bckg.at( 0 ), true );
  set_axis_labels( analysis_variable_hists_bckg.at( 0 ), variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( bckg_pad, Form( "%s %s %s", variables.analysis_bound.get_ltx().c_str(), unique.c_str(), variables.mass.c_str() ) );
  add_atlas_decorations( bckg_pad, true, false );
  TLegend * bckg_legend = below_logo_legend();
  bckg_legend->AddEntry( analysis_variable_hists_bckg.at( 0 ), Form( "%s-1", unique.c_str() ) );
  for ( uint8_t split_idx = 1; split_idx < split_count; split_idx++ ){
    analysis_variable_hists_bckg.at( split_idx )->Draw( "HIST SAME" );
    bckg_legend->AddEntry( analysis_variable_hists_bckg.at( split_idx ), Form( "%s-%i", unique.c_str(), (int) split_idx+1 ) );
  }
  bckg_legend->Draw( "SAME" );
  bckg_overlay->SaveAs( Form( "bckg_overlay_%s_%s.png", variables.analysis_variable.c_str(), unique.c_str() ) );

  analysis_variable_hists_bckg.at( 0 )->GetYaxis()->SetRangeUser( 1, analysis_variable_hists_bckg.at( 0 )->GetMaximum()*100 );
  bckg_pad->SetLogy();
  bckg_overlay->SaveAs( Form( "bckg_overlay_%s_%s_log.png", variables.analysis_variable.c_str(), unique.c_str() ) );


}

void integral_plots( std::vector< basic_fileset *> & splits,  variable_set & variables, style_mgr * styles, const std::string & unique ){

  uint8_t split_count = splits.size();
  if ( split_count <= 1 ){ return; }

  TH1F * extr_sign_integral = new TH1F( "extr_sign_integral", "", split_count, 0, split_count ); 
  TH1F * extr_bckg_integral = new TH1F( "extr_bckg_integral", "", split_count, 0, split_count ); 
  TH1F * sign_integral = new TH1F( "sign_integral", "", split_count, 0, split_count ); 
  TH1F * bckg_integral = new TH1F( "bckg_integral", "", split_count, 0, split_count ); 

  styles->style_histogram( extr_sign_integral, "extracted_sign" );
  styles->style_histogram( extr_bckg_integral, "extracted_bckg" );
  styles->style_histogram( sign_integral, "extracted_sign" );
  styles->style_histogram( bckg_integral, "extracted_bckg" );


  for ( uint8_t split_idx = 0; split_idx < split_count; split_idx++ ){
    std::string current_unique = unique + "-" + std::to_string( split_idx );
    hist_group extr_hists = splits.at( split_idx )->recreate_histograms_weights( variables, true, false );
    extr_sign_integral->SetBinContent( split_idx+1, extr_hists.sign_hist->Integral() );
    extr_bckg_integral->SetBinContent( split_idx+1, extr_hists.bckg_hist->Integral() );
    extr_sign_integral->GetXaxis()->SetBinLabel( split_idx+1, Form( "%s-%i", unique.c_str(), split_idx + 1 ) );
    extr_bckg_integral->GetXaxis()->SetBinLabel( split_idx+1, Form( "%s-%i", unique.c_str(), split_idx + 1 ) );
    hist_group hists = splits.at( split_idx )->recreate_histograms_subtracted( variables, false, false );
    sign_integral->SetBinContent( split_idx+1, hists.sign_hist->Integral() );
    bckg_integral->SetBinContent( split_idx+1, hists.bckg_hist->Integral() );
    sign_integral->GetXaxis()->SetBinLabel( split_idx+1, Form( "%s-%i", unique.c_str(), split_idx + 1 ) );
    bckg_integral->GetXaxis()->SetBinLabel( split_idx+1, Form( "%s-%i", unique.c_str(), split_idx + 1 ) );

  }
  
  gStyle->SetOptStat( "krimes" );
  TCanvas * integral_canvas = new TCanvas( "integral_canv", "", 200, 200, 2000, 1000 );
  integral_canvas->Divide( 2, 1 );

  TPad * integral_extr_pad = (TPad *) integral_canvas->cd( 1 );
  TH1F * integral_extr_ratio = ratio_pad( extr_sign_integral, extr_bckg_integral, integral_extr_pad );
  TLegend * integral_extr_legend = below_logo_legend();
  add_pad_title( integral_extr_pad, Form( "Extracted - %s %s", variables.analysis_variable.c_str(), unique.c_str() ), false );
  add_atlas_decorations( integral_extr_pad, true, false );
  integral_extr_legend->AddEntry( extr_sign_integral, "sign", "L" );
  integral_extr_legend->AddEntry( extr_bckg_integral, "bckg", "L" );
  integral_extr_legend->AddEntry( integral_extr_ratio, "ratio", "L" );
  integral_extr_legend->Draw();

  TPad * integral_subt_pad = (TPad *) integral_canvas->cd( 2 );
  TH1F * integral_subt_ratio = ratio_pad( sign_integral, bckg_integral, integral_subt_pad );
  TLegend * integral_subt_legend = below_logo_legend();
  add_pad_title( integral_subt_pad, Form( "Subtraction - %s %s", variables.analysis_variable.c_str(), unique.c_str() ), false );
  add_atlas_decorations( integral_subt_pad, true, false );
  integral_subt_legend->AddEntry( sign_integral, "sign", "L" );
  integral_subt_legend->AddEntry( bckg_integral, "bckg", "L" );
  integral_subt_legend->AddEntry( integral_subt_ratio, "ratio", "L" );
  integral_subt_legend->Draw();

  integral_canvas->SaveAs( Form( "integral_%s.png", variables.analysis_variable.c_str() ) );

}

void q_plot( basic_fileset * fileset, variable_set & variables, style_mgr * styles ){
  
  const char * unique = fileset->get_unique();
  
  hist_group hists = fileset->recreate_histograms_weights( variables, false, false, "Q" );
  TH1F * sign = hists.sign_hist;
  TH1F * bckg = hists.bckg_hist;

  styles->style_histogram( sign, "extracted_sign" );
  styles->style_histogram( bckg, "extracted_bckg" );

  gStyle->SetOptStat( "krimes" );
  TCanvas * q_canvas = new TCanvas( Form( "%s_canv", unique ) , "", 200, 200, 1000, 1000 );
  q_canvas->Divide( 1, 1 );
  TPad * q_pad = (TPad *) q_canvas->cd( 1 );
  TH1F * q_ratio = ratio_pad( sign, bckg, q_pad );
  TLegend * q_legend = below_logo_legend();
  add_pad_title( q_pad, Form( "Q - %s", unique ), false );
  add_atlas_decorations( q_pad, true, false );
  q_legend->AddEntry( sign, "sign", "L" );
  q_legend->AddEntry( bckg, "bckg", "L" );
  q_legend->AddEntry( q_ratio, "ratio", "L" );
  q_legend->Draw();
  q_canvas->SaveAs( Form( "q_%s.png", unique ) );

}

void qt_map( basic_fileset * fileset, variable_set & variables ){

  const char * unique = fileset->get_unique();

  hist_group_2d hists = fileset->recreate_map_histograms_weights( variables, false, "qtA", "qtB"  );
  TH2F * sign = hists.sign_hist;
  TH2F * bckg = hists.bckg_hist;
  sign->SetName( "sign" );
  bckg->SetName( "bckg" );
  gStyle->SetOptStat( "ourmines" );

  TCanvas * qt_canvas = new TCanvas( Form( "%s_qt_canv", unique ) , "", 200, 200, 2000, 1000 );
  qt_canvas->Divide( 2, 1 );

  TPad * qt_pad = (TPad *) qt_canvas->cd( 1 );
  sign->Draw( "COLZ" );
  hist_prep_axes( sign, true );
  set_axis_labels( sign, "q_{T}^{A}", "q_{T}^{B}" );
  add_pad_title( qt_pad, Form( "Signal weight q_{T}^{A} vs q_{T}^{B} %s", unique ) );
  add_atlas_decorations( qt_pad, true, false );
  TPaveStats * sign_stats = make_stats( sign );
  sign_stats->Draw( "SAME" );

  qt_pad = (TPad *) qt_canvas->cd( 2 );
  bckg->Draw( "COLZ" );
  hist_prep_axes( bckg, true );
  set_axis_labels( bckg, "q_{T}^{A}", "q_{T}^{B}" );
  add_pad_title( qt_pad, Form( "Background weight q_{T}^{A} vs q_{T}^{B} %s", unique ) );
  add_atlas_decorations( qt_pad, true, false );
  TPaveStats * bckg_stats = make_stats( bckg );
  bckg_stats->Draw( "SAME" );

  qt_canvas->SaveAs( Form( "qt_%s.png", unique ) );

}

// generalise it to be var vs var
// make a diff mirror of this.
void qt_combo_weight( std::vector< basic_fileset *> & splits,  variable_set & variables, const std::string & unique_str ){


  gStyle->SetOptStat( "ourmines" );
  const char * unique =  unique_str.c_str();
  bound & x_axis = variables.bound_manager->get_bound( "qtA" );
  bound & y_axis = variables.bound_manager->get_bound( "qtB" );
  TH2F * sign = x_axis.get_2d_hist( "sign_all", y_axis );
  TH2F * bckg = x_axis.get_2d_hist( "bckg_all", y_axis );
  sign->SetName( "sign" );
  bckg->SetName( "bckg" );

  for ( basic_fileset * split : splits ){

    hist_group_2d hists = split->recreate_map_histograms_weights( variables, false, "qtA", "qtB"  );
    sign->Add( hists.sign_hist, 1.0 );
    bckg->Add( hists.bckg_hist, 1.0 );

  }

  TCanvas * qt_canvas = new TCanvas( Form( "%s_all_qt_canv", unique ) , "", 200, 200, 2000, 1000 );
  qt_canvas->Divide( 2, 1 );

  TPad * qt_pad = (TPad *) qt_canvas->cd( 1 );
  sign->Draw( "COLZ" );
  hist_prep_axes( sign, true );
  set_axis_labels( sign, "q_{T}^{A}", "q_{T}^{B}" );
  add_pad_title( qt_pad, Form( "Signal weight q_{T}^{A} vs q_{T}^{B} %s", unique ) );
  add_atlas_decorations( qt_pad, true, false );
  TPaveStats * sign_stats = make_stats( sign );
  sign_stats->Draw( "SAME" );

  qt_pad = (TPad *) qt_canvas->cd( 2 );
  bckg->Draw( "COLZ" );
  hist_prep_axes( bckg, true );
  set_axis_labels( bckg, "q_{T}^{A}", "q_{T}^{B}" );
  add_pad_title( qt_pad, Form( "Background weight q_{T}^{A} vs q_{T}^{B} %s", unique ) );
  add_atlas_decorations( qt_pad, true, false );
  TPaveStats * bckg_stats = make_stats( bckg );
  bckg_stats->Draw( "SAME" );

  qt_canvas->SaveAs( Form( "qt_combo_%s.png", unique )  );

}

//void combo_2d_general( std::vector< basic_fileset *> & splits,  variable_set & variables, const std::string & unique_str, const std::string & x_variable, const std::string & y_variable, bool diff=false ){
//
//
//  gStyle->SetOptStat( "ourmines" );
//  const char * unique =  unique_str.c_str();
//  bound x_axis = variables.bound_manager->get_bound( x_variable );
//  bound y_axis = variables.bound_manager->get_bound( y_variable );
//  //const char * x_var_c = x_axis.get_ltx().c_str();
//  //const char * y_var_c = y_axis.get_ltx().c_str();
//
//  TH2F * sign = x_axis.get_2d_hist( "sign_all", y_axis );
//  TH2F * bckg = x_axis.get_2d_hist( "bckg_all", y_axis );
//  sign->SetName( "sign" );
//  bckg->SetName( "bckg" );
//
//  for ( basic_fileset * split : splits ){
//
//    // need 2d diff recreation
//    hist_group_2d hists;
//    if ( diff ){
//      split->recreate_map_histograms_weights( variables, false, x_variable, y_variable ); 
//    } else { 
//      split->recreate_map_histograms_weights( variables, false, x_variable, y_variable );
//    }
//    sign->Add( hists.sign_hist, 1.0 );
//    bckg->Add( hists.bckg_hist, 1.0 );
//
//  }
//
//  TCanvas * qt_canvas = new TCanvas( Form( "%s_all_qt_canv", unique ) , "", 200, 200, 2000, 1000 );
//  qt_canvas->Divide( 2, 1 );
//
//  TPad * qt_pad = (TPad *) qt_canvas->cd( 1 );
//  sign->Draw( "COLZ" );
//  hist_prep_axes( sign, true );
//  set_axis_labels( sign, "q_{T}^{A}", "q_{T}^{B}" );
//  add_pad_title( qt_pad, Form( "Signal weight q_{T}^{A} vs q_{T}^{B} %s", unique ) );
//  add_atlas_decorations( qt_pad, true, false );
//  TPaveStats * sign_stats = make_stats( sign );
//  sign_stats->Draw( "SAME" );
//
//  qt_pad = (TPad *) qt_canvas->cd( 2 );
//  bckg->Draw( "COLZ" );
//  hist_prep_axes( bckg, true );
//  set_axis_labels( bckg, "q_{T}^{A}", "q_{T}^{B}" );
//  add_pad_title( qt_pad, Form( "Background weight q_{T}^{A} vs q_{T}^{B} %s", unique ) );
//  add_atlas_decorations( qt_pad, true, false );
//  TPaveStats * bckg_stats = make_stats( bckg );
//  bckg_stats->Draw( "SAME" );
//
//  qt_canvas->SaveAs( Form( "%s_%s_combo_%s.png", unique )  );
//
//}

void x_combo( std::vector< basic_fileset *> splits,  variable_set & variables, const std::string & unique_str ){

  gStyle->SetOptStat( "ourmines" );
  const char * unique =  unique_str.c_str();

  std::string mass_cut = variables.bound_manager->get_bound( "Q12" ).get_cut();
  double bin_edges[14] = { 5.62e-5, 1e-4, 1.778e-4, 3.16e-4, 5.62e-4, 1e-3, 1.778e-3, 3.16e-3, 5.62e-3, 1e-2, 1.778e-2, 3.16e-2, 5.62e-2, 1.0e-1 };

  TH2F * compare_bjorken = new TH2F( "compare_bjorken", "", 13, bin_edges, 13, bin_edges );
  TH2F * bjorken_components = new TH2F( "bjorken_components", "", 30, -3, 3, 60, 0.0005, 0.004 );

  const char * component_draw = "Q/13000:Y>>+bjorken_components";
  const char * x_draw = "(Q/13000)*exp(Y):(Q/13000)*exp(-Y)>>+compare_bjorken";
  const char * cut = "abs(Y)<2.5&&(DiMuonMass>3000&&DiMuonMass<3200)";

  for ( basic_fileset * split : splits ){
    ( (TTree * ) split->efficiency_file->Get( "reco_tree" ) )->Draw( component_draw, cut );
    ( (TTree * ) split->efficiency_file->Get( "reco_tree" ) )->Draw( x_draw, cut );
  }
  TH1D * rapidity = bjorken_components->ProjectionX();
  TH1D * qs = bjorken_components->ProjectionY();


  TCanvas * bjorken_canvas = new TCanvas( "bjorken_canvas", "", 200, 200, 2000, 2000 );
  bjorken_canvas->Divide( 2, 2 );

  TPad * bjorken_pad = (TPad *) bjorken_canvas->cd( 1 );
  compare_bjorken->Draw( "COLZ" );
  bjorken_pad->SetLogx();
  bjorken_pad->SetLogy();
  hist_prep_axes( compare_bjorken, true );
  add_pad_title( bjorken_pad, "x" );
  set_axis_labels( compare_bjorken, "x_{2}", "x_{1}" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * bjorken_stats = make_stats( compare_bjorken, false, false );
  bjorken_stats->Draw();


  bjorken_pad = (TPad *) bjorken_canvas->cd( 2 );
  bjorken_components->Draw( "COLZ" );
  hist_prep_axes( bjorken_components, true );
  add_pad_title( bjorken_pad, "x components" );
  set_axis_labels( bjorken_components, "y", "Q/#sqrt{s}" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * component_stats = make_stats( bjorken_components, false, false );
  component_stats->Draw();
  
  
  bjorken_pad = (TPad *) bjorken_canvas->cd( 3 );
  rapidity->Draw( "HIST" );
  hist_prep_axes( rapidity, true );
  add_pad_title( bjorken_pad, "Rapidity" );
  set_axis_labels( rapidity, "y", "Yield" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * rapidity_stats = make_stats( rapidity, false, false );
  rapidity_stats->Draw();

  bjorken_pad = (TPad *) bjorken_canvas->cd( 4 );
  qs->Draw( "HIST" );
  hist_prep_axes( qs, true );
  add_pad_title( bjorken_pad, "Q/#sqrt{s}" );
  set_axis_labels( qs, "Q/#sqrt{s}", "Yield" );
  add_atlas_decorations( bjorken_pad, true, true );
  TPaveStats * qs_stats = make_stats( qs, false, false );
  qs_stats->Draw();

  bjorken_canvas->SaveAs( Form( "%s_x.png", unique ) );

}

void fit_check( basic_fileset * fileset, variable_set & variables, style_mgr * styles, const std::string & check_variable ){

  //const char * mass = variables.mass.c_str();
  const char * unique = fileset->get_unique();
  bound & variable = variables.bound_manager->get_bound( check_variable );
  hist_group hists = fileset->recreate_histograms_weights( variables, false, false, check_variable );
  hist_group sub_hists = fileset->recreate_histograms_subtracted( variables, false, false, check_variable );
  TH1F * sign = hists.sign_hist;
  TH1F * bckg = hists.bckg_hist;
  TH1F * data = hists.data_hist;
  TH1F * fit = (TH1F *) sign->Clone();
  TH1F * sub_sign = sub_hists.sign_hist;
  TH1F * sub_bckg = sub_hists.bckg_hist;
  fit->Reset();
  fit->Add( sign, bckg, 1.0, 1.0 );
  sign->SetName( "sign" );
  bckg->SetName( "bckg" );
  sub_sign->SetName( "sub_sign" );
  sub_bckg->SetName( "sub_bckg" );
  fit->SetName( "fit" );
  data->SetName( "data" );

  styles->set_stage( "default" );
  styles->style_histogram( sign,      "extracted_sign" );
  styles->style_histogram( bckg,      "extracted_bckg" );
  styles->style_histogram( sub_sign,  "extracted_sign" );
  styles->style_histogram( sub_bckg,  "extracted_bckg" );
  styles->style_histogram( fit,       "extracted_data" );
  styles->style_histogram( data,      "raw_data" );

  gStyle->SetOptStat( "krimens" );
  TCanvas * fit_canv = new TCanvas( Form( "fit_canv_%s_%s", unique, check_variable.c_str() ), "", 200, 200, 2000, 2000 );
  fit_canv->Divide( 2, 2 );

  TPad * fit_pad = (TPad *) fit_canv->cd( 1 );
  sign->Draw( "HIST E1" );
  bckg->Draw( "HIST E1 SAMES" );
  fit->Draw( "HIST E1 SAME" );
  sign->GetYaxis()->SetRangeUser( 0, fit->GetMaximum()*1.5 );
  hist_prep_axes( sign );
  set_axis_labels( sign, variable.get_x_str(), Form( "Yield%s", variable.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s - %s", variable.get_ltx().c_str(), unique ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * fit_legend = below_logo_legend();
  fit_legend->AddEntry( sign, "sign" );
  fit_legend->AddEntry( bckg, "bckg" );
  fit_legend->AddEntry( fit, "fit" );
  fit_legend->Draw( "SAME" );
  TPaveStats * sign_stats = make_stats( sign, true, false );
  sign_stats->Draw( "SAME" );
  TPaveStats * bckg_stats = make_stats( bckg, true, true );
  bckg_stats->Draw( "SAME" );

  fit_pad = (TPad *) fit_canv->cd( 2 );
  TH1F * fit_ratio = ratio_pad( data, fit, fit_pad );
  set_axis_labels( fit_ratio, variable.get_x_str(), "" );
  set_axis_labels( data, "", Form( "Yield%s", variable.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s - %s", variable.get_ltx().c_str(), unique ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * comp_legend = below_logo_legend();
  comp_legend->AddEntry( fit, "fit" );
  comp_legend->AddEntry( data, "data" );
  comp_legend->AddEntry( fit_ratio, "ratio" );
  comp_legend->Draw( "SAME" );


  fit_pad = (TPad *) fit_canv->cd( 3 );
  sub_sign->Draw( "HIST E1" );
  sub_bckg->Draw( "HIST E1 SAMES" );
  hist_prep_axes( sub_sign );
  set_axis_labels( sub_sign, variable.get_x_str(), Form( "Yield%s", variable.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s - %s", variable.get_ltx().c_str(), unique ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * sub_legend = below_logo_legend();
  sub_legend->AddEntry( sub_sign, "sub_sign" );
  sub_legend->AddEntry( sub_bckg, "sub_bckg" );
  sub_legend->Draw( "SAME" );
  TPaveStats * sub_sign_stats = make_stats( sub_sign, true, false );
  sub_sign_stats->Draw( "SAME" );
  TPaveStats * sub_bckg_stats = make_stats( sub_bckg, true, true );
  sub_bckg_stats->Draw( "SAME" );


  fit_canv->SaveAs( Form( "fit_check_%s_%s.png", unique, check_variable.c_str() ) );
  
  delete fit_canv;


}


void fit_check_combo( std::vector< basic_fileset * > & splits, variable_set & variables, style_mgr * styles, const std::string & check_variable, const std::string & unique ){

  //const char * mass = variables.mass.c_str();
  const char * unique_c = unique.c_str();
  bound & variable = variables.bound_manager->get_bound( check_variable );

  TH1F * sign = variable.get_hist( Form( "combo_sign_%s", unique_c ) );
  TH1F * bckg = variable.get_hist( Form( "combo_bckg_%s", unique_c ) );
  TH1F * data = variable.get_hist( Form( "combo_data_%s", unique_c ) );
  TH1F * fit = (TH1F *) sign->Clone();
  TH1F * sub_sign = variable.get_hist( Form( "combo_sub_sign_%s", unique_c ) );
  TH1F * sub_bckg = variable.get_hist( Form( "combo_sub_bckg_%s", unique_c ) );
  TH1F * sign_ratio = variable.get_hist( Form( "sign_ratio_%s", unique_c ) ); 
  TH1F * bckg_ratio = variable.get_hist( Form( "bckg_ratio_%s", unique_c ) ); 


  for ( basic_fileset * fileset : splits ){

    hist_group hists = fileset->recreate_histograms_weights( variables, false, false, check_variable );
    hist_group sub_hists = fileset->recreate_histograms_subtracted( variables, false, false, check_variable );
    sign->Add( hists.sign_hist );
    bckg->Add( hists.bckg_hist );
    data->Add( hists.data_hist );
    fit->Add( hists.sign_hist );
    fit->Add( hists.bckg_hist );
    sub_sign->Add( sub_hists.sign_hist );
    sub_bckg->Add( sub_hists.bckg_hist );
  }

  sign_ratio->Divide( sign, sub_sign );
  sign_ratio->SetName( "sign_ratio" );
  bckg_ratio->Divide( bckg, sub_bckg );
  bckg_ratio->SetName( "bckg_ratio" );

  styles->set_stage( "default" );
  styles->style_histogram( sign,        "extracted_sign" );
  styles->style_histogram( bckg,        "extracted_bckg" );
  styles->style_histogram( sub_sign,    "extracted_sign" );
  styles->style_histogram( sign_ratio,  "extracted_sign" );
  styles->style_histogram( sub_bckg,    "extracted_bckg" );
  styles->style_histogram( bckg_ratio,  "extracted_bckg" );
  styles->style_histogram( fit,         "extracted_data" );
  styles->style_histogram( data,        "raw_data" );


  gStyle->SetOptStat( "krimens" );
  TCanvas * fit_canv = new TCanvas( Form( "fit_combined_canv_%s_%s", unique_c, check_variable.c_str() ), "", 200, 200, 2000, 2000 );
  fit_canv->Divide( 2, 2 );

  TPad * fit_pad = (TPad *) fit_canv->cd( 1 );
  sign->Draw( "HIST E1" );
  bckg->Draw( "HIST E1 SAMES" );
  fit->Draw( "HIST E1 SAME" );
  sign->GetYaxis()->SetRangeUser( 0, fit->GetMaximum()*1.5 );
  hist_prep_axes( sign );
  set_axis_labels( sign, variable.get_x_str(), Form( "Yield%s", variable.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s - %s", variable.get_ltx().c_str(), unique_c ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * fit_legend = below_logo_legend();
  fit_legend->AddEntry( sign, "sign" );
  fit_legend->AddEntry( bckg, "bckg" );
  fit_legend->AddEntry( fit, "fit" );
  fit_legend->Draw( "SAME" );
  TPaveStats * sign_stats = make_stats( sign, true, false );
  sign_stats->Draw( "SAME" );
  TPaveStats * bckg_stats = make_stats( bckg, true, true );
  bckg_stats->Draw( "SAME" );

  fit_pad = (TPad *) fit_canv->cd( 2 );
  TH1F * fit_ratio = ratio_pad( data, fit, fit_pad );
  set_axis_labels( fit_ratio, variable.get_x_str(), "" );
  set_axis_labels( data, "", Form( "Yield%s", variable.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s - %s", variable.get_ltx().c_str(), unique_c ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * comp_legend = below_logo_legend();
  comp_legend->AddEntry( fit, "fit" );
  comp_legend->AddEntry( data, "data" );
  comp_legend->AddEntry( fit_ratio, "ratio" );
  comp_legend->Draw( "SAME" );


  fit_pad = (TPad *) fit_canv->cd( 3 );
  sign_ratio->Draw( "HIST E1" );
  hist_prep_axes( sign_ratio );
  set_axis_labels( sign_ratio, variable.get_x_str(), Form( "Yield%s", variable.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "extr/MC %s - %s", variable.get_ltx().c_str(), unique_c ), false );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * sign_ratio_legend = below_logo_legend();
  sign_ratio_legend->AddEntry( sign_ratio, "sub_sign" );
  sign_ratio_legend->Draw( "SAME" );
  TPaveStats * sign_ratio_stats = make_stats( sign_ratio );
  sign_ratio_stats->Draw( "SAME" );
  sign_ratio->GetYaxis()->SetRangeUser( 1e-3, 1000 );
  fit_pad->SetLogy();

  fit_pad = (TPad *) fit_canv->cd( 4 );
  bckg_ratio->Draw( "HIST E1" );
  hist_prep_axes( bckg_ratio );
  set_axis_labels( bckg_ratio, variable.get_x_str(), Form( "Yield%s", variable.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "extr/MC %s - %s", variable.get_ltx().c_str(), unique_c ), false );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * bckg_ratio_legend = below_logo_legend();
  bckg_ratio_legend->AddEntry( bckg_ratio, "sub_bckg" );
  bckg_ratio_legend->Draw( "SAME" );
  TPaveStats * bckg_ratio_stats = make_stats( bckg_ratio );
  bckg_ratio_stats->Draw( "SAME" );
  bckg_ratio->GetYaxis()->SetRangeUser( 1e-1, 1000 );
  fit_pad->SetLogy();

  fit_canv->SaveAs( Form( "combined_fit_check_%s_%s.png", unique_c, check_variable.c_str() ) );

  TFile * ratio_store = new TFile( "ratios.root", "UPDATE" );
  sign_ratio->Write( Form ( "sign_ratio_%s_%s", check_variable.c_str(), unique_c ) );
  bckg_ratio->Write( Form ( "bckg_ratio_%s_%s", check_variable.c_str(), unique_c ) );
  ratio_store->Close();
  
  delete ratio_store;
  delete fit_canv;
  delete sign;
  delete bckg;
  delete data;
  delete fit;
  delete fit_ratio;
  delete sub_sign;
  delete sub_bckg;
  delete sign_ratio;
  delete bckg_ratio;

}


void split_extr( std::vector< basic_fileset *> splits,  variable_set & variables, const std::string & unique ){

  prep_style();
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "default" );

  std::vector< basic_fileset *> lower, upper;
  std::copy( splits.begin(), splits.begin()+3, std::back_inserter( lower ) );
  std::copy( splits.begin()+3, splits.end(), std::back_inserter( upper ) );
  
  // full set
  //analysis_var_overlay( splits, variables, styles, unique );
  //integral_plots( splits, variables, styles, unique );
  //qt_combo_weight( splits, variables, unique );
  //x_combo( splits, variables, unique );
  //fit_check_combo( splits, variables, styles, "qtA", unique );
  //fit_check_combo( splits, variables, styles, "Q", unique );
  //fit_check_combo( splits, variables, styles, "DiMuonPt", unique );
  //fit_check_combo( splits, variables, styles, "PhotonPt", unique );

  weight_analysis_combo( splits, variables, styles, "qtA", unique );
  //weight_analysis_combo( lower, variables, styles, "qtA", "qtbsplit_lower" );


  fit_check_combo( lower, variables, styles, "qtA", "qtbsplit_lower" );
  //fit_check_combo( lower, variables, styles, "Q",   "qtbsplit_lower" );
  //fit_check_combo( lower, variables, styles, "DiMuonPt", "qtbsplit_lower" );
  //fit_check_combo( lower, variables, styles, "PhotonPt", "qtbsplit_lower" );



  //analysis_var_overlay( lower, variables, styles, "qtbsplit_lower" );
  //integral_plots( lower, variables, styles, "qtbsplit_lower" );
  //qt_combo_weight( lower, variables, "qtbsplit_lower" );
  //x_combo( lower, variables, "qtbsplit_lower" );


  //analysis_var_overlay( upper, variables, styles, "qtbsplit_upper" );
  //integral_plots( upper, variables, styles, "qtbsplit_upper" );
  //qt_combo_weight( upper, variables, "qtbsplit_upper" );
  //x_combo( upper, variables, "qtbsplit_upper" );

  //weight_analysis( splits.at( 0 ), variables, styles, "qtA" );
  for ( basic_fileset * fileset : splits ){
    weight_analysis( fileset, variables, styles, "qtA" );
    //weight_analysis( fileset, variables, styles, "Q" );
    //weight_analysis( fileset, variables, styles, "PhotonPt" );
    //weight_analysis( fileset, variables, styles, "DiMuonPt" );
  }
  //  fit_check( fileset, variables, styles, "qtA" );
  //  fit_check( fileset, variables, styles, "DiMuonPt" );
  //  fit_check( fileset, variables, styles, "PhotonPt" );
  //  fit_check( fileset, variables, styles, "qtB" );
  //  q_plot( fileset, variables, styles );
  //  qt_map( fileset, variables );
  //}

}
