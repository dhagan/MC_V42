#include <hf_extr_plotters.hxx>
#include <TF2.h>

void fit_process( TH1F * sign_hist, TH1F * bckg_hist, const char * unique, variable_set & variables ){

  std::string & mass = variables.mass;
  bound & analysis_bound = variables.analysis_bound;
  
  TF1 * sign_sg = prep_sg( analysis_bound.get_min(), analysis_bound.get_max() ); 
  TF1 * bckg_dg = prep_dg( analysis_bound.get_min(), analysis_bound.get_max() );
  TF1 * bckg_sg = prep_sg( analysis_bound.get_min(), analysis_bound.get_max() );

  //sign_dg->SetParLimits( 0, 0, 1e6 );
  //sign_dg->SetParLimits( 3, 0, 1e6 );
  //bckg_dg->SetParLimits( 0, 0, 1e6 );
  //bckg_dg->SetParLimits( 3, 0, 1e6 );
  //sign_sg->SetParLimits( 0, 0, 1e6 );
  //bckg_sg->SetParLimits( 3, 0, 1e6 );

  TH1F * sign_dg_hist = (TH1F *) sign_hist->Clone();
  TH1F * sign_sg_hist = (TH1F *) sign_hist->Clone();
  TH1F * bckg_dg_hist = (TH1F *) bckg_hist->Clone();
  TH1F * bckg_sg_hist = (TH1F *) bckg_hist->Clone();

  gStyle->SetOptStat( "krims" );

  fit_mgr * fit_manager = new fit_mgr();
  fit_manager->load_fits();

  std::string sign_fit_key = "event_extract_fit_process" + analysis_bound.get_var() + "_" + mass + "_" + unique ;



  TCanvas * fit_canv = new TCanvas( Form( "fit_canv_%s_%s", unique, mass.c_str() ), "", 200, 200, 2000, 2000 );
  fit_canv->Divide( 2, 2 );

  TPad * fit_pad;

  fit_pad = (TPad *) fit_canv->cd( 1 );
  sign_dg_hist->Draw( "HIST E1" );
  TF1 * sign_dg = prep_dg( -5.0, 14 );

  sign_dg->SetParLimits( 0, 0, sign_dg_hist->GetMaximum() );
  sign_dg->SetParLimits( 2, 2, 10 );
  sign_dg->SetParLimits( 3, 0, sign_dg_hist->GetMaximum() );
  sign_dg->SetParLimits( 4, 10, 20 );

  
  sign_dg_hist->Fit( sign_dg, "MR", "", -5.0, 14 );
  sign_dg->Draw( "SAME" );
  hist_prep_axes( sign_dg_hist );
  set_axis_labels( sign_dg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s %s %s", unique, analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * sign_dg_legend = below_logo_legend();
  sign_dg_legend->AddEntry( sign_dg_hist, Form( "Signal %s", unique ) );
  sign_dg_legend->AddEntry( sign_dg, "Fit, Double gaussian" );
  sign_dg_legend->Draw();
  TPaveStats * sign_dg_stats = make_stats( sign_dg_hist );
  sign_dg_stats->Draw( "SAME" );

  fit_manager->save_parameterisation( sign_dg, sign_fit_key, "dg" );
  fit_manager->save_fits();


  fit_pad = (TPad *) fit_canv->cd( 2 );
  sign_sg_hist->Draw( "HIST E1" );
  //align_sg( sign_sg, sign_sg_hist, false );
  sign_sg_hist->Fit( sign_sg, "MQR", "", analysis_bound.get_min(), analysis_bound.get_max() );
  sign_sg->Draw( "SAME" );
  hist_prep_axes( sign_sg_hist );
  set_axis_labels( sign_sg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s %s %s", unique, analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * sign_sg_legend = below_logo_legend();
  sign_sg_legend->AddEntry( sign_sg_hist, Form( "Signal %s", unique ) );
  sign_sg_legend->AddEntry( sign_sg, "Fit, Single gaussian" );
  sign_sg_legend->Draw();
  TPaveStats * sign_sg_stats = make_stats( sign_sg_hist );
  sign_sg_stats->Draw( "SAME" );



  fit_pad = (TPad *) fit_canv->cd( 3 );
  bckg_dg_hist->Draw( "HIST E1" );
  align_sg( bckg_dg, bckg_dg_hist, false );
  bckg_dg_hist->Fit( bckg_dg, "MQR", "", analysis_bound.get_min(), analysis_bound.get_max() );
  bckg_dg->Draw( "SAME" );
  hist_prep_axes( bckg_dg_hist  );
  set_axis_labels( bckg_dg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s %s %s", unique, analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * bckg_dg_legend = below_logo_legend();
  bckg_dg_legend->AddEntry( bckg_dg_hist, Form( "Background %s", unique ) );
  bckg_dg_legend->AddEntry( bckg_dg, "Fit, Double gaussian", "L" );
  bckg_dg_legend->Draw();
  TPaveStats * bckg_dg_stats = make_stats( bckg_dg_hist );
  bckg_dg_stats->Draw( "SAME" );

  fit_pad = (TPad *) fit_canv->cd( 4 );
  bckg_sg_hist->Draw( "HIST E1" );
  align_sg( bckg_sg, bckg_sg_hist, false );
  bckg_sg_hist->Fit( bckg_sg, "MQR", "", analysis_bound.get_min(), analysis_bound.get_max() );
  bckg_sg->Draw( "SAME" );
  hist_prep_axes( bckg_sg_hist  );
  set_axis_labels( bckg_sg_hist, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
  add_pad_title( fit_pad, Form( "%s %s %s", unique, analysis_bound.get_ltx().c_str(), mass.c_str() ) );
  add_atlas_decorations( fit_pad, true, false );
  TLegend * bckg_sg_legend = below_logo_legend();
  bckg_sg_legend->AddEntry( bckg_sg_hist, Form( "Background %s", unique ) );
  bckg_sg_legend->AddEntry( bckg_sg, "Fit, Single gaussian" );
  bckg_sg_legend->Draw();
  TPaveStats * bckg_sg_stats = make_stats( bckg_sg_hist );
  bckg_sg_stats->Draw( "SAME" );

  fit_canv->SaveAs( Form( "./fits_%s_%s_%s.png", analysis_bound.get_var().c_str(), mass.c_str(), unique ) );

}

void sign_extraction( TH1F * sign_extracted, const char * unique, variable_set & variables ){

  gStyle->SetOptStat( "krimes" );

  std::string & mass = variables.mass;

  TCanvas * sign_extr_canv  = new TCanvas( Form( "sign_extr_canv_%s_%s", unique, mass.c_str() ), "", 100, 100, 1000, 1000 );
  sign_extr_canv->Divide( 1, 1 );

  TPad * current_pad = (TPad *) sign_extr_canv->cd( 1 );
  sign_extracted->Draw( "HIST E1" );
  hist_prep_axes( sign_extracted, true );
  set_axis_labels( sign_extracted, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_pad, Form( "Signal - Weight, %s %s", variables.analysis_variable.c_str(), mass.c_str() ) );
  add_atlas_decorations( current_pad, true, false );
  TPaveStats * sign_extracted_stats = make_stats( sign_extracted );
  sign_extracted_stats->Draw();
  sign_extr_canv->SaveAs( Form("./extracted_signal_%s_%s_%s.png", mass.c_str(), variables.analysis_variable.c_str(), unique ) ); 

}

void bckg_extraction( TH1F * bckg_extracted, const char * unique, variable_set & variables ){

  gStyle->SetOptStat( "krimes" );

  std::string & mass = variables.mass;

  TCanvas * bckg_extr_canv  = new TCanvas( Form( "bckg_extr_canv_%s_%s", unique, mass.c_str() ), "", 100, 100, 1000, 1000 );
  bckg_extr_canv->Divide( 1, 1 );

  TPad * current_pad = (TPad *) bckg_extr_canv->cd( 1 );
  bckg_extracted->Draw( "HIST E1" );
  hist_prep_axes( bckg_extracted, true );
  set_axis_labels( bckg_extracted, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_pad, Form( "Background - Weight, %s %s", variables.analysis_variable.c_str(), mass.c_str() ) );
  add_atlas_decorations( current_pad, true, false );
  TPaveStats * bckg_extracted_stats = make_stats( bckg_extracted );
  bckg_extracted_stats->Draw();
  bckg_extr_canv->SaveAs( Form("./extracted_background_%s_%s_%s.png", mass.c_str(), variables.analysis_variable.c_str(), unique ) ); 

}

void weight_bckg_extraction( TH1F * bckg_extracted, basic_fileset & input, variable_set & variables ){

  const char * unique = input.get_unique();
  std::string & mass = variables.mass;

  TCanvas * sign_extr_canv  = new TCanvas( Form( "bckg_extr_canv_%s_%s", unique, mass.c_str() ), "", 100, 100, 1000, 1000 );
  sign_extr_canv->Divide( 1, 1 );

  TPad * current_pad = (TPad *) sign_extr_canv->cd( 1 );
  bckg_extracted->Draw( "HIST E1" );
  hist_prep_axes( bckg_extracted, true );
  set_axis_labels( bckg_extracted, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( current_pad, Form( "Background - Weight, %s %s", variables.analysis_variable.c_str(), mass.c_str() ) );
  add_atlas_decorations( current_pad, true, false );
  TPaveStats * sign_extracted_stats = make_stats( bckg_extracted );
  sign_extracted_stats->Draw();
  sign_extr_canv->SaveAs( Form("./extracted_background_%s_%s_%s.png", mass.c_str(), variables.analysis_variable.c_str(), unique ) ); 

}

void shape_extraction( TH1F * sign_extracted_normalised, TH1F * bckg_extracted_normalised, const char * unique, variable_set & variables ){

  const char * mass = variables.mass.c_str();

  TCanvas * norm_canv = new TCanvas( Form( "norm_canv_%s_%s", unique, mass ), "", 100, 100, 1000, 1000 );
  norm_canv->Divide( 1, 1 );
  TPad * norm_pad = (TPad *) norm_canv->cd( 1 );
  sign_extracted_normalised->Draw( "HIST E1" );
  bckg_extracted_normalised->Draw( "HIST E1 SAMES" );
  hist_prep_axes( sign_extracted_normalised, true );
  set_axis_labels( sign_extracted_normalised, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( norm_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), mass ), false );
  add_atlas_decorations( norm_pad, true, false );
  TLegend * norm_legend = below_logo_legend();
  norm_legend->AddEntry( sign_extracted_normalised, "sign", "LP" );
  norm_legend->AddEntry( bckg_extracted_normalised, "bckg", "LP" );
  norm_legend->Draw();
  TPaveStats * norm_sign_stats = make_stats( sign_extracted_normalised, true, true );
  norm_sign_stats->Draw();
  TPaveStats * norm_bckg_stats = make_stats( bckg_extracted_normalised, true );
  norm_bckg_stats->Draw();

  norm_canv->SaveAs( Form("./extracted_shape_comparison_%s_%s_%s.png", mass, variables.analysis_variable.c_str(), unique ) );

}


void data_comparison( TH1F * sign_extracted, TH1F * bckg_extracted, TH1F * data_extracted, THStack * sign_bckg_stack, TH1F * data_raw, const char * unique, variable_set & variables ){

  std::string & mass = variables.mass;

  gStyle->SetOptStat( "" );
  TCanvas * combination_canv = new TCanvas( Form( "combination_%s_%s", unique, mass.c_str() ), "", 100, 100, 2000, 1000 );
  combination_canv->Divide( 2, 1 );

  TPad * combination_pad = (TPad *) combination_canv->cd( 1 );
  data_raw->Draw( "HIST E1" );
  bckg_extracted->Draw( "HIST E1 SAMES" );
  sign_extracted->Draw( "HIST E1 SAMES" );
  data_extracted->Draw( "HIST E1 SAME" );
  hist_prep_axes( data_raw, true );
  set_axis_labels( data_raw, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( combination_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), mass.c_str() ) );
  add_atlas_decorations( combination_pad, true, false );
  TLegend * comb_legend = below_logo_legend();
  comb_legend->AddEntry( data_raw,        "data raw",   "LP" );
  comb_legend->AddEntry( sign_extracted,  "sign extr",  "LP" );
  comb_legend->AddEntry( bckg_extracted,  "bckg extr",  "LP" );
  comb_legend->AddEntry( data_extracted,  "data extr",  "LP" );
  comb_legend->Draw();

  combination_pad = (TPad *) combination_canv->cd( 2 );
  data_extracted->Draw( "HIST E1" );
  data_raw->Draw( "HIST E1 SAME" );
  sign_bckg_stack->Draw( "HIST SAME" );
  hist_prep_axes( data_extracted, true );
  set_axis_labels( data_extracted, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( combination_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), mass.c_str() ) );
  add_atlas_decorations( combination_pad, true, false );
  TList * hist_list = sign_bckg_stack->GetHists();
  TLegend * stack_legend = below_logo_legend();
  stack_legend->AddEntry( data_raw,       "data raw",   "LP" );
  stack_legend->AddEntry( data_extracted, "data extr",  "LP" );
  stack_legend->AddEntry( hist_list->At( 1 ),     "sign stack", "F" );
  stack_legend->AddEntry( hist_list->At( 0 ),     "bckg stack", "F" );
  stack_legend->Draw();

  combination_canv->SaveAs( Form( "./data_comparison_%s_%s_%s.png", mass.c_str(), variables.analysis_variable.c_str(), unique ) );

}

void mc_shape_comparison( TH1F * sign_extracted_normalised, TH1F * sign_mc, TH1F * bckg_extracted_normalised, TH1F * bckg_mc, const char * unique, variable_set & variables ){
  
  std::string & mass = variables.mass;

  TCanvas * comparison_canv = new TCanvas( Form( "comparison_%s_%s", unique, mass.c_str() ), "", 100, 100, 2000, 1000 );
  comparison_canv->Divide( 2, 1 );
  gStyle->SetOptStat( "rmeink" );

  TPad * mc_sign_pad = (TPad *) comparison_canv->cd( 1 );
  sign_extracted_normalised->Draw( "HIST E1" );
  sign_mc->Draw( "HIST E1 SAMES");
  hist_prep_axes( sign_extracted_normalised, true );
  set_axis_labels( sign_extracted_normalised, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( mc_sign_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), mass.c_str() ), false );
  add_atlas_decorations( mc_sign_pad, true, false );
  TLegend * sign_comp_legend = below_logo_legend();
  sign_comp_legend->AddEntry( sign_extracted_normalised,  "ex sign", "LP" );
  sign_comp_legend->AddEntry( sign_mc,          "mc sign", "LP" );
  sign_comp_legend->Draw();
  TPaveStats * sign_extr_stats = make_stats( sign_extracted_normalised, true, true );
  sign_extr_stats->Draw();
  TPaveStats * sign_mc_stats = make_stats( sign_mc, true, false );
  sign_mc_stats->Draw();
  

  TPad * mc_bckg_pad = (TPad *) comparison_canv->cd( 2 );
  bckg_extracted_normalised->Draw( "HIST E1" );
  bckg_mc->Draw( "HIST E1 SAMES");
  hist_prep_axes( bckg_extracted_normalised, true );
  set_axis_labels( bckg_extracted_normalised, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( mc_bckg_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), mass.c_str() ), false );
  add_atlas_decorations( mc_bckg_pad, true, false );
  TLegend * bckg_comp_legend = below_logo_legend();
  bckg_comp_legend->AddEntry( bckg_extracted_normalised,  "ex bckg", "LP" );
  bckg_comp_legend->AddEntry( bckg_mc,          "mc bckg", "LP" );
  bckg_comp_legend->Draw();
  TPaveStats * bckg_extr_stats = make_stats( bckg_extracted_normalised, true, true );
  bckg_extr_stats->Draw();
  TPaveStats * bckg_mc_stats = make_stats( bckg_mc, true, false );
  bckg_mc_stats->Draw();
  comparison_canv->SaveAs( Form( "./mc_comparison_%s_%s_%s.png", mass.c_str(), variables.analysis_variable.c_str(), unique ) );

}

void ratio_plots( TH1F * sign_extracted_normalised, TH1F * sign_mc, TH1F * bckg_extracted_normalised, TH1F * bckg_mc, const char * unique, variable_set & variables ){

  std::string & mass = variables.mass;

  set_axis_labels( sign_mc, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );

  TH1F * sign_mc_upper = (TH1F *) sign_mc->Clone();
  TH1F * sign_extr_upper = (TH1F *) sign_extracted_normalised->Clone();
  TH1F * bckg_mc_upper = (TH1F *) bckg_mc->Clone();
  TH1F * bckg_extr_upper = (TH1F *) bckg_extracted_normalised->Clone();

  TH1F * sign_mc_alter = (TH1F *) sign_mc->Clone();
  TH1F * bckg_mc_alter = (TH1F *) bckg_mc->Clone();
  TH1F * sign_extr_alter = (TH1F *) sign_extracted_normalised->Clone();
  TH1F * bckg_extr_alter = (TH1F *) bckg_extracted_normalised->Clone();


  TCanvas * rat_comparison_canv = new TCanvas( Form( "rat_comp_%s_%s", unique, mass.c_str() ), "", 100, 100, 2000, 2000 );
  gStyle->SetOptStat( "krimes" );
  rat_comparison_canv->Divide( 2, 2 );

  TPad * sign_comp_pad = (TPad *) rat_comparison_canv->cd( 1 );
  TH1F * sign_ratio = ratio_pad( sign_extr_upper, sign_mc_upper, sign_comp_pad );
  TLegend * sign_ratio_legend = below_logo_legend();
  add_pad_title( sign_comp_pad, Form( "Signal - %s %s %s", mass.c_str(), variables.analysis_variable.c_str(), unique ), false );
  add_atlas_decorations( sign_comp_pad, true, false );
  sign_ratio_legend->AddEntry( sign_extr_upper, "extr", "L" );
  sign_ratio_legend->AddEntry( sign_mc_upper, "mc", "L" );
  sign_ratio_legend->AddEntry( sign_ratio, "ratio", "L" );
  sign_ratio_legend->Draw();

  TPad * bckg_comp_pad = (TPad *) rat_comparison_canv->cd( 2 );
  TH1F * bckg_ratio = ratio_pad( bckg_extr_upper, bckg_mc_upper, bckg_comp_pad );
  TLegend * bckg_ratio_legend = below_logo_legend();
  add_pad_title( bckg_comp_pad, Form( "Background - %s %s %s", mass.c_str(), variables.analysis_variable.c_str(), unique ), false );
  add_atlas_decorations( bckg_comp_pad, true, false );
  bckg_ratio_legend->AddEntry( bckg_extr_upper, "extr", "L" );
  bckg_ratio_legend->AddEntry( bckg_mc_upper, "mc", "L" );
  bckg_ratio_legend->AddEntry( bckg_ratio, "ratio", "L" );
  bckg_ratio_legend->Draw();

  TPad * sbmc_comp_pad = (TPad *) rat_comparison_canv->cd( 3 );
  TH1F * sbmc_ratio = ratio_pad( sign_mc_alter, bckg_mc_alter, sbmc_comp_pad );
  TLegend * sbmc_ratio_legend = below_logo_legend();
  add_pad_title( sbmc_comp_pad, Form( "S/B MC - %s %s %s", mass.c_str(), variables.analysis_variable.c_str(), unique ), false );
  add_atlas_decorations( sbmc_comp_pad, true, false );
  sbmc_ratio_legend->AddEntry( sign_mc_alter, "sign", "L" );
  sbmc_ratio_legend->AddEntry( bckg_mc_alter, "bckg", "L" );
  sbmc_ratio_legend->AddEntry( sbmc_ratio, "ratio, s/b", "L" );
  sbmc_ratio_legend->Draw();

  TPad * sbex_comp_pad = (TPad *) rat_comparison_canv->cd( 4 );
  TH1F * sbex_ratio = ratio_pad( sign_extr_alter, bckg_extr_alter, sbex_comp_pad );
  TLegend * sbex_ratio_legend = below_logo_legend();
  add_pad_title( sbex_comp_pad, Form( "S/B Extracted - %s %s %s", mass.c_str(), variables.analysis_variable.c_str(), unique ), false );
  add_atlas_decorations( sbex_comp_pad, true, false );
  sbex_ratio_legend->AddEntry( sign_extr_alter, "sign", "L" );
  sbex_ratio_legend->AddEntry( bckg_extr_alter, "bckg", "L" );
  sbex_ratio_legend->AddEntry( sbex_ratio, "ratio, s/b", "L" );
  sbex_ratio_legend->Draw();

  rat_comparison_canv->SaveAs( Form( "./ratio_comparison_%s_%s_%s.png", mass.c_str(), variables.analysis_variable.c_str(), unique ) );

}

void extraction_method_comparison( TH1F * weight_hist_in, TH1F * differential_hist_in, variable_set & variables, const char * unique, const char * first, const char * second ){

  gStyle->SetOptStat( "krimesn" );

  TH1F * weight_hist = (TH1F *) weight_hist_in->Clone( first );
  TH1F * differential_hist = (TH1F *) differential_hist_in->Clone( second );
  weight_hist->ResetStats();
  differential_hist->ResetStats();

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "event_extract_method_compare" );
  styles->style_histogram( weight_hist, "weight_hist", true );
  styles->style_histogram( differential_hist, "differential_hist", true );

  TCanvas * extract_comp_canv = new TCanvas( Form( "extract_method_comp_canv_%s_%s", unique, variables.mass.c_str() ), "", 200, 200, 1000, 1000 );
  extract_comp_canv->Divide( 1 );
  TPad * method_comp_pad = (TPad *) extract_comp_canv->cd( 1 );
  TH1F * method_ratio = ratio_pad( differential_hist, weight_hist, method_comp_pad );
  styles->style_histogram( method_ratio, "ratio_hist", true );
  TLegend * method_comp_legend = below_logo_legend();
  add_pad_title( method_comp_pad, Form( "%s - %s", variables.analysis_variable.c_str(), unique ), false );
  add_atlas_decorations( method_comp_pad, true, false );
  method_comp_legend->AddEntry( weight_hist, first, "LP" );
  method_comp_legend->AddEntry( differential_hist, second, "LP" );
  method_comp_legend->AddEntry( method_ratio, "ratio, 1/2", "LP" );
  method_comp_legend->Draw();
  extract_comp_canv->SaveAs( Form( "./method_ratio_%s_%s_%s.png", variables.mass.c_str(), 
                                    variables.analysis_variable.c_str(), unique ) );
  delete extract_comp_canv;

}

void extraction_details( basic_fileset & input, variable_set & variables ){

  const char * unique = input.get_unique();
  TTree * stats_tree = (TTree *) input.statistics_file->Get( variables.mass.c_str() );

  TH1F * mu_hist = variables.analysis_bound.get_hist( "mu_hist" );
  TH1F * spp_hist = variables.analysis_bound.get_hist( "spp_hist" );

  std::vector < TH1F * > sign_weight_hists;
  std::vector < TH1F * > bckg_weight_hists;

  for ( int spectator_idx = 0; spectator_idx <= variables.spectator_bound.get_bins(); spectator_idx++ ){
    sign_weight_hists.push_back( variables.analysis_bound.get_hist( Form( "sign_weight_%i", spectator_idx ) ) );
    bckg_weight_hists.push_back( variables.analysis_bound.get_hist( Form( "bckg_weight_%i", spectator_idx ) ) );
  }
 
  fit_results * results_entry = new fit_results();
  stats_tree->SetBranchAddress( "fit_stats", &results_entry );
  for ( int analysis_bin = 0; analysis_bin < variables.analysis_bound.get_bins(); analysis_bin++ ){
    stats_tree->GetEntry( analysis_bin );
    mu_hist->SetBinContent( analysis_bin+1, (*results_entry).mu_value );
    mu_hist->SetBinContent( analysis_bin+1, (*results_entry).mu_error );
    spp_hist->SetBinContent( analysis_bin+1, (*results_entry).spp_value );
    spp_hist->SetBinContent( analysis_bin+1, (*results_entry).spp_error );
    std::vector< double > sign_weights = (*results_entry).sign_weight;
    std::vector< double > bckg_weights = (*results_entry).bckg_weight;
    for ( int spectator_idx = 0; spectator_idx < variables.spectator_bound.get_bins(); spectator_idx++ ){
      if ( sign_weights.at( spectator_idx ) != 0 ){ sign_weight_hists.at( spectator_idx )->SetBinContent( analysis_bin, sign_weights.at( spectator_idx ) ); }
      if ( bckg_weights.at( spectator_idx ) != 0 ){ bckg_weight_hists.at( spectator_idx )->SetBinContent( analysis_bin, bckg_weights.at( spectator_idx ) ); }
    }
  }


  TCanvas * parameter_canvas = new TCanvas( "parameter_canvas", "", 200, 200, 2000, 2000 );

  parameter_canvas->Divide( 2, 2 );

  TPad * parameter_pad = (TPad *) parameter_canvas->cd( 1 );
  mu_hist->Draw( "HIST E1" );
  hist_prep_axes( mu_hist, true );
  set_axis_labels( mu_hist, variables.analysis_bound.get_x_str(), Form( "#mu value %s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( parameter_pad, Form( "#mu %s %s %s", unique, variables.analysis_variable.c_str(), variables.mass.c_str() ) );
  add_atlas_decorations( parameter_pad, true, false );
  TPaveStats * mu_stats = make_stats( mu_hist, false, false );
  mu_stats->Draw();
  TLegend * mu_legend = below_logo_legend();
  mu_legend->AddEntry( mu_hist, "#mu values",   "LP" );
  mu_legend->Draw();

  parameter_pad = (TPad *) parameter_canvas->cd( 2 );
  spp_hist->Draw( "HIST E1" );
  hist_prep_axes( spp_hist, true );
  set_axis_labels( spp_hist, variables.analysis_bound.get_x_str(), Form( "s_{pp} value%s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( parameter_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), variables.mass.c_str() ) );
  add_atlas_decorations( parameter_pad, true, false );
  TPaveStats * spp_stats = make_stats( spp_hist, false, false );
  spp_stats->Draw();
  TLegend * spp_legend = below_logo_legend();
  spp_legend->AddEntry( spp_hist, "s_{pp} values", "LP" );
  spp_legend->Draw();

  parameter_pad = (TPad *) parameter_canvas->cd( 3 );
  sign_weight_hists.at( 0 )->SetStats( kFALSE );
  sign_weight_hists.at( 0 )->Draw( "P E1" );
  sign_weight_hists.at( 0 )->GetYaxis()->SetRangeUser( -2, 2 );
  hist_prep_axes( sign_weight_hists.at( 0 ), true );
  set_axis_labels( sign_weight_hists.at( 0 ), variables.analysis_bound.get_x_str(), Form( "Signal Weight %s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( parameter_pad, Form( "Signal Weights %s %s %s", unique, variables.analysis_variable.c_str(), variables.mass.c_str() ) );
  add_atlas_decorations( parameter_pad, true, false );
  for ( int hist_idx = 1; hist_idx < (int) sign_weight_hists.size(); hist_idx++ ){
    sign_weight_hists.at( hist_idx )->Draw( "P E1 SAME" );
  }

  parameter_pad = (TPad *) parameter_canvas->cd( 4 );
  bckg_weight_hists.at( 0 )->SetStats( kFALSE );
  bckg_weight_hists.at( 0 )->Draw( "P E1" );
  bckg_weight_hists.at( 0 )->GetYaxis()->SetRangeUser( 3 , -3 );
  hist_prep_axes( bckg_weight_hists.at( 0 ), true );
  set_axis_labels( bckg_weight_hists.at( 0 ), variables.analysis_bound.get_x_str(), Form( "Background Weight %s", variables.analysis_bound.get_y_str().c_str() ) );
  add_pad_title( parameter_pad, Form( "Background Weights %s %s %s", unique, variables.analysis_variable.c_str(), variables.mass.c_str() ) );
  add_atlas_decorations( parameter_pad, true, false );
  for ( int hist_idx = 1; hist_idx < (int) bckg_weight_hists.size(); hist_idx++ ){
    bckg_weight_hists.at( hist_idx )->Draw( "P E1 SAME" );
  }
  
  parameter_canvas->SaveAs( Form( "./parameter_%s_%s_%s.png", variables.mass.c_str(), 
                                    variables.analysis_variable.c_str(), unique ) );
}

void jpsi_gamma( basic_fileset & input, variable_set & variables ){

  gStyle->SetOptStat( "krimesn" );

  std::string ana_var = variables.analysis_variable;
  const char * core_unique = input.get_unique();
  const char * mass = variables.mass.c_str();
  char dimuon_unique[50], photon_unique[50];
  sprintf( dimuon_unique, "%s_DiMuonPt", core_unique );
  sprintf( photon_unique, "%s_PhotonPt", core_unique );


  variables.analysis_variable = "DiMuonPt";
  variables.analysis_bound = variables.bound_manager->get_bound( "DiMuonPt" );
  hist_group jpsi = input.recreate_histograms_weights( variables, true, false );
  variables.analysis_variable = "PhotonPt";
  variables.analysis_bound = variables.bound_manager->get_bound( "PhotonPt" );
  hist_group phot = input.recreate_histograms_weights( variables, true, false );

  TH1F *& jpsi_sign = jpsi.sign_hist;
  TH1F *& jpsi_bckg = jpsi.bckg_hist;
  TH1F *& phot_sign = phot.sign_hist;
  TH1F *& phot_bckg = phot.bckg_hist;
  

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "default" );
  styles->style_histogram( jpsi_sign, "extracted_sign", true );
  styles->style_histogram( jpsi_bckg, "extracted_bckg", true );
  styles->style_histogram( phot_sign, "extracted_sign", true );
  styles->style_histogram( phot_bckg, "extracted_bckg", true );


  TCanvas * jpsigamma_canv = new TCanvas( "jpsigamma_canv", "", 200, 200, 2000, 2000 ); 

  jpsigamma_canv->Divide( 2, 2 );

  TPad * jgpad = (TPad *) jpsigamma_canv->cd( 1 );
  jpsi_sign->Draw( "HIST E1" );
  hist_prep_axes( jpsi_sign, true );
  set_axis_labels( jpsi_sign, variables.bound_manager->get_bound( "DiMuonPt").get_x_str(), Form( "Yield%s", variables.bound_manager->get_bound( "DiMuonPt").get_x_str().c_str() ) );
  add_pad_title( jgpad, Form( "sign %s %s", dimuon_unique, variables.mass.c_str() ) );
  add_atlas_decorations( jgpad, true, false );
  TPaveStats * jsign_stats = make_stats( jpsi_sign, false, false );
  jsign_stats->Draw();


  jgpad = (TPad *) jpsigamma_canv->cd( 2 );
  jpsi_bckg->Draw( "HIST E1" );
  hist_prep_axes( jpsi_bckg, true );
  set_axis_labels( jpsi_bckg, variables.bound_manager->get_bound( "DiMuonPt").get_x_str(), Form( "Yield%s", variables.bound_manager->get_bound( "DiMuonPt").get_x_str().c_str() ) );
  add_pad_title( jgpad, Form( "bckg %s %s", dimuon_unique, variables.mass.c_str() ) );
  add_atlas_decorations( jgpad, true, false );
  TPaveStats * jbckg_stats = make_stats( jpsi_bckg, false, false );
  jbckg_stats->Draw();

  jgpad = (TPad *) jpsigamma_canv->cd( 3 );
  phot_sign->Draw( "HIST E1" );
  hist_prep_axes( phot_sign, true );
  set_axis_labels( phot_sign, variables.bound_manager->get_bound( "PhotonPt").get_x_str(), Form( "Yield%s", variables.bound_manager->get_bound( "PhotonPt").get_x_str().c_str() ) );
  add_pad_title( jgpad, Form( "sign %s %s", photon_unique, variables.mass.c_str() ) );
  add_atlas_decorations( jgpad, true, false );
  TPaveStats * jsig_stats = make_stats( phot_sign, false, false );
  jsig_stats->Draw();

  jgpad = (TPad *) jpsigamma_canv->cd( 4 );
  phot_bckg->Draw( "HIST E1" );
  hist_prep_axes( phot_bckg, true );
  set_axis_labels( phot_bckg, variables.bound_manager->get_bound( "PhotonPt").get_x_str(), Form( "Yield%s", variables.bound_manager->get_bound( "PhotonPt").get_x_str().c_str() ) );
  add_pad_title( jgpad, Form( "bckg %s %s", photon_unique, variables.mass.c_str() ) );
  add_atlas_decorations( jgpad, true, false );
  TPaveStats * pbckg_stats = make_stats( phot_bckg, false, false );
  pbckg_stats->Draw();

  variables.analysis_variable = ana_var;
  variables.analysis_bound = variables.bound_manager->get_bound( ana_var );

  jpsigamma_canv->SaveAs( Form( "jpsigamma_pt_weight_%s.png", core_unique ) ); 
 
  jgpad = (TPad *) jpsigamma_canv->cd( 1 );
  jpsi_sign->GetYaxis()->SetRangeUser( 1, jpsi_sign->GetMaximum()*100 );
  jgpad->SetLogy();
  jgpad = (TPad *) jpsigamma_canv->cd( 2 );
  jpsi_bckg->GetYaxis()->SetRangeUser( 1, jpsi_bckg->GetMaximum()*100 );
  jgpad->SetLogy();
  jgpad = (TPad *) jpsigamma_canv->cd( 3 );
  phot_sign->GetYaxis()->SetRangeUser( 1, phot_sign->GetMaximum()*100 );
  jgpad->SetLogy();
  jgpad = (TPad *) jpsigamma_canv->cd( 4 );
  phot_bckg->GetYaxis()->SetRangeUser( 1, phot_bckg->GetMaximum()*100 );
  jgpad->SetLogy();

  jpsigamma_canv->SaveAs( Form( "jpsigamma_pt_weight_%s_log.png", core_unique ) ); 

  TH1F * jpsi_combined = (TH1F *) jpsi_sign->Clone();
  jpsi_combined->Reset();
  TH1F * phot_combined = (TH1F *) phot_sign->Clone();
  phot_combined->Reset();


  jpsi_combined->Add( jpsi_sign, jpsi_bckg );
  phot_combined->Add( phot_sign, phot_bckg );

  TH1F *& jpsi_data = jpsi.data_hist;
  TH1F *& phot_data = phot.data_hist;
   
  styles->style_histogram( jpsi_combined, "extracted_data" );
  styles->style_histogram( phot_combined, "extracted_data" );
  styles->style_histogram( jpsi_data, "raw_data" );
  styles->style_histogram( phot_data, "raw_data" );

  jpsi_data->SetStats( kTRUE ); 
  phot_data->SetStats( kTRUE ); 

  jpsi_data->SetName( "jpsi data" );
  phot_data->SetName( "photon data" );
  jpsi_combined->SetName( "jpsi combo" );
  phot_combined->SetName( "photon combo" );


  TCanvas * jpsi_phot_combo_canv = new TCanvas( "jpsi_phot_combo_canv", "", 200, 200, 2000, 1000 );
  jpsi_phot_combo_canv->Divide( 2, 1 );

  TPad * jgc_pad = (TPad *) jpsi_phot_combo_canv->cd( 1 );
  jpsi_combined->Draw( "HIST E1" );
  jpsi_data->Draw( "HIST SAMES" );
  hist_prep_axes( jpsi_combined, true );
  set_axis_labels( jpsi_combined, variables.bound_manager->get_bound( "DiMuonPt").get_x_str(), Form( "Yield%s", variables.bound_manager->get_bound( "DiMuonPt").get_x_str().c_str() ) );
  add_pad_title( jgc_pad, Form( "J/#psi P_{T} %s %s", core_unique, mass ) );
  add_atlas_decorations( jgc_pad, true, false );
  TPaveStats * jpsi_comb_stats = make_stats( jpsi_combined, true, true );
  jpsi_comb_stats->Draw( "SAME" );
  TPaveStats * jpsi_data_stats = make_stats( jpsi_data, true );
  jpsi_data_stats->Draw( "SAME" );
  TLegend * jpsi_sub_combo_legend = below_logo_legend();
  jpsi_sub_combo_legend->AddEntry( jpsi_combined, "Recombined J/#psi P_{T}" );
  jpsi_sub_combo_legend->AddEntry( jpsi_data, "Subtracted Data J/#psi P_{T}" );
  jpsi_sub_combo_legend->Draw( "SAME" );

  jgc_pad = (TPad *) jpsi_phot_combo_canv->cd( 2 );
  phot_combined->Draw( "HIST E1" );
  phot_data->Draw( "HIST E1 SAMES" );
  hist_prep_axes( phot_combined, true );
  set_axis_labels( phot_combined, variables.bound_manager->get_bound( "PhotonPt").get_x_str(), Form( "Yield%s", variables.bound_manager->get_bound( "PhotonPt").get_x_str().c_str() ) );
  add_pad_title( jgc_pad, Form( "#gamma P_{T} %s %s", core_unique, mass ) );
  add_atlas_decorations( jgc_pad, true, false );
  TPaveStats * phot_comb_stats = make_stats( phot_combined, true, true );
  phot_comb_stats->Draw( "SAME" );
  TPaveStats * phot_data_stats = make_stats( phot_data, true );
  phot_data_stats->Draw( "SAME" );
  TLegend * phot_sub_combo_legend = below_logo_legend();
  phot_sub_combo_legend->AddEntry( phot_combined, "Recombined #gamma P_{T}" );
  phot_sub_combo_legend->AddEntry( phot_data, "Subtracted Data #gamma P_{T}" );
  phot_sub_combo_legend->Draw( "SAME" );
  
  jpsi_phot_combo_canv->SaveAs( Form( "jpsi_gamma_sub_data_%s.png", core_unique ) );

  TFile * output_file = new TFile( "jpsi_gamma_pt.root", "RECREATE" );
  output_file->cd();
  jpsi_sign->Write( "jpsi_sign" ); 
  jpsi_bckg->Write( "jpsi_bckg" );
  phot_sign->Write( "phot_sign" );
  phot_bckg->Write( "phot_bckg" );
  jpsi_combined->Write( "jpsi_combined" );
  phot_combined->Write( "phot_combined" );
  jpsi_data->Write( "jpsi_data" );
  phot_data->Write( "phot_data" );
  output_file->Save();
  output_file->Close();
  delete output_file;



}

void delta_map( basic_fileset & input, variable_set & variables, style_mgr * styles ){

  const char * unique = input.get_unique();

  hist_group_2d delta = input.recreate_map_histograms_subtraction( variables, false, "AbsdY", "AbsdPhiZ" );

  bound & bound_x = variables.bound_manager->get_bound( "AbsdY" ); 
  bound & bound_y = variables.bound_manager->get_bound( "AbsdPhiZ" ); 

  TH2F * sign = delta.sign_hist;
  TH2F * bckg = delta.bckg_hist;
  
  TH1D * sign_dy = sign->ProjectionX();
  TH1D * sign_dp = sign->ProjectionY();
  TH1D * bckg_dy = bckg->ProjectionX();
  TH1D * bckg_dp = bckg->ProjectionY();
  
  sign_dy->GetYaxis()->SetMaxDigits( 3 );
  bckg_dy->GetYaxis()->SetMaxDigits( 3 );
  
  styles->style_histogram( sign_dy, "extracted_sign" );
  styles->style_histogram( sign_dp, "extracted_sign" );
  styles->style_histogram( bckg_dy, "extracted_bckg" );
  styles->style_histogram( bckg_dp, "extracted_bckg" );


  //TF2 * ellipse = new TF2( "ell", "( ((x*x)/([0]*[0]) + (y*y)/([1]*[1])) > 0.95 )&&( ( (x*x)/([0]*[0]) + (y*y)/([1]*[1]) ) < 1.05 )", 0, 5, 0, M_PI );
  //TF2 * ellipse = new TF2( "ell", "( ((x*x)/([0]*[0]) + (y*y)/([1]*[1])) > 0.95 )&&( ( (x*x)/([0]*[0]) + (y*y)/([1]*[1]) ) < 1.05 )", 0, 5, 0, M_PI );
  //TF2 * ellipse = new TF2( "ell", " abs( ((x*x)/([0]*[0]) + (y*y)/([1]*[1])) -1 ) < 0.1 * 1000", 0, 5, 0, M_PI );
  //ellipse->SetLineColor( kRed+1 );
  //ellipse->SetFillColor( kRed+1 );
  
  //TF2 * sr = new TF2( "sr", "( ((x*x)/([0]*[0]) + (y*y)/([1]*[1])) < 1 )", 0, 5, 0, M_PI );
  //sr->SetContour( 2 );
  //sr->SetFillColorAlpha( kRed+1, 0.5 );
  //sr->SetFillStyle( 3005 );
  //sr->SetParameter( 0, 2.5 );
  //sr->SetParameter( 1, 1.5 );
  //TF2 * cr = new TF2( "cr", "( ((x*x)/([0]*[0]) + (y*y)/([1]*[1])) > 1 )", 0, 5, 0, M_PI );
  //cr->SetContour( 2 );
  //cr->SetFillColorAlpha( kBlue+1, 0.5 );
  //cr->SetParameter( 0, 2.5 );
  //cr->SetParameter( 1, 1.5 );


  
  TCanvas * delta_canvas = new TCanvas( "delta_canv", "", 200, 2000, 2000, 2000 );
  delta_canvas->Divide( 2, 2 );

  TPad * delta_pad = (TPad *) delta_canvas->cd( 1 );
  sign->Draw( "COLZ" );
  //sr->Draw( "SAME CONT" );
  //cr->Draw( "SAME CONT" );
  set_axis_labels( sign, bound_x.get_x_str().c_str(), bound_y.get_x_str().c_str() );
  hist_prep_text( sign );
  add_pad_title( delta_pad, Form( "Signal Deltas" ), false ); 
  //TPaveStats * sign_stats = make_stats( sign );
  //sign_stats->Draw( "SAME" );

  delta_pad = (TPad *) delta_canvas->cd( 2 );
  bckg->Draw( "COLZ" );
  set_axis_labels( bckg, bound_x.get_x_str().c_str(), bound_y.get_x_str().c_str() );
  hist_prep_text( sign );
  add_pad_title( delta_pad, Form( "Background deltas" ), false ); 
  TPaveStats * bckg_stats = make_stats( bckg );
  bckg_stats->Draw( "SAME" );

  

  delta_pad = (TPad *) delta_canvas->cd( 3 );
  TH1F * ratio_dy = ratio_pad( sign_dy, bckg_dy, delta_pad );
  set_axis_labels( sign_dy, bound_x.get_x_str().c_str(), "Ratio" );
  delta_pad = (TPad *) delta_canvas->cd( 3 );
  TLegend * dy_ratio_legend = below_logo_legend();
  add_pad_title( delta_pad, Form( "DY - %s", unique ), false );
  add_atlas_decorations( delta_pad, true, false );
  dy_ratio_legend->AddEntry( sign_dy, "sign", "L" );
  dy_ratio_legend->AddEntry( bckg_dy, "bckg", "L" );
  dy_ratio_legend->AddEntry( ratio_dy, "ratio", "L" );
  dy_ratio_legend->Draw();

  delta_pad = (TPad *) delta_canvas->cd( 4 );
  TH1F * ratio_dp = ratio_pad( sign_dp, bckg_dp, delta_pad );
  set_axis_labels( sign_dy, bound_y.get_x_str().c_str(), "Ratio" );
  delta_pad = (TPad *) delta_canvas->cd( 4 );
  TLegend * dp_ratio_legend = below_logo_legend();
  add_pad_title( delta_pad, Form( "DPhi - %s", unique ), false );
  add_atlas_decorations( delta_pad, true, false );
  dp_ratio_legend->AddEntry( sign_dp, "sign", "L" );
  dp_ratio_legend->AddEntry( bckg_dp, "bckg", "L" );
  dp_ratio_legend->AddEntry( ratio_dp, "ratio", "L" );
  dp_ratio_legend->Draw();

  delta_canvas->SaveAs( Form( "./delta_yp_%s.png", unique ) );

}
