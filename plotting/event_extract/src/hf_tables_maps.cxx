#include <hf_tables_maps.hxx>


void generate_weight_maps( bound & analysis_bound, bound & spectator_bound, TDirectoryFile * stats_file, std::string & unique, TFile * output_file ){

  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  TH2F * sign_weight_map = spectator_bound.get_2d_hist( "sign_weight_map", analysis_bound ); 
  TH2F * bckg_weight_map = spectator_bound.get_2d_hist( "bckg_weight_map", analysis_bound ); 
  
  std::vector<std::string> ana_cut_names = analysis_bound.get_series_names();
   
  gStyle->SetOptStat( "" );

  for ( int ana_bin = 1; ana_bin <= analysis_bound.get_bins(); ana_bin++ ){

    TH1F * sign_weight_hist = (TH1F *) stats_file->Get( Form( "sign_weight_Q12_%s", ana_cut_names[ ana_bin-1 ].c_str() ) );
    TH1F * bckg_weight_hist = (TH1F *) stats_file->Get( Form( "bckg_weight_Q12_%s", ana_cut_names[ ana_bin-1 ].c_str() ) );

    for ( int spec_bin = 1; spec_bin <= spectator_bound.get_bins(); spec_bin++ ){

      sign_weight_map->SetBinContent( spec_bin, ana_bin, sign_weight_hist->GetBinContent( spec_bin ) );
      bckg_weight_map->SetBinContent( spec_bin, ana_bin, bckg_weight_hist->GetBinContent( spec_bin ) );

    }
  }

  TCanvas * map_canv = new TCanvas( "map_canv", "", 200, 200, 2000, 1000 );

  map_canv->Divide( 2, 1 );
  TPad * current_pad;

  current_pad = (TPad *) map_canv->cd( 1 );
  sign_weight_map->Draw( "COLZ" );
  set_axis_labels( sign_weight_map, spectator_bound.get_x_str().c_str(), analysis_bound.get_x_str().c_str() );
  hist_prep_text( sign_weight_map );
  add_pad_title( current_pad, Form( "Signal weights" ), false ); 

  current_pad = (TPad *) map_canv->cd( 2 );
  bckg_weight_map->Draw( "COLZ" );
  set_axis_labels( bckg_weight_map, spectator_bound.get_x_str().c_str(), analysis_bound.get_x_str().c_str() );
  hist_prep_text( bckg_weight_map );
  add_pad_title( current_pad, Form( "Background weights" ), false ); 

  map_canv->SaveAs( Form( "./weight_map_%s_%s_Q12_%s.png",  spectator_bound.get_var().c_str(), analysis_bound.get_var().c_str(), unique.c_str() ) );

  gStyle->SetOptStat( "miner" );

  output_file->cd();
  sign_weight_map->Write();
  bckg_weight_map->Write();

}

void produce_table( TTree * stats_tree ){

	int ana_bin = 0;
  fit_results * fit_stats = new fit_results();
  stats_tree->SetBranchAddress( "fit_stats", &fit_stats);
  stats_tree->SetBranchAddress( "ana_bin",   &ana_bin );

	int entries = stats_tree->GetEntries();
  std::vector< double > sign, bckg, data, sign_fit, bckg_fit, data_fit, data_fit_diff, mu, spp, rho;
  std::vector< double > sign_fit_err, bckg_fit_err, mu_err, spp_err;
  for ( int entry = 0; entry < entries; entry++ ){
    stats_tree->GetEntry( entry );
    sign.push_back( fit_stats->sign );
    bckg.push_back( fit_stats->bckg );
    data.push_back( fit_stats->data );
    sign_fit.push_back( fit_stats->sign_fit );
    sign_fit_err.push_back( fit_stats->sign_fit_error );
    bckg_fit.push_back( fit_stats->bckg_fit );
    bckg_fit_err.push_back( fit_stats->bckg_fit_error );
		data_fit.push_back( fit_stats->data_fit );
    data_fit_diff.push_back( fit_stats->data - fit_stats->data_fit );
    mu.push_back( fit_stats->mu_value);
    mu_err.push_back( fit_stats->mu_error );
    spp.push_back( fit_stats->spp_value );
    spp_err.push_back( fit_stats->spp_error );
    rho.push_back( fit_stats->rho );
  }
  std::ofstream stats_table;
  stats_table.open ("./Q12_table.tex");
	stats_table << Form("\\begin{tabular}{ |l||r|r|r|r|r|r|r|r|r|r| } \n\\hline\n" );
	stats_table << "bin & sign & bckg & data\\_fit & sign\\_fit & bckg\\_fit & data\\_fit & diff & mu & spp & rho \\\\\n\\hline\n\\hline\n";
	for ( int idx = 0; idx < entries; idx++ ){
		stats_table << Form( "%i & %.0f & %.0f & %.0f & %.1f $\\pm$ %.2f & %.1f $\\pm$ %.2f & %.1f & %.1f & %.2f $\\pm$ %.2f & %.1f $\\pm$ %.1f & %.1f \\\\\n\\hline\n", 
													idx+1, sign.at( idx ), bckg.at(idx), data.at(idx), sign_fit.at(idx), sign_fit_err.at(idx), bckg_fit.at(idx), bckg_fit_err.at(idx), 
                          data_fit.at(idx), data_fit_diff.at(idx), mu.at(idx), mu_err.at(idx), spp.at(idx), spp_err.at(idx),  rho.at(idx) );
	}
	stats_table << "\\end{tabular}";
  stats_table.close();

	stats_tree->ResetBranchAddresses();
	std::cout << "stats table has been created" << std::endl;
  
}
