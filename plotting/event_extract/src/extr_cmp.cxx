#include <extr_cmp.hxx>

void extr_compare( std::string & input, std::string & abin_var, std::string & spec_var,
                   bound_mgr * selections, std::string unique ){


  prep_style();
  gStyle->SetOptStat( "mierk" );

  //std::vector< std::string > mass_bins = { "Q3", "Q4", "Q5", "Q12" };
  std::vector< std::string > mass_bins = { "Q12" };

  std::vector< std::string > filenames, uniques;
  split_strings( filenames, input, ":" );
  split_strings( uniques, unique, ":" );

  std::string & unique_1 = uniques.at( 0 );
  std::string & unique_2 = uniques.at( 1 );

  std::string & data_path_1 = filenames.at( 0 );
  std::string & data_path_2 = filenames.at( 1 );
  std::string & sign_path_1 = filenames.at( 2 );
  std::string & sign_path_2 = filenames.at( 3 );
  std::string & bckg_path_1 = filenames.at( 4 );
  std::string & bckg_path_2 = filenames.at( 5 );
  


  TFile * input_file_1 = new TFile( data_path_1.c_str(), "READ" );
  TTree * input_tree_1 = (TTree *) input_file_1->Get( "tree" );
  TFile * sign_mc_file_1 = new TFile( sign_path_1.c_str(), "READ" );
  TTree * sign_tree_1 = (TTree *) sign_mc_file_1->Get( "tree" );
  TFile * bckg_mc_file_1 = new TFile( bckg_path_1.c_str(), "READ" );
  TTree * bckg_tree_1 = (TTree *) bckg_mc_file_1->Get( "tree" );

  TFile * input_file_2 = new TFile( data_path_2.c_str(), "READ" );
  TTree * input_tree_2 = (TTree *) input_file_2->Get( "tree" );
  TFile * sign_mc_file_2 = new TFile( sign_path_2.c_str(), "READ" );
  TTree * sign_tree_2 = (TTree *) sign_mc_file_2->Get( "tree" );
  TFile * bckg_mc_file_2 = new TFile( bckg_path_2.c_str(), "READ" );
  TTree * bckg_tree_2 = (TTree *) bckg_mc_file_2->Get( "tree" );

  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int ana_bins      = analysis_bound.get_bins();
  float ana_min     = analysis_bound.get_min(); 
  float ana_max     = analysis_bound.get_max(); 
  std::vector< std::string > ana_cuts = analysis_bound.get_cut_series();
  std::vector< std::string > ana_cut_names = analysis_bound.get_series_names( ana_bins );

  std::vector< float > eff_style =  {  21, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,      0.0, 0.0 };
  std::vector< float > sign_style = { 0.0, 0.0, 0.0, 0.0, 1.0, kRed+1, 1.0, 1.0, 0.0, 0.0, 0.0 };
  std::vector< float > bckg_style = { 0.0, 0.0, 0.0, 0.0, 1.0, kBlue+1, 1.0, 1.0, 0.0, 0.0, 0.0 };
  std::vector< float > data_style = { 0.0, 0.0, 0.0, 0.0, 1.0, kGreen+2, 1.0, 1.0, 0.0, 0.0, 0.0 };

  std::vector< float > sign_fill_style = { 1.0, 1.0, 1.0, 1.0, 1.0, kRed+1, 1.0,  0.0,  kRed+1,  0.5, 1001 };
  std::vector< float > bckg_fill_style = { 1.0, 1.0, 1.0, 1.0, 1.0, kBlue+1, 1.0, 0.0, kBlue+1, 0.5, 1001 };


  //TFile * output_file = new TFile( "extract.root", "RECREATE" );


  for ( std::string & mass : mass_bins ){

    bound mass_bound = selections->get_bound( mass );
    std::string mass_cut = mass_bound.get_cut();

    TH1F * sign_extracted_1 = new TH1F( Form( "sign_extr_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_extracted_1 = new TH1F( Form( "bckg_extr_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_extracted_1 = new TH1F( Form( "data_extr_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * sign_mc_1 = new TH1F( Form( "sign_mc_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_mc_1 = new TH1F( Form( "bckg_mc_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_raw_1 = new TH1F( Form( "data_raw_1_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );

    TH1F * sign_extracted_2 = new TH1F( Form( "sign_extr_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_extracted_2 = new TH1F( Form( "bckg_extr_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_extracted_2 = new TH1F( Form( "data_extr_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * sign_mc_2 = new TH1F( Form( "sign_mc_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * bckg_mc_2 = new TH1F( Form( "bckg_mc_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );
    TH1F * data_raw_2 = new TH1F( Form( "data_raw_2_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max );



    style_hist( sign_extracted_1,   sign_style );
    style_hist( bckg_extracted_1,   bckg_style );
    style_hist( sign_mc_1,          sign_style );
    style_hist( bckg_mc_1,          bckg_style );
    style_hist( data_raw_1,         data_style );
    style_hist( data_extracted_1,   data_style );

    style_hist( sign_extracted_2,   sign_style );
    style_hist( bckg_extracted_2,   bckg_style );
    style_hist( sign_mc_2,          sign_style );
    style_hist( bckg_mc_2,          bckg_style );
    style_hist( data_raw_2,         data_style );
    style_hist( data_extracted_2,   data_style );


    data_raw_1->SetLineColorAlpha( 1.0, 1.0 );
    data_raw_2->SetLineColorAlpha( 1.0, 1.0 );

    sign_extracted_2->SetLineStyle( 2 );
    bckg_extracted_2->SetLineStyle( 2 );
    sign_mc_2->SetLineStyle( 2 );
    bckg_mc_2      ->SetLineStyle( 2 );
    data_raw_2     ->SetLineStyle( 2 );
    data_extracted_2->SetLineStyle( 2 );
    
    //THStack * comb_stack = new THStack( Form( "comb_stack_%s", mass.c_str() ), "" );

    input_tree_1->Draw( Form( "%s>>sign_extr_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str() ) );

    input_tree_1->Draw( Form( "%s>>bckg_extr_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str() ) );

    input_tree_1->Draw( Form( "%s>>data_raw_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );

    sign_tree_1->Draw( Form( "%s>>sign_mc_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );
    bckg_tree_1->Draw( Form( "%s>>bckg_mc_1_%s", analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );



    input_tree_2->Draw( Form( "%s>>sign_extr_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_sign_weight)*(%s)", mass_cut.c_str() ) );

    input_tree_2->Draw( Form( "%s>>bckg_extr_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight*hf_bckg_weight)*(%s)", mass_cut.c_str() ) );

    input_tree_2->Draw( Form( "%s>>data_raw_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), 
                      Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );

    sign_tree_2->Draw( Form( "%s>>sign_mc_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );
    bckg_tree_2->Draw( Form( "%s>>bckg_mc_2_%s", analysis_bound.get_var().c_str(), mass.c_str() ), Form( "(subtraction_weight)*(%s)", mass_cut.c_str() ) );

    data_extracted_1->Add( sign_extracted_1, bckg_extracted_1, 1.0, 1.0 );
    data_extracted_2->Add( sign_extracted_2, bckg_extracted_2, 1.0, 1.0 );


    gStyle->SetOptStat( "" );
    TCanvas * comparison_canv = new TCanvas( Form( "comparison_%s", mass.c_str() ), "", 200, 200, 2000, 2000 );
    comparison_canv->Divide( 2, 2 );

    TPad * sign_ex_pad = (TPad *) comparison_canv->cd( 1 );
    TH1F * sign_ex_ratio = ratio_pad( sign_extracted_1, sign_extracted_2, sign_ex_pad );
    sign_extracted_1->GetYaxis()->SetRangeUser( 0, std::max( { sign_extracted_1->GetMaximum(), sign_extracted_2->GetMaximum() } )*1.3 );
    add_pad_title( sign_ex_pad, Form( "Signal Extracted - %s %s %s/%s", mass.c_str(), abin_var.c_str(), unique_1.c_str(), unique_2.c_str() ), false );
    add_atlas_decorations( sign_ex_pad, true, false );
    TLegend * sign_ex_ratio_legend = below_logo_legend();
    sign_ex_ratio_legend->AddEntry( sign_extracted_1, unique_1.c_str(), "L" );
    sign_ex_ratio_legend->AddEntry( sign_extracted_2, unique_2.c_str(), "L" );
    sign_ex_ratio_legend->AddEntry( sign_ex_ratio, "ratio", "L" );
    sign_ex_ratio_legend->Draw();


    TPad * bckg_ex_pad = (TPad *) comparison_canv->cd( 2 );
    TH1F * bckg_ex_ratio = ratio_pad( bckg_extracted_1, bckg_extracted_2, bckg_ex_pad );
    bckg_extracted_1->GetYaxis()->SetRangeUser( 0, std::max( { bckg_extracted_1->GetMaximum(), bckg_extracted_2->GetMaximum() } )*1.3 );
    add_pad_title( bckg_ex_pad, Form( "Background Extracted - %s %s %s/%s", mass.c_str(), abin_var.c_str(), unique_1.c_str(), unique_2.c_str() ), false );
    add_atlas_decorations( bckg_ex_pad, true, false );
    TLegend * bckg_ex_ratio_legend = below_logo_legend();
    bckg_ex_ratio_legend->AddEntry( bckg_extracted_1, unique_1.c_str(), "L" );
    bckg_ex_ratio_legend->AddEntry( bckg_extracted_2, unique_2.c_str(), "L" );
    bckg_ex_ratio_legend->AddEntry( bckg_ex_ratio, "ratio", "L" );
    bckg_ex_ratio_legend->Draw();


    TPad * sign_mc_pad = (TPad *) comparison_canv->cd( 3 );
    TH1F * sign_mc_ratio = ratio_pad( sign_mc_1, sign_mc_2, sign_mc_pad );
    sign_mc_1->GetYaxis()->SetRangeUser( 0, std::max( { sign_mc_1->GetMaximum(), sign_mc_2->GetMaximum() } )*1.3 );
    add_pad_title( sign_mc_pad, Form( "Signal MC - %s %s %s/%s", mass.c_str(), abin_var.c_str(), unique_1.c_str(), unique_2.c_str() ), false );
    add_atlas_decorations( sign_mc_pad, true, false );
    TLegend * sign_mc_ratio_legend = below_logo_legend();
    sign_mc_ratio_legend->AddEntry( sign_mc_1, unique_1.c_str(), "L" );
    sign_mc_ratio_legend->AddEntry( sign_mc_2, unique_2.c_str(), "L" );
    sign_mc_ratio_legend->AddEntry( sign_mc_ratio, "ratio", "L" );
    sign_mc_ratio_legend->Draw();


    TPad * bckg_mc_pad = (TPad *) comparison_canv->cd( 4 );
    TH1F * bckg_mc_ratio = ratio_pad( bckg_mc_1, bckg_mc_2, bckg_mc_pad );
    bckg_mc_1->GetYaxis()->SetRangeUser( 0, std::max( { bckg_mc_1->GetMaximum(), bckg_mc_2->GetMaximum() } )*1.3 );
    add_pad_title( bckg_mc_pad, Form( "Background MC - %s %s %s/%s", mass.c_str(), abin_var.c_str(), unique_1.c_str(), unique_2.c_str() ), false );
    add_atlas_decorations( bckg_mc_pad, true, false );
    TLegend * bckg_mc_ratio_legend = below_logo_legend();
    bckg_mc_ratio_legend->AddEntry( bckg_mc_1, unique_1.c_str(), "L" );
    bckg_mc_ratio_legend->AddEntry( bckg_mc_2, unique_2.c_str(), "L" );
    bckg_mc_ratio_legend->AddEntry( bckg_mc_ratio, "ratio", "L" );
    bckg_mc_ratio_legend->Draw();

    comparison_canv->SaveAs( Form( "comparison_ratios_%s_%s_%s_%s.png", abin_var.c_str(), mass.c_str(), unique_1.c_str(), unique_2.c_str() ) );

    TCanvas * data_comparison_canv = new TCanvas( Form( "data_comparison_%s", mass.c_str() ), "", 200, 200, 2000, 1000 );
    data_comparison_canv->Divide( 2, 1 );

    TPad * data_ex_pad = (TPad *) data_comparison_canv->cd( 1 );
    TH1F * data_ex_ratio = ratio_pad( data_extracted_1, data_extracted_2, data_ex_pad );
    data_extracted_1->GetYaxis()->SetRangeUser( 0, std::max( { data_extracted_1->GetMaximum(), data_extracted_2->GetMaximum() } )*1.3 );
    add_pad_title( data_ex_pad, Form( "Data extracted - %s %s %s/%s", mass.c_str(), abin_var.c_str(), unique_1.c_str(), unique_2.c_str() ), false );
    add_atlas_decorations( data_ex_pad, true, false );
    TLegend * data_ex_ratio_legend = below_logo_legend();
    data_ex_ratio_legend->AddEntry( data_extracted_1, unique_1.c_str(), "L" );
    data_ex_ratio_legend->AddEntry( data_extracted_2, unique_2.c_str(), "L" );
    data_ex_ratio_legend->AddEntry( data_ex_ratio, "ratio", "L" );
    data_ex_ratio_legend->Draw();

    TPad * data_raw_pad = (TPad *) data_comparison_canv->cd( 2 );
    TH1F * data_raw_ratio = ratio_pad( data_raw_1, data_raw_2, data_raw_pad );
    data_raw_1->GetYaxis()->SetRangeUser( 0, std::max( { data_raw_1->GetMaximum(), data_raw_2->GetMaximum() } )*1.3 );
    add_pad_title( data_raw_pad, Form( "Data raw - %s %s %s/%s", mass.c_str(), abin_var.c_str(), unique_1.c_str(), unique_2.c_str() ), false );
    add_atlas_decorations( data_raw_pad, true, false );
    TLegend * data_raw_ratio_legend = below_logo_legend();
    data_raw_ratio_legend->AddEntry( data_raw_1, unique_1.c_str(), "L" );
    data_raw_ratio_legend->AddEntry( data_raw_2, unique_2.c_str(), "L" );
    data_raw_ratio_legend->AddEntry( data_raw_ratio, "ratio", "L" );
    data_raw_ratio_legend->Draw();


    data_comparison_canv->SaveAs( Form( "data_comparison_ratios_%s_%s_%s_%s.png", abin_var.c_str(), mass.c_str(), unique_1.c_str(), unique_2.c_str() ) );


  }

}
