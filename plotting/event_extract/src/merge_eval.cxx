#include <merge_eval.hxx>

TH1F * split_fit( basic_fileset * fileset, variable_set & variables, style_mgr * styles, fit_mgr * fits, const std::string & variable ){

  const char * unique = fileset->get_unique();
  bound & variable_bound = variables.bound_manager->get_bound( variable );
  hist_group histograms = fileset->recreate_histograms_weights( variables, true, false, variable );
  TH1F * sign = histograms.sign_hist;

  styles->style_histogram( sign, "extracted_sign" );

  TF1 * dg = prep_dg( variable_bound.get_min(), variable_bound.get_max() );
  align_dg( dg, sign, true );
  fits->load_parameterisation( dg, Form( "event_extract_fit_process_%s_Q12_%s", variable.c_str(), unique ), "dg", false );

  TCanvas * split = new TCanvas( unique, "", 200, 200, 1000, 1000 );
  split->Divide( 1, 1 );
  TPad * split_pad = (TPad *) split->cd( 1 );

  sign->Draw( "HIST E1" );
  hist_prep_axes( sign );
  set_axis_labels( sign, variable_bound.get_x_str(), Form( "Yield/%s", variable_bound.get_y_str().c_str() ) );
  add_pad_title( split_pad, Form( "%s %s", unique, variable_bound.get_ltx().c_str() ) );
  add_atlas_decorations( split_pad, true, false );
  sign->Fit( dg, "MQR", "", variable_bound.get_min(), variable_bound.get_max() );
  TPaveStats * stats = make_stats( sign );
  stats->Draw( "SAME" );
  dg->Draw( "SAME" );

  split->SaveAs( Form( "merge_eval_solo_fit_%s_%s.png", unique, variable.c_str() ) );
  delete dg;
  delete split;
  return sign;
}


void compare_merge( TH1F * post_merge_hist, std::vector< TH1F *> & pre_merge_hists, variable_set & variables, fit_mgr * fits, const std::string & variable, const std::string & unique ){

  gStyle->SetOptStat( "miners" );

  const char * unique_c = unique.c_str();
  bound & variable_bound = variables.bound_manager->get_bound( variable );
  TH1F * pre_merge_hist = (TH1F *) post_merge_hist->Clone();
  pre_merge_hist->Reset();
  std::for_each( pre_merge_hists.begin(), pre_merge_hists.end(), [ pre_merge_hist ]( TH1F * split ){ pre_merge_hist->Add( split ); } );
  TH1F * pre_rat = (TH1F *) pre_merge_hist->Clone();
  TH1F * post_rat = (TH1F *) post_merge_hist->Clone();
  pre_merge_hist->SetName( "pre merge" ); post_merge_hist->SetName( "post merge" );
  pre_rat->SetName( "pre merge" ); post_rat->SetName( "post merge" );

  TF1 * post_dg = prep_dg();
  TF1 * pre_dg = prep_dg();
  fits->load_parameterisation( post_dg, Form( "merge_eval_compare_post_%s_Q12_%s", variable.c_str(), unique_c ), "dg", false );
  fits->load_parameterisation( pre_dg, Form( "merge_eval_compare_pre_%s_Q12_%s", variable.c_str(), unique_c ), "dg", false );
  align_dg( post_dg, post_merge_hist );
  align_dg( pre_dg, pre_merge_hist );
  post_dg->SetParLimits( 4, 2, 4 );
  pre_dg->SetParLimits( 4, 2, 4 );


  TCanvas * merge = new TCanvas( unique_c, "", 200, 200, 2000, 2000 );
  merge->Divide( 2, 2 );

  TPad * merge_pad = (TPad *) merge->cd( 1 );
  TH1F * extract_ratio = ratio_pad( pre_rat, post_rat, merge_pad );
  merge_pad = (TPad *) merge->cd( 2 );
  TH1F * draw_ratio = (TH1F *) extract_ratio->Clone();
  draw_ratio->Draw( "HIST E1" );
  hist_prep_axes( draw_ratio, true );
  set_axis_labels( draw_ratio, variable_bound.get_x_str(), Form( "Ratio/%s", variable_bound.get_y_str().c_str() ) );
  add_pad_title( merge_pad, Form( "Ratio %s %s", unique_c, variable_bound.get_ltx().c_str() ) );
  add_atlas_decorations( merge_pad, true, false );

  merge_pad = (TPad *) merge->cd( 3 );
  post_merge_hist->Draw( "HIST E1" );
  hist_prep_axes( post_merge_hist );
  post_merge_hist->GetYaxis()->SetRangeUser( 0, 16500 );
  set_axis_labels( post_merge_hist, variable_bound.get_x_str(), Form( "Yield/%s", variable_bound.get_y_str().c_str() ) );
  add_pad_title( merge_pad, Form( "hf after merge %s %s", unique_c, variable_bound.get_ltx().c_str() ) );
  add_atlas_decorations( merge_pad, true, false );
  post_merge_hist->Fit( post_dg, "MQR", "", variable_bound.get_min(), variable_bound.get_max() );
  post_dg->Draw( "SAME" );
  TPaveStats * post_stats = make_stats( post_merge_hist );
  post_stats->Draw( "SAME" );


  merge_pad = (TPad *) merge->cd( 4 );
  pre_merge_hist->Draw( "HIST E1" );
  hist_prep_axes( pre_merge_hist );
  pre_merge_hist->GetYaxis()->SetRangeUser( 0, 16500 );
  set_axis_labels( pre_merge_hist, variable_bound.get_x_str(), Form( "Yield/%s", variable_bound.get_y_str().c_str() ) );
  add_pad_title( merge_pad, Form( "hf before merge %s %s", unique_c, variable_bound.get_ltx().c_str() ) );
  add_atlas_decorations( merge_pad, true, false );
  pre_merge_hist->Fit( pre_dg, "MQR", "", variable_bound.get_min(), variable_bound.get_max() );
  pre_dg->Draw( "SAME" );
  TPaveStats * pre_stats = make_stats( pre_merge_hist );
  pre_stats->Draw( "SAME" );

  merge->SaveAs( Form( "merge_eval_compare_fit_%s_%s.png", unique_c, variable.c_str() ) );

  delete post_dg;
  delete pre_dg;
  delete merge;

}

void mass_split_study( basic_fileset * merge, variable_set & variables, style_mgr * styles, fit_mgr * fits, const std::string & variable ){

  const char * unique = merge->get_unique();
  gStyle->SetOptStat( "ourmein" );

  if ( fits == nullptr ){ return; }

  bound & variable_bound = variables.bound_manager->get_bound( variable );
  TH1F * merge_hist = variable_bound.get_hist( "qmerge" );
  TH1F * merge_sub_hist = variable_bound.get_hist( "qmerge_sub" );

  variables.mass = "Q3";
  TH1F * q3_hist = merge->get_weighted_histogram( sign, variables, variable, false, true, "Q3" );
  TH1F * q3_sub_hist = merge->get_subtracted_histogram( sign, variables, variable, false, true, "Q3_sub" );
  variables.mass = "Q4";
  TH1F * q4_hist = merge->get_weighted_histogram( sign, variables, variable, false, true, "Q4" );
  TH1F * q4_sub_hist = merge->get_subtracted_histogram( sign, variables, variable, false, true, "Q4_sub" );
  variables.mass = "Q5";
  TH1F * q5_hist = merge->get_weighted_histogram( sign, variables, variable, false, true, "Q5" );
  TH1F * q5_sub_hist = merge->get_subtracted_histogram( sign, variables, variable, false, true, "Q5_sub" );


  merge_hist->Add( q3_hist );
  merge_hist->Add( q4_hist );
  merge_hist->Add( q5_hist );
  merge_sub_hist->Add( q3_sub_hist );
  merge_sub_hist->Add( q4_sub_hist );
  merge_sub_hist->Add( q5_sub_hist );


  styles->style_histogram( q3_hist, "extracted_sign" );
  styles->style_histogram( q4_hist, "extracted_sign" );
  styles->style_histogram( q5_hist, "extracted_sign" );
  styles->style_histogram( merge_hist, "extracted_sign" );
  styles->style_histogram( q3_sub_hist, "extracted_sign" );
  styles->style_histogram( q4_sub_hist, "extracted_sign" );
  styles->style_histogram( q5_sub_hist, "extracted_sign" );
  styles->style_histogram( merge_sub_hist, "extracted_sign" );
  q3_sub_hist->SetLineStyle( 2 ); 
  q4_sub_hist->SetLineStyle( 2 ); 
  q5_sub_hist->SetLineStyle( 2 ); 
  merge_sub_hist->SetLineStyle( 2 );


  TF1 * q3_fit = fits->prep_function( dg, variable_bound.get_min(), variable_bound.get_max() );
  TF1 * q4_fit = fits->prep_function( dg, variable_bound.get_min(), variable_bound.get_max() );
  TF1 * q5_fit = fits->prep_function( dg, variable_bound.get_min(), variable_bound.get_max() );
  TF1 * merge_fit = fits->prep_function( dg, variable_bound.get_min(), variable_bound.get_max() );
  q3_fit->SetLineColor( 1 ); 
  q4_fit->SetLineColor( 1 );
  q5_fit->SetLineColor( 1 );
  merge_fit->SetLineColor( 1 );
  
  q3_fit->SetParLimits( 4, 2, 4 );
  q4_fit->SetParLimits( 4, 2, 4 );
  q5_fit->SetParLimits( 4, 2, 4 );
  merge_fit->SetParLimits( 4, 2, 4 );


  align_dg( q3_fit, q3_hist );
  align_dg( q4_fit, q4_hist );
  align_dg( q5_fit, q5_hist );
  align_dg( merge_fit, merge_hist );


  TCanvas * mass_canv = new TCanvas( "mass_canv", "", 200, 200, 2000, 2000 );
  mass_canv->Divide( 2, 2 );
  
  TPad * pad = (TPad *) mass_canv->cd( 1 );
  TH1F * q3_ratio = ratio_pad( q3_hist, q3_sub_hist, pad );
  pad = (TPad *) mass_canv->cd( 1 );
  add_pad_title( pad, Form( "Q3 %s", variable_bound.get_ltx().c_str() ), false );
  add_atlas_decorations( pad, true, false );
  pad = (TPad *) pad->cd( 1 );
  q3_hist->Fit( q3_fit, "MQR" );
  q3_fit->Draw( "SAME" );
  pad = (TPad *) mass_canv->cd( 1 );
  TLegend * q3_legend = below_logo_legend();
  q3_legend->AddEntry( q3_hist, "q3", "L" );
  q3_legend->AddEntry( q3_sub_hist, "q3_sub", "L" );
  q3_legend->AddEntry( q3_ratio, "q3_ratio", "L" );
  q3_legend->Draw( "SAME" );


  pad = (TPad *) mass_canv->cd( 2 );
  TH1F * q4_ratio = ratio_pad( q4_hist, q4_sub_hist, pad );
  pad = (TPad *) mass_canv->cd( 2 );
  add_pad_title( pad, Form( "Q4 %s", variable_bound.get_ltx().c_str() ), false );
  add_atlas_decorations( pad, true, false );
  pad = (TPad *) pad->cd( 1 );
  q4_hist->Fit( q4_fit, "MQR" );
  q4_fit->Draw( "SAME" );
  pad = (TPad *) mass_canv->cd( 2 );
  TLegend * q4_legend = below_logo_legend();
  q4_legend->AddEntry( q4_hist, "q4", "L" );
  q4_legend->AddEntry( q4_sub_hist, "q4_sub", "L" );
  q4_legend->AddEntry( q4_ratio, "q4_ratio", "L" );
  q4_legend->Draw( "SAME" );


  pad = (TPad *) mass_canv->cd( 3 );
  TH1F * q5_ratio = ratio_pad( q5_hist, q5_sub_hist, pad );
  pad = (TPad *) mass_canv->cd( 3 );
  add_pad_title( pad, Form( "Q4 %s", variable_bound.get_ltx().c_str() ), false );
  add_atlas_decorations( pad, true, false );
  pad = (TPad *) pad->cd( 1 );
  q5_hist->Fit( q5_fit, "MQR" );
  q5_fit->Draw( "SAME" );
  pad = (TPad *) mass_canv->cd( 3 );
  TLegend * q5_legend = below_logo_legend();
  q5_legend->AddEntry( q5_hist, "q5", "L" );
  q5_legend->AddEntry( q5_sub_hist, "q5_sub", "L" );
  q5_legend->AddEntry( q5_ratio, "q5_ratio", "L" );
  q5_legend->Draw( "SAME" );


  pad = (TPad *) mass_canv->cd( 4 );
  TH1F * merge_ratio = ratio_pad( merge_hist, merge_sub_hist, pad );
  pad = (TPad *) mass_canv->cd( 4 );
  add_pad_title( pad, Form( "merge %s", variable_bound.get_ltx().c_str() ), false );
  add_atlas_decorations( pad, true, false );
  pad = (TPad *) pad->cd( 1 );
  merge_hist->Fit( merge_fit, "MQR" );
  merge_fit->Draw( "SAME" );
  pad = (TPad *) mass_canv->cd( 4 );
  TLegend * merge_legend = below_logo_legend();
  merge_legend->AddEntry( merge_hist, "merge", "L" );
  merge_legend->AddEntry( merge_sub_hist, "merge_sub", "L" );
  merge_legend->AddEntry( merge_ratio, "merge_ratio", "L" );
  merge_legend->Draw( "SAME" );

  mass_canv->SaveAs( Form( "mass_merge_%s_%s.png", variable.c_str(), unique ) );
  
}


void merge_split_eval( basic_fileset * merge, std::vector< basic_fileset *> splits,  variable_set & variables, const std::string & unique ){

  prep_style();

  std::cout << unique << std::endl;

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "default" );

  fit_mgr * fits = new fit_mgr();
  fits->load_fits();

  std::vector< TH1F * > pre_merge_hists, pre_merge_Q_hists;

  for ( basic_fileset * fileset : splits ){ pre_merge_hists.push_back( split_fit( fileset, variables, styles, fits, "qtA" ) ); }
  if ( merge == nullptr ){ std::cout << "nullmrg" << std::endl; }
  TH1F * post_merge_hist = split_fit( merge, variables, styles, fits, "qtA" );
  compare_merge( post_merge_hist, pre_merge_hists, variables, fits, "qtA", unique );

  mass_split_study( merge, variables, styles, fits, "qtA" );
  mass_split_study( splits.at( 0 ), variables, styles, fits, "qtA" );
  mass_split_study( splits.at( 1 ), variables, styles, fits, "qtA" );
  mass_split_study( splits.at( 2 ), variables, styles, fits, "qtA" );
  

  return;
}
