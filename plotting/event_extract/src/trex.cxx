#include <trex.hxx>
#include <hf_extr_plotters.hxx>


void fit_trex( TH1F * hist, fit_mgr * fits, style_mgr * styles, variable_set & variables, const char * unique ){

  gStyle->SetOptStat( "mines" );

  const char * mass = variables.mass.c_str();
  TH1F * hist_dg = static_cast<TH1F *>( hist->Clone( "trex_dg" ) );
  TH1F * hist_sg = static_cast<TH1F *>( hist->Clone( "trex_sg" ) );

  TF1 * dg_func = fits->prep_function( dg, variables.analysis_bound );
  TF1 * sg_func = fits->prep_function( sg, variables.analysis_bound );
  align_dg( dg_func, hist, true );
  align_dg( sg_func, hist, true );

  //styles->style_histogram( hist, "sign_extracted" );
  styles->style_histogram( hist_dg, "sign_extract" );
  styles->style_histogram( hist_sg, "sign_extract" );

  TCanvas canv = TCanvas( "", "", 200, 200, 2000, 1000 );
  canv.Divide( 2, 1 );

  TPad * current_pad = static_cast<TPad *>( canv.cd( 1 ) );
  // have this done automatically?
  hist_prep_axes( hist_dg, true );
  hist_dg->Draw( "HIST E1" );
  hist_dg->Fit( dg_func, "M" );
  dg_func->Draw( "SAME" );
  // write a new function that just takes a variables, or a bound, and a const char, for it to set this up.
  set_axis_labels( hist_dg, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str().c_str() );
  // write a function that applies a title to this buisiness, using a variables, no form, and a bonus string
  add_pad_title( current_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), mass ), true );
  // just auto apply this as part of the title buisiness?
  add_atlas_decorations( current_pad, true, false );
  // Track these automatically, disable with a function?
  TLegend * dg_fit_legend = below_logo_legend();
  dg_fit_legend->AddEntry( hist_dg, "extracted", "LP" );
  dg_fit_legend->AddEntry( dg_func,   "dg", "LP" );
  dg_fit_legend->Draw();
  // STATS CAN BE HAND MANAGED?
  TPaveStats * dg_fit_stats = make_stats( hist_dg );
  dg_fit_stats->Draw();

  current_pad = static_cast<TPad *>( canv.cd( 2 ) );
  hist_prep_axes( hist_sg, true );
  hist_sg->Draw( "HIST E1" );
  hist_sg->Fit( sg_func, "M" );
  sg_func->Draw( "SAME" );
  set_axis_labels( hist_sg, variables.analysis_bound.get_x_str(), variables.analysis_bound.get_y_str().c_str() );
  add_pad_title( current_pad, Form( "%s %s %s", unique, variables.analysis_variable.c_str(), mass ), true );
  add_atlas_decorations( current_pad, true, false );
  TLegend * fit_legend = below_logo_legend();
  fit_legend->AddEntry( hist_sg, "extracted", "LP" );
  fit_legend->AddEntry( sg_func,   "sg", "LP" );
  fit_legend->Draw();
  TPaveStats * sg_fit_stats = make_stats( hist_sg );
  sg_fit_stats->Draw();



  // function for this, autodetermines the name, give some readout with the other stuff
  canv.SaveAs( "trex_fit.png" );

  delete hist_dg;
  delete hist_sg;
}


void trex_extr( basic_fileset * fileset,  variable_set & variables ){

  prep_style();
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "default" );

  fit_mgr * fit_manager = new fit_mgr();
  fit_manager->load_fits();

 
  hist_group trex_hists = fileset->recreate_histograms_trex( variables, true, false );
  hist_group diff_hists = fileset->recreate_histograms_differential( variables, true, false );

  extraction_method_comparison( trex_hists.sign_hist, diff_hists.sign_hist, variables, "trex_vs_diff", "trex", "diff" );
  fit_trex( trex_hists.sign_hist, fit_manager, styles, variables, fileset->get_unique() );



  //delete trex_hists;
  //delete diff_hists;

  delete styles;
  delete fit_manager;

}
