#include <main.hxx>
#include <extr.hxx>
#include <hf_extr.hxx>
#include <trex.hxx>
#include <split_extr.hxx>
#include <post_merge_hf_extr.hxx>
#include <extr_cmp.hxx>
#include <merge_eval.hxx>

int help(){

  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"input",       required_argument,        0,      'i'},
      {"eff",         required_argument,        0,      'e'},
      {"anavar",      required_argument,        0,      'a'},
      {"specvar",     required_argument,        0,      's'},
      {"type",        required_argument,        0,      't'},
      {"unique",      required_argument,        0,      'u'},
      {"selections",  required_argument,        0,      'v'},
      {"stats",       required_argument,        0,      'b'},
      {"range",       required_argument,        0,      'r'},
      {"hf_mode",     no_argument,              0,      'f'},
      {"pos_mode",    no_argument,              0,      'p'},
      {"t_ranges",    no_argument,              0,      'm'},
      {0,             0,                        0,      0}
    };

  std::string input, eff, avar, svar, type;
  std::string unique, selections_file, ranges;
  std::string stats_path, sub_path, trex_path;
  bool pos_mode = false;
  //bool hf_mode = false;
  //bool compare_mode = false;   
  //bool post_merge = false;
  //bool merge_comp = false;
  int qtbsplit = 0;
  int n_merge = 0;
  std::string mode = "";


  do {
    option = getopt_long( argc, argv, "i:e:a:s:t:u:v:r:m:g:T:b:pofdqnch", long_options, &option_index);
    switch (option){
      case 'i': 
        input       = std::string( optarg );
        break;
      case 'e':
        eff         = std::string ( optarg );
        break;
      case 'a':
        avar        = std::string( optarg );
        break;
      case 's':
        svar        = std::string( optarg );
        break;
      case 't':
        type        = std::string( optarg );
        break;
      case 'u':
        unique      = std::string( optarg );
        break;
      case 'v':
        selections_file = std::string( optarg );
        break;
      case 'r':
        ranges      = std::string( optarg );
        break;
      case 'b':
        qtbsplit    = atoi( optarg );
        break;
      case 'g':
        sub_path    = std::string( optarg );
        break;
      case 'm':
        mode        = std::string( optarg );
        break;
      case 'n':
        n_merge = atoi( optarg );
        break;
      case 'p':
        pos_mode    = true;
        break;
      case 'T':
        trex_path   = std::string( optarg );
        break;
      //case 'f':
      //  hf_mode = true;
      //  break;
      //case 'c':
      //  compare_mode = true;
      //  break;
      //case 'q':
      //  post_merge = true;
      //  break;
      case 'h':
        return help();
    }
  } while (option != -1);


  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( selections_file );
  if ( !ranges.empty() ){
    selections->process_bounds_string( ranges );
  }

  if ( n_merge != 0 ){
    int file = n_merge - 1 ;
    std::vector< std::string > filepaths;
    split_strings( filepaths, input, ":" );
    std::string merged_filepath = "merged_tree.root";
    merge_tree( filepaths.at( file ), filepaths.at( file+1 ), merged_filepath );
    filepaths.erase( filepaths.begin() + file + 1 );
    filepaths.at( file ) = merged_filepath;
    auto colon_concat = []( std::string & first, std::string & second ){  return (first + ":" + second); };
    input = std::accumulate( filepaths.begin()+1, filepaths.end(), filepaths.at(0), colon_concat );
    std::cout << input << std::endl;
  }

  if ( mode.find( "extr_cmp" ) != std::string::npos ){
    extr_compare( input, avar, svar, selections, unique );
    return 0;
  }

  if ( mode.find( "post_merge" ) != std::string::npos ){
    std::cout << input << std::endl;
    post_hf_extr( input, avar, svar, type, selections, unique, eff, false, stats_path );
    return 0;
  }

  std::vector< std::string > input_filepaths;
  split_strings( input_filepaths, input, ":" );


  variable_set variables = variable_set( selections_file, ranges, avar, svar );

  // previously -f
  if ( mode.find( "hf" ) != std::string::npos ){
    std::cout << "hf" << std::endl;
  
    std::vector< std::string > path_strings;
    std::string hf_path = input_filepaths.at(0) + unique + "/eval/";
    std::string ef_path = input_filepaths.at(1);
    std::string sub_path = input_filepaths.at(2) + unique + "/";
    std::cout << hf_path << std::endl;
    std::cout << ef_path << std::endl;
    std::cout << sub_path << std::endl;
    basic_fileset input_fileset = basic_fileset();
    input_fileset.set_unique( unique );
    input_fileset.load_final_fileset( hf_path, ef_path, unique, false );
    input_fileset.load_subtraction_fileset( sub_path, unique );
    hf_extr( input_fileset, variables );
    return 0;
  }

  // previously -f with a -b number
  if ( mode.find( "split" ) != std::string::npos ){
    std::string & hf_path = input_filepaths.at( 0 ); 
    std::string & ef_path = input_filepaths.at( 1 ); 
    std::vector< basic_fileset * > qtbsplits;
    for ( int split = 0; split < qtbsplit; split ++ ){
      qtbsplits.push_back( new basic_fileset() );
      std::string current_unique = Form( "%s-%i", unique.c_str(), split+1 );
      std::string current_hf_path = hf_path + current_unique + "/eval/";
      std::string current_subtraction_path = sub_path + "/" + current_unique + "/";
      qtbsplits.at( split )->load_final_fileset( current_hf_path, ef_path, current_unique, false );
      qtbsplits.at( split )->load_subtraction_fileset( current_subtraction_path, current_unique );
    }
    split_extr( qtbsplits, variables, unique );

  } 

  if ( mode.find( "extr" ) != std::string::npos ){
    extr( input, avar, svar, type, selections, unique, false, pos_mode );
  }

  if ( mode.find( "merge" ) != std::string::npos ){

    std::string & hf_path = input_filepaths.at( 0 ); 
    std::string & ef_path = input_filepaths.at( 1 ); 
    std::vector< std::string > uniques;
    split_strings( uniques, unique, ":" );
    std::string & merge_unique = uniques.at( 0 );
    std::string & split_unique = uniques.at( 1 );
    std::vector< basic_fileset * > splits;
    basic_fileset * merge = new basic_fileset();
    std::string merge_hf_path = hf_path + merge_unique + "/eval/";
    merge->load_final_fileset( merge_hf_path, ef_path, merge_unique, false );
    for ( int split = 0; split < qtbsplit; split ++ ){
      splits.push_back( new basic_fileset() );
      std::string current_unique = Form( "%s-%i", split_unique.c_str(), split+1 );
      std::string current_hf_path = hf_path + current_unique + "/eval/";
      std::string current_subtraction_path = sub_path + "/" + current_unique + "/";
      splits.at( split )->load_final_fileset( current_hf_path, ef_path, current_unique, false );
      splits.at( split )->load_subtraction_fileset( current_subtraction_path, current_unique );
    }
    merge_split_eval( merge, splits, variables, uniques.at( 2 ) );
  }

  if ( mode.find( "trex" ) != std::string::npos ){
    
    basic_fileset * fileset = new basic_fileset();
    std::vector< std::string > path_strings;
    std::string hf_path = input_filepaths.at(0) + unique + "/eval/";
    std::string ef_path = input_filepaths.at(1);
    std::string sub_path = input_filepaths.at(2) + unique + "/";
    fileset->set_unique( unique );
    fileset->load_final_fileset( hf_path, ef_path, unique, false );
    fileset->load_subtraction_fileset( sub_path, unique );
    fileset->load_trex_fileset( trex_path, unique );
    trex_extr( fileset, variables );

  }




  return 0;


}
