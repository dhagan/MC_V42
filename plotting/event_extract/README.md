# event\_extract

## Description
Final solo signal extraction and examination for the event-based histfactory step.

## Usage

### Modes
* hf\_extr      - Produces a multitude of outputs. Poduces table of histfactory stats, hf extracted signal, background, subtraction signal, background and data, fit data, stacked combinations of signal and background, normalised extracted signal and background, and also produces weight plots.
* extr          - Regular extraction with subtraction involved, outputs analysis var and spectator var.
* extr\_compare - Direct comparison of the results of two runs of hf\_extr.
* add           - Combine histograms (only 1D histograms) after hf\_extr process
