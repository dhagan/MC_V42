#include <process_scalefactors.hxx>

std::vector< TH1F * > prepare_sf_hists ( bound & spec_bound ){

  std::vector< TH1F * > hists;
  int hist_index = 0;
  std::generate_n( std::back_inserter( hists ), 100, [&hist_index, &spec_bound ](){ return spec_bound.get_hist( std::to_string( hist_index ) ); } );
  return hists;
}

void fill_sf_variation( TTree * sf_tree, int & hist, const char * analysis_cut, const char * qtb_cut ){
  char draw_expr[9]; sprintf( draw_expr, "BDT>>%i", hist ); 
  char cut_expr[200]; sprintf( cut_expr, "muon_scalefactor_samples[%i]*(%s&&%s)", hist, analysis_cut, qtb_cut ); 
  sf_tree->Draw( draw_expr, cut_expr );
}

double calculate_rms( int & bin, std::vector< TH1F *> & bdt_hists ){
  return std::sqrt( std::accumulate( bdt_hists.begin(), bdt_hists.end(), 0, [ &bin ]( const double & rms, TH1F * hist ){ return rms + hist->GetBinContent( bin )*hist->GetBinContent( bin ); } )/100 );
}

void process_scalefactors( TTree * sf_tree, TTree * core_tree, variable_set * variables ){

  core_tree->AddFriend( sf_tree );

  bound & analysis_bound = variables->analysis_bound;
  bound & spectator_bound = variables->spectator_bound;
  bound & qtb_bound = variables->bound_manager->get_bound( "qtB" );
  std::string qtb_cut = qtb_bound.get_cut();
  std::vector< std::string > analysis_cut_series = analysis_bound.get_cut_series();
  std::vector< std::string > analysis_cut_names = analysis_bound.get_series_names();
  std::vector< TH1F * > output_histograms( analysis_bound.get_bins() ) ;

  for ( int analysis_bin = 0; analysis_bin < analysis_bound.get_bins(); analysis_bin++ ){

    std::vector< TH1F * > bdt_hists = prepare_sf_hists( spectator_bound );

    for ( int hist = 0; hist < 100; hist++ ){
      fill_sf_variation( core_tree, hist, analysis_cut_series[analysis_bin].c_str(), qtb_cut.c_str() ); 
    }

    TH1F * rms_hist = spectator_bound.get_hist( "RMS" );
    for ( int bin = 1; bin < spectator_bound.get_bins(); bin++ ){  
      rms_hist->SetBinContent( bin, calculate_rms( bin, bdt_hists ) );
    }
  
    rms_hist->SetName( analysis_cut_names[ analysis_bin ].c_str() );
    output_histograms.push_back( rms_hist );

  }

}
