
#include <ntuple_utils.hxx>

bool VatoCS( const TLorentzVector &mupl, const TLorentzVector &mumi, const TLorentzVector &gamma,
	           TVector3 &CSAxis, TVector3 &xAxis, TVector3 &yAxis,
	           double &cosTheta_dimu, double &cosTheta_gamma,
	           double &phi_dimu, double &phi_gamma ){

  TLorentzVector dimu = mupl + mumi;
  TLorentzVector higgs = dimu + gamma;

  double ProtonMass = 938.272;  // MeV
  double BeamEnergy = 6500000.; // MeV

  // 1)
  TLorentzVector p1, p2;
  p1.SetPxPyPzE( 0., 0., BeamEnergy, TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass ) );
  p2.SetPxPyPzE( 0., 0., -1 * BeamEnergy, TMath::Sqrt(BeamEnergy * BeamEnergy + ProtonMass * ProtonMass ) );

  TLorentzVector dimu_hf, mupl_hf, mumi_hf, gamma_hf, higgs_hf, p1_hf, p2_hf;

  dimu_hf = dimu;
  mupl_hf = mupl;
  mumi_hf = mumi;
  gamma_hf = gamma;
  higgs_hf = higgs;
  p1_hf = p1;
  p2_hf = p2;

  dimu_hf.Boost( -higgs.BoostVector() );
  mupl_hf.Boost( -higgs.BoostVector() );
  mumi_hf.Boost( -higgs.BoostVector() );
  gamma_hf.Boost( -higgs.BoostVector() );
  higgs_hf.Boost( -higgs.BoostVector() );
  p1_hf.Boost( -higgs.BoostVector() );
  p2_hf.Boost( -higgs.BoostVector() );

  // 3)
  CSAxis = ( p1_hf.Vect().Unit() - p2_hf.Vect().Unit()).Unit();
  yAxis =  ( p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()) );
  yAxis = yAxis.Unit();
  xAxis = yAxis.Cross( CSAxis );
  xAxis = xAxis.Unit();

  //4)
  phi_dimu = atan2( (dimu_hf.Vect() * yAxis), (dimu_hf.Vect() * xAxis) );
  cosTheta_dimu = ( dimu_hf.Vect().Dot( CSAxis ) )/( dimu_hf.Vect().Mag() );

  // cross-check
  cosTheta_gamma = ( gamma_hf.Vect().Dot(CSAxis) )/( gamma_hf.Vect().Mag() );
  phi_gamma = atan2( ( gamma_hf.Vect() * yAxis ), ( gamma_hf.Vect() * xAxis ) );

  if ( fabs(cosTheta_dimu + cosTheta_gamma) > 0.001){
    std::cout << "VatoCS: cosThetaCS sum, beyond tolerance: " << cosTheta_dimu << " " << cosTheta_gamma << std::endl;
  }
  if (fabs(sin(phi_dimu - phi_gamma)) > 0.001){
    std::cout << "VatoCS: phiCS difference, beyond tolerance: " << phi_dimu << " " << phi_gamma << std::endl;
  }
  return true;
}

Float_t dphi_calc( const ROOT::RVecF &DiLept_Phi, const ROOT::RVecF &Photon_Phi ){
	Float_t dPhi = (DiLept_Phi[0] - Photon_Phi[0]); 
	while( dPhi > M_PI ){ dPhi -= 2*M_PI; };
	while( dPhi < -M_PI ){ dPhi += 2*M_PI; };
	return dPhi;
}


Float_t phi_calc( const ROOT::RVecF &MuMuGamma_CS_Phi ){
	return ( MuMuGamma_CS_Phi[0] >= 0 ) ? ( MuMuGamma_CS_Phi[0] - M_PI ) : ( MuMuGamma_CS_Phi[0] + M_PI );
}

Float_t photon_dr_calc( const ROOT::RVecF &Photon_Pt, const ROOT::RVecF &Photon_Eta, const ROOT::RVecF &Photon_Phi, const ROOT::RVecF &Photon_E,
											 const ROOT::RVecF &mcPhoton_Pt,  const ROOT::RVecF &mcPhoton_Eta, const ROOT::RVecF &mcPhoton_Phi ){
  TLorentzVector tlv_truth_photon,tlv_reco_photon,tlv_truth_muplus,tlv_reco_muplus;
  tlv_reco_photon.SetPtEtaPhiE( Photon_Pt[0], Photon_Eta[0], Photon_Phi[0], Photon_E[0] );
  tlv_truth_photon.SetPtEtaPhiM( mcPhoton_Pt[0], mcPhoton_Eta[0], mcPhoton_Phi[0], 0.0 );
  Float_t PhotonDRTruthReco  = tlv_reco_photon.DeltaR(tlv_truth_photon) ;
	return PhotonDRTruthReco;
}
