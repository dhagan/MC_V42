#include <common.hxx>
#include <bound_mgr.hxx>

#include <ntuple_utils.hxx>
#include <process_scalefactors.hxx>

int main(int argc, char ** argv){

  int option_index{0},option{0};
  static struct option long_options[] = {
      { "help",           no_argument,          0,      'h'},
      { "input",          required_argument,    0,      'i'},
      { "type",           required_argument,    0,      't'},
      { "unique",         required_argument,    0,      'u'},
      { "ranges",         required_argument,    0,      'r'},
      { "selection",      required_argument,    0,      'v'},
      { "reco",           no_argument,          0,      'o'},
      { 0,                0,                    0,      0}
  };

  std::string subtraction_filepath;
  const char * sf_filepath = "";
  std::string input_type;
  std::string unique = "";
  std::string ranges = "";
  std::string selection_file;

  const char * config_path = "";

  do {
    option = getopt_long( argc, argv, "s:b:i:t:u:r:v:c:", long_options, &option_index );
    switch ( option ){
      case 'c':
        config_path = optarg; 
        option = -1;
        break;
      case 'b':
        subtraction_filepath = std::string( optarg );
        break;
      case 's':
        sf_filepath = optarg;
        break;
      case 't':
        input_type  = std::string(optarg);
        break;
      case 'u':
        unique      = std::string(optarg);
        break;
      case 'r':
        ranges      = std::string(optarg);
        break;
      case 'v':
        selection_file  = std::string( optarg );
        break;
    }
  } while ( option != -1 );

  std::cout << config_path << std::endl;

  sample_type type = static_cast<sample_type>( std::distance( type_c.begin(), std::find( type_c.begin(), type_c.end(), input_type ) ) ); 
  variable_set * variables = new variable_set( selection_file, ranges, "qtA", "Q12" );

  basic_fileset * core_fileset = new basic_fileset();
  core_fileset->load_subtraction_fileset( subtraction_filepath, unique );
  TTree * core_tree = core_fileset->get_tree( type );

  TFile * sf_file = new TFile( sf_filepath, "READ" );
  TTree * sf_tree = ( TTree * ) sf_file->Get( "tree" );

  process_scalefactors( sf_tree, core_tree, variables );

  delete core_fileset;
  delete variables;

  //bdt_fileset->
    

}

