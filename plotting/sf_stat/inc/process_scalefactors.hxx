#include <common.hxx>
#include <bound_mgr.hxx>
#include <cutflow.hxx>
#include <fileset.hxx>
#include <scalefactor.hxx>
#include <ntuple_utils.hxx>
#include <ROOT/RDataFrame.hxx>

void process_scalefactors( TTree * sf_tree, TTree * core_tree, variable_set * variables );
