executable=${exec_path}/sf_stat
unique="base"
selections=${LIB_PATH}/share/ntuple_selection_bounds.txt
files=( "sign" "data" "bckg" "bbbg" )

mkdir -p ${OUT_PATH}/sf_stat/${unique}
pushd ${OUT_PATH}/sf_stat/${unique} >> /dev/null

for file_index  in {0..0};
do
	file="${files[$file_index]}"
	mkdir -p ${file}
	subtraction_filepath="${OUT_PATH}/event_subtraction/${unique}/"
	sf_filepath="${OUT_PATH}/trees/${unique}/${file}/sf_${file}_${unique}.root"
	${executable} -b ${subtraction_filepath} -s ${sf_filepath} -t ${file} -u ${unique} -v ${selections}
done

popd >> /dev/null
