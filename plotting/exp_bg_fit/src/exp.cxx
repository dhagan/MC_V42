#include <exp.hxx>

void exp( std::string & input_filepath, bound_mgr * selections, std::string & unique ){

  prep_style();

  TFile * tree_file = new TFile( input_filepath.c_str(), "READ" );
  TTree * tree = (TTree *) tree_file->Get( "tree" );

  bound jpsi_mass = selections->get_bound( "jpsi_mass_branch" );
  bound qta_bound = selections->get_bound( "qtA" );

  TH1F * mass_g_hist = new TH1F( "mass_g_hist", "", jpsi_mass.get_bins(), 
                              jpsi_mass.get_min(), jpsi_mass.get_max() );

  std::vector< float > hist_style = { 1.00, 0, 0.00, 0, 1.00, 1.00, 1.00, 1.00, 0.00, 0.00, 0.00 };

  tree->Draw( Form( "%s>>mass_g_hist", jpsi_mass.get_var().c_str()  ), "", "goff" );  
  //tree->Draw( Form( "%s>>mass_bw_hist", jpsi_mass.get_var().c_str()  ), "", "goff" );  

  TF1 * combine_func = new TF1( "dual_fit", "[0]*e^(-[1]*x) + [2]*e^((-([3]-x)^(2))/(2*([4])^(2)))", 
                               jpsi_mass.get_min(), jpsi_mass.get_max() );

  combine_func->SetParName( 0, "c_{1}" );
  combine_func->SetParName( 1, "a" );
  combine_func->SetParName( 2, "c_{2}" );
  combine_func->SetParName( 3, "#bar{x}" );
  combine_func->SetParName( 4, "#sigma" );

  combine_func->SetParameter( 0, 1000 );
  combine_func->SetParameter( 1, 0.01 );
  combine_func->SetParameter( 2, mass_g_hist->GetMaximum()/2.0 );
  combine_func->SetParameter( 3, 3096.0 );
  combine_func->SetParameter( 4, mass_g_hist->GetStdDev()/2.0 );

  TF1 * g2 = new TF1( "g2", "[0]*e^(-[1]*x) + [2]*e^((-([3]-x)^(2))/(2*([4])^(2)))", 
                               jpsi_mass.get_min(), jpsi_mass.get_max() );
  g2->SetParName( 0, "c_{1}" );
  g2->SetParName( 1, "a" );
  g2->SetParName( 2, "c_{2}" );
  g2->SetParName( 3, "#bar{x}" );
  g2->SetParName( 4, "#sigma" );

  g2->SetParameter( 0, 1000 );
  g2->SetParameter( 1, 0.01 );
  g2->SetParameter( 2, mass_g_hist->GetMaximum()/2.0 );
  g2->SetParameter( 3, 3096.0 );
  g2->SetParameter( 4, mass_g_hist->GetStdDev()/2.0 );


  TF1 * g3 = new TF1( "g3", "[0]*e^(-[1]*x) + [2]*e^((-([3]-x)^(2))/(2*([4])^(2)))", 
                               jpsi_mass.get_min(), jpsi_mass.get_max() );

  g3->SetParName( 0, "c_{1}" );
  g3->SetParName( 1, "a" );
  g3->SetParName( 2, "c_{2}" );
  g3->SetParName( 3, "#bar{x}" );
  g3->SetParName( 4, "#sigma" );

  g3->SetParameter( 0, 1000 );
  g3->SetParameter( 1, 0.01 );
  g3->SetParameter( 2, mass_g_hist->GetMaximum()/2.0 );
  g3->SetParameter( 3, 3096.0 );
  g3->SetParameter( 4, mass_g_hist->GetStdDev() );

  TF1 * g4 = new TF1( "g4", "[0]*e^(-[1]*x) + [2]*e^((-([3]-x)^(2))/(2*([4])^(2)))", 
                               jpsi_mass.get_min(), jpsi_mass.get_max() );

  g4->SetParName( 0, "c_{1}" );
  g4->SetParName( 1, "a" );
  g4->SetParName( 2, "c_{2}" );
  g4->SetParName( 3, "#bar{x}" );
  g4->SetParName( 4, "#sigma" );

  g4->SetParameter( 0, 1000 );
  g4->SetParameter( 1, 0.01 );
  g4->SetParameter( 2, mass_g_hist->GetMaximum()/2.0 );
  g4->SetParameter( 3, 3096.0 );
  g4->SetParameter( 4, mass_g_hist->GetStdDev() );



  std::vector< double > y_exp_vec;
  TH1F * y_calc_hist = new TH1F( "y_calc_hist", "", jpsi_mass.get_bins(), jpsi_mass.get_min(), jpsi_mass.get_max() );
  std::vector< std::string > bins = qta_bound.get_cut_series();
  for( int idx = 0; idx < qta_bound.get_bins(); idx++ ){
    //std::cout << "qta-" << idx+1 << std::endl;
    //std::cout << bins[idx] << std::endl;
    tree->Draw( Form( "%s>>y_calc_hist", jpsi_mass.get_var().c_str() ), bins[idx].c_str(), "goff" );  
    double integral_region_1 = y_calc_hist->Integral( 1, 25 );
    double integral_region_4 = y_calc_hist->Integral( 75, 100 );
    double y_by_int = std::pow( integral_region_4/integral_region_1, 1.0/3.0 );  
    //std::cout << "y by integral" << y_by_int << std::endl;
    y_exp_vec.push_back( y_by_int );
  }

  std::string y_exp_initial = Form( "%.5f", y_exp_vec.at( 0 ) );
  auto delimit = []( std::string & initial, float element ){ return initial += Form( ":%.5f", element ); };
  std::string y_exp_output = std::accumulate( y_exp_vec.begin()+1, y_exp_vec.end(), y_exp_initial, delimit );
  std::cout << "\"" << y_exp_output << "\"" << std::endl;

  TCanvas * new_canv = new TCanvas( "jpsi", "jpsi", 200, 200, 2000, 2000 );

  new_canv->Divide( 2, 2 );

  TH1F * mass_hist_1 = new TH1F( "mass_hist_1", "", jpsi_mass.get_bins(), jpsi_mass.get_min(), jpsi_mass.get_max() );
  TH1F * mass_hist_2 = new TH1F( "mass_hist_2", "", jpsi_mass.get_bins(), jpsi_mass.get_min(), jpsi_mass.get_max() );
  TH1F * mass_hist_3 = new TH1F( "mass_hist_3", "", jpsi_mass.get_bins(), jpsi_mass.get_min(), jpsi_mass.get_max() );
  TH1F * mass_hist_4 = new TH1F( "mass_hist_4", "", jpsi_mass.get_bins(), jpsi_mass.get_min(), jpsi_mass.get_max() );


  tree->Draw( Form( "%s>>mass_hist_1", jpsi_mass.get_var().c_str()  ), "qtA>-10.0&&qtA<15.0", "goff" );  
  tree->Draw( Form( "%s>>mass_hist_2", jpsi_mass.get_var().c_str()  ), "qtA>-10.0&&qtA<2.0", "goff" );  
  tree->Draw( Form( "%s>>mass_hist_3", jpsi_mass.get_var().c_str()  ), "qtA>2.0&&qtA<8.0", "goff" );  
  tree->Draw( Form( "%s>>mass_hist_4", jpsi_mass.get_var().c_str()  ), "qtA>8.0&&qtA<15.0", "goff" );  
  
  //double bin_width = jpsi_mass.get_bin_width();

  //double integral_1_region_0 = mass_hist_1->Integral( 1, 25 );
  //double integral_4_region_0 = mass_hist_1->Integral( 75, 100 );
  //double integral_1_region_1 = mass_hist_2->Integral( 1, 25 );
  //double integral_4_region_1 = mass_hist_2->Integral( 75, 100 );
  //double integral_1_region_2 = mass_hist_3->Integral( 1, 25 );
  //double integral_4_region_2 = mass_hist_3->Integral( 75, 100 );
  //double integral_1_region_3 = mass_hist_4->Integral( 1, 25 );
  //double integral_4_region_3 = mass_hist_4->Integral( 75, 100 );

  //double full_y_region_0 = std::pow( integral_4_region_0/integral_1_region_0, 1.0/3.0 );  
  //double full_y_region_1 = std::pow( integral_4_region_1/integral_1_region_1, 1.0/3.0 );
  //double full_y_region_2 = std::pow( integral_4_region_2/integral_1_region_2, 1.0/3.0 );
  //double full_y_region_3 = std::pow( integral_4_region_3/integral_1_region_3, 1.0/3.0 );

  //std::cout << "y integral calculated - full qta - " << full_y_region_0 << std::endl;
  //std::cout << "y integral calculated - region 1 - " << full_y_region_1 << std::endl;
  //std::cout << "y integral calculated - region 2 - " << full_y_region_2 << std::endl;
  //std::cout << "y integral calculated - region 3 - " << full_y_region_3 << std::endl;




  TPad * current_pad = (TPad *) new_canv->cd(1);
  mass_hist_1->Draw( "HIST E1 X0" );
  style_hist( mass_hist_1, hist_style );
  hist_prep_axes( mass_hist_1, true );
  add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  set_axis_labels( mass_hist_1, jpsi_mass.get_ltx(), "Yield" );
  add_atlas_decorations( current_pad, true, false );
  mass_hist_1->Fit( combine_func, "MLIQ", "", jpsi_mass.get_min(), jpsi_mass.get_max() );
  combine_func->Draw( "SAME" );
  TLegend * fit_legend = below_logo_legend();
  fit_legend->AddEntry( mass_hist_1, "jpsi mass" ); 
  fit_legend->AddEntry( combine_func, "gauss and exp" ); 
  fit_legend->AddEntry( "Calculated y - " );
  TPaveStats * g_stats = make_stats( mass_hist_1 ); 
  g_stats->Draw(  );


  current_pad = (TPad * ) new_canv->cd( 2 );
  mass_hist_2->Draw( "HIST E1 X0" );
  style_hist( mass_hist_2, hist_style );
  hist_prep_axes( mass_hist_2, true );
  add_pad_title( current_pad, "J/#psi Mass, exponential and g", true);
  set_axis_labels( mass_hist_2, jpsi_mass.get_ltx(), "Yield" );
  add_atlas_decorations( current_pad, true, false );
  mass_hist_2->Fit( g2, "ML", "", jpsi_mass.get_min(), jpsi_mass.get_max() );
  g2->Draw( "SAME" );
  TLegend * fit_legend_g2 = below_logo_legend();
  fit_legend_g2->AddEntry( mass_hist_2, "jpsi mass" ); 
  fit_legend_g2->AddEntry( g2, "gauss and exp" ); 
  TPaveStats * g2_stats = make_stats( mass_hist_2 ); 
  g2_stats->Draw();

  current_pad = (TPad * ) new_canv->cd( 3 );
  mass_hist_3->Draw( "HIST E1" );
  style_hist( mass_hist_3, hist_style );
  hist_prep_axes( mass_hist_3, true );
  add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  set_axis_labels( mass_hist_3, jpsi_mass.get_ltx(), "Yield" );
  add_atlas_decorations( current_pad, true, false );
  mass_hist_3->Fit( g3, "ML", "", jpsi_mass.get_min(), jpsi_mass.get_max() );
  g3->Draw( "SAME" );
  TLegend * fit_legend_g3 = below_logo_legend();
  fit_legend_g3->AddEntry( mass_hist_3, "jpsi mass" ); 
  fit_legend_g3->AddEntry( g3, "gauss and exp" ); 
  TPaveStats * g3_stats = make_stats( mass_hist_3 ); 
  g3_stats->Draw();

  current_pad = (TPad * ) new_canv->cd( 4 );
  mass_hist_4->Draw( "HIST E1" );
  style_hist( mass_hist_4, hist_style );
  hist_prep_axes( mass_hist_4, true );
  add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  set_axis_labels( mass_hist_4, jpsi_mass.get_ltx(), "Yield" );
  add_atlas_decorations( current_pad, true, false );
  mass_hist_4->Fit( g4, "ML", "", jpsi_mass.get_min(), jpsi_mass.get_max() );
  g4->Draw( "SAME" );
  TLegend * fit_legend_g4 = below_logo_legend();
  fit_legend_g4->AddEntry( mass_hist_4, "jpsi mass" ); 
  fit_legend_g4->AddEntry( g4, "gauss and exp" ); 
  TPaveStats * g4_stats = make_stats( mass_hist_4 ); 
  g4_stats->Draw();


  new_canv->SaveAs( Form( "mass_qta_%s.png", unique.c_str() ) );

}

//#include <TMath.hxx>

//#include <PdfFuncMathCore.h>

//Double_t neg_exp( Double_t * x, Double_t * par ){
//  return par[0] * exp( - par[1] * x[0] );
//}
//
//Double_t exp_extra( Double_t * x, Double_t * par ){
//  double y = neg_exp( x, par );
//  double c = (y + y*y)/(1 + y*y*y); 
//}

//Double_t breit_wigner( Double_t * x, Double_t * par){
//  Double_t arg1 = 2.0/M_PI; // 2 over pi
//  Double_t arg2 = par[1]*par[1]*par[2]*par[2]; //Gamma=par[1]  M=par[2]
//  Double_t arg3 = ((x[0]*x[0]) - (par[2]*par[2]))*((x[0]*x[0]) - (par[2]*par[2]));
//  Double_t arg4 = x[0]*x[0]*x[0]*x[0]*((par[1]*par[1])/(par[2]*par[2]));
//  return par[0]*arg1*arg2/(arg3 + arg4);
//}

// THREE REGIONS OF QTA


  //current_pad = (TPad *) canv->cd(3);
  //mass_g_hist->Draw( "HIST E1 X0" );
  //style_hist( mass_g_hist, hist_style );
  //hist_prep_axes( mass_g_hist, true );
  //add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  //set_axis_labels( mass_g_hist, jpsi_mass.get_ltx(), "Yield" );
  //add_atlas_decorations( current_pad, true, false );
  //mass_g_hist->Fit( g2, "MLI", "", jpsi_mass.get_max()/3.0, jpsi_mass.get_max()*2.0/3.0 );
  //mass_g_hist->Draw( "SAME" );
  //TLegend * fit_g2_legend = below_logo_legend();
  //fit_g2_legend->AddEntry( mass_g_hist, "jpsi mass" ); 
  //fit_g2_legend->AddEntry( g2, "gauss" ); 
  //TPaveStats * g2_stats = make_stats( mass_g_hist ); 
  //g2_stats->Draw();

  //current_pad = (TPad *) canv->cd(4);
  //mass_g_hist->Draw( "HIST E1 X0" );
  //style_hist( mass_g_hist, hist_style );
  //hist_prep_axes( mass_g_hist, true );
  //add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  //set_axis_labels( mass_g_hist, jpsi_mass.get_ltx(), "Yield" );
  //add_atlas_decorations( current_pad, true, false );
  //mass_g_hist->Fit( g3, "MLI", "", jpsi_mass.get_max()*2.0/3.0, jpsi_mass.get_max() );
  //mass_g_hist->Draw( "SAME" );
  //TLegend * fit_g3_legend = below_logo_legend();
  //fit_g3_legend->AddEntry( mass_g_hist, "jpsi mass" ); 
  //fit_g3_legend->AddEntry( g3, "gauss" ); 
  //TPaveStats * g3_stats = make_stats( mass_g_hist ); 
  //g3_stats->Draw();



  //canv->SaveAs( "fits.png" ); 
  







  //TF1 * bw_func = new TF1( "bw", breit_wigner, jpsi_mass.get_min(), jpsi_mass.get_max(), 3 );
  //TF1 * bw_func = new TF1( "bw", "ROOT::Math::breitwigner_pdf", jpsi_mass.get_min(), jpsi_mass.get_max(), 3 );
  //TF1 * bw_func = new TF1( "bw", "TMath::BreitWigner(x, [0], [1] )", jpsi_mass.get_min(), jpsi_mass.get_max() );
  //TF1 * exp = new TF1( "exp_forbw", "exp^(-(x-[0])*[1])", jpsi_mass.get_min(), jpsi_mass.get_max() );

  //TF1 * combo = new TF1( "bw+exp_forbw", jpsi_mass.get_min(), jpsi_mass.get_max(), "" );
  

  //bw_func->SetParameter( 1, 3096 );
  //bw_func->SetParameter( 0, mass_bw_hist->GetMaximum() );
  //bw_func->SetParameter( 2, mass_bw_hist->GetStdDev() );
  //TF1 * ROOT::Math::breit




  //current_pad = (TPad *) canv->cd( 2 );
  //mass_bw_hist->Draw( "HIST E1 X0" );
  //style_hist( mass_bw_hist, hist_style );
  //hist_prep_axes( mass_bw_hist, true );
  //add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  //set_axis_labels( mass_bw_hist, jpsi_mass.get_ltx(), "Yield" );
  //add_atlas_decorations( current_pad, true, false );
  //mass_bw_hist->Fit( bw_func, "MLI", "", jpsi_mass.get_min(), jpsi_mass.get_max() );
  //mass_bw_hist->Draw( "SAME" );
  //TLegend * fit_bw_legend = below_logo_legend();
  //fit_bw_legend->AddEntry( mass_bw_hist, "jpsi mass" ); 
  //fit_bw_legend->AddEntry( combine_func, "bw" ); 
  //TPaveStats * bw_stats = make_stats( mass_bw_hist ); 

  //TF1 * alt_func = new TF1( "alt_func", "")

  //RooRealVar DiMuonTau( "DiMuonTau", "DiMuonTau", 0.3, 15.0 );
  //RooDataSet * lifetime = new RooDataSet( "tau_Q0", "tau_Q0", tree_base, DiMuonTau);
  //RooRealVar tau0( "tau0", "tau0", 0.2, 1.0 );
  //tau0.setRange( 0, 1.0 );

  //RooRealVar bias("bias", "bias", 0, 15);
  //RooRealVar sigma("sigma", "sigma", 0, 3);
  //RooRealVar coeff("coeff", "coeff", -1, 1);
  //

  //RooGaussian gauss("gm", "gauss", DiMuonTau, bias, sigma);
  //RooAbsPdf * genexp = RooClassFactory::makePdfInstance( "hlexp", 
  //                                                      "exp(((-0.69314718056)*DiMuonTau)/tau0)", 
  //                                                      RooArgSet(DiMuonTau, tau0) );
  //RooAbsReal *errorFunc = RooFit::bindFunction("erf", ROOT::Math::erf, tau0);
  
  //RooAddPdf model("model", "model", gauss, *genexp, coeff);
  //model.fitTo(*lifetime);
  //genexp->fitTo( * lifetime );



  //TF1 * g2 = (TF1 *) combine_func->Clone( "g2" ); TF1 * g3 = (TF1 *) combine_func->Clone( "g3" );

  
  //TCanvas * canv = new TCanvas( "canv", "", 200, 200, 2000, 2000 );
  //canv->Divide( 2, 2 );

  //TPad * current_pad = (TPad *) canv->cd(1);
  //mass_g_hist->Draw( "HIST E1 X0" );
  //style_hist( mass_g_hist, hist_style );
  //hist_prep_axes( mass_g_hist, true );
  //add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  //set_axis_labels( mass_g_hist, jpsi_mass.get_ltx(), "Yield" );
  //add_atlas_decorations( current_pad, true, false );
  //mass_g_hist->Fit( combine_func, "ML", "", jpsi_mass.get_min(), jpsi_mass.get_max() );
  //mass_g_hist->Draw( "SAME" );
  //TLegend * fit_legend = below_logo_legend();
  //fit_legend->AddEntry( mass_g_hist, "jpsi mass" ); 
  //fit_legend->AddEntry( combine_func, "gauss" ); 
  //TPaveStats * g_stats = make_stats( mass_g_hist ); 
  //g_stats->Draw();

  ////canv->SaveAs( "fits1.png" ); 
  ////return;

  //current_pad = (TPad *) canv->cd(2);
  //mass_g_hist->Draw( "HIST E1 X0" );
  //style_hist( mass_g_hist, hist_style );
  //hist_prep_axes( mass_g_hist, true );
  //add_pad_title( current_pad, "J/#psi Mass, exponential and ", true);
  //set_axis_labels( mass_g_hist, jpsi_mass.get_ltx(), "Yield" );
  //add_atlas_decorations( current_pad, true, false );
  //mass_g_hist->Fit( g1, "MLI", "", jpsi_mass.get_min(), jpsi_mass.get_max()/3.0 );
  //mass_g_hist->Draw( "SAME" );
  //TLegend * fit_g1_legend = below_logo_legend();
  //fit_g1_legend->AddEntry( mass_g_hist, "jpsi mass" ); 
  //fit_g1_legend->AddEntry( g1, "gauss" ); 
  //TPaveStats * g1_stats = make_stats( mass_g_hist ); 
  //g1_stats->Draw();

