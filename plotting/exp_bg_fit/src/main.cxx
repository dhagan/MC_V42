#include <main.hxx>
#include <exp.hxx>

int help(){
  std::cout << "h     --    help      --  output help" << std::endl;
  std::cout << "i     --    input     --  path to input file" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"input",       required_argument,        0,      'i'},
      {"unique",      required_argument,        0,      'u'},
      {"selections",  required_argument,        0,      'v'},
      {"ranges",      required_argument,        0,      'r'},
      {0,             0,                        0,      0}
    };

  std::string input_filepath, ranges, selections_filepath, unique;

  do {
    option = getopt_long( argc, argv, "i:v:u:r:h", long_options, &option_index );
    switch (option){
      case 'h':
        return help();
      case 'i': 
        input_filepath      = std::string( optarg );
        break;
      case 'v': 
        selections_filepath = std::string( optarg );
        break;
      case 'u': 
        unique              = std::string( optarg );
        break;
      case 'r': 
        ranges              = std::string( optarg );
        break;
    }
  } while (option != -1);

  if ( input_filepath.empty() ){ 
    std::cout << "WARNING: No filepath provided, exiting" << std::endl;
    return 0; 
  } 
  
  if ( selections_filepath.empty() ){ 
    std::cout << "WARNING: No selections provided, exiting" << std::endl;
    return 0; 
  } 

  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( selections_filepath ); 
  selections->process_bounds_string( ranges );

  exp( input_filepath, selections, unique );

  return 0;

}

