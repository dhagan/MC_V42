executable=${ANA_IP}/plotting/exp_bg_fit/build/exp_bg_fit
unique=base
input="${OUT_PATH}/trees/gen_base/data/data_base.root:tree"


mkdir -p ${OUT_PATH}/exp_bg_fit/${unique}

pushd ${OUT_PATH}/exp_bg_fit/${unique} >> /dev/null

${executable} -i "${input}" 

popd >> /dev/null
