executable=${ANA_IP}/plotting/exp_bg_fit/build/exp_bg_fit
selections=${LIB_PATH}/share/hf_bounds.txt

ranges_1="qtB:abs(qtB)&1:0:2"
ranges_2="qtB:abs(qtB)&1:2:4"
ranges_3="qtB:abs(qtB)&1:4:6"
ranges_4="qtB:abs(qtB)&1:6:8"
ranges_5="qtB:abs(qtB)&1:8:10"
ranges_6="qtB:abs(qtB)&1:10:12"
ranges_7="qtB:abs(qtB)&1:12:14"
ranges_8="qtB:abs(qtB)&1:14:16"
ranges_list=( "${ranges_1}" "${ranges_2}" "${ranges_3}" "${ranges_4}" "${ranges_5}" "${ranges_6}" "${ranges_7}" "${ranges_8}" )

for qtb in ${!ranges_list[@]}; do

	number=$(($qtb + 1))
	input_unique="qtbsplit-${number}"
	output_unique="qtbsplit-${number}"
	range="${ranges_list[$qtb]}"
	input="${OUT_PATH}/trees/gen_${input_unique}/data/data_${input_unique}.root"

	mkdir -p ${OUT_PATH}/exp_bg_fit/${output_unique}

	pushd ${OUT_PATH}/exp_bg_fit/${output_unique} >> /dev/null

	##echo ${executable}
	##echo ${input}
	##echo ${selections}
	##echo ${output_unique}
	##echo ${range}

	${executable} -i "${input}" -v ${selections} -u ${output_unique} -r ${range}

	popd >> /dev/null
done


