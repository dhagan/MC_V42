executable=${ANA_IP}/plotting/exp_bg_fit/build/exp_bg_fit
selections=${LIB_PATH}/share/hf_bounds.txt

ranges_1="qtB:abs(qtB)&1:0:6"
ranges_2="qtB:abs(qtB)&1:6:16"
ranges_list=( "${ranges_1}" "${ranges_2}" ) 
unique=( "lower" "upper" )

for index in {0..1}; do

	range="${ranges_list[$index]}"
	output_unique="qtb_${unique[$index]}"
	input_unique="qtb_${unique[$index]}"
	log=${LOG_PATH}/extract/${output_unique}.txt
	input="${OUT_PATH}/trees/gen_${input_unique}/data/data_${input_unique}.root"

	mkdir -p ${OUT_PATH}/exp_bg_fit/${output_unique}

	pushd ${OUT_PATH}/exp_bg_fit/${output_unique} >> /dev/null

	${executable} -i "${input}" -v ${selections} -u ${output_unique} -r ${range}

	popd >> /dev/null
done


