#include <aw.hxx>

#include <sys/stat.h>
#include <unistd.h>
#include <fstream>

#include "TF1.h"


Float_t phi_calc( const ROOT::RVecF &MuMuGamma_CS_Phi ){
	return ( MuMuGamma_CS_Phi[0] >= 0 ) ? ( MuMuGamma_CS_Phi[0] - M_PI ) : ( MuMuGamma_CS_Phi[0] + M_PI );
}

Float_t dphi_calc( const ROOT::RVecF &DiLept_Phi, const ROOT::RVecF &Photon_Phi ){
	Float_t dPhi = (DiLept_Phi[0] - Photon_Phi[0]); 
	while( dPhi > M_PI ){ dPhi -= 2*M_PI; };
	while( dPhi < -M_PI ){ dPhi += 2*M_PI; };
	return dPhi;
}

inline bool file_exists(const std::string& name) {
  struct stat buffer;   
  return ( stat( name.c_str(), &buffer ) == 0 ); 
}


void make_trees( const std::string & filepath, bool gen = false ){

  ROOT::RDF::RSnapshotOptions snapshot_options;
  snapshot_options.fMode = "UPDATE";
	auto phi_lambda = phi_calc;
  auto dphi_lambda = dphi_calc;

  TChain input_chain;
  input_chain.Add( filepath.c_str());
  ROOT::RDataFrame input_frame( input_chain );

  std::vector< std::string > snapshot_columns = { "minv", "photon_eta", "muneg_eta", "mupos_eta", 
                                                  "photon_pt", "mupos_pt",  "muneg_pt",  "Phi", 
                                                  "qTSquared", "qx", "qy", "qtA", "qtB" };

  auto frame = input_frame.Filter( ( gen ) ? "mcDecPhoton_Pt >= 0" : "Photon_Pt.size() != 0" );

  if ( gen ){

    frame = frame.Define( "minv", "Float_t( mcDecMuMuY_M/1000.0 )" );
    frame = frame.Define( "photon_eta", "Float_t( mcDecPhoton_Eta )" );
    frame = frame.Define( "muneg_eta",  "Float_t( mcDecMuMi_Eta )" );
    frame = frame.Define( "mupos_eta",  "Float_t( mcDecMuPl_Eta )" );
    frame = frame.Define( "photon_pt",	"Float_t( mcDecPhoton_Pt/1000.0 )" );
    frame = frame.Define( "mupos_pt",   "Float_t( mcDecMuPl_Pt/1000.0 )" );
    frame = frame.Define( "muneg_pt",   "Float_t( mcDecMuMi_Pt/1000.0 )" );
    frame = frame.Define( "DPhi", dphi_lambda, { "mcDecDiLept_Phi", "mcDecPhoton_Phi" } );
	  frame = frame.Define( "Phi", phi_lambda, {"truth_angleDec_cs_phi_mumugamma"} );
	  frame = frame.Define( "qTSquared", "Float_t( (mcDecMuMuY_Pt/1000.0) * (mcDecMuMuY_Pt/1000.0) )" );
    frame = frame.Define( "jpsipt",	 "Float_t( mcDecDiLept_Pt/1000.0 )" );
	  frame = frame.Define( "Lambda", "Float_t( (mcDecMuMuY_M/3097.0)*(mcDecMuMuY_M/3097.0) )" );

  } else {

    frame = frame.Define( "minv", "Float_t( MuMuY_M[0]/1000.0 )" );
    frame = frame.Define( "mumu_mass",  "Float_t( DiLept_M[0]/1000.0 )" );
    frame = frame.Define( "photon_eta",	"Float_t( Photon_Eta[0] )" );
    frame = frame.Define( "mupos_eta",  "Float_t( MuPlus_Eta[0] )" );
    frame = frame.Define( "muneg_eta",  "Float_t( MuMinus_Eta[0] )" );
    frame = frame.Define( "photon_pt",	"Float_t( Photon_Pt[0]/1000.0 )" );
    frame = frame.Define( "mupos_pt",  "Float_t( MuPlus_Pt[0]/1000.0 )" );
    frame = frame.Define( "muneg_pt",  "Float_t( MuMinus_Pt[0]/1000.0 )" );
	  frame = frame.Define( "DPhi", dphi_lambda, { "DiLept_Phi", "Photon_Phi" } );
	  frame = frame.Define( "Phi", phi_lambda, {"MuMuGamma_CS_Phi"} );
	  frame = frame.Define( "qTSquared", "Float_t( (MuMuY_Pt[0]/1000.0) * (MuMuY_Pt[0]/1000.0) )" );
    frame = frame.Define( "jpsipt",	 "Float_t( DiLept_Pt[0]/1000.0 )" );


  }

  frame = frame.Define( "qx",   "Float_t( sqrt( qTSquared )*cos( Phi ) )" );
  frame = frame.Define( "qy",   "Float_t( sqrt( qTSquared )*sin( Phi ) )" );
  frame = frame.Define( "qtA",  "Float_t( jpsipt - photon_pt)" );
	frame = frame.Define( "qtB",  "Float_t( sqrt( (jpsipt)*(photon_pt) )*sin(DPhi) )" );



  frame = frame.Filter( "abs( muneg_eta ) < 2.4" );
  frame = frame.Filter( "abs( mupos_eta ) < 2.4" );
  frame = frame.Filter( "abs( photon_eta ) < 2.4" );

  std::string treefile = (gen) ? "./gen.root" : "./acc.root";

  if ( gen ){

    auto frame000 = frame.Filter( "photon_pt > 0" );
    frame000 = frame.Filter( "mupos_pt > 0" );
    frame000 = frame.Filter( "muneg_pt > 0" );
    frame000.Snapshot( "000", treefile.c_str(), "", snapshot_options );

    auto frame111 = frame.Filter( "photon_pt > 1" );
    frame111 = frame.Filter( "mupos_pt > 1" );
    frame111 = frame.Filter( "muneg_pt > 1" );
    frame111.Snapshot( "111", treefile.c_str(), "", snapshot_options );

    auto frame112 = frame.Filter( "photon_pt > 2" );
    frame112 = frame.Filter( "mupos_pt > 1" );
    frame112 = frame.Filter( "muneg_pt > 1" );
    frame112.Snapshot( "112", treefile.c_str(), "", snapshot_options );

    auto frame224 = frame.Filter( "photon_pt > 3" );
    frame224 = frame.Filter( "mupos_pt > 2" );
    frame224 = frame.Filter( "muneg_pt > 2" );
    frame224.Snapshot( "224", treefile.c_str(), "", snapshot_options );

    auto frame334 = frame.Filter( "photon_pt > 4" );
    frame334 = frame.Filter( "mupos_pt > 3" );
    frame334 = frame.Filter( "muneg_pt > 3" );
    frame334.Snapshot( "334", treefile.c_str(), "", snapshot_options );
  
  }


  auto frame445 = frame.Filter( "photon_pt > 5" );
  frame445 = frame.Filter( "mupos_pt > 4" );
  frame445 = frame.Filter( "muneg_pt > 4" );
  frame445.Snapshot( "445", treefile.c_str(), snapshot_columns, snapshot_options );
  

}


TFile * get_progressive_trees_file( const std::string & filepath, bool gen = false ){
  
  std::string infile = (gen) ? "./gen.root" : "./acc.root";
  if ( !file_exists( infile ) && !filepath.empty() ){ make_trees( filepath, gen ); }
  if ( !filepath.empty() ){ 
    return new TFile( infile.c_str(), "READ" ); 
  } else {
    return nullptr;
  }
}

void acceptance_plots( TTree * cut_tree, bool gen = false ){

  std::string cut_level = cut_tree->GetName(); 

  TH1F qx = TH1F( "qx", "qx", 100, -20, 20 );
  TH1F qta = TH1F( "qta", "qta", 100, -20, 20 );
  TH1F qtb = TH1F( "qtb", "qtb", 100, -20, 20 );

  std::string filetype = ( gen ) ? "./gen_" : "./acc_";
  std::string qx_outfile, qta_outfile, qtb_outfile;
  qx_outfile = "qx_" +  std::string( cut_tree->GetName() ) + ".png";
  qta_outfile = "qta_" +  std::string( cut_tree->GetName() ) + ".png";
  qtb_outfile = "qtb_" +  std::string( cut_tree->GetName() ) + ".png";

  cut_tree->Draw( "qx>>qx", "", "goff" );
  cut_tree->Draw( "qtA>>qta", "", "goff" );
  cut_tree->Draw( "qtB>>qtb", "", "goff" );


  TCanvas canvas( "", "", 100, 100, 1000, 1000 );
  canvas.Divide( 1, 1 );

  canvas.cd( 1 );
  qx.SetName( cut_tree->GetName() );
  qx.Draw( "HIST" );
  plotting::axes_titles( &qx, "q_{x} (GeV)", "Entries" );
  //TPaveStats * qx_stats = plotting::stats( top_right, &qx );
  //qx_stats->Draw();
  canvas.SaveAs( qx_outfile.c_str() );

  canvas.Clear();
  qta.SetName( cut_tree->GetName() );
  qta.Draw( "HIST" );
  plotting::axes_titles( &qta, "q_{T}^{A} (GeV)", "Entries" );
  //TPaveStats * qta_stats = plotting::stats( top_right, &qta);
  //qta_stats->Draw();
  canvas.SaveAs( qta_outfile.c_str() );

  qtb.SetName( cut_tree->GetName() );
  qtb.Draw( "HIST" );
  plotting::axes_titles( &qtb, "q_{T}^{B} (GeV)", "Entries" );
  //TPaveStats * qtb_stats = plotting::stats( top_right, &qtb);
  //qtb_stats->Draw();
  canvas.SaveAs( qtb_outfile.c_str() );

}


void acc( const std::string & filepath, bool gen ){

  gStyle->SetOptStat( "ormen" );

  TFile * input_file = get_progressive_trees_file( filepath, gen );

  if ( gen ){

    TTree * tree000 = static_cast< TTree * >( input_file->Get( "000" ) );
    TTree * tree111 = static_cast< TTree * >( input_file->Get( "111" ) );
    TTree * tree112 = static_cast< TTree * >( input_file->Get( "112" ) );
    TTree * tree224 = static_cast< TTree * >( input_file->Get( "224" ) );
    TTree * tree334 = static_cast< TTree * >( input_file->Get( "334" ) );
    acceptance_plots( tree000, gen );
    acceptance_plots( tree111, gen );
    acceptance_plots( tree112, gen );
    acceptance_plots( tree224, gen );
    acceptance_plots( tree334, gen );

  }


  TTree * tree445 = static_cast< TTree * >( input_file->Get( "445" ) );
  acceptance_plots( tree445, gen );

}

void preprod( const std::string & gen_path, const std::string & acc_path ){

  TFile * gen_file = new TFile( gen_path.c_str(), "READ" );
  TFile * acc_file = new TFile( acc_path.c_str(), "READ" );
  TTree * gen_tree = static_cast< TTree *>( gen_file->Get( "tree" ) );
  TTree * acc_tree = static_cast< TTree *>( acc_file->Get( "tree" ) );

  std::cout << gen_tree->GetName() << std::endl;
  std::cout << acc_tree->GetName() << std::endl;

  TH1F * qta000 = new TH1F( "qta000", "", 100, -15, 15 );  
  TH1F * qta111 = new TH1F( "qta111", "", 100, -15, 15 );
  TH1F * qta112 = new TH1F( "qta112", "", 100, -15, 15 );
  TH1F * qta224 = new TH1F( "qta224", "", 100, -15, 15 );
  TH1F * qta334 = new TH1F( "qta334", "", 100, -15, 15 );
  TH1F * qta445 = new TH1F( "qta445", "", 100, -15, 15 );
  TH1F * qta449 = new TH1F( "qta449", "", 100, -15, 15 );

  TH1F * qtb000 = new TH1F( "qtb000", "", 100, -15, 15 );  
  TH1F * qtb111 = new TH1F( "qtb111", "", 100, -15, 15 );
  TH1F * qtb112 = new TH1F( "qtb112", "", 100, -15, 15 );
  TH1F * qtb224 = new TH1F( "qtb224", "", 100, -15, 15 );
  TH1F * qtb334 = new TH1F( "qtb334", "", 100, -15, 15 );
  TH1F * qtb445 = new TH1F( "qtb445", "", 100, -15, 15 );



  TH1F * qx000 = new TH1F( "qx000", "", 40, -15, 15 );  
  TH1F * qx111 = new TH1F( "qx111", "", 40, -15, 15 );
  TH1F * qx112 = new TH1F( "qx112", "", 40, -15, 15 );
  TH1F * qx224 = new TH1F( "qx224", "", 40, -15, 15 );
  TH1F * qx334 = new TH1F( "qx334", "", 40, -15, 15 );
  TH1F * qx445 = new TH1F( "qx445", "", 40, -15, 15 );
  TH1F * qx449 = new TH1F( "qx449", "", 40, -15, 15 );


  TH1F * qta000M = new TH1F( "qta000M", "", 40, -15, 15 );  
  TH1F * qta111M = new TH1F( "qta111M", "", 40, -15, 15 );
  TH1F * qta112M = new TH1F( "qta112M", "", 40, -15, 15 );
  TH1F * qta224M = new TH1F( "qta224M", "", 40, -15, 15 );
  TH1F * qta334M = new TH1F( "qta334M", "", 40, -15, 15 );
  TH1F * qta445M = new TH1F( "qta445M", "", 40, -15, 15 );



  std::string cut000 = "MuPos_Pt > 0.0 && MuNeg_Pt > 0.0 && PhotonPt > 0.0 && abs( qtA ) < 10.0"; 
  std::string cut111 = "MuPos_Pt > 1.0 && MuNeg_Pt > 1.0 && PhotonPt > 1.0 && abs( qtA ) < 10.0";
  std::string cut112 = "MuPos_Pt > 1.0 && MuNeg_Pt > 1.0 && PhotonPt > 2.0 && abs( qtA ) < 10.0";
  std::string cut224 = "MuPos_Pt > 2.0 && MuNeg_Pt > 2.0 && PhotonPt > 4.0 && abs( qtA ) < 10.0";
  std::string cut334 = "MuPos_Pt > 3.0 && MuNeg_Pt > 3.0 && PhotonPt > 4.0 && abs( qtA ) < 10.0";
  std::string cut445 = "MuPos_Pt > 4.0 && MuNeg_Pt > 4.0 && PhotonPt > 5.0 && abs( qtA ) < 10.0";
  std::string cut449 = "MuPos_Pt > 4.0 && MuNeg_Pt > 4.0 && PhotonPt > 9.0 && abs( qtA ) < 10.0";

  std::string cut000q3 = "MuPos_Pt > 0.0 && MuNeg_Pt > 0.0 && PhotonPt > 0.0 && Lambda > 25 && Lambda < 50"; 
  std::string cut445q3 = "MuPos_Pt > 4.0 && MuNeg_Pt > 4.0 && PhotonPt > 4.0 && Lambda > 25 && Lambda < 50";

  gen_tree->Draw( "qtA>>qta000M", cut000.c_str(), "goff" );
  gen_tree->Draw( "qtA>>qta111M", cut111.c_str(), "goff" ); 
  gen_tree->Draw( "qtA>>qta112M", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qtA>>qta224M", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qtA>>qta334M", cut334.c_str(), "goff" );
  acc_tree->Draw( "qtA>>qta445M", cut445.c_str(), "goff" ); 
  acc_tree->Draw( "qtA>>qta449", cut449.c_str(), "goff" ); 

  gen_tree->Draw( "qtB>>qtb000", cut000.c_str(), "goff" );
  gen_tree->Draw( "qtB>>qtb111", cut111.c_str(), "goff" ); 
  gen_tree->Draw( "qtB>>qtb112", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qtB>>qtb224", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qtB>>qtb334", cut334.c_str(), "goff" );
  acc_tree->Draw( "qtB>>qtb445", cut445.c_str(), "goff" ); 

  double mean000 = qta000M->GetMean();
  double mean111 = qta111M->GetMean();
  double mean112 = qta112M->GetMean();
  double mean224 = qta224M->GetMean();
  double mean334 = qta334M->GetMean();
  double mean445 = qta445M->GetMean();

  gen_tree->Draw( Form( "( qtA - %.5f )>>qta000", mean000 ), cut000.c_str(), "goff" );
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta111", mean111 ), cut111.c_str(), "goff" ); 
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta112", mean112 ), cut112.c_str(), "goff" ); 
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta224", mean224 ), cut112.c_str(), "goff" ); 
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta334", mean334 ), cut334.c_str(), "goff" );
  acc_tree->Draw( Form( "( qtA - %.5f )>>qta445", mean445 ), cut445.c_str(), "goff" ); 
 
  gen_tree->Draw( "qx>>qx000", cut000.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx111", cut111.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx112", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx224", cut224.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx334", cut334.c_str(), "goff" );
  gen_tree->Draw( "qx>>qx445", cut445.c_str(), "goff" ); 
  acc_tree->Draw( "qx>>qx449", cut449.c_str(), "goff" ); 
  

  qta000->Scale( 1.0/qta000->Integral() ); 
  qta111->Scale( 1.0/qta111->Integral() );  
  qta112->Scale( 1.0/qta112->Integral() );  
  qta224->Scale( 1.0/qta224->Integral() );  
  qta334->Scale( 1.0/qta334->Integral() );  
  qta445->Scale( 1.0/qta445->Integral() );  

  qtb000->Scale( 1.0/qtb000->Integral() ); 
  qtb111->Scale( 1.0/qtb111->Integral() );  
  qtb112->Scale( 1.0/qtb112->Integral() );  
  qtb224->Scale( 1.0/qtb224->Integral() );  
  qtb334->Scale( 1.0/qtb334->Integral() );  
  qtb445->Scale( 1.0/qtb445->Integral() );  


  qx000->Scale( 1.0/qx000->Integral() ); 
  qx111->Scale( 1.0/qx111->Integral() );  
  qx112->Scale( 1.0/qx112->Integral() );  
  qx224->Scale( 1.0/qx224->Integral() );  
  qx334->Scale( 1.0/qx334->Integral() );  
  qx445->Scale( 1.0/qx445->Integral() );  



  TCanvas canv( "canv", "", 200, 200, 6000, 1000 );
  canv.Divide( 6, 1 );

  canv.cd(1);
  qta000->Draw( "HIST" );
  canv.cd(2);
  qta111->Draw( "HIST" );
  canv.cd(3);
  qta112->Draw( "HIST" );
  canv.cd(4);
  qta334->Draw( "HIST" );
  canv.cd(5);
  qta445->Draw( "HIST" );
  canv.cd(6);
  qta449->Draw( "HIST" );
  canv.SaveAs( "qta.png" );

  TCanvas canv2( "canv", "", 200, 200, 6000, 1000 );
  canv2.Divide( 6, 1 );

  canv2.cd(1);
  qx000->Draw( "HIST" );
  canv2.cd(2);
  qx111->Draw( "HIST" );
  canv2.cd(3);
  qx112->Draw( "HIST" );
  canv2.cd(4);
  qx334->Draw( "HIST" );
  canv2.cd(5);
  qx445->Draw( "HIST" );
  canv2.cd(6);
  qx449->Draw( "HIST" );
  canv2.SaveAs( "qx.png" );

  //Int_t night = TColor::GetColor( "01110a" );
  //Int_t orng = TColor::GetColor( "ba2d0b" );
  //Int_t honey = TColor::GetColor( "d5d2e3" );
  //Int_t camb = TColor::GetColor( "73ba9b" );
  //Int_t brg = TColor::GetColor( "003e1f" );

  Int_t xanthous = TColor::GetColor( 247, 181, 56 );
  Int_t ochre = TColor::GetColor( 219, 124, 38 );
  //Int_t flame = TColor::GetColor( 216, 87, 42 );
  Int_t fer = TColor::GetColor( 195, 47, 39 );
  Int_t burg = TColor::GetColor( 120, 1, 22 );

  //Int_t khaki = TColor::GetColor( 190, 183, 164 );

  Int_t night = TColor::GetColor( 1, 17, 10 );
  Int_t orng = TColor::GetColor( 186, 45, 11 );
  //Int_t honey = TColor::GetColor( 213, 242, 227 );
  //Int_t camb = TColor::GetColor( 115, 186, 155 );
  Int_t brg = TColor::GetColor( 0, 62, 31 );


  qta000->SetMarkerColor( xanthous );
  qta112->SetMarkerColor( ochre );
  qta224->SetMarkerColor( brg );
  qta334->SetMarkerColor( fer );
  qta445->SetMarkerColor( burg );

  qtb000->SetMarkerColor( xanthous );
  qtb112->SetMarkerColor( ochre );
  qtb224->SetMarkerColor( brg );
  qtb334->SetMarkerColor( fer );
  qtb445->SetMarkerColor( burg );

  qx000->SetMarkerColor( night );
  qx445->SetMarkerColor( orng );

  // qta overlay
  TCanvas canv3( "canv", "", 200, 200, 1000, 1000 );
  canv3.Divide( 1, 1 );
  qta000->Draw( "E1" );
  //qta111->Draw( "E1 SAME" );
  qta112->Draw( "E1 SAME" );
  qta224->Draw( "E1 SAME" );
  qta334->Draw( "E1 SAME" );
  qta445->Draw( "E1 SAME" );
  plotting::title( "q_{T}^{A}, progressive acceptance cuts", over_top_left, true );
  plotting::axes_titles( qta000, "q_{T}^{A} [GeV]", "Normalised Entries" );
  plotting::atlas( top_left, true, true );
  qta000->GetYaxis()->SetRangeUser( 0, qta000->GetMaximum()*1.5 );
  TLegend * qta_legend = plotting::legend( top_right, 3 );
  qta_legend->AddEntry( qta000, "p_{T,#mu}>0 p_{T,#gamma}>0" );
  qta_legend->AddEntry( qta112, "p_{T,#mu}>1 p_{T,#gamma}>2" );
  qta_legend->AddEntry( qta224, "p_{T,#mu}>2 p_{T,#gamma}>4" );
  qta_legend->AddEntry( qta334, "p_{T,#mu}>3 p_{T,#gamma}>4" );
  qta_legend->AddEntry( qta445, "p_{T,#mu}>4 p_{T,#gamma}>5" );
  qta_legend->SetX1NDC( qta_legend->GetX1NDC() - 0.1 );
  qta_legend->Draw();
  canv3.SaveAs( "AOverlay.png" );


  TCanvas canv4( "canv", "", 200, 200, 1000, 1000 );
  canv4.Divide( 1, 1 );
  qtb000->Draw( "E1" );
  ///qtb111->Draw( "E1 SAME" );
  qtb112->Draw( "E1 SAME" );
  qtb224->Draw( "E1 SAME" );
  qtb334->Draw( "E1 SAME" );
  qtb445->Draw( "E1 SAME" );
  plotting::title( "q_{T}^{B}, progressive acceptance cuts", over_top_left, true );
  plotting::axes_titles( qtb000, "q_{T}^{B} [GeV]", "Normalised Entries" );
  plotting::atlas( top_left, true, true );
  qtb000->GetYaxis()->SetRangeUser( 0, qtb000->GetMaximum()*1.5 );
  TLegend * qtb_legend = plotting::legend( top_right, 3 );
  qtb_legend->AddEntry( qtb000, "p_{T,#mu}>0 p_{T,#gamma}>0" );
  qtb_legend->AddEntry( qtb112, "p_{T,#mu}>1 p_{T,#gamma}>2" );
  qtb_legend->AddEntry( qtb224, "p_{T,#mu}>2 p_{T,#gamma}>4" );
  qtb_legend->AddEntry( qtb334, "p_{T,#mu}>3 p_{T,#gamma}>4" );
  qtb_legend->AddEntry( qtb445, "p_{T,#mu}>4 p_{T,#gamma}>5" );
  qtb_legend->SetX1NDC( qtb_legend->GetX1NDC() - 0.1 );
  qtb_legend->Draw();
  canv4.SaveAs( "BOverlay.png" );

  gen_tree->Draw( "qx>>qx000", cut000q3.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx445", cut445q3.c_str(), "goff" ); 
  qx000->Scale( 1.0/qx000->Integral() ); 
  qx445->Scale( 1.0/qx445->Integral() );  


  TCanvas canv5( "canv", "", 200, 200, 1000, 1000 );
  canv5.Divide( 1, 1 );
  canv5.cd( 1 );
  qx000->Draw( "E1" );
  qx445->Draw( "E1 SAME" );
  plotting::title( "q_{x}, no cuts", over_top_left, false);
  plotting::axes_titles( qx000, "q_{x} [GeV]", "Normalised Entries" );
  plotting::atlas( top_left, true, true );
  qx000->GetYaxis()->SetRangeUser( 0, qx000->GetMaximum()*1.5 );
  canv5.SaveAs( "qxcut.png" );


  gen_tree->Draw( "qtA>>qta000", cut000q3.c_str(), "goff" ); 
  gen_tree->Draw( "qtA>>qta445", cut445q3.c_str(), "goff" ); 
  qta000->Scale( 1.0/qta000->Integral() ); 
  qta445->Scale( 1.0/qta445->Integral() );  
  TCanvas canv6( "canv", "", 200, 200, 1000, 1000 );
  canv6.Divide( 1, 1 );
  canv6.cd( 1 );
  qta000->Draw( "E1" );
  qta445->Draw( "E1 SAME" );
  plotting::title( "q_{T}^{A}, no cuts", over_top_left, false);
  plotting::axes_titles( qta000, "q_{T}^{A} [GeV]", "Normalised Entries" );
  plotting::atlas( top_left, true, true );
  qta000->GetYaxis()->SetRangeUser( 0, qta000->GetMaximum()*1.5 );
  canv6.SaveAs( "qtacut.png" );

  gen_tree->Draw( "qtB>>qtb000", cut000q3.c_str(), "goff" ); 
  gen_tree->Draw( "qtB>>qtb445", cut445q3.c_str(), "goff" ); 
  qtb000->Scale( 1.0/qtb000->Integral() ); 
  qtb445->Scale( 1.0/qtb445->Integral() );  
  TCanvas canv7( "canv", "", 200, 200, 1000, 1000 );
  canv7.Divide( 1, 1 );
  canv7.cd( 1 );
  qtb000->Draw( "E1" );
  qtb445->Draw( "E1 SAME" );
  plotting::title( "q_{T}^{B}, no cuts", over_top_left, false);
  plotting::axes_titles( qtb000, "q_{T}^{B} [GeV]", "Normalised Entries" );
  plotting::atlas( top_left, true, true );
  qtb000->GetYaxis()->SetRangeUser( 0, qtb000->GetMaximum()*1.5 );
  canv7.SaveAs( "qtbcut.png" );


}

void scatter( const std::string & sign_path, const std::string & bckg_path ){


  TFile * sign_file = new TFile( sign_path.c_str(), "READ" );
  TFile * bckg_file = new TFile( bckg_path.c_str(), "READ" );
  TTree * sign_tree = static_cast< TTree *>( sign_file->Get( "tree" ) );
  TTree * bckg_tree = static_cast< TTree *>( bckg_file->Get( "tree" ) );

  TH2F * sign_scatter = new TH2F( "sign", "", 30, -10, 20, 30, -20, 20 );
  TH2F * bckg_scatter = new TH2F( "bckg", "", 30, -10, 20, 30, -20, 20 );

  sign_tree->Draw( "qtB:qtA>>sign", "", "goff" );
  bckg_tree->Draw( "qtB:qtA>>bckg", "", "goff" );


  TCanvas canv( "canv", "", 200, 200, 2000, 1000 );
  canv.Divide( 2, 1 );

  //box_edges top_right = plotting::positions( position::top_right );

  canv.cd( 1 );
  sign_scatter->Draw( "COLZ" );
  plotting::title( "Signal" );
  plotting::axes_titles( sign_scatter, "q_{T}^{A} [GeV]", "q_{T}^{B} [GeV]");
  plotting::atlas( position::top_left, true, true );


  canv.cd( 2 );
  bckg_scatter->Draw( "COLZ" );
  plotting::title( "Background" );
  plotting::axes_titles( bckg_scatter, "q_{T}^{A} [GeV]", "q_{T}^{B} [GeV]" );
  plotting::atlas( position::top_left, true, true );

  canv.SaveAs( "scatter_qta_qtb.png" );

}

void checkacc( const std::string & gen, const std::string & acc ){

  Int_t night = TColor::GetColor( 1, 17, 10 );
  Int_t orng = TColor::GetColor( 186, 45, 11 );

  TFile * gen_file = new TFile( gen.c_str(), "READ" );
  TFile * acc_file = new TFile( acc.c_str(), "READ" );
  TTree * gen_tree = static_cast< TTree *>( gen_file->Get( "tree" ) );
  TTree * acc_tree = static_cast< TTree *>( acc_file->Get( "tree" ) );

  std::string cut445 = "MuPos_Pt > 4.0 && MuNeg_Pt > 4.0 && PhotonPt > 5.0";
  std::string cut445q3 = "MuPos_Pt > 4.0 && MuNeg_Pt > 4.0 && PhotonPt > 5.0 && Lambda > 25 && Lambda < 50";

  TH1F * qta_gen = new TH1F( "qta_gen", "", 50, -15, 15 );
  TH1F * qta_acc = new TH1F( "qta_acc", "", 50, -15, 15 );

  gen_tree->Draw( "qtA>>qta_gen", cut445.c_str(), "goff" );
  acc_tree->Draw( "qtA>>qta_acc", cut445.c_str(), "goff" );

  qta_gen->SetMarkerColor( night );
  qta_acc->SetMarkerColor( orng );

  qta_gen->Scale( 1.0/qta_gen->Integral() );
  qta_acc->Scale( 1.0/qta_acc->Integral() );

  
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  canv.cd( 1 );
  qta_gen->Draw( "E1" );
  qta_acc->Draw( "E1 SAME" );
  plotting::title( "q_{T}^{A}, progressive acceptance cuts", over_top_left, true );
  plotting::axes_titles( qta_gen, "q_{T}^{A} [GeV]", "Normalised Entries" );
  plotting::atlas( top_left, true, true );
  TLegend * qta_legend = plotting::legend( top_right, 1 );
  qta_legend->AddEntry( qta_gen, qta_gen->GetName() );
  qta_legend->AddEntry( qta_acc, qta_acc->GetName() );
  qta_legend->Draw();

  canv.SaveAs( "genvacc.png" );

  TH1F * qx_gen = new TH1F( "qx_gen", "", 30, -15, 15 );
  TH1F * qx_acc = new TH1F( "qx_acc", "", 30, -15, 15 );

  gen_tree->Draw( "qx>>qx_gen", "Lambda > 25 && Lambda < 50", "goff" );
  acc_tree->Draw( "qx>>qx_acc", cut445q3.c_str(), "goff" );


  qx_gen->Scale( 1.0/qx_gen->Integral() );
  qx_acc->Scale( 1.0/qx_acc->Integral() );

  //Int_t fer = TColor::GetColor( 195, 47, 39 );
  Int_t xanthous = TColor::GetColor( 247, 181, 56 );
  Int_t burg = TColor::GetColor( 120, 1, 22 );

  qx_gen->SetMarkerColor( xanthous );
  qx_acc->SetMarkerColor( burg );

  TF1 * sg1 = new TF1( "sg_1","[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -7.5, 7.5 );
  sg1->SetParName( 0, "c"); 
  sg1->SetParName( 1, "mean" ); 
  sg1->SetParName( 2, "#sigma" );
  sg1->SetParameter( 0, qx_gen->GetMaximum() ); 
  sg1->SetParameter( 1, qx_gen->GetMean() ); 
  sg1->SetParameter( 2, qx_gen->GetStdDev() );
  sg1->SetParLimits( 0, qx_gen->GetMaximum()/2, qx_gen->GetMaximum()*2 ); 
  sg1->SetParLimits( 1, qx_gen->GetMean()/2, qx_gen->GetMean()*2 ); 
  sg1->SetParLimits( 2, qx_gen->GetStdDev()/2, qx_gen->GetStdDev()*2 );



  TF1 * sg2 = new TF1( "sg_1","[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -7.5, 7.5 );
  sg2->SetParName( 0, "c"); 
  sg2->SetParName( 1, "mean" ); 
  sg2->SetParName( 2, "#sigma" );

  sg2->SetParameter( 0, 1 ); 
  sg2->SetParameter( 1, qx_acc->GetMean() ); 
  sg2->SetParameter( 2, qx_acc->GetStdDev() );
  sg2->SetParLimits( 0, qx_acc->GetMaximum()/2, qx_acc->GetMaximum()*2 ); 
  sg2->SetParLimits( 1, qx_acc->GetMean()/2, qx_acc->GetMean()*2 ); 
  sg2->SetParLimits( 2, qx_acc->GetStdDev()/2, qx_acc->GetStdDev()*2 );


  gStyle->SetOptStat( "e" );
  gStyle->SetOptFit( 1 );

  TCanvas canvx1( "canv", "", 200, 200, 1000, 1000 );
  canvx1.Divide( 1, 1 );
  canvx1.cd( 1 );
  qx_gen->Draw( "E1" );
  qx_gen->GetYaxis()->SetRangeUser( 0, qx_gen->GetMaximum()*1.5 );
  qx_gen->Fit( sg1, "MQ", "", -7.5, 7.5 );
  qx_gen->GetYaxis()->SetTitleOffset( 1.5 );
  plotting::title( "q_{x}, no acceptance cuts", over_top_left, false );
  plotting::axes_titles( qx_gen, "q_{x} [GeV]", "Normalised Entries/1.0 [GeV]" );
  plotting::atlas( top_left, true, true );
  qx_gen->SetStats( kTRUE );
  plotting::stats( top_right_legend, qx_gen );
  canvx1.SaveAs( "qx000.png" );

  TCanvas canvx2( "canv", "", 200, 200, 1000, 1000 );
  canvx2.Divide( 1, 1 );
  canvx2.cd( 1 );

  canvx2.cd( 2 );
  qx_acc->Draw( "E1" );
  qx_acc->GetYaxis()->SetRangeUser( 0, qx_acc->GetMaximum()*1.5 );
  qx_acc->Fit( sg2, "MQ", "", -10, 10 );
  qx_acc->GetYaxis()->SetTitleOffset( 1.5 );
  plotting::title( "q_{x}, minimum acceptance", over_top_left, false );
  plotting::axes_titles( qx_acc, "q_{x} [GeV]", "Normalised Entries/1.0 [GeV]" );
  plotting::atlas( top_left, true, true );
  qx_acc->SetStats( kTRUE );
  plotting::stats( top_right_legend, qx_acc );
  canvx2.SaveAs( "qx445.png" );

}



void allsys( const std::string & storepath ){
  
  TFile * important_store = new TFile( storepath.c_str(), "READ" );

  TH1F * split1 = (TH1F *) important_store->Get( "qtbsplit-1" ); 
  TH1F * split2 = (TH1F *) important_store->Get( "qtbsplit-2" );
  TH1F * split3 = (TH1F *) important_store->Get( "qtbsplit-3" );

  TH1F * combo = (TH1F *) split1->Clone( "combo" );
  combo->Reset();
  combo->Add( split1 );
  combo->Add( split2 );
  combo->Add( split3 );

  
  TF1 * dg = new TF1( "dg", "[0]*e^(( -( ([1]-x) * ([1] - x ) ) )/(2*([2]*[2])))+ [3]*e^(( -( ([1]-x) * ([1] - x ) ) ) / (2*([4]*[4])))", 
                     -4, 14 );
  dg->SetParName( 0, "c_{1}"); 
  dg->SetParName( 1, "mean" ); 
  dg->SetParName( 2, "#sigma_{1}" );
  dg->SetParName( 3, "c_{2}"); 
  dg->SetParName( 4, "#sigma_{2}" );

  dg->SetParLimits( 0, 1000, 12000 );
  dg->SetParLimits( 1, 4, 6 );
  dg->SetParLimits( 2, 1, 5 );
  dg->SetParLimits( 3, 0, 2000 );
  dg->SetParLimits( 4, 5, 100 );


  TF1 * sg = new TF1( "sg","[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -4, 14 );
  sg->SetParName( 0, "c"); 
  sg->SetParName( 1, "mean" ); 
  sg->SetParName( 2, "#sigma" );
  sg->SetParLimits( 0, 1000, 12000 );
  sg->SetParLimits( 1, 4, 6 );
  sg->SetParLimits( 2, 1, 5 );

  TF1 * sgc = new TF1( "sg","[0]*e^((-([1]-x)^(2))/(2*([2])^(2))) + [3]", -4, 14 );
  sgc->SetParName( 0, "c_{1}"); 
  sgc->SetParName( 1, "mean" ); 
  sgc->SetParName( 2, "#sigma" );
  sgc->SetParName( 3, "c_{2}" );
  sgc->SetParLimits( 0, 1000, 12000 );
  sgc->SetParLimits( 1, 4, 6 );
  sgc->SetParLimits( 2, 1, 5 );
  sgc->SetParLimits( 3, 0, 1500 );



  double range = combo->GetMaximum()*1.5;
  
  //dg->SetParameter( 0, 8000 );
  //dg->SetParameter( 0, 5 );
  //dg->SetParameter( 2, 3 );
  //dg->SetParameter( 3, 3000 );
  //dg->SetParameter( 4, 7 );

  gStyle->SetOptFit( 1 );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  canv.cd( 1 );
  combo->Draw( "E1" );
  combo->SetStats( kTRUE );
  plotting::title( "q_{T}^{A}, combined", over_top_left, true );
  plotting::axes_titles( combo, "q_{T}^{A} [GeV]", "Yield/2.0 [GeV^{-1}]" );
  plotting::atlas( top_left, true, false );
  combo->GetYaxis()->SetRangeUser( 0, range );
  combo->Fit( dg, "MQ", "", -4, 14 );
  dg->Draw( "SAME" );
  TPaveStats * dg_stats = plotting::stats( top_right_legend, combo );
  dg_stats->Draw();
  canv.SaveAs( "combo_dg.png" );

  canv.Clear();
  canv.Divide( 1 );
  canv.cd( 1 );
  combo->Draw( "E1" );
  combo->SetStats( kTRUE );
  plotting::title( "q_{T}^{A}, combined", over_top_left, true );
  plotting::axes_titles( combo, "q_{T}^{A} [GeV]", "Yield/2.0 [GeV^{-1}]" );
  plotting::atlas( top_left, true, false );
  combo->GetYaxis()->SetRangeUser( 0, range );
  combo->Fit( sg, "MQ", "", -4, 14 );
  sg->Draw( "SAME" );
  TPaveStats * sg_stats = plotting::stats( top_right_legend, combo );
  sg_stats->Draw();
  canv.SaveAs( "combo_sg.png" );

  canv.Clear();
  canv.Divide( 1 );
  canv.cd( 1 );
  combo->Draw( "E1" );
  combo->SetStats( kTRUE );
  plotting::title( "q_{T}^{A}, combined", over_top_left, true );
  plotting::axes_titles( combo, "q_{T}^{A} [GeV]", "Yield/2.0 [GeV^{-1}]" );
  plotting::atlas( top_left, true, false );
  combo->GetYaxis()->SetRangeUser( 0, range );
  combo->Fit( sgc, "MQ", "", -4, 14 );
  sgc->Draw( "SAME" );
  TPaveStats * sgc_stats = plotting::stats( top_right_legend, combo );
  sgc_stats->Draw();
  canv.SaveAs( "combo_sgc.png" );



}


void subt( const std::string & sign_path, const std::string & bckg_path ){

  std::cout << sign_path << std::endl;
  std::cout << bckg_path << std::endl;

  
  TFile * sign_file = new TFile( sign_path.c_str(), "READ" );
  TFile * bckg_file = new TFile( bckg_path.c_str(), "READ" );
  sign_file->ls();
  bckg_file->ls();

  TTree * sign_tree = (TTree *) sign_file->Get( "tree" );
  TTree * bckg_tree = (TTree *) bckg_file->Get( "tree" );

  TH1F * sign_qtb = new TH1F( "sign_qtb", "", 50, 0, 15 );
  TH1F * bckg_qtb = new TH1F( "bckg_qtb", "", 50, 0, 15 );

  sign_tree->Draw( "abs(qtB)>>sign_qtb", "", "goff" );
  bckg_tree->Draw( "abs(qtB)>>bckg_qtb", "", "goff" );

  std::cout << sign_qtb->Integral() << std::endl;
  std::cout << bckg_qtb->Integral() << std::endl;

  sign_qtb->Scale( 1.0/sign_qtb->Integral() );
  bckg_qtb->Scale( 1.0/bckg_qtb->Integral() );

  TCanvas * qtb_canvas = new TCanvas( "qtb_canvas", "", 200, 200, 1000, 1000 );
  qtb_canvas->Divide( 1, 1 );

  gStyle->SetOptStat( "" );
  
  TPad * qtb_pad = (TPad *) qtb_canvas->cd( 1 );
  TH1F * ratio = plotting::ratio_subplot( sign_qtb, bckg_qtb, qtb_pad );
  sign_qtb->GetYaxis()->SetRangeUser( 0, 0.095 );
  ratio->GetXaxis()->SetTitle( "q_{T}^{B} [GeV]" );
  ratio->GetYaxis()->SetRangeUser( 0, 2.6 );
  ratio->GetXaxis()->SetTitleSize( 0.1 );
  ratio->GetYaxis()->SetTitleSize( 0.1 );
  sign_qtb->GetXaxis()->SetTitleSize( 0.05 );
  sign_qtb->GetYaxis()->SetTitleSize( 0.05 );

  qtb_pad = (TPad *) qtb_canvas->cd( 1 );

  box_edges edges = plotting::positions( top_left );
  float & x1 = edges.x1;
  float & y1 = edges.y1;

  gPad->cd();
  TLatex * atlas_logo_ltx = new TLatex(); 
  atlas_logo_ltx->SetNDC(); 
  atlas_logo_ltx->SetTextSize(0.038);
  atlas_logo_ltx->SetTextFont(72);
  atlas_logo_ltx->SetTextColor(1);
  TLatex * wip_ltx = new TLatex(); 
  wip_ltx->SetNDC();
  wip_ltx->SetTextFont(42);
  wip_ltx->SetTextSize(0.038);
  wip_ltx->SetTextColor(1);
  TLatex * sim_ltx = new TLatex();
  sim_ltx->SetNDC();
  sim_ltx->SetTextFont(42);
  sim_ltx->SetTextSize(0.038);
  sim_ltx->SetTextColor(1);
  

  atlas_logo_ltx->DrawLatexNDC( x1, y1-0.05, "ATLAS" );
  sim_ltx->DrawLatexNDC( x1+0.13, y1-0.05, "Simulation" );
	wip_ltx->DrawLatexNDC( x1, y1-0.045-0.05, "Work In Progress" );
  //plotting::atlas( top_left, true, true );
  plotting::axes_titles( ratio, "q_{T}^{B} [GeV]", "Ratio" );
  ratio->GetYaxis()->SetTitleOffset( 0.35 );
  sign_qtb->GetYaxis()->SetTitleOffset( 0.7 );
  sign_qtb->GetYaxis()->SetRangeUser( -0.005, 0.09 );
  ratio->GetYaxis()->SetLabelSize( 0.1 );
  ratio->GetXaxis()->SetLabelSize( 0.1 );
  ratio->GetXaxis()->SetTitleOffset( 1.0 );
  sign_qtb->GetYaxis()->SetLabelSize( 0.06 );

  sign_qtb->SetMarkerColor( kRed + 2 ); 
  bckg_qtb->SetMarkerColor( kBlue + 2 );

  plotting::axes_titles( sign_qtb, "" , "Yield/0.3 [GeV]" );
  plotting::title( "Normalised q_{T}^{B}" , top_left, true );

  box_edges ledges = plotting::positions( top_right );
  TLegend * legend = new TLegend( ledges.x1, ledges.y1-0.03, ledges.x2, ledges.y1 - 0.1 );
  legend->SetBorderSize( 0 );
  legend->SetFillColor( 0 );
  legend->SetFillStyle( 0 );
  legend->SetTextFont( 42 );
  legend->SetTextSize( 0.025 );
  legend->AddEntry( sign_qtb, "Signal","P" );
  legend->AddEntry( bckg_qtb, "Background","P" );
  legend->AddEntry( ratio, "Ratio","P" );
  legend->Draw();

  qtb_canvas->SaveAs( "qtbratio.png" );


}

void bbbg( const std::string & bbbg_path ){

  TFile * bbbg_file = new TFile( bbbg_path.c_str(), "READ" );
  bbbg_file->ls();

  TTree * bbbg_tree = (TTree *) bbbg_file->Get( "tree" );

  TH1F * presub = new TH1F(   "pre",  "", 50, -10, 20 );
  TH1F * postsub = new TH1F(  "post", "", 50, -10, 20 );
  

  bbbg_tree->Draw( "qtA>>pre", "", "goff" );
  bbbg_tree->Draw( "qtA>>post", "subtraction_weight", "goff" );

  presub->SetMarkerColor( kGray+2 );
  postsub->SetMarkerColor( kRed+2 );
  presub->SetLineColor( kGray+2 );
  postsub->SetLineColor( kRed+2 );


  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  canv.cd( 1 );

  gStyle->SetEndErrorSize( 5 );

  presub->Draw( "E1 HIST" );
  postsub->Draw( "E1 HIST SAME" );
  plotting::title( "Non-prompt subtraction", over_top_left, true );
  plotting::axes_titles( presub, "q_{T}^{A} [GeV]", "Yield/0.6 [GeV]" );
  plotting::atlas( top_left, true, true );
  TLegend * sub_legend = plotting::legend( top_right_legend, 2 );
  sub_legend->AddEntry( presub, "Before subtraction", "p" );
  sub_legend->AddEntry( postsub, "After subtraction", "p" );
  sub_legend->Draw();

  presub->GetYaxis()->SetRangeUser( -100, presub->GetMaximum()*1.5 );
  gStyle->SetEndErrorSize( 0 );

  canv.SaveAs( "sp.png" );




}
