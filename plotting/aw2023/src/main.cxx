#include <main.hxx>
#include <aw.hxx>


int main(int argc, char ** argv){

  int option_index{0},option{0};
  static struct option long_options[] = {
      { "input",          required_argument,    0,      'i'},
      { 0,                0,                    0,      0}
  };

  const char * config_path = "";

  do {
    option = getopt_long( argc, argv, "i:", long_options, &option_index );
    switch ( option ){
      case 'i':
        config_path = optarg; 
        option = -1;
        break;
    }
  } while ( option != -1 );


  if ( strlen( config_path ) == 0 ){ throw std::invalid_argument( "No config file provided" ); }

    
  laurel::initialise();

  YAML::Node config = YAML::LoadFile( config_path );
  std::vector< std::string > run_nodes = config["general"]["runs"].as<std::vector<std::string>>();

  for ( std::string & input : run_nodes ){

    YAML::Node run = config[input];
    std::string input_filepath = run["input_filepath"].as<std::string>();
    std::cout << input << std::endl;
    std::map< std::string, bool > modes  = run["modes"].as< std::map< std::string, bool > >();
  

    gStyle->SetErrorX( 0 );

    std::string inpath = std::string( std::getenv( "IN_PATH" ) );
    std::string outpath = std::string( std::getenv( "OUT_PATH" ) );
    std::string filepath = inpath + input_filepath;
    std::cout << filepath << std::endl;

    if ( input.find( "allsys" ) != std::string::npos ){
      allsys( outpath + run["store"].as<std::string>() );
      continue;
    }
    if ( input.find( "subt" ) != std::string::npos ){
      subt( outpath + run["sign"].as<std::string>(), outpath + run["bckg"].as<std::string>() );
      continue;
    }

    if ( input.find( "bbbg" ) != std::string::npos ){
      bbbg( outpath + run["bbbg"].as<std::string>() );
      continue;
    }



    if ( modes["acc"] ){
      filepath += "/tree";
      acc( filepath, false );
    } 
    if ( modes["gen"] ){
      filepath += "/truthTree";
      acc( filepath, true );
    }
    if ( modes["preprod"] ){
      preprod( outpath + run["gen"].as<std::string>(), outpath + run["acc"].as<std::string>() ); 
    }
    if ( modes["scatter"] ){
      scatter( outpath + run["sign"].as<std::string>(), outpath + run["bckg"].as<std::string>() ); 
    }
    if ( modes["check"] ){
      checkacc( outpath + run["gen"].as<std::string>(), outpath + run["acc"].as<std::string>() ); 
    }

  }
}
