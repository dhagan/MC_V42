unique="base"
yaml="$(pwd)/${unique}.yaml"

mkdir -p ${OUT_PATH}/aw/${unique}
mkdir -p ${OUT_PATH}/aw/${unique}/plots
pushd ${OUT_PATH}/aw/${unique} >> /dev/null

echo ${yaml}

aw -i ${yaml}

popd >> /dev/null




##for file_index  in {0..1};
##do
##	file="${files[$file_index]}"
##	mkdir -p ${file}
##	subtraction_filepath="${OUT_PATH}/event_subtraction/${unique}/${file}_${unique}.root"
##	allsub_path="${OUT_PATH}/event_subtraction/${unique}/"
##	${executable} -b ${allsub_path} -t ${file} -u ${unique} -v ${selections} -w
##	${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -c -n "sr"
##	${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -c -n "cr"
##done


##for file_index  in {1..1};
##do
##	file="${files[$file_index]}"
##	mkdir -p ${file}
##	subtraction_filepath="${OUT_PATH}/event_subtraction/${unique}/${file}_${unique}.root"
##	##${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -s -n "sr"
##	${executable} -b ${subtraction_filepath} -t ${file} -u ${unique} -v ${selections} -c -n "cr"
##done

#popd >> /dev/null
