#include <main.hxx>

void acc( const std::string & filepath, bool gen = false );
void preprod( const std::string & gen_path, const std::string & acc_path );
void scatter( const std::string & sign_path, const std::string & bckg_path );
void checkacc( const std::string & gen_path, const std::string & acc_path );
void allsys( const std::string & storepath );
void subt( const std::string & sign_path, const std::string & bckg_path );
void bbbg( const std::string & bbbg_path );
