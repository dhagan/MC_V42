cmake_minimum_required( VERSION 3.14.0 )

## Project details
project( apply_eff )

## Set sources and lib paths.
set( sources src/main.cxx src/eff.cxx )

## Find root package
find_package( ROOT CONFIG REQUIRED COMPONENTS )
##target_compile_definitions(ROOT::Core INTERFACE ${ROOT_DEFINITIONS} )
target_compile_options(ROOT::Core INTERFACE ${ROOT_DEFINITIONS} )

## Prepare executable
add_compile_options( -std=c++17 -Werror -Wall -pedantic -Wextra -O -Ofast )
add_executable( apply_eff ${sources} )
target_include_directories( apply_eff PUBLIC inc ${CMAKE_SOURCE_DIR}/lib/analysis_utils/inc )

## Link libraries
target_link_libraries( apply_eff PRIVATE ROOT::Core ${ROOT_LIBRARIES} )
target_link_libraries( apply_eff PRIVATE analysis_utils )

## Enable export.
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
