#include <eff.hxx>

void apply_efficiency( std::string & base_path, std::string & base_efficiency_path, std::string & analysis_var, 
                       std::string & spectator_var, std::string & type, bound_mgr * selections, 
                       std::string & unique, bool normalise ){

  // Set the canvas/hist styles
  prep_style();
  gStyle->SetOptFit( 11111111 );

  // base files
  TFile * base_extraction_file    = new TFile( base_path.c_str(),    "READ");
  TFile * base_efficiency_file    = new TFile( base_efficiency_path.c_str(),   "READ");
  std::string baseline_spectator = spectator_var;


  bound analysis_bound = selections->get_bound( analysis_var );
  bound baseline_spectator_bound = selections->get_bound( baseline_spectator );
  int analysis_bins = analysis_bound.get_bins();
  float analysis_min = analysis_bound.get_min();
  float analysis_max = analysis_bound.get_max();


  std::vector< int > mass_bins = { 0, 3, 4, 5, 12 };
  std::vector< float > base_style               = { 1, 1, 1.0, 1.00,  1, 1, 1.00, 1,  0, 0.00, 0.00 };


  // mass loop
  for ( int & mass_idx : mass_bins ){

    std::string mass = Form( "Q%i", mass_idx );


    TH1F * base_extracted = new TH1F( Form( "base_extracted_Q%i", mass_idx ), "", 
                                      analysis_bins, analysis_min, analysis_max );
    TH1F * base_corrected = new TH1F( Form( "base_corrected_Q%i", mass_idx ), "", 
                                      analysis_bins, analysis_min, analysis_max );
    TH1F * base_efficiency = (TH1F *) base_efficiency_file->Get( Form( "eff_%s_Q%i", analysis_bound.get_var().c_str(), mass_idx ) );  

    std::string baseline_differential_bin_name = Form( "%s_99_pos_%s_%s", baseline_spectator.c_str(), 
                                                      mass.c_str() , analysis_var.c_str() );
    std::string baseline_bin_hist_name = type + "_" + baseline_differential_bin_name;
    std::string baseline_err_hist_name = "errh_" + baseline_differential_bin_name;

    TH1F * be_clone = (TH1F*) base_extracted->Clone();
    TH1F * bc_clone = (TH1F*) base_corrected->Clone();

    int type_num = type_to_num( type );
    rc_hist( baseline_bin_hist_name, baseline_err_hist_name, base_extraction_file, base_extracted, type_num );
    base_corrected->Divide( base_extracted, base_efficiency );


    double base_integral = base_corrected->Integral();

    std::cout << "Baseline integral:   " << base_integral << std::endl;

    if ( normalise ){
      base_corrected->Scale( 1.0/base_integral );
    }

    style_hist( base_corrected, base_style );

    TCanvas * canvas = new TCanvas( Form( "canv_%i", mass_idx ),"canv", 100, 100, 3000, 1000 );
    canvas->Divide( 1, 3 );
    

    // histogram before correction
    TPad * active_pad = (TPad *) canvas->cd( 1 );
    base_extracted->Draw( "HIST E1" );
    hist_prep_axes( base_extracted );
    add_pad_title( active_pad, Form("%s Before correction - Q%i", analysis_bound.get_ltx().c_str(), mass_idx ) );
    set_axis_labels( base_extracted, Form( "%s", analysis_bound.get_ltx().c_str() ), Form("Yield %s", analysis_bound.get_y_str().c_str() ) );
    add_atlas_decorations( active_pad, true, false );
    TLegend * uncorr_legend = create_atlas_legend();
    uncorr_legend->AddEntry( base_extracted, "Uncorrected", "L" );
    uncorr_legend->Draw();

    active_pad = (TPad *) canvas->cd( 2 );
    base_efficiency->Draw( "HIST E1" );
    hist_prep_axes( base_efficiency );
    add_pad_title( active_pad, Form("%s Efficiency - Q%i", analysis_bound.get_ltx().c_str(), mass_idx ) );
    set_axis_labels( base_efficiency, Form( "%s", analysis_bound.get_ltx().c_str() ), Form("Efficiency %s", analysis_bound.get_y_str().c_str() ) );
    add_atlas_decorations( active_pad, true, false );
    TLegend * eff_legend = create_atlas_legend();
    eff_legend->AddEntry( base_efficiency, "Efficiency", "L" );
    eff_legend->Draw();

    active_pad = (TPad *) canvas->cd( 3 );
    base_corrected->Draw( "HIST E1" );
    hist_prep_axes( base_corrected );
    add_pad_title( active_pad, Form("%s Eff Correction - Q%i", analysis_bound.get_ltx().c_str(), mass_idx ) );
    set_axis_labels( base_corrected, Form( "%s", analysis_bound.get_ltx().c_str() ), Form("Yield %s", analysis_bound.get_y_str().c_str() ) );
    add_atlas_decorations( active_pad, true, false );
    TLegend * corr_legend = create_atlas_legend();
    corr_legend->AddEntry( base_corrected, "Corrected", "L" );
    corr_legend->Draw();

    active_pad = (TPad *) canvas->cd( 4 );
    be_clone->Draw( "HIST E1" );
    bc_clone->Draw( "HIST E1 SAME" );
    hist_prep_axes( base_corrected );
    add_pad_title( active_pad, Form("%s Pre and Post correction - Q%i", analysis_bound.get_ltx().c_str(), mass_idx ) );
    set_axis_labels( base_corrected, Form( "%s", analysis_bound.get_ltx().c_str() ), Form("Yield %s", analysis_bound.get_y_str().c_str() ) );
    add_atlas_decorations( active_pad, true, false );
    TLegend * both_legend = create_atlas_legend();
    both_legend->AddEntry( be_clone, "Uncorrected", "L" );
    both_legend->AddEntry( bc_clone, "corrected", "L" );
    both_legend->Draw();

    std::string canvas_name = Form( "eff_applied_%s_%s", unique.c_str(), mass.c_str() );
    if ( normalise ){ canvas_name += "_norm"; }
    canvas_name += ".png";
    canvas->SaveAs( canvas_name.c_str() );
    delete canvas;


  }
}
