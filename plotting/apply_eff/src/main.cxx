#include <main.hxx>
#include <eff.hxx>


int help(){
  std::cout << "help, as of yet, unwritten" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"base_file",         required_argument,        0,      'b'},
      {"base_efficiency",   required_argument,        0,      'e'},
      {"sys_file",          required_argument,        0,      'c'},
      {"sys_efficiency",    required_argument,        0,      'f'},
      {"type",              required_argument,        0,      't'},
      {"analysis_var",      required_argument,        0,      'a'},
      {"spectator_var",     required_argument,        0,      's'},
      {"selections_file",   required_argument,        0,      'v'},
      {"ranges",            required_argument,        0,      'r'},
      {"unique",            required_argument,        0,      'u'},
      {"norm",              no_argument,              0,      'n'},
      {"help",              no_argument,              0,      'h'},
      {0,                   0,                        0,      0}
    };

  std::string base_file, base_efficiency, systematic_efficiency, systematic_file;
  std::string type, analysis_var, spectator_var;
  std::string selections_file, ranges, unique;
  bool normalise = false;

  do {
    option = getopt_long(argc, argv, "b:e:c:f:t:a:s:v:r:u:nh", long_options,&option_index);
    switch (option){
      case 'b': 
        base_file               = std::string( optarg );
        break;
      case 'e': 
        base_efficiency         = std::string( optarg );
        break;
      case 'c': 
        systematic_file         = std::string( optarg );
        break;
      case 'f':
        systematic_efficiency   = std::string( optarg );
        break;
      case 't': 
        type                    = std::string( optarg );
        break;
      case 'a': 
        analysis_var            = std::string( optarg );
        break;
      case 's':
        spectator_var           = std::string( optarg );
        break;
      case 'v': 
        selections_file         = std::string( optarg );
        break;
      case 'r':
        ranges                  = std::string( optarg );
        break;
      case 'n':
        normalise = true;
        break;
      case 'u':
        unique                  = std::string( optarg );
        break;
      case 'h':
        return help();
    }

  } while (option != -1);

  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( selections_file );
  selections->process_bounds_string( ranges );
  
  apply_efficiency( base_file, base_efficiency,  analysis_var, spectator_var, type, selections, unique, normalise );
  return 0;

}

