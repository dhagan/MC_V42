
#include <common.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>
#include <style.hxx>
#include <fit_mgr.hxx>

void apply_efficiency( std::string & base_path, std::string & base_efficiency_path, std::string & analysis_var, 
                       std::string & spectator_var, std::string & type, bound_mgr * selections, 
                       std::string & unique, bool normalise );
