executable=${ANA_IP}/plotting/tree_check/build/tree_check
selections=${LIB_PATH}/share/compare_trees.txt
files=( "data" "sign" "bckg" "bbbg" )

for file in ${files[@]}
do
	unique=bdt_dec_${file}
	input_base="${HOME}/analysis/pulldown/base/tmva/recent/eval/eval_${file}.root:${file}_base"
	input_comp="${OUT_PATH}/bdt/base/eval/eval_${file}.root:${file}_base"
	log=${LOG_PATH}/tree_check/check_${unique}.txt
	mkdir -p ${OUT_PATH}/tree_check/check_${unique}
	touch ${log}
	pushd ${OUT_PATH}/tree_check/check_${unique} >> /dev/null
	${executable} -i "${input_base}#${input_comp}" -n "original:new" -s ${selections} -u ${unique} -t ${file} -b 2>&1 | tee ${log}
	popd >> /dev/null
done


