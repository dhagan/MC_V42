executable=${ANA_IP}/plotting/tree_check/build/tree_check
selections=${LIB_PATH}/share/compare_trees.txt
unique=jpsi_pt
sign_input="${OUT_PATH}/bdt/${unique}/eval/eval_sign.root"
bckg_input="${OUT_PATH}/bdt/${unique}/eval/eval_bckg.root"
data_input="${OUT_PATH}/bdt/${unique}/eval/eval_data.root"
bbbg_input="${OUT_PATH}/bdt/${unique}/eval/eval_bbbg.root"

mkdir -p ${OUT_PATH}/tree_check/overlay/${unique}/ 
pushd ${OUT_PATH}/tree_check/overlay/${unique}/ >> /dev/null
${executable} -i "${sign_input}:${bckg_input}:${data_input}:${bbbg_input}" -s ${selections} -u ${unique} -o
popd >> /dev/null
