executable=${ANA_IP}/plotting/tree_check/build/tree_check
selections=${LIB_PATH}/share/hf_bounds.txt
unique="base"
input=${OUT_PATH}/trees/gen_${unique}/sign/sign_${unique}.root
analysis_variable="qtA"
spectator_variable="qtB"
ranges="qtB:abs(qtB)&10:0:20"

mkdir -p ${OUT_PATH}/tree_check/diff_${unique}

pushd ${OUT_PATH}/tree_check/diff_${unique} >> /dev/null

$executable -m "diff" -i ${input} -a ${analysis_variable} -s ${spectator_variable} -v ${selections} -u ${unique} -r ${ranges}

popd >> /dev/null
