executable=${ANA_IP}/plotting/tree_check/build/tree_check
unique=base_data
log=${LOG_PATH}/tree_check/check_${unique}.txt
selections=${LIB_PATH}/share/compare_trees.txt

touch ${log}

mkdir -p ${OUT_PATH}/tree_check/check_${unique}
pushd ${OUT_PATH}/tree_check/check_${unique} >> /dev/null

input_base="${HOME}/analysis/pulldown/base/trees/data/data_base.root"
input_comp="${ANA_IP}/out/run/trees/gen_base/data/data_base.root"
${executable} -i "${input_base}:${input_comp}" -s ${selections} 2>&1 | tee ${log}

popd >> /dev/null
