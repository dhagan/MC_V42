executable=${ANA_IP}/plotting/tree_check/build/tree_check
selections=${LIB_PATH}/share/hf_bounds.txt
output_unique="diff_000"
input="${IN_PATH}/000/truth_uncut.root"
analysis_variable="qtA"
spectator_variable="qtB"
ranges="qtB:abs(qtB)&10:0:20"

mkdir -p ${OUT_PATH}/tree_check/diff_${output_unique}

pushd ${OUT_PATH}/tree_check/diff_${output_unique} >> /dev/null

$executable -m "diff" -i ${input} -a ${analysis_variable} -s ${spectator_variable} -v ${selections} -u ${output_unique} -r ${ranges}

popd >> /dev/null
