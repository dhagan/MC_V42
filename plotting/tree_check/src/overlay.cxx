#include <overlay.hxx>
  
void overlay( std::string & input, bound_mgr * selections, std::string & unique ){
  
  prep_style();
  
  std::vector<std::string> files;
  split_strings( files, input, ":" );
  
  TFile * sign_file = new TFile( files.at(0).c_str(), "READ" ); 
  TFile * bckg_file = new TFile( files.at(1).c_str(), "READ" );  
  TFile * data_file = new TFile( files.at(2).c_str(), "READ" ); 
  TFile * bbbg_file = new TFile( files.at(3).c_str(), "READ" ); 

  TTree * sign_tree = (TTree *) sign_file->Get( Form( "sign_%s", unique.c_str() ) );
  TTree * bckg_tree = (TTree *) bckg_file->Get( Form( "bckg_%s", unique.c_str() ) );
  TTree * data_tree = (TTree *) data_file->Get( Form( "data_%s", unique.c_str() ) );
  TTree * bbbg_tree = (TTree *) bbbg_file->Get( Form( "bbbg_%s", unique.c_str() ) );

  std::vector<std::string> overlay_vars = {"ActIpX", "AvgIpX", "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
															             "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
															             "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
															             "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0" };

  std::vector< float > sign_style = { 0, 0, 0, 0, 1, kRed+1, 1, 1, 0, 0, 0 };
  std::vector< float > bckg_style = { 0, 0, 0, 0, 1, kBlue+1, 1, 1, 0, 0, 0 };
  std::vector< float > data_style = { 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 };
  std::vector< float > bbbg_style = { 0, 0, 0, 0, 1, kGreen+1, 1, 1, 0, 0, 0 };


  for ( std::string & var : overlay_vars ){
  
    bound var_bound = selections->get_bound( var );

    double var_min = var_bound.get_min();
    double var_max = var_bound.get_max();
    int var_bins = var_bound.get_bins();



    TH1F * sign_hist = new TH1F( "sign", "sign", var_bins, var_min, var_max ); 
    TH1F * bckg_hist = new TH1F( "bckg", "bckg", var_bins, var_min, var_max ); 
    TH1F * data_hist = new TH1F( "data", "data", var_bins, var_min, var_max ); 
    TH1F * bbbg_hist = new TH1F( "bbbg", "bbbg", var_bins, var_min, var_max ); 

    style_hist( sign_hist, sign_style );
    style_hist( bckg_hist, bckg_style );
    style_hist( data_hist, data_style );
    style_hist( bbbg_hist, bbbg_style );

    sign_tree->Draw( Form( "%s>>sign", var.c_str() ) );
    bckg_tree->Draw( Form( "%s>>bckg", var.c_str() ) );
    data_tree->Draw( Form( "%s>>data", var.c_str() ) );
    bbbg_tree->Draw( Form( "%s>>bbbg", var.c_str() ) );

    double sign_int = sign_hist->Integral(); 
    double bckg_int = bckg_hist->Integral();
    double data_int = data_hist->Integral();
    double bbbg_int = bbbg_hist->Integral();

    sign_hist->Scale( 1.0/sign_int ); 
    bckg_hist->Scale( 1.0/bckg_int );
    data_hist->Scale( 1.0/data_int );
    bbbg_hist->Scale( 1.0/bbbg_int );

    TCanvas * overlay_canvas = new TCanvas( "ovly", "ovly", 200, 200, 1000, 1000 );
    overlay_canvas->Divide( 1 );

    TPad * active_pad = (TPad *) overlay_canvas->cd( 1 );
    sign_hist->Draw( "HIST" );
    bckg_hist->Draw( "HIST SAME" );
    data_hist->Draw( "HIST SAME" );
    bbbg_hist->Draw( "HIST SAME" );
    hist_prep_axes( sign_hist );
    sign_hist->GetYaxis()->SetRangeUser( 0, 0.15 );
    add_pad_title( active_pad, Form( "%s %s", var.c_str(), unique.c_str() ), false );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( sign_hist, var_bound.get_x_str(), var_bound.get_y_str() );
    TLegend * ovly_legend = create_atlas_legend();
    ovly_legend->AddEntry( sign_hist, "sign" );
    ovly_legend->AddEntry( bckg_hist, "bckg" );
    ovly_legend->AddEntry( data_hist, "data" );
    ovly_legend->AddEntry( bbbg_hist, "bbbg" );
    ovly_legend->Draw();

    overlay_canvas->SaveAs( Form( "./%s_overlay.png", var.c_str() ) ); 

    delete overlay_canvas;
    delete sign_hist;
    delete bckg_hist;
    delete data_hist;
    delete bbbg_hist;


  }



}
