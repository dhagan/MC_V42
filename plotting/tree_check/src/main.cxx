#include <main.hxx>
#include <compare.hxx>
#include <overlay.hxx>
#include <diff.hxx>


int main ( int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "input",            required_argument,        0,      'i'},
      { "selections",       required_argument,        0,      's'},
      { "names",            required_argument,        0,      'n'},
      { "unique",           required_argument,        0,      'u'},
      { "BDT",              no_argument,              0,      'b'},
      {0,             0,                        0,      0}
    };

  bool bdt = false;
  std::string input, names, unique, type;
  std::string bounds_filepath, ranges, analysis_variable, spectator_variable;
  std::string mode;

  do {
    option = getopt_long(argc, argv, "i:a:s:v:n:r:u:t:b:m:", long_options,&option_index);
    switch (option){
      case 'i':
        input       = std::string( optarg );
        break;
      case 'a':
        analysis_variable = std::string( optarg );
        break;
      case 's':
        spectator_variable = std::string( optarg );
        break;
      case 'v':
        bounds_filepath  = std::string( optarg );
        break;
      case 'n':
        names = std::string( optarg );
        break;
      case 'u':
        unique  = std::string( optarg );
        break;
      case 'b':
        bdt = true;
        break;
      case 't':
        type  = std::string( optarg );
        break;
      case 'm':
        mode = std::string( optarg );
        break;
      case 'r':
        ranges = std::string( optarg );
        break;
    }

  } while (option != -1);
  

  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( bounds_filepath );
  if ( !ranges.empty() ){
    selections->process_bounds_string( ranges );
  }



  if ( mode.find( "comp" ) != std::string::npos ){
    compare( input, selections,  names, type, unique, bdt );  
  } else if ( mode.find( "ovly" ) != std::string::npos ){
    overlay( input, selections, unique );
  } else if ( mode.find( "diff" ) != std::string::npos ){

    variable_set variables = variable_set();
    variables.analysis_variable = analysis_variable;
    variables.spectator_variable = spectator_variable;
    variables.analysis_bound = selections->get_bound( analysis_variable );
    variables.spectator_bound = selections->get_bound( spectator_variable );
    variables.bound_manager = selections;
  
    TFile * input_file = new TFile( input.c_str(), "READ" );
    TTree * input_tree = (TTree *) input_file->Get( "tree" );

    diff( input_tree, variables, unique );

  }

  return 0;
}

