#include <compare.hxx>

void compare( std::string & input, bound_mgr * selections, std::string & names, std::string & type, std::string & unique, bool bdt){

  prep_style();
  gStyle->SetOptStat( "imrn" );

  std::vector<std::string> input_files, base_strs, comp_strs;
  split_strings( input_files, input, "#" );
  split_strings( base_strs, input_files.at(0), ":" );
  split_strings( comp_strs, input_files.at(1), ":" );
  std::string & base_file = base_strs.at( 0 );
  std::string & base_ttree = base_strs.at( 1 );
  std::string & comp_file = comp_strs.at( 0 );
  std::string & comp_ttree = comp_strs.at( 1 );

  std::vector<std::string> name_vec;
  split_strings( name_vec, names, ":" );
  std::string & base_name = name_vec.at( 0 );
  std::string & comp_name = name_vec.at( 1 );

  
  TFile * base = new TFile( base_file.c_str(), "READ" );
  TFile * comp = new TFile( comp_file.c_str(), "READ" );
  
  TTree * base_tree = (TTree*) base->Get( base_ttree.c_str() );
  TTree * comp_tree = (TTree*) comp->Get( comp_ttree.c_str() );


  std::vector< std::string > branches = { "ActIpX", "AvgIpX", "costheta", "AbsCosTheta", "DPhi", "AbsdPhi", "Phi", "AbsPhi", 
                                          "DY", "AbsdY", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau",
                                          "DiMuonPt", "PhotonPt", "qxpsi", "qypsi", 
                                          "qxsum", "qysum", "qxgamma", "qygamma", "qtA", "qtB", "qtL", "qtM", "JPsi_Eta", 
                                          "Phot_Eta", "DeltaZ0" };
  if ( bdt ){ branches.push_back( "BDT" ); }

 

  for ( std::string & branch : branches ){

    bound branch_bound = selections->get_bound( branch );
    int branch_bins = branch_bound.get_bins(); 
    float branch_min = branch_bound.get_min();
    float branch_max = branch_bound.get_max();


    TH1F * base_hist = new TH1F( Form( "base_%s", branch.c_str() ), "", branch_bins, branch_min, branch_max ); 
    TH1F * comp_hist = new TH1F( Form( "comp_%s", branch.c_str() ), "", branch_bins, branch_min, branch_max ); 
    TH1F * ratio_hist = new TH1F( Form( "ratio_%s", branch.c_str() ), "", branch_bins, branch_min, branch_max ); 
    TH1F * diff_hist = new TH1F( Form( "diff_%s", branch.c_str() ), "", branch_bins, branch_min, branch_max ); 
    

    base_tree->Draw( Form( "%s>>base_%s", branch.c_str(), branch.c_str() ), "", "goff");
    comp_tree->Draw( Form( "%s>>comp_%s", branch.c_str(), branch.c_str() ), "", "goff");
    ratio_hist->Divide( comp_hist, base_hist );
    diff_hist->Add( base_hist, comp_hist, 1.0, -1.0 );


    TH1F * log_base_hist = (TH1F *) base_hist->Clone();
    TH1F * log_comp_hist = (TH1F *) comp_hist->Clone();
    TH1F * log_ovly_base_hist = (TH1F *) base_hist->Clone();
    TH1F * log_ovly_comp_hist = (TH1F *) comp_hist->Clone();
    TH1F * norm_base_hist = (TH1F *) base_hist->Clone();
    TH1F * norm_comp_hist = (TH1F *) comp_hist->Clone();
    TH1F * norm_ovly_base_hist = (TH1F *) base_hist->Clone();
    TH1F * norm_ovly_comp_hist = (TH1F *) comp_hist->Clone();
    norm_base_hist->Scale( 1.0/norm_base_hist->Integral() );  
    norm_comp_hist->Scale( 1.0/norm_comp_hist->Integral() );
    norm_ovly_base_hist->Scale( 1.0/norm_ovly_base_hist->Integral() );
    norm_ovly_comp_hist->Scale( 1.0/norm_ovly_comp_hist->Integral() );

    std::vector< float > base_style = { 0, 0, 0, 0, 1.0, kRed+1, 1.0, 1.0, 0, 0, 0};
    std::vector< float > comp_style = { 0, 0, 0, 0, 2.0, kBlue+1, 1.0, 1.0, 0, 0, 0};

    style_hist( base_hist, base_style );
    style_hist( comp_hist, comp_style );
    style_hist( log_base_hist, base_style );
    style_hist( log_comp_hist, comp_style );
    style_hist( log_ovly_base_hist, base_style );
    style_hist( log_ovly_comp_hist, comp_style );
    style_hist( norm_base_hist, base_style );
    style_hist( norm_comp_hist, comp_style );
    style_hist( norm_ovly_base_hist, base_style );
    style_hist( norm_ovly_comp_hist, comp_style );
    
  
    TCanvas * canv = new TCanvas( "comp_canv", "", 200, 200, 4000, 3000 );
    canv->Divide( 4, 3 );

    TPad * current_pad = (TPad*) canv->cd( 1 );
    base_hist->Draw( "HIST" );
    hist_prep_axes( base_hist );
    add_pad_title( current_pad, Form( "%s %s", base_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( base_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * base_stats = make_stats( base_hist );
    base_stats->Draw();

  
    // LONE PLOTS, RATIO AND DIFFS
    current_pad = (TPad*) canv->cd( 2 );
    comp_hist->Draw( "HIST" );
    hist_prep_axes( comp_hist );
    add_pad_title( current_pad, Form( "%s %s", comp_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( comp_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * comp_stats = make_stats( comp_hist );
    comp_stats->Draw();

    current_pad = (TPad*) canv->cd( 3 );
    ratio_hist->Draw( "HIST" );
    ratio_hist->GetYaxis()->SetRangeUser( 0.95, 1.05  );
    hist_prep_axes( ratio_hist );
    ratio_hist->GetYaxis()->SetRangeUser( 0.95, 1.05  );
    add_pad_title( current_pad, Form( "%s %s %s ratio", base_name.c_str(), comp_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( ratio_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * ratio_stats = make_stats( ratio_hist );
    ratio_stats->Draw();

    current_pad = (TPad*) canv->cd( 4 );
    diff_hist->Draw( "HIST" );
    diff_hist->GetYaxis()->SetRangeUser( -25, 25 );
    hist_prep_axes( diff_hist );
    diff_hist->GetYaxis()->SetRangeUser( -25, 25 );
    add_pad_title( current_pad, Form( "%s %s %s Difference", base_name.c_str(), comp_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( diff_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * diff_stats = make_stats( diff_hist );
    diff_stats->Draw();
    


    // LOGARITHMIC PLOTS
    current_pad = (TPad*) canv->cd( 5 );
    log_base_hist->Draw( "HIST" );
    gPad->SetLogy();
    log_base_hist->GetYaxis()->SetRangeUser( 1.0, log_base_hist->GetMaximum()*10 );
    add_pad_title( current_pad, Form( "%s %s Logarithm", base_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( log_base_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * log_base_stats = make_stats( log_base_hist );
    log_base_stats->Draw();

    current_pad = (TPad*) canv->cd( 6 );
    log_comp_hist->Draw( "HIST" );
    gPad->SetLogy();
    log_comp_hist->GetYaxis()->SetRangeUser( 1.0, log_comp_hist->GetMaximum()*10 );
    add_pad_title( current_pad, Form( "%s %s Logarithm", comp_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( log_comp_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * log_comp_stats = make_stats( log_comp_hist );
    log_comp_stats->Draw();
    
    current_pad = (TPad*) canv->cd( 7 );
    gPad->SetLogy();
    log_ovly_base_hist->Draw( "HIST" );
    log_ovly_comp_hist->Draw( "HIST SAMES" );
    log_ovly_base_hist->GetYaxis()->SetRangeUser( 1.0, log_ovly_comp_hist->GetMaximum()*10 );
    add_pad_title( current_pad, Form( "Logarithmic Overlay %s", branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( log_ovly_base_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * log_ovly_base_stats = make_stats( log_ovly_base_hist, true, true );
    log_ovly_base_stats->Draw();
    TPaveStats * log_ovly_comp_stats = make_stats( log_ovly_comp_hist, true, false );
    log_ovly_comp_stats->Draw();
    TLegend * log_ovly_legend = below_logo_legend();
    log_ovly_legend->AddEntry( log_ovly_base_hist, base_name.c_str() );
    log_ovly_legend->AddEntry( log_ovly_comp_hist, comp_name.c_str() );
    log_ovly_legend->Draw();


    // NORM PLOTS
    current_pad = (TPad*) canv->cd( 9 );
    norm_base_hist->Draw( "HIST" );
    add_pad_title( current_pad, Form( "%s %s", base_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( norm_base_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * norm_base_stats = make_stats( norm_base_hist );
    norm_base_stats->Draw();

    current_pad = (TPad*) canv->cd( 10 );
    norm_comp_hist->Draw( "HIST" );
    add_pad_title( current_pad, Form( "%s %s Normalised", comp_name.c_str(), branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( norm_comp_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * norm_comp_stats = make_stats( norm_comp_hist );
    norm_comp_stats->Draw();
    
    current_pad = (TPad*) canv->cd( 11 );
    norm_ovly_base_hist->Draw( "HIST" );
    norm_ovly_comp_hist->Draw( "HIST SAMES" );
    norm_ovly_base_hist->GetYaxis()->SetRangeUser( 0, norm_ovly_base_hist->GetMaximum() * 1.5 );
    add_pad_title( current_pad, Form( "Normalised overlay %s", branch.c_str() ), true);
    add_atlas_decorations( current_pad, true );
    set_axis_labels( norm_ovly_base_hist, branch_bound.get_x_str(), branch_bound.get_y_str() );
    TPaveStats * norm_ovly_base_stats = make_stats( norm_ovly_base_hist, true, true );
    norm_ovly_base_stats->Draw();
    TPaveStats * norm_ovly_comp_stats = make_stats( norm_ovly_comp_hist, true, false );
    norm_ovly_comp_stats->Draw();
    TLegend * norm_ovly_legend = below_logo_legend();
    norm_ovly_legend->AddEntry( norm_ovly_base_hist, base_name.c_str() );
    norm_ovly_legend->AddEntry( norm_ovly_comp_hist, comp_name.c_str() );
    norm_ovly_legend->Draw();

    canv->SaveAs( Form( "./%s_%s_%s_comp.png", type.c_str(), unique.c_str(), branch.c_str() ));
    delete canv;

  }





  return;
}
