#include <diff.hxx>


void single_slice( TH1F * slice_hist, int slice_number, variable_set & variables, std::string & unique ){
 
  gStyle->SetOptStat( "krimes" );
  
  TH1F * current_slice = (TH1F *) slice_hist->Clone();
  
  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "default_" );
  styles->style_histogram( current_slice, "default" );

  TCanvas * slice_canvas = new TCanvas( Form( "diff_canvas_%i", slice_number ), "", 200, 200, 2000, 2000 );
  slice_canvas->Divide( 1, 1 );

  TPad * slice_pad = (TPad *) slice_canvas->cd( 1 );

  hist_prep_axes( current_slice );
  current_slice->SetLineColorAlpha( kBlack, 1.0 );
  current_slice->Draw( "HIST E1" );
  current_slice->Draw( "P SAME" );
  add_pad_title( slice_pad, Form( "%s - %i", variables.analysis_variable.c_str(), slice_number ), true );
  add_atlas_decorations( slice_pad, true, false );
  set_axis_labels( current_slice, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  TLegend * slice_legend = below_logo_legend();
  slice_legend->AddEntry( current_slice, Form( "%s-%i", variables.analysis_variable.c_str(), slice_number ) , "LP" );
  slice_legend->Draw();
  TPaveStats * slice_stats = make_stats( current_slice );
  slice_stats->Draw();

  slice_canvas->SaveAs( Form( "./slice_%s-%i_%s.png", variables.analysis_variable.c_str(), slice_number, unique.c_str()) );
  delete slice_canvas;
  gStyle->SetOptStat( "" );

}


void diff( TTree * input_tree, variable_set & variables, std::string & unique ){

  prep_style();
  gStyle->SetOptStat( "" );

  style_mgr * styles = new style_mgr();
  styles->load_styles();
  styles->set_stage( "diff_style" );
  std::vector< float > initial_style = { 20, kRed, 1.0, 2,0, 1.0, kRed+1, 1.0, 1.0, 0.0, 0.0, 0.0 };

  std::vector< std::string > spectator_cuts = variables.spectator_bound.get_cut_series();
  std::vector< std::string > spectator_cut_names = variables.spectator_bound.get_series_names();
  std::vector< TH1F * > diff_histograms;
  std::vector< double > maxes;
  std::make_heap( maxes.begin(), maxes.end() );

  for ( int cut = 0; cut < (int) spectator_cuts.size(); cut++ ){

    TH1F * current = variables.analysis_bound.get_hist( spectator_cut_names[ cut ] );
    input_tree->Draw( Form( "%s>>%s", variables.analysis_variable.c_str(), 
                      spectator_cut_names[ cut ].c_str() ), Form( "(%s)", spectator_cuts[ cut ].c_str() ), "goff" );
    style_hist( current, initial_style );
    current->SetLineColor( kBlack );
    single_slice( current, cut+1, variables, unique );  
    initial_style[ 0 ] += 1; initial_style[ 1 ] += 1;
    current->Scale( 1.0/current->Integral() );
    diff_histograms.push_back( current );
    maxes.push_back( current->GetMaximum() );
    std::push_heap( maxes.begin(), maxes.end() );

  }

  
  TCanvas * diff_canvas = new TCanvas( "diff_canvas", "", 200, 200, 2000, 2000 ); 
  diff_canvas->Divide( 1, 1 );

  TPad * diff_pad = (TPad *) diff_canvas->cd( 1 );

  TH1F * initial_histogram = diff_histograms.at( 0 );
  initial_histogram->Draw( "P" );
  initial_histogram->Draw( "E1 SAME" );

  hist_prep_axes( initial_histogram );
  initial_histogram->GetYaxis()->SetRangeUser( 0, maxes.at( 0 )*1.7 );
  add_pad_title( diff_pad, Form( "Progression - %s in %s", variables.analysis_variable.c_str(), variables.spectator_variable.c_str() ), true );
  add_atlas_decorations( diff_pad, true, true );
  set_axis_labels( initial_histogram, variables.analysis_bound.get_x_str(), Form( "Yield%s", variables.analysis_bound.get_y_str().c_str() ) );
  TLegend * diff_legend = create_atlas_legend();
  diff_legend->AddEntry( initial_histogram, spectator_cut_names.at( 0 ).c_str(), "P" );
  for ( int cut = 1; cut < (int) spectator_cuts.size(); cut++ ){
    diff_histograms.at( cut )->Draw( "P SAME" );  
    diff_histograms.at( cut )->Draw( "E1 SAME" );
    diff_legend->AddEntry( diff_histograms.at( cut ), spectator_cut_names.at( cut ).c_str(), "PE" );

  }
  diff_legend->Draw( "SAME" );

  diff_canvas->SaveAs( Form( "progression_%s_in_%s_%s.png", variables.analysis_variable.c_str(), variables.spectator_variable.c_str(), unique.c_str() ) );
  initial_histogram->GetYaxis()->SetRangeUser( 1, maxes.at(0)*100 );
  gPad->SetLogy();
  diff_canvas->SaveAs( Form( "progression_%s_in_%s_%s_log.png", variables.analysis_variable.c_str(), variables.spectator_variable.c_str(), unique.c_str() ) );


}
