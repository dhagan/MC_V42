#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef pre_cut_hxx 
#define pre_cut_hxx

void pre_cut( YAML::Node & run_node, variable_set & variables );

#endif
