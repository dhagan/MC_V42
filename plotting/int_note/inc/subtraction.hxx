#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef sub_hxx 
#define sub_hxx

void subtraction_plots( YAML::Node & run_node, variable_set & variables );
void demo_plots( YAML::Node & run_node, variable_set & variables );
void proof( YAML::Node & run_node, variable_set & variables );

#endif

