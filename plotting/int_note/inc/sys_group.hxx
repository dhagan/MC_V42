#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef sys_group_hxx 
#define sys_group_hxx

void sys_group( YAML::Node & run_node, variable_set & variables );

TH1F * sys_group_err( std::string & nominal_unique, 
  std::vector<std::string> & systematics, variable_set & variables,
  bool absolute=false );

#endif

