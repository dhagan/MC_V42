
#ifndef func_hf_hxx
#define func_hf_hxx

#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "RooAbsPdf.h"
#include "RooFit.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "RooCategory.h"
#include "Roo1DTable.h"
#include "RooFormulaVar.h"
#include "RooConstVar.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooCBShape.h"
#include "RooAbsReal.h"
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooPolynomial.h"
#include "RooDataHist.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include <ROOT/RDataFrame.hxx>

#include <TObject.h>

#include <yaml-cpp/yaml.h>

#include <regex>

#include <anna.hxx>

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;

struct measure{
  Measurement measure;
  RooStats::HistFactory::Channel channel;
  RooStats::HistFactory::Sample sign, bckg;
  RooWorkspace * wkspc;
  RooAbsPdf * pdf;
  RooAbsData * data;
};

measure produce_measurement( TH1F * sign, TH1F * bckg, TH1F * data );
void pos_hists( TH1F * data, TH1F * sign, TH1F * bckg );
TH1F * output_histogram( RooFitResult * results, TH1F * sign, TH1F * bckg, TH1F * data, std::string & output_string, fit_results & fit_stats );



inline float uniform_error( float  max, float min ){
  return ( max - min )*0.57735026919;
}
inline float absolute_quadrature_two( float left, float right){
  return std::sqrt( left*left + right*right );
}

// look, i know this is shitty, get result has control regions, get fit has only 'Analysis' region.
std::tuple< TH1F *, hist_group > get_result( const std::string & sys_unique, variable_set & variables );
std::tuple< TH1F *, hist_group > get_fit( const std::string & unique, variable_set & variables );

std::vector< double > get_effects( const std::string & filepath );

double rel_effect_systxt( const std::vector<std::string> & lines, const std::string & param );

TH1F * combine_sc( TH1F * sr, TH1F * cr );
void save_and_clear( TCanvas & canv, TPad * active_pad, std::string output_name );

hist_group combine_sc_group( const hist_group & sr, const hist_group & cr, variable_set & variables );

std::pair< hist_group, hist_group> trex_inputs_ana( const std::string & unique, variable_set & variables );

std::vector< std::pair< hist_group, hist_group > > trex_inputs_fullspec( const std::string & unique, variable_set & variables );

std::pair< hist_group, hist_group > trex_input_spec( const std::string & unique, const std::string & bin );

void add_pad( TH1F * hist, const std::string & name, const std::string & y_axis, bound & bound );

Float_t dphi_calc( const ROOT::RVecF &DiLept_Phi, const ROOT::RVecF &Photon_Phi );

Float_t phi_calc( const ROOT::RVecF &MuMuGamma_CS_Phi );

Float_t photon_dr_calc( const ROOT::RVecF &Photon_Pt, const ROOT::RVecF &Photon_Eta, const ROOT::RVecF &Photon_Phi, const ROOT::RVecF &Photon_E,
											 const ROOT::RVecF &mcPhoton_Pt,  const ROOT::RVecF &mcPhoton_Eta, const ROOT::RVecF &mcPhoton_Phi );

#endif
