#include <anna.hxx>
#include <funcs.hxx>
#include <ROOT/RDataFrame.hxx>

void analyse_cutflow( YAML::Node & run_node, variable_set & variables );
void make_cutflow( YAML::Node & run_node, variable_set & variables );
void truth_cutflow( YAML::Node & run_node, variable_set & variables );
void make_cutflow_table( YAML::Node & run_node, variable_set & variables );
void truth_cutflow_table( YAML::Node & run_node, variable_set & variables );


//void cutflow( const std::string & file_path, const std::string & type,
//                  const std::string & unique, const std::string & cutflow_var,
//                  bound_mgr * selections );
