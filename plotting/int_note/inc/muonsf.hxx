#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef muonsf_hxx 
#define muonsf_hxx

void muonsf( YAML::Node & run_node, variable_set & variables );

#endif

