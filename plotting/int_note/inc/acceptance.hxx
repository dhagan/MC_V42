#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef acc_hxx 
#define acc_hxx

void acceptance_plots( YAML::Node & run_node, variable_set & variables );
void make_acceptance_trees( bool gen );
void make_acceptance_overlays( YAML::Node & run_node, variable_set & variables );

#endif

