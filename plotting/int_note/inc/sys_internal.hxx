#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef sys_internal_hxx 
#define sys_internal_hxx

void sys_internal( YAML::Node & run_node, variable_set & variables );

#endif

