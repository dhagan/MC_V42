#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef photsf_hxx 
#define photsf_hxx

void photonsf( YAML::Node & run_node, variable_set & variables );

#endif

