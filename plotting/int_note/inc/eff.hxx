#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef eff_hxx 
#define eff_hxx

void efficiency_plots( YAML::Node & run_node, variable_set & variables );

#endif

