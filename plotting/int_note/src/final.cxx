#include <final.hxx>
#include <sys/stat.h>
#include <sys_group.hxx>

double rwp(double value, double precision = 0.01 ){
    return std::round(value / precision) * precision;
}

/*
* Function that outputs table of either TMD or cross section setup
* @param Histogram with total errors, no efficiency.
* @param Histogram with statistical errors, no efficiency.
* @param Histogram with internal systematic errors, no efficiency.
* @param Histogram with external systematic errors, no efficiency.
* @param Histogram with efficiencies and their errors.
* @param Histogram with yield adjustments and efficiency.
* @param Name of error table (tmd or Xsec).
* @param Variable set.
*/
void error_table( TH1F * total, TH1F * stat, TH1F * internal, TH1F * external,
  TH1F * efficiency, TH1F * final, std::string name, variable_set & variables){

  // define row and column names, define table structure
  int bins = variables.analysis_bound.get_bins();
  std::string column_names = "$q_{T}^{A}$ & \\makecell{Yield} & \\makecell{Statistical\\\\Error} & \\makecell{Internal\\\\Error}"
                             "& \\makecell{External\\\\Error} & \\makecell{Efficiency\\\\ and Error}" 
                             "& \\makecell{Total error} & \\makecell{Corrected Yield} & \\makecell{Corrected Error}"
                             "\\\\ \\hline\n";
  std::string table_layout = "|c|r|r|r|r|r|r|r|r|r|";

  // open file and write prelude.
  std::ofstream error_table( name + ".tex" );
  error_table << "\\documentclass[landscape]{article}\n";
  error_table << "\\usepackage[paperheight=4in,paperwidth=10in]{geometry}\n";
  error_table << "\\usepackage{makecell}";
  error_table << "\\begin{document}";
  error_table << std::fixed << std::setprecision( 2 );
  error_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{" << table_layout << "}\n\\hline\n";
  error_table << column_names;

  // iterate over rows
  for ( int bin = 1; bin <= bins; bin++ ){ 

    // name row, add yield
    error_table << std::to_string( bin )  << " & ";
    error_table << rwp( total->GetBinContent( bin ), 0.01) << " & ";
    error_table << rwp( stat->GetBinError( bin ), 0.01 ) << " & ";
    error_table << rwp( internal->GetBinError( bin ), 0.01) << " & ";
    error_table << rwp( external->GetBinError( bin ), 0.01 ) << " & ";
    error_table << rwp( efficiency->GetBinContent( bin ), 0.01 )  << "$\\pm$" << std::setprecision(3) << rwp( efficiency->GetBinError( bin ), 0.001) << std::setprecision(2) << " & ";
    error_table << rwp( total->GetBinError( bin ), 0.01) << " & ";
    error_table << rwp( final->GetBinContent( bin ), 0.01) << " & ";
    error_table << rwp( final->GetBinError( bin ), 0.01 );

    // next row
    error_table << "\\\\ \\hline\n";
  }

  // finish documennd and close
  error_table << "\\end{tabular}\n\\caption{Things}\n\\end{table}\n\\end{document}";
  error_table.close();

}

void plot_cross_section( TH1F * cross_section, TH1F * stat_error, TH1F * efficiency ){

  TH1F * stat_with_eff = static_cast<TH1F *>( stat_error->Clone() );
  stat_with_eff->Scale( 1/(2.0*2.57));
  stat_with_eff->Divide( stat_with_eff, efficiency );
  
  // define axes labels
  std::string x_string = "q_{T}^{A} [GeV]";
  std::string y_string = "#sigma/2.000 [fb^{-1}]";

  // style histograms
  stat_with_eff->SetLineColor( kBlack );
  cross_section->SetLineColor( kRed+2 );
  cross_section->SetLineStyle( 1 );
  cross_section->SetMarkerStyle( 20 );
  cross_section->SetMarkerColor( kBlack );

  // create canvas, pad, and draw histograms
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  stat_with_eff->Draw( "HIST E1" );
  cross_section->Draw( "HIST E1 SAME" );
  
  
  // label, decorate, and range
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( stat_with_eff, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( stat_with_eff, true );
  
  // add legend
  TLegend * legend = below_logo_legend();
  legend->AddEntry( stat_with_eff, "Statistical error", "L" );
  legend->AddEntry( cross_section, "Cross Section", "LP" );
  legend->Draw();

  // set backgrounds, margin, save, and clean up
  canv.SetFillStyle( 4000 );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  std::string output_name = "cross_section.pdf";
  canv.SaveAs( output_name.c_str() );
  canv.Clear();

}

void plot_tmd( TH1F * tmd, TH1F * stat_error, TH1F * efficiency ){

  TH1F * stat_with_eff = static_cast<TH1F *>( stat_error->Clone() );
  stat_with_eff->Divide( stat_with_eff, efficiency );
  
  // define axes labels
  std::string x_string = "q_{T}^{A} [GeV]";
  std::string y_string = "d#sigma/dq_{T}^{A} [fb/GeV]";

  // style histograms
  stat_with_eff->SetLineColor( kBlack );
  stat_with_eff->SetLineWidth( 2 );
  stat_with_eff->SetMarkerSize( 0 );
  tmd->SetLineColor( rapsberry );
  tmd->SetLineStyle( 1 );
  tmd->SetLineWidth( 2 );
  tmd->SetMarkerStyle( 20 );
  tmd->SetMarkerColor( rapsberry );

  // create canvas, pad, and draw histograms
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  stat_with_eff->Draw( "HIST E1" );
  tmd->Draw( "HIST E1 SAME" );
  
  // label, decorate, and range
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( stat_with_eff, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( stat_with_eff, true );
  
  // add legend
  TLegend * legend = below_logo_legend();
  legend->AddEntry( stat_with_eff, "Statistical error", "L" );
  legend->AddEntry( tmd, "Total Error", "LP" );
  legend->Draw();

  // set backgrounds, margin, save, and clean up
  canv.SetFillStyle( 4000 );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  std::string output_name = "tmd.pdf";
  canv.SaveAs( output_name.c_str() );
  canv.Clear();

}

void fitting( TH1F * TMD ){

  // define axes labels
  std::string x_string = "q_{T}^{A} [GeV]";
  std::string y_string = "d#sigma/dq_{T}^{A} [fb/GeV]";

  TMD->SetLineColor( kGray+3 );

  // create and name the single gaussian
  TF1 * single_gauss = new TF1( "sg", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -5, 13 );
  single_gauss->SetParName( 0, "c"); 
  single_gauss->SetParName( 1, "mean" ); 
  single_gauss->SetParName( 2, "#sigma" );

  // create and name the double gaussian
  TF1 * double_gauss = new TF1( "dg", "[0]*e^(( -( ([1]-x) * ([1] - x ) ) )/(2*([2]*[2])))"
                                "+ [3]*e^(( -( ([1]-x) * ([1] - x ) ) ) / (2*([4]*[4])))", 
                                -10, 20 );
  double_gauss->SetParName( 0, "c_{1}"); 
  double_gauss->SetParName( 1, "mean" ); 
  double_gauss->SetParName( 2, "#sigma_{1}" );
  double_gauss->SetParName( 3, "c_{2}"); 
  double_gauss->SetParName( 4, "#sigma_{2}" );

  // create and name the single+constgaussian
  TF1 * single_const = new TF1( "sc", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2))) + [3]", -10, 20 );
  single_const->SetParName( 0, "c" ); 
  single_const->SetParName( 1, "mean" ); 
  single_const->SetParName( 2, "#sigma" );
  single_const->SetParName( 3, "const." );

  TF1 * gauss_breit = new TF1( "sg", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2))) "
   " + [3]*TMath::BreitWigner( x, [1], [4])", -10.0, 20.0 );
  gauss_breit->SetParName( 0, "c"); 
  gauss_breit->SetParName( 1, "mean" ); 
  gauss_breit->SetParName( 2, "#sigma" );
  gauss_breit->SetParName( 3, "cbw"); 
  gauss_breit->SetParName( 4, "#gamma"); 

  TF1 * voigt = new TF1( "vgt", "[0]*TMath::Voigt(x-[1], [2], [3], [4] )", -10.0, 20.0 );
  voigt->SetParName( 0, "c_{v}"); 
  voigt->SetParName( 1, "mean" ); 
  voigt->SetParName( 2, "#sigma_{v}" );
  voigt->SetParName( 3, "#gamma_{v}" );
  voigt->SetParName( 4, "r" );
  voigt->SetParameter( 0, 10e3 );
  voigt->SetParameter( 1, 5 );
  voigt->SetParameter( 2, 2.4 );
  voigt->SetParameter( 3, 8 );
  voigt->SetParameter( 4, -10000 );
  //voigt->SetParLimits( 3, 4, 30 );





  TF1 * gauss_voigt = new TF1( "gv", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))"
   " + [3]*TMath::Voigt( x-[1], [4], [5], [6] )", -10.0, 20.0 );
  gauss_voigt->SetParName( 0, "c_{g}"); 
  gauss_voigt->SetParName( 1, "mean" ); 
  gauss_voigt->SetParName( 2, "#sigma_{g}" );
  gauss_voigt->SetParName( 3, "c_{v}"); 
  gauss_voigt->SetParName( 4, "#sigma_{v}" );
  gauss_voigt->SetParName( 5, "#gamma_{v}"); 
  gauss_voigt->SetParName( 6, "r"); 
  gauss_voigt->SetParameter( 0, 8000 );
  gauss_voigt->SetParameter( 1, 5    );
  gauss_voigt->SetParameter( 2, 3.1  );
  gauss_voigt->SetParameter( 3, 4000 ); 
  gauss_voigt->SetParameter( 4, 5    ); 
  gauss_voigt->SetParameter( 5, 4    );
  gauss_voigt->SetParameter( 6, 4    );

  // align the gaussians to the TMD
  align_dg( double_gauss, TMD );
  align_sg( single_gauss, TMD );
  align_sg( single_const, TMD );
  align_sg( gauss_breit, TMD );
  gauss_breit->SetParameter( 3, 10000 );
  gauss_breit->SetParameter( 4, 50);
  //gauss_breit->SetParLimits( 4, 20, 30 );

  //gauss_breit->SetParLimits( 3, 0, 40000 );

  // create some colors, and create some decomposed functions
  Int_t orng = TColor::GetColor( 186, 45, 11 );
  Int_t ochre = TColor::GetColor( 219, 124, 38 );
  TF1 * g1 = new TF1( "g1", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 ); 
  g1->SetLineColor( orng );
  g1->SetLineStyle( 2 );
  TF1 * g2 = new TF1( "g2", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  g2->SetLineColor( ochre );
  g2->SetLineStyle( 2 );


  TF1 * ng1 = new TF1( "ng1", "([0]/([2]*sqrt(2*3.141592)))*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 ); 
  ng1->SetLineColor( orng );
  ng1->SetLineStyle( 2 );
  TF1 * ng2 = new TF1( "ng2", "([0]/([2]*sqrt(2*3.141592)))*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 ); 
  ng2->SetLineColor( ochre );
  ng2->SetLineStyle( 2 );
  TF1 * ndg = new TF1( "ndg", "([0]/([2]*sqrt(2*3.141592)))*e^((-([1]-x)^(2))/(2*([2])^(2)))"
                                           "+ ([3]/([4]*sqrt(2*3.141592)))*e^((-([1]-x)^(2))/(2*([4])^(2)))", -10, 20 );
  ndg->SetParName( 0, "c_{1}"); 
  ndg->SetParName( 1, "mean" ); 
  ndg->SetParName( 2, "#sigma_{1}" );
  ndg->SetParName( 3, "c_{2}"); 
  ndg->SetParName( 4, "#sigma_{2}" );


  align_dg( ndg, TMD );




  //TF1 * g3 = new TF1( "g3", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  //g3->SetLineColor( orng );
  //g3->SetLineStyle( 2 );

  //TF1 * c1 = new TF1( "c1", "[0]", -10, 20 );
  //c1->SetLineColor( ochre );
  //c1->SetLineStyle( 2 );

  //TF1 * g4 = new TF1( "g4", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  //g4->SetLineColor( orng );
  //g4->SetLineStyle( 2 );
  //
  //TF1 * b1 = new TF1( "b1", "[0]*TMath::BreitWigner( x, [1], [2] )", -10, 20 );
  //b1->SetLineColor( ochre );
  //b1->SetLineStyle( 2 );


  //TF1 * g5 = new TF1( "g5", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  //g5->SetLineColor( orng );
  //g5->SetLineStyle( 2 );
  TF1 * g6 = new TF1( "g5", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  g6->SetLineColor( orng );
  g6->SetLineStyle( 2 );

  TF1 * v1 = new TF1( "v1", "[0]*TMath::Voigt(x-[1], [2], [3], [4] )", -10.0, 20.0 );
  v1->SetLineColor( ochre );
  v1->SetLineStyle( 2 );




  //TF1 * v2 = new TF1( "v1", "[0]*TMath::Voigt(x-[1], [2], [3], [4] )", -10.0, 20.0 );



  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  TMD->Draw( "HIST E1");
  TMD->Fit( single_gauss, "MQ", "", -7, 12 );
  single_gauss->Draw( "SAME" ); 
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( TMD, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( TMD, true );
  TPaveStats * stats1 = make_stats( TMD );
  stats1->Draw( "SAME" );
  save_and_clear( canv, active_pad, "single_gauss" );

  //canv.Divide( 1, 1 );
  //active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  //TMD->Draw( "HIST E1");
  //TMD->Fit( double_gauss, "MQ", "", -7, 12 );
  //double_gauss->Draw( "SAME" );
  //g1->SetParameter( 0, double_gauss->GetParameter( 0 ) ); 
  //g1->SetParameter( 1, double_gauss->GetParameter( 1 ) );
  //g1->SetParameter( 2, double_gauss->GetParameter( 2 ) );
  //g2->SetParameter( 0, double_gauss->GetParameter( 3 ) );
  //g2->SetParameter( 1, double_gauss->GetParameter( 1 ) );
  //g2->SetParameter( 2, double_gauss->GetParameter( 4 ) );
  //g1->Draw( "SAME" );
  //g2->Draw( "SAME" );
  //add_atlas_decorations( active_pad, false, false );
  //add_internal( active_pad );
  //set_axis_labels( TMD, x_string.c_str(), y_string.c_str() );
  //TPaveStats * stats2 = make_stats( TMD );
  //stats2->Draw( "SAME" );
  //save_and_clear( canv, active_pad, "double_gauss" );


  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  TMD->Draw( "HIST E1");
  TMD->Fit( ndg, "MQ", "", -7, 12 );
  ndg->Draw( "SAME" );
  ng1->SetParameter( 0, ndg->GetParameter( 0 ) ); 
  ng1->SetParameter( 1, ndg->GetParameter( 1 ) );
  ng1->SetParameter( 2, ndg->GetParameter( 2 ) );
  ng2->SetParameter( 0, ndg->GetParameter( 3 ) );
  ng2->SetParameter( 1, ndg->GetParameter( 1 ) );
  ng2->SetParameter( 2, ndg->GetParameter( 4 ) );
  ng1->Draw( "SAME" );
  ng2->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( TMD, x_string.c_str(), y_string.c_str() );
  TPaveStats * stats3 = make_stats( TMD );
  stats3->Draw( "SAME" );
  save_and_clear( canv, active_pad, "double_gauss2" );



  //canv.Divide( 1, 1 );
  //active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  //TMD->Draw( "HIST E1");
  //TMD->Fit( single_const, "MQ", "", -7, 12 );
  //single_const->Draw( "SAME" );
  //g3->SetParameter( 0, single_const->GetParameter( 0 ) ); 
  //g3->SetParameter( 1, single_const->GetParameter( 1 ) );
  //g3->SetParameter( 2, single_const->GetParameter( 2 ) );
  //c1->SetParameter( 0, single_const->GetParameter( 3 ) );
  //g3->Draw( "SAME" );
  //c1->Draw( "SAME" );
  //add_atlas_decorations( active_pad, false, false );
  //add_internal( active_pad );
  //set_axis_labels( TMD, x_string.c_str(), y_string.c_str() );
  //TPaveStats * stats3 = make_stats( TMD );
  //stats3->Draw( "SAME" );
  //save_and_clear( canv, active_pad, "single_const" );

  //canv.Divide( 1, 1 );
  //active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  //TMD->Draw( "HIST E1");
  //TMD->Fit( gauss_breit, "MQ", "", -10.0, 20.0 );
  //gauss_breit->Draw( "SAME" );
  //g4->SetParameter( 0, gauss_breit->GetParameter( 0 ) );  
  //g4->SetParameter( 1, gauss_breit->GetParameter( 1 ) );
  //g4->SetParameter( 2, gauss_breit->GetParameter( 2 ) );
  //g4->Draw( "SAME" );
  //b1->SetParameter( 0, gauss_breit->GetParameter( 3 ) );  
  //b1->SetParameter( 1, gauss_breit->GetParameter( 1 ) );  
  //b1->SetParameter( 2, gauss_breit->GetParameter( 4 ) );  
  //b1->Draw( "SAME" );
  //add_atlas_decorations( active_pad, false, false );
  //add_internal( active_pad );
  //set_axis_labels( TMD, x_string.c_str(), y_string.c_str() );
  //TPaveStats * stats4 = make_stats( TMD );
  //stats4->Draw( "SAME" );
  //TLegend * legend4 = below_logo_legend();
  //legend4->AddEntry( gauss_breit, "Gauss+Breit", "L" );
  //legend4->AddEntry( g4, "Gauss", "F" );
  //legend4->AddEntry( b1, "Breit", "F" );
  //legend4->Draw( "SAME" );
  //save_and_clear( canv, active_pad, "gauss_breit" );


  //canv.Divide( 1, 1 );
  //active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  //TMD->Draw( "HIST E1");
  //TMD->Fit( gauss_voigt, "MQ", "", -10.0, 20.0 );
  //gauss_voigt->Draw( "SAME" );
  //g6->SetParameter( 0, gauss_voigt->GetParameter( 0 ) );  
  //g6->SetParameter( 1, gauss_voigt->GetParameter( 1 ) );
  //g6->SetParameter( 2, gauss_voigt->GetParameter( 2 ) );
  //g6->Draw( "SAME" );
  //v1->SetParameter( 0, gauss_voigt->GetParameter( 3 ) );  
  //v1->SetParameter( 1, gauss_voigt->GetParameter( 1 ) );
  //v1->SetParameter( 2, gauss_voigt->GetParameter( 4 ) );
  //v1->SetParameter( 3, gauss_voigt->GetParameter( 5 ) );
  //v1->SetParameter( 4, gauss_voigt->GetParameter( 6 ) );
  //v1->Draw( "SAME" );
  //add_atlas_decorations( active_pad, false, false );
  //add_internal( active_pad );
  //set_axis_labels( TMD, x_string.c_str(), y_string.c_str() );
  //TPaveStats * stats5 = make_stats( TMD );
  //stats5->Draw( "SAME" );
  //TLegend * legend5 = below_logo_legend();
  //legend5->AddEntry( gauss_voigt, "Gauss+Voigt", "L" );
  //legend5->AddEntry( g6, "gauss", "L" );
  //legend5->AddEntry( v1, "voigt", "L" );
  //legend5->Draw( "SAME" );
  //save_and_clear( canv, active_pad, "gauss_voigt" );

  //canv.Divide( 1, 1 );
  //active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  //TMD->Draw( "HIST E1");
  //TMD->Fit( voigt, "MQ", "", -10.0, 20.0 );
  //voigt->Draw( "SAME" );
  //add_atlas_decorations( active_pad, false, false );
  //add_internal( active_pad );
  //set_axis_labels( TMD, x_string.c_str(), y_string.c_str() );
  //TPaveStats * stats6 = make_stats( TMD );
  //stats6->Draw( "SAME" );
  //TLegend * legend6 = below_logo_legend();
  //legend6->AddEntry( voigt, "Voigt", "L" );
  //legend6->Draw( "SAME" );
  //save_and_clear( canv, active_pad, "voigt" );

  // define row and column names, define table structure
  std::string column_names = "fit & $C_{1}$ & $\\sigma_{1}$ & $X$ & $C_{2}$ & $\\sigma_{2}$ & $C_{3}$\\\\ \\hline\n";
  std::string table_layout = "|c|r|r|r|r|r|r|";

  // open file and write prelude.
  std::ofstream fit_table( "fits.tex" );
  fit_table << "\\documentclass[landscape]{article}\n";
  fit_table << "\\usepackage[paperheight=4in,paperwidth=10in]{geometry}\n";
  fit_table << "\\usepackage{makecell}";
  fit_table << "\\begin{document}";
  fit_table << std::fixed << std::setprecision( 2 );
  fit_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{" << table_layout << "}\n\\hline\n";
  fit_table << column_names;
  fit_table << "\\makecell{Single Gaussian} & " << single_gauss->GetParameter( 0 ) << "$\\pm$" << single_gauss->GetParError( 0 ) ;
  fit_table << " & " << single_gauss->GetParameter( 2 ) << "$\\pm$" << single_gauss->GetParError( 2 );
  fit_table << " & " << single_gauss->GetParameter( 1 ) << "$\\pm$" << single_gauss->GetParError( 1 );
  fit_table << " & - & - & -\\\\ \\hline\n";
  fit_table << "\\makecell{Double Gaussian} & " << double_gauss->GetParameter( 0 ) << "$\\pm$" << double_gauss->GetParError( 0 ) ;
  fit_table << " & " << double_gauss->GetParameter( 2 ) << "$\\pm$" << double_gauss->GetParError( 2 );
  fit_table << " & " << double_gauss->GetParameter( 1 ) << "$\\pm$" << double_gauss->GetParError( 1 );
  fit_table << " & " << double_gauss->GetParameter( 3 ) << "$\\pm$" << double_gauss->GetParError( 3 );
  fit_table << " & " << double_gauss->GetParameter( 4 ) << "$\\pm$" << double_gauss->GetParError( 4 );
  fit_table << " & - \\\\ \\hline\n";
  fit_table << "\\makecell{Single Gaussian\\\\+const}   & " << single_const->GetParameter( 0 ) << "$\\pm$" << single_const->GetParError( 0 ) ;
  fit_table << " & " << single_const->GetParameter( 2 ) << "$\\pm$" << single_const->GetParError( 2 );
  fit_table << " & " << single_const->GetParameter( 1 ) << "$\\pm$" << single_const->GetParError( 1 );
  fit_table << " & - & - & " << single_const->GetParameter( 3 ) << "$\\pm$" << single_const->GetParError( 3 );
  fit_table << "\\\\ \\hline\n";
  // finish documennd and close
  fit_table << "\\end{tabular}\n\\caption{Things}\n\\end{table}\n\\end{document}";
  fit_table.close();


}


// load the fiducial or efficiency fileset
basic_fileset * load_efficiency( std::string unique, bool fiducial ){
  std::string filepath = std::string( std::getenv( "OUT_PATH" ) )
    + "/trees/" + unique + "/efficiency/sign_";
  filepath += (fiducial) ? "fiducials_" : "efficiencies_";
  filepath += unique + ".root";
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  std::cout << filepath << std::endl;
  fileset->load_efficiency_fileset( filepath, unique, fiducial );
  return fileset;
}

// compile the two main final results for the analysis
void final_results( YAML::Node & run_node, variable_set & variables ){

  // enter output directory
  mkdir( "./final", 744 );
  chdir( "./final" );

  // get unique
  std::string unique = run_node["output_unique"].as<std::string>();
  std::string stat_unique = unique+"_stats";

  // load efficiency files
  std::string eff_unique = run_node[ "eff_unique" ].as<std::string>();
  std::string fid_unique = run_node[ "fid_unique" ].as<std::string>();
  basic_fileset * eff_fileset = load_efficiency( eff_unique, false );
  basic_fileset * fid_fileset = load_efficiency( fid_unique, true );

  // get nominal, and statonly yields
  auto [ base_total, base_extracted ] = get_fit( unique, variables );
  auto [ stat_total, stat_extracted ] = get_fit( stat_unique, variables );

  // is this an absolute or a relative sys_group error calculation
  bool absolute = run_node["absolute"].as<bool>();

  // get systematic errors, these have their own nominal
  // hists here are relative errors
  std::vector< TH1F * > sys_errs;
  std::string sys_nominal = run_node["sys_nominal_unique"].as<std::string>();
  std::vector<std::string> systematic_groups = run_node[ "sys_groups" ].as<std::vector<std::string>>();
  for ( std::string & sys_group : systematic_groups ){
    std::vector<std::string> sys_uniques = run_node[sys_group].as<std::vector<std::string>>();
    sys_errs.push_back( sys_group_err( sys_nominal, sys_uniques, variables, absolute ) );
  }

  // prepare a hist with the stat, total, internal, & external error
  TH1F * stat_hist = static_cast<TH1F *>( stat_extracted.sign_hist->Clone() );
  TH1F * total_hist = static_cast<TH1F *>( base_extracted.sign_hist->Clone() );
  TH1F * external_hist = static_cast<TH1F *>( base_extracted.sign_hist->Clone() );
  TH1F * internal_hist = static_cast<TH1F *>( base_extracted.sign_hist->Clone() );
  for ( int bin = 1; bin < total_hist->GetNbinsX(); bin++ ){

    double external_error = 0;     
    double total_error = total_hist->GetBinError(bin)*total_hist->GetBinError(bin); 

    std::cout << absolute << std::endl;

    // add group errors together to create external error, add external errors to total
    for ( TH1F * sys : sys_errs ){

      // important. the absolute relative error is calculated with reference to
      // the extraction with no systematic error
      double group_error = absolute ? sys->GetBinContent( bin ) : sys->GetBinContent( bin )*total_hist->GetBinContent( bin );

      total_error += group_error*group_error;
      external_error += group_error*group_error;
    }

    double internal_error = total_hist->GetBinError( bin )*total_hist->GetBinError( bin );
    internal_error -= stat_hist->GetBinError(bin)*stat_hist->GetBinError(bin);

    internal_hist->SetBinError( bin, std::sqrt(internal_error) );
    external_hist->SetBinError( bin, std::sqrt( external_error ));
    total_hist->SetBinError(bin, std::sqrt( total_error ) );
  }

  // create the cross section hist
  TH1F * cross_section = static_cast<TH1F*>( total_hist->Clone( "xsec" ) );
  // scaling is by total luminosity and deltaqta (qta bin width)
  cross_section->Scale( 1.0/(2.0*2.57) );
  TH1F * fiducial_efficiency = fid_fileset->get_efficiency( variables );
  cross_section->Divide( cross_section, fiducial_efficiency );

  double integral_err = 0;
  double integral = cross_section->IntegralAndError( 1, cross_section->GetNbinsX(), integral_err, "" );
  std::cout << "Integral: " << integral*2 << " \\pm " << integral_err*2<< std::endl;

  // make cross section plot and table
  error_table( total_hist, stat_hist, internal_hist, external_hist, 
    fiducial_efficiency, cross_section, "xsec", variables );

  // create the tmd hist
  TH1F * tmd = static_cast<TH1F*>( total_hist->Clone( "tmd" ) );
  TH1F * selection_efficiency = eff_fileset->get_efficiency( variables );
  tmd->Scale( 1.0/(2.0*2.57) );
  tmd->Divide( tmd, selection_efficiency );

  // make tmd plot, and table
  error_table( total_hist, stat_hist, internal_hist, external_hist, 
    selection_efficiency, tmd, "tmd", variables );

  stat_hist->Scale( 1.0/(2.0*2.57) );
  // make tmd fits, and fit table
  plot_tmd( tmd, stat_hist, selection_efficiency );
  fitting( tmd );
  //plot_cross_section( cross_section, stat_hist, fiducial_efficiency );
  
  
  



  // exit to start
  chdir( ".." );
}
