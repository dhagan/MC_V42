#include <funcs.hxx>

measure produce_measurement( TH1F * sign, TH1F * bckg, TH1F * data ){
  
  Measurement measurement( "measurement","measurement");
  measurement.SetOutputFilePrefix( "./results/example_UsingC" );
  measurement.SetPOI( "mu" ); 
  measurement.AddConstantParam( "Lumi" );
  measurement.SetLumi( 1.0 );
  measurement.SetExportOnly( true );
    
  // Create a channel
  RooStats::HistFactory::Channel chan( "channel1" );
  chan.SetData( data );
  chan.SetStatErrorConfig( 0.05, "Poisson" ); //original
  
  // Create the signal sample
  RooStats::HistFactory::Sample signal( "sign" );
  signal.SetHisto( sign );
  signal.AddNormFactor( "mu", 0.2, 0.0, 5.0 );
  signal.SetNormalizeByTheory( true );
  chan.AddSample( signal );
  
  // Background
  RooStats::HistFactory::Sample background( "bckg" );
  background.SetHisto( bckg );
  background.AddNormFactor( "ScalingPP", 2.0, 0, 10.0 );
  background.SetNormalizeByTheory( true );
  chan.AddSample( background );
  
  measurement.AddChannel( chan );
  RooWorkspace * workspace = RooStats::HistFactory::MakeModelAndMeasurementFast( measurement );
  RooAbsPdf * pd = workspace->pdf( "simPdf" );
  RooAbsData * d = workspace->data( "obsData" );

  measure complete_measure;
  complete_measure.measure = measurement;
  complete_measure.channel = chan;
  complete_measure.sign = signal;
  complete_measure.bckg = background;
  complete_measure.wkspc = workspace;
  complete_measure.pdf = pd;
  complete_measure.data = d;

  return complete_measure;
}

TH1F * output_histogram( RooFitResult * results, TH1F * sign, TH1F * bckg, TH1F * data, std::string & output_string, fit_results & fit_stats ){


  // scale
  sign->Scale( fit_stats.mu_value );
  bckg->Scale( fit_stats.spp_value );
  TH1F * fit = (TH1F *) sign->Clone( "fit" );
  fit->Reset();
  fit->SetLineColor( kGreen+1 );
  fit->Add( sign );
  fit->Add( bckg );

  TCanvas * result_canvas = new TCanvas( "result_canv", "result_canv", 200, 200, 1000, 1000 );
  result_canvas->Divide( 1 );
   
  TPad * result_pad = (TPad *) result_canvas->cd( 1 ); 

  data->Draw( "E1 P" );
  sign->Draw("HIST E1 SAME");
  bckg->Draw("HIST E1 SAME");
  fit->Draw("HIST E1 SAME");
  hist_prep_axes( data );
  add_atlas_decorations( result_pad, true );
  set_axis_labels( data, "BDT Score", Form("Entries/%.2f", data->GetBinWidth(8) ) );
  add_pad_title( result_pad, Form( "%s", output_string.c_str() ) );
  TLegend * results_legend = below_logo_legend();
  results_legend->AddEntry( data,   "data",       "lep");
  results_legend->AddEntry( sign,   "sign_extr",  "l" );
  results_legend->AddEntry( bckg,   "bckg_extr",  "l" );
  results_legend->AddEntry( fit,    "Fit",        "l" );
  results_legend->Draw();

  auto ltx = new TLatex();
  ltx->SetTextSize( 0.025 );
  ltx->DrawLatexNDC( 0.54, 0.77, Form("n_{sign_extr} = %.0f #pm %0.0f", sign->Integral() ,fit_stats.sign_fit_error ));
  ltx->DrawLatexNDC( 0.54, 0.74, Form("n_{bckg_extr} = %.0f #pm %0.0f", bckg->Integral() ,fit_stats.bckg_fit_error ));
  ltx->DrawLatexNDC( 0.54, 0.71, Form("#mu = %.4f #pm %.4f", fit_stats.mu_value, fit_stats.mu_error));
  ltx->DrawLatexNDC( 0.54, 0.68, Form("SPP = %.4f #pm %0.4f",fit_stats.spp_value, fit_stats.spp_error ) );
  ltx->DrawLatexNDC( 0.54, 0.65, Form("#chi^{2}_{V1}/ndf = %.4f/%i",fit_stats.chi2_v1, fit_stats.ndof) );
  ltx->DrawLatexNDC( 0.54, 0.61, Form("#chi^{2}_{V2}/ndf = %.4f/%i",fit_stats.chi2_v2, fit_stats.ndof ) );

  if ( results->status() == 0 ){
    result_canvas->SaveAs(Form( "./fit/%s.png", output_string.c_str() ));
  } else if ( results->status() == 4 ){
    TPaveText * warning = new TPaveText( 0.4, 0.5, 0.6, 0.8, "NDC" );
    warning->AddText( "NO CONVERGENCE: MINUIT 4" );
    warning->SetTextFont( 42 );
    warning->SetTextSize( 0.03 );
    warning->SetTextColor( 2 );
    warning->SetFillStyle( 0 );
    warning->SetLineColor( 0 );
    warning->SetLineStyle( 0 );
    warning->SetLineWidth( 0 );
    warning->SetBorderSize( 0 );
    warning->Draw();
    result_canvas->SaveAs( Form( "./fit/%s-FAILED.png", output_string.c_str() ) );
    result_canvas->SaveAs( Form( "./fit_failed/%s-FAILED.png", output_string.c_str() ) );
  }
  delete result_canvas;

  TH1F * error = new TH1F( Form( "errh_%s", output_string.c_str() ), "", 3, 0.0, 3.0);
  error->SetBinContent( 1, 1 );
  error->SetBinError(1, sqrt( data->Integral() ) );
  error->SetBinContent( 2, 1 );
  error->SetBinError( 2, fit_stats.sign_fit_error );
  error->SetBinContent( 3, 1 );
  error->SetBinError( 3, fit_stats.bckg_fit_error );

  return error;

}


//std::tuple< TH1F *, hist_group > get_bin( const std::string & unique, variable_set & variables );
std::tuple< TH1F *, hist_group > get_fit( const std::string & unique, variable_set & variables ){

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();

  TH1F * sign = variables.analysis_bound.get_hist( "sign_" + unique );
  TH1F * bckg = variables.analysis_bound.get_hist( "bckg_" + unique );
  TH1F * data = variables.analysis_bound.get_hist( "data_" + unique );
  TH1F * fit = variables.analysis_bound.get_hist( "fit_" + unique );

  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    std::string plots_path = trex_qta_path + "Plots/";
    std::string table_path = trex_qta_path + "Tables/";

    YAML::Node table_result = YAML::LoadFile( table_path + "Table_postfit.yaml" );
    
    double sign_fit_signal_yield = table_result[0]["Samples"][0]["Yield"].as<double>();
    double sign_fit_signal_error = table_result[0]["Samples"][0]["Error"].as<double>();
    double bckg_fit_signal_yield = table_result[0]["Samples"][1]["Yield"].as<double>();
    double bckg_fit_signal_error = table_result[0]["Samples"][1]["Error"].as<double>();
    double fit_signal_yield = table_result[0]["Samples"][2]["Yield"].as<double>();
    double fit_signal_error = table_result[0]["Samples"][2]["Error"].as<double>();
    double data_signal_yield = table_result[0]["Samples"][3]["Yield"].as<double>();

    sign->SetBinContent( ana_idx+1, sign_fit_signal_yield );
    sign->SetBinError( ana_idx+1,   sign_fit_signal_error );
    bckg->SetBinContent( ana_idx+1, bckg_fit_signal_yield );
    bckg->SetBinError( ana_idx+1,   bckg_fit_signal_error );
    data->SetBinContent( ana_idx+1, data_signal_yield );
    data->SetBinError( ana_idx+1,   std::sqrt( data_signal_yield ) );
    fit->SetBinContent( ana_idx+1,  fit_signal_yield );
    fit->SetBinError( ana_idx+1,    fit_signal_error );

  }

  hist_group results( sign, bckg, data );
  return std::make_tuple( fit, std::move( results ) );

}


std::tuple< TH1F *, hist_group > get_result( const std::string & unique, variable_set & variables ){

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";

  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();

  TH1F * sign = variables.analysis_bound.get_hist( "sign_" + unique );
  TH1F * bckg = variables.analysis_bound.get_hist( "bckg_" + unique );
  TH1F * data = variables.analysis_bound.get_hist( "data_" + unique );
  TH1F * fit = variables.analysis_bound.get_hist( "fit_" + unique );

  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    std::string plots_path = trex_qta_path + "Plots/";
    std::string table_path = trex_qta_path + "Tables/";

    YAML::Node table_result = YAML::LoadFile( table_path + "Table_postfit.yaml" );
    
    double sign_fit_signal_yield = table_result[0]["Samples"][0]["Yield"].as<double>();
    double sign_fit_signal_error = table_result[0]["Samples"][0]["Error"].as<double>();
    double sign_fit_control_yield = table_result[1]["Samples"][0]["Yield"].as<double>();
    double sign_fit_control_error = table_result[1]["Samples"][0]["Error"].as<double>();
    double bckg_fit_signal_yield = table_result[0]["Samples"][1]["Yield"].as<double>();
    double bckg_fit_signal_error = table_result[0]["Samples"][1]["Error"].as<double>();
    double bckg_fit_control_yield = table_result[1]["Samples"][1]["Yield"].as<double>();
    double bckg_fit_control_error = table_result[1]["Samples"][1]["Error"].as<double>();
    double data_signal_yield = table_result[0]["Samples"][1]["Yield"].as<double>();
    double data_control_yield = table_result[1]["Samples"][1]["Yield"].as<double>();
    double fit_signal_yield = table_result[0]["Samples"][2]["Yield"].as<double>();
    double fit_signal_error = table_result[0]["Samples"][2]["Error"].as<double>();
    double fit_control_yield = table_result[1]["Samples"][2]["Yield"].as<double>();
    double fit_control_error = table_result[1]["Samples"][2]["Error"].as<double>();

    sign->SetBinContent( ana_idx+1, sign_fit_signal_yield + sign_fit_control_yield );
    sign->SetBinError( ana_idx+1, std::sqrt( sign_fit_signal_error * sign_fit_signal_error
                        + sign_fit_control_error * sign_fit_control_error ) );

    bckg->SetBinContent( ana_idx+1, bckg_fit_signal_yield + bckg_fit_control_yield );
    bckg->SetBinError( ana_idx+1, std::sqrt( bckg_fit_signal_error * bckg_fit_signal_error
                        + bckg_fit_control_error * bckg_fit_control_error ) );

    data->SetBinContent( ana_idx+1, data_signal_yield + data_control_yield );
    data->SetBinError( ana_idx+1, std::sqrt( data_signal_yield + data_control_yield ) );

    fit->SetBinContent( ana_idx+1, fit_signal_yield + fit_control_yield );
    fit->SetBinError( ana_idx+1, std::sqrt( fit_signal_error * fit_signal_error
                        + fit_control_error * fit_control_error ) );

  }

  hist_group results( sign, bckg, data );
  return std::make_tuple( fit, std::move( results ) );

}

std::pair< hist_group, hist_group > trex_input_spec( const std::string & unique, const std::string & bin ){

  std::string filepath = std::string( std::getenv( "OUT_PATH" ) ) + "/trex/" 
                        + unique + "/" + bin + "/" + unique + "/Histograms/"
                        + unique + "_histos.root";
  TFile * hist_file = new TFile( filepath.c_str(), "READ" ); 
  std::pair< hist_group, hist_group > sr_cr = { 
    hist_group( static_cast< TH1F * >( hist_file->Get( "SIGNAL/sign/nominal/SIGNAL_sign" ) ),
                static_cast< TH1F * >( hist_file->Get( "SIGNAL/bckg/nominal/SIGNAL_bckg" ) ),
                static_cast< TH1F * >( hist_file->Get( "SIGNAL/data/nominal/SIGNAL_data" ) ) ),
    hist_group( static_cast< TH1F * >( hist_file->Get( "CONTROL/sign/nominal/CONTROL_sign" ) ),
                static_cast< TH1F * >( hist_file->Get( "CONTROL/bckg/nominal/CONTROL_bckg" ) ),
                static_cast< TH1F * >( hist_file->Get( "CONTROL/data/nominal/CONTROL_data" ) ) )
  };
  return sr_cr;
}

std::vector< std::pair< hist_group, hist_group > > trex_inputs_fullspec( const std::string & unique, variable_set & variables ){
    
  std::vector< std::string > analysis_bins = variables.analysis_bound.get_series_names(); 
  std::vector< std::pair< hist_group, hist_group > > inputs = {};
  std::transform( analysis_bins.begin(), analysis_bins.end(), std::back_inserter( inputs ), 
    [ &unique ]( const std::string & bin ){
      return trex_input_spec( unique, bin );
    }
  );
  return inputs;
}

// need error behaviour!!
std::pair< hist_group, hist_group> trex_inputs_ana_diff( const std::string & unique, variable_set & variables ){

  bound & ana_bound = variables.analysis_bound;
  std::vector< std::pair< hist_group, hist_group > > inputs = trex_inputs_fullspec( unique, variables );
  hist_group sr( ana_bound.get_hist(), ana_bound.get_hist(), ana_bound.get_hist() );
  hist_group cr( ana_bound.get_hist(), ana_bound.get_hist(), ana_bound.get_hist() );
  int bin = 1;
  std::for_each( inputs.begin(), inputs.end(), 
    [ &sr, &cr, &bin ]( std::pair< hist_group, hist_group > & input ){
      sr.sign_hist->SetBinContent( bin, input.first.sign_hist->Integral() ); 
      sr.bckg_hist->SetBinContent( bin, input.first.bckg_hist->Integral() );
      sr.data_hist->SetBinContent( bin, input.first.data_hist->Integral() );
      cr.sign_hist->SetBinContent( bin, input.first.sign_hist->Integral() );
      cr.bckg_hist->SetBinContent( bin, input.first.bckg_hist->Integral() );
      cr.data_hist->SetBinContent( bin, input.first.data_hist->Integral() );
      //sr.sign_hist->SetBinContent( bin, input.first.sign_hist->IntegralAndError() ); 
      //sr.bckg_hist->SetBinContent( bin, input.first.bckg_hist->IntegralAndError() );
      //sr.data_hist->SetBinContent( bin, input.first.data_hist->IntegralAndError() );
      //cr.sign_hist->SetBinContent( bin, input.first.sign_hist->IntegralAndError() );
      //cr.bckg_hist->SetBinContent( bin, input.first.bckg_hist->IntegralAndError() );
      //cr.data_hist->SetBinContent( bin, input.first.data_hist->IntegralAndError() );
      bin++;
    }
  );
  return { sr, cr };
}

hist_group combine_sc_group( const hist_group & sr, const hist_group & cr, variable_set & variables ){

  bound & ana_bound = variables.analysis_bound;
  
  hist_group combo = hist_group( variables.analysis_bound );
  for ( int bin = 1; bin <= ana_bound.get_bins(); bin++ ){
    combo.sign_hist->SetBinContent( bin, sr.sign_hist->GetBinContent( bin ) + cr.sign_hist->GetBinContent( bin ) );
    combo.bckg_hist->SetBinContent( bin, sr.bckg_hist->GetBinContent( bin ) + cr.bckg_hist->GetBinContent( bin ) );
    combo.sign_hist->SetBinContent( bin, sr.data_hist->GetBinContent( bin ) + cr.data_hist->GetBinContent( bin ) );
    double sr_sign_err = sr.sign_hist->GetBinError( bin );
    double sr_bckg_err = sr.bckg_hist->GetBinError( bin );
    double sr_data_err = sr.data_hist->GetBinError( bin );
    double cr_sign_err = cr.sign_hist->GetBinError( bin );
    double cr_bckg_err = cr.bckg_hist->GetBinError( bin );
    double cr_data_err = cr.data_hist->GetBinError( bin );
    combo.sign_hist->SetBinError( bin, std::sqrt( sr_sign_err*sr_sign_err + cr_sign_err*cr_sign_err ) );
    combo.bckg_hist->SetBinError( bin, std::sqrt( sr_bckg_err*sr_bckg_err + cr_bckg_err*cr_bckg_err ) );
    combo.data_hist->SetBinError( bin, std::sqrt( sr_data_err*sr_data_err + cr_data_err*cr_data_err ) );
  }
  return combo;
}

TH1F * combine_sc( TH1F * sr, TH1F * cr ){
  TH1F * combo = static_cast<TH1F *>( sr->Clone() ); 
  combo->Reset();
  for ( int bin = 1; bin <= combo->GetNbinsX(); bin++ ){
    combo->SetBinContent( bin, sr->GetBinContent( bin ) + cr->GetBinContent( bin ) );
    double sr_err = sr->GetBinError( bin );
    double cr_err = cr->GetBinError( bin );
    combo->SetBinContent( bin, std::sqrt( sr_err*sr_err + cr_err*cr_err ) );
  }
  return combo;
}

double rel_effect_systxt( const std::vector<std::string> & lines, const std::string & param ){
  std::regex param_regex( ".*" + param + ".*" );
  std::string line = "";
  std::for_each( lines.begin(), lines.end(), [&param_regex, &line]( const std::string & current_line ){
    if ( std::regex_search( current_line, param_regex ) ){ line = current_line; }
  } );
  if ( line.empty() ){ return 0.0; }
  std::vector<std::string> columns = {};
  split_strings( columns, line, "|" );
  std::vector<std::string> mc_sign = {};
  split_strings( mc_sign, columns.at(2), "/" );
  return std::stod( mc_sign[0], nullptr );
}

std::vector< double > get_effects( const std::string & filepath ){
  std::ifstream param_effect_file( filepath );
  std::vector< std::string > lines; 
  std::string line = "";
  while ( std::getline( param_effect_file, line ) ){ lines.push_back( line ); }
  return {
    rel_effect_systxt( lines, "photon_sf" ),
    rel_effect_systxt( lines, "msf" ),
    rel_effect_systxt( lines, "mr" )
  };
}

void add_pad( TH1F * hist, const std::string & name, const std::string & y_axis, bound & bound ){
  int current_pad_number = static_cast<TPad*>(gPad)->GetNumber();
  TPad * next_pad = static_cast<TPad*>( gPad->GetCanvas()->cd( ++current_pad_number ) );
  hist->Draw( "HIST E1" );
  hist_prep_axes(  hist, true );
  set_axis_labels( hist, bound.get_ltx().c_str(), y_axis.c_str() );
  add_pad_title( next_pad, name.c_str(), true );
  add_atlas_decorations( next_pad, true, false );
}

Float_t dphi_calc( const ROOT::RVecF &DiLept_Phi, const ROOT::RVecF &Photon_Phi ){
	Float_t dPhi = (DiLept_Phi[0] - Photon_Phi[0]); 
	while( dPhi > M_PI ){ dPhi -= 2*M_PI; };
	while( dPhi < -M_PI ){ dPhi += 2*M_PI; };
	return dPhi;
}

Float_t phi_calc( const ROOT::RVecF &MuMuGamma_CS_Phi ){
	return ( MuMuGamma_CS_Phi[0] >= 0 ) ? ( MuMuGamma_CS_Phi[0] - M_PI ) : ( MuMuGamma_CS_Phi[0] + M_PI );
}

Float_t photon_dr_calc( const ROOT::RVecF &Photon_Pt, const ROOT::RVecF &Photon_Eta, const ROOT::RVecF &Photon_Phi, const ROOT::RVecF &Photon_E,
											 const ROOT::RVecF &mcPhoton_Pt,  const ROOT::RVecF &mcPhoton_Eta, const ROOT::RVecF &mcPhoton_Phi ){
  TLorentzVector tlv_truth_photon,tlv_reco_photon,tlv_truth_muplus,tlv_reco_muplus;
  tlv_reco_photon.SetPtEtaPhiE( Photon_Pt[0], Photon_Eta[0], Photon_Phi[0], Photon_E[0] );
  tlv_truth_photon.SetPtEtaPhiM( mcPhoton_Pt[0], mcPhoton_Eta[0], mcPhoton_Phi[0], 0.0 );
  Float_t PhotonDRTruthReco  = tlv_reco_photon.DeltaR(tlv_truth_photon) ;
	return PhotonDRTruthReco;
}

// set backgrounds, margin, save, and clean up
void save_and_clear( TCanvas & canv, TPad * active_pad, std::string output_name ){

  canv.SetFillStyle( 4000 );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->RedrawAxis();
  output_name += ".pdf";
  canv.SaveAs( output_name.c_str() );
  canv.Clear();
}
