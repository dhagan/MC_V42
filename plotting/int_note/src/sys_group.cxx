#include <funcs.hxx>
#include <muonsf.hxx>
#include <sys/stat.h>


// output table of systematic group error data
void sys_group_err_table( std::string unique, TH1F * nominal, TH1F * max, TH1F * min, 
                      TH1F * group_error, TH1F * total, variable_set & variables ){

  // define row and column names, define table structure
  int bins = variables.analysis_bound.get_bins();
  std::string column_names = "$q_{T}^{A}$ & Yield & \\makecell{Maximum\\\\Variation} & \\makecell{Minium\\\\Variation}"
                             " & \\makecell{Stat\\\\error} & \\makecell{Group\\\\error} & \\makecell{Total\\\\Error}\\\\ \\hline\n";
  std::string table_layout = "|c|r|r|r|r|r|r|";

  // open file and write prelude.
  std::ofstream sys_table( unique + ".tex" );
  sys_table << "\\documentclass[landscape]{article}\n";
  sys_table << "\\usepackage[paperheight=4in,paperwidth=10in]{geometry}\n";
  sys_table << "\\usepackage{makecell}";
  sys_table << "\\begin{document}";
  sys_table << std::fixed << std::setprecision( 2 );
  sys_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{" << table_layout << "}\n\\hline\n";
  sys_table << column_names;

  // iterate over rows
  for ( int bin = 1; bin <= bins; bin++ ){ 

    // name row, add yield
    sys_table << std::to_string( bin ) << " & " << nominal->GetBinContent( bin ) << " & ";

    // stat_error, max and min, error
    
    sys_table << max->GetBinContent( bin ) << " & ";
    sys_table << min->GetBinContent( bin ) << " & ";
    sys_table << nominal->GetBinError( bin ) << " & ";
    sys_table << group_error->GetBinContent( bin ) << " & ";
    sys_table << total->GetBinError( bin );

    // next row
    sys_table << "\\\\ \\hline\n";
  }

  // finish document and close
  sys_table << "\\end{tabular}\n\\caption{Things}\n\\end{table}\n\\end{document}";
  sys_table.close();

}

// create table for systematic group yields
void sys_group_yield_table( std::string unique, TH1F * nominal, std::vector<std::string> sys_names, 
                            std::vector<TH1F *> sys_yields, variable_set & variables, TH1F * error ){

  // set up columns, rows, and structure
  int bins = variables.analysis_bound.get_bins();
  std::string column_names = "$q_{T}^{A}$ & Yield ";
  std::string table_layout = "|c|r|";
  for ( std::string & name : sys_names ){ 
    column_names += " & " + name;
    table_layout += "r|";
  }
  column_names += " & Error \\\\ \\hline\n";

  // open file and save prelude.
  std::ofstream sys_table( unique + "_yields.tex" );
  sys_table << "\\documentclass[landscape]{article}\n";
  sys_table << "\\usepackage[paperheight=4in,paperwidth=10in]{geometry}\n";
  sys_table << "\\usepackage{makecell}";
  sys_table << "\\begin{document}";
  sys_table << std::fixed << std::setprecision( 2 );
  sys_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{" << table_layout << "}\n\\hline\n";
  sys_table << column_names;

  for ( int bin = 1; bin <= bins; bin++ ){ 
    // name row, add yield
    sys_table << std::to_string( bin ) << " & " << nominal->GetBinContent( bin );
    // add systematics yields
    for ( TH1F * sys : sys_yields ){
      sys_table << " & " << sys->GetBinContent( bin ) - nominal->GetBinContent( bin);
    }
    // next row
    sys_table << "&" << error->GetBinContent( bin ) << "\\\\ \\hline\n";
  }

  // finish documenbt and close
  sys_table << "\\end{tabular}\n\\caption{Things}\n\\end{table}\n\\end{document}";
  sys_table.close();

}


/*
* A function for getting the systematics only error returns a histogram with only the relative errors as the bin content.
* @param Unique string of the nominal to derive error from.
* @param Unique strings of the systematic variations to calculate errors.
* @param Variables object, contains limits and binnings of variables in analysis. Defaults to false, returns relative error.
*/
TH1F * sys_group_err( std::string & nominal_unique, 
  std::vector<std::string> & systematics, variable_set & variables, bool absolute=false ){

  // prepare vecs for extracted results
  std::vector< hist_group > extracted_results;
  std::vector< TH1F * > extracted_sys_sign;
  extracted_results.reserve( systematics.size() );
  extracted_sys_sign.reserve( systematics.size() );

  // get the extracted yields of each systematic
  for ( std::string & sys_unique : systematics ){
    auto[ total_yield, extracted ] = get_fit( sys_unique, variables );
    extracted_results.push_back( std::move( extracted ) );
  }

  // get the extracted yield of the nominal
  auto[ nominal_total, nominal_fits ] = get_fit( nominal_unique, variables );
  TH1F * nominal_sign = nominal_fits.sign_hist;

  // create two histograms, maximal and minimal variation numbers
  TH1F * max_hist = variables.analysis_bound.get_hist( "max_hist" );
  TH1F * min_hist = static_cast<TH1F *>( nominal_sign->Clone( "min_hist" ) );

  // this does a finding max and min for each bin over all variations
  std::for_each( 
    extracted_results.begin(), extracted_results.end(),
    [ &max_hist, &min_hist, &extracted_sys_sign ]( hist_group & results ){
      extracted_sys_sign.push_back( results.sign_hist );
      for ( int bin_idx = 1; bin_idx <= max_hist->GetNbinsX(); bin_idx++ ){
        if ( results.sign_hist->GetBinContent( bin_idx ) >= max_hist->GetBinContent( bin_idx ) ){
          max_hist->SetBinContent( bin_idx, results.sign_hist->GetBinContent( bin_idx ) );
        }
        if ( results.sign_hist->GetBinContent( bin_idx) < min_hist->GetBinContent( bin_idx ) ){
          min_hist->SetBinContent( bin_idx, results.sign_hist->GetBinContent( bin_idx ) );
        }
      }
    }
  );

  // calcuate the error associated with the max and min deviation
  // store this in a histogram, bin content as the relative error
  TH1F * sys_err_hist = static_cast< TH1F * >( nominal_sign->Clone() );
  for ( int bin = 1; bin <= max_hist->GetNbinsX(); bin++ ){
    double sys_err = uniform_error( max_hist->GetBinContent( bin ), min_hist->GetBinContent( bin ) );
    sys_err_hist->SetBinContent( bin, (absolute) ? sys_err : sys_err/nominal_sign->GetBinContent( bin ) );
  }

  //return the relative error hist
  return sys_err_hist;
}



void sys_group( YAML::Node & run_node, variable_set & variables ){


  mkdir( "./sys_group", 744 );
  chdir( "./sys_group" );

  std::string unique = run_node[ "output_unique" ].as<std::string>();
  std::vector<std::string> systematics = run_node[ "systematics"].as<std::vector<std::string>>();

  std::string base_unique = run_node[ "base_unique" ].as<std::string>();

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string efficiency_path = std::string( std::getenv( "OUT_PATH" ) ) + "/trees/";
  std::string efficiency_unique = run_node["eff_unique"].as<std::string>();
  efficiency_path += efficiency_unique + "/sign_efficiencies_" + efficiency_unique + ".root";
  basic_fileset * fileset = new basic_fileset();
  fileset->load_efficiency_fileset( efficiency_path, "" );

  std::vector< hist_group > extracted_results;
  std::vector< TH1F * > extracted_sys_sign;
  std::vector< TH1F * > total_fit_yields;
  std::vector< TH1F * > sign_abs_differences;
  std::vector< TH1F * > sign_rel_errs;
  extracted_results.reserve( systematics.size() );
  extracted_sys_sign.reserve( systematics.size() );

  for ( std::string & sys_unique : systematics ){
    auto[ total_yield, extracted ] = get_fit( sys_unique, variables );
    extracted_results.push_back( std::move( extracted ) );
    total_fit_yields.push_back( total_yield );
  }

  auto[ baseline_total, baseline_extracted ] = get_fit( base_unique, variables );

  TH1F * max_hist = variables.analysis_bound.get_hist( "max_hist" );
  TH1F * min_hist = static_cast<TH1F *>( baseline_extracted.sign_hist->Clone( "min_hist" ) );
  std::for_each( 
    extracted_results.begin(), extracted_results.end(),
    [ &max_hist, &min_hist, &extracted_sys_sign ]( hist_group & results ){
      extracted_sys_sign.push_back( results.sign_hist );
      for ( int bin_idx = 1; bin_idx <= max_hist->GetNbinsX(); bin_idx++ ){
        if ( results.sign_hist->GetBinContent( bin_idx ) >= max_hist->GetBinContent( bin_idx ) ){
          max_hist->SetBinContent( bin_idx, results.sign_hist->GetBinContent( bin_idx ) );
        }
        if ( results.sign_hist->GetBinContent( bin_idx) < min_hist->GetBinContent( bin_idx ) ){
          min_hist->SetBinContent( bin_idx, results.sign_hist->GetBinContent( bin_idx ) );
        }
      }
    }
  );

  TH1F * stat_hist = static_cast< TH1F * >( baseline_extracted.sign_hist->Clone( "stat_hist" ) );
  TH1F * sys_err_hist = static_cast< TH1F * >( baseline_extracted.sign_hist->Clone( "sys_hist" ) );
  sys_err_hist->Reset();
  TH1F * errorbar_hist = static_cast< TH1F * >( baseline_extracted.sign_hist->Clone( "errorbar_hist" ) );
  TH1F * rel_hist = static_cast< TH1F * >( baseline_extracted.sign_hist->Clone( "rel_hist" ) );
  //TH1F * rel_hist->Reset();
  for ( int bin_idx = 1; bin_idx <= max_hist->GetNbinsX(); bin_idx++ ){
    double uni_error = uniform_error( max_hist->GetBinContent( bin_idx ), min_hist->GetBinContent( bin_idx ) );
    sys_err_hist->SetBinContent( bin_idx, uni_error );
    rel_hist->SetBinContent( bin_idx, sys_err_hist->GetBinContent( bin_idx )/stat_hist->GetBinContent( bin_idx ) );
    errorbar_hist->SetBinError( bin_idx, absolute_quadrature_two( uni_error, errorbar_hist->GetBinError( bin_idx ) ) ); 
  }
  
  // awful parameter names 
  sys_group_err_table( unique, stat_hist, max_hist, min_hist, sys_err_hist, errorbar_hist, variables );

  // also awful param names
  sys_group_yield_table( unique, stat_hist, systematics, extracted_sys_sign, variables, sys_err_hist );

  min_hist->SetLineColorAlpha( kRed+2, 1.0 );
  max_hist->SetLineColorAlpha( kRed+2, 1.0 );

  stat_hist->SetLineColor( kBlack );
  stat_hist->SetMarkerStyle( 20 );
  stat_hist->SetMarkerColor( kBlack );
  errorbar_hist->SetLineColor( kRed );
  errorbar_hist->SetLineStyle( 1 );

  baseline_extracted.sign_hist->SetLineColor( kBlack );
  baseline_extracted.sign_hist->SetMarkerStyle( 20 );

  TCanvas canv = TCanvas( "sc", "",  200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TPad* active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  baseline_extracted.sign_hist->Draw( "HIST P" );
  baseline_extracted.sign_hist->Draw( "HIST SAME" );
  max_hist->Draw( "HIST SAME" );
  min_hist->Draw( "HIST SAME" );
  hist_prep_axes( baseline_extracted.sign_hist, true );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( baseline_extracted.sign_hist, "q_{T}^{A} [GeV]", "Yield" );
  //add_pad_title( active_pad, "Nominal, maximal deviations", true );
  TLegend * var_leg = below_logo_legend();
  var_leg->AddEntry( baseline_extracted.sign_hist, "Nominal" );
  var_leg->AddEntry( max_hist, "Maximal Variation" );
  var_leg->AddEntry( min_hist, "Minimal Variation" );
  var_leg->Draw();
  save_and_clear( canv, active_pad, "maxmin_" + unique );

  active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  errorbar_hist->Draw( "HIST E1" );
  stat_hist->Draw( "SAME HIST E1" );
  hist_prep_axes( errorbar_hist, true );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( errorbar_hist, "q_{T}^{A} [GeV]", "Yield" );
  TLegend * stat_leg = below_logo_legend();
  stat_leg->AddEntry( stat_hist, "Statistical Error" );
  stat_leg->AddEntry( errorbar_hist, "Stat+Sys Error" );
  stat_leg->Draw();
  save_and_clear( canv, active_pad, "sysstat_" + unique ); 

  for( auto hg : extracted_results ){ hg.erase(); }
  extracted_results.clear();
  sign_abs_differences.clear();
  delete baseline_total;
  baseline_extracted.erase();
  delete max_hist;
  delete min_hist;

  chdir( ".." );


}

  //TPad * active_pad = static_cast<TPad*>( sys_canv->cd( 1 ) );
  //zero->Draw( "HIST" );
  //hist_prep_axes( zero );
  //zero->GetYaxis()->SetRangeUser( -400, 600 );
  //add_atlas_decorations( active_pad, true );
  //set_axis_labels( zero, "q_{T}^{A}", "#Delta_{absolute}" );
  //add_pad_title( active_pad, "Postfit, absolute deviation", false );
  //TLegend * abs_leg = below_logo_legend();
  ////abs_leg->SetY2NDC( 0.4 );
  //int step = 0;
  //std::for_each(   
  //  sign_abs_differences.begin(), sign_abs_differences.end(), 
  //  [ &step, &color_itr, &abs_leg ]( TH1F * hist ){  
  //    hist->Draw( "HIST SAME P" ); 
  //    hist->Draw( "HIST SAME" ); 
  //    hist->SetLineColorAlpha( *color_itr+step, 1.0 );
  //    hist->SetLineStyle( 1 );
  //    hist->SetLineWidth( 1 );
  //    hist->SetMarkerStyle( step+2 );
  //    hist->SetMarkerColor( *color_itr+step );
  //    abs_leg->AddEntry( hist, hist->GetName() );
  //    step++;
  //    if ( step >= 4 ){ step = 0; color_itr++; }
  //  }
  //);
  //abs_leg->Draw();

  //TH1F * zero = new TH1F( "zero", "", 15, -10, 20 );
  //for ( int bin = 1; bin <= 15; bin++ ){ zero->SetBinContent(bin, 0.0 ); }
  //zero->SetLineColor( kBlack );
  //std::vector< int > colors = { kRed, kBlue, kGreen, kMagenta };
  //auto color_itr = colors.begin();

  //TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  //canv.Divide( 1, 1 );
  //canv.cd( 1 );
  //active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  ////BDT_l20->Draw( "HIST" );
  //add_atlas_decorations( active_pad, false, false );
  //add_internal( active_pad );
  ////set_axis_labels( BDT_l20, x_string.c_str(), y2_string.c_str() );
  //hist_prep_axes( BDT_l20, true );
  //active_pad->SetBottomMargin( 0.1 );
  //active_pad->SetFillStyle( 4000 );
  //canv.SetFillStyle( 4000 );
  //canv.SaveAs( "BDT_l20.pdf" );
  //canv.Clear(); 

  //active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  //errorbar_hist->Draw( "HIST E1" );
  //hist_prep_axes( errorbar_hist );
  //add_atlas_decorations( active_pad, false );
  //add_internal( active_pad );
  //set_axis_labels( errorbar_hist, "q_{T}^{A}", "Yield" );
  //TLegend * err_leg = below_logo_legend();
  //err_leg->Draw();
  //save_and_clear( canv, active_pad, "sysstat_" + unique );
