#include <funcs.hxx>
#include <threefits.hxx>
#include <sys/stat.h>
#include <TMath.h>
#include <TFitResult.h>

void threefits( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./threefits", 744 );
  chdir( "./threefits" );

  std::string unique = run_node[ "unique" ].as<std::string>();
  auto [ base_total, base_extracted ] = get_fit( unique, variables );
  TH1F * sign_h = base_extracted.sign_hist;

  std::string eff_unique, eff_filepath;
  if ( run_node[ "eff_unique" ] ){ 
      eff_unique = run_node[ "eff_unique" ].as< std::string >();
      eff_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
        + "/trees/" + eff_unique
        + "/efficiency/sign_efficiencies_" 
        + eff_unique + ".root";
  }
  basic_fileset * eff_fileset = new basic_fileset();
  eff_fileset->set_unique( eff_unique );
  eff_fileset->load_efficiency_fileset( eff_filepath, eff_unique );

  TH1F * efficiency = eff_fileset->get_efficiency( variables );
  sign_h->Divide( sign_h, efficiency, 1.0, 1.0 );

  sign_h->SetLineColor( kBlack );
  sign_h->SetMarkerColor( kBlack );
  sign_h->SetMarkerColor( kBlack );

  TF1 * single_gauss = new TF1( "sg", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -5, 13 );
  single_gauss->SetParName( 0, "c"); 
  single_gauss->SetParName( 1, "mean" ); 
  single_gauss->SetParName( 2, "#sigma" );
  TF1 * double_gauss = new TF1( "dg", "[0]*e^(( -( ([1]-x) * ([1] - x ) ) )/(2*([2]*[2])))"
                                "+ [3]*e^(( -( ([1]-x) * ([1] - x ) ) ) / (2*([4]*[4])))", 
                                -10, 20 );
  double_gauss->SetParName( 0, "c_{1}"); 
  double_gauss->SetParName( 1, "mean" ); 
  double_gauss->SetParName( 2, "#sigma_{1}" );
  double_gauss->SetParName( 3, "c_{2}"); 
  double_gauss->SetParName( 4, "#sigma_{2}" );
  TF1 * sincst_gauss = new TF1( "sc", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2))) + [3]", -10, 20 );
  sincst_gauss->SetParName( 0, "c" ); 
  sincst_gauss->SetParName( 1, "mean" ); 
  sincst_gauss->SetParName( 2, "#sigma" );
  sincst_gauss->SetParName( 3, "const." );

  align_dg( double_gauss, sign_h );
  align_sg( single_gauss, sign_h );
  align_sg( sincst_gauss, sign_h );

  TF1 * gauss_breit = new TF1( "sg", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2))) "
   " + [3]*TMath::BreitWigner( x, [1], [4])", -10.0, 20.0 );
  gauss_breit->SetParName( 0, "c"); 
  gauss_breit->SetParName( 1, "mean" ); 
  gauss_breit->SetParName( 2, "#sigma" );
  gauss_breit->SetParName( 3, "cbw"); 
  gauss_breit->SetParName( 4, "#gamma"); 
  align_sg( gauss_breit, sign_h );
  gauss_breit->SetParameter( 3, gauss_breit->GetParameter( 0 )/5.0f );


  Int_t orng = TColor::GetColor( 186, 45, 11 );
  Int_t ochre = TColor::GetColor( 219, 124, 38 );
  TF1 * g1 = new TF1( "g1", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 ); 
  g1->SetLineColor( orng );
  g1->SetLineStyle( 2 );
  TF1 * g2 = new TF1( "g2", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  g2->SetLineColor( ochre );
  g2->SetLineStyle( 2 );

  TF1 * g3 = new TF1( "g3", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  g3->SetLineColor( orng );
  g3->SetLineStyle( 2 );

  TF1 * c1 = new TF1( "c1", "[0]", -10, 20 );
  c1->SetLineColor( ochre );
  c1->SetLineStyle( 2 );

  TF1 * g4 = new TF1( "g4", "[0]*e^((-([1]-x)^(2))/(2*([2])^(2)))", -10, 20 );
  g4->SetLineColor( orng );
  g4->SetLineStyle( 2 );
  
  TF1 * b1 = new TF1( "b1", "[0]*TMath::BreitWigner( x, [1], [2] )", -10, 20 );
  b1->SetLineColor( ochre );
  b1->SetLineStyle( 2 );

  std::string x_string = "q_{T}^{A} [GeV]";
  std::string y_string = "Entries/2.0";

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  sign_h->Draw( "HIST E1");
  sign_h->Fit( single_gauss, "MQ", "", -7, 12 );
  single_gauss->Draw( "SAME" ); 
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_h, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( sign_h, true );
  TPaveStats * stats1 = make_stats( sign_h );
  stats1->Draw( "SAME" );
  save_and_clear( canv, active_pad, "single_gauss" );

  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  sign_h->Draw( "HIST E1");
  sign_h->Fit( double_gauss, "MQ", "", -7, 12 );
  double_gauss->Draw( "SAME" );
  g1->SetParameter( 0, double_gauss->GetParameter( 0 ) ); 
  g1->SetParameter( 1, double_gauss->GetParameter( 1 ) );
  g1->SetParameter( 2, double_gauss->GetParameter( 2 ) );
  g2->SetParameter( 0, double_gauss->GetParameter( 3 ) );
  g2->SetParameter( 1, double_gauss->GetParameter( 1 ) );
  g2->SetParameter( 2, double_gauss->GetParameter( 4 ) );
  g1->Draw( "SAME" );
  g2->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_h, x_string.c_str(), y_string.c_str() );
  TPaveStats * stats2 = make_stats( sign_h );
  stats2->Draw( "SAME" );
  save_and_clear( canv, active_pad, "double_gauss" );

  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  sign_h->Draw( "HIST E1");
  sign_h->Fit( sincst_gauss, "MQ", "", -7, 12 );
  sincst_gauss->Draw( "SAME" );
  g3->SetParameter( 0, sincst_gauss->GetParameter( 0 ) ); 
  g3->SetParameter( 1, sincst_gauss->GetParameter( 1 ) );
  g3->SetParameter( 2, sincst_gauss->GetParameter( 2 ) );
  c1->SetParameter( 0, sincst_gauss->GetParameter( 3 ) );
  g3->Draw( "SAME" );
  c1->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_h, x_string.c_str(), y_string.c_str() );
  TPaveStats * stats3 = make_stats( sign_h );
  stats3->Draw( "SAME" );
  save_and_clear( canv, active_pad, "sincst_gauss" );


  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  sign_h->Draw( "HIST E1");
  sign_h->Fit( gauss_breit, "MQ", "", -10.0, 20.0 );
  gauss_breit->Draw( "SAME" );
  g4->SetParameter( 0, gauss_breit->GetParameter( 0 ) );  
  g4->SetParameter( 1, gauss_breit->GetParameter( 1 ) );
  g4->SetParameter( 2, gauss_breit->GetParameter( 2 ) );
  g4->Draw( "SAME" );
  b1->SetParameter( 0, gauss_breit->GetParameter( 3 ) );  
  b1->SetParameter( 1, gauss_breit->GetParameter( 1 ) );  
  b1->SetParameter( 2, gauss_breit->GetParameter( 4 ) );  
  b1->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_h, x_string.c_str(), y_string.c_str() );
  TPaveStats * stats4 = make_stats( sign_h );
  stats4->Draw( "SAME" );
  TLegend * legend4 = below_logo_legend();
  legend4->AddEntry( gauss_breit, "Gauss+Breit", "L" );
  legend4->AddEntry( g4, "Gauss", "F" );
  legend4->AddEntry( b1, "Breit", "F" );
  legend4->Draw( "SAME" );
  save_and_clear( canv, active_pad, "gauss_breit" );



  chdir( ".." );


}
