#include <mig.hxx>
#include <sys/stat.h>

struct result{
  double x;
  double sigma;
  double x_err;
  double sigma_err;
};

void fit_plot( const std::vector<result> & results ){

  TH1F * mean = new TH1F( "mean", "", 60, -10, 20 );
  TH1F * sigma = new TH1F( "sigma", "", 60, -10, 20 );

  for (int bin = 0; bin < 60; bin++ ){
    mean->SetBinContent(bin+1, results.at(bin).x );
    mean->SetBinError( bin + 1, results.at(bin).x_err );
    sigma->SetBinContent(bin+1, results.at(bin).sigma );
    sigma->SetBinError( bin + 1, results.at(bin).sigma_err );
  }

  TF1 * line = prep_line( -10.0, 20.0 );
  
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TPad * active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  mean->Draw( "HIST E1" );
  mean->Fit( line, "M", "", -10.0, 20.0 );
  mean->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( mean, "Mean Migration q_{T}^{A} [GeV]", "q_{T}^{A} Truth [GeV]" );
  hist_prep_axes( mean, false );
  TPaveStats * meanstat = make_stats( mean );
  meanstat->Draw( "SAME" );
  save_and_clear( canv, active_pad, "mean" );

  active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  sigma->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sigma, "#sigma migration [GeV]", "q_{T}^{A} Truth [GeV]" );
  hist_prep_axes( sigma, false );
  save_and_clear( canv, active_pad, "sigma" );




}

result proj_fit( TTree * mig_tree, std::string cut, int bin ){

  TH2F * mig_hist = new TH2F( "mig_hist" , "", 60, -10, 20, 60, -10, 20 );
  mig_tree->Draw( "re_qta:tr_qta>>mig_hist", cut.c_str(), "e goff" );
  TH1F * reco_hist = (TH1F*) mig_hist->ProjectionY();

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TF1 * fit = prep_sg( -10, 20.0 );
  align_sg( fit, reco_hist );

  TPad * active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  mig_hist->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( mig_hist, "q_{T}^{A} Truth [GeV]", "q_{T}^{A} Reco [GeV]" );
  hist_prep_axes( mig_hist, false );
  mig_hist->GetYaxis()->SetRangeUser( -10.0, 20.0 );
  std::string output_name = "migration_qta" + std::to_string( bin+1 );
  save_and_clear( canv, active_pad, output_name );

  active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  reco_hist->SetLineColor( kBlue+1 );
  reco_hist->Draw( "HIST E1" );
  reco_hist->Fit( fit, "MQ", "" );
  fit->SetNpx( 1000 );
  fit->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( reco_hist, "Reco Events", "q_{T}^{A} [GeV]" );
  hist_prep_axes( reco_hist, false );
  TPaveStats * fitstat = make_stats( reco_hist );
  fitstat->Draw( "SAME" );
  output_name = "reco_qta" + std::to_string( bin+1 );
  save_and_clear( canv, active_pad, output_name );


  delete reco_hist;
  delete mig_hist;
  return result{ fit->GetParameter(1), fit->GetParameter( 2 ), fit->GetParError( 1 ), fit->GetParError( 2 ) };
}

void all_migration( TTree * mig_tree, const std::string & cut ){

  TH2F * mig_hist = new TH2F( "mig_hist_all" , "", 60, -10, 20, 60, -10, 20 );
  mig_tree->Draw( "re_qta:tr_qta>>mig_hist_all" , cut.c_str() );
  std::cout << cut << std::endl;
  std::cout << mig_hist->Integral() << std::endl;
  TH1D * reco_hist  = static_cast<TH1D*>( mig_hist->ProjectionY() );
  TH1D * truth_hist = static_cast<TH1D*>( mig_hist->ProjectionX() );
  TH1D * ratio_hist = static_cast<TH1D*>( reco_hist->Clone() );
  ratio_hist->Reset();
  ratio_hist->Divide( reco_hist, truth_hist, 1.0, 1.0, "B" );
  
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TPad * active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  mig_hist->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( mig_hist, "q_{T}^{A} Truth [GeV]", "q_{T}^{A} Reco [GeV]" );
  hist_prep_axes( mig_hist, false );
  mig_hist->GetYaxis()->SetRangeUser( -10.0, 20.0 );
  save_and_clear( canv, active_pad, "migration" );

  gStyle->SetOptStat( "rme" );

  active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  reco_hist->SetLineColor( kBlue+1 );
  reco_hist->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( reco_hist, "Reco Events", "q_{T}^{A} [GeV]" );
  hist_prep_axes( reco_hist, true );
  TPaveStats * reco_stats = make_stats( reco_hist );
  reco_stats->Draw( "SAME" );
  save_and_clear( canv, active_pad, "reco" );

  active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  truth_hist->SetLineColor( kRed+1 );
  truth_hist->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( truth_hist, "Truth Events", "q_{T}^{A} [GeV]" );
  hist_prep_axes( truth_hist, true );
  TPaveStats * truth_stats = make_stats( truth_hist );
  truth_stats->Draw( "SAME" );
  save_and_clear( canv, active_pad, "truth" );

  gStyle->SetOptStat( "" );
  active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  ratio_hist->SetLineColor( kBlack );
  ratio_hist->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( ratio_hist, "Reco/Truth", "q_{T}^{A} [GeV]" );
  hist_prep_axes( ratio_hist, false );
  save_and_clear( canv, active_pad, "ratio" );

}

void mig( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./mig", 744 );
  chdir( "./mig" );

  std::cout << variables.analysis_variable << std::endl;

  std::string unique = run_node["unique"].as<std::string>();
  std::string mig_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
    + "/trees/" + unique + "/migration/sign_" 
    + unique + "_migration.root";

  TFile * mig_file = new TFile( mig_filepath.c_str(), "READ" );
  TTree * mig_tree = static_cast<TTree*>( mig_file->Get( "sign_migration" ) );

  
  std::string core_cut = "Lambda>15&&Lambda<200"
    "&&(truth_pass>=1.0)&&(reco_pass>=1.0)"
    "&&(re_qta>-10.0)&&(re_qta<20.0)";

  double lower = -10.0;
  double width = 0.5;

  std::vector< result > results;
  results.reserve( 60 );

  all_migration( mig_tree, core_cut );

  for ( int bin = 0; bin < 60; bin++ ){

    std::string window_cut = core_cut + "&&(tr_qta>" + std::to_string( lower+(bin*width)) + ")"
      + "&&(tr_qta<" + std::to_string( lower+((1+bin)*width)) + ")";

    results.push_back( proj_fit( mig_tree, window_cut, bin ) );

  }

  fit_plot( results );

  chdir( ".." );

}
