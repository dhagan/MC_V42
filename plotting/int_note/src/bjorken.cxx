#include <mig.hxx>
#include <sys/stat.h>

void bjorken( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./bjorken", 744 );
  chdir( "./bjorken" );

  std::string unique = run_node["unique"].as<std::string>();
  std::string efficiency_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
    + "/trees/" + unique + "/efficiency/sign_efficiencies_" 
    + unique + ".root";

  std::cout << variables.analysis_variable << std::endl;

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_efficiency_fileset( efficiency_filepath, unique );

  TTree * reco_tree = fileset->eff_reco_tree;

  double bin_edges[14] = { 5.62e-5, 1e-4, 1.778e-4, 3.16e-4, 5.62e-4, 1e-3, 1.778e-3, 3.16e-3, 5.62e-3, 1e-2, 1.778e-2, 3.16e-2, 5.62e-2, 1.0e-1 };

  TH2F * compare_bjorken = new TH2F( "compare_bjorken", "", 13, bin_edges, 13, bin_edges );
  TH2F * bjorken_components = new TH2F( "bjorken_components", "", 30, -3, 3, 60, 0.0005, 0.004 );
  //reco_tree->Draw( "Q/13000:Y>>bjorken_components", "abs(Y)<2.5&&(DiMuonMass>3000&&DiMuonMass<3200)" );
  //reco_tree->Draw( "(Q/13000)*exp(Y):(Q/13000)*exp(-Y)>>compare_bjorken", "abs(Y)<2.5&&(DiMuonMass>3000&&DiMuonMass<3200)" );
  reco_tree->Draw( "Q/13000:Y>>bjorken_components", "abs(Y)<2.5" );
  reco_tree->Draw( "(Q/13000)*exp(Y):(Q/13000)*exp(-Y)>>compare_bjorken", "abs(Y)<2.5" );
  TH1D * rapidity = bjorken_components->ProjectionX();
  TH1D * qs = bjorken_components->ProjectionY();

  rapidity->SetLineWidth( 0 );
  rapidity->SetFillStyle(1001);
  rapidity->SetFillColor( rapsberry );
  qs->SetLineWidth( 0 );
  qs->SetFillStyle(1001);
  qs->SetFillColor( rapsberry );


  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  compare_bjorken->Draw( "COLZ" );
  active_pad->SetLogx();
  active_pad->SetLogy();
  hist_prep_axes( compare_bjorken, true );
  set_axis_labels( compare_bjorken, "x_{2}", "x_{1}" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  save_and_clear( canv, active_pad, "bjorken_map" );


  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  bjorken_components->Draw( "COLZ" );
  hist_prep_axes( bjorken_components, true );
  set_axis_labels( bjorken_components, "y", "Q/#sqrt{s}" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( bjorken_components, true );
  save_and_clear( canv, active_pad, "bjorken_components" );
  
  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  rapidity->Draw( "HIST E1" );
  set_axis_labels( rapidity, "y", "Entries" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( rapidity, true );
  TLegend * rap_legend = create_atlas_legend();
  rap_legend->AddEntry( rapidity, "MC Signal" );
  rap_legend->Draw( "SAME" );
  rap_legend->SetY1( 0.7 );
  save_and_clear( canv, active_pad, "rap" );

  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  qs->GetXaxis()->SetMaxDigits( 3 );
  qs->Draw( "HIST E1" );
  set_axis_labels( qs, "Q/#sqrt{s}", "Entries" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( qs, true );
  TLegend * qs_legend = create_atlas_legend();
  qs_legend->AddEntry( rapidity, "MC Signal" );
  qs_legend->Draw( "SAME" );
  qs_legend->SetY1( 0.7 );
  save_and_clear( canv, active_pad, "qrts" );

  chdir( ".." );

}
