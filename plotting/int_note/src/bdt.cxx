#include <bdt.hxx>
#include <sys/stat.h>

void delta_2d( YAML::Node & run_node, variable_set & variables ){

  std::string filepath = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  filepath += "/scalefactors/" + unique; 

  std::vector< std::string > mass_bounds = { "Q12", "Q3", "Q4", "Q5" };

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * sign_tree = fileset->get_tree( sign );
  TTree * bckg_tree = fileset->get_tree( bckg );

  gStyle->SetOptStat( "" );

 
  for ( std::string & mass : mass_bounds ){

    TH2F * sign = new TH2F( "sign", "", 40, 0.0, M_PI, 
                                        40, -4.0, 4.0 );

    TH2F * bckg = new TH2F( "bckg", "", 40, 0.0, M_PI,  
                                        40, -4.0, 4.0 );

    bound & mass_bound = variables.bound_manager->get_bound( mass );
    std::cout << mass_bound.get_cut() << std::endl;
    sign_tree->Draw( "DY:AbsdPhi>>sign", mass_bound.get_cut().c_str(), "e goff" );
    bckg_tree->Draw( "DY:AbsdPhi>>bckg", mass_bound.get_cut().c_str(), "e goff" );

    std::string x_string = "|#Delta#Phi|";
    std::string y_string = "#Delta y";
    TPad * active_pad;


    TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
    canv.Divide( 1 );

    x_string = "|#Delta#Phi|";
    y_string = "#Delta Y";

    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    sign->Draw( "COLZ" ); 
    add_internal( active_pad );
    set_axis_labels( sign, x_string.c_str(), y_string.c_str() );
    save_and_clear( canv, active_pad, "sign_dPhidY");

    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    canv.SetFillStyle( 4000 );
    active_pad->SetFillStyle( 4000 );
    active_pad->SetBottomMargin( 0.1 );
    bckg->Draw( "COLZ" );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( bckg, x_string.c_str(), y_string.c_str() );
    save_and_clear( canv, active_pad, "bckg_dPhidY");

    delete sign;
    delete bckg;

  }
  
  /*
  You've got DY:AbsPhi
  Need 
  Lambda:qTSquared
  qxpsi:qypsi
  qxgamma:qygamma
  qxsum:qysum
  */


  bound & mass_bound = variables.bound_manager->get_bound( "Q12" );
  std::vector< std::pair<std::string, std::string> > vars = { {"Lambda", "qTSquared" }, {"qxpsi", "qypsi"}, { "qxgamma", "qygamma" }, { "qxsum", "qysum" } };

  for ( auto var : vars ){

    bound & x_bound = variables.bound_manager->get_bound( var.first );
    bound & y_bound = variables.bound_manager->get_bound( var.second );
    std::string draw_sign = var.second + ":" + var.first + ">>sign";
    std::string draw_bckg = var.second + ":" + var.first + ">>bckg";

    TH2F * sign = new TH2F( "sign", "", 40, x_bound.get_min(), x_bound.get_max(), 
                                        40, y_bound.get_min(), y_bound.get_max() );
    
    TH2F * bckg = new TH2F( "bckg", "", 40, x_bound.get_min(), x_bound.get_max(), 
                                        40, y_bound.get_min(), y_bound.get_max() );

    sign_tree->Draw( draw_sign.c_str(), mass_bound.get_cut().c_str(), "e goff" );
    bckg_tree->Draw( draw_bckg.c_str(), mass_bound.get_cut().c_str(), "e goff" );
  
    TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
    canv.Divide( 1 );


    TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    sign->Draw( "COLZ" ); 
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( sign, x_bound.get_ltx() + " [" +x_bound.get_units()+"]", y_bound.get_ltx() + " [" +y_bound.get_units()+"]");
    save_and_clear( canv, active_pad, "sign_" + var.first+ var.second);

    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    bckg->Draw( "COLZ" );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( bckg, x_bound.get_ltx() + " [" +x_bound.get_units()+"]", y_bound.get_ltx() + " [" +y_bound.get_units()+"]");
    save_and_clear( canv, active_pad, "bckg_" + var.first+ var.second);


    delete sign;
    delete bckg;
  }


}

void delta_1d( YAML::Node & run_node, variable_set & variables ){
  std::string filepath = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  filepath += "/scalefactors/" + unique; 

  std::vector< std::string > mass_bounds = { "Q12", "Q3", "Q4", "Q5" };

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * sign_tree = fileset->get_tree( sign );
  TTree * bckg_tree = fileset->get_tree( bckg );

  for ( std::string & mass : mass_bounds ){

    TH1F * sign_dphi = new TH1F( "sign_dphi", "", 30, 0.0, M_PI );
    TH1F * bckg_dphi = new TH1F( "bckg_dphi", "", 30, 0.0, M_PI );
    TH1F * sign_dy = new TH1F( "sign_dy", "", 30, -4.0, 4.0 );
    TH1F * bckg_dy = new TH1F( "bckg_dy", "", 30, -4.0, 4.0 );

    bound & mass_bound = variables.bound_manager->get_bound( mass );
    std::cout << mass_bound.get_cut() << std::endl;
    sign_tree->Draw( "DY>>sign_dy", mass_bound.get_cut().c_str(), "e goff" );
    bckg_tree->Draw( "DY>>bckg_dy", mass_bound.get_cut().c_str(), "e goff" );
    sign_tree->Draw( "AbsdPhi>>sign_dphi", mass_bound.get_cut().c_str(), "e goff" );
    bckg_tree->Draw( "AbsdPhi>>bckg_dphi", mass_bound.get_cut().c_str(), "e goff" );

    std::string dy_x = "#Delta y";
    std::string dy_y = "Entries/0.2";
    std::string dphi_x = "|#Delta#phi|";
    std::string dphi_y = "Entries/(#pi/40)";
    std::string savename;
    savename.reserve( 50 ); 

    sign_dphi->SetMarkerStyle( 20 );
    bckg_dphi->SetMarkerStyle( 20 );
    sign_dy->SetMarkerStyle( 20 );
    bckg_dy->SetMarkerStyle( 20 );
    sign_dphi->SetMarkerColor( 1 );
    bckg_dphi->SetMarkerColor( 1 );
    sign_dy->SetMarkerColor( 1 );
    bckg_dy->SetMarkerColor( 1 );
    sign_dphi->SetLineColor( 1 );
    bckg_dphi->SetLineColor( 1 );
    sign_dy->SetLineColor( 1 );
    bckg_dy->SetLineColor( 1 );

    gStyle->SetOptStat( "imr" );

    TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
    canv.Divide( 1 );

    TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    canv.SetFillStyle( 4000 );
    active_pad->SetFillStyle( 4000 );
    active_pad->SetBottomMargin( 0.1 );
    sign_dy->Draw( "HIST P" );
    sign_dy->Draw( "HIST E1 SAME" );
    hist_prep_axes( sign_dy, true);
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( sign_dy, dy_x.c_str(), dy_y.c_str() );
    TPaveStats * sign_dy_stats = make_stats( sign_dy );
    sign_dy_stats->SetLineWidth( 0.0 );
    sign_dy_stats->Draw( "SAME" );
    savename = "sign_dY" + mass + ".pdf" ;
    canv.SaveAs( savename.c_str() );
    canv.Clear();

    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    canv.SetFillStyle( 4000 );
    active_pad->SetFillStyle( 4000 );
    active_pad->SetBottomMargin( 0.1 );
    bckg_dy->Draw( "HIST P" );
    bckg_dy->Draw( "HIST E1 SAME" );
    hist_prep_axes( bckg_dy, true  );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( bckg_dy, dy_x.c_str(), dy_y.c_str() );
    TPaveStats * bckg_dy_stats = make_stats( bckg_dy );
    bckg_dy_stats->Draw( "SAME" );
    bckg_dy_stats->SetLineWidth( 0.0 );
    savename = "bckg_dY" + mass + ".pdf" ;
    canv.SaveAs( savename.c_str() );
    canv.Clear();

    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    canv.SetFillStyle( 4000 );
    active_pad->SetFillStyle( 4000 );
    active_pad->SetBottomMargin( 0.1 );
    sign_dphi->Draw( "HIST P" );
    sign_dphi->Draw( "HIST E1 SAME" );
    hist_prep_axes( sign_dphi, true );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( sign_dphi, dphi_x.c_str(), dphi_y.c_str() );
    TPaveStats * sign_dphi_stats = make_stats( sign_dphi );
    sign_dphi_stats->Draw( "SAME" );
    sign_dphi_stats->SetLineWidth( 0.0 );
    savename = "sign_dphi" + mass + ".pdf" ;
    canv.SaveAs( savename.c_str() );
    canv.Clear();
    
    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    canv.SetFillStyle( 4000 );
    active_pad->SetFillStyle( 4000 );
    active_pad->SetBottomMargin( 0.1 );
    bckg_dphi->Draw( "HIST P" );
    bckg_dphi->Draw( "HIST E1 SAME" );
    hist_prep_axes( bckg_dphi, true );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( bckg_dphi, dphi_x.c_str(), dphi_y.c_str() );
    TPaveStats * bckg_dphi_stats = make_stats( bckg_dphi );
    bckg_dphi_stats->Draw( "SAME" );
    bckg_dphi_stats->SetLineWidth( 0.0 );
    savename = "bckg_dphi" + mass + ".pdf" ;
    canv.SaveAs( savename.c_str() );
    canv.Clear();


    delete sign_dphi;
    delete bckg_dphi;
    delete sign_dy;
    delete bckg_dy;


  }


}

void train_vis( YAML::Node & run_node, variable_set & variables ){
  
  std::cout << variables.analysis_variable << std::endl; 

  std::string unique = run_node[ "bdt_unique" ].as< std::string >();
  std::string filepath = std::string( std::getenv( "OUT_PATH" ) )
                       + "/bdt/" + unique + "/tmva_train_"
                       + unique + ".root";

  TFile * training_output = new TFile( filepath.c_str(), "READ");
  TDirectory * dataloader_directory = training_output->GetDirectory( "tmva_dataloader" );
  
  TDirectory * BDT_upper_directory = dataloader_directory->GetDirectory( "Method_BDT" );
  TDirectory * BDT_lower_directory = BDT_upper_directory->GetDirectory( "BDT" );

  TH1D * sign_bckg_rej_eff = (TH1D *) BDT_lower_directory->Get( "MVA_BDT_rejBvsS" );
  sign_bckg_rej_eff->SetLineColor( 1.0 );

  TTree * test_tree =  static_cast<TTree *>( dataloader_directory->Get( "TestTree" ) );
  TTree * train_tree = static_cast<TTree *>( dataloader_directory->Get( "TrainTree" ) );

  TH1F * sign_test = new TH1F( "sign_test", "", 50, -1.0, 1.0 );
  TH1F * bckg_test = new TH1F( "bckg_test", "", 50, -1.0, 1.0 );

  TH1F * sign_train = new TH1F( "sign_train", "", 50, -1.0, 1.0 );
  TH1F * bckg_train = new TH1F( "bckg_train", "", 50, -1.0, 1.0 );

  test_tree->Draw( "BDT>>sign_test", "classID==0" );
  test_tree->Draw( "BDT>>bckg_test", "classID==1" );
  train_tree->Draw( "BDT>>sign_train", "classID==0" );
  train_tree->Draw( "BDT>>bckg_train", "classID==1" );

  //std::cout << "test entries: " << test_tree->GetEntries() << std::endl;
  //std::cout << "sign_test:    " << sign_test->Integral() << std::endl;
  //std::cout << "bckg_test:    " << bckg_test->Integral() << std::endl;
  //std::cout << " " << std::endl;
  //std::cout << "train entries: " << train_tree->GetEntries() << std::endl;
  //std::cout << "sign_train:  " << sign_train->Integral() << std::endl;
  //std::cout << "bckg_train:   " << bckg_train->Integral() << std::endl;

  sign_test->Scale( 1.0/ sign_test->Integral() );
  bckg_test->Scale( 1.0/ bckg_test->Integral() );
  sign_train->Scale( 1.0/ sign_train->Integral() ); 
  bckg_train->Scale( 1.0/ bckg_train->Integral() );

  sign_test->SetLineColor( kRed+2);
  sign_test->SetFillStyle( 3354 );
  sign_test->SetFillColor( kRed+2 );

  bckg_test->SetLineColor( kBlue+2);
  bckg_test->SetFillStyle( 3345 );
  bckg_test->SetFillColor( kBlue+2 );

  sign_train->SetMarkerStyle( 20 );
  sign_train->SetMarkerColor( kRed+1 );
  sign_train->SetMarkerSize( 1.0 );
  
  bckg_train->SetMarkerStyle( 20 );
  bckg_train->SetMarkerColor( kBlue+1 );
  bckg_train->SetMarkerSize( 1.0 );

  //int max = std::max( { sign_train->GetMaximum(), bckg_train->GetMaximum(), 
  //                      sign_test->GetMaximum(), bckg_test->GetMaximum() } );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );

  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );

  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  sign_bckg_rej_eff->Draw( "HIST" );
  
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_bckg_rej_eff, "Signal Efficiency", "Background Rejection" );
  sign_bckg_rej_eff->GetYaxis()->SetRangeUser( 0.0, 1.0 );

  canv.SetFillStyle( 4000 );
  canv.SaveAs( "EffRejBDT.pdf" );
  canv.Clear();

  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  canv.SetFillStyle( 4000 );
  sign_test->Draw( "HIST" );
  sign_test->Draw( "F SAME" );
  sign_test->Draw( "E1 SAME" );
  sign_train->Draw( "P SAME" );
  sign_train->Draw( "E1 SAME" );
  bckg_test->Draw( "HIST SAME" );
  bckg_test->Draw( "F SAME" );
  bckg_test->Draw( "E1 SAME" );
  bckg_train->Draw( "P SAME" );
  bckg_train->Draw( "E1 SAME" );
  hist_prep_axes( sign_test );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_test, "BDT [score]", "Normalised Entries/0.04" );
  sign_test->GetYaxis()->SetRangeUser( 0.0, 0.25 );
  sign_test->GetYaxis()->SetTitleOffset( 1.6 );
  TLegend * testtrain_legend = create_atlas_legend();
  testtrain_legend->AddEntry( sign_test, "sign - test", "LF" );
  testtrain_legend->AddEntry( bckg_test, "bckg - test", "LF" );
  testtrain_legend->AddEntry( sign_train, "sign - train", "P" ); 
  testtrain_legend->AddEntry( bckg_train, "bckg - train", "P" );
  testtrain_legend->Draw( "SAME" );
  canv.SaveAs( "TrainAndTest.pdf" );
  canv.Clear();

 






}


void bdt( YAML::Node & run_node, variable_set & variables ){

  gStyle->SetTitleYOffset(1.5);
  mkdir( "./bdt", 744 );
  chdir( "./bdt" );
  delta_1d( run_node, variables );  
  delta_2d( run_node, variables );  
  train_vis( run_node, variables );
  chdir( ".." );


}
