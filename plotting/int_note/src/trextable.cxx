#include <trextable.hxx>
#include <sys/stat.h>


void result_table( TH1F * fit, hist_group & all ){

  TH1F * sign = all.sign_hist; 
  TH1F * bckg = all.bckg_hist;
  TH1F * data = all.data_hist; 

  std::ofstream table( "result_table.tex" );
  table << "\\documentclass{article}\n";
  table << "\\usepackage{geometry}\n";
  table << "\\begin{document}";
  table << std::fixed << std::setprecision( 0 );
  table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{ |c|r|r|r|r| }\\hline\n";
  table << "$q_{T}^{A}$ & \\textit{sign} & \\textit{bckg} & \\textit{data} & \\textit{fit} \\textit{$\\chi^{2}$} \\\\ \\hline \n";
  double sign_err = 0, bckg_err = 0, data_err = 0, fit_err = 0;
  for ( int idx = 1; idx <= sign->GetNbinsX(); idx++ ){
    table << idx << " & ";
    table << sign->GetBinContent( idx ) << "$\\pm$" << sign->GetBinError( idx ) << " & ";
    table << bckg->GetBinContent( idx ) << "$\\pm$" << bckg->GetBinError( idx ) << " & ";
    table << data->GetBinContent( idx ) << "$\\pm$" << data->GetBinError( idx ) << " & ";
    table << fit->GetBinContent( idx ) <<  "$\\pm$" << fit->GetBinError( idx ) << " \\\\ \\hline \n";
    sign_err += sign->GetBinError( idx );
    bckg_err += bckg->GetBinError( idx );
    data_err += data->GetBinError( idx );
    fit_err += fit->GetBinError( idx );
  }
  table << "Integral & ";
  table << sign->Integral() << " $\\pm$ " << sign_err << " & ";
  table << bckg->Integral() << " $\\pm$ " << bckg_err << " & ";
  table << data->Integral() << " $\\pm$ " << data_err << " & ";
  table << fit->Integral() <<  " $\\pm$ " << fit_err << " \\\\ \\hline \n";
  table << "\\end{tabular}\n\\caption{MARP}\n\\label{tab:merp}\\end{table}\n\\end{document}";
  table.close();
}


void trextable( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./trextable", 744 );
  chdir( "./trextable" );

  std::string unique = run_node[ "unique" ].as<std::string>();
  auto [ base_total, base_extracted ] = get_fit( unique, variables );
  result_table( base_total, base_extracted );

  chdir( ".." );

}
