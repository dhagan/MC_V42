#include <eff.hxx>
#include <sys/stat.h>

void efficiency_plots( YAML::Node & run_node, variable_set & variables ){

  prep_style();
  gStyle->SetOptStat( "" );
  gStyle->SetTitleYOffset(1.5);

  std::string sel_eff_unique, sel_eff_filepath;
  if ( run_node[ "sel_eff_unique" ] ){ 
      sel_eff_unique = run_node[ "sel_eff_unique" ].as< std::string >();
      sel_eff_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
        + "/trees/" + sel_eff_unique
        + "/efficiency/sign_efficiencies_" 
        + sel_eff_unique + ".root";
  }

  std::string fid_eff_unique, fid_eff_filepath;
  if ( run_node[ "fid_eff_unique" ] ){ 
      fid_eff_unique = run_node[ "fid_eff_unique" ].as< std::string >();
      fid_eff_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
        + "/trees/" + fid_eff_unique
        + "/efficiency/sign_fiducials_" 
        + fid_eff_unique + ".root";
  }

  basic_fileset * sel_fileset = new basic_fileset();
  sel_fileset->set_unique( sel_eff_unique );
  sel_fileset->load_efficiency_fileset( sel_eff_filepath, sel_eff_unique );

  basic_fileset * fid_fileset = new basic_fileset();
  fid_fileset->set_unique( fid_eff_unique );
  fid_fileset->load_efficiency_fileset( fid_eff_filepath, fid_eff_unique );


  std::vector< TH1F * > sel_hists = sel_fileset->get_efficiency_hists( variables );
  std::vector< TH1F * > fid_hists = fid_fileset->get_efficiency_hists( variables );

  TH1F * sel_qta_truth  = sel_hists.at( 0 );
  TH1F * fid_qta_truth  = fid_hists.at( 0 );
  TH1F * sel_qta_reco   = sel_hists.at( 1 );
  TH1F * fid_qta_reco   = fid_hists.at( 1 );
  TH1F * sel_qta        = sel_hists.at( 2 );
  TH1F * fid_qta        = fid_hists.at( 2 );

  std::cout << sel_qta_truth->Integral() << std::endl;
  std::cout << fid_qta_truth->Integral() << std::endl;

  sel_qta->SetMarkerStyle( 20 );
  sel_qta->SetMarkerSize( 1 );
  sel_qta->SetMarkerColor( 1 );
  sel_qta->SetLineColor( 1 );
  sel_qta_truth->SetLineColor( kBlue+2 );
  sel_qta_truth->SetFillColor( kBlue+2 );
  sel_qta_reco->SetLineColor( kRed+2 );
  sel_qta_reco->SetFillColor( kRed+2 );

  fid_qta_truth->SetLineColor( kBlue+2 );
  fid_qta_truth->SetFillColor( kBlue+2 );
  fid_qta_reco->SetLineColor( kRed+2 );
  fid_qta_reco->SetFillColor( kRed+2 );
  fid_qta->SetMarkerStyle( 20 );
  fid_qta->SetLineColor( 1 );


  bound & qta_bound = variables.bound_manager->get_bound( "qtA" );

  mkdir( "./eff", 744 );
  chdir( "./eff" );

  
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad * >( gPad->cd() );
  sel_qta_truth->Draw( "HIST E1" );
  sel_qta_reco->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( sel_qta_truth, true );
  std::string sel_ovly_y_string =  "Entries";//qta_bound.get_y_str();
  std::string sel_ovly_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
  set_axis_labels( sel_qta_truth, sel_ovly_x_string.c_str(), sel_ovly_y_string.c_str() );
  sel_qta->GetYaxis()->SetRangeUser( 0, sel_qta->GetMaximum()*1.1 );
  TLegend * sel_ovly_legend = below_logo_legend();
  sel_ovly_legend->AddEntry( sel_qta_truth, "Truth", "F" );
  sel_ovly_legend->AddEntry( sel_qta_reco, "Reco", "LP" );
  sel_ovly_legend->SetX1NDC( sel_ovly_legend->GetX1NDC() - 0.1 );
  sel_ovly_legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "sel_truth_reco" );


  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad * >( gPad->cd() );
  sel_qta->Draw( "HIST E1" );
  sel_qta->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( sel_qta, true );
  std::string sel_y_string =  "Efficiency";//qta_bound.get_y_str();
  std::string sel_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
  set_axis_labels( sel_qta, sel_x_string.c_str(), sel_y_string.c_str() );
  sel_qta->GetYaxis()->SetRangeUser( 0, sel_qta->GetMaximum()*1.1 );
  TLegend * sel_legend = below_logo_legend();
  sel_legend->AddEntry( sel_qta, "Selection Efficiency", "LP" );
  sel_legend->SetX1NDC( sel_legend->GetX1NDC() - 0.1 );
  sel_legend->Draw();
  save_and_clear( canv, active_pad, "sel_efficiency" );

  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad * >( gPad->cd() );
  fid_qta_truth->Draw( "HIST E1" );
  fid_qta_truth->Draw( "P SAME" );
  fid_qta_reco->Draw( "HIST E1 SAME" );
  fid_qta_reco->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( fid_qta_truth, true );
  std::string fid_ovly_y_string =  "Entries";//qta_bound.get_y_str();
  std::string fid_ovly_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
  set_axis_labels( fid_qta_truth, fid_ovly_x_string.c_str(), fid_ovly_y_string.c_str() );
  fid_qta->GetYaxis()->SetRangeUser( 0, fid_qta->GetMaximum()*1.1 );
  TLegend * fid_ovly_legend = below_logo_legend();
  fid_ovly_legend->AddEntry( fid_qta_truth, "Truth", "LP" );
  fid_ovly_legend->AddEntry( fid_qta_reco, "Reco", "LP" );
  fid_ovly_legend->SetX1NDC( fid_ovly_legend->GetX1NDC() - 0.1 );
  fid_ovly_legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "fid_truth_reco" );


  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad * >( gPad->cd() );
  fid_qta->Draw( "HIST E1" );
  fid_qta->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( fid_qta, true );
  std::string fid_y_string =  "Efficiency";//qta_bound.get_y_str();
  std::string fid_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
  set_axis_labels( fid_qta, fid_x_string.c_str(), fid_y_string.c_str() );
  fid_qta->GetYaxis()->SetRangeUser( 0, fid_qta->GetMaximum()*1.1 );
  TLegend * fid_legend = below_logo_legend();
  fid_legend->AddEntry( fid_qta, "Fiducial Efficiency", "LP" );
  fid_legend->SetX1NDC( fid_legend->GetX1NDC() - 0.1 );
  fid_legend->Draw();
  save_and_clear( canv, active_pad, "fid_efficiency" );

  double sel_error = 0;
  double sel_int = sel_qta_truth->IntegralAndError( 1, sel_qta_truth->GetNbinsX(), sel_error );
  double fid_error = 0;
  double fid_int = fid_qta_truth->IntegralAndError( 1, fid_qta_truth->GetNbinsX(), fid_error );
  sel_error /= sel_int;
  fid_error /= fid_int;

  double new_eff = sel_int/fid_int;
  double new_error = (new_eff)*std::sqrt( sel_error*sel_error + fid_error*fid_error );
  std::cout << "Raw numbers:" << std::endl;
  std::cout << "  Total qtb efficiency: " << new_eff << std::endl;
  std::cout << "  Total qtb eff error:  " << new_error << std::endl;
  std::cout << "Percentage:" << std::endl;
  std::cout << "  Total qtb efficiency (%): " << new_eff*100.0 << std::endl;
  std::cout << "  Total qtb eff error (%):  " << new_error*100.0 << std::endl;


  TH1F * qtb_eff = static_cast<TH1F*>( fid_qta->Clone() );
  qtb_eff->Reset();
  qtb_eff->Divide( sel_qta_truth, fid_qta_truth, 1.0, 1.0, "B" );

  //qtb eff
  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad * >( gPad->cd() );
  qtb_eff->Draw("HIST E1");
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( qtb_eff, true );
  std::string qtb_y_string =  "Efficiency";
  std::string qtb_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
  set_axis_labels( fid_qta, fid_x_string.c_str(), fid_y_string.c_str() );
  fid_qta->GetYaxis()->SetRangeUser( 0, fid_qta->GetMaximum()*1.1 );
  TLegend * qtb_legend = below_logo_legend();
  qtb_legend->AddEntry( fid_qta, "Efficiency, q_{T}^{B} cut", "LP" );
  qtb_legend->Draw();
  save_and_clear( canv, active_pad, "qtb_eff" );



  chdir( ".." );

}


//TH1F * diff_qta = static_cast<TH1F*>( sel_qta->Clone("thing") );
//diff_qta->Reset();              
//diff_qta->Divide( sel_qta, fid_qta, 1.0, 1.0 );
//diff_qta->SetMarkerStyle( 20 ); 
//diff_qta->SetLineColor( 1 );    
//TCanvas eff_diff( "qta_canv", "", 200, 200, 1000, 1000 );
//eff_diff.Divide( 1, 1 );
//active_pad = static_cast< TPad * >( gPad->cd() );
//diff_qta->Draw( "P" );
//add_atlas_decorations( active_pad, false, false );
//add_internal( active_pad );
//hist_prep_axes( diff_qta, true );
//std::string diff_y_string =  "Efficiency Ratio/2.00 [GeV]";//qta_bound.get_y_str();
//std::string diff_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
//set_axis_labels( diff_qta, diff_x_string.c_str(), diff_y_string.c_str() );
//diff_qta->GetYaxis()->SetRangeUser( 0, 2.0 );
//TLegend * diff_legend = below_logo_legend();
//diff_legend->AddEntry( diff_qta, "Selection/Analysis", "LP" );
//diff_legend->SetX1NDC( diff_legend->GetX1NDC() - 0.1 );
//diff_legend->Draw();
//active_pad->SetBottomMargin( 0.1 );
//active_pad->SetFillStyle( 4000 );
//eff_diff.SetFillStyle( 4000 );
//eff_diff.SaveAs( "diff_eff.png" );
//eff_diff.SaveAs( "diff_eff.eps" );
//eff_diff.SaveAs( "diff_eff.pdf" );
