#include <mig.hxx>
#include <sys/stat.h>

void postsel( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./postsel", 744 );
  chdir( "./postsel" );

  std::string unique = run_node["unique"].as<std::string>();
  std::string filepath = std::string( getenv("OUT_PATH" ) ) + "/event_subtraction/" + unique;
  std::cout << variables.analysis_variable << std::endl;

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );

  std::vector< std::string > output_columns = {
    "DPhi", "DY", "AbsPhi", "cos2theta", 
		"Lambda", "DiMuonMass", "DiMuonTau",
    "DiMuonPt", "PhotonPt", "qtA", "qtB" };

  for ( int sample_int = 0; sample_int <= sample_type::bbbg; sample_int++ ){

    sample_type samp = static_cast<sample_type>( sample_int );

    TTree * tree = fileset->get_tree( samp );
  
    for ( std::string & variable : output_columns ){

      bound & var_bound = variables.bound_manager->get_bound( variable );
      TH1F * selection_hist = new TH1F( "sel", "sel", 50, var_bound.get_min(), var_bound.get_max() );
      selection_hist->SetLineWidth( 0 );
      selection_hist->SetFillStyle(1001);
      selection_hist->SetFillColor( rapsberry );

      std::string draw_string = var_bound.get_var() + ">>sel";

      tree->Draw( draw_string.c_str(), "", "e goff" );

      active_pad = static_cast<TPad *>( canv.cd( 1 ) );
      selection_hist->Draw( "HIST E1" );
      set_axis_labels( selection_hist, var_bound.get_x_str(),  "Entries" );
      add_atlas_decorations( active_pad, false, false );
      add_internal( active_pad );
      hist_prep_axes( selection_hist, true );
      TLegend * legend = below_logo_legend();
      legend->AddEntry( selection_hist, type_str[samp] );
      legend->Draw( "SAME" );

      std::string name = std::string(type_str[samp]) + "_" + variable + "_sel"; 
      save_and_clear( canv, active_pad, name );

      delete selection_hist;

    }

  }

  chdir( ".." );

}
