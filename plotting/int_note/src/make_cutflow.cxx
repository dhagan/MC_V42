#include <cutflow.hxx>
#include <funcs.hxx>
#include <BphysEffTool.h>
#include <sys/stat.h>
#include <make_cutflow.hxx>

void analyse_cutflow( YAML::Node & run_node, variable_set & variables ){
  make_cutflow( run_node, variables );
  truth_cutflow( run_node, variables );
}

void truth_cutflow_table( std::vector<std::string> cuts, std::vector<double> counts ){
  std::ofstream truth_table( "truth_table.tex" );
  truth_table << "\\documentclass{article}\n";
  truth_table << "\\usepackage{geometry}\n";
  truth_table << "\\begin{document}";
  truth_table << std::fixed << std::setprecision( 0 );
  truth_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{ |c|c|r| }\\hline\n";
  truth_table << "Cut & descriptiion &Truth Signal \\\\ \\hline \n";
  for ( size_t idx = 0; idx < cuts.size(); idx++ ){
    truth_table << std::to_string( idx ) << "& " << cuts[idx] << "&" << counts[idx] << " \\\\ \\hline \n";
  }
  truth_table << "\\end{tabular}\n\\caption{Signal MC truth Cutflow}\n\\end{table}\n\\end{document}";
  truth_table.close();
}



void make_cutflow_table( std::vector< std::string > types, std::vector<std::string> cuts, 
                        std::vector<std::vector<double>> counts ){
  
  std::ofstream cutflow_table( "cutflow_table.tex" );
  cutflow_table << "\\documentclass{article}\n";
  cutflow_table << "\\usepackage{geometry}\n";
  cutflow_table << "\\begin{document}";
  cutflow_table << std::fixed << std::setprecision( 0 );
  cutflow_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{ |c|c|r|r|r|r| }\\hline\n";
  cutflow_table << "cut & name";
  for ( std::string & type : types ){ cutflow_table << "&" << type; }
  cutflow_table << "\\\\ \\hline \n";
  
  for ( size_t cut_idx = 0; cut_idx < cuts.size(); cut_idx++ ){
    cutflow_table << cut_idx + 1 << " & " << cuts[cut_idx];
    for ( std::vector<double> & cutflow : counts ){
      if ( cut_idx < cutflow.size() ){ cutflow_table << "&" << cutflow.at(cut_idx); }
      else { cutflow_table << "& -- "; }
    }
    cutflow_table << "\\\\ \\hline\n";
  }
  cutflow_table << "\\end{tabular}\n\\caption{Cutflow on all samples}\n\\end{table}\n\\end{document}";
  cutflow_table.close();

}

void image_dr( TH1D dr_graph ){
  
  dr_graph.SetMarkerStyle( 20 );
  dr_graph.SetLineColor( 1 );
  dr_graph.SetMarkerSize( 0.5 );
  gStyle->SetOptStat("irm");

  TCanvas dr_canv( "dr_canv", "", 200, 200, 1000, 1000 );
  dr_canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad * >( gPad->cd() );
  dr_graph.Draw( "HIST" );
  dr_graph.Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( &dr_graph, true );
  std::string sel_y_string =  "Entries/0.004";//qta_bound.get_y_str();
  std::string sel_x_string =  "#Delta R";
  set_axis_labels( &dr_graph, sel_x_string.c_str(), sel_y_string.c_str() );
  TLegend * dr_legend = below_logo_legend();
  dr_legend->AddEntry( &dr_graph, "#Delta R (truth-reco #gamma)", "LP" );
  dr_legend->SetX1NDC( dr_legend->GetX1NDC() - 0.2 );
  dr_legend->Draw("SAME");
  TPaveStats * dr_stats = make_stats( &dr_graph );
  dr_stats->Draw("SAME");
  dr_stats->SetLineWidth( 0 );
  dr_graph.GetYaxis()->SetRangeUser( 0, 50e3 );
  dr_canv.SaveAs( "Phot_dR.png" );
  dr_canv.SaveAs( "Phot_dR.eps" );
  dr_canv.SaveAs( "Phot_dR.pdf" );

  gStyle->SetOptStat("");
}

void make_cutflow( YAML::Node & run_node, variable_set & variables ){
  
	ROOT::EnableImplicitMT( 6 );

  std::string in_path = std::string( getenv( "IN_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::vector< std::string > type_strs = run_node[ "types" ].as< std::vector< std::string > >();
  std::string cutflow_var = run_node[ "cutflow_var" ].as< std::string >();
  bound_mgr * selections = variables.bound_manager;

  std::string dir_name = "./cutflow_" + unique;
  mkdir( dir_name.c_str(), 744 );
  chdir( dir_name.c_str() );
  mkdir( "./cutflow", 744 );


  // define cuts and output columns
	std::vector< std::string > output_columns = { "ActIpX", "AvgIpX", "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
															 "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
															 "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
															 "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0", "qx", "qy", "MuPos_Pt", "MuPos_Eta", "MuNeg_Pt", "MuNeg_Eta", "DiLept_DeltaR", "DiLept_Rap" };

	std::vector< std::string > vector_cuts = { "mu_pos_pt", "mu_neg_pt" };
	std::vector< std::string > float_cuts = { "lambda", "costheta", "qtSquared", "AbsPhi", 
                                            "DiMuonMass", "DiMuonTau", "qtA", "qtB", 
                                            "DiMuonPt", "DeltaZ0", "Phot_Eta", "MuPos_Eta", "MuNeg_Eta", "PhotonPt" };

	std::vector< bound > vector_bounds; 
  for ( std::string & bound_str : vector_cuts ){
    vector_bounds.push_back( selections->get_bound( bound_str ) );
  }

	std::vector< bound > float_bounds; 
	for ( std::string & bound_str : float_cuts ){
    float_bounds.push_back( selections->get_bound( bound_str ) );
  }

  std::vector< std::vector<double> > counts;
  std::vector<std::string> cuts;

  for ( const std::string & type : type_strs ){

    std::string file_path = in_path + "/ntuples/"  + type + "_ntuple.root";

	  // Ready the input and output
    TChain chain( "tree" );
    chain.Add( file_path.c_str() );
    ROOT::RDataFrame input_frame( chain );
    std::string output_file = Form( "./%s/%s_%s.root", type.c_str(), type.c_str(), unique.c_str() );
	  

    // prepare cutflow object and hist structure, 
    bound cf_bound = selections->get_bound( cutflow_var );
    ROOT::RDF::TH1DModel cf_hist_model = { "cutflow_var", "", cf_bound.get_bins(), cf_bound.get_min(), cf_bound.get_max() };
    std::vector< std::string > all_cuts = { "photon_count", "trigger" };
    all_cuts.insert( all_cuts.end(), vector_cuts.begin(), vector_cuts.end() );
    all_cuts.insert( all_cuts.end(), float_cuts.begin(), float_cuts.end() );
    all_cuts.push_back( "Photon_quality" );
    if ( type.find( "sign" ) != std::string::npos ){
      all_cuts.push_back( "Truth_match" );
    }
    auto cut_delimit = []( std::string acc, std::string cut ){
      return  std::string( std::move( acc ) + ":" + cut );
    };
    std::string all_cut_string = std::accumulate( all_cuts.begin(), all_cuts.end(), std::string("no_cut"), cut_delimit ); 
    cutflow cf_selection( all_cut_string, true, cf_bound.get_var(), 0 ); 
    cf_selection.set_var_str( cutflow_var );

    
	  // two base operations before adding the redefinitions and cuts
    cf_selection.set_cut( 0, *input_frame.Count() );
    auto filtered = input_frame.Filter( "Photon_Pt.size() != 0" );
    cf_selection.set_cut( 1, *filtered.Count() );
    filtered = filtered.Filter( "HLT_2mu4_bJpsimumu_noL2 != 0" );
    cf_selection.set_cut( 2, *filtered.Count() );
    
	  // manual redefinitions
	  auto phi_lambda = phi_calc;
	  auto dphi_lambda = dphi_calc;

	  filtered = filtered.Define( "sc", 																			"Float_t( MuMuY_M[0]/1000.0)" );	
	  filtered = filtered.Define( "ActIpX", 																			"Float_t( actIpX )" );	
	  filtered = filtered.Define( "AvgIpX", 																			"Float_t( avgIpX )" );	
	  filtered = filtered.Define( "costheta", 																		"Float_t( MuMuGamma_CS_CosTheta[0] )" );
	  filtered = filtered.Define( "AbsCosTheta", 																	"Float_t( abs(costheta) )" );
	  filtered = filtered.Define( "DPhi",                                         dphi_lambda, { "DiLept_Phi", "Photon_Phi" } );
	  filtered = filtered.Define( "AbsdPhi", 																			"Float_t( abs(DPhi) )" );
	  filtered = filtered.Define( "Phi",                                          phi_lambda, {"MuMuGamma_CS_Phi"} );
	  filtered = filtered.Define( "AbsPhi", 																			"Float_t( sqrt( MuMuGamma_CS_Phi[0] * MuMuGamma_CS_Phi[0]) )" );
	  filtered = filtered.Define( "DY", 																					"Float_t(DiLept_Y[0] - Photon_Eta[0])" );
	  filtered = filtered.Define( "AbsdY", 																				"Float_t( abs(DY) )" );
	  filtered = filtered.Define( "EventNumber",																	"Float_t( evt )" );
	  filtered = filtered.Define( "Lambda", 																			"Float_t( (MuMuY_M[0]/3097.0)*(MuMuY_M[0]/3097.0) )" );
	  filtered = filtered.Define( "qTSquared",																		"Float_t( (MuMuY_Pt[0]/1000.0) * (MuMuY_Pt[0]/1000.0) )" );
	  filtered = filtered.Define( "DiMuonMass",																		"Float_t( DiLept_M[0] )" );
	  filtered = filtered.Define( "DiMuonTau",																		"Float_t( DiMuonVertex_Tau[0] )" );
	  filtered = filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	  filtered = filtered.Define( "DiMuonPt",																			"Float_t( DiLept_Pt[0]/1000.0 )" );
	  filtered = filtered.Define( "PhotonPt",																			"Float_t( Photon_Pt[0]/1000.0 )" );
	  filtered = filtered.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	  filtered = filtered.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
 	  filtered = filtered.Define( "qxsum", 																				"Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	  filtered = filtered.Define( "qysum", 																				"Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	  filtered = filtered.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	  filtered = filtered.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	  filtered = filtered.Define( "qtA",																					"Float_t( DiMuonPt - PhotonPt )" );
	  filtered = filtered.Define( "qtB",																					"Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	  filtered = filtered.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	  filtered = filtered.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	  filtered = filtered.Define( "JPsi_Eta",																			"Float_t( DiLept_Eta[0] )" );
	  filtered = filtered.Define( "Phot_Eta",																			"Float_t( Photon_Eta[0] )" );
    filtered = filtered.Define( "MuPos_Eta",                                    "Float_t( MuPlus_Eta[0] )" );
    filtered = filtered.Define( "MuNeg_Eta",                                    "Float_t( MuMinus_Eta[0] )" );
    filtered = filtered.Define( "MuPos_Pt",                                     "Float_t( MuPlus_Pt[0]/1000.0 )" );
    filtered = filtered.Define( "MuNeg_Pt",                                     "Float_t( MuMinus_Pt[0]/1000.0 )" );
    filtered = filtered.Define( "DiLept_Rap",                                   "Float_t( DiLept_Y[0] )" );
    filtered = filtered.Define( "DiLept_DeltaR",                                "Float_t( DiLept_dR[0] )" );
	  filtered = filtered.Define( "DeltaZ0",																			"Float_t( DiMuon_DeltaZ0[0] )" );
    filtered = filtered.Define( "qx",                                           "Float_t( sqrt(qTSquared)*cos( MuMuY_Phi[0] ) )" );
    filtered = filtered.Define( "qy",                                           "Float_t( sqrt(qTSquared)*sin( MuMuY_Phi[0] ) )" );

    if ( chain.GetBranch( "pu_weight" ) ){
      output_columns.push_back( "pu_weight" );
    } else {
      filtered = filtered.Define( "pu_weight",                                  "Float_t( 1.0 )" );
      output_columns.push_back( "pu_weight" );
    }

    int current_cut = 3;

	  // apply reductions from vector to float and apply cut
	  for ( bound & cut_bound : vector_bounds ){
	  	filtered = filtered.Redefine( cut_bound.get_var(),  []( const ROOT::RVecF &val ){ return val[0]; }, { cut_bound.get_var() } );
	  	filtered = filtered.Filter( cut_bound.get_cut() );
      cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
      current_cut++;
    }

	  for ( bound & cut_bound : float_bounds ){
	  	filtered = filtered.Filter( cut_bound.get_cut() );
      cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
      current_cut++;
	  }

    filtered = filtered.Filter( "Photon_quality[0] <= 1" );
    cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
    current_cut++;

    if ( type.find( "sign" ) != std::string::npos ){
	  	auto photon_dr_lambda = photon_dr_calc;
	  	filtered = filtered.Define( "phot_dR", photon_dr_lambda, { "Photon_Pt", "Photon_Eta", "Photon_Phi", "Photon_E", "mcPhoton_Pt", "mcPhoton_Eta", "mcPhoton_Phi" } ); 
      ROOT::RDF::TH1DModel dr_hist_model = { "photdr", "", 100, 0.0, 0.4 };
      image_dr( *filtered.Histo1D( dr_hist_model, {"phot_dR"} ) );
      output_columns.push_back( "phot_dR" );
      //filtered.Snapshot( "tree", "./before_phot.root", output_columns );
      filtered = filtered.Filter( selections->get_bound( "phot_dR" ).get_cut() );
      cf_selection.set_dist( current_cut, &( *filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
      current_cut++;
	  }
	  // output the filtered tree
	  //std::cout << output_file << std::endl;
    //filtered.Snapshot( "tree", output_file, output_columns );
    cf_selection.write( (type + "_" + unique), cf_bound );

    counts.push_back( cf_selection.get_counts() );
    if ( cf_selection.cut_names.size() > cuts.size() ){
      cuts = cf_selection.cut_names;
    }

  }

  make_cutflow_table( type_strs, cuts, counts );
  


  chdir( ".." );
  return;
}


void truth_cutflow( YAML::Node & run_node, variable_set & variables ){
  
  ROOT::EnableImplicitMT( 6 );
  std::string in_path = std::string( getenv( "IN_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::vector< std::string > type_strs = run_node[ "types" ].as< std::vector< std::string > >();
  std::string cutflow_var = run_node[ "cutflow_var" ].as< std::string >();
  bound_mgr * selections = variables.bound_manager;

  std::string dir_name = "./cutflow_" + unique;
  mkdir( dir_name.c_str(), 744 );
  chdir( dir_name.c_str() );
  mkdir( "./cutflow", 744 );

  std::string file_path = in_path + "/ntuples/sign_ntuple.root";
  TChain chain( "tree" );
  chain.Add( file_path.c_str() );
  ROOT::RDataFrame input_frame( chain );
  //std::string output_file = Form( "./%s/%s_%s.root", type.c_str(), type.c_str(), unique.c_str() );

  std::vector< std::string > truth_vector_cuts = { "truth_mu_pos_pt", "truth_mu_neg_pt" };
  std::vector< std::string > truth_float_cuts = { "lambda", "costheta", "qtSquared", "AbsPhi", 
                                            "DiMuonMass", "DiMuonTau", "qtA", "qtB", 
                                            "DiMuonPt", "DeltaZ0", "Phot_Eta", "MuPos_Eta", "MuNeg_Eta", "PhotonPt" };

  std::vector< bound > truth_vector_bounds, truth_float_bounds;
  for ( std::string & bound_str : truth_vector_cuts ){
    truth_vector_bounds.push_back( selections->get_bound( bound_str ) );
  }

	for ( std::string & bound_str : truth_float_cuts ){
    truth_float_bounds.push_back( selections->get_bound( bound_str ) );
  }

  // prepare cutflow object and hist structure, 
  bound cf_bound = selections->get_bound( cutflow_var );
  ROOT::RDF::TH1DModel cf_hist_model = { "cutflow_var", "", cf_bound.get_bins(), cf_bound.get_min(), cf_bound.get_max() };
  std::vector< std::string > all_cuts = { "trigger", "object_count" };
  all_cuts.insert( all_cuts.end(), truth_vector_cuts.begin(), truth_vector_cuts.end() );
  all_cuts.insert( all_cuts.end(), truth_float_cuts.begin(),  truth_float_cuts.end() );
  auto cut_delimit = []( std::string acc, std::string cut ){ return  std::string( std::move( acc ) + ":" + cut ); };
  std::string all_cut_string = std::accumulate( all_cuts.begin(), all_cuts.end(), std::string("no_cut"), cut_delimit );
  cutflow cf_selection( all_cut_string, true, cf_bound.get_var(), 0 ); 
  cf_selection.set_var_str( cutflow_var );
  
	auto phi_lambda = phi_calc;
	auto dphi_lambda = dphi_calc;

  cf_selection.set_cut( 0, *input_frame.Count() );
  auto truth_filtered = input_frame.Filter( "HLT_2mu4_bJpsimumu_noL2 != 0" );
  cf_selection.set_cut( 1, *input_frame.Count() );
  truth_filtered = truth_filtered.Filter( "mcPhoton_Pt.size() != 0" );
  truth_filtered = truth_filtered.Filter( "mcMuMinus_Pt.size() != 0" );
  truth_filtered = truth_filtered.Filter( "mcMuPlus_Pt.size() != 0" );
  cf_selection.set_cut( 2, *input_frame.Count() );

	// manual redefinitions
	truth_filtered = truth_filtered.Define( "ActIpX", 																			"Float_t( actIpX )" );	
	truth_filtered = truth_filtered.Define( "AvgIpX", 																			"Float_t( avgIpX )" );	
	truth_filtered = truth_filtered.Define( "costheta", 																		"Float_t( Truth_MuMuGamma_CS_CosTheta[0] )" );
	truth_filtered = truth_filtered.Define( "AbsCosTheta", 																	"Float_t( abs(costheta) )" );
	truth_filtered = truth_filtered.Define( "DPhi",                                         dphi_lambda, { "mcDiLept_Phi", "mcPhoton_Phi" } );
	truth_filtered = truth_filtered.Define( "AbsdPhi", 																			"Float_t( abs(DPhi) )" );
	truth_filtered = truth_filtered.Define( "Phi",                                          phi_lambda, {"Truth_MuMuGamma_CS_Phi"} );
	truth_filtered = truth_filtered.Define( "AbsPhi", 																			"Float_t( sqrt( Truth_MuMuGamma_CS_Phi[0] * Truth_MuMuGamma_CS_Phi[0]) )" );
	truth_filtered = truth_filtered.Define( "DY", 																					"Float_t( mcDiLept_Eta[0] - mcPhoton_Eta[0])" );
	truth_filtered = truth_filtered.Define( "AbsdY", 																				"Float_t( abs(DY) )" );
	truth_filtered = truth_filtered.Define( "EventNumber",																	"Float_t( evt )" );
	truth_filtered = truth_filtered.Define( "Lambda", 																			"Float_t( (mcMuMuY_M[0]/3097.0) * (mcMuMuY_M[0]/3097.0) )" );
	truth_filtered = truth_filtered.Define( "qTSquared",																		"Float_t( (mcMuMuY_Pt[0]/1000.0) * (mcMuMuY_Pt[0]/1000.0) )" );
	truth_filtered = truth_filtered.Define( "DiMuonMass",																		"Float_t( mcDiLept_M[0] )" );
	truth_filtered = truth_filtered.Define( "DiMuonTau",																		"Float_t( 0 )" );
	truth_filtered = truth_filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	truth_filtered = truth_filtered.Define( "DiMuonPt",																			"Float_t( mcDiLept_Pt[0]/1000.0 )" );
	truth_filtered = truth_filtered.Define( "PhotonPt",																			"Float_t( mcPhoton_Pt[0]/1000.0 )" );
	truth_filtered = truth_filtered.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
 	truth_filtered = truth_filtered.Define( "qxsum", 																				"Float_t( (DiMuonPt + PhotonPt)*(1+cos(DPhi)) )" );
 	truth_filtered = truth_filtered.Define( "qysum", 																				"Float_t( (DiMuonPt + PhotonPt)*(sin(DPhi)) )" );
	truth_filtered = truth_filtered.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qtA",																					"Float_t( DiMuonPt - PhotonPt )" );
	truth_filtered = truth_filtered.Define( "qtB",																					"Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	truth_filtered = truth_filtered.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	truth_filtered = truth_filtered.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	truth_filtered = truth_filtered.Define( "JPsi_Eta",																			"Float_t( mcDiLept_Eta[0] )" );
	truth_filtered = truth_filtered.Define( "Phot_Eta",																			"Float_t( mcPhoton_Eta[0] )" );
  truth_filtered = truth_filtered.Define( "MuPos_Eta",                                    "Float_t( mcMuPlus_Eta[0] )" );
  truth_filtered = truth_filtered.Define( "MuNeg_Eta",                                    "Float_t( mcMuMinus_Eta[0] )" );
	truth_filtered = truth_filtered.Define( "DeltaZ0",																			"Float_t( 0 )" );
  truth_filtered = truth_filtered.Define( "qx",                                           "Float_t( sqrt(qTSquared)*cos( mcMuMuY_Phi[0] ) )" );
  truth_filtered = truth_filtered.Define( "qy",                                           "Float_t( sqrt(qTSquared)*sin( mcMuMuY_Phi[0] ) )" );

	// apply reductions from vector to float and apply cut
  int current_cut = 3;
	for ( bound & cut_bound : truth_vector_bounds ){
		truth_filtered = truth_filtered.Redefine( cut_bound.get_var(),  []( const ROOT::RVecF &val ){ return val[0]; }, { cut_bound.get_var() } );
		truth_filtered = truth_filtered.Filter( cut_bound.get_cut() );
    cf_selection.set_dist( current_cut, &( *truth_filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
    current_cut++;
  }

	for ( bound & cut_bound : truth_float_bounds ){
		truth_filtered = truth_filtered.Filter( cut_bound.get_cut() );
    cf_selection.set_dist( current_cut, &( *truth_filtered.Histo1D( cf_hist_model, {cutflow_var} ) ) );
    current_cut++;
	}

  std::vector< double > counts = cf_selection.get_counts();
  std::vector< std::string > cuts = cf_selection.cut_names;

  truth_cutflow_table( cuts, counts );
  chdir( ".." );
  return;
}