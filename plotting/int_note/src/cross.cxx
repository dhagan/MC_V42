#include <cross.hxx>
#include <sys/stat.h>


void cross_table( TH1F * sign, TH1F * sign_eff, TH1F * eff ){

  std::ofstream table( "cross_table.tex" );
  table << "\\documentclass{article}\n";
  table << "\\usepackage{geometry}\n";
  table << "\\usepackage{makecell}\n";
  table << "\\begin{document}";
  table << std::fixed << std::setprecision( 2 );
  table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{ |c|r|r|r|r||r|r| }\\hline\n";
  table << "$q_{T}^{A}$ & \\makecell{$\\frac{\\textit{Yield}}{\\Delta q_{T}^{A}*L_{int}}$} & $\\textit{sign}_{err}$ ($\\pm$) & $\\epsilon$ & $\\epsilon_{err}$ ($\\pm$) & total & $\\epsilon_{\\textit{total}}$ \\\\ \\hline \n";
  for ( int idx = 1; idx <= sign->GetNbinsX(); idx++ ){
    table << idx << " & ";
    table << sign->GetBinContent( idx ) << " & " << sign->GetBinError( idx ) << " & ";
    table << eff->GetBinContent( idx ) << " & " << std::setprecision(3) << eff->GetBinError( idx ) << std::setprecision(2) << " & ";
    table << sign_eff->GetBinContent( idx ) << " & " << sign_eff->GetBinError( idx ) << "\\\\ \\hline \n";
  }
  table << "\\end{tabular}\n\\caption{IS EPSILON GONNA BE EFFICIENCY OR ERROR?}\n\\label{tab:merp}\\end{table}\n\\end{document}";
  table.close();
}


void cross( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./cross", 744 );
  chdir( "./cross" );

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string fit_unique = run_node[ "unique" ].as<std::string>();

  //std::string sf_unique = run_node[ "sf_unique" ].as<std::string>();
  //std::string sf_filepath = in_path + "/scalefactors/" + sf_unique; 
  //basic_fileset * fileset = new basic_fileset();
  //fileset->set_unique( sf_unique );
  //fileset->load_subtraction_fileset( sf_filepath, sf_unique, true );

  std::string eff_unique = run_node[ "eff_unique" ].as<std::string>();
  std::string eff_filepath = in_path + "/trees/" + eff_unique
                           + "/efficiency/sign_fiducials_" 
                           + eff_unique + ".root";

  basic_fileset * eff_fileset = new basic_fileset();
  eff_fileset->set_unique( eff_unique );
  eff_fileset->load_efficiency_fileset( eff_filepath, eff_unique, true );

  auto [ base_total, base_extracted ] = get_fit( fit_unique, variables );

  TH1F * sign = base_extracted.sign_hist;
  //H1F * data = base_extracted.data_hist;
  sign->Scale( 1.0/(2.0*2.57) );
  TH1F * sign_eff = static_cast<TH1F *>( sign->Clone() );

  TH1F * efficiency = eff_fileset->get_efficiency( variables );
  sign_eff->Divide( sign, efficiency, 1.0, 1.0 );

  cross_table( sign, sign_eff, efficiency );

  TCanvas * canv = new TCanvas( "canv", "", 200, 200, 1000, 1000 );

  std::string x_string = "q_{T}^{A} [GeV]";
  std::string y_string = "d#sigma/dq_{T}^{A} [pb/GeV]";
  

  sign->SetMarkerStyle( 20 );
  sign->SetLineColor( kBlack );
  sign_eff->SetMarkerStyle( 20 );
  sign_eff->SetLineColor( kBlack );
  gStyle->SetOptStat( "imr" );

  canv->Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv->cd( 1 ) );
  sign->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( sign, true );
  TPaveStats * sign_stats = make_stats( sign );
  sign_stats->Draw( "SAME" );
  sign_stats->SetLineWidth( 0 );
  //sign->GetYaxis()->SetRangeUser( 0.0, 0.1 );
  sign->GetYaxis()->SetTitleOffset( 1.65 );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  canv->SetFillStyle( 4000 );
  canv->SaveAs( "qtA_noEff_dcs.pdf" );
  canv->Clear();

  canv->Divide( 1 );
  active_pad = static_cast< TPad *>( canv->cd( 1 ) );
  sign_eff->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_eff, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( sign_eff, true );
  TPaveStats * sign_eff_stats = make_stats( sign_eff );
  sign_eff_stats->Draw( "SAME" );
  sign_eff_stats->SetLineWidth( 0 );
  //sign_eff->GetYaxis()->SetRangeUser( 0.0, 0.35 );
  sign_eff->GetYaxis()->SetTitleOffset( 1.65 );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  canv->SetFillStyle( 4000 );
  canv->SaveAs( "qtA_eff_dcs.pdf" );
  canv->Clear();
  
  chdir( ".." );

}
