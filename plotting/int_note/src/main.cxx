#include <main.hxx>

#include <pre_cut.hxx>
#include <make_cutflow.hxx>
#include <acceptance.hxx>
#include <eff.hxx>
#include <subtraction.hxx>
#include <fig16.hxx>
#include <bdt.hxx>
#include <region.hxx>
#include <lower.hxx>
#include <trextable.hxx>
#include <cross.hxx>
#include <tmd.hxx>
#include <bgexp.hxx>
#include <threefits.hxx>
#include <photonsf.hxx>
#include <muonsf.hxx>
#include <sys_group.hxx>
#include <sys_internal.hxx>
#include <final.hxx>
#include <mig.hxx>
#include <bjorken.hxx>
#include <modsf.hxx>
#include <chi.hxx>
#include <jpsipt.hxx>
#include <postsel.hxx>

#include <yaml-cpp/yaml.h>

int help(){
  std::cout << " Usage:" << std::endl;
  std::cout << " ./gen --input,-i INPUT_PATH" << std::endl;
  std::cout << " " << std::endl;
  return 0;
}
int main( int argc, char * argv[] ){

  prep_style();
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "help",        no_argument,              0,      'h'},
      { "input",       required_argument,        0,      'i'},
      {0,             0,                        0,      0}
    };

  std::string input = "";
  do {
    option = getopt_long(argc, argv, "i:ph", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'i': 
        input         = std::string( optarg );
        break;
    }
  } while (option != -1);

  if ( input.empty() ){
    std::cout << "No input, exiting..." << std::endl;
    return 0;
  }

  YAML::Node config = YAML::LoadFile( input );
  std::vector< std::string > run_list = config["general"]["runs"].as< std::vector<std::string > >();

  for ( std::string & run : run_list ){

    YAML::Node run_node = config[ run ];

    std::vector< std::string > modes = run_node[ "modes" ].as< std::vector< std::string > >();
    if ( modes.size() == 0 ){ std::cout << "No modes provided, exiting..." << std::endl; }

    std::string avar = "qtA", svar = "BDT";
    std::string range = "", bounds_path = "";
    if ( run_node[ "avar" ] ){ avar = run_node["avar"].as< std::string >(); }
    if ( run_node[ "svar" ] ){ svar = run_node["svar"].as< std::string >(); }
    if ( run_node[ "ranges" ] ){ range = run_node["ranges"].as< std::string >(); }
    if ( run_node[ "bounds_file" ] ){ bounds_path = std::string( std::getenv( "LIB_PATH" ) ) 
                                              + "/share/" 
                                              + run_node["bounds_file"].as< std::string >(); }

    std::cout << bounds_path << std::endl;
    variable_set variables = variable_set( bounds_path, range, avar, svar );

    for ( std::string & mode : modes ){

      std::cout << mode << std::endl;

      if ( mode.find( "pre_cut" ) != std::string::npos ){
        pre_cut( run_node, variables  );
        continue;
      }
      
      if ( mode.find( "cutflow" ) != std::string::npos ){
        analyse_cutflow( run_node, variables  );
        continue;
      }
      if ( mode.find( "acceptance" ) != std::string::npos ){
        acceptance_plots( run_node, variables );
        continue;
      }
      
      if ( mode.find( "eff" ) != std::string::npos ){
        efficiency_plots( run_node, variables );
        continue;
      }

      if ( mode.find( "sub" ) != std::string::npos ){
        subtraction_plots( run_node, variables );
        continue;
      }
      
      if ( mode.find( "fig16" ) != std::string::npos ){
        fig16( run_node, variables );
        continue;
      }


      if ( mode.find( "bdt" ) != std::string::npos ){
        bdt( run_node, variables );
        continue;
      }

      if ( mode.find( "region" ) != std::string::npos ){
        region( run_node, variables );
        continue;
      }

      if ( mode.find( "lower" ) != std::string::npos ){
        lower_sb( run_node, variables );
        continue;
      }
      
      if ( mode.find( "trextable" ) != std::string::npos ){
        trextable( run_node, variables );
        continue;
      }

      if ( mode.find( "cross" ) != std::string::npos ){
        cross( run_node, variables );
        continue;
      }
      
      if ( mode.find( "tmd" ) != std::string::npos ){
        tmd( run_node, variables );
        continue;
      }
      
      if ( mode.find( "bgexp" ) != std::string::npos ){
        bgexp( run_node, variables );
        continue;
      }

      if ( mode.find( "threefits" ) != std::string::npos ){
        threefits( run_node, variables );
        continue;
      }
      if ( mode.find( "photonsf" ) != std::string::npos ){
        photonsf( run_node, variables );
        continue;
      }
      if ( mode.find( "muonsf" ) != std::string::npos ){
        muonsf( run_node, variables );
        continue;
      }
      if ( mode.find( "sys_group" ) != std::string::npos ){
        sys_group( run_node, variables );
        continue;
      }
      if ( mode.find( "sys_internal" ) != std::string::npos ){
        sys_internal( run_node, variables );
        continue;
      }
      if ( mode.find( "final" ) != std::string::npos ){
        final_results( run_node, variables );
        continue;
      }
      if ( mode.find( "mig" ) != std::string::npos ){
        mig( run_node, variables );
        continue;
      }
      if ( mode.find( "bjorken" ) != std::string::npos ){
        bjorken( run_node, variables );
        continue;
      }
      if ( mode.find( "modsf" ) != std::string::npos ){
        modsf( run_node, variables );
        continue;
      }
      if ( mode.find( "chi" ) != std::string::npos ){
        chi( run_node );
        continue;
      }
      if ( mode.find( "jpsipt" ) != std::string::npos ){
        jpsipt( run_node, variables );
        continue;
      }
      if ( mode.find( "cmpb" ) != std::string::npos ){
        bdt_cmp( run_node );
        continue;
      }
      if ( mode.find( "postsel" ) != std::string::npos ){
        postsel( run_node, variables );
        continue;
      }
    }
  }

  return 0;

}

//else if ( mode.find( "sr" ) != std::string::npos ){
//
//  if ( avar.empty() || svar.empty() ){ parse_filename( input, avar, svar ); }
//  bound_mgr * selections = new bound_mgr();
//  selections->load_bound_mgr( bounds_path );
//  if ( !range.empty() ){ selections->process_bounds_string( range ); }
//  generate_sr( input, avar, svar, cuts, output_unique, selections );  
