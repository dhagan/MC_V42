#include <bgexp.hxx>
#include <sys/stat.h>
#include <TMathBase.h>
#include <TMath.h>
#include <TFitResult.h>

struct voigt_add {
  TF1 full, vgt, other;
  virtual void align( TH1F * mass_hist ) = 0;
  virtual void fit( TH1F * mass_hist ) = 0;
};


struct voigt_exp : voigt_add {
  voigt_exp(){
    full = TF1( "Vgt + Exp", "[0]*TMath::Voigt(x-[1], [2], [3], [4] ) + [5]*exp(-(x-2.7)*[6])", 2.7, 3.5);
    vgt  = TF1( "Vgt", "[0]*TMath::Voigt(x-[1], [2], [3], [4] )", 2.7, 3.5);
    other = TF1( "Exp", "[0]*exp(-(x-2.7)*[1])", 2.7, 3.5);
  }
  void align( TH1F * mass_hist ){
    full.SetParameter( 0, mass_hist->GetMaximum()/10 );
    full.SetParLimits( 0, 0, mass_hist->GetMaximum()*1.5 );
    full.SetParameter( 1, 3.096 );
    full.SetParLimits( 1, 3.05, 3.15 );
    full.SetParameter( 2, 0.02 );
    full.SetParLimits( 2, 0.001,  mass_hist->GetStdDev()*1.5 );
    full.SetParameter( 3, 0.05 );
    full.SetParameter( 4, 3 );
    full.SetParameter( 5, mass_hist->GetMaximum()*(1.0/5.0) );
    full.SetParameter( 6, 1 );
    full.SetParNames( "C_{v}", "#bar{x}", "#sigma", "#gamma", "C_{e}", "#mu" );
    //full.SetParName( 0, );
    //full.SetParName( 1, );
    //full.SetParName( 2, );
    //full.SetParName( 3, );
    //full.SetParName( 4, );
    //full.SetParName( 5, );
    //full.SetParName( 6, );






  }
  void fit( TH1F * mass_hist ){
    mass_hist->Fit( &full, "MQ", "", 2.7, 3.5 );
    vgt.SetParameter( 0, full.GetParameter( 0 ) );
    vgt.SetParameter( 1, full.GetParameter( 1 ) );
    vgt.SetParameter( 2, full.GetParameter( 2 ) );
    vgt.SetParameter( 3, full.GetParameter( 3 ) );
    vgt.SetParameter( 4, full.GetParameter( 4 ) );
    other.SetParameter( 0, full.GetParameter( 5 ) );
    other.SetParameter( 1, full.GetParameter( 6 ) );
  }

};

struct voigt_lin : voigt_add {
  voigt_lin(){
    full = TF1( "Vgt + Lin", "[0]*TMath::Voigt(x-[1], [2], [3], [4] ) + [5]*(x-2.7) + [6]", 2.7, 3.5);
    vgt  = TF1( "Vgt", "[0]*TMath::Voigt(x-[1], [2], [3], [4] )", 2.7, 3.5);
    other = TF1( "Lin", "[0]*(x-2.7)+ [1]", 2.7, 3.5);
  }
  void align( TH1F * mass_hist ){
    full.SetParameter( 0, mass_hist->GetMaximum()/10 );
    full.SetParLimits( 0, 0, mass_hist->GetMaximum()*1.5 );
    full.SetParameter( 1, 3.096 );
    full.SetParLimits( 1, 3.05, 3.15 );
    full.SetParameter( 2, 0.02 );
    full.SetParLimits( 2, 0.001,  mass_hist->GetStdDev()*1.5 );
    full.SetParameter( 3, 0.05 );
    full.SetParameter( 4, 3 );
    full.SetParameter( 5, -0.1 );
    full.SetParameter( 6, mass_hist->GetMaximum()*(1.0/10.0) );
  }
  void fit( TH1F * mass_hist ){
    mass_hist->Fit( &full, "MQ", "", 2.7, 3.5 );
    vgt.SetParameter( 0, full.GetParameter( 0 ) );
    vgt.SetParameter( 1, full.GetParameter( 1 ) );
    vgt.SetParameter( 2, full.GetParameter( 2 ) );
    vgt.SetParameter( 3, full.GetParameter( 3 ) );
    vgt.SetParameter( 4, full.GetParameter( 4 ) );
    other.SetParameter( 0, full.GetParameter( 5 ) );
    other.SetParameter( 1, full.GetParameter( 6 ) );
  }

};

void voigt_graph( TH1F * mass_hist, voigt_add & funcs ){

  std::string x_string = "M_{#mu#mu}/0.008 [GeV]";
  std::string y_string = "Entries/0.008 [GeV^{-1}]";

  mass_hist->SetLineColor( kBlack );
  mass_hist->SetMarkerSize( 0.5 );
  mass_hist->SetMarkerStyle( 20  );
  funcs.full.SetLineColorAlpha( kRed+2, 0.8 );
  funcs.other.SetLineColorAlpha( kGreen+1, 0.8 );
  funcs.other.SetLineStyle( 2 );
  funcs.vgt.SetLineColorAlpha( kMagenta+1, 0.8 );
  funcs.vgt.SetLineStyle( 2 );


  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  mass_hist->Draw( "E1" );
  mass_hist->Draw( "SAME HIST" );
  funcs.full.Draw( "SAME" );
  funcs.vgt.Draw( "SAME" );
  funcs.other.Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( mass_hist, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( mass_hist, true );
  //mass_hist->GetYaxis()->SetRangeUser( 0.0, mass_hist->GetMaximumStored()*1.3 );
  TPaveStats * stats = make_stats( mass_hist );
  stats->Draw( "SAME" );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( mass_hist, "Data", "LP" );
  legend->AddEntry( &funcs.full, funcs.full.GetName(), "L" );
  legend->AddEntry( &funcs.vgt, funcs.vgt.GetName(), "L" );
  legend->AddEntry( &funcs.other, funcs.other.GetName(), "L" );
  legend->Draw();
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  std::string output_filename = "mass_vgt_fit_" 
                                + std::string( mass_hist->GetName() ) + "_" 
                                + funcs.other.GetName() + ".pdf";
  canv.SaveAs( output_filename.c_str() );
  output_filename = "mass_vgt_fit_" + std::string( mass_hist->GetName() ) + "_" 
                  + funcs.other.GetName() + ".png";
  canv.SaveAs( output_filename.c_str() );
  canv.Clear();
  //delete legend;
  //delete stats;
}



void bgexp( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./bgexp", 744 );
  chdir( "./bgexp" );

  gStyle->SetOptStat("");

  std::string unique = run_node["unique"].as<std::string>();
  std::string filepath = std::string( getenv("OUT_PATH" ) ) + "/event_subtraction/" + unique;

  std::cout << variables.analysis_variable << std::endl;

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, false  );
  
  
  TH1F * mass_hist = new TH1F( "qtA0", "", 200, 2.7, 3.5 );
  TTree * tree = fileset->get_tree( sample_type::data );
  tree->Draw( "(DiMuonMass/1000.0) >>qtA0", "", "e goff" );
  mass_hist->SetMarkerStyle( 20 );
  mass_hist->SetMarkerSize( 0.5 );
  mass_hist->SetLineColor( kBlack );
 
  voigt_exp full_qta_fit = voigt_exp();
  full_qta_fit.align( mass_hist );
  full_qta_fit.fit( mass_hist );
  voigt_graph( mass_hist, full_qta_fit );
 
  double i1 = full_qta_fit.full.Integral( 2.7, 2.9 );
  double i4 = full_qta_fit.full.Integral( 3.3, 3.5 );
  std::cout << std::pow( i4/i1, 0.3333333 ) << std::endl;

  std::vector<std::string> ana_bin_cuts   = variables.analysis_bound.get_cut_series();
  std::vector<std::string> ana_bin_names  = variables.analysis_bound.get_series_names();
  std::vector<voigt_exp> exp_fits( ana_bin_cuts.size(), voigt_exp() );
  std::vector<voigt_lin> lin_fits( ana_bin_cuts.size(), voigt_lin() );
  std::vector< TH1F * > hists;
  hists.reserve( ana_bin_cuts.size() );

  for ( size_t ana_idx = 0; ana_idx < ana_bin_names.size(); ana_idx++ ){

    std::string & cut   = ana_bin_cuts[ana_idx]; 
    std::string & name  = ana_bin_names[ana_idx]; 
    voigt_exp & exp_fit    = exp_fits[ana_idx];
    voigt_lin & lin_fit    = lin_fits[ana_idx];
    std::string draw_expression = "DiMuonMass/1000.0>>" + name;
    int tau_bins = ( ana_idx < 4 || ana_idx > 11 ) ? 50 : 200;
    hists.push_back( new TH1F( name.c_str(), "", tau_bins, 2.7, 3.5 ) );
    tree->Draw( draw_expression.c_str(), cut.c_str(), "e goff" );
    exp_fit.align( hists[ana_idx] );
    exp_fit.fit( hists[ana_idx] );
    voigt_graph( hists[ana_idx], exp_fit );
    lin_fit.align( hists[ana_idx] );
    lin_fit.fit( hists[ana_idx] );
    voigt_graph( hists[ana_idx], lin_fit );
    double i1 = exp_fit.full.Integral( 2.7, 2.9 );
    double i4 = exp_fit.full.Integral( 3.3, 3.5 );
    std::cout << std::pow( i4/i1, 0.3333333 ) << ":";

  }
  std::cout << std::flush;

  chdir( ".." );

}


// struct voigt{
//   TF1 * vgt;
//   voigt(){
//     vgt  = new TF1( "func", "[0]*TMath::Voigt(x-[1], [2], [3], [4] )", 2.7, 3.5);
//   }
//   void align( TH1F * mass_hist ){
//     vgt->SetParameter( 0, mass_hist->GetMaximum()*(2.0/3.0) );
//     vgt->SetParameter( 1, 3.096 );
//     vgt->SetParLimits( 1, 3.05, 3.15 );
//     vgt->SetParameter( 2, mass_hist->GetStdDev()/5 );
//     vgt->SetParLimits( 2, mass_hist->GetStdDev()/50,  mass_hist->GetStdDev() );
//     vgt->SetParameter( 3, 1 );
//     vgt->SetParameter( 4, 10 );
//   }
//   void fit( TH1F * mass_hist ){
//     mass_hist->Fit( vgt, "MQ", "", 2.7, 3.5 );
//     vgt->SetParameter( 0, vgt->GetParameter( 0 ) );
//     vgt->SetParameter( 1, vgt->GetParameter( 1 ) );
//     vgt->SetParameter( 2, vgt->GetParameter( 2 ) );
//     vgt->SetParameter( 3, vgt->GetParameter( 3 ) );
//     vgt->SetParameter( 4, vgt->GetParameter( 4 ) );
//   }

// };

// struct voigt_plus : voigt {
//   TF1 * plus;
//   TF1 * other;
//   voigt_plus( TF1 * other ) : voigt() {
//     other  = static_cast< TF1 * >( other->Clone() );
//     plus = vgt->Comb
//   }
//   void align( TH1F * mass_hist ){
//     voigt::align( mass_hist );
//   }
//   void fit( TH1F * mass_hist ){
//     mass_hist->Fit( plus, "MQ", "", 2.7, 3.5 );
//     for ( int param = 0; param = other->GetNpar(); param++ ){
//       other->SetParameter( param, plus->GetParameter( param+5 ) );
//     }
//   }
// };