#include <funcs.hxx>
#include <muonsf.hxx>
#include <sys/stat.h>
#include <sstream>

TH1F * average_sf( TTree * tree, bound & var ){

  std::cout << var.get_name() << std::endl;
  std::string name = var.get_name() + "main";
  TH1F * out_hist = new TH1F( name.c_str(), "", 15, var.get_min(), var.get_max() );
  std::vector< std::string > cuts = var.get_cut_series( 15 );
  TH1F * sf_temp = new TH1F( "temp", "", 100, 0.0, 2.0 );
  for ( size_t bin_idx = 0; bin_idx < 15; bin_idx++ ){
    tree->Draw( "muon_sf>>temp", cuts.at(bin_idx).c_str(), "e goff" );
    out_hist->SetBinContent( bin_idx+1, sf_temp->GetMean() );
    std::cout << sf_temp->Integral() << " " << std::flush;
    sf_temp->Reset();
  }
  std::cout << " " << std::endl;
  delete sf_temp;
  return out_hist;
  

}

TH1F * average_sferr_up( TTree * tree, bound & var ){

  std::cout << var.get_name() << " upper" << std::endl;
  //TH1F * out_hist = var.get_hist();
  //std::vector< std::string > cuts = var.get_cut_series();
  TH1F * out_hist = new TH1F( var.get_name().c_str(), "", 15, var.get_min(), var.get_max() );
  std::vector< std::string > cuts = var.get_cut_series( 15 );
  TH1F * sf_temp = new TH1F( "temp_up", "", 100, 0.0, 2.0 );
  for ( size_t bin_idx = 0; bin_idx < 15; bin_idx++ ){
    tree->Draw( "muon_sf*(1+muon_sf_systematic)>>temp_up", cuts.at(bin_idx).c_str(), "e goff" );
    out_hist->SetBinContent( bin_idx+1, sf_temp->GetMean() );
    std::cout << sf_temp->Integral() << " " << std::flush;
    sf_temp->Reset();
  }
  std::cout << " " << std::endl;
  std::cout << " " << std::endl;
  delete sf_temp;
  return out_hist;
}


template <typename T>
std::string tswp(const T a_value, const int n = 6)
{
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    return std::move(out).str();
}

std::map<std::string, std::string > x_strs = { {"MuPos_Pt", "p_{T,#mu^{+}} [GeV]"}, 
                                              {"MuNeg_Pt", "p_{T,#mu^{-}} [GeV]"}, 
                                               {"DiLept_DeltaR", "#Delta R"  }, 
                                               {"DiLept_Rap", "y_{#mu#mu}" }, 
                                               {"qtA", "q_{T}^{A} [GeV]" } };

std::map<std::string, std::string > unit = { { "MuPos_Pt", "[GeV]"}, 
                                             { "MuNeg_Pt", "[GeV]"}, 
                                             { "DiLept_DeltaR", ""  }, 
                                             { "DiLept_Rap", "" }, 
                                             { "qtA", "[GeV]" } };




void plot_sf( TH1F * sf_graph, bound & var ){

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  sf_graph->SetLineColor( kBlack );
  sf_graph->SetMarkerStyle( 20  );
  sf_graph->SetMarkerColor( kBlack );
  std::string x_string = x_strs.at( var.get_name() );
  std::string y_string = "Mean SF/" + tswp( var.get_bin_width(), 3 );

  canv.Divide( 1, 1 );
  canv.cd( 1 );
  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  sf_graph->Draw( "HIST" );
  sf_graph->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sf_graph, x_string,  y_string );
  hist_prep_axes( sf_graph, true );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  sf_graph->GetYaxis()->SetRangeUser( 0.5, 1.5 );
  std::string save_name = var.get_name() + "_sf.pdf";
  canv.SaveAs( save_name.c_str() );
  canv.Clear();


}



void muonsf( YAML::Node & run_node, variable_set & variables ){

  prep_style();
  gStyle->SetTitleYOffset( 1.5 );

  mkdir( "./muonsf", 744 );
  chdir( "./muonsf" );

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * sign_tree = fileset->get_tree( sample_type::sign );

  std::vector< std::string > sf_vars = { "MuPos_Pt" , "MuNeg_Pt" , 
                                         "DiLept_Rap" , "DiLept_DeltaR" , 
                                         "qtA" };

  for ( std::string & sf_var : sf_vars ){
    bound & var = variables.bound_manager->get_bound( sf_var );
    TH1F * sf_graph =  average_sf( sign_tree, var ); 
    TH1F * sf_up =  average_sferr_up( sign_tree, var ); 
    for ( int bin = 1; bin <= sf_graph->GetNbinsX(); bin++ ){
      sf_graph->SetBinError( bin, abs(sf_up->GetBinContent(bin)-sf_graph->GetBinContent(bin) ) );
    }
    plot_sf( sf_graph, var );
  }

  chdir( ".." );

}

