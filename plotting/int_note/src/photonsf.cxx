#include <funcs.hxx>
#include <photonsf.hxx>
#include <sys/stat.h>

void photonsf( YAML::Node & run_node, variable_set & variables ){

  prep_style();

  mkdir( "./photonsf", 744 );
  chdir( "./photonsf" );

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * sign_tree = fileset->get_tree( sample_type::sign );

  bound & phot_bound = variables.bound_manager->get_bound( "PhotonPt" );
  std::vector<std::string> photon_bins = phot_bound.get_cut_series();

  //TF1 * lin_fit = new TF1( "lf", "[0]*x+[1]", 10, 25 );
  //TF1 * lin_copy = new TF1( "lfc", "[0]*x+[1]", 5, 25 );

  TH1F * photon_sf = new TH1F( "photosf", "", 20, 5, 25 ); 
  TH1F * photon_sf_err = new TH1F( "photosf_err", "", 20, 5, 25 ); 

  TH1F * photon_l20 = new TH1F( "photosf20", "", 20, 5, 25 ); 
  TH1F * photon_l20_err = new TH1F( "photosf20_err", "", 20, 5, 25 ); 

  TH1F * BDT_rec = new TH1F( "brec", "", 20, -1.0, 1.0 );
  TH1F * BDT_l20 = new TH1F( "bl20", "", 20, -1.0, 1.0 );
  TH1F * BDT_rec_upper = new TH1F( "brec_upper", "", 20, -1.0, 1.0 );
  TH1F * BDT_l20_upper = new TH1F( "bl20_upper", "", 20, -1.0, 1.0 );
  TH1F * BDT_rec_lower = new TH1F( "brec_lower", "", 20, -1.0, 1.0 );
  TH1F * BDT_l20_lower = new TH1F( "bl20_lower", "", 20, -1.0, 1.0 );


  sign_tree->Draw( "BDT>>brec", "photon_sf_rec", "e goff" );
  sign_tree->Draw( "BDT>>brec_upper", "photon_upper_sf_rec", "e goff" );
  sign_tree->Draw( "BDT>>brec_lower", "photon_lower_sf_rec", "e goff" );

  sign_tree->Draw( "BDT>>bl20", "photon_sf_l20_c20", "e goff" );
  sign_tree->Draw( "BDT>>bl20_upper", "photon_upper_sf_l20_c20", "e goff" );
  sign_tree->Draw( "BDT>>bl20_lower", "photon_lower_sf_l20_c20", "e goff" );


  for ( size_t phot_idx = 0; phot_idx < photon_bins.size(); phot_idx++ ){

    TH1F * sfl20 = new TH1F( "sfl20", "", 20, 0, 2.0 ); 
    TH1F * sfl20_err = new TH1F( "sfl20_err", "", 20, 0, 2.0 ); 
    sign_tree->Draw( "photon_sf_l20_c20>>sfl20", photon_bins.at( phot_idx).c_str(), "e goff" );
    sign_tree->Draw( "photon_sf_err_l20_c20>>sfl20_err", photon_bins.at( phot_idx).c_str(), "e goff" );
    photon_l20->SetBinContent( phot_idx+1, sfl20->GetMean() );
    photon_l20_err->SetBinContent( phot_idx+1, sfl20_err->GetMean() );
    delete sfl20;
    delete sfl20_err;

    TH1F * scalefactor  = new TH1F( "sf", "", 20, 0, 2.0 ); 
    TH1F * scalefactor_err  = new TH1F( "sf_err", "", 20, 0, 2.0 ); 
    sign_tree->Draw( "photon_sf_rec>>sf", photon_bins.at( phot_idx).c_str(), "e goff" );
    sign_tree->Draw( "photon_sf_err_rec>>sf_err", photon_bins.at( phot_idx).c_str(), "e goff" );
    photon_sf->SetBinContent( phot_idx+1, scalefactor->GetMean() );
    //photon_by_sf->SetBinError( phot_idx+1, scalefactor->GetStdDev() );
    photon_sf_err->SetBinContent( phot_idx+1, scalefactor_err->GetMean() );
    //photon_by_sf->SetBinError( phot_idx+1, scalefactor->GetStdDev() );
    delete scalefactor;
    delete scalefactor_err;
  }

  std::string x_string = "p_{T,#gamma} [GeV]";
  std::string y_string = "Mean SF/1.000 [GeV^{-1}]";
  std::string y2_string = "Mean SF Error/1.000 [GeV^{-1}]";

  photon_sf->SetLineColor( kBlack );
  photon_sf->SetMarkerColor( kBlack );
  photon_sf->SetMarkerStyle( 20 );
  photon_l20->SetLineColor( kBlack );
  photon_l20->SetMarkerColor( kBlack );
  photon_l20->SetMarkerStyle( 20 );

  photon_sf_err->SetLineColor( kBlack );
  photon_sf_err->SetMarkerColor( kBlack );
  photon_sf_err->SetMarkerStyle( 20 );
  photon_l20_err->SetLineColor( kBlack );
  photon_l20_err->SetMarkerColor( kBlack );
  photon_l20_err->SetMarkerStyle( 20 );

  for ( int bin = 1; bin <= photon_sf->GetNbinsX(); bin++ ){
    photon_sf->SetBinError( bin, photon_sf_err->GetBinContent( bin ) );
    photon_l20->SetBinError( bin, photon_l20_err->GetBinContent( bin ) );
  }


  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  
  canv.Divide( 1, 1 );
  canv.cd( 1 );
  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  photon_sf->Draw( "HIST" );
  photon_sf->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( photon_sf, x_string.c_str(), y_string.c_str() );
  photon_sf->GetYaxis()->SetRangeUser( 0.5, 1.5 ); 
  save_and_clear( canv, active_pad, "photsf_rec");

  canv.Divide( 1, 1 );
  canv.cd( 1 );
  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  photon_sf_err->Draw( "HIST" );
  photon_sf_err->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( photon_sf_err, x_string.c_str(), y2_string.c_str() );
  photon_sf_err->GetYaxis()->SetRangeUser( 0.0, 1.5 );
  save_and_clear( canv, active_pad, "photsf_err_rec");


  photon_sf->SetLineColorAlpha( kRed, 0.5 );
  photon_sf->SetMarkerColorAlpha( kRed, 0.5 );

  canv.Divide( 1, 1 );
  canv.cd( 1 );
  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  photon_l20->Draw( "HIST" );
  photon_sf->Draw( "HIST SAME" );
  photon_sf->Draw( "E1 SAME" );
  photon_sf->Draw( "P SAME" );
  photon_l20->Draw( "HIST SAME" );
  photon_l20->Draw( "E1 SAME" );
  photon_l20->Draw( "P SAME" ); 
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( photon_l20, x_string.c_str(), y_string.c_str() );
  TLegend * l20_legend = create_atlas_legend();
  l20_legend->AddEntry( photon_sf, "Recommended", "LP" );
  l20_legend->AddEntry( photon_l20, "Derived", "LP" );
  l20_legend->SetX1(0.5);
  l20_legend->SetY1(0.65);
  l20_legend->Draw( "SAME" );
  photon_l20->GetYaxis()->SetRangeUser( 0.5, 1.5 );
  save_and_clear( canv, active_pad, "photsf_l20");

  canv.Divide( 1, 1 );
  canv.cd( 1 );
  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  photon_l20_err->Draw( "HIST" );
  photon_l20_err->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( photon_l20_err, x_string.c_str(), y2_string.c_str() );
  photon_l20_err->GetYaxis()->SetRangeUser( 0.0, 0.2 );
  save_and_clear( canv, active_pad, "photsf_err_l20" );


  BDT_rec->SetLineColor( kBlack );
  BDT_l20->SetLineColor( kBlack );
  BDT_rec->SetMarkerStyle( 20 );
  BDT_l20->SetMarkerStyle( 20 );
  BDT_rec_upper->SetLineColor( kRed+1 );
  BDT_l20_upper->SetLineColor( kRed+1 );
  BDT_rec_upper->SetLineColor( kRed+1 );
  BDT_l20_upper->SetLineColor( kRed+1 );





  canv.Divide( 1, 1 );
  canv.cd( 1 );
  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  BDT_rec->Draw( "HIST" );
  BDT_rec->Draw( "P SAME" );
  BDT_rec_upper->Draw( "HIST SAME" );
  BDT_rec_lower->Draw( "HIST SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( BDT_rec, x_string.c_str(), y2_string.c_str() );
  hist_prep_axes( BDT_rec, true );
  BDT_rec->GetYaxis()->SetRangeUser( 0, 70e3 );
  TLegend * rec_leg = below_logo_legend();
  rec_leg->AddEntry( BDT_rec_upper, "Upper Variation" );
  rec_leg->AddEntry( BDT_rec_lower, "Lower Variation" );
  rec_leg->Draw( "SAME" );
  save_and_clear( canv, active_pad, "BDT_rec");

  canv.Divide( 1, 1 );
  canv.cd( 1 );
  active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  BDT_l20->Draw( "HIST" );
  BDT_l20->Draw( "P SAME" );
  BDT_l20_upper->Draw( "HIST SAME" );
  BDT_l20_lower->Draw( "HIST SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( BDT_l20, x_string.c_str(), y2_string.c_str() );
  hist_prep_axes( BDT_l20, true );
  TLegend * l20_leg = below_logo_legend();
  l20_leg->AddEntry( BDT_l20_upper, "Upper Variation" );
  l20_leg->AddEntry( BDT_l20_lower, "Lower Variation" );
  l20_leg->Draw( "SAME" );
  BDT_l20->GetYaxis()->SetRangeUser( 0, 50e3 );
  save_and_clear( canv, active_pad, "BDT_l20" );


  chdir( ".." );

}

