#include <funcs.hxx>
#include <sys/stat.h>
#include <sstream>

  //TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  //input->Draw( "HIST E1" ); 
  //extracted->Draw( "HIST E1 SAME" ); 
  //add_atlas_decorations( active_pad, false );
  //add_internal( active_pad );
  //set_axis_labels( input, val_bound.get_ltx(), "");
  //hist_prep_axes( input );
  //TLegend * legend = below_logo_legend();
  //legend->AddEntry( input, "Subtracted Data", "l" );
  //legend->AddEntry( extracted, "Extracted Yield", "l" );
  //legend->Draw( "SAME" );
  //input->GetYaxis()->SetRangeUser( 100, 10e5 );
  //extracted->GetYaxis()->SetRangeUser( 100, 10e5 );
  //active_pad->SetBottomMargin( 0.1 );
  //active_pad->SetFillStyle( 4000 );
  //active_pad->SetLogy( 1 ); 

  //active_pad = static_cast<TPad *>( canv.cd( 2 ) );
  //ratio->Draw( "HIST E1" );
  //add_atlas_decorations( active_pad, false );
  //add_internal( active_pad );
  //set_axis_labels( ratio, val_bound.get_ltx(), "");
  //hist_prep_axes( ratio );
  //TLegend * legend3 = below_logo_legend();
  //legend3->AddEntry( ratio, "Ratio, Data/Yield", "l" );
  //legend3->Draw( "SAME" );
  //ratio->GetYaxis()->SetRangeUser( 0.0, 2.0 );
  //active_pad->SetBottomMargin( 0.1 );
  //active_pad->SetFillStyle( 4000 );



// Enums and maps for the structure of the table reads.
enum val_type{ val_sign, val_bckg, val_data, val_yield };
std::unordered_map< val_type, std::string > val_string = { {val_sign, "Samples"}, {val_bckg, "Samples" }, 
  {val_data, "Data"}, { val_yield, "Total" } };
std::unordered_map< val_type, int > val_idx = { {val_sign, 0}, {val_bckg, 1 }, { val_data, 0 }, { val_yield, 0 } };
std::unordered_map< val_type, int > val_color = { {val_sign, kRed+1}, {val_bckg, kBlue+1 }, { val_data, kBlack }, { val_yield, kGreen+1} };
std::unordered_map< val_type, int > val_style= { {val_sign, 1}, {val_bckg, 1 }, { val_data, 1 }, { val_yield, 2 } };
std::unordered_map< val_type, std::string > val_name = { {val_sign, "sign"}, {val_bckg, "bckg" }, 
  {val_data, "data"}, { val_yield, "fit" } };


/*
  Structure for a validation variable.

  This is a structure that contains the information to create a combined validation histogram.
  Lots of functions that are associated with this.
  Constructors are heavily dependent on the YAML structure. This needs to reflect two things.
    1. variable_idx, this is the index of the validation variable in the TRExFitter run.
    2. variable_arr, an array with the variable bin edges.
  TODO: Reliant on a lot of enums and other rubbish, needs fixed.
*/
struct val{
  int index;
  bound var_bound;
  std::vector<float> binning;
  TH1F * hist;
  val(){ 
    index = -1;
    binning = {}; 
  }
  val( const std::string & variable, YAML::Node & run_node, variable_set & variables ){
    index = run_node[variable+"_idx"].as<int>();
    var_bound = variables.bound_manager->get_bound( variable );                                       
    binning = run_node[variable+"_arr"].as<std::vector<float>>();                                   
    std::string hist_name = variable + "_" + std::to_string(rand()%100000);
    hist = new TH1F( hist_name.c_str(), "", binning.size()-1, &binning[0] ); 
  }
};

/*
  Function for initialising the validation histogram structs for all variables in a differential fit.

  @param run_node YAML node, passed to constructor
  @param val_vars Vector of strings, names of validation variables  
  @param variables variable set object, used for getting the variable bound associated with the validation variable.
  @return Vector of validation structres
*/
std::vector< val > create_vals( YAML::Node & run_node, const std::vector<std::string> & val_vars, variable_set & variables ){
  std::vector< val > validations = {};
  std::transform( val_vars.cbegin(), val_vars.cend(), std::back_inserter( validations ),
    [ &variables, &run_node ]( const std::string & variable ){
      return val( variable, run_node, variables );
    }  
  );
  return validations;
}

/*
  Function to fill the validation structs

  Combines the output validation hists from a differntial fit into the validation structs 

  @param validation The validation struct.
  @param variables Variable set used for the differential likelihood fit binnings
  @param unique Name of this TRExFitter fit job
  @param type The type (signal, background, etc) of the validation struct
*/
void fill_val( val & validation, variable_set & variables, const std::string & unique, val_type type ){

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string config_path =  std::string( std::getenv("ANA_IP") ) + "/plotting/int_note/share/";
  
  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();

  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string val_path = trex_path + qta_name + "/" + unique + "/Plots";
    val_path += "/VALIDATION" + std::to_string( validation.index ) + "_postfit.yaml";
    YAML::Node val_node = YAML::LoadFile( val_path );
    std::vector< double > yields = val_node[val_string[type]][val_idx[type]]["Yield"].as<std::vector<double>>();
    std::vector< double > errors;
    if ( type == val_yield ){ errors = val_node[val_string[type]][val_idx[type]]["UncertaintyUp"].as<std::vector<double>>(); }
    for ( int bin_idx = 0; bin_idx < validation.hist->GetNbinsX(); bin_idx++ ){
      validation.hist->AddBinContent( bin_idx+1, yields[bin_idx] );
      if (type == val_yield ){
        validation.hist->SetBinError( bin_idx+1, validation.hist->GetBinError(bin_idx+1) + (errors[bin_idx]*errors[bin_idx]) );
      }
    }
  }
  for ( int bin_idx = 0; bin_idx < validation.hist->GetNbinsX(); bin_idx++ ){
    validation.hist->SetBinError( bin_idx+1, std::sqrt( validation.hist->GetBinError(bin_idx+1) ) );
  }
}

/*
  Function to find closure weights for the jpsipt

  Takes two input histograms and produces a closure weight

  @param extracted the extracted validation histogram.
  @param comparison the reference histogram for closure.
  @param val_bound the bound object of the validated variable.

*/
void closure_yield( TH1F * extracted, TH1F * comparison, bound & val_bound ){

  TH1F * ratio = static_cast<TH1F *>( extracted->Clone() );
  ratio->Reset();
  ratio->Divide( comparison, extracted, 1.0, 1.0 );

  TH1F * closure_weights = static_cast<TH1F *>( ratio->Clone() );
  closure_weights->Reset();
  for ( int bin = 1; bin <= ratio->GetNbinsX(); bin++ ){ 
    closure_weights->SetBinContent( bin, ratio->GetBinContent( bin ) );
    closure_weights->SetBinError( bin, ratio->GetBinError( bin ) );
  }
  closure_weights->GetYaxis()->SetRangeUser( 0, 2.0 );
  closure_weights->GetYaxis()->SetRange( 0, 2.0 );

  std::string cwf_name = "./cwf_" + val_bound.get_name() + ".root";
  TFile * closure_weight_file = new TFile( cwf_name.c_str(), "RECREATE" );
  closure_weights->Write( "cw" );
  closure_weight_file->Close();
  gStyle->SetOptStat( "mirse" );
}



void jpsipt( YAML::Node & run_node, variable_set & variables ){

  prep_style();
  gStyle->SetTitleYOffset( 1.5 );

  mkdir( "./jpsipt", 744 );
  chdir( "./jpsipt" );

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "source_unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * bckg = fileset->get_tree( sample_type::bckg );

  std::vector<double> bins = run_node[ "bins" ].as<std::vector<double>>();
  // 8, 10, 12, 18, 26 
  std::string mc_weight = "(subtraction_weight*photon_sf*muon_sf*pu_weight)"
    "*((Lambda>=25.00000)&&(Lambda<=200.00000)&&(abs(qtB)>=0.00000)&&(abs(qtB)<=6.00000))";
  TH1F * raw_yield        = new TH1F( "raw",      "", 4, &bins[0] );
  TH1F * input_yield      = new TH1F( "inyield",  "", 4, &bins[0] );
  //TH1F * extracted_yield  = new TH1F( "extr", "", 4, &bins[0] );

  std::vector< val > bckg_validations = create_vals( run_node, { "DiMuonPt" }, variables );
  fill_val( bckg_validations.at(0), variables,  run_node[ "run_unique" ].as<std::string>(), val_bckg );
  TH1F * validated_bckg = bckg_validations.at(0).hist;

  bckg->Draw( "DiMuonPt>>inyield", "", "e goff" );
  //std::cout << bckg->GetEntries() << std::endl;

  raw_yield->SetBinContent( 1, 1.9812598e6 ); 
  raw_yield->SetBinContent( 2, 2.7067987e6 );
  raw_yield->SetBinContent( 3, 3.8516468e6 );
  raw_yield->SetBinContent( 4, 1.0554351e6 );
  raw_yield->SetBinError( 1, 3.4705886e3 );
  raw_yield->SetBinError( 2, 4.5406917e3 );
  raw_yield->SetBinError( 3, 3.0611014e3 );
  raw_yield->SetBinError( 4, 1.4121758e3 );

  std::cout << input_yield->Integral() << std::endl;
  std::cout << raw_yield->Integral() << std::endl;

  raw_yield->Scale( 1.0/raw_yield->Integral() );
  input_yield->Scale( 1.0/input_yield->Integral() );
  validated_bckg->Scale( 1.0/validated_bckg->Integral() );

  closure_yield( validated_bckg, raw_yield, variables.bound_manager->get_bound( "DiMuonPt" ) );

  raw_yield->SetLineColor( kBlack );
  raw_yield->SetLineStyle( 1 );
  raw_yield->SetLineWidth( 2 );

  input_yield->SetLineColor( kBlue+1 );
  input_yield->SetLineStyle( 2 );

  validated_bckg->SetLineColor( kBlue+1 );
  validated_bckg->SetLineStyle( 2 );
  validated_bckg->SetLineWidth( 2 );

  //std::cout << input_yield->Integral() << std::endl;
  //std::cout << raw_yield->Integral() << std::endl;
  //std::cout << validated_bckg->Integral() << std::endl;

  TH1F * ratio = static_cast<TH1F *>( raw_yield->Clone() );
  ratio->Reset();
  ratio->Divide( validated_bckg, raw_yield, 1.0, 1.0 );


  gStyle->SetOptStat("");

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  raw_yield->Draw( "HIST E1" );
  validated_bckg->Draw( "HIST E1 SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( raw_yield, "p_{T}(J/#psi) [GeV]", "Arbitrary Units" );
  hist_prep_axes( raw_yield, true );
  raw_yield->GetYaxis()->SetTitleOffset( 1.65 );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( raw_yield, "Prompt (ATLAS)", "L" );
  legend->AddEntry( validated_bckg, "Validation", "L" );
  legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "PromptAndValidation" );


  canv.Divide( 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  ratio->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( ratio, "p_{T}(J/#psi) [GeV]", "Ratio" );
  hist_prep_axes( ratio, true );
  raw_yield->GetYaxis()->SetTitleOffset( 1.65 );
  TLegend * ratio_legend  = below_logo_legend();
  ratio_legend->AddEntry( ratio, "Ratio" );
  ratio_legend->Draw( "SAME" );
  ratio_legend->SetX1NDC(0.4);
  ratio->GetYaxis()->SetRangeUser( 0.0, 2.0 );
  save_and_clear( canv, active_pad, "ClosureWeight" );

  chdir( ".." );

}
