#include <lower.hxx>
#include <sys/stat.h>


void progressive( YAML::Node & run_node, variable_set & variables ){

  gStyle->SetOptStat("");
  
  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  bound & qtb = variables.bound_manager->get_bound( "qtB" );
  std::vector<std::string> qtb_series = qtb.get_cut_series();
  std::vector<std::string> qtb_names = qtb.get_series_names( 0, true );
  std::vector< TH1F * > hists = {};
  hists.reserve( qtb_series.size() );

  //TTree * sign_tree = fileset->get_tree( sign );
  //TTree * bckg_tree = fileset->get_tree( bckg );
  TTree * sign_tree = fileset->get_tree( sign );

  std::vector< int > marker = { 20, 21, 22, 23, 24, 25, 26, 27, 28, 32, 34 };
  std::vector< int > colors = { kRed+1, kRed+1, kRed+1, kMagenta+3, kMagenta+3, 
                                kMagenta+3, kMagenta+3, kMagenta+3, kMagenta+3, 
                                kMagenta+3 };

  for ( size_t idx = 0; idx < qtb_series.size(); idx++ ){
    hists.push_back( new TH1F ( qtb_names.at(idx).c_str(), "", 15, -10, 20 ) );
    std::string drawstring = "qtA>>" + qtb_names.at(idx);
    sign_tree->Draw( drawstring.c_str(), qtb_series.at(idx).c_str(), "e goff" );
    hists.at(idx)->SetMarkerColorAlpha( colors.at(idx), 1.0 - 0.08*idx );
    hists.at(idx)->SetMarkerStyle( marker.at(idx) );
    hists.at(idx)->Scale( 1.0/hists.at(idx)->Integral() );
    hists.at(idx)->SetLineWidth( 0 );
  }


  TH1F * first = hists.front();
  std::string x_string = "q_{T}^{A} [GeV]";
  std::string y_string = "Arbitrary Units";


  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  first->Draw( "P" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( first, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( first );
  TLegend * prog_legend = create_atlas_legend();
  //prog_legend->SetX1( 0.6 );
  //prog_legend->SetY1( 0.4 );

  int qtbwin = 0;

  for ( TH1F * hist : hists ){  
    hist->Draw( "P SAME" );
    //
    //std::string prog_name = std::to_string(win*2) 
    std::string prog_name = std::to_string( qtbwin*2) + " < |q_{T}^{B}| < " + std::to_string( (qtbwin+1)*2 ) + " GeV";
    prog_legend->AddEntry( hist, prog_name.c_str() );
    qtbwin++;
  }
  prog_legend->SetY2( 0.775 );
  prog_legend->SetY1( 0.35 );
  prog_legend->SetX1( 0.55 );
  prog_legend->SetX2( 0.8 );

  prog_legend->Draw();
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "qta_prog.pdf" );
  canv.Clear();






}

void sbr( YAML::Node & run_node, variable_set & variables ){

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  std::cout << variables.analysis_variable << std::endl; 
  
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * sign_tree = fileset->get_tree( sign );
  TTree * bckg_tree = fileset->get_tree( bckg );

  TH2F * sign_scatter = new TH2F( "sign", "", 30, -10, 20, 30, -20, 20 );
  TH2F * bckg_scatter = new TH2F( "bckg", "", 30, -10, 20, 30, -20, 20 );

  TH1F * sign_qtb = new TH1F( "signb", "", 40, 0, 20 );
  TH1F * bckg_qtb = new TH1F( "bckgb", "", 40, 0, 20 );

  sign_tree->Draw( "qtB:qtA>>sign", "", "goff" );
  bckg_tree->Draw( "qtB:qtA>>bckg", "", "goff" );
  sign_tree->Draw( "abs(qtB)>>signb", "", "goff" );
  bckg_tree->Draw( "abs(qtB)>>bckgb", "", "goff" );

  sign_qtb->Scale( 1.0/sign_qtb->Integral() );
  bckg_qtb->Scale( 1.0/bckg_qtb->Integral() );

  std::string x_string( "q_{T}^{A} [GeV]" );
  std::string y_string( "q_{T}^{B} [GeV]" );
  std::string x2_string( "|q_{T}^{B}| [GeV]" );
  std::string y2_string( "Normalised Entries" );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  sign_scatter->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_scatter, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( sign_scatter );
  sign_scatter->GetYaxis()->SetRangeUser( -20, 20 );
  save_and_clear( canv, active_pad, "sign_map" );

  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  bckg_scatter->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bckg_scatter, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( bckg_scatter );
  bckg_scatter->GetYaxis()->SetRangeUser( -20, 20 );
  save_and_clear( canv, active_pad, "bckg_map" );


  sign_qtb->SetLineColor( kRed+1 );
  bckg_qtb->SetLineColor( kBlue+1 );
  sign_qtb->SetMarkerColor( kRed+1 );
  bckg_qtb->SetMarkerColor( kBlue+1 );
  sign_qtb->SetMarkerStyle( 20 );
  bckg_qtb->SetMarkerStyle( 20 );

  TH1F * ratio = static_cast<TH1F*>(sign_qtb->Clone() );
  ratio->Reset();
  ratio->SetMarkerColor( kBlack );
  ratio->SetLineColor( kBlack );
  ratio->SetMarkerStyle( 20 );
  ratio->Divide( sign_qtb, bckg_qtb, 1.0, 1.0 );


  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  sign_qtb->Draw( "HIST P E1" );
  bckg_qtb->Draw( "HIST P E1 SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_qtb, x2_string.c_str(), y2_string.c_str() );
  hist_prep_axes( sign_qtb );
  TLegend * sb_legend = create_atlas_legend();
  sb_legend->AddEntry( sign_qtb, "sign", "LP" );
  sb_legend->AddEntry( bckg_qtb, "bckg", "LP" );
  sb_legend->Draw("SAME");
  save_and_clear( canv, active_pad, "qtb_sb" );

  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  ratio->Draw( "HIST P E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( ratio, x2_string.c_str(), "S/B" );
  hist_prep_axes( ratio );
  ratio->GetYaxis()->SetRangeUser( 0, 3.0 );
  TLegend * rat_legend = below_logo_legend();
  rat_legend->AddEntry( ratio, "ratio" );
  rat_legend->Draw("SAME");
  save_and_clear( canv, active_pad, "ratio" );

  sign_qtb->SetLineWidth( 0 );
  bckg_qtb->SetLineWidth( 0 );
  gStyle->SetOptStat("imr");
  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  TH1F * other_rat = ratio_pad( sign_qtb, bckg_qtb, active_pad );
  other_rat->SetMarkerColor( kBlack );
  other_rat->SetLineWidth( 0 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  sign_qtb->SetStats( false );
  bckg_qtb->SetStats( false );
  sign_qtb->Draw( "SAME E1" );
  bckg_qtb->Draw( "SAME E1" );
  sign_qtb->GetYaxis()->SetRangeUser( 0.0, 0.125 );
  other_rat->SetMarkerStyle( 20 );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  save_and_clear( canv, active_pad, "other_ratio" );

}

void lower_sb( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./lower", 744 );
  chdir( "./lower" );

  gStyle->SetTitleYOffset(1.4);
  sbr( run_node, variables );
  progressive( run_node, variables );

  chdir( ".." );




}

  

