#include <subtraction.hxx>
#include <sys/stat.h>

void subtraction_plots( YAML::Node & run_node, variable_set & variables ){

  

  prep_style();
  gStyle->SetOptStat( "" );
  gStyle->SetTitleYOffset( 1.55 );
  
  mkdir( "./sub", 744 );
  chdir( "./sub" );

  demo_plots( run_node, variables ); 
  proof( run_node, variables ); 

  chdir( ".." );
  
}

void demo_plots( YAML::Node & run_node, variable_set & variables ){

  std::string unique = run_node["unique"].as<std::string>();
  std::string filepath = std::string( getenv("OUT_PATH" ) ) + "/event_subtraction/" + unique;

  basic_fileset * sub_fileset = new basic_fileset();
  sub_fileset->set_unique( unique );
  sub_fileset->load_subtraction_fileset( filepath, unique, false  );

  if ( variables.bound_manager == nullptr ){ return; }

  bound & dimuon_m_bound = variables.bound_manager->get_bound( "DiMuonMass" );
  bound & dimuon_t_bound = variables.bound_manager->get_bound( "DiMuonTau" );

  TTree * data_tree = sub_fileset->data_tree;
  TTree * sign_tree = sub_fileset->sign_tree;
  TTree * bckg_tree = sub_fileset->bckg_tree;

  std::string bbbg_filepath = filepath + "/bbbg_" + unique + ".root";
  TFile * bbbg_file = new TFile( bbbg_filepath.c_str(), "READ" );
  TTree * bbbg_tree = static_cast< TTree *>( bbbg_file->Get("tree") );
  
  TH1F * m_region_1 = new TH1F( "m1", "", 80, 2700, 3500 ); 
  TH1F * m_region_2 = new TH1F( "m2", "", 80, 2700, 3500 ); 
  TH1F * m_region_3 = new TH1F( "m3", "", 80, 2700, 3500 );
  TH1F * m_region_4 = new TH1F( "m4", "", 80, 2700, 3500 );
  TH1F * t_region_1 = new TH1F( "t1", "", 120, -5.16, 15.48 ); 
  TH1F * t_region_2 = new TH1F( "t2", "", 120, -5.16, 15.48 ); 

  data_tree->Draw( "DiMuonMass>>m1", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>m2", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 )", "e goff" );
  data_tree->Draw( "DiMuonMass>>m3", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 )", "e goff" );
  data_tree->Draw( "DiMuonMass>>m4", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 )", "e goff" );
  data_tree->Draw( "DiMuonTau>>t1", "( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" );
  data_tree->Draw( "DiMuonTau>>t2", "( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" );

  //Int_t xanthous = TColor::GetColor( 247, 181, 56 );
  //Int_t ochre = TColor::GetColor( 219, 124, 38 ); 
  //Int_t fer = TColor::GetColor( 195, 47, 39 );
  //Int_t burg = TColor::GetColor( 120, 1, 22 );
  //Int_t night = TColor::GetColor( 1, 17, 10 );
  //Int_t orng = TColor::GetColor( 186, 45, 11 );
  //Int_t brg = TColor::GetColor( 0, 62, 31 );
  //Int_t da_g = TColor::GetColor( 37, 68, 65 );
  //Int_t cadet = TColor::GetColor( 37, 40, 61 );
  //Int_t cadet = TColor::GetColor( 47, 50, 71 );

  //Int_t da_g = TColor::GetColor( 10+57, 50+88, 10+85 );

  Int_t dp = TColor::GetColor( "#331832" );
  Int_t rsp = TColor::GetColor( "#D81E5B" );
  Int_t verm = TColor::GetColor( "#F0544F" );
  Int_t ash = TColor::GetColor( "#C6D8D3" );

  //ochre, fer/brg, da_g, cadet
  m_region_1->SetMarkerStyle( 0 );
  m_region_2->SetMarkerStyle( 0 );  
  m_region_3->SetMarkerStyle( 0 );
  m_region_4->SetMarkerStyle( 0 );
  t_region_1->SetMarkerStyle( 0 );
  t_region_2->SetMarkerStyle( 0 );

  m_region_1->SetLineColor( 1 );
  m_region_2->SetLineColor( 1 );  
  m_region_3->SetLineColor( 1 );
  m_region_4->SetLineColor( 1 );
  t_region_1->SetLineColor( 1 );
  t_region_2->SetLineColor( 1 );


  //m_region_1->SetMarkerSize( 1.0 );
  //m_region_2->SetMarkerSize( 1.0 );  
  //m_region_3->SetMarkerSize( 1.0 );
  //m_region_4->SetMarkerSize( 1.0 );
  //t_region_1->SetMarkerSize( 1.0 );
  //t_region_2->SetMarkerSize( 1.0 );

  //m_region_1->SetMarkerColor( ochre );
  //m_region_2->SetMarkerColor( fer   );  
  //m_region_3->SetMarkerColor( da_g  );
  //m_region_4->SetMarkerColor( cadet );
  //t_region_1->SetMarkerColor( fer   );
  //t_region_2->SetMarkerColor( da_g  );
  
  m_region_1->SetFillColor( dp );
  m_region_2->SetFillColor( rsp   );  
  m_region_3->SetFillColor( verm  );
  m_region_4->SetFillColor( ash );
  t_region_1->SetFillColor( dp  );
  t_region_2->SetFillColor( rsp  );

  m_region_1->SetFillStyle( 1001 );
  m_region_2->SetFillStyle( 1001 );  
  m_region_3->SetFillStyle( 1001 );
  m_region_4->SetFillStyle( 1001 );
  t_region_1->SetFillStyle( 1001 );
  t_region_2->SetFillStyle( 1001 );

  m_region_1->SetLineWidth( 0 );
  m_region_2->SetLineWidth( 0 );  
  m_region_3->SetLineWidth( 0 );
  m_region_4->SetLineWidth( 0 );
  t_region_1->SetLineWidth( 0 );
  t_region_2->SetLineWidth( 0 );


  

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  m_region_2->Draw( "HIST " );
  //m_region_2->Draw( "P SAME" );
  m_region_1->Draw( "HIST  SAME ");
  //m_region_1->Draw( "P SAME ");
  m_region_3->Draw( "HIST  SAME ");
  //m_region_3->Draw( "P SAME ");
  m_region_4->Draw( "HIST  SAME ");
  //m_region_4->Draw( "P SAME ");
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( m_region_2, true );
  std::string m_y_string =  "Entries";//qta_bound.get_y_str();
  std::string m_x_string =  "m_{#mu#mu} [" + dimuon_m_bound.get_units() + "]";
  set_axis_labels( m_region_2, m_x_string.c_str(), m_y_string.c_str() );
  TLegend * m_legend = create_atlas_legend();
  m_legend->Clear();
  m_legend->AddEntry( m_region_1, "m_{#mu#mu} Region 1", "F" );
  m_legend->AddEntry( m_region_2, "m_{#mu#mu} Region 2", "F" );
  m_legend->AddEntry( m_region_3, "m_{#mu#mu} Region 3", "F" );
  m_legend->AddEntry( m_region_4, "m_{#mu#mu} Region 4", "F" );
  m_legend->Draw( "SAME" );
  m_region_2->GetYaxis()->SetRangeUser( 0, m_region_2->GetMaximum()*1.1 );
  std::cout << m_region_2->GetXaxis()->GetLabelOffset() << std::endl;
  m_region_2->GetXaxis()->SetLabelOffset( 0.01 );
  m_region_2->GetYaxis()->SetRangeUser( 0, 35e3 );
  save_and_clear( canv, active_pad, "MassSubDemo" );


  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  t_region_1->Draw( "HIST E" );
  //t_region_1->Draw( "P SAME" );
  t_region_2->Draw( "HIST E SAME" );
  //t_region_2->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  //std::string t_y_string =  "Entries/0.172 [ps^{-1}]";//qta_bound.get_y_str();
  std::string t_y_string =  "Entries";//qta_bound.get_y_str();
  std::string t_x_string =  dimuon_t_bound.get_ltx() + " [" + dimuon_t_bound.get_units() + "]";
  set_axis_labels( t_region_1, t_x_string.c_str(), t_y_string.c_str() );
  hist_prep_axes( t_region_1, true );
  t_region_1->GetYaxis()->SetRangeUser( 0, t_region_1->GetMaximum()*1.2 );
  TLegend * t_legend = create_atlas_legend();
  t_legend->Clear();
  t_legend->AddEntry( t_region_1, "#tau_{#mu#mu} Region 1", "F" );
  t_legend->AddEntry( t_region_2, "#tau_{#mu#mu} Region 2", "F" );
  t_legend->Draw( "SAME" );
  t_region_1->GetYaxis()->SetRangeUser( 0, 0.155e6 );
  save_and_clear( canv, active_pad, "TauSubDemo" );


  std::ofstream subtraction_table( "subtraction.tex" );
  subtraction_table << "\\documentclass{article}\n";
  subtraction_table << "\\usepackage{geometry}\n";
  subtraction_table << "\\begin{document}";
  subtraction_table << std::fixed << std::setprecision( 0 );
  subtraction_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{ |c|c|r| }\\hline\n";
  subtraction_table << "Subtraction Region & Cut & Integral\\\\ \\hline \n";
  subtraction_table << "M1(10) & M_{#mu#mu} > 2.7 GeV \\&\\& M_{#mu#mu} < 2.9 " << m_region_1->Integral() << " \\\\ \\hline \n";
  subtraction_table << "M2(20) & M_{#mu#mu} > 2.9 GeV \\&\\& M_{#mu#mu} < 3.1 " << m_region_2->Integral() << " \\\\ \\hline \n";
  subtraction_table << "M3(30) & M_{#mu#mu} > 3.1 GeV \\&\\& M_{#mu#mu} < 3.3 " << m_region_3->Integral() << " \\\\ \\hline \n";
  subtraction_table << "M4(40) & M_{#mu#mu} > 3.3 GeV \\&\\& M_{#mu#mu} < 3.5 " << m_region_4->Integral() << " \\\\ \\hline \n";
  subtraction_table << "T1(01) &  #tau_{#mu#mu} > -5.0 ps \\&\\& #tau_{#mu#mu} < 0.86 ps & " << t_region_1->Integral() << " \\\\ \\hline \n";
  subtraction_table << "T2(02) &  #tau_{#mu#mu} > 0.86 ps \\&\\& #tau_{#mu#mu} < 15.0 ps & " << t_region_2->Integral() << " \\\\ \\hline \n";
  subtraction_table << "\\\\ \\hline\n";
  subtraction_table << "\\end{tabular}\n\\caption{Subtraction Region Integrals}\n\\end{table}\n\\end{document}";
  subtraction_table.close();


  TH1F * dh11 = new TH1F( "dh11", "", 80, 2700, 3500 ); 
  TH1F * dh12 = new TH1F( "dh12", "", 80, 2700, 3500 ); 
  TH1F * dh21 = new TH1F( "dh21", "", 80, 2700, 3500 );
  TH1F * dh22 = new TH1F( "dh22", "", 80, 2700, 3500 );
  TH1F * dh31 = new TH1F( "dh31", "", 80, 2700, 3500 ); 
  TH1F * dh32 = new TH1F( "dh32", "", 80, 2700, 3500 ); 
  TH1F * dh41 = new TH1F( "dh41", "", 80, 2700, 3500 );
  TH1F * dh42 = new TH1F( "dh42", "", 80, 2700, 3500 );

  TH1F * dh71 = new TH1F( "dh71", "", 80, 2700, 3500 );
  TH1F * dh72 = new TH1F( "dh72", "", 80, 2700, 3500 );
  TH1F * dh81 = new TH1F( "dh81", "", 80, 2700, 3500 );
  TH1F * dh82 = new TH1F( "dh82", "", 80, 2700, 3500 );
  TH1F * dh91 = new TH1F( "dh91", "", 80, 2700, 3500 );
  TH1F * dh92 = new TH1F( "dh92", "", 80, 2700, 3500 );
  TH1F * dh99 = new TH1F( "dh99", "", 80, 2700, 3500 );

  TH1F * bh11 = new TH1F( "bh11", "", 80, 2700, 3500 ); 
  TH1F * bh12 = new TH1F( "bh12", "", 80, 2700, 3500 ); 
  TH1F * bh21 = new TH1F( "bh21", "", 80, 2700, 3500 );
  TH1F * bh22 = new TH1F( "bh22", "", 80, 2700, 3500 );
  TH1F * bh31 = new TH1F( "bh31", "", 80, 2700, 3500 ); 
  TH1F * bh32 = new TH1F( "bh32", "", 80, 2700, 3500 ); 
  TH1F * bh41 = new TH1F( "bh41", "", 80, 2700, 3500 );
  TH1F * bh42 = new TH1F( "bh42", "", 80, 2700, 3500 );

  TH1F * bh71 = new TH1F( "bh71", "", 80, 2700, 3500 );
  TH1F * bh72 = new TH1F( "bh72", "", 80, 2700, 3500 );
  TH1F * bh81 = new TH1F( "bh81", "", 80, 2700, 3500 );
  TH1F * bh82 = new TH1F( "bh82", "", 80, 2700, 3500 );
  TH1F * bh91 = new TH1F( "bh91", "", 80, 2700, 3500 );
  TH1F * bh92 = new TH1F( "bh92", "", 80, 2700, 3500 );
  TH1F * bh99 = new TH1F( "bh99", "", 80, 2700, 3500 );


  TH1F * sh11 = new TH1F( "sh11", "", 80, 2700, 3500 ); 
  TH1F * sh12 = new TH1F( "sh12", "", 80, 2700, 3500 ); 
  TH1F * sh21 = new TH1F( "sh21", "", 80, 2700, 3500 );
  TH1F * sh22 = new TH1F( "sh22", "", 80, 2700, 3500 );
  TH1F * sh31 = new TH1F( "sh31", "", 80, 2700, 3500 ); 
  TH1F * sh32 = new TH1F( "sh32", "", 80, 2700, 3500 ); 
  TH1F * sh41 = new TH1F( "sh41", "", 80, 2700, 3500 );
  TH1F * sh42 = new TH1F( "sh42", "", 80, 2700, 3500 );

  TH1F * sh71 = new TH1F( "sh71", "", 80, 2700, 3500 );
  TH1F * sh72 = new TH1F( "sh72", "", 80, 2700, 3500 );
  TH1F * sh81 = new TH1F( "sh81", "", 80, 2700, 3500 );
  TH1F * sh82 = new TH1F( "sh82", "", 80, 2700, 3500 );
  TH1F * sh91 = new TH1F( "sh91", "", 80, 2700, 3500 );
  TH1F * sh92 = new TH1F( "sh92", "", 80, 2700, 3500 );
  TH1F * sh99 = new TH1F( "sh99", "", 80, 2700, 3500 );

  
  TH1F * gh11 = new TH1F( "gh11", "", 80, 2700, 3500 ); 
  TH1F * gh12 = new TH1F( "gh12", "", 80, 2700, 3500 ); 
  TH1F * gh21 = new TH1F( "gh21", "", 80, 2700, 3500 );
  TH1F * gh22 = new TH1F( "gh22", "", 80, 2700, 3500 );
  TH1F * gh31 = new TH1F( "gh31", "", 80, 2700, 3500 ); 
  TH1F * gh32 = new TH1F( "gh32", "", 80, 2700, 3500 ); 
  TH1F * gh41 = new TH1F( "gh41", "", 80, 2700, 3500 );
  TH1F * gh42 = new TH1F( "gh42", "", 80, 2700, 3500 );
  
  TH1F * gh71 = new TH1F( "gh71", "", 80, 2700, 3500 );
  TH1F * gh72 = new TH1F( "gh72", "", 80, 2700, 3500 );
  TH1F * gh81 = new TH1F( "gh81", "", 80, 2700, 3500 );
  TH1F * gh82 = new TH1F( "gh82", "", 80, 2700, 3500 );
  TH1F * gh91 = new TH1F( "gh91", "", 80, 2700, 3500 );
  TH1F * gh92 = new TH1F( "gh92", "", 80, 2700, 3500 );
  TH1F * gh99 = new TH1F( "gh99", "", 80, 2700, 3500 );


  TH1F * dh00 = new TH1F( "dh00", "", 80, 2700, 3500 ); 
  TH1F * bh00 = new TH1F( "bh00", "", 80, 2700, 3500 ); 
  TH1F * sh00 = new TH1F( "sh00", "", 80, 2700, 3500 ); 
  TH1F * gh00 = new TH1F( "gh00", "", 80, 2700, 3500 ); 

  data_tree->Draw( "DiMuonMass>>dh00", "", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh00", "", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh00", "", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh00", "", "e goff" ); 

  data_tree->Draw( "DiMuonMass>>dh11", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>dh12", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>dh21", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>dh22", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>dh31", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>dh32", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>dh41", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  data_tree->Draw( "DiMuonMass>>dh42", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 

  sign_tree->Draw( "DiMuonMass>>sh11", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh12", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh21", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh22", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh31", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh32", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh41", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  sign_tree->Draw( "DiMuonMass>>sh42", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 

  bckg_tree->Draw( "DiMuonMass>>bh11", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh12", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh21", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh22", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh31", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh32", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh41", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bckg_tree->Draw( "DiMuonMass>>bh42", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 

  bbbg_tree->Draw( "DiMuonMass>>gh11", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh12", "( DiMuonMass > 2700 ) & ( DiMuonMass < 2900 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh21", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh22", "( DiMuonMass > 2900 ) & ( DiMuonMass < 3100 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh31", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh32", "( DiMuonMass > 3100 ) & ( DiMuonMass < 3300 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh41", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > -5.0 ) & ( DiMuonTau < 0.86 )", "e goff" ); 
  bbbg_tree->Draw( "DiMuonMass>>gh42", "( DiMuonMass > 3300 ) & ( DiMuonMass < 3500 ) & ( DiMuonTau > 0.86 ) & ( DiMuonTau < 15.0 )", "e goff" ); 


  dh71->Add( dh21, dh31, 1.0, 1.0 );
  dh81->Add( dh11, dh41, 1.0, 1.0 );
  dh72->Add( dh22, dh32, 1.0, 1.0 );
  dh82->Add( dh12, dh42, 1.0, 1.0 );
  dh91->Add( dh71, dh81, 1.0, -1.0 );
  dh92->Add( dh72, dh82, 1.0, -1.0 );
  dh99->Add( dh91, dh92, 1.0, -1.0 );

  bh71->Add( bh21, bh31, 1.0, 1.0 );
  bh81->Add( bh11, bh41, 1.0, 1.0 );
  bh72->Add( bh22, bh32, 1.0, 1.0 );
  bh82->Add( bh12, bh42, 1.0, 1.0 );
  bh91->Add( bh71, bh81, 1.0, -1.0 );
  bh92->Add( bh72, bh82, 1.0, -1.0 );
  bh99->Add( bh91, bh92, 1.0, -1.0 );

  sh71->Add( sh21, sh31, 1.0, 1.0 );
  sh81->Add( sh11, sh41, 1.0, 1.0 );
  sh72->Add( sh22, sh32, 1.0, 1.0 );
  sh82->Add( sh12, sh42, 1.0, 1.0 );
  sh91->Add( sh71, sh81, 1.0, -1.0 );
  sh92->Add( sh72, sh82, 1.0, -1.0 );
  sh99->Add( sh91, sh92, 1.0, -1.0 );

  gh71->Add( gh21, gh31, 1.0, 1.0 );
  gh81->Add( gh11, gh41, 1.0, 1.0 );
  gh72->Add( gh22, gh32, 1.0, 1.0 );
  gh82->Add( gh12, gh42, 1.0, 1.0 );
  gh91->Add( gh71, gh81, 1.0, -1.0 );
  gh92->Add( gh72, gh82, 1.0, -1.0 );
  gh99->Add( gh91, gh92, 1.0, -1.0 );


  std::ofstream full_subtraction_table( "full_subtraction.tex" );
  full_subtraction_table << "\\documentclass{article}\n";
  full_subtraction_table << "\\usepackage{geometry}\n";
  full_subtraction_table << "\\begin{document}";
  full_subtraction_table << std::fixed << std::setprecision( 0 );
  full_subtraction_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{ |c|r|r|r|r| }\\hline\n";
  full_subtraction_table << "Subtraction Region & data & sign & bckg & bbbg \\\\ \\hline \n";
  full_subtraction_table << " 00 & " << dh00->Integral() << "&" << sh00->Integral() << "&" << bh00->Integral() << "&" << gh00->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 11 & " << dh11->Integral() << "&" << sh11->Integral() << "&" << bh11->Integral() << "&" << gh11->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 12 & " << dh12->Integral() << "&" << sh12->Integral() << "&" << bh12->Integral() << "&" << gh12->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 21 & " << dh21->Integral() << "&" << sh21->Integral() << "&" << bh21->Integral() << "&" << gh21->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 22 & " << dh22->Integral() << "&" << sh22->Integral() << "&" << bh22->Integral() << "&" << gh22->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 31 & " << dh31->Integral() << "&" << sh31->Integral() << "&" << bh31->Integral() << "&" << gh31->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 32 & " << dh32->Integral() << "&" << sh32->Integral() << "&" << bh32->Integral() << "&" << gh32->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 41 & " << dh41->Integral() << "&" << sh41->Integral() << "&" << bh41->Integral() << "&" << gh41->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 42 & " << dh42->Integral() << "&" << sh42->Integral() << "&" << bh42->Integral() << "&" << gh42->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 71 & " << dh71->Integral() << "&" << sh71->Integral() << "&" << bh71->Integral() << "&" << gh71->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 72 & " << dh72->Integral() << "&" << sh72->Integral() << "&" << bh72->Integral() << "&" << gh72->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 81 & " << dh81->Integral() << "&" << sh81->Integral() << "&" << bh81->Integral() << "&" << gh81->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 82 & " << dh82->Integral() << "&" << sh82->Integral() << "&" << bh82->Integral() << "&" << gh82->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 91 & " << dh91->Integral() << "&" << sh91->Integral() << "&" << bh91->Integral() << "&" << gh91->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 92 & " << dh92->Integral() << "&" << sh92->Integral() << "&" << bh92->Integral() << "&" << gh92->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << " 99 & " << dh99->Integral() << "&" << sh99->Integral() << "&" << bh99->Integral() << "&" << gh99->Integral() << " \\\\ \\hline \n";
  full_subtraction_table << "\\end{tabular}\n\\caption{Subtraction Region Integrals}\n\\end{table}\n\\end{document}";
  subtraction_table.close();




}




void proof( YAML::Node & run_node, variable_set & variables ){ 


  std::string unique = run_node["unique"].as<std::string>();
  std::string filepath = std::string( getenv("OUT_PATH" ) ) + "/event_subtraction/" + unique;
  filepath += "/bbbg_" + unique + ".root";

  TFile * bbbg_file = new TFile( filepath.c_str(), "READ" );
  TTree * bbbg_tree = static_cast< TTree *>( bbbg_file->Get("tree") );
  //basic_fileset * sub_fileset = new basic_fileset();
  //sub_fileset->set_unique( unique );
  //sub_fileset->load_subtraction_fileset( filepath, unique, false );

  bound & qta_bound = variables.analysis_bound;
  TH1F * presub = new TH1F(   "pre",  "", 50, -10, 20 );
  TH1F * postsub = new TH1F(  "post", "", 50, -10, 20 );

  bbbg_tree->Draw( "qtA>>pre", "", "goff" );
  bbbg_tree->Draw( "qtA>>post", "subtraction_weight", "goff" );

  presub->SetMarkerSize( 0 );
  postsub->SetMarkerSize( 0  );
  presub->SetFillColor( kGray+1 );
  postsub->SetFillColor( rapsberry );
  presub->SetLineColor( kGray+1 );
  postsub->SetLineColor( rapsberry );
  //presub->SetMarkerSize( 1.0 );
  //postsub->SetMarkerSize( 1.0 );

  TF1 * line = prep_line( -10.0, 20.0  );
  line->SetLineColor( 1.0 );
  line->SetParameter( 0, 0 );
  line->SetParameter( 1, 0 );
  line->SetLineStyle( 1 );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  presub->Draw( "HIST SAME" );
  postsub->Draw( "HIST SAME" );
  line->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  hist_prep_axes( presub, true );
  std::string ana_y_string =  "Entries";//qta_bound.get_y_str();
  std::string ana_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
  set_axis_labels( presub, ana_x_string.c_str(), ana_y_string.c_str() );
  TLegend * sub_legend = below_logo_legend();
  sub_legend->AddEntry( presub, "Before subtraction", "LF" );
  sub_legend->AddEntry( postsub, "After subtraction", "LF" );
  sub_legend->Draw();
  presub->GetYaxis()->SetRangeUser( -500, 3750 );
  save_and_clear( canv, active_pad, "subtraction_proof" );

  std::cout << presub->Integral() << std::endl;
  std::cout << postsub->Integral() << std::endl;


}
