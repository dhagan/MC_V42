#include <region.hxx>
#include <sys/stat.h>

void region( YAML::Node & run_node, variable_set & variables ){

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  std::cout << variables.analysis_variable << std::endl; 
  
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * sign_tree = fileset->get_tree( sign );
  TTree * bckg_tree = fileset->get_tree( bckg );
  TH2F * sign_region = new TH2F( "sign", "", 50, 0, 5.0, 50, 0, M_PI );
  TH2F * bckg_region = new TH2F( "bckg", "", 50, 0, 5.0, 50, 0, M_PI );
  sign_tree->Draw( "abs(3.141592-AbsdPhi):AbsdY>>sign", "", "e goff" );
  bckg_tree->Draw( "abs(3.141592-AbsdPhi):AbsdY>>bckg", "", "e goff" );

  //TF2 * bound = new TF2( "100* (x*x)/1.", 0,5,0,M_PI );

  //"((abs(3.141592-AbsdPhi)*abs(3.141592-AbsdPhi) )/(1.2*1.2) + ( (AbsdY)*(AbsdY) )/(2.0*2.0) ) < 1"  
  //"(y*y)/(1.2*1.2) + ( (x)*(x) )/(2.0*2.0) ) = 1"  
  //y = 1.2*sqrt( 1 - x*x/4)

  TF1 * bf = new TF1( "bound", "1.5*sqrt(1.0 - (x*x)/(2.5001*2.5001) )", 0.0, 2.50005 );
  bf->SetNpx( 10000 );
  //for ( int x = 0; x < 100; x++ ){
  //  std::cout << "(" << x/20.0 << "," << bf->Eval( x/20.0 )  << " ";
  //} std::cout << std::flush;

  //TPaveText * region_text = new TPaveText();
  //region_text->AddText( 1.0, 0.75, "SR");
  //region_text->AddText( 3.0, 1.75, "CR");
  //region_text->SetTextColor( kRed );
  

  std::string x_string = "|#Delta Y|/0.1";
  std::string y_string = "|#Delta#phi_{0}|/0.063 [rad]";

  mkdir( "./region", 744 );
  chdir( "./region" );

  TCanvas * canv = new TCanvas( "canv", "", 200, 200, 1000, 1000 );
  canv->Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv->cd( 1 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  sign_region->Draw( "COLZ" );
  bf->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_region, x_string.c_str(), y_string.c_str() );
  active_pad->SetFillStyle( 4000 );
  canv->SetFillStyle( 4000 );
  TLatex * region_text = new TLatex();
  region_text->SetTextAlign( 22 );
  region_text->SetTextColor( kRed );
  region_text->DrawLatex( 1.0, 0.75, "SR" );
  region_text->SetTextAlign( 22 );
  region_text->DrawLatex( 2.25, 1.5, "CR" );
  canv->SaveAs( "sign_region.pdf" );
  canv->Clear();
  
  canv->Divide( 1 );
  active_pad = static_cast< TPad *>( canv->cd( 1 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  bckg_region->Draw( "COLZ" );
  bf->Draw( "SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bckg_region, x_string.c_str(), y_string.c_str() );
  active_pad->SetFillStyle( 4000 );
  canv->SetFillStyle( 4000 );
  TLatex * region_text2 = new TLatex();
  region_text2->SetTextAlign( 22 );
  region_text2->SetTextColor( kRed );
  region_text2->DrawLatex( 1.0, 0.75, "SR" );
  region_text2->SetTextAlign( 22 );
  region_text2->DrawLatex( 2.25, 1.5, "CR" );
  canv->SaveAs( "bckg_region.pdf" );
  canv->Clear();
  

  chdir( ".." );




}
