#include <funcs.hxx>
#include <sys_internal.hxx>
#include <sys/stat.h>

// create a tex table for the internal systematics
void sys_internal_table( std::string unique, TH1F * statonly, TH1F * total, variable_set & variables ){

  // get the layout, row, and colomn names
  std::string table_layout = "|c|r|r|r|r|r|}\n \\hline\n";
  std::vector<std::string > row_names = variables.analysis_bound.get_series_names();
  std::string column_names = "$q_{T}^{A}$ & Yield & \\makecell{Statistical\\\\Error} & \\makecell{Systematic\\\\Error} & \\makecell{Total\\\\Error} & \\makecell{Total Error}\\\\ \\hline\n";

  // open stream, do text doc and table setup
  std::ofstream sys_table( unique + ".tex" );
  sys_table << "\\documentclass[landscape]{article}\n";
  sys_table << "\\usepackage[paperheight=4in,paperwidth=10in]{geometry}\n";
  sys_table << "\\usepackage{makecell}";
  sys_table << "\\begin{document}";
  sys_table << std::fixed << std::setprecision( 2 );
  sys_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{";
  sys_table << table_layout;
  sys_table << column_names;

  // iterate over the rows
  for ( size_t bin = 1; bin <= row_names.size(); bin++ ){ 

    // row name and yield
    sys_table << std::to_string( bin ) << " & " << statonly->GetBinContent( bin ) << " & ";
    // stat error
    sys_table << statonly->GetBinError( bin ) << " & ";
    // sys error
    double total_err = total->GetBinError( bin )*total->GetBinError( bin );
    double stat_err = statonly->GetBinError( bin )*statonly->GetBinError( bin );
    sys_table << std::sqrt( total_err - stat_err ) << " & ";

    //total error, total relative, newline
    sys_table << std::sqrt(total_err) << " & " << std::sqrt(total_err)/statonly->GetBinContent( bin ) << "\\\\ \\hline\n";
  }
  // finish table and LaTeX document
  sys_table << "\\end{tabular}\n\\caption{Sys errors}\n\\end{table}\n\\end{document}";
  sys_table.close();

}

void sys_internal_plot( std::string unique, TH1F * stat, TH1F * total ){

  // style histograms
  stat->SetLineColor( kBlack );
  total->SetLineColor( kRed+2 );
  total->SetLineStyle( 1 );

  // define axes labels
  std::string x_string = "q_{T}^{A} [GeV]";
  std::string y_string = "Yield/2.000";

  // create canvas, pad, and draw histograms
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  total->Draw( "HIST E1" );
  stat->Draw( "HIST E1 SAME" );
  

  // label, decorate, and range
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( total, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( total, true );
  
  // add legend
  TLegend * legend = below_logo_legend();
  legend->AddEntry( stat, "Statistical error" );
  legend->AddEntry( total, "Total error" );
  legend->Draw();
  
  // set backgrounds, margin, save, and clean up
  canv.SetFillStyle( 4000 );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  std::string output_name = "sys_" + unique + ".pdf";
  canv.SaveAs( output_name.c_str() );
  canv.Clear();


}



// Plot internal systematic details
void sys_internal( YAML::Node & run_node, variable_set & variables ){

  // move to output dir
  mkdir( "./sys_internal", 744 );
  chdir( "./sys_internal" );

  // unpack yaml for relevant statistical and systematic runs.
  std::string output_unique = run_node[ "output_unique" ].as<std::string>();
  std::string stat_unique = run_node[ "stat_unique" ].as<std::string>();
  std::string sys_unique = run_node[ "sys_unique" ].as<std::string>();
  //std::vector<std::string> systematics = run_node[ "systematics"].as<std::vector<std::string>>();

  // get ncr style fits
  auto[ sys_total, sys_extracted ] = get_fit( sys_unique, variables );
  auto[ stat_total, stat_extracted ] = get_fit( stat_unique, variables );

  // produce table
  sys_internal_table( output_unique, stat_extracted.sign_hist,
                      sys_extracted.sign_hist, variables );

  sys_internal_plot( output_unique, stat_extracted.sign_hist, sys_extracted.sign_hist );

  // clean up, should write a proper destructor for hist_group
  delete sys_total;
  delete stat_total;
  sys_extracted.erase();
  stat_extracted.erase();

  // exit
  chdir( ".." );


}
