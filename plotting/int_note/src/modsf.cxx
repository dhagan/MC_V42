#include <funcs.hxx>
#include <modsf.hxx>
#include <sys/stat.h>


//void sf_to_bin( TH1F * hist, int bin, TTree * tree, const std::string & weight ){
//
//  TH1F * calc_hist = static_cast<TH1F*>( hist->Clone( "calc_hist" ) );
//  calc_hist->Reset();
//
//  std::string draw = weight + ">>calc_hist";
//  tree->Draw( "closure_weight_lowpt_base_final_noval>>calc_hist", 
//    "(PhotonPt>5.0)&&(PhotonPt<=7.5)", "e goff" );
//  hist->SetBinContent( bin, calc_hist->GetMean() );
//  draw = weight + "_upper>>calc_hist";
//  tree->Draw( "closure_weight_lowpt_base_final_noval_upper>>calc_hist", 
//    "(PhotonPt>5.0)&&(PhotonPt<=7.5)", "e goff" );
//  hist->SetBinError( bin, abs( hist->GetBinContent( 1 ) - calc_hist->GetMean() ) );
//
//}



void modsf( YAML::Node & run_node, variable_set & variables ){

  prep_style();

  mkdir( "./modsf", 744 );
  chdir( "./modsf" );

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * sign_tree = fileset->get_tree( sample_type::sign );

  bound & phot_bound = variables.bound_manager->get_bound( "PhotonPt" );
  std::vector<std::string> photon_bins = phot_bound.get_cut_series();

  double bin_edges[5] = { 5.0, 7.5, 10.0, 15.0, 20.0 };

  TH1F * calc_hist = new TH1F( "calc_hist", "", 100, 0, 2 ); 

  TH1F * der_hist = new TH1F( "der_hist", "", 4, bin_edges );
  TH1F * rec_hist = new TH1F( "rec_hist", "", 4, bin_edges );





  sign_tree->Draw( "closure_weight_lowpt_base_final_noval>>calc_hist", 
    "(PhotonPt>5.0)&&(PhotonPt<=7.5)", "e goff" );
  der_hist->SetBinContent( 1, calc_hist->GetMean() );
  sign_tree->Draw( "closure_weight_lowpt_base_final_noval_upper>>calc_hist", 
    "(PhotonPt>5.0)&&(PhotonPt<=7.5)", "e goff" );
  der_hist->SetBinError( 1, abs( der_hist->GetBinContent( 1 ) - calc_hist->GetMean() ) );

  sign_tree->Draw( "closure_weight_base_final_noval>>calc_hist", 
    "(PhotonPt>7.5)&&(PhotonPt<=10.0)", "e goff" );
  der_hist->SetBinContent( 2, calc_hist->GetMean() );
  sign_tree->Draw( "closure_weight_base_final_noval_upper>>calc_hist", 
    "(PhotonPt>7.5)&&(PhotonPt<=10.0)", "e goff" );
  der_hist->SetBinError( 2, abs( der_hist->GetBinContent( 2 ) - calc_hist->GetMean() ) );

  sign_tree->Draw( "closure_weight_base_final_noval>>calc_hist", 
    "(PhotonPt>10.0)&&(PhotonPt<=15.0)", "e goff" );
  der_hist->SetBinContent( 3, calc_hist->GetMean() );
  sign_tree->Draw( "closure_weight_base_final_noval_upper>>calc_hist", 
    "(PhotonPt>10.0)&&(PhotonPt<=15.0)", "e goff" );
  der_hist->SetBinError( 3, abs( der_hist->GetBinContent( 3 ) - calc_hist->GetMean() ) );

  sign_tree->Draw( "closure_weight_base_final_noval>>calc_hist", 
    "(PhotonPt>15.0)&&(PhotonPt<=20.0)", "e goff" );
  der_hist->SetBinContent( 4, calc_hist->GetMean() );
  sign_tree->Draw( "closure_weight_base_final_noval_upper>>calc_hist", 
    "(PhotonPt>15.0)&&(PhotonPt<=20.0)", "e goff" );
  der_hist->SetBinError( 4, abs( der_hist->GetBinContent( 4 ) - calc_hist->GetMean() ) );


  sign_tree->Draw( "photon_sf_rec>>calc_hist", "(PhotonPt>10.0)&&(PhotonPt<=15.0)", "e goff" );
  rec_hist->SetBinContent( 3, calc_hist->GetMean() );
  sign_tree->Draw( "photon_upper_sf_rec>>calc_hist", "(PhotonPt>10.0)&&(PhotonPt<=15.0)", "e goff" );
  rec_hist->SetBinError( 3, abs( rec_hist->GetBinContent( 3 ) - calc_hist->GetMean() ) );

  sign_tree->Draw( "photon_sf_rec>>calc_hist", "(PhotonPt>15.0)&&(PhotonPt<=20.0)", "e goff" );
  rec_hist->SetBinContent( 4, calc_hist->GetMean() );
  sign_tree->Draw( "photon_upper_sf_rec>>calc_hist", "(PhotonPt>15.0)&&(PhotonPt<=20.0)", "e goff" );
  rec_hist->SetBinError( 4, abs( rec_hist->GetBinContent( 4 ) - calc_hist->GetMean() ) );

  rec_hist->SetBinContent( 1, -1.0 );
  rec_hist->SetBinContent( 2, -1.0 );
  //der_hist->SetBinContent( 3, -1.0 );
  //der_hist->SetBinContent( 4, -1.0 );


  rec_hist->SetLineWidth( 2 ); 
  der_hist->SetLineWidth( 2 ); 
  rec_hist->SetLineColor( kBlue+1 ); 
  der_hist->SetLineColor( kRed+1 ); 
  rec_hist->SetMarkerColor( kBlue+1 ); 
  der_hist->SetMarkerColor( kRed+1 ); 
  rec_hist->SetMarkerStyle( 20 ); 
  der_hist->SetMarkerStyle( 20 ); 


  TH1F * one_hist = static_cast<TH1F *>( der_hist->Clone() );
  one_hist->Reset();
  one_hist->SetBinContent( 1, 1.0 );
  one_hist->SetBinContent( 2, 1.0 );
  one_hist->SetBinContent( 3, 1.0 );
  one_hist->SetBinContent( 4, 1.0 );
  one_hist->SetLineColorAlpha(kBlack, 0.9);
  one_hist->SetLineStyle( 2 );
  one_hist->SetLineWidth( 2 );



  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  rec_hist->Draw( "E1");
  rec_hist->Draw( "P SAME");
  der_hist->Draw( "SAME E1" );
  der_hist->Draw( "P SAME" );
  rec_hist->Draw( "P SAME");
  rec_hist->Draw( "E1 SAME");
  one_hist->Draw( "HIST SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( rec_hist, "", "Mean SF" );
  hist_prep_axes( rec_hist, true );
  rec_hist->GetYaxis()->SetTitleOffset( 1.65 );
  rec_hist->GetYaxis()->SetRangeUser( 0.0, 2.0 );
  TLegend * legend = create_atlas_legend();
  legend->AddEntry( der_hist, "Derived" );
  legend->AddEntry( rec_hist, "Provided" );
  legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "sfmod" );

  chdir( ".." );
 
}
