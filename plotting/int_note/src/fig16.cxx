#include <fig16.hxx>
#include <sys/stat.h>

void fig16( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./fig16", 744 );
  chdir( "./fig16" );

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string filepath = in_path + "/scalefactors/" + unique; 

  std::cout << variables.analysis_variable << std::endl; 
  
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( unique );
  fileset->load_subtraction_fileset( filepath, unique, true );

  TTree * data_tree = fileset->data_tree;
  std::vector<float> cuts = { 5.0, 6.0, 7.0, 8.0, 9.0 };

  double norm445 = 1.0;
  double axis445 = 1.0;

  for ( size_t cut = 0; cut < cuts.size(); cut++ ){

    int cut_val = cut + 5;
    std::string cut_str = "PhotonPt>" + std::to_string( cuts[cut] );
    TH2F * qt2Lambda = new TH2F( "qt2Lambda", "", 50, 0, 400, 50, 0, 200 );
    TH2F * qtALambda = new TH2F( "qtALambda", "", 50, -10, 20, 50, 0, 200 );
    data_tree->Draw( "Lambda:qTSquared>>qt2Lambda", cut_str.c_str(), "e goff" );
    data_tree->Draw( "Lambda:qtA>>qtALambda", cut_str.c_str(), "e goff");
    
    if ( cut == 0 ){ 
      norm445 = qtALambda->Integral();
      axis445 = qtALambda->GetMaximum()/norm445;  
    }

    std::string y_string = "#lambda";
    std::string x_string;
    x_string.reserve( 100 );
    
    //gStyle->SetOptStat( "i");

    TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
    canv.Divide( 1 );
    TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    active_pad->SetFillStyle( 4000 );
    active_pad->SetBottomMargin( 0.1 );

    qt2Lambda->Draw( "COLZ" );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    x_string = "q_{T}^{2} [GeV^{2}]";
    set_axis_labels( qt2Lambda, x_string.c_str(), y_string.c_str() );
    char buf[30]; sprintf( buf, "Integral: %.0f", qt2Lambda->Integral() );
    TPaveText * int_qt2 = new TPaveText( 250, 170, 400, 200 );
    int_qt2->SetTextFont( 42 );
    int_qt2->SetTextSize( 0.035 );
    int_qt2->SetLineWidth( 0 );
    int_qt2->AddText( buf );
    int_qt2->Draw( "SAME" );
    //int_qt2->Draw();
    //TPaveStats * stat1 = make_stats( qt2Lambda );
    //stat1->Draw();
    save_and_clear( canv, active_pad, "qt2Lambda44" + std::to_string( cut_val ) );

    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    qtALambda->Draw( "COLZ" );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    x_string = "q_{T}^{A} [GeV]";
    set_axis_labels( qtALambda, x_string.c_str(), y_string.c_str() );
    sprintf( buf, "Integral: %.0f", qtALambda->Integral() );
    TPaveText * int_qtA = new TPaveText( 10, 170, 15, 200 );
    int_qtA->SetTextFont( 42 );
    int_qtA->SetTextSize( 0.035 );
    int_qtA->SetLineWidth( 0 );
    int_qtA->AddText( buf );
    int_qtA->Draw( "SAME" );
    save_and_clear( canv, active_pad,"qtALambda44" + std::to_string( cut_val ) );


    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    qtALambda->Scale( 1.0/norm445 );
    qtALambda->Draw( "COLZ" );
    qtALambda->GetZaxis()->SetRangeUser( 0, axis445 );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    x_string = "q_{T}^{A} [GeV]";
    set_axis_labels( qtALambda, x_string.c_str(), y_string.c_str() );
    save_and_clear( canv, active_pad, "qtALambda44" + std::to_string( cut_val ) + "Norm" );

    delete qt2Lambda;
    delete qtALambda;
  }

  chdir( ".." );

}
