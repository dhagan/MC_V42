#include <bdt.hxx>
#include <sys/stat.h>

struct train_hists {
  TH1F * sign_test;
  TH1F * sign_train;
  TH1F * bckg_test;
  TH1F * bckg_train;
  TH1D * sign_bckg_rej_eff;
};

train_hists get_training( const std::string & unique ){
  
  std::string filepath = std::string( std::getenv( "OUT_PATH" ) )
                       + "/bdt/" + unique + "/tmva_train_"
                       + unique + ".root";

  TFile * training_output = new TFile( filepath.c_str(), "READ");
  TDirectory * dataloader_directory = training_output->GetDirectory( "tmva_dataloader" );
  
  TDirectory * BDT_upper_directory = dataloader_directory->GetDirectory( "Method_BDT" );
  TDirectory * BDT_lower_directory = BDT_upper_directory->GetDirectory( "BDT" );

  TH1D * sign_bckg_rej_eff = (TH1D *) BDT_lower_directory->Get( "MVA_BDT_rejBvsS" );
  sign_bckg_rej_eff->SetLineColor( 1.0 );

  TTree * test_tree =  static_cast<TTree *>( dataloader_directory->Get( "TestTree" ) );
  TTree * train_tree = static_cast<TTree *>( dataloader_directory->Get( "TrainTree" ) );

  std::string sign_test_name  = "sign_test_" + unique; 
  std::string sign_train_name = "sign_train_" + unique; 
  std::string bckg_test_name  = "bckg_test_" + unique; 
  std::string bckg_train_name = "bckg_train_" + unique; 


  TH1F * sign_test = new TH1F( sign_test_name.c_str(), "", 50, -1.0, 1.0 );
  TH1F * bckg_test = new TH1F( bckg_test_name.c_str(), "", 50, -1.0, 1.0 );

  TH1F * sign_train = new TH1F( sign_train_name.c_str(), "", 50, -1.0, 1.0 );
  TH1F * bckg_train = new TH1F( bckg_train_name.c_str(), "", 50, -1.0, 1.0 );

  sign_test_name  = "BDT>>" + sign_test_name;
  sign_train_name = "BDT>>" + sign_train_name;
  bckg_test_name  = "BDT>>" + bckg_test_name;
  bckg_train_name = "BDT>>" + bckg_train_name;

  test_tree->Draw(  sign_test_name.c_str(),   "classID==0" );
  test_tree->Draw(  bckg_test_name.c_str(),   "classID==1" );

  train_tree->Draw(  sign_train_name.c_str(),  "classID==0" );
  train_tree->Draw(  bckg_train_name.c_str(),  "classID==1" );


  sign_test->Scale( 1.0/ sign_test->Integral() );
  bckg_test->Scale( 1.0/ bckg_test->Integral() );
  sign_train->Scale( 1.0/ sign_train->Integral() ); 
  bckg_train->Scale( 1.0/ bckg_train->Integral() );

  sign_test->SetLineColor( kRed+2);
  sign_test->SetFillStyle( 3354 );
  sign_test->SetFillColor( kRed+2 );

  bckg_test->SetLineColor( kBlue+2);
  bckg_test->SetFillStyle( 3345 );
  bckg_test->SetFillColor( kBlue+2 );

  sign_train->SetMarkerStyle( 20 );
  sign_train->SetMarkerColor( kRed+1 );
  sign_train->SetMarkerSize( 1.0 );
  
  bckg_train->SetMarkerStyle( 20 );
  bckg_train->SetMarkerColor( kBlue+1 );
  bckg_train->SetMarkerSize( 1.0 );

  return train_hists{ sign_test, sign_train, bckg_test, bckg_train, sign_bckg_rej_eff };

}

void bckg_peak( YAML::Node & run_node ){

  

  //std::string dphi_tf = "abs(3.141592-AbsdPhi)";
  std::string unique1 = run_node[ "bdt_unique1" ].as<std::string>();
  std::string unique2 = run_node[ "bdt_unique2" ].as<std::string>();

  std::string filepath1 = std::string( getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique1; 
  std::string filepath2 = std::string( getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique2; 

  basic_fileset * fileset1 = new basic_fileset();
  fileset1->set_unique( unique1 );
  fileset1->load_subtraction_fileset( filepath1, unique1, true );

  TTree * sign_tree1 = fileset1->get_tree( sign );
  TTree * bckg_tree1 = fileset1->get_tree( bckg );

  basic_fileset * fileset2 = new basic_fileset();
  fileset2->set_unique( unique2 );
  fileset2->load_subtraction_fileset( filepath2, unique2, true );

  TTree * sign_tree2 = fileset2->get_tree( sign );
  TTree * bckg_tree2 = fileset2->get_tree( bckg );

  TH2F * sign1 = new TH2F( "sign1", "", 30, 0, 2.5, 30, 0, M_PI );  
  TH2F * sign2 = new TH2F( "sign2", "", 30, 0, 2.5, 30, 0, M_PI );  
  TH2F * bckg1 = new TH2F( "bckg1", "", 30, 0, 2.5, 30, 0, M_PI );
  TH2F * bckg2 = new TH2F( "bckg2", "", 30, 0, 2.5, 30, 0, M_PI );

  sign_tree1->Draw( "AbsdY:abs(3.141592-AbsdPhi)>>sign1", "BDT<-0.7", "e goff" ); 
  sign_tree2->Draw( "AbsdY:abs(3.141592-AbsdPhi)>>sign2", "BDT<-0.7", "e goff" );
  bckg_tree1->Draw( "AbsdY:abs(3.141592-AbsdPhi)>>bckg1", "BDT<-0.7", "e goff" );
  bckg_tree2->Draw( "AbsdY:abs(3.141592-AbsdPhi)>>bckg2", "BDT<-0.7", "e goff" );

  std::string x_string = "|#pi - |#Delta#phi||";
  std::string y_string = "|#Delta y|";

  TCanvas canv( "canv", "", 200, 200, 2000, 2000 );
  canv.SetFillStyle( 4000 );
  canv.Divide( 2, 2 );
  
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  sign1->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign1, x_string.c_str(), y_string.c_str() );


  active_pad = static_cast< TPad *>( canv.cd( 2 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  sign2->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign2, x_string.c_str(), y_string.c_str() );

  
  active_pad = static_cast< TPad *>( canv.cd( 3 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  bckg1->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bckg1, x_string.c_str(), y_string.c_str() );


  active_pad = static_cast< TPad *>( canv.cd( 4 ) );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetBottomMargin( 0.1 );
  bckg2->Draw( "COLZ" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bckg2, x_string.c_str(), y_string.c_str() );

  save_and_clear( canv, active_pad, "peak" );

}


void train_and_test( train_hists & bdt1, train_hists & bdt2 ){

  TCanvas canv( "canv", "", 200, 200, 2000, 1000 );
  canv.Divide( 2, 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
 
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  bdt1.sign_test->Draw( "HIST" );
  bdt1.sign_test->Draw( "F SAME" );
  bdt1.sign_test->Draw( "E1 SAME" );
  bdt1.sign_train->Draw( "P SAME" );
  bdt1.sign_train->Draw( "E1 SAME" );
  bdt1.bckg_test->Draw( "HIST SAME" );
  bdt1.bckg_test->Draw( "F SAME" );
  bdt1.bckg_test->Draw( "E1 SAME" );
  bdt1.bckg_train->Draw( "P SAME" );
  bdt1.bckg_train->Draw( "E1 SAME" );
  hist_prep_axes( bdt1.sign_test );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bdt1.sign_test, "BDT [score]", "Normalised Entries/0.04" );
  bdt1.sign_test->GetYaxis()->SetRangeUser( 0.0, 0.25 );
  bdt1.sign_test->GetYaxis()->SetTitleOffset( 1.6 );
  TLegend * testtrain_legend1 = create_atlas_legend();
  testtrain_legend1->AddEntry( bdt1.sign_test, "sign - test", "LF" );
  testtrain_legend1->AddEntry( bdt1.bckg_test, "bckg - test", "LF" );
  testtrain_legend1->AddEntry( bdt1.sign_train, "sign - train", "P" ); 
  testtrain_legend1->AddEntry( bdt1.bckg_train, "bckg - train", "P" );
  testtrain_legend1->Draw( "SAME" );

  active_pad = static_cast< TPad *>( canv.cd( 2 ) );
  bdt2.sign_test->Draw( "HIST" );
  bdt2.sign_test->Draw( "F SAME" );
  bdt2.sign_test->Draw( "E1 SAME" );
  bdt2.sign_train->Draw( "P SAME" );
  bdt2.sign_train->Draw( "E1 SAME" );
  bdt2.bckg_test->Draw( "HIST SAME" );
  bdt2.bckg_test->Draw( "F SAME" );
  bdt2.bckg_test->Draw( "E1 SAME" );
  bdt2.bckg_train->Draw( "P SAME" );
  bdt2.bckg_train->Draw( "E1 SAME" );
  hist_prep_axes( bdt2.sign_test );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bdt2.sign_test, "BDT [score]", "Normalised Entries/0.04" );
  bdt2.sign_test->GetYaxis()->SetRangeUser( 0.0, 0.25 );
  bdt2.sign_test->GetYaxis()->SetTitleOffset( 1.6 );
  TLegend * testtrain_legend2 = create_atlas_legend();
  testtrain_legend2->AddEntry( bdt2.sign_test, "sign - test", "LF" );
  testtrain_legend2->AddEntry( bdt2.bckg_test, "bckg - test", "LF" );
  testtrain_legend2->AddEntry( bdt2.sign_train, "sign - train", "P" ); 
  testtrain_legend2->AddEntry( bdt2.bckg_train, "bckg - train", "P" );
  testtrain_legend2->Draw( "SAME" );

  save_and_clear( canv, active_pad, "TrainAndTest.pdf" );

}


void eff_rej( train_hists & bdt1, train_hists & bdt2 ){

  TCanvas canv( "canv", "", 200, 200, 2000, 1000 );
  canv.Divide( 2, 1 );

  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  bdt1.sign_bckg_rej_eff->Draw( "HIST" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bdt1.sign_bckg_rej_eff, "Signal Efficiency", "Background Rejection" );
  bdt1.sign_bckg_rej_eff->GetYaxis()->SetRangeUser( 0.0, 1.0 );

  active_pad = static_cast< TPad *>( canv.cd( 2 ) );
  bdt2.sign_bckg_rej_eff->Draw( "HIST" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bdt2.sign_bckg_rej_eff, "Signal Efficiency", "Background Rejection" );
  bdt2.sign_bckg_rej_eff->GetYaxis()->SetRangeUser( 0.0, 1.0 );

  save_and_clear( canv, active_pad, "EffRejBDT" );

} 

void forbidden_methods( YAML::Node & run_node ){

  std::string unique1 = run_node[ "bdt_unique1" ].as<std::string>();
  std::string unique2 = run_node[ "bdt_unique2" ].as<std::string>();

  std::string sign_filepath1 = std::string( getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique1
    + "/sign/sign_" + unique1 + ".root"; 
  std::string bckg_filepath1 = std::string( getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique1
    + "/bckg/bckg_" + unique1 + ".root";  
  std::string sign_filepath2 = std::string( getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique2
    + "/sign/sign_" + unique2 + ".root"; 
  std::string bckg_filepath2 = std::string( getenv( "OUT_PATH" ) ) + "/scalefactors/" + unique2
    + "/bckg/bckg_" + unique2 + ".root"; 

  TChain chain_sign1( "tree" );
  TChain chain_bckg1( "tree" );
  TChain chain_sign2( "tree" );
  TChain chain_bckg2( "tree" );
  chain_sign1.Add( sign_filepath1.c_str() ); 
  chain_bckg1.Add( bckg_filepath1.c_str() );
  chain_sign2.Add( sign_filepath2.c_str() );
  chain_bckg2.Add( bckg_filepath2.c_str() );

  ROOT::RDataFrame sign_frame1( chain_sign1 );
  ROOT::RDataFrame bckg_frame1( chain_bckg1 );
  ROOT::RDataFrame sign_frame2( chain_sign2 );
  ROOT::RDataFrame bckg_frame2( chain_bckg2 );

  auto s1 = sign_frame1.Define( "s1_BDT", "BDT" );
  s1.Snapshot( "tree", "s1f.root", { "s1_BDT" } );
  
  auto b1 = bckg_frame1.Define( "b1_BDT", "BDT" );
  b1.Snapshot( "tree", "b1f.root", { "b1_BDT" } );
  
  auto s2 = sign_frame2.Define( "s2_BDT", "BDT" );
  s2.Snapshot( "tree", "s2f.root", { "s2_BDT" } );

  auto b2 = bckg_frame2.Define( "b2_BDT", "BDT" );
  b2.Snapshot( "tree", "b2f.root", { "b2_BDT" } );

  TFile * s1_file = new TFile( "s1f.root", "READ" );
  TFile * b1_file = new TFile( "b1f.root", "READ" );
  //TFile * s2_file = new TFile( "s2f.root", "READ" );
  //TFile * b2_file = new TFile( "b2f.root", "READ" );

  TTree * s1_tree = static_cast<TTree *>( s1_file->Get( "tree" ) );
  TTree * b1_tree = static_cast<TTree *>( b1_file->Get( "tree" ) );

  s1_tree->AddFriend( "tree", "s2f.root" );
  b1_tree->AddFriend( "tree", "b2f.root" );

  TH2F * sign_BDT = new TH2F( "sign_BDT", "", 20, -1.0, 1.0, 20, -1.0, 1.0 );
  TH2F * bckg_BDT = new TH2F( "bckg_BDT", "", 20, -1.0, 1.0, 20, -1.0, 1.0 );



  s1_tree->Draw( "s1_BDT:s2_BDT>>sign_BDT", "", "e goff" );
  b1_tree->Draw( "b1_BDT:b2_BDT>>bckg_BDT", "", "" );

  TCanvas canv( "canv", "", 200, 200, 2000, 2000 );
  canv.Divide( 1, 1 );
  canv.cd( 1 );

  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  sign_BDT->Draw( "LEGO" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( sign_BDT, unique2.c_str(), unique1.c_str() );
  save_and_clear( canv, active_pad, "sign_BDT" );

  active_pad = static_cast<TPad *>( canv.cd( 2 ) );
  bckg_BDT->Draw( "LEGO" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( bckg_BDT, unique2.c_str(), unique1.c_str() );
  save_and_clear( canv, active_pad, "bckg_BDT" );



  



}



void train_cmp( YAML::Node & run_node ){

  train_hists bdt1 = get_training( run_node[ "bdt_unique1" ].as<std::string>() );
  train_hists bdt2 = get_training( run_node[ "bdt_unique2" ].as<std::string>() );

  bckg_peak( run_node );
  forbidden_methods( run_node );
  train_and_test( bdt1, bdt2 );
  eff_rej( bdt1, bdt2 );

}


void bdt_cmp( YAML::Node & run_node ){

  gStyle->SetTitleYOffset(1.5);
  mkdir( "./bdt_cmp", 744 );
  chdir( "./bdt_cmp" );
  train_cmp( run_node );
  chdir( ".." );


}
