#include <acceptance.hxx>
#include <sys/stat.h>

void acceptance_plots( YAML::Node & run_node, variable_set & variables ){

  prep_style();
  gStyle->SetOptStat( "" );
  gStyle->SetTitleYOffset(1.5);

  mkdir( "./acceptance", 744 );
  chdir( "./acceptance" );

  if ( run_node["generate"].as<bool>() ){ 
    make_acceptance_trees( true );
    make_acceptance_trees( false );
  }
  make_acceptance_overlays( run_node, variables );

  chdir( ".." );

}



void make_acceptance_trees( bool gen ){

  ROOT::EnableImplicitMT( 6 );

  //bool gen = run_node["gen"].as<bool>();
  std::string filepath = std::string( std::getenv("IN_PATH") );
  filepath += ( ( gen ) ? "/000/gen_ntuple.root/truthTree" : "/ntuples/sign_ntuple.root/tree" );

  std::cout << filepath << std::endl;

  ROOT::RDF::RSnapshotOptions snapshot_options;
  snapshot_options.fMode = "UPDATE";
	auto phi_lambda = phi_calc;
  auto dphi_lambda = dphi_calc;

  TChain input_chain;
  input_chain.Add( filepath.c_str());
  ROOT::RDataFrame input_frame( input_chain );

  std::vector< std::string > snapshot_columns = { "minv", "photon_eta", "muneg_eta", "mupos_eta", 
                                                  "photon_pt", "mupos_pt",  "muneg_pt",  "Phi", 
                                                  "qTSquared", "qx", "qy", "qtA", "qtB" };

  auto frame = input_frame.Filter( ( gen ) ? "mcDecPhoton_Pt >= 0" : "Photon_Pt.size() != 0" );

  if ( gen ){

    frame = frame.Define( "minv", "Float_t( mcDecMuMuY_M/1000.0 )" );
    frame = frame.Define( "photon_eta", "Float_t( mcDecPhoton_Eta )" );
    frame = frame.Define( "muneg_eta",  "Float_t( mcDecMuMi_Eta )" );
    frame = frame.Define( "mupos_eta",  "Float_t( mcDecMuPl_Eta )" );
    frame = frame.Define( "photon_pt",	"Float_t( mcDecPhoton_Pt/1000.0 )" );
    frame = frame.Define( "mupos_pt",   "Float_t( mcDecMuPl_Pt/1000.0 )" );
    frame = frame.Define( "muneg_pt",   "Float_t( mcDecMuMi_Pt/1000.0 )" );
    frame = frame.Define( "DPhi", dphi_lambda, { "mcDecDiLept_Phi", "mcDecPhoton_Phi" } );
	  frame = frame.Define( "Phi", phi_lambda, {"truth_angleDec_cs_phi_mumugamma"} );
	  frame = frame.Define( "qTSquared", "Float_t( (mcDecMuMuY_Pt/1000.0) * (mcDecMuMuY_Pt/1000.0) )" );
    frame = frame.Define( "jpsipt",	 "Float_t( mcDecDiLept_Pt/1000.0 )" );
	  frame = frame.Define( "Lambda", "Float_t( (mcDecMuMuY_M/3097.0)*(mcDecMuMuY_M/3097.0) )" );

  } else {

    frame = frame.Define( "minv", "Float_t( MuMuY_M[0]/1000.0 )" );
    frame = frame.Define( "mumu_mass",  "Float_t( DiLept_M[0]/1000.0 )" );
    frame = frame.Define( "photon_eta",	"Float_t( Photon_Eta[0] )" );
    frame = frame.Define( "mupos_eta",  "Float_t( MuPlus_Eta[0] )" );
    frame = frame.Define( "muneg_eta",  "Float_t( MuMinus_Eta[0] )" );
    frame = frame.Define( "photon_pt",	"Float_t( Photon_Pt[0]/1000.0 )" );
    frame = frame.Define( "mupos_pt",  "Float_t( MuPlus_Pt[0]/1000.0 )" );
    frame = frame.Define( "muneg_pt",  "Float_t( MuMinus_Pt[0]/1000.0 )" );
	  frame = frame.Define( "DPhi", dphi_lambda, { "DiLept_Phi", "Photon_Phi" } );
	  frame = frame.Define( "Phi", phi_lambda, {"MuMuGamma_CS_Phi"} );
	  frame = frame.Define( "qTSquared", "Float_t( (MuMuY_Pt[0]/1000.0) * (MuMuY_Pt[0]/1000.0) )" );
    frame = frame.Define( "jpsipt",	 "Float_t( DiLept_Pt[0]/1000.0 )" );


  }

  frame = frame.Define( "qx",   "Float_t( sqrt( qTSquared )*cos( Phi ) )" );
  frame = frame.Define( "qy",   "Float_t( sqrt( qTSquared )*sin( Phi ) )" );
  frame = frame.Define( "qtA",  "Float_t( jpsipt - photon_pt)" );
  frame = frame.Define( "qtB",  "Float_t( sqrt( (jpsipt)*(photon_pt) )*sin(DPhi) )" );

  frame = frame.Filter( "abs( muneg_eta ) < 2.4" );
  frame = frame.Filter( "abs( mupos_eta ) < 2.4" );
  frame = frame.Filter( "abs( photon_eta ) < 2.4" );

  std::string treefile = (gen) ? "./gen.root" : "./acc.root";
  frame.Snapshot( "tree", treefile.c_str(), snapshot_columns, snapshot_options );

  if ( gen ){

    auto frame000 = frame.Filter( "photon_pt > 0" );
    frame000 = frame.Filter( "mupos_pt > 0" );
    frame000 = frame.Filter( "muneg_pt > 0" );
    frame000.Snapshot( "000", treefile.c_str(), "", snapshot_options );

    auto frame111 = frame.Filter( "photon_pt > 1" );
    frame111 = frame.Filter( "mupos_pt > 1" );
    frame111 = frame.Filter( "muneg_pt > 1" );
    frame111.Snapshot( "111", treefile.c_str(), "", snapshot_options );

    auto frame112 = frame.Filter( "photon_pt > 2" );
    frame112 = frame.Filter( "mupos_pt > 1" );
    frame112 = frame.Filter( "muneg_pt > 1" );
    frame112.Snapshot( "112", treefile.c_str(), "", snapshot_options );

    auto frame224 = frame.Filter( "photon_pt > 3" );
    frame224 = frame.Filter( "mupos_pt > 2" );
    frame224 = frame.Filter( "muneg_pt > 2" );
    frame224.Snapshot( "224", treefile.c_str(), "", snapshot_options );

    auto frame334 = frame.Filter( "photon_pt > 4" );
    frame334 = frame.Filter( "mupos_pt > 3" );
    frame334 = frame.Filter( "muneg_pt > 3" );
    frame334.Snapshot( "334", treefile.c_str(), "", snapshot_options );
  
  }


  auto frame445 = frame.Filter( "photon_pt > 5" );
  frame445 = frame.Filter( "mupos_pt > 4" );
  frame445 = frame.Filter( "muneg_pt > 4" );
  frame445.Snapshot( "445", treefile.c_str(), snapshot_columns, snapshot_options );

  
  
}

void make_acceptance_overlays( YAML::Node & run_node, variable_set & variables ){

  std::string gen_path = std::string( getenv( "OUT_PATH" ) ) + run_node[ "gen" ].as<std::string>();
  std::string acc_path = std::string( getenv( "OUT_PATH" ) ) + run_node[ "acc" ].as<std::string>();

  bound & qta_bound = variables.bound_manager->get_bound( "qtA" );
  bound & qtb_bound = variables.bound_manager->get_bound( "qtB" );
  bound & qx_bound = variables.bound_manager->get_bound( "qx" );

  TFile * gen_file = new TFile( gen_path.c_str(), "READ" );
  TFile * acc_file = new TFile( acc_path.c_str(), "READ" );
  TTree * gen_tree = static_cast< TTree *>( gen_file->Get( "tree" ) );
  TTree * acc_tree = static_cast< TTree *>( acc_file->Get( "tree" ) );

  TH1F * qta000 = new TH1F( "qta000", "", 60, -15, 15 );  
  TH1F * qta111 = new TH1F( "qta111", "", 60, -15, 15 );
  TH1F * qta112 = new TH1F( "qta112", "", 60, -15, 15 );
  TH1F * qta224 = new TH1F( "qta224", "", 60, -15, 15 );
  TH1F * qta334 = new TH1F( "qta334", "", 60, -15, 15 );
  TH1F * qta445 = new TH1F( "qta445", "", 60, -15, 15 );
  TH1F * qta445g = new TH1F( "qta445g", "", 60, -15, 15 );
  //TH1F * qta449 = new TH1F( "qta449", "", 100, -15, 15 );

  TH1F * qtb000 = new TH1F( "qtb000", "", 60, -15, 15 );  
  TH1F * qtb111 = new TH1F( "qtb111", "", 60, -15, 15 );
  TH1F * qtb112 = new TH1F( "qtb112", "", 60, -15, 15 );
  TH1F * qtb224 = new TH1F( "qtb224", "", 60, -15, 15 );
  TH1F * qtb334 = new TH1F( "qtb334", "", 60, -15, 15 );
  TH1F * qtb445 = new TH1F( "qtb445", "", 60, -15, 15 );
  TH1F * qtb445g = new TH1F( "qtb445g", "", 60, -15, 15 );

  TH1F * qx000 = new TH1F( "qx000", "", 60, -15, 15 );  
  TH1F * qx111 = new TH1F( "qx111", "", 60, -15, 15 );
  TH1F * qx112 = new TH1F( "qx112", "", 60, -15, 15 );
  TH1F * qx224 = new TH1F( "qx224", "", 60, -15, 15 );
  TH1F * qx334 = new TH1F( "qx334", "", 60, -15, 15 );
  TH1F * qx445 = new TH1F( "qx445", "", 60, -15, 15 );
  TH1F * qx445g = new TH1F( "qx445g", "", 60, -15, 15 );
  //TH1F * qx449 = new TH1F( "qx449", "", 60, -15, 15 );
  //
  
  TH1F * qy000 = new TH1F( "qy000", "", 60, -15, 15 );  
  TH1F * qy112 = new TH1F( "qy112", "", 60, -15, 15 );
  TH1F * qy224 = new TH1F( "qy224", "", 60, -15, 15 );
  TH1F * qy334 = new TH1F( "qy334", "", 60, -15, 15 );
  TH1F * qy445 = new TH1F( "qy445", "", 60, -15, 15 );
  TH1F * qy445g = new TH1F( "qy445g", "", 60, -15, 15 );



  TH1F * qta000M = new TH1F( "qta000M", "", 100, -15, 15 );  
  TH1F * qta111M = new TH1F( "qta111M", "", 100, -15, 15 );
  TH1F * qta112M = new TH1F( "qta112M", "", 100, -15, 15 );
  TH1F * qta224M = new TH1F( "qta224M", "", 100, -15, 15 );
  TH1F * qta334M = new TH1F( "qta334M", "", 100, -15, 15 );
  TH1F * qta445M = new TH1F( "qta445M", "", 100, -15, 15 );
  TH1F * qta445Mg = new TH1F( "qta445Mg", "", 100, -15, 15 );

  std::string cut000 = "MuPos_Pt > 0.0 && MuNeg_Pt > 0.0 && PhotonPt > 0.0"; //&& abs( qtA ) < 10.0"; 
  std::string cut111 = "MuPos_Pt > 1.0 && MuNeg_Pt > 1.0 && PhotonPt > 1.0"; //&& abs( qtA ) < 10.0";
  std::string cut112 = "MuPos_Pt > 1.0 && MuNeg_Pt > 1.0 && PhotonPt > 2.0"; //&& abs( qtA ) < 10.0";
  std::string cut224 = "MuPos_Pt > 2.0 && MuNeg_Pt > 2.0 && PhotonPt > 4.0"; //&& abs( qtA ) < 10.0";
  std::string cut334 = "MuPos_Pt > 3.0 && MuNeg_Pt > 3.0 && PhotonPt > 4.0"; //&& abs( qtA ) < 10.0";
  std::string cut445 = "MuPos_Pt > 4.0 && MuNeg_Pt > 4.0 && PhotonPt > 5.0"; //&& abs( qtA ) < 10.0";
  std::string cut449 = "MuPos_Pt > 4.0 && MuNeg_Pt > 4.0 && PhotonPt > 9.0"; //&& abs( qtA ) < 10.0";
  //std::string cut000q3 = "mupos_pt > 0.0 && muneg_pt > 0.0 && photon_pt > 0.0 && Lambda > 25 && Lambda < 50"; 
  //std::string cut445q3 = "mupos_pt > 4.0 && muneg_pt > 4.0 && photon_pt > 4.0 && Lambda > 25 && Lambda < 50";

  gen_tree->Draw( "qtA>>qta000M", cut000.c_str(), "goff" );
  gen_tree->Draw( "qtA>>qta111M", cut111.c_str(), "goff" ); 
  gen_tree->Draw( "qtA>>qta112M", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qtA>>qta224M", cut224.c_str(), "goff" ); 
  gen_tree->Draw( "qtA>>qta334M", cut334.c_str(), "goff" );
  gen_tree->Draw( "qtA>>qta445Mg", cut445.c_str(), "goff" );
  acc_tree->Draw( "qtA>>qta445M", cut445.c_str(), "goff" ); 
  acc_tree->Draw( "qtA>>qta449", cut449.c_str(), "goff" ); 

  gen_tree->Draw( "qtB>>qtb000", cut000.c_str(), "goff" );
  gen_tree->Draw( "qtB>>qtb111", cut111.c_str(), "goff" ); 
  gen_tree->Draw( "qtB>>qtb112", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qtB>>qtb224", cut224.c_str(), "goff" ); 
  gen_tree->Draw( "qtB>>qtb334", cut334.c_str(), "goff" );
  gen_tree->Draw( "qtB>>qtb445g", cut445.c_str(), "goff" );
  acc_tree->Draw( "qtB>>qtb445", cut445.c_str(), "goff" ); 

  double mean000 = qta000M->GetMean();
  double mean111 = qta111M->GetMean();
  double mean112 = qta112M->GetMean();
  double mean224 = qta224M->GetMean();
  double mean334 = qta334M->GetMean();
  double mean445 = qta445M->GetMean();
  double mean445g = qta445Mg->GetMean();

  gen_tree->Draw( Form( "( qtA - %.5f )>>qta000", mean000 ), cut000.c_str(), "goff" );
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta111", mean111 ), cut111.c_str(), "goff" ); 
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta112", mean112 ), cut112.c_str(), "goff" ); 
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta224", mean224 ), cut224.c_str(), "goff" ); 
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta334", mean334 ), cut334.c_str(), "goff" );
  gen_tree->Draw( Form( "( qtA - %.5f )>>qta445g", mean445g ), cut445.c_str(), "goff" ); 
  acc_tree->Draw( Form( "( qtA - %.5f )>>qta445", mean445 ), cut445.c_str(), "goff" ); 

 
  gen_tree->Draw( "qx>>qx000", cut000.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx111", cut111.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx112", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx224", cut224.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx334", cut334.c_str(), "goff" );
  acc_tree->Draw( "qx>>qx445", cut445.c_str(), "goff" ); 
  gen_tree->Draw( "qx>>qx445g", cut445.c_str(), "goff" );
  acc_tree->Draw( "qx>>qx449", cut449.c_str(), "goff" ); 

  
  gen_tree->Draw( "qy>>qy000", cut000.c_str(), "goff" ); 
  gen_tree->Draw( "qy>>qy112", cut112.c_str(), "goff" ); 
  gen_tree->Draw( "qy>>qy224", cut224.c_str(), "goff" ); 
  gen_tree->Draw( "qy>>qy334", cut334.c_str(), "goff" );
  acc_tree->Draw( "qy>>qy445", cut445.c_str(), "goff" ); 
  gen_tree->Draw( "qy>>qy445g", cut445.c_str(), "goff" ); 
  acc_tree->Draw( "qy>>qy449", cut449.c_str(), "goff" ); 
  
  // Int_t non_photo_blue = TColor::GetColor( "A3E7FC" )


  // make the table
  std::ofstream table( "acc.tex" );
  table << "\\documentclass{article}\n";
  table << "\\usepackage{geometry}\n";
  table << "\\usepackage{makecell}\n";
  table << "\\begin{document}";
  table << std::fixed << std::setprecision( 0 );
  table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{ |c|r|r|r|r||r|r| }\\hline\n";
  table << "$p_{T}(#mu)$ & p_{T}(#gamma) & N(q_{T}{A} ) \\\\ \\hline \n";
  table << " 0 & 0 & N(q_{T}{A} ) \\\\ \\hline \n";
  table << " 1 & 2 & N(q_{T}{A} ) \\\\ \\hline \n";
  table << " 2 & 4 & N(q_{T}{A} ) \\\\ \\hline \n";
  table << " 3 & 4 & N(q_{T}{A} ) \\\\ \\hline \n";
  table << " 4 & 5 ";
  table << " 4 & 9 ";
  table << "\\end{tabular}\n\\caption{IS EPSILON GONNA BE EFFICIENCY OR ERROR?}\n\\label{tab:merp}\\end{table}\n\\end{document}";
  table.close();
  
  qta000->SetMarkerStyle( 20 );
  qta112->SetMarkerStyle( 20 );
  qta224->SetMarkerStyle( 20 );
  qta334->SetMarkerStyle( 20 );
  qta445->SetMarkerStyle( 20 );
  qtb000->SetMarkerStyle( 20 );
  qtb112->SetMarkerStyle( 20 );
  qtb224->SetMarkerStyle( 20 );
  qtb334->SetMarkerStyle( 20 );
  qtb445->SetMarkerStyle( 20 );
  qx000 ->SetMarkerStyle( 20 ); 
  qx112 ->SetMarkerStyle( 20 ); 
  qx224 ->SetMarkerStyle( 20 ); 
  qx334 ->SetMarkerStyle( 20 ); 
  qx445 ->SetMarkerStyle( 20 ); 
  qy000 ->SetMarkerStyle( 20 ); 
  qy112 ->SetMarkerStyle( 20 ); 
  qy224 ->SetMarkerStyle( 20 ); 
  qy334 ->SetMarkerStyle( 20 ); 
  qy445 ->SetMarkerStyle( 20 ); 
  
  qta000->SetMarkerColor( tyrian );
  qta112->SetMarkerColor( carmine );
  qta224->SetMarkerColor( rapsberry  );
  qta334->SetMarkerColor( violet  );
  qta445->SetMarkerColor( tiffany );
  qta445g->SetMarkerColor( tiffany );

  qtb000->SetMarkerColor( tyrian );
  qtb112->SetMarkerColor( carmine );
  qtb224->SetMarkerColor( rapsberry  );
  qtb334->SetMarkerColor( violet  );
  qtb445->SetMarkerColor( tiffany );
  qtb445g->SetMarkerColor( tiffany );

  qx000->SetMarkerColor( tyrian );
  qx112->SetMarkerColor( carmine );
  qx224->SetMarkerColor( rapsberry  );
  qx334->SetMarkerColor( violet  );
  qx445->SetMarkerColor( tiffany );
  qx445g->SetMarkerColor( tiffany );

  qy000->SetMarkerColor( tyrian );
  qy112->SetMarkerColor( carmine );
  qy224->SetMarkerColor( rapsberry  );
  qy334->SetMarkerColor( violet  );
  qy445->SetMarkerColor( tiffany );
  qy445g->SetMarkerColor( tiffany );

  qta000->SetFillColor( tyrian );
  qta112->SetFillColor( carmine );
  qta224->SetFillColor( rapsberry  );
  qta334->SetFillColor( violet  );
  qta445->SetFillColor( tiffany );
  qta445g->SetFillColor( tiffany );

  qtb000->SetFillColor( tyrian );
  qtb112->SetFillColor( carmine );
  qtb224->SetFillColor( rapsberry  );
  qtb334->SetFillColor( violet  );
  qtb445->SetFillColor( tiffany );
  qtb445g->SetFillColor( tiffany );

  qx000->SetFillColor( tyrian );
  qx112->SetFillColor( carmine );
  qx224->SetFillColor( rapsberry  );
  qx334->SetFillColor( violet  );
  qx445->SetFillColor( tiffany );
  qx445g->SetFillColor( tiffany );

  qy000->SetFillColor( tyrian );
  qy112->SetFillColor( carmine );
  qy224->SetFillColor( rapsberry  );
  qy334->SetFillColor( violet  );
  qy445->SetFillColor( tiffany );
  qy445g->SetFillColor( tiffany );


  qta000->SetFillStyle( 1001 );
  qta112->SetFillStyle( 1001 );
  qta224->SetFillStyle( 1001 );
  qta334->SetFillStyle( 1001 );
  qta445->SetFillStyle( 1001 );
  qta445g->SetFillStyle(1001 );
  qtb000->SetFillStyle( 1001 );
  qtb112->SetFillStyle( 1001 );
  qtb224->SetFillStyle( 1001 );
  qtb334->SetFillStyle( 1001 );
  qtb445->SetFillStyle( 1001 );
  qtb445g->SetFillStyle(1001 );
  qx000 ->SetFillStyle( 1001 ); 
  qx112 ->SetFillStyle( 1001 ); 
  qx224 ->SetFillStyle( 1001 ); 
  qx334 ->SetFillStyle( 1001 ); 
  qx445 ->SetFillStyle( 1001 ); 
  qx445g->SetFillStyle( 1001 ); 
  qy000 ->SetFillStyle( 1001 ); 
  qy112 ->SetFillStyle( 1001 ); 
  qy224 ->SetFillStyle( 1001 ); 
  qy334 ->SetFillStyle( 1001 ); 
  qy445 ->SetFillStyle( 1001 ); 
  qy445g->SetFillStyle( 1001 );
  
  qta000->SetLineWidth( 0 );
  qta112->SetLineWidth( 0 );
  qta224->SetLineWidth( 0 );
  qta334->SetLineWidth( 0 );
  qta445->SetLineWidth( 0 );
  qta445g->SetLineWidth( 0 );
  qtb000->SetLineWidth( 0 );
  qtb112->SetLineWidth( 0 );
  qtb224->SetLineWidth( 0 );
  qtb334->SetLineWidth( 0 );
  qtb445->SetLineWidth( 0 );
  qtb445g->SetLineWidth( 0 );
  qx000 ->SetLineWidth( 0 ); 
  qx112 ->SetLineWidth( 0 ); 
  qx224 ->SetLineWidth( 0 ); 
  qx334 ->SetLineWidth( 0 ); 
  qx445 ->SetLineWidth( 0 ); 
  qx445g->SetLineWidth( 0 ); 
  qy000 ->SetLineWidth( 0 ); 
  qy112 ->SetLineWidth( 0 ); 
  qy224 ->SetLineWidth( 0 ); 
  qy334 ->SetLineWidth( 0 ); 
  qy445 ->SetLineWidth( 0 ); 
  qy445g->SetLineWidth( 0 );


  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad * >( gPad->cd() );


  active_pad = static_cast<TPad *>( gPad->cd() );
  qx000->Draw( "HIST" );  
  qx112->Draw( "HIST SAME" ); 
  qx224->Draw( "HIST SAME" ); 
  qx334->Draw( "HIST SAME" ); 
  qx445g->Draw( "HIST SAME" ); 
  hist_prep_axes( qx000, true );
  //set_axis_labels( qx000, "q_{x} [GeV]", "Entries/0.500 [GeV^{-1}]" );
  set_axis_labels( qx000, "q_{x} [GeV]", "Entries/0.500 [GeV^{-1}]" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  qx000->GetYaxis()->SetRangeUser( 0, 1750 );
  TLegend * qx_legend_a = create_atlas_legend();
  qx_legend_a->AddEntry( qx000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qx_legend_a->AddEntry( qx112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qx_legend_a->AddEntry( qx224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qx_legend_a->AddEntry( qx334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qx_legend_a->AddEntry( qx445g,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qx_legend_a->SetX1(0.54);
  qx_legend_a->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qx" );
  
  active_pad = static_cast<TPad *>( gPad->cd() );
  qy000->Draw( "HIST" );  
  qy112->Draw( "HIST SAME" ); 
  qy224->Draw( "HIST SAME" ); 
  qy334->Draw( "HIST SAME" ); 
  qy445g->Draw( "HIST SAME" ); 
  hist_prep_axes( qy000, true );
  set_axis_labels( qy000, "q_{y} [GeV]", "Entries/0.500 [GeV^{-1}]" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  qy000->GetYaxis()->SetRangeUser( 0, 1750 );
  TLegend * qy_legend_a = create_atlas_legend();
  qy_legend_a->AddEntry( qy000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qy_legend_a->AddEntry( qy112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qy_legend_a->AddEntry( qy224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qy_legend_a->AddEntry( qy334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qy_legend_a->AddEntry( qy445g,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qy_legend_a->SetX1(0.54);
  qy_legend_a->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qy" );


  active_pad = static_cast<TPad *>( gPad->cd() );
  qta000->Draw( "HIST" );  
  qta112->Draw( "HIST SAME" ); 
  qta112->Draw( "HIST SAME" ); 
  qta224->Draw( "HIST SAME" ); 
  qta334->Draw( "HIST SAME" ); 
  qta445g->Draw( "HIST SAME" ); 
  std::cout << qta000->Integral() << std::endl; 
  std::cout << qta112->Integral() << std::endl;
  std::cout << qta224->Integral() << std::endl;
  std::cout << qta334->Integral() << std::endl;
  std::cout << qta445g->Integral() << std::endl;
  hist_prep_axes( qta000, true );
  set_axis_labels( qta000, "q_{T}^{A} [GeV]", "Entries/0.500 [GeV^{-1}]" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  qta000->GetYaxis()->SetRangeUser( 0, 1750 );
  TLegend * qta_legend_a = create_atlas_legend();
  qta_legend_a->AddEntry( qta000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qta_legend_a->AddEntry( qta112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qta_legend_a->AddEntry( qta224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qta_legend_a->AddEntry( qta334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qta_legend_a->AddEntry( qta445g,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qta_legend_a->SetX1(0.54);
  qta_legend_a->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qta" );

  active_pad = static_cast<TPad *>( gPad->cd() );
  qtb000->Draw( "HIST" );  
  qtb112->Draw( "HIST SAME" ); 
  qtb112->Draw( "HIST SAME" ); 
  qtb224->Draw( "HIST SAME" ); 
  qtb334->Draw( "HIST SAME" ); 
  qtb445g->Draw( "HIST SAME" ); 
  hist_prep_axes( qtb000, true );
  set_axis_labels( qtb000, "q_{T}^{B} [GeV]", "Entries/0.500 [GeV^{-1}]" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  qtb000->GetYaxis()->SetRangeUser( 0, 1750 );
  TLegend * qtb_legend_a = create_atlas_legend();
  qtb_legend_a->AddEntry( qtb000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qtb_legend_a->AddEntry( qtb112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qtb_legend_a->AddEntry( qtb224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qtb_legend_a->AddEntry( qtb334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qtb_legend_a->AddEntry( qtb445g,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qtb_legend_a->SetX1(0.54);
  qtb_legend_a->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qtb" );



  qta000->Scale( 1.0/qta000->Integral() ); 
  qta111->Scale( 1.0/qta111->Integral() );  
  qta112->Scale( 1.0/qta112->Integral() );  
  qta224->Scale( 1.0/qta224->Integral() );  
  qta334->Scale( 1.0/qta334->Integral() );  
  qta445->Scale( 1.0/qta445->Integral() );  

  qtb000->Scale( 1.0/qtb000->Integral() ); 
  qtb111->Scale( 1.0/qtb111->Integral() );  
  qtb112->Scale( 1.0/qtb112->Integral() );  
  qtb224->Scale( 1.0/qtb224->Integral() );  
  qtb334->Scale( 1.0/qtb334->Integral() );  
  qtb445->Scale( 1.0/qtb445->Integral() );  

  qx000->Scale( 1.0/qx000->Integral() ); 
  qx111->Scale( 1.0/qx111->Integral() );  
  qx112->Scale( 1.0/qx112->Integral() );  
  qx224->Scale( 1.0/qx224->Integral() );  
  qx334->Scale( 1.0/qx334->Integral() );  
  qx445->Scale( 1.0/qx445->Integral() );  

  qy000->Scale( 1.0/qy000->Integral() ); 
  qy112->Scale( 1.0/qy112->Integral() );  
  qy224->Scale( 1.0/qy224->Integral() );  
  qy334->Scale( 1.0/qy334->Integral() );  
  qy445->Scale( 1.0/qy445->Integral() );  

 
  qta000->SetLineColor( tyrian );
  qta112->SetLineColor( carmine   );
  qta224->SetLineColor( rapsberry );
  qta334->SetLineColor( violet );
  qta445->SetLineColor( tiffany );
  qta445g->SetLineColor(tiffany );
  
  qtb000->SetLineColor( tyrian );
  qtb112->SetLineColor( carmine   );
  qtb224->SetLineColor( rapsberry );
  qtb334->SetLineColor( violet );
  qtb445->SetLineColor( tiffany );
  qtb445g->SetLineColor(tiffany );

  qx000->SetLineColor( tyrian );
  qx112->SetLineColor( carmine   );
  qx224->SetLineColor( rapsberry );
  qx334->SetLineColor( violet );
  qx445->SetLineColor( tiffany );
  qx445g->SetLineColor(tiffany );

  qy000->SetLineColor( tyrian );
  qy112->SetLineColor( carmine   );
  qy224->SetLineColor( rapsberry );
  qy334->SetLineColor( violet );
  qy445->SetLineColor( tiffany );
  qy445g->SetLineColor(tiffany );


  
  qta000->SetLineWidth( 2 );
  qta112->SetLineWidth( 2 );
  qta224->SetLineWidth( 2 );
  qta334->SetLineWidth( 2 );
  qta445->SetLineWidth( 2 );
  qta445g->SetLineWidth( 2 );
  qtb000->SetLineWidth( 2 );
  qtb112->SetLineWidth( 2 );
  qtb224->SetLineWidth( 2 );
  qtb334->SetLineWidth( 2 );
  qtb445->SetLineWidth( 2 );
  qtb445g->SetLineWidth( 2 );
  qx000 ->SetLineWidth( 2 ); 
  qx112 ->SetLineWidth( 2 ); 
  qx224 ->SetLineWidth( 2 ); 
  qx334 ->SetLineWidth( 2 ); 
  qx445 ->SetLineWidth( 2 ); 
  qx445g->SetLineWidth( 2 ); 
  qy000 ->SetLineWidth( 2 ); 
  qy112 ->SetLineWidth( 2 ); 
  qy224 ->SetLineWidth( 2 ); 
  qy334 ->SetLineWidth( 2 ); 
  qy445 ->SetLineWidth( 2 ); 
  qy445g->SetLineWidth( 2 );


  qta000->SetFillStyle( 0 );
  qta112->SetFillStyle( 0 );
  qta224->SetFillStyle( 0 );
  qta334->SetFillStyle( 0 );
  qta445->SetFillStyle( 0 );
  qta445g->SetFillStyle(0 );
  qtb000->SetFillStyle( 0 );
  qtb112->SetFillStyle( 0 );
  qtb224->SetFillStyle( 0 );
  qtb334->SetFillStyle( 0 );
  qtb445->SetFillStyle( 0 );
  qtb445g->SetFillStyle(0 );
  qx000 ->SetFillStyle( 0 ); 
  qx112 ->SetFillStyle( 0 ); 
  qx224 ->SetFillStyle( 0 ); 
  qx334 ->SetFillStyle( 0 ); 
  qx445 ->SetFillStyle( 0 ); 
  qx445g->SetFillStyle( 0 ); 
  qy000 ->SetFillStyle( 0 ); 
  qy112 ->SetFillStyle( 0 ); 
  qy224 ->SetFillStyle( 0 ); 
  qy334 ->SetFillStyle( 0 ); 
  qy445 ->SetFillStyle( 0 ); 
  qy445g->SetFillStyle( 0 );



  active_pad = static_cast<TPad *>( gPad->cd() );
  qx000->Draw( "HIST E1" );  
  qx112->Draw( "HIST E1 SAME" ); 
  qx224->Draw( "HIST E1 SAME" ); 
  qx334->Draw( "HIST E1 SAME" ); 
  qx445->Draw( "HIST E1 SAME" ); 
  hist_prep_axes( qx000, true );
  set_axis_labels( qx000, "q_{x} [GeV]", "Arbitrary Units" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  //qx000->GetYaxis()->SetRangeUser( 0, 2500 );
  TLegend * qx_legend_b = create_atlas_legend();
  qx_legend_b->AddEntry( qx000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qx_legend_b->AddEntry( qx112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qx_legend_b->AddEntry( qx224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qx_legend_b->AddEntry( qx334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qx_legend_b->AddEntry( qx445,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qx_legend_b->SetX1(0.54);
  qx_legend_b->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qx_n" );
  
  active_pad = static_cast<TPad *>( gPad->cd() );
  qy000->Draw( "HIST E1" );  
  qy112->Draw( "HIST E1 SAME" ); 
  qy224->Draw( "HIST E1 SAME" ); 
  qy334->Draw( "HIST E1 SAME" ); 
  qy445->Draw( "HIST E1 SAME" ); 
  hist_prep_axes( qy000, true );
  set_axis_labels( qy000, "q_{y} [GeV]", "Arbitrary Units" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  //qy000->GetYaxis()->SetRangeUser( 0, 2500 );
  TLegend * qy_legend_b = create_atlas_legend();
  qy_legend_b->AddEntry( qy000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qy_legend_b->AddEntry( qy112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qy_legend_b->AddEntry( qy224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qy_legend_b->AddEntry( qy334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qy_legend_b->AddEntry( qy445,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qy_legend_b->SetX1(0.54);
  qy_legend_b->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qy_n" );


  active_pad = static_cast<TPad *>( gPad->cd() );
  qta000->Draw( "HIST E1" );  
  qta112->Draw( "HIST E1 SAME" ); 
  qta112->Draw( "HIST E1 SAME" ); 
  qta224->Draw( "HIST E1 SAME" ); 
  qta334->Draw( "HIST E1 SAME" ); 
  qta445->Draw( "HIST E1 SAME" ); 
  hist_prep_axes( qta000, true );
  set_axis_labels( qta000, "q_{T}^{A} [GeV]", "Arbitrary Units" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  //qta000->GetYaxis()->SetRangeUser( 0, 1200 );
  TLegend * qta_legend_b = create_atlas_legend();
  qta_legend_b->AddEntry( qta000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qta_legend_b->AddEntry( qta112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qta_legend_b->AddEntry( qta224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qta_legend_b->AddEntry( qta334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qta_legend_b->AddEntry( qta445,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qta_legend_b->SetX1(0.54);
  qta_legend_b->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qta_n" );

  active_pad = static_cast<TPad *>( gPad->cd() );
  qtb000->Draw( "HIST E1" );  
  qtb112->Draw( "HIST E1 SAME" ); 
  qtb112->Draw( "HIST E1 SAME" ); 
  qtb224->Draw( "HIST E1 SAME" ); 
  qtb334->Draw( "HIST E1 SAME" ); 
  qtb445->Draw( "HIST E1 SAME" ); 
  hist_prep_axes( qtb000, true );
  set_axis_labels( qtb000, "q_{T}^{B} [GeV]", "Arbitrary Units" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  //qtb000->GetYaxis()->SetRangeUser( 0, 1000 );
  TLegend * qtb_legend_b = create_atlas_legend();
  qtb_legend_b->AddEntry( qtb000, "p_{T}(#mu) > 0, p_{T}(#gamma) > 0", "F" );
  qtb_legend_b->AddEntry( qtb112, "p_{T}(#mu) > 1, p_{T}(#gamma) > 2", "F" );
  qtb_legend_b->AddEntry( qtb224, "p_{T}(#mu) > 2, p_{T}(#gamma) > 4", "F" );
  qtb_legend_b->AddEntry( qtb334, "p_{T}(#mu) > 3, p_{T}(#gamma) > 4", "F" );
  qtb_legend_b->AddEntry( qtb445,"p_{T}(#mu) > 4, p_{T}(#gamma) > 5", "F" );
  qtb_legend_b->SetX1(0.54);
  qtb_legend_b->Draw( "SAME" );
  save_and_clear( canv, active_pad, "qtb_n" );



  //Int_t flame = TColor::GetColor( 216, 87, 42 );
  //Int_t khaki = TColor::GetColor( 190, 183, 164 );
  //Int_t honey = TColor::GetColor( 213, 242, 227 );
  //Int_t camb = TColor::GetColor( 115, 186, 155 );

  //Int_t xanthous = TColor::GetColor( 247, 181, 56 );
  //Int_t ochre = TColor::GetColor( 219, 124, 38 ); 
  //Int_t fer = TColor::GetColor( 195, 47, 39 );
  //Int_t burg = TColor::GetColor( 120, 1, 22 );
  //Int_t night = TColor::GetColor( 1, 17, 10 );
  //Int_t orng = TColor::GetColor( 186, 45, 11 );
  //Int_t brg = TColor::GetColor( 0, 62, 31 );



  qta000->SetMarkerStyle( 20 );
  qta112->SetMarkerStyle( 20 );
  qta224->SetMarkerStyle( 20 );
  qta334->SetMarkerStyle( 20 );
  qta445->SetMarkerStyle( 20 );
  qtb000->SetMarkerStyle( 20 );
  qtb112->SetMarkerStyle( 20 );
  qtb224->SetMarkerStyle( 20 );
  qtb334->SetMarkerStyle( 20 );
  qtb445->SetMarkerStyle( 20 );
  qx000->SetMarkerStyle( 20 );
  qx445->SetMarkerStyle( 20 );

  qta000->SetMarkerColor( tyrian );
  qta112->SetMarkerColor( carmine );
  qta224->SetMarkerColor( rapsberry  );
  qta334->SetMarkerColor( violet  );
  qta445->SetMarkerColor( tiffany );
  qta445g->SetMarkerColor( tiffany );

  qtb000->SetMarkerColor( tyrian );
  qtb112->SetMarkerColor( carmine );
  qtb224->SetMarkerColor( rapsberry  );
  qtb334->SetMarkerColor( violet  );
  qtb445->SetMarkerColor( tiffany );
  qtb445g->SetMarkerColor( tiffany );

  qx000->SetMarkerColor( tyrian );
  qx112->SetMarkerColor( carmine );
  qx224->SetMarkerColor( rapsberry  );
  qx334->SetMarkerColor( violet  );
  qx445->SetMarkerColor( tiffany );
  qx445g->SetMarkerColor( tiffany );

  qy000->SetMarkerColor( tyrian );
  qy112->SetMarkerColor( carmine );
  qy224->SetMarkerColor( rapsberry  );
  qy334->SetMarkerColor( violet  );
  qy445->SetMarkerColor( tiffany );
  qy445g->SetMarkerColor( tiffany );

  qta000->SetLineColor( tyrian );
  qta112->SetLineColor( carmine );
  qta224->SetLineColor( rapsberry  );
  qta334->SetLineColor( violet  );
  qta445->SetLineColor( tiffany );
  qta445g->SetLineColor( tiffany );

  qtb000->SetLineColor( tyrian );
  qtb112->SetLineColor( carmine );
  qtb224->SetLineColor( rapsberry  );
  qtb334->SetLineColor( violet  );
  qtb445->SetLineColor( tiffany );
  qtb445g->SetLineColor( tiffany );

  qx000->SetLineColor( tyrian );
  qx112->SetLineColor( carmine );
  qx224->SetLineColor( rapsberry  );
  qx334->SetLineColor( violet  );
  qx445->SetLineColor( tiffany );  
  qx445g->SetLineColor( tiffany );



  qy000->SetLineColor( tyrian );
  qy112->SetLineColor( carmine );
  qy224->SetLineColor( rapsberry  );
  qy334->SetLineColor( violet  );
  qy445->SetLineColor( tiffany );
  qy445g->SetLineColor( tiffany );

  // qta overlay
  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad * >( gPad->cd() );
  qta000->Draw( "P" );
  qta112->Draw( "P SAME" );
  qta224->Draw( "P SAME" );
  qta334->Draw( "P SAME" );
  qta445->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  add_simulation( active_pad );
  hist_prep_axes( qta000, true );
  std::string qta_y_string =  "Arbitrary Units";//qta_bound.get_y_str();
  std::string qta_x_string =  qta_bound.get_ltx() + " [" + qta_bound.get_units() + "]";
  set_axis_labels( qta000, qta_x_string.c_str(), qta_y_string.c_str() );
  qta000->GetYaxis()->SetRangeUser( 0, qta000->GetMaximum()*1.1 );
  TLegend * qta_legend = create_atlas_legend();
  qta_legend->AddEntry( qta000, "p_{T}(#mu)>0, p_{T}(#gamma)>0", "LP" );
  qta_legend->AddEntry( qta112, "p_{T}(#mu)>1, p_{T}(#gamma)>2", "LP" );
  qta_legend->AddEntry( qta224, "p_{T}(#mu)>2, p_{T}(#gamma)>4", "LP" );
  qta_legend->AddEntry( qta334, "p_{T}(#mu)>3, p_{T}(#gamma)>4", "LP" );
  qta_legend->AddEntry( qta445, "p_{T}(#mu)>4, p_{T}(#gamma)>5", "LP" );
  //qta_legend->SetX1NDC( qta_legend->GetX1NDC() - 0.1 );
  qta_legend->SetX1( 0.55 );
  qta_legend->SetY2( 0.775 );
  qta_legend->SetY1( 0.63 );
  qta_legend->Draw();
  qta000->GetYaxis()->SetRangeUser( 0, 0.15 );
  save_and_clear( canv, active_pad, "AOverlay" );

  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad * >( gPad->cd() );
  qtb000->Draw( "P" );
  qtb112->Draw( "P SAME" );
  qtb224->Draw( "P SAME" );
  qtb334->Draw( "P SAME" );
  qtb445->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  add_simulation( active_pad );
  hist_prep_axes( qtb000, true );
  //std::string qtb_y_string =  "Normalised Entries/0.500 [GeV^{-1}]";//qta_bound.get_y_str();
  std::string qtb_y_string =  "Arbitrary Units";//qta_bound.get_y_str();
  std::string qtb_x_string =  qtb_bound.get_ltx() + " [" + qtb_bound.get_units() + "]";
  set_axis_labels( qtb000, qtb_x_string.c_str(), qtb_y_string.c_str() );
  qtb000->GetYaxis()->SetRangeUser( 0, qtb000->GetMaximum()*1.1 );
  TLegend * qtb_legend = create_atlas_legend();
  qtb_legend->AddEntry( qtb000, "p_{T}(#mu)>0 p_{T}(#gamma)>0", "LP" );
  qtb_legend->AddEntry( qtb112, "p_{T}(#mu)>1 p_{T}(#gamma)>2", "LP" );
  qtb_legend->AddEntry( qtb224, "p_{T}(#mu)>2 p_{T}(#gamma)>4", "LP" );
  qtb_legend->AddEntry( qtb334, "p_{T}(#mu)>3 p_{T}(#gamma)>4", "LP" );
  qtb_legend->AddEntry( qtb445, "p_{T}(#mu)>4 p_{T}(#gamma)>5", "LP" );
  //qtb_legend->SetX1NDC( qtb_legend->GetX1NDC() - 0.1 );
  qtb_legend->SetX1( 0.55 );
  qtb_legend->SetY2( 0.775 );
  qtb_legend->SetY1( 0.63 );
  qtb_legend->Draw();
  qtb000->GetYaxis()->SetRangeUser( 0, 0.15 );
  save_and_clear( canv, active_pad, "BOverlay" );

  canv.Divide( 1, 1 );
  active_pad = static_cast< TPad * >( gPad->cd() );
  qx000->SetMarkerColor( kBlack );
  qx000->SetLineColor( kBlack );
  qx445->SetMarkerColor( rapsberry );
  qx445->SetLineColor( rapsberry );
  qx000->Draw( "P" );
  //qx112->Draw( "P SAME" );
  //qx224->Draw( "P SAME" );
  //qx334->Draw( "P SAME" );
  qx445->Draw( "P SAME" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  add_simulation( active_pad );
  hist_prep_axes( qx000, true );
  //std::string qx_y_string =  "Normalised Entries/0.750 [GeV^{-1}]";//qta_bound.get_y_str();
  std::string qx_y_string =  "Arbitrary Units";
  std::string qx_x_string =  qx_bound.get_ltx() + " [" + qx_bound.get_units() + "]";
  set_axis_labels( qx000, qx_x_string.c_str(), qx_y_string.c_str() );
  qx000->GetYaxis()->SetRangeUser( 0, qx000->GetMaximum()*1.1 );
  TLegend * qx_legend = create_atlas_legend();
  qx_legend->AddEntry( qx000, "p_{T,#mu} > 0, p_{T,#gamma} > 0", "LP" );
  //qx_legend->AddEntry( qx112, "#mu>1 #mu>1 #gamma>2", "LP" );
  //qx_legend->AddEntry( qx224, "#mu>2 #mu>2 #gamma>4", "LP" );
  //qx_legend->AddEntry( qx334, "#mu>3 #mu>3 #gamma>4", "LP" );
  qx_legend->AddEntry( qx445, "p_{T,#mu} > 4, p_{T,#gamma} > 5", "LP" );
  qx_legend->SetX1( 0.55 );
  qx_legend->SetY2( 0.775 );
  qx_legend->SetY1( 0.675 );
  qx000->GetYaxis()->SetRangeUser( 0, 0.15 );
  //qx_legend->SetX1NDC( 0.5 );
  qx_legend->Draw( "SAME" );
  save_and_clear( canv, active_pad, "XOverlay" );
  qx000->SetLineColor( tyrian );
  qx445g->SetLineColor( tiffany );

  std::string column_names = "$p_{T}(\\mu)$ & $p_{T}(\\gamma)$ & Events & $\\bar{q_{T}^{A}}$ & $\\bar{q_{T}^{B}}$ & $\\bar{q_{x}}$ & $\\bar{q_{y}}$"
  "& $\\sigma(q_{T}^{A})$ & $\\sigma(q_{T}^{B})$ & $\\sigma(q_{x})$ & $\\sigma(q_{y})$ & Notes \\\\ \\hline\n";
  std::string table_layout = "|c|c|r|r|r|r|r|r|r|r|r|l|";

  // open file and write prelude.
  std::ofstream stats_table( "accept.tex" );
  stats_table << "\\documentclass[landscape]{article}\n";
  stats_table << "\\usepackage[paperheight=4in,paperwidth=10in]{geometry}\n";
  stats_table << "\\usepackage{makecell}";
  stats_table << "\\begin{document}";
  stats_table << std::fixed << std::setprecision( 2 );
  stats_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{" << table_layout << "}\n\\hline\n";
  stats_table << column_names;
  stats_table << " 0 & 0 & " << std::setprecision(0) <<  qta000->GetEntries() << "&" << std::setprecision(2) << qta000M->GetMean() << "&" << qtb000->GetMean() << "&" << qx000->GetMean() << "&" << qy000->GetMean() << "&";
  stats_table << qta000->GetStdDev() << "&" << qtb000->GetStdDev() << "&" << qx000->GetStdDev() << "&" <<  qy000->GetStdDev() << "& \\\\ \\hline\n";
  stats_table << " 1 & 2 & " << std::setprecision(0) << qta112->GetEntries() << "&" << std::setprecision(2) <<  qta112M->GetMean() << "&" << qtb112->GetMean() << "&" << qx112->GetMean() << "&" << qy112->GetMean() << "&";
  stats_table << qta112->GetStdDev() << "&" << qtb112->GetStdDev() << "&" << qx112->GetStdDev() << "&" <<  qy112->GetStdDev() << "& \\\\ \\hline\n";
  stats_table << " 2 & 4 & " << std::setprecision(0) << qta224->GetEntries() << "&" << std::setprecision(2) <<  qta224M->GetMean() << "&" << qtb224->GetMean() << "&" << qx224->GetMean() << "&" << qy224->GetMean() << "&";
  stats_table << qta224->GetStdDev() << "&" << qtb224->GetStdDev() << "&" << qx224->GetStdDev() << "&" <<  qy224->GetStdDev() << "& \\\\ \\hline\n";
  stats_table << " 3 & 4 & " << std::setprecision(0) << qta334->GetEntries() << "&" << std::setprecision(2) << qta334M->GetMean() << "&" << qtb334->GetMean() << "&" << qx334->GetMean() << "&" << qy334->GetMean() << "&";
  stats_table << qta334->GetStdDev() << "&" << qtb334->GetStdDev() << "&" << qx334->GetStdDev() << "&" <<  qy334->GetStdDev() << "& \\\\ \\hline\n";
  stats_table << " 4 & 5 & " << std::setprecision(0) << qta445g->GetEntries() << "&" << std::setprecision(2) << qta445Mg->GetMean() << "&" << qtb445g->GetMean() << "&" << qx445g->GetMean() << "&" << qy445g->GetMean() << "&";
  stats_table << qta445g->GetStdDev() << "&" << qtb445g->GetStdDev() << "&"<< qx445g->GetStdDev() << "&" <<  qy445g->GetStdDev() << "& \\\\ \\hline\n";
  stats_table << " 4 & 5 & " << std::setprecision(0) << qta445->GetEntries() << "&" << std::setprecision(2) << qta445M->GetMean() << "&" << qtb445->GetMean() << "&" << qx445->GetMean() << "&" << qy445->GetMean() << "&";
  stats_table << qta445->GetStdDev() << "&" << qtb445->GetStdDev() << "&" << qx445->GetStdDev() << "&" <<  qy445->GetStdDev() << "&  \\makecell[l]{ Sample with\\\\increased generator\\\\level cuts} \\\\ \\hline \n";
  // finish documennd and close
  stats_table << "\\end{tabular}\n\\caption{Things}\n\\end{table}\n\\end{document}";
  stats_table.close();
  



  //TCanvas canv4( "canv", "", 200, 200, 1000, 1000 );
  //canv4.Divide( 1, 1 );
  //qtb000->Draw( "E1" );
  /////qtb111->Draw( "E1 SAME" );
  //qtb112->Draw( "E1 SAME" );
  //qtb224->Draw( "E1 SAME" );
  //qtb334->Draw( "E1 SAME" );
  //qtb445->Draw( "E1 SAME" );
  //qtb000->GetYaxis()->SetRangeUser( 0, qtb000->GetMaximum()*1.5 );
  //TLegend * qtb_legend = plotting::legend( top_right, 3 );
  //qtb_legend->AddEntry( qtb000, "#mu>0 #mu>0 #gamma>0" );
  //qtb_legend->AddEntry( qtb112, "#mu>1 #mu>1 #gamma>2" );
  //qtb_legend->AddEntry( qtb224, "#mu>2 #mu>2 #gamma>4" );
  //qtb_legend->AddEntry( qtb334, "#mu>3 #mu>3 #gamma>4" );
  //qtb_legend->AddEntry( qtb445, "#mu>4 #mu>4 #gamma>5" );
  //qtb_legend->SetX1NDC( qtb_legend->GetX1NDC() - 0.1 );
  //qtb_legend->Draw();
  //canv4.SaveAs( "BOverlay.png" );

  //gen_tree->Draw( "qx>>qx000", cut000q3.c_str(), "goff" ); 
  //gen_tree->Draw( "qx>>qx445", cut445q3.c_str(), "goff" ); 
  //qx000->Scale( 1.0/qx000->Integral() ); 
  //qx445->Scale( 1.0/qx445->Integral() );  


  //TCanvas canv5( "canv", "", 200, 200, 1000, 1000 );
  //canv5.Divide( 1, 1 );
  //canv5.cd( 1 );
  //qx000->Draw( "E1" );
  //qx445->Draw( "E1 SAME" );
  ////plotting::title( "q_{x}, no cuts", over_top_left, false);
  ////plotting::axes_titles( qx000, "q_{x} [GeV]", "Normalised Entries" );
  ////plotting::atlas( top_left, true, true );
  //qx000->GetYaxis()->SetRangeUser( 0, qx000->GetMaximum()*1.5 );
  //canv5.SaveAs( "qxcut.png" );



}

//gen_tree->Draw( "qtA>>qta000", cut000q3.c_str(), "goff" ); 
//gen_tree->Draw( "qtA>>qta445", cut445q3.c_str(), "goff" ); 
//qta000->Scale( 1.0/qta000->Integral() ); 
//qta445->Scale( 1.0/qta445->Integral() );  
//TCanvas canv6( "canv", "", 200, 200, 1000, 1000 );
//canv6.Divide( 1, 1 );
//canv6.cd( 1 );
//qta000->Draw( "E1" );
//qta445->Draw( "E1 SAME" );
//plotting::title( "q_{T}^{A}, no cuts", over_top_left, false);
//plotting::axes_titles( qta000, "q_{T}^{A} [GeV]", "Normalised Entries" );
//plotting::atlas( top_left, true, true );
//qta000->GetYaxis()->SetRangeUser( 0, qta000->GetMaximum()*1.5 );
//canv6.SaveAs( "qtacut.png" );
//
//gen_tree->Draw( "qtB>>qtb000", cut000q3.c_str(), "goff" ); 
//gen_tree->Draw( "qtB>>qtb445", cut445q3.c_str(), "goff" ); 
//qtb000->Scale( 1.0/qtb000->Integral() ); 
//qtb445->Scale( 1.0/qtb445->Integral() );  
//TCanvas canv7( "canv", "", 200, 200, 1000, 1000 );
//canv7.Divide( 1, 1 );
//canv7.cd( 1 );
//qtb000->Draw( "E1" );
//qtb445->Draw( "E1 SAME" );
//plotting::title( "q_{T}^{B}, no cuts", over_top_left, false);
//plotting::axes_titles( qtb000, "q_{T}^{B} [GeV]", "Normalised Entries" );
//plotting::atlas( top_left, true, true );
//qtb000->GetYaxis()->SetRangeUser( 0, qtb000->GetMaximum()*1.5 );
//canv7.SaveAs( "qtbcut.png" );


//TCanvas canv( "canv", "", 200, 200, 6000, 1000 );
  //canv.Divide( 6, 1 );

  //canv.cd(1);
  //qta000->Draw( "HIST" );
  //canv.cd(2);
  //qta111->Draw( "HIST" );
  //canv.cd(3);
  //qta112->Draw( "HIST" );
  //canv.cd(4);
  //qta334->Draw( "HIST" );
  //canv.cd(5);
  //qta445->Draw( "HIST" );
  //canv.cd(6);
  //qta449->Draw( "HIST" );
  //canv.SaveAs( "qta.png" );

  //TCanvas canv2( "canv", "", 200, 200, 6000, 1000 );
  //canv2.Divide( 6, 1 );

  //canv2.cd(1);
  //qx000->Draw( "HIST" );
  //canv2.cd(2);
  //qx111->Draw( "HIST" );
  //canv2.cd(3);
  //qx112->Draw( "HIST" );
  //canv2.cd(4);
  //qx334->Draw( "HIST" );
  //canv2.cd(5);
  //qx445->Draw( "HIST" );
  //canv2.cd(6);
  //qx449->Draw( "HIST" );
  //canv2.SaveAs( "qx.png" );

  //Int_t night = TColor::GetColor( "01110a" );
  //Int_t orng = TColor::GetColor( "ba2d0b" );
  //Int_t honey = TColor::GetColor( "d5d2e3" );
  //Int_t camb = TColor::GetColor( "73ba9b" );
  //Int_t brg = TColor::GetColor( "003e1f" );
