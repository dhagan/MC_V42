#include <pre_cut.hxx>
#include <sys/stat.h>
#include <yaml-cpp/yaml.h>

void pre_cut( YAML::Node & run_node, variable_set & variables ){

  prep_style();
  gStyle->SetOptStat( "imr" );
  gStyle->SetTitleYOffset(1.4);

  std::string in_path = std::string( getenv( "IN_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::vector< std::string > type_strs = run_node[ "types" ].as< std::vector< std::string > >();

  std::string dir_name = "./pre_cut";
  mkdir( dir_name.c_str(), 744 );
  chdir( dir_name.c_str() );
  mkdir( "./pdf", 744 );

  //std::vector< std::string > output_columns = { "ActIpX", "AvgIpX", "AbsCosTheta", "AbsdPhi", "AbsPhi", "DPhi", "DY", "Phi", "costheta", 
	//														 "EventNumber", "Lambda", "qTSquared", "DiMuonMass", "DiMuonTau", "Trigger_HLT_2mu4_bJpsimumu_noL2", 
	//														 "DiMuonPt", "PhotonPt", "AbsdY", "qxpsi", "qypsi", "qxgamma", "qygamma", "qxsum", "qysum", "qtA", "qtB",
	//														 "qtL", "qtM", "JPsi_Eta", "Phot_Eta", "DeltaZ0", "qx", "qy", "MuPos_Pt", "MuPos_Eta", "MuNeg_Pt", "MuNeg_Eta", "DiLept_DeltaR", "DiLept_Rap" };

  //std::vector< std::string > output_columns = { "cos2theta", "DPhi", "AbsPhi", "DY", "Lambda" };
  //std::vector< std::string > output_columns = { "cos2theta", "DPhi", "AbsPhi", "DY", 
  //std::vector< std::string > output_columns = { "DiMuonMass" };
  std::vector< std::string > output_columns = { "PhotonPt", "DiMuonPt", "DY" };

  auto phi_lambda = phi_calc;
	auto dphi_lambda = dphi_calc;

  for ( std::string & type : type_strs ){

    std::string file_path = std::string( std::getenv("IN_PATH") ) + "/ntuples/" + type + "_ntuple.root";
    TChain chain( "tree" );
    chain.Add( file_path.c_str() );

    ROOT::RDataFrame input_frame( chain );
    auto filtered = input_frame.Filter( "Photon_Pt.size() != 0" );
	  filtered = filtered.Define( "ActIpX", 															"Float_t( actIpX )" );	
	  filtered = filtered.Define( "AvgIpX", 																			"Float_t( avgIpX )" );	
	  filtered = filtered.Define( "costheta", 																		"Float_t( MuMuGamma_CS_CosTheta[0] )" );
	  filtered = filtered.Define( "AbsCosTheta", 																	"Float_t( abs(costheta) )" );
    filtered = filtered.Define( "cos2theta", 																		"Float_t( MuMuGamma_CS_CosTheta[0] )" );
	  filtered = filtered.Define( "DPhi",                                         dphi_lambda, { "DiLept_Phi", "Photon_Phi" } );
	  filtered = filtered.Define( "AbsdPhi", 																			"Float_t( abs(DPhi) )" );
	  filtered = filtered.Define( "Phi",                                          phi_lambda, {"MuMuGamma_CS_Phi"} );
	  filtered = filtered.Define( "AbsPhi", 																			"Float_t( sqrt( MuMuGamma_CS_Phi[0] * MuMuGamma_CS_Phi[0]) )" );
	  filtered = filtered.Define( "DY", 																					"Float_t(DiLept_Y[0] - Photon_Eta[0])" );
	  filtered = filtered.Define( "AbsdY", 																				"Float_t( abs(DY) )" );
	  filtered = filtered.Define( "EventNumber",																	"Float_t( evt )" );
	  filtered = filtered.Define( "Lambda", 																			"Float_t( (MuMuY_M[0]/3097.0)*(MuMuY_M[0]/3097.0) )" );
	  filtered = filtered.Define( "qTSquared",																		"Float_t( (MuMuY_Pt[0]/1000.0) * (MuMuY_Pt[0]/1000.0) )" );
	  filtered = filtered.Define( "DiMuonMass",																		"Float_t( DiLept_M[0]/1000.0 )" );
	  filtered = filtered.Define( "DiMuonTau",																		"Float_t( DiMuonVertex_Tau[0] )" );
	  filtered = filtered.Define( "Trigger_HLT_2mu4_bJpsimumu_noL2",							"Float_t( HLT_2mu4_bJpsimumu_noL2 )" );
	  filtered = filtered.Define( "DiMuonPt",																			"Float_t( DiLept_Pt[0]/1000.0 )" );
	  filtered = filtered.Define( "PhotonPt",																			"Float_t( Photon_Pt[0]/1000.0 )" );
	  filtered = filtered.Define( "qxpsi",																				"Float_t( DiMuonPt + (PhotonPt)*cos(DPhi) )" );
	  filtered = filtered.Define( "qypsi",																				"Float_t( (PhotonPt)*sin(DPhi) )" );
	  filtered = filtered.Define( "qxgamma",																			"Float_t( PhotonPt + (DiMuonPt)*cos(DPhi) )" );
	  filtered = filtered.Define( "qygamma",																			"Float_t( (DiMuonPt)*sin(DPhi) )" );
	  filtered = filtered.Define( "qtA",																					"Float_t( DiMuonPt - PhotonPt )" );
	  filtered = filtered.Define( "qtB",																					"Float_t( sqrt( (DiMuonPt)*(PhotonPt) )*sin(DPhi) )" );
	  filtered = filtered.Define( "qtL",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*cos( (M_PI-abs(DPhi))/2.0 ) )" );
	  filtered = filtered.Define( "qtM",																					"Float_t( ( (DiMuonPt) - (PhotonPt) )*sin( (M_PI-abs(DPhi))/2.0 ) )" );
	  filtered = filtered.Define( "JPsi_Eta",																			"Float_t( DiLept_Eta[0] )" );
	  filtered = filtered.Define( "Phot_Eta",																			"Float_t( Photon_Eta[0] )" );
    filtered = filtered.Define( "MuPos_Eta",                                    "Float_t( MuPlus_Eta[0] )" );
    filtered = filtered.Define( "MuNeg_Eta",                                    "Float_t( MuMinus_Eta[0] )" );
    filtered = filtered.Define( "MuPos_Pt",                                     "Float_t( MuPlus_Pt[0]/1000.0 )" );
    filtered = filtered.Define( "MuNeg_Pt",                                     "Float_t( MuMinus_Pt[0]/1000.0 )" );
    filtered = filtered.Define( "DiLept_Rap",                                   "Float_t( DiLept_Y[0] )" );
    filtered = filtered.Define( "DiLept_DeltaR",                                "Float_t( DiLept_dR[0] )" );
	  filtered = filtered.Define( "DeltaZ0",																			"Float_t( DiMuon_DeltaZ0[0] )" );
    filtered = filtered.Define( "qx",                                           "Float_t( sqrt(qTSquared)*cos( MuMuY_Phi[0] ) )" );
    filtered = filtered.Define( "qy",                                           "Float_t( sqrt(qTSquared)*sin( MuMuY_Phi[0] ) )" );


    for ( std::string & variable : output_columns ){

      bound & var_bound = variables.bound_manager->get_bound( variable );
      TH1D var_hist = *filtered.Histo1D( { "var", "", 50, var_bound.get_min(), var_bound.get_max() }, variable );
      var_hist.SetMarkerStyle( 20 );
      var_hist.SetLineStyle( 1 );
      var_hist.SetLineColor( 1 );

      TCanvas * canvas = new TCanvas( "canv", "", 200, 200, 1000, 1000 );
      canvas->Divide( 1, 1 );
      TPad * active_pad = static_cast<TPad *>( canvas->cd( 1 ) );
      var_hist.Draw( "HIST E1" );
      add_atlas_decorations( active_pad, false, false );
      add_internal( active_pad );
      hist_prep_axes( &var_hist, true );
      std::cout << var_bound.get_units() << std::endl;
      std::string y_string =  "Entries/" + var_bound.get_y_str();
      std::string x_string =  var_bound.get_x_str();
      set_axis_labels( &var_hist, x_string.c_str(), y_string.c_str() );
      TPaveStats * var_stats = make_stats( &var_hist );
      var_stats->Draw( "SAME" );
      var_stats->SetLineWidth( 0 );
      TLegend * var_legend = below_logo_legend();
      var_legend->AddEntry( &var_hist, type.c_str() );
      var_legend->Draw( "SAME" );
      active_pad->SetBottomMargin( 0.1 );
      active_pad->SetFillStyle( 4000 );
      canvas->SetFillStyle( 4000 );
      std::string output_filename_pdf = "./pdf/" + type + "_" + variable + ".pdf";
      canvas->SaveAs( output_filename_pdf.c_str() );

      delete canvas;

    }


  }

  chdir( ".." );

}
