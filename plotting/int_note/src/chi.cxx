#include <funcs.hxx>
#include <chi.hxx>
#include <sys/stat.h>

inline bool file_exists(const std::string& name) {
  struct stat buffer;   
  return ( stat( name.c_str(), &buffer ) == 0 ); 
}

void chi( YAML::Node & run_node ){

  prep_style();

  mkdir( "./chi", 744 );
  chdir( "./chi" );

  std::string in_path = std::string( getenv( "OUT_PATH" ) );
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string postcut_filepath = in_path + "/trees/" 
    + unique + "/data/data_" + unique + ".root"; 

  TFile * postcut_file = new TFile( postcut_filepath.c_str(), "READ" );
  TTree * postcut_tree = static_cast<TTree *>( postcut_file->Get( "tree" ) );

  bool process_ntuple = !file_exists( "./chi_snapshot.root" );
  if ( process_ntuple ){
    TChain input_chain;
    std::string precut_filepath = std::string( std::getenv( "IN_PATH" ) ) + "/ntuples/" 
      + "data_ntuple.root/tree";
    std::cout << precut_filepath << std::endl;
    input_chain.Add( precut_filepath.c_str());
    ROOT::RDataFrame input_frame( input_chain );
    auto frame = input_frame.Filter( "(Photon_Pt.size() != 0)" );
    frame = frame.Define( "minv", "Float_t( MuMuY_M[0]/1e3 )" );
    frame = frame.Define( "mumu_mass",  "Float_t( DiLept_M[0]/1000.0 )" );
    frame = frame.Define( "photon_eta",	"Float_t( Photon_Eta[0] )" );
    frame = frame.Define( "mupos_eta",  "Float_t( MuPlus_Eta[0] )" );
    frame = frame.Define( "muneg_eta",  "Float_t( MuMinus_Eta[0] )" );
    frame = frame.Define( "deltaEta_mu", "abs( MuMinus_Eta[0] - MuPlus_Eta[0] ) "  );
    frame = frame.Define( "deltaPhi_mu", "abs( MuMinus_Phi[0] - MuPlus_Phi[0] )"  );
    frame = frame.Define( "minvres", "Float_t( minv - mumu_mass + 3.097 )"  );
    frame = frame.Define( "photpt", "Photon_Pt[0]/1000.0"  );
    frame = frame.Define( "jpsipt", "DiLept_Pt[0]/1000.0"  );
    frame = frame.Define( "mumugamma_mass", "MuMuY_M[0]/1000.0"  );
    frame = frame.Filter( "abs( muneg_eta ) < 2.4" );
    frame = frame.Filter( "abs( mupos_eta ) < 2.4" );
    frame = frame.Filter( "abs( photon_eta ) < 2.4" );
    frame.Snapshot( "tree", "./chi_snapshot.root", "" );

  }
 
  TFile * precut_file = new TFile( "./chi_snapshot.root", "READ" );
  TTree * precut_tree = static_cast<TTree *>( precut_file->Get( "tree" ) );

  TH1F * precut = new TH1F( "precut", "", 60, 3.0, 4.0 );
  TH1F * precut_corr = new TH1F( "precut_corr", "", 60, 3.0, 4.0 );
  TH1F * postcut = new TH1F( "postcut", "", 60, 3.0, 4.0 );
  TH1F * postcut_corr = new TH1F( "postcut_corr", "", 60, 3, 4.0 );

  std::vector<std::string> cuts = run_node["cut"].as<std::vector<std::string>>();

  precut_tree->Draw( "mumugamma_mass>>precut", "", "e goff" );
  precut_tree->Draw( "mumugamma_mass-mumu_mass+3.096>>precut_corr", "", "e goff" );

  postcut_tree->Draw( "(sqrt(Lambda)*3.096)>>postcut", "", "e goff" );
  postcut_tree->Draw( "(sqrt(Lambda)*3.096)-DiMuonMass/1000.0+3.096>>postcut_corr", "", "e goff" );

  for ( std::string & cut : cuts ){
    cut += "&&Photon_quality[0]==0";
  }

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );

  gStyle->SetOptStat( "rme" );

  canv.Divide( 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  precut->Draw( "HIST" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( precut, "M(J/#psi+#gamma)", "Entries" );
  hist_prep_axes( precut, true );
  TPaveStats * stat = make_stats( precut );
  stat->Draw("SAME");
  TLegend * legend1 = below_logo_legend();
  legend1->AddEntry( precut_corr, "precut");
  legend1->Draw();
  save_and_clear( canv, active_pad, "precut" );

  canv.Divide( 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  precut_corr->Draw( "HIST" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( precut_corr, "M(J/#psi+#gamma)", "Entries" );
  hist_prep_axes( precut_corr, true );
  stat = make_stats( precut_corr );
  TLegend * legend2 = below_logo_legend();
  legend2->AddEntry( precut_corr, "precut, M(J/#psi) resolution corrected");
  legend2->Draw();
  save_and_clear( canv, active_pad, "precut_corr" );

  canv.Divide( 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  postcut->Draw( "HIST" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( postcut, "M(J/#psi+#gamma)", "Entries" );
  hist_prep_axes( postcut, true );
  stat = make_stats( postcut );
  TLegend * legend3 = below_logo_legend();
  legend3->AddEntry( precut_corr, "post cut");
  legend3->Draw();
  save_and_clear( canv, active_pad, "postcut" );

  canv.Divide( 1 );
  active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  postcut_corr->Draw( "HIST" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( postcut_corr, "M(J/#psi+#gamma)", "Entries" );
  hist_prep_axes( postcut_corr, true );
  stat = make_stats( postcut_corr );
  TLegend * legend4 = below_logo_legend();
  legend4->AddEntry( precut_corr, "post cut, M(J/#psi) resolution corrected");
  legend4->Draw();
  save_and_clear( canv, active_pad, "postcut_corr" );


  for ( size_t cut = 0; cut < cuts.size(); cut++ ){

    precut->Reset();
    precut_corr->Reset();

    precut_tree->Draw( "mumugamma_mass>>precut", cuts[cut].c_str(), "e goff" );
    precut_tree->Draw( "mumugamma_mass-mumu_mass+3.096>>precut_corr", cuts[cut].c_str(), "e goff" );

    std::cout << cuts[cut] << std::endl;
    
    canv.Divide( 1 );
    active_pad = static_cast< TPad *>( canv.cd( 1 ) );
    precut_corr->Draw( "HIST" );
    add_atlas_decorations( active_pad, false, false );
    add_internal( active_pad );
    set_axis_labels( precut_corr, "M(J/#psi+#gamma)", "Entries" );
    add_pad_title( active_pad, cuts[cut].c_str());
    TLegend * legend2 = below_logo_legend();
    legend2->AddEntry( precut_corr, "precut, M(J/#psi) resolution corrected");
    legend2->Draw();
    stat = make_stats( precut_corr );
    std::string prec_name = "precut_corr_" + std::to_string( cut );
    save_and_clear( canv, active_pad, prec_name );

  }
 
  chdir( ".." );


}