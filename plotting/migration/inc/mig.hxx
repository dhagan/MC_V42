#ifndef mig_hxx
#define mig_hxx

#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TChain.h>
#include "TString.h"
#include "TROOT.h"
#include "TGaxis.h"
#include "TCut.h"

#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <math.h>
#include <fstream>
#include <cassert>

#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

#include <anna.hxx>

struct result{
  std::string fit_unique;
  double x;
  double sigma;
};

void mig();

void create_mig_image( bound_mgr * selections, hist_store * store, std::string var, std::string mass, TTree * tree, 
                      std::string cut, std::vector<result> * fit_data=nullptr, std::string unique="", bool non_reco=false );
void create_delta_image( bound_mgr * selections, std::string var, std::string mass, TTree * tree, 
                      std::string cut, std::string unique="" );
void fit_plots( bound_mgr * selections, std::vector<result> * results, std::string mass );


#endif
