#include <mig.hxx>

void th(){

  prep_style();
  //gStyle->SetPalette( kBlueRedYellow );
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();

  std::map< std::string, std::vector<double> > bins_map;
  bins_map["qtA"] = { 60, -10, 20 };
  bins_map["qtB"] = { 60,  0, 20 };
  bins_map["lambda"] = { 5, 0, 200 };

  //TFile * mig_file = new TFile("~/analysis/analysisFork/445/TMVATrees/run/gen_base/Migrations_sign.root", "READ" );
  TFile * mig_file = new TFile("~/analysis/analysisFork/445/TMVATrees/run/gen_base/Migrations_sign.root", "READ" );
  
  std::vector< int > mass_bins = { 0, 3, 4, 5, 12 };
  std::vector< std::string > vars = { "qtA", "qtB", "lambda" };

  double lambda_bins[] = { 0, 15, 25, 50, 100, 200 };

  TF1 * line = prep_line( -10, 20 );
  TF1 * dg_truth = prep_dg( -10, 20 );
  TF1 * dg_reco = prep_dg( -10, 20 );

  std::vector< float > reco_style = { 1, 0, 0, 0, 1, kBlue+1, 1.00, 1, 0, 0, 0};
  std::vector< float > truth_style = { 1, 0, 0, 0, 1, kRed+1, 1.00, 1, 0, 0, 0};
  std::vector< float > ratio_style = { 1, 0, 0, 0, 1, kMagenta+2, 1.00, 1, 0, 0, 0};
  std::vector< float > fit_style = { 1, 1, 1.00, 2 };

  style_func( dg_truth, fit_style );
  style_func( dg_reco, fit_style );
  style_func( line, fit_style );

  for ( std::string var : vars ){

    int bins = (int) bins_map[var].at( 0 );
    double bin_min = bins_map[var].at( 1 );
    double bin_max = bins_map[var].at( 2 );
    double width = ( bin_max - bin_min )/( (double) bins );

    std::string bin_str;
    if ( var.find("Lambda") == std::string::npos ){
      bin_str = std::string( Form( "%.3f GeV", width ) );
    } else { bin_str = "asym bin GeV"; }

    for ( int mass_no : mass_bins ){
  
      TH2F * mig_hist = (TH2F*) mig_file->Get( Form( "mig_%s_Q%i", var.c_str(), mass_no ) );
      TH1F * reco_hist  = (TH1F*) mig_hist->ProjectionY();
      TH1F * truth_hist = (TH1F*) mig_hist->ProjectionX();
      TH1F * ratio_hist = new TH1F( Form( "ratio_%s_Q%i", var.c_str(), mass_no ), "", bins, bin_min, bin_max );
      if ( var.find("lambda") != std::string::npos ){
        ratio_hist = new TH1F( Form( "ratio_%s_Q%i_l", var.c_str(), mass_no ), "", bins, lambda_bins );
      }
      ratio_hist->Divide( reco_hist, truth_hist, 1.0, 1.0 );
  
      TCanvas * canv = new TCanvas( Form( "canv_%s_Q%i", var.c_str(), mass_no ),"",200,200,2000,2000);
      canv->Divide(2,2);

      TPad * current_pad = (TPad*) canv->cd( 1 );
      mig_hist->Draw("COLZ");
      set_2d_axis_labels( mig_hist, Form( "truth entries/%s", bin_str.c_str() ), Form( "reco entries/%s", bin_str.c_str() ) );
      hist_prep_axes( mig_hist );
      add_atlas_decorations( current_pad, true, false );
      mig_hist->GetZaxis()->SetMaxDigits(3);
      mig_hist->GetYaxis()->SetRangeUser( bin_min, bin_max );
      mig_hist->GetXaxis()->SetRangeUser( bin_min, bin_max );
      add_pad_title( current_pad, Form("%s Migration Matrix Q%i", var.c_str(), mass_no ), false );


      current_pad = (TPad*) canv->cd( 2 );
      reco_hist->Draw("HIST");
      reco_hist->Draw("E1 SAME");
      style_hist( reco_hist, reco_style );
      hist_prep_axes( reco_hist );
      add_atlas_decorations( current_pad, true, false );
      set_axis_labels( reco_hist, Form("%s (GeV)", var.c_str() ), Form("reco projection/%s", bin_str.c_str() ) );
      add_pad_title( current_pad, Form( "Reco migration projection Q%i", mass_no ) );
      align_dg( dg_reco, reco_hist );
      reco_hist->Fit( dg_reco, "MQ","", bin_min, bin_max );
      TPaveStats * reco_gauss_fit = get_fit_stats( reco_hist );
      reco_gauss_fit->Draw( "SAME" );
      TLegend * reco_legend = below_logo_legend();
      reco_legend->AddEntry( reco_hist, "Reco projection","L");
      reco_legend->AddEntry( dg_reco, "Gaussian fit","L");
      reco_legend->Draw();


      current_pad = (TPad*) canv->cd( 3 );
      truth_hist->Draw("HIST");
      truth_hist->Draw("E1 SAME");
      style_hist( truth_hist, truth_style );
      set_axis_labels( truth_hist,  Form("%s (GeV)", var.c_str() ), Form("truth projection/%s", bin_str.c_str() ) );
      hist_prep_axes( truth_hist );
      add_atlas_decorations( current_pad, true, false );
      add_pad_title( current_pad, Form( "Truth migration projection Q%i", mass_no ) );
      align_dg( dg_truth, truth_hist );
      truth_hist->Fit( dg_truth, "MQ","", bin_min, bin_max );
      TPaveStats * truth_gauss_fit = get_fit_stats( truth_hist );
      truth_gauss_fit->Draw( "SAME" );
      TLegend * truth_legend = below_logo_legend();
      truth_legend->AddEntry( truth_hist, "Truth projection", "L" );
      truth_legend->AddEntry( dg_truth, "Gaussian fit", "L" );
      truth_legend->Draw();


      current_pad = (TPad*) canv->cd( 4 );
      ratio_hist->Draw("HIST");
      ratio_hist->SetMarkerStyle(21);
      ratio_hist->SetLineColor(1);
      style_hist( ratio_hist, ratio_style );
      set_axis_labels( ratio_hist,  Form("%s (GeV)", var.c_str()), Form("truth/reco proj ratio/%s", bin_str.c_str()) );
      hist_prep_axes( ratio_hist );
      add_atlas_decorations( current_pad, true, false );
      add_pad_title( current_pad, Form( "%s projection ratio Q%i", var.c_str(), mass_no ), false );
      ratio_hist->Fit( line, "MQ", "", bin_min, bin_max );
      TPaveStats * ratio_line_fit = get_fit_stats( ratio_hist );
      TLegend * ratio_legend = below_logo_legend();
      ratio_legend->AddEntry( ratio_hist, "Reco/Truth projection", "L" );
      ratio_legend->AddEntry( line, "ratio fit", "L" );
      ratio_legend->Draw();
      line->Draw( "SAME" );

      //gStyle->SetEndErrorSize( 2. );


      canv->SaveAs( Form( "./Mig_Q%i_%s.png", mass_no, var.c_str() ));
      delete canv;

    }
  }
}



