#include <mig.hxx>

void create_mig_image( bound_mgr * selections, hist_store * store, std::string var, std::string mass, TTree * mig_tree, 
                      std::string cut, std::vector<result> * fit_data, std::string unique, bool non_reco ){

  bound tr_bound = selections->get_bound( Form( "tr_%s", var.c_str() ) );
  bound re_bound = selections->get_bound( Form( "re_%s", var.c_str() ) );

  bool no_cut_fit = !( unique.empty() || non_reco );

  std::vector< float > reco_style = { 1, 0, 0, 0, 1, kBlue+1, 1.00, 1, 0, 0, 0};
  std::vector< float > truth_style = { 1, 0, 0, 0, 1, kRed+1, 1.00, 1, 0, 0, 0};
  std::vector< float > ratio_style = { 1, 0, 0, 0, 1, kMagenta+2, 1.00, 1, 0, 0, 0};
  std::vector< float > fit_style = { 1, 1, 1.00, 2 };   

  
  ///std::string bin_str = Form( "%.3f %s", width, re_bound.get_units().c_str() );
  std::string bin_str = re_bound.get_x_str(); 
  int rand_int = std::rand() * 10000 + 1;
  
  TH2F * mig_hist = new TH2F( Form( "mig_hist_%i", rand_int ), "mig_hist",
                                  tr_bound.get_bins(), tr_bound.get_min(), tr_bound.get_max(),
                                  re_bound.get_bins(), re_bound.get_min(), re_bound.get_max() ); 

  TCanvas * temp_canv = new TCanvas( "TEMP", "TEMP" );
  mig_tree->Draw( Form( "re_%s:tr_%s>>mig_hist_%i", var.c_str(), var.c_str(), rand_int ), cut.c_str() );
  TH1F * reco_hist  = (TH1F*) mig_hist->ProjectionY();
  TH1F * truth_hist = (TH1F*) mig_hist->ProjectionX();
  TH1F * ratio_hist = new TH1F( Form( "ratio_%s_%s_%i", var.c_str(), mass.c_str(), rand_int ), "", re_bound.get_bins(),
                               re_bound.get_min(), re_bound.get_max() );
  ratio_hist->Divide( reco_hist, truth_hist, 1.0, 1.0, "B" );
  delete temp_canv;

  TF1 * line = prep_line( tr_bound.get_min(), tr_bound.get_max() );
  TF1 * truth_fit;
  TF1 * reco_fit; 

  if ( no_cut_fit ){
    truth_fit = prep_sg( tr_bound.get_min(), tr_bound.get_max() );
    reco_fit = prep_sg( re_bound.get_min(), re_bound.get_min() );
    align_sg( reco_fit, reco_hist );
    align_sg( truth_fit, truth_hist );
  } else {
    truth_fit = prep_dg( tr_bound.get_min(), tr_bound.get_max() );
    reco_fit =  prep_dg( re_bound.get_min(), re_bound.get_min() );
    align_dg( reco_fit, reco_hist );
    align_dg( truth_fit, truth_hist );
  }

  style_func( truth_fit, fit_style );
  style_func( reco_fit, fit_style );
  style_func( line, fit_style );
  
  gStyle->SetOptStat( "oui" );

  TCanvas * canv = new TCanvas( Form( "canv_%s_%i", mass.c_str(), rand_int ), "canv", 200, 200, 2000, 2000 );
  canv->Divide( 2, 2 );     


  TPad * current_pad = (TPad*) canv->cd( 1 );
  mig_hist->Draw("COLZ");
  gPad->Modified(); gPad->Update();
  set_axis_labels( mig_hist, Form( "Truth %s", bin_str.c_str() ), Form( "Reco %s", bin_str.c_str() ) );
  hist_prep_axes( mig_hist );
  add_atlas_decorations( current_pad, true, false );
  mig_hist->GetZaxis()->SetMaxDigits(3);
  mig_hist->GetYaxis()->SetRangeUser( re_bound.get_min(), re_bound.get_max() );
  mig_hist->GetXaxis()->SetRangeUser( tr_bound.get_min(), tr_bound.get_max() );
  add_pad_title( current_pad, Form("%s Migration Matrix %s", var.c_str(), mass.c_str() ), false );
  TPaveStats * mig_stats = make_stats( mig_hist );
  mig_stats->Draw();

  current_pad = (TPad*) canv->cd( 2 );
  reco_hist->Draw("HIST");
  reco_hist->Draw("E1 SAME");
  style_hist( reco_hist, reco_style );
  hist_prep_axes( reco_hist );
  add_atlas_decorations( current_pad, true, false );
  set_axis_labels( reco_hist, Form("Reco %s/%s", var.c_str(), bin_str.c_str() ), 
                  Form("Reco Projection/%.3f [%s]", re_bound.get_bin_width( 60 ), re_bound.get_units().c_str() ) );
  add_pad_title( current_pad, Form( "Reco migration projection %s", mass.c_str() ) );
  reco_hist->Fit( reco_fit, "MQ", "", re_bound.get_min(), re_bound.get_max() );
  TPaveStats * reco_gauss_fit = make_stats( reco_hist );
  reco_gauss_fit->Draw( "SAME" );
  TLegend * reco_legend = below_logo_legend();
  reco_legend->AddEntry( reco_hist, "Reco projection","L");
  reco_legend->AddEntry( reco_fit, "Gaussian fit","L");
  reco_legend->Draw();

  current_pad = (TPad*) canv->cd( 3 );
  truth_hist->Draw("HIST");
  truth_hist->Draw("E1 SAME");
  style_hist( truth_hist, truth_style );
  set_axis_labels( truth_hist, Form("Truth %s",bin_str.c_str() ), 
                  Form("Truth Projection/%.3f [%s]", tr_bound.get_bin_width( 60 ), tr_bound.get_units().c_str() ) );
  add_atlas_decorations( current_pad, true, false );
  add_pad_title( current_pad, Form( "Truth migration projection %s", mass.c_str() ) );
  hist_prep_axes( truth_hist );
  truth_hist->Fit( truth_fit, "MQ","", tr_bound.get_min(), tr_bound.get_max() );
  TPaveStats * truth_gauss_fit = make_stats( truth_hist );
  truth_gauss_fit->Draw( "SAME" );
  TLegend * truth_legend = below_logo_legend();
  truth_legend->AddEntry( truth_hist, "Truth projection", "L" );
  truth_legend->AddEntry( truth_fit, "Gaussian fit", "L" );
  truth_legend->Draw();

  current_pad = (TPad*) canv->cd( 4 );
  ratio_hist->Draw( "HIST E1" );
  ratio_hist->SetMarkerStyle(21);
  ratio_hist->SetLineColor(1);
  style_hist( ratio_hist, ratio_style );
  set_axis_labels( ratio_hist,  Form("%s", bin_str.c_str() ), Form("truth/reco proj ratio/%s", bin_str.c_str()) );
  add_atlas_decorations( current_pad, true, false );
  add_pad_title( current_pad, Form( "%s projection ratio %s", var.c_str(), mass.c_str() ), false );
  hist_prep_axes( ratio_hist );
  TLegend * ratio_legend = below_logo_legend();
  ratio_legend->AddEntry( ratio_hist, "Reco/Truth projection", "L" );
  TPaveStats * ratio_line_fit = make_stats( ratio_hist );
  ratio_line_fit->Draw();
  if ( !no_cut_fit ){
    ratio_hist->Fit( line, "MQ", "", re_bound.get_min(), re_bound.get_max() );
    ratio_legend->AddEntry( line, "ratio fit", "L" );
    line->Draw( "SAME" );
  }
  ratio_legend->Draw();
  
  if ( non_reco ){
    mig_hist->GetXaxis()->SetRange( 0, re_bound.get_bins() + 1 );
    mig_hist->GetYaxis()->SetRange( 0, re_bound.get_bins() + 1 );
    reco_hist->GetXaxis()->SetRange( 0, re_bound.get_bins() + 1 );
    truth_hist->GetXaxis()->SetRange( 0, re_bound.get_bins() + 1 );
    //ratio_hist->GetXaxis();
  }


  std::string output_file = Form( "./Mig_%s", var.c_str() );
  if ( !mass.empty() ){ output_file += "_" +  mass; }
  if ( !unique.empty() ){ output_file += "_" + unique; }
  if ( non_reco ){ output_file += "_non_reco"; }
  output_file += ".png";
  canv->SaveAs( output_file.c_str() );

  store->add_hist( truth_hist, Form( "tr_%s_%s_%s", mass.c_str(), var.c_str(), unique.c_str() ) );
  store->add_hist( reco_hist, Form( "re_%s_%s_%s", mass.c_str(), var.c_str(), unique.c_str() ) );
  store->add_hist( ratio_hist, Form( "ratio_%s_%s_%s", mass.c_str(), var.c_str(), unique.c_str() ) );
  store->add_hist( mig_hist, Form( "mig_%s_%s_%s", mass.c_str(), var.c_str(), unique.c_str() ) );

  if ( fit_data != nullptr ){
    fit_data->push_back( result{ unique.c_str(), reco_fit->GetParameter( 1 ), reco_fit->GetParameter( 2 ) } );
  }

  delete canv;
  //delete mig_hist;

}


void create_delta_image( bound_mgr * selections, std::string var, std::string mass, TTree * mig_tree, 
                      std::string cut, std::string unique ){

  bound tr_bound = selections->get_bound( Form( "tr_%s", var.c_str() ) );
  bound d_bound = selections->get_bound( "delta10" );
  double width = d_bound.get_bin_width();
  


  std::vector< float > delta_style = { 1, 0, 0, 0, 1, 1, 1.00, 1, 0, 0, 0};
  std::vector< float > fit_style = { 1, kRed+1, 1.00, 2 };   

  TF1 * dg = prep_dg( d_bound.get_min(), d_bound.get_max() );
  style_func( dg, fit_style );


  std::string bin_str = Form( "%.3f %s", width, tr_bound.get_units().c_str() );
  int rand_int = std::rand() * 10000 + 1;
  
  TH1F * delta_hist = new TH1F( Form( "delta_hist_%i", rand_int ), "delta_hist",
                            d_bound.get_bins(), d_bound.get_min(), d_bound.get_max() );

  TCanvas * temp_canv = new TCanvas( "TEMP", "TEMP" );
  mig_tree->Draw( Form( "(tr_%s-re_%s)>>delta_hist_%i", var.c_str(), var.c_str(), rand_int ), cut.c_str() );
  delete temp_canv;

  TCanvas * canv = new TCanvas( Form( "canv_%s_%i", mass.c_str(), rand_int ), "canv", 200, 200, 2000, 2000 );
  canv->Divide( 1 );     

  TPad * current_pad = (TPad*) canv->cd( 1 );
  delta_hist->Draw("COLZ");
  set_axis_labels( delta_hist, Form( "#Delta, reco-truth /%s", bin_str.c_str() ), Form( "reco entries/%s", bin_str.c_str() ) );
  hist_prep_axes( delta_hist );
  style_hist( delta_hist, delta_style );
  align_dg( dg, delta_hist, true );
  delta_hist->Fit( dg, "MQ", "", d_bound.get_min(), d_bound.get_max() );
  add_atlas_decorations( current_pad, true, false );
  add_pad_title( current_pad, Form("%s Delta %s", var.c_str(), mass.c_str() ), true );
  TPaveStats *  delta_stats = make_stats( delta_hist );
  delta_stats->Draw();

  if ( unique.empty() ){
    canv->SaveAs( Form( "./Delta_%s_%s.png", mass.c_str(), var.c_str() ));
  } else {
    canv->SaveAs( Form( "./Delta_%s_%s_%s.png", mass.c_str(), var.c_str(), unique.c_str() ));
  }

  delete canv;
  delete delta_hist;

}


void fit_plots( bound_mgr * selections, std::vector<result> * results, std::string mass ){

  prep_style();

  float points = results->size();
  bound qta_bound = selections->get_bound( "qTA" );
  double min = qta_bound.get_min();
  double max = qta_bound.get_max();
  double width = ( max - min )/( (float) points );

  std::string ltx = qta_bound.get_ltx();
  std::string bin_str = Form( "%s/%.5f %s", qta_bound.get_ltx().c_str(),
                             width, qta_bound.get_units().c_str() );

  std::vector< float > sigma_style = { 1, 0, 0, 0, 1, kOrange+7, 1.00, 1, 0, 0, 0};
  std::vector< float > mean_style = { 1, 0, 0, 0, 1, kViolet+5, 1.00, 1, 0, 0, 0};

  
  TF1 * line = prep_line( min, max );
  std::vector<float> float_style = { 1.0, 1, 1.00, 1 };
  style_func( line, float_style );

  TH1F * sigma_hist = new TH1F( Form( "sigma_%s", mass.c_str() ), "", points, min, max );
  TH1F * mean_hist = new TH1F( Form( "mean_%s", mass.c_str() ), "", points, min, max );
  for ( size_t idx = 0; idx < points; idx++ ){
    sigma_hist->SetBinContent( idx + 1, results->at(idx).sigma );
    mean_hist->SetBinContent( idx + 1, results->at(idx).x );
  }

  TCanvas * canv = new TCanvas( Form( "canv_%s", mass.c_str() ), "", 200, 200, 2000, 1000 );
  canv->Divide( 2, 1 );

  TPad * current_pad = (TPad *) canv->cd( 1 );
  sigma_hist->Draw( "HIST" );
  set_axis_labels( sigma_hist, Form( "%s", bin_str.c_str() ), Form( "#sigma reco projection") );
  hist_prep_axes( sigma_hist, true );
  style_hist( sigma_hist, sigma_style );
  add_atlas_decorations( current_pad, true, false );
  add_pad_title( current_pad, Form("%s #sigma %s", ltx.c_str(), mass.c_str() ), false );
  TLegend * sigma_leg = below_logo_legend(); 
  sigma_leg->AddEntry( sigma_hist, "#sigma" );
  sigma_leg->Draw();


  current_pad = (TPad *) canv->cd( 2 );
  mean_hist->Draw( "HIST" );
  mean_hist->Fit( line, "MQ", "", min, max );
  line->Draw( "SAME" );
  set_axis_labels( mean_hist, Form( "%s", bin_str.c_str() ), Form( "#bar{x} reco projection") );
  hist_prep_axes( mean_hist );
  style_hist( mean_hist, mean_style );
  add_atlas_decorations( current_pad, true, false );
  add_pad_title( current_pad, Form("%s #bar{x} %s", ltx.c_str(), mass.c_str() ), false );
  TPaveStats * stats = make_stats( mean_hist );
  stats->Draw();
  TLegend * mean_leg = below_logo_legend(); 
  mean_leg->AddEntry( mean_hist, "#bar{x}" );
  mean_leg->Draw();

  canv->SaveAs( Form( "./reco_fits_%s.png", mass.c_str()  ) );

}



void mig(){

  prep_style();
  gStyle->SetOptStat( "oui" );
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();
  
  TFile * mig_file = new TFile("~/analysis/analysisFork/445/newTrees/run/gen_base/migration/sign_base_migration.root", "READ" );
  TTree * mig_tree = (TTree *) mig_file->Get( "sign_migration" );
  
  bound_mgr * selections = new bound_mgr() ;
  selections->load_bound_mgr( "/home/atlas/dhagan/libs/configs/selection_bounds.txt" );

  std::vector< std::string > mig_vars = { "qta" }; // "qtb", "lambda" };
  std::vector< std::string > mass_vars = { "Q0" }; // "Q3", "Q4", "Q5", "Q12" };
                                           //"T0", "T3", "T4", "T5", "T12" };
  
  hist_store * store = new hist_store( Form("./migration_hist_store.root" ) );

  std::vector<result> * fit_results = new std::vector<result>;

  int splits = 60;

  for ( std::string var : mig_vars ){
    
    std::string base_cut;
    std::vector< std::string > cut_vec = {}, unique_vec = {};

    bound re_bound = selections->get_bound( Form( "re_%s", var.c_str() ) );
    bound tr_bound = selections->get_bound( Form( "tr_%s", var.c_str() ) );

    for ( std::string mass : mass_vars ){

      base_cut = "";
      unique_vec.push_back( "" );
      
      bound mass_bound = selections->get_bound( mass );

      // regular migration and delta
      base_cut +=  Form( "(%s)", mass_bound.get_cut().c_str() );
      base_cut += Form( "&&(%s)", re_bound.get_cut().c_str() );
      base_cut += "&&(truth_pass>=1.0)"; 
      base_cut += "&&(reco_pass>=1.0)"; 

      if ( var.find( "qta" ) != std::string::npos ){  
        cut_vec.push_back( base_cut + "&&(" + tr_bound.get_cut() + ")" );
        std::vector<std::string> cut_series = tr_bound.get_cut_series( splits );
        std::vector<std::string> uniques = tr_bound.get_series_names(  splits );
        for ( int idx = 0; idx < (int) cut_series.size(); idx++ ){
          cut_vec.push_back( base_cut + "&&(" + cut_series.at( idx ) + ")" ); 
          unique_vec.push_back( uniques.at( idx ) ); 
        } 
      } else {
        base_cut += "&&(" + tr_bound.get_cut() + ")";
        cut_vec.push_back( base_cut );
      }
      for ( int cut_idx = 0; cut_idx < (int) cut_vec.size(); cut_idx++ ){
        create_mig_image( selections, store, var, mass, mig_tree, cut_vec.at( cut_idx ), (cut_idx == 0) ? nullptr : fit_results, unique_vec.at( cut_idx ) );
        //std::cout << cut_idx << std::endl;
      }
      create_delta_image( selections, var, mass, mig_tree, cut_vec.at( 0 ), unique_vec.at( 0 ) );
      fit_plots( selections, fit_results, mass );
      base_cut.clear();
      cut_vec.clear();
      unique_vec.clear();

    }

    //base_cut += "(" + selections->get_cut( Form( "%s", mass.c_str() ) ) + ")";
    base_cut += Form( "(%s)", tr_bound.get_cut().c_str() );
    base_cut += "&&(truth_pass>=1.0)"; 
    create_mig_image( selections, store, var, "" , mig_tree, { base_cut }, nullptr, "", true );

  }

}
