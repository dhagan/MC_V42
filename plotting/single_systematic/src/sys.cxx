#include <sys.hxx>

void systematic( std::string & base_path, std::string & base_efficiency_path, std::string & systematic_path, 
          std::string & systematic_efficiency_path, std::string & analysis_var, 
          std::string & spectator_var, std::string & type, bound_mgr * selections,
          std::string & unique, bool normalise ){

  // Set the canvas/hist styles
  prep_style();
  gStyle->SetOptFit( 11111111 );


  // base files
  TFile * base_extraction_file    = new TFile( base_path.c_str(),    "READ");
  TFile * base_efficiency_file    = new TFile( base_efficiency_path.c_str(),   "READ");

  // sys files
  TFile * systematic_extraction_file    = new TFile( systematic_path.c_str(),    "READ");
  TFile * systematic_efficiency_file    = new TFile( systematic_efficiency_path.c_str(),   "READ");

  std::vector<std::string> spectator_variables;
  std::string systematic_spectator, baseline_spectator;
  split_strings( spectator_variables, spectator_var, ":" );
  if ( spectator_variables.size() < 2 ){
    baseline_spectator = spectator_variables.at( 0 );
    systematic_spectator = baseline_spectator;
  } else {
    baseline_spectator = spectator_variables.at( 0 );
    systematic_spectator = spectator_variables.at( 1 );
  }


  // prep bounds
  //fit_mgr * fit_manager = new fit_mgr();
  //fit_manager->load_fit_mgr( "/home/atlas/dhagan/libs/configs/fit_spec.txt" );

  bound analysis_bound = selections->get_bound( analysis_var );
  bound baseline_spectator_bound = selections->get_bound( baseline_spectator );
  bound systematic_spectator_bound = selections->get_bound( systematic_spectator );
  int analysis_bins = analysis_bound.get_bins();
  float analysis_min = analysis_bound.get_min();
  float analysis_max = analysis_bound.get_max();
  float analysis_width = analysis_bound.get_bin_width();


  std::vector< int > mass_bins = { 0, 3, 4, 5, 12 };

  std::vector< float > systematic_error_style   = { 1, 1, 1.0, 1.00,  1, 1, 1.00, 1,  0, 0.00, 0.00 };
  std::vector< float > statistical_error_style  = { 0, 0, 1.0, 0.00,  1, 1, 1.00, 0,  1, 0.30, 1001 };
  std::vector< float > total_error_style        = { 0, 0, 1.0, 0.00,  1, 1, 1.00, 3,  0, 0.00, 0.00 };


  std::vector< float > base_style               = { 1, 1, 1.0, 1.00,  1, 1, 1.00, 1,  0, 0.00, 0.00 };
  std::vector< float > systematic_style         = { 0, 0, 1.0, 1.00,  2, 1, 1.00, 1,  1, 0.80, 0.00 };
  std::vector< float > total_style              = { 1, 1, 1.0, 1.00,  1, 1, 1.00, 2,  0, 0.00, 0.00 };
  std::vector< float > eb_base_style = { 1, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 3.0, 0, 0, 0 };
  std::vector< float > errorbar_style = { 1, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0, 0, 0 };
  std::vector< float > eb_sys_style =  { 1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kRed+1, 0.6, 1 };

  



  // mass loop
  for ( int & mass_idx : mass_bins ){

    std::string mass = Form( "Q%i", mass_idx );


    TH1F * base_extracted = new TH1F( Form( "base_extracted_Q%i", mass_idx ), "", 
                                      analysis_bins, analysis_min, analysis_max );
    TH1F * systematic_extracted = new TH1F( Form( "systematic_extracted_Q%i", mass_idx ), "", 
                                                   analysis_bins, analysis_min, analysis_max );
    TH1F * base_corrected = new TH1F( Form( "base_corrected_Q%i", mass_idx ), "", 
                                      analysis_bins, analysis_min, analysis_max );
    TH1F * systematic_corrected = new TH1F( Form( "systematic_corrected_Q%i", mass_idx ), "", 
                                                   analysis_bins, analysis_min, analysis_max );

    TH1F * base_efficiency = (TH1F *) base_efficiency_file->Get( Form( "eff_%s_Q%i", analysis_bound.get_var().c_str(), mass_idx ) );  
    TH1F * systematic_efficiency = (TH1F *) systematic_efficiency_file->Get( Form( "eff_%s_Q%i", analysis_bound.get_var().c_str(), mass_idx ) );
    
    std::string baseline_differential_bin_name = Form( "%s_99_pos_%s_%s", baseline_spectator.c_str(), 
                                                      mass.c_str() , analysis_var.c_str() );
    std::string baseline_bin_hist_name = type + "_" + baseline_differential_bin_name;
    std::string baseline_err_hist_name = "errh_" + baseline_differential_bin_name;

    std::string systematic_differential_bin_name = Form( "%s_99_pos_%s_%s", systematic_spectator.c_str(), 
                                                        mass.c_str() , analysis_var.c_str() );
    std::string systematic_bin_hist_name = type + "_" + systematic_differential_bin_name;
    std::string systematic_err_hist_name = "errh_" + systematic_differential_bin_name;

    int type_num = type_to_num( type );
    rc_hist( baseline_bin_hist_name, baseline_err_hist_name, base_extraction_file, base_extracted, type_num );
    rc_hist( systematic_bin_hist_name, systematic_err_hist_name, systematic_extraction_file, systematic_extracted, type_num );


    base_corrected->Divide( base_extracted, base_efficiency );
    systematic_corrected->Divide( systematic_extracted, systematic_efficiency );


    double base_integral = base_corrected->Integral();
    double systematic_integral = systematic_corrected->Integral();

    std::cout << "Baseline integral:   " << base_integral << std::endl;
    std::cout << "Systematic integral: " << systematic_integral << std::endl;
    std::cout << "Difference:          " << abs( base_integral - systematic_integral ) << std::endl;
    std::cout << "Relative error:      " << abs( base_integral - systematic_integral )/systematic_integral << std::endl;

    if ( normalise ){
      base_corrected->Scale( 1.0/base_integral );
      systematic_corrected->Scale( 1.0/systematic_integral );
    }

    TH1F * systematic_error_on_base_only = single_sys_to_error( base_corrected, systematic_corrected );
    TH1F * absolute_statistical_error = errorbar_to_hist( base_corrected, true );
    TH1F * relative_statistical_error = errorbar_to_hist( base_corrected, false );
    TH1F * absolute_systematic_error = sys_to_error_hist( base_corrected, systematic_corrected, true );
    TH1F * relative_systematic_error = sys_to_error_hist( base_corrected, systematic_corrected, false );

    TH1F * statistical_and_systematic = quadrature_error_combination( base_corrected, { systematic_error_on_base_only }, false );
    TH1F * absolute_total_error = errorbar_to_hist( statistical_and_systematic, true );
    TH1F * relative_total_error = errorbar_to_hist( statistical_and_systematic, false );

    TH1F * total_hist = quadrature_error_combination( base_corrected, { systematic_error_on_base_only  }, false );

    abs_hist( absolute_statistical_error );
    abs_hist( absolute_systematic_error );
    abs_hist( relative_statistical_error );
    abs_hist( relative_systematic_error );

    style_hist( base_corrected, base_style );
    style_hist( systematic_corrected, systematic_style );
    style_hist( total_hist, total_style );

    style_hist( absolute_statistical_error, statistical_error_style );
    style_hist( absolute_systematic_error, systematic_error_style );
    style_hist( absolute_total_error, total_error_style );

    style_hist( relative_statistical_error, statistical_error_style );
    style_hist( relative_systematic_error, systematic_error_style );
    style_hist( relative_total_error, total_error_style );

    // ready lines and diffs
  
    TH1F * eb_base_hist = (TH1F *) base_corrected->Clone( "eb_base_hist" ); 
    TH1F * eb_systematic_hist = (TH1F *) systematic_corrected->Clone( "eb_systematic_hist" ); 

    TF1 * unit_line = new TF1( "unit_line", "1", analysis_min, analysis_max );
    TF1 * zero_line = new TF1( "zero_line", "1", analysis_min, analysis_max );
    TH1F * unit_hist = new TH1F( "unit", "",  analysis_bins , analysis_min, analysis_max );
    for ( int idx = 1; idx <= unit_hist->GetNbinsX(); idx++ ){ unit_hist->SetBinContent( idx, 1.0); }
    TH1F * rel_diff = sys_to_error_hist( base_corrected, systematic_corrected, false );
    TH1F * abs_diff = sys_to_error_hist( base_corrected, systematic_corrected, true );
    TH1F * errorbar_hist = (TH1F *) systematic_error_on_base_only->Clone( "sys_as_errorrbar" );

    // diff to grapherr start
    double * sys_x = new double[ analysis_bins ];
    double * sys_y = new double[ analysis_bins ];
    double * err_x_lower = new double[ analysis_bins ];
    double * err_x_upper = new double[ analysis_bins ];
    double * rel_err_y_lower = new double[ analysis_bins ];
    double * rel_err_y_upper = new double[ analysis_bins ];
    double * abs_err_y_lower = new double[ analysis_bins ];
    double * abs_err_y_upper = new double[ analysis_bins ];

    for ( int bin = 0; bin <  analysis_bins ; bin++ ){

      sys_x[bin] = ( analysis_min + ( analysis_width/2.0 ) ) + ( analysis_width * ( (double) bin ) );
      sys_y[bin] = 1.0;
      err_x_lower[bin] = 0;
      err_x_upper[bin] = 0;
      double rel_err = rel_diff->GetBinContent( bin + 1 );
      double abs_err = abs_diff->GetBinContent( bin + 1 );
      if ( rel_err > 0 ){
        rel_err_y_lower[bin] = 0.0;
        rel_err_y_upper[bin] = abs( rel_err );
        abs_err_y_lower[bin] = 0.0;
        abs_err_y_upper[bin] = abs( abs_err );
      } else {
        rel_err_y_lower[bin] = abs( rel_err );
        rel_err_y_upper[bin] = 0.0;
        abs_err_y_lower[bin] = abs( abs_err );
        abs_err_y_upper[bin] = 0.0;
      }
    }

    rel_diff->Add( rel_diff, unit_hist );

    // create symm error graphs
    TGraphAsymmErrors * rel_err_graph = new TGraphAsymmErrors(  analysis_bins , sys_x, sys_y, 
                                         err_x_lower, err_x_upper,
                                            rel_err_y_lower, rel_err_y_upper);
    rel_err_graph->SetFillStyle( 1 );
    rel_err_graph->SetFillColorAlpha( kBlue+1, 0.6 );
    rel_err_graph->SetLineWidth( 0 );

    TGraphAsymmErrors * abs_err_graph = new TGraphAsymmErrors(  analysis_bins , sys_x, sys_y, 
                                            err_x_lower, err_x_upper,
                                            abs_err_y_lower, abs_err_y_upper);
    abs_err_graph->SetFillStyle( 1 );
    abs_err_graph->SetFillColorAlpha( kBlue+1, 0.6 );
    abs_err_graph->SetLineWidth( 0 );


    style_hist( eb_base_hist, eb_base_style );
    style_hist( eb_systematic_hist, eb_sys_style );
    style_hist( errorbar_hist, errorbar_style );

    TH1F * sg_fit = (TH1F *) errorbar_hist->Clone( "sg_hist" );
    TH1F * dg_fit = (TH1F *) errorbar_hist->Clone( "dg_hist" );
    TF1 * sg = prep_sg( analysis_min, analysis_max );
    TF1 * dg = prep_dg( analysis_min, analysis_max );
    align_sg( sg, sg_fit );
    align_dg( dg, dg_fit );

    TCanvas * sys_canvas = new TCanvas( Form( "canv_%i", mass_idx ),"canv", 100, 100, 2000, 2000 );
    sys_canvas->Divide( 4, 2 );
    

    // regular error bars of grouped systematics
    TPad * active_pad = (TPad *) sys_canvas->cd( 1 );
    base_corrected->Draw( "HIST E1" );
    systematic_corrected->Draw( "HIST SAME" );
    hist_prep_axes( base_corrected );
    add_pad_title( active_pad, Form("%s distribution of baseline and systematic - Q%i", 
                                    analysis_bound.get_ltx().c_str(), mass_idx ) );
    set_axis_labels( base_corrected, Form( "%s (J/#psi+#gamma) [GeV]", analysis_bound.get_ltx().c_str() ),
                    "Yield/2GeV [counts]");
    add_atlas_decorations( active_pad, true, false );
    TLegend * dist_pad_legend = create_atlas_legend();
    dist_pad_legend->AddEntry( base_corrected, "Nominal", "PL" );
    dist_pad_legend->AddEntry( systematic_corrected, unique.c_str(), "L" );
    dist_pad_legend->Draw();

    active_pad = (TPad *) sys_canvas->cd( 2 );
    total_hist->Draw( "HIST E1" );
    hist_prep_axes( total_hist, true );
    base_corrected->Draw( "HIST E1 SAME" );
    add_pad_title( active_pad, Form("%s Combined error - Q%i", analysis_bound.get_ltx().c_str(), mass_idx ) );
    set_axis_labels( total_hist, Form( "%s (J/#psi+#gamma) [GeV]", analysis_bound.get_ltx().c_str() ),
                    "Yield/2GeV [counts]");
    add_atlas_decorations( active_pad, true, false );
    TLegend * total_pad_legend = create_atlas_legend();
    total_pad_legend->AddEntry( total_hist, "Total", "PL" );
    total_pad_legend->AddEntry( base_corrected, "Statistical", "L" );
    total_pad_legend->Draw();


    // absolute error bars of grouped systematics
    active_pad = (TPad *) sys_canvas->cd( 3 );
    absolute_total_error->Draw( "HIST" );
    hist_prep_axes( absolute_total_error, true );
    absolute_systematic_error->Draw( "HIST SAME" );
    absolute_statistical_error->Draw( "HIST SAME" );
    add_pad_title( active_pad, Form("%s Absolute error - Q%i", analysis_bound.get_ltx().c_str(), mass_idx ) );
    set_axis_labels( total_hist, Form( "%s (J/#psi+#gamma) [GeV]", analysis_bound.get_ltx().c_str() ),
                                        "Absolute error/2GeV");
    add_atlas_decorations( active_pad, true, false );
    TLegend * absolute_error_legend = create_atlas_legend();
    absolute_error_legend->AddEntry( absolute_statistical_error, "Statistical", "F" );
    absolute_error_legend->AddEntry( absolute_systematic_error, "Systematic", "L" );
    absolute_error_legend->AddEntry( absolute_total_error, "Total", "L" );
    absolute_error_legend->Draw();



    active_pad = (TPad *) sys_canvas->cd( 4 );
    relative_total_error->Draw( "HIST" );
    hist_prep_axes( relative_total_error );
    relative_statistical_error->Draw( "HIST SAME" );
    relative_systematic_error->Draw( "HIST SAME" );
    add_pad_title( active_pad, Form("%s Relative error - Q%i", analysis_bound.get_ltx().c_str(), mass_idx ), false );
    set_axis_labels( total_hist, Form( "%s (J/#psi+#gamma) [GeV]", analysis_bound.get_ltx().c_str() ),
                                        "Relative error/2GeV");
    add_atlas_decorations( active_pad, true, false );
    TLegend * relative_error_legend = create_atlas_legend();
    relative_error_legend->AddEntry( relative_statistical_error, "Statistical", "F" );
    relative_error_legend->AddEntry( relative_systematic_error, "Systematic", "L" );
    relative_error_legend->AddEntry( relative_total_error, "Total", "L" );
    relative_error_legend->Draw();



    // fitting
    active_pad = (TPad *) sys_canvas->cd( 5 );
    sg_fit->Draw( "HIST E1" );
    hist_prep_axes( sg_fit, true );
    add_pad_title( active_pad, Form( "%s %s", analysis_var.c_str(), unique.c_str() ), true );
    add_atlas_decorations( active_pad, true, false );
    set_axis_labels( errorbar_hist, analysis_bound.get_x_str(), Form( "Extracted Yield %s", analysis_bound.get_y_str().c_str() ) );
    TPaveStats * sg_stats = make_stats( sg_fit, false, false );;
    sg_stats->Draw( "SAME" );
    TLegend * sg_legend = below_logo_legend();
    sg_legend->AddEntry( sg_fit, "#splitline{Nominal +}{sys error}" );
    sg_legend->AddEntry( sg, "Single Gaussain" );
    sg_legend->Draw( "SAME" );

    active_pad = (TPad *) sys_canvas->cd( 6 );
    dg_fit->Draw( "HIST E1" );
    hist_prep_axes( dg_fit, true );
    add_pad_title( active_pad, Form( "%s %s", analysis_var.c_str(), unique.c_str() ), true );
    add_atlas_decorations( active_pad, true, false );
    set_axis_labels( errorbar_hist, analysis_bound.get_x_str(), Form( "Extracted Yield %s", analysis_bound.get_y_str().c_str() ) );
    TPaveStats * dg_stats = make_stats( dg_fit, false, false );;
    dg_stats->Draw( "SAME" );
    TLegend * dg_legend = below_logo_legend();
    dg_legend->AddEntry( dg_fit, "#splitline{Nominal +}{sys error}" );
    dg_legend->AddEntry( dg, "Double Gaussain" );
    dg_legend->Draw( "SAME" );


    active_pad = (TPad *) sys_canvas->cd( 7 );
    abs_diff->Draw( "HIST P" );
    abs_diff->SetMarkerStyle( 21 );
    abs_err_graph->Draw( "3 SAME" );
    zero_line->Draw( "SAME L" );
    hist_prep_axes( abs_diff, false );//, true, 0.0 );
    add_pad_title( active_pad, Form( "Absolute Error %s %s", analysis_var.c_str(), unique.c_str() ), true );
    add_atlas_decorations( active_pad, true, false );
    set_axis_labels( abs_diff, analysis_bound.get_x_str(), Form( "Absolute Error %s", analysis_bound.get_y_str().c_str() ) );
    TLegend * abs_legend = create_atlas_legend();
    abs_legend->AddEntry( abs_err_graph, "Absolute Error" );
    abs_legend->AddEntry( zero_line, "Nominal" );
    abs_legend->Draw( "SAME" );



    active_pad = (TPad *) sys_canvas->cd( 8 );
    rel_diff->Draw( "HIST P" );
    rel_diff->SetMarkerStyle( 21 );
    rel_err_graph->Draw( "3 SAME ");
    unit_line->Draw( "SAME L" );
    hist_prep_axes( rel_diff, true );// true, 1.0 );
    add_pad_title( active_pad, Form( "Relative Error %s %s", analysis_var.c_str(), unique.c_str() ), false );
    add_atlas_decorations( active_pad, true, false );
    set_axis_labels( rel_diff, analysis_bound.get_x_str(), Form( "Relative Error %s", analysis_bound.get_y_str().c_str() ) );
    //if ( unique.find( "dz02" ) != std::string::npos ){ rel_diff->GetYaxis()->SetRangeUser( 0.5, 1.5 ); }
    TLegend * rel_legend = create_atlas_legend();
    rel_legend->AddEntry( rel_err_graph, "Relative Error" );
    rel_legend->AddEntry( unit_line, "Nominal" );
    rel_legend->Draw( "SAME" );

        

    std::string canvas_name = Form( "systematic_%s_%s", unique.c_str(), mass.c_str() );
    if ( normalise ){ canvas_name += "_norm"; }
    canvas_name += ".png";
    sys_canvas->SaveAs( canvas_name.c_str() );
    delete sys_canvas;
    delete unit_hist;


  }
}
