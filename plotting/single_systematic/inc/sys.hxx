
#include <anna.hxx>


void systematic( std::string & base_path, std::string & base_efficiency_path, std::string & systematic_path, 
          std::string & systematic_efficiency_path, std::string & analysis_var, 
          std::string & spectator_var, std::string & type, bound_mgr * selections,
          std::string & unique, bool normalise );

