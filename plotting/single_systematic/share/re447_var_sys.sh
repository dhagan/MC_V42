#!/bin/bash

executable=${ANA_IP}/plotting/single_systematic/build/single_systematic
selections=${LIB_PATH}/share/selection_bounds.txt

base_unique=re447
base_input="${OUT_PATH}/hf/${base_unique}/eval/hf_fit_${base_unique}_A-qtA_S-BDT.root"
base_efficiency="${OUT_PATH}/trees/gen_${base_unique}/sign_efficiencies_${base_unique}.root"
sys_efficiency="${base_efficiency}"

vars=( aaf afa faa extd noqt2Disc noL ffDPDY aaDPDY faDPDY )

for var in ${vars[@]}
do

	sys_unique=re447_${var}
	sys_input="${OUT_PATH}/hf/${sys_unique}/eval/hf_fit_${sys_unique}_A-qtA_S-BDT.root"
	log=${LOG_PATH}/extract/${sys_unique}.txt

	mkdir -p ${OUT_PATH}/single_systematic/${sys_unique}
	
	pushd ${OUT_PATH}/single_systematic/${sys_unique} >> /dev/null
	
	touch ${log}
	${executable} -b "${base_input}" -e "${base_efficiency}" -c "${sys_input}" -f "${sys_efficiency}" -v ${selections} -u ${sys_unique} -a "qtA" -s "BDT" -t "sign" -f 2>&1 | tee ${log}
	
	popd >> /dev/null




done
