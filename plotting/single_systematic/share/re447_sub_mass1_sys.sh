executable=${ANA_IP}/plotting/single_systematic/build/single_systematic
selections=${LIB_PATH}/share/selection_bounds.txt
unique=re447_sub_mass1_sys
log=${LOG_PATH}/extract/${unique}.txt
base_unique=base
sys_unique=re447_sub_mass1_sys
base_input="${OUT_PATH}/hf/${base_unique}/eval/hf_fit_${base_unique}_A-qtA_S-BDT.root"
base_efficiency="${OUT_PATH}/trees/gen_${base_unique}/sign_efficiencies_${base_unique}.root"
sys_input="${OUT_PATH}/hf/${sys_unique}/eval/hf_fit_${sys_unique}_A-qtA_S-BDT.root"
sys_efficiency="${OUT_PATH}/trees/gen_${sys_unique}/sign_efficiencies_${sys_unique}.root"

mkdir -p ${OUT_PATH}/single_systematic/${unique}

pushd ${OUT_PATH}/single_systematic/${unique} >> /dev/null

touch ${log}
${executable} -b "${base_input}" -e "${base_efficiency}" -c "${sys_input}" -f "${sys_efficiency}" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -t "sign" -f 2>&1 | tee ${log}

popd >> /dev/null
