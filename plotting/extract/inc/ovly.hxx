#ifndef ovly_hxx
#define ovly_hxx

#include <anna.hxx>

void ovly( std::string & input, std::string & abin_var, std::string & spec_var,
           bound_mgr * selections, std::string unique, bool hf_step, bool t_ranges, 
           bool pos_mode );

#endif
