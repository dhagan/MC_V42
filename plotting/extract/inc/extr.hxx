#ifndef extr_hxx
#define extr_hxx

#include <anna.hxx>

//void extr( std::string & input, std::string & eff_path, std::string & abin_var, std::string & spec_var,
//           std::string & type, bound_mgr * selections, std::string unique );

void extr( std::string & input, std::string & eff_path, std::string & abin_var, std::string & spec_var,
           std::string & type, bound_mgr * selections, std::string unique, bool hf_step, bool t_ranges, bool pos_mode );


#endif
