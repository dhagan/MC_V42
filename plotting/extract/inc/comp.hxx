#ifndef comp_hxx
#define comp_hxx

#include <anna.hxx>

void comp( std::string & input_files, std::string & analysis_var, std::string & spectator_var,
            bound_mgr * selections, std::string & unique, bool histfactory_step, bool positive_mode );

#endif
