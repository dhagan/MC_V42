#include <extr.hxx>
void extr( std::string & input, std::string & eff_path, std::string & abin_var, std::string & spec_var,
           std::string & type, bound_mgr * selections, std::string unique, bool hf_step, bool t_ranges, bool pos_mode ){

  prep_style();


  std::vector< std::string > mass_bins = { "Q0", "Q3", "Q4", "Q5", "Q12" };
  if ( t_ranges ){
    mass_bins.insert( mass_bins.end(), {"T0", "T3", "T4", "T5", "T12" } );
  }
  TFile * integral_file = new TFile( input.c_str(),   "READ");
  std::cout << input << std::endl;


  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int ana_bins      = analysis_bound.get_bins();
  float ana_min     = analysis_bound.get_min(); 
  float ana_max     = analysis_bound.get_max(); 

  std::string pos = ( pos_mode ) ? "_99_pos_" : "_99_npos_";
  std::vector< float > eff_style =  {  21, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,      0.0, 0.0 };
  std::vector< float > extr_style = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kRed+1,   0.5, 1001 };
  std::vector< float > corr_style = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kBlue+1,  0.5, 1001 };

  for ( std::string & mass : mass_bins ){

    gStyle->SetOptStat( "miner" );
    TCanvas * extr_canv  = new TCanvas( Form( "extr_canv_%s", mass.c_str() ), "", 100, 100, 1000, 1000 );
    extr_canv->Divide( 1, 1 );

    TH1F * extracted = new TH1F( Form( "extracted_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max);
    std::string ana_str = Form( "%s%s%s_%s-", spec_var.c_str(), pos.c_str(), mass.c_str() , abin_var.c_str() );
    std::cout << ana_str << std::endl;
    recombine_spectator( integral_file, ana_bins, ana_str, type, hf_step, extracted );
    style_hist( extracted,  extr_style );

    TPad * current_pad = (TPad *) extr_canv->cd(1);
    extracted->Draw( "HIST" );
    hist_prep_axes( extracted, true );
    set_axis_labels( extracted, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( current_pad, Form( "Extracted %s %s %s", type.c_str(), abin_var.c_str(), mass.c_str() ) );
    TPaveStats * extracted_stats = make_stats( extracted );
    extracted_stats->Draw();

    extr_canv->SaveAs( Form("./extract_%s_%s_A-%s_%s.png", type.c_str(), mass.c_str(), abin_var.c_str(), unique.c_str()) );
    delete extr_canv;

    if ( type.find( "sign" ) != std::string::npos ){

      gStyle->SetOptStat( 00000000 );
      TFile * efficiency_file = new TFile( eff_path.c_str(),   "READ");
      TCanvas * corr_canv  = new TCanvas( Form( "canv_%s", mass.c_str() ), "", 100, 100, 2000, 1000 );
      corr_canv->Divide( 2, 1 );

      TH1F * efficiency   = (TH1F *) efficiency_file->Get( Form( "eff_%s_%s",  abin_var.c_str(), mass.c_str() ) );
      TH1F * corrected    = new TH1F( Form( "corrected_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max);
      style_hist( efficiency, eff_style );
      style_hist( corrected,  corr_style );
      corrected->Divide( extracted, efficiency );
      
      current_pad = (TPad *) corr_canv->cd(1);
      efficiency->Draw( "E1" );
      hist_prep_axes( efficiency, true );
      set_axis_labels( efficiency, analysis_bound.get_x_str(), Form( "Efficiency%s", analysis_bound.get_y_str().c_str() ) );
      add_pad_title( current_pad, Form( "Efficiency, %s %s", abin_var.c_str(), mass.c_str() ) );
      add_atlas_decorations( current_pad, true, false );
      TLegend * eff_legend = create_atlas_legend();
      eff_legend->AddEntry( efficiency, "efficiency", "P" );
      eff_legend->Draw();

      current_pad = (TPad *) corr_canv->cd(2);
      corrected->Draw( "HIST" );
      extracted->Draw( "HIST SAMES" );
      hist_prep_axes( corrected, true );
      set_axis_labels( corrected, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
      add_pad_title( current_pad, Form( "Corrected %s %s", abin_var.c_str(), mass.c_str() ) );
      add_atlas_decorations( current_pad, true, false );
      TLegend * corr_legend = create_atlas_legend();
      corr_legend->AddEntry( corrected, "Extracted", "F" );
      corr_legend->AddEntry( extracted, "Corrected", "F" );
      corr_legend->Draw();

      corr_canv->SaveAs( Form("./extract_correct_%s_%s_A-%s_%s.png", type.c_str(), mass.c_str(), abin_var.c_str(), unique.c_str()) );
      delete corr_canv;

    }

    
  }


}
