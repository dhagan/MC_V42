#include <comp.hxx>

void comp ( std::string & input_files, std::string & analysis_var, std::string & spectator_var,
            bound_mgr * selections, std::string & unique, bool histfactory_step, bool positive_mode ){

  prep_style();

  std::cout << histfactory_step << std::endl;

  std::vector< std::string > mass_bins = { "Q0", "Q3", "Q4", "Q5", "Q12" };

  std::vector<std::string> filename_vec;
  split_strings( filename_vec, input_files, ":" );

  TFile * nominal_file = new TFile( filename_vec.at(0).c_str(), "READ" );
  TFile * altered_file = new TFile( filename_vec.at(1).c_str(), "READ" );

  bound analysis_bound = selections->get_bound( analysis_var );
  bound spectator_bound = selections->get_bound( spectator_var );


  std::string pos = ( positive_mode ) ? "pos" : "npos";
  std::vector< float > nominal_style = { 1.0, 0.0, 0.0, 0.0, 1.0, kRed+1, 1.0, 1.0, 0.0, 0.0, 0 };
  std::vector< float > altered_style = { 1.0, 0.0, 0.0, 0.0, 2.0, kBlue+1, 1.0, 1.0, 0.0, 0.0, 0 };

  for ( std::string & mass : mass_bins ){

    TCanvas * subtracted_canvas = new TCanvas( Form( "%s_subtracted_canvas", mass.c_str() ), "", 200, 200, 10000, 6000 ); 
    subtracted_canvas->Divide( 5, 3 );


    std::vector<TH1F *> hists;

    for ( int bin = 1; bin <= 15; bin ++ ){

      gStyle->SetOptStat( 11111111 );

      std::string hist_string = Form( "BDT_99_%s_%s_%s-%i", pos.c_str(), mass.c_str(), analysis_var.c_str(), bin );
      TH1F * nominal_histogram = (TH1F *) nominal_file->Get( hist_string.c_str() );
      TH1F * altered_histogram = (TH1F *) altered_file->Get( hist_string.c_str() );

      style_hist( nominal_histogram, nominal_style );
      style_hist( altered_histogram, altered_style );

      TPad * subtracted_pad = (TPad *) subtracted_canvas->cd( bin );
      nominal_histogram->Draw( "HIST E1" );
      altered_histogram->Draw( "HIST E1 SAMES" ); 
      add_pad_title( subtracted_pad, Form( "%s - %s %i", spectator_var.c_str(), analysis_var.c_str(), bin) );
      hist_prep_axes( nominal_histogram );
      set_axis_labels( nominal_histogram, spectator_bound.get_x_str(), Form( "Yield/%s", spectator_bound.get_y_str().c_str() ) );
      add_atlas_decorations( subtracted_pad, true );
      TLegend * subtracted_legend = below_logo_legend();
      subtracted_legend->SetTextSize( 0.02 );
      subtracted_legend->AddEntry( nominal_histogram, Form( "Hist mode - %s", nominal_histogram->GetName() ), "LP" );
      subtracted_legend->AddEntry( altered_histogram, Form( "Weight mode - %s", altered_histogram->GetName() ), "LP" );
      subtracted_legend->Draw();
      TPaveStats * nominal_stats = make_stats( nominal_histogram, true, false );
      TPaveStats * altered_stats = make_stats( altered_histogram, true, true );
      nominal_stats->Draw();
      altered_stats->Draw();

      hists.push_back( nominal_histogram );
      hists.push_back( altered_histogram );

    }

    subtracted_canvas->SaveAs( Form( "./comp_%s_%s_99_pos.png", unique.c_str(), mass.c_str() ) );

  }


}
