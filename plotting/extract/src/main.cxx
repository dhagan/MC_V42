#include <main.hxx>
#include <extr.hxx>
#include <ovly.hxx>
#include <comp.hxx>

int help(){

  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
      {"help",        no_argument,              0,      'h'},
      {"input",       required_argument,        0,      'i'},
      {"eff",         required_argument,        0,      'e'},
      {"anavar",      required_argument,        0,      'a'},
      {"specvar",     required_argument,        0,      's'},
      {"type",        required_argument,        0,      't'},
      {"unique",      required_argument,        0,      'u'},
      {"selections",  required_argument,        0,      'v'},
      {"range",       required_argument,        0,      'r'},
      {"hf_mode",     no_argument,              0,      'f'},
      {"pos_mode",    no_argument,              0,      'p'},
      {"t_ranges",    no_argument,              0,      'm'},
      {"comp_mode",   no_argument,              0,      'c'},
      {0,             0,                        0,      0}
    };

  std::string input, eff, avar, svar, type;
  std::string unique, selections_file, ranges;
  bool hf = false;
  bool t_range = false;
  bool pos_mode = false;
  bool ovly_mode = false;
  bool comp_mode = false;

  do {
    option = getopt_long( argc, argv, "i:e:a:s:t:u:v:r:cpofh", long_options, &option_index);
    switch (option){
      case 'i': 
        input       = std::string( optarg );
        break;
      case 'e':
        eff         = std::string ( optarg );
        break;
      case 'a':
        avar        = std::string( optarg );
        break;
      case 's':
        svar        = std::string( optarg );
        break;
      case 't':
        type        = std::string( optarg );
        break;
      case 'u':
        unique      = std::string( optarg );
        break;
      case 'v':
        selections_file = std::string( optarg );
        break;
      case 'r':
        ranges      = std::string( optarg );
        break;
      case 'p':
        pos_mode    = true;
        break;
      case 'f':
        hf = true;
        break;
      case 'm':
        t_range = true;
        break;
      case 'o':
        ovly_mode = true;
        break;
      case 'c':
        comp_mode = true;
        break;

      case 'h':
        return help();
    }
  } while (option != -1);


  bound_mgr * selections = new bound_mgr();
  selections->load_bound_mgr( selections_file );
  if ( !ranges.empty() ){
    selections->process_bounds_string( ranges );
  }


  if ( comp_mode ){

    comp( input, avar, svar, selections, unique, hf, pos_mode );

  } else if ( ovly_mode ) {

    ovly( input, avar, svar, selections, unique, hf, t_range, pos_mode );

  } else {
    
    extr( input, eff, avar, svar, type, selections, unique, hf, t_range, pos_mode );

  }

}
