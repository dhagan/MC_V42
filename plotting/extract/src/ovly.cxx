#include <ovly.hxx>


void ovly( std::string & input, std::string & abin_var, std::string & spec_var,
          bound_mgr * selections, std::string unique, bool hf_step, bool t_ranges, bool pos_mode ){

  prep_style();

  std::vector< std::string > mass_bins = { "Q0", "Q3", "Q4", "Q5", "Q12" };
  if ( t_ranges ){
    mass_bins.insert( mass_bins.end(), {"T0", "T3", "T4", "T5", "T12" } );
  }

  
  std::vector<std::string> filename_vec;
  split_strings( filename_vec, input, ":" );

  std::cout << input << std::endl;

  std::cout << filename_vec.at(0) << std::endl;
  std::cout << filename_vec.at(1) << std::endl;
  std::cout << filename_vec.at(2) << std::endl;
  std::cout << filename_vec.at(3) << std::endl;


  TFile * sign_file = new TFile( filename_vec.at(0).c_str(), "READ" );
  TFile * bckg_file = new TFile( filename_vec.at(1).c_str(), "READ" );
  TFile * bbbg_file = new TFile( filename_vec.at(2).c_str(), "READ" );
  TFile * data_file = new TFile( filename_vec.at(3).c_str(), "READ" );



  bound analysis_bound = selections->get_bound( abin_var );
  bound spectator_bound = selections->get_bound( spec_var );

  int ana_bins      = analysis_bound.get_bins();
  float ana_min     = analysis_bound.get_min(); 
  float ana_max     = analysis_bound.get_max(); 

  std::string pos = ( pos_mode ) ? "_99_pos_" : "_99_npos_";
  std::vector< float > eff_style =  {  21, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,      0.0, 0.0 };
  std::vector< float > extr_style = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kRed+1,   0.5, 1001 };
  std::vector< float > extr_sign = { 0.0, 0.0, 0.0, 0.0, 1.0, kRed+1, 1.0, 1.0, 0.0, 0.0, 0 };
  std::vector< float > extr_bckg = { 0.0, 0.0, 0.0, 0.0, 1.0, kBlue+1, 1.0, 1.0, 0.0, 0.0, 0 };
  std::vector< float > extr_bbbg = { 0.0, 0.0, 0.0, 0.0, 1.0, kGreen+1, 1.0, 1.0, 0.0, 0.0, 0 };
  std::vector< float > extr_data = { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0 };
  std::vector< float > corr_style = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, kBlue+1,  0.5, 1001 };

  for ( std::string & mass : mass_bins ){

    TCanvas * extr_canv  = new TCanvas( Form( "extr_canv_%s", mass.c_str() ), "", 100, 100, 1000, 1000 );
    extr_canv->Divide( 1, 1 );

    TH1F * extracted_sign = new TH1F( Form( "extracted_sign_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max);
    TH1F * extracted_bckg = new TH1F( Form( "extracted_bckg_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max);
    TH1F * extracted_bbbg = new TH1F( Form( "extracted_bbbg_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max);
    TH1F * extracted_data = new TH1F( Form( "extracted_data_%s", mass.c_str() ), "", ana_bins, ana_min, ana_max);
    std::string ana_str = Form( "%s%s%s_%s-", spec_var.c_str(), pos.c_str(), mass.c_str() , abin_var.c_str() );

    recombine_spectator( sign_file, ana_bins, ana_str, "sign", hf_step, extracted_sign );
    recombine_spectator( bckg_file, ana_bins, ana_str, "bckg", hf_step, extracted_bckg );
    recombine_spectator( bbbg_file, ana_bins, ana_str, "bbbg", hf_step, extracted_bbbg );
    recombine_spectator( data_file, ana_bins, ana_str, "data", hf_step, extracted_data );

    style_hist( extracted_sign, extr_sign );
    style_hist( extracted_bckg, extr_bckg );
    style_hist( extracted_bbbg, extr_bbbg );
    style_hist( extracted_data, extr_data );

    TPad * current_pad = (TPad *) extr_canv->cd(1);
    extracted_sign->Draw( "HIST E1" );
    extracted_bckg->Draw( "HIST E1 SAMES" );
    extracted_bbbg->Draw( "HIST E1 SAMES" );
    extracted_data->Draw( "HIST E1 SAMES" );
    hist_prep_axes( extracted_sign, true );
    hist_limits( extracted_sign, -1000, 1.5*std::max( { extracted_bbbg->GetMaximum(), extracted_sign->GetMaximum(), extracted_bckg->GetMaximum(), extracted_data->GetMaximum() } ) );
    set_axis_labels( extracted_sign, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( current_pad, Form( "Extracted %s %s", abin_var.c_str(), mass.c_str() ) );
    add_atlas_decorations( current_pad, true, false );
    TLegend * ovly_legend = create_atlas_legend();
    ovly_legend->AddEntry( extracted_sign, "sign", "LP" );
    ovly_legend->AddEntry( extracted_bckg, "bckg", "LP" );
    ovly_legend->AddEntry( extracted_bbbg, "bbbg", "LP" );
    ovly_legend->AddEntry( extracted_data, "data", "LP" );
    ovly_legend->Draw();


    extr_canv->SaveAs( Form("./extract_ovly_%s_A-%s_%s.png", mass.c_str(), abin_var.c_str(), unique.c_str()) );

    TCanvas * norm_canv  = new TCanvas( Form( "extr_canv_%s", mass.c_str() ), "", 100, 100, 1000, 1000 );
    norm_canv->Divide( 1, 1 );

    TH1F * norm_sign = (TH1F *) extracted_sign->Clone(); 
    TH1F * norm_bckg = (TH1F *) extracted_bckg->Clone();
    TH1F * norm_bbbg = (TH1F *) extracted_bbbg->Clone();
    TH1F * norm_data = (TH1F *) extracted_data->Clone();

    norm_sign->Scale( 1.0/norm_sign->Integral() ); 
    norm_bckg->Scale( 1.0/norm_bckg->Integral() );
    norm_bbbg->Scale( 1.0/norm_bbbg->Integral() );
    norm_data->Scale( 1.0/norm_data->Integral() );

    TPad * norm_pad = (TPad *) norm_canv->cd(1);
    norm_bbbg->Draw( "HIST E1" );
    norm_sign->Draw( "HIST E1 SAMES" );
    norm_bckg->Draw( "HIST E1 SAMES" );
    norm_data->Draw( "HIST E1 SAMES" );
    hist_prep_axes( norm_bbbg, true );
    hist_limits( norm_bbbg, -0.5, 1.5*std::max( { norm_bbbg->GetMaximum(), norm_sign->GetMaximum(), norm_bckg->GetMaximum(), norm_data->GetMaximum() } ) );
    set_axis_labels( norm_bbbg, analysis_bound.get_x_str(), Form( "Yield%s", analysis_bound.get_y_str().c_str() ) );
    add_pad_title( norm_pad, Form( "Extracted %s %s", abin_var.c_str(), mass.c_str() ), false );
    add_atlas_decorations( norm_pad, true, false );
    TLegend * norm_legend = create_atlas_legend();
    norm_legend->AddEntry( norm_sign, "sign", "LP" );
    norm_legend->AddEntry( norm_bckg, "bckg", "LP" );
    norm_legend->AddEntry( norm_bbbg, "bbbg", "LP" );
    norm_legend->AddEntry( norm_data, "data", "LP" );
    norm_legend->Draw();

    norm_canv->SaveAs( Form("./extract_norm_ovly_%s_A-%s_%s.png", mass.c_str(), abin_var.c_str(), unique.c_str()) );

    
  }

}
