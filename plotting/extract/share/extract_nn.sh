executable=${ANA_IP}/plotting/extract/build/extr
selections=${LIB_PATH}/share/selection_bounds.txt
unique=nn
log=${LOG_PATH}/extract/${unique}.txt
input="${OUT_PATH}/hf/${unique}/eval/hf_fit_nn_A-qtA_S-DNN.root"
efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_nn.root"

mkdir -p ${OUT_PATH}/extract/${unique}

pushd ${OUT_PATH}/extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input}" -e "${efficiency}" -v ${selections} -u ${unique} -a "qtA" -s "DNN" -t "sign" -f 2>&1 | tee ${log}

popd >> /dev/null
