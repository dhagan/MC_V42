executable=${ANA_IP}/plotting/extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique="phot_pt"
log=${LOG_PATH}/extract/${unique}.txt
variable="PhotonPt"
##input="${OUT_PATH}/hf/${unique}/eval/hf_fit_${unique}_A-${variable}_S-BDT.root"
sign_input="${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_sign_A-${variable}_S-BDT.root"
bckg_input="${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_bckg_A-${variable}_S-BDT.root"
bbbg_input="${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_bbbg_A-${variable}_S-BDT.root"
data_input="${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_data_A-${variable}_S-BDT.root"

efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_${unique}.root"

mkdir -p ${OUT_PATH}/extract/${unique}_sub

pushd ${OUT_PATH}/extract/${unique}_sub >> /dev/null

touch ${log}
##${executable} -i "${sign_input}" -e "${efficiency}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -t "sign" -p 
##${executable} -i "${bckg_input}" -e "${efficiency}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -t "bckg" -p
##${executable} -i "${bbbg_input}" -e "${efficiency}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -t "bbbg"
##${executable} -i "${data_input}" -e "${efficiency}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -t "data" -p
##${executable} -i "${input}" -e "${efficiency}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -t "bckg" 

${executable} -i "${sign_input}:${bckg_input}:${bbbg_input}:${data_input}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -o
popd >> /dev/null
