executable=${ANA_IP}/plotting/extract/build/extr
selections=${LIB_PATH}/share/selection_bounds.txt
unique=exp_bg_rat
log=${LOG_PATH}/extract/${unique}.txt
input="${OUT_PATH}/hf/${unique}/eval/hf_fit_${unique}_A-qtA_S-BDT.root"
efficiency="${OUT_PATH}/trees/gen_base/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/extract/${unique}

pushd ${OUT_PATH}/extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input}" -e "${efficiency}" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -t "sign" -f 2>&1 | tee ${log}

popd >> /dev/null
