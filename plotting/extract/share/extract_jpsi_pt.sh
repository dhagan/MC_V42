executable=${ANA_IP}/plotting/extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique="jpsi_pt"
log=${LOG_PATH}/extract/${unique}.txt
variable="DiMuonPt"
input="${OUT_PATH}/hf/${unique}/eval/hf_fit_${unique}_A-${variable}_S-BDT.root"
efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_${unique}.root"

mkdir -p ${OUT_PATH}/extract/${unique}

pushd ${OUT_PATH}/extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input}" -e "${efficiency}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -t "sign" -f 2>&1 | tee ${log}
${executable} -i "${input}" -e "${efficiency}" -v ${selections} -u ${unique} -a ${variable} -s "BDT" -t "bckg" -f 2>&1 | tee ${log}

popd >> /dev/null
