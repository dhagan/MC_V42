executable=${ANA_IP}/plotting/extract/build/extr
selections=${LIB_PATH}/share/selection_bounds.txt
unique=base
log=${LOG_PATH}/extract/${unique}.txt
input="${OUT_PATH}/event_hf/${unique}/eval/hf_fit_base_A-qtA_S-BDT.root"
efficiency="${OUT_PATH}/trees/gen_${unique}/sign_efficiencies_base.root"

mkdir -p ${OUT_PATH}/extract/${unique}

pushd ${OUT_PATH}/extract/${unique} >> /dev/null

touch ${log}
${executable} -i "${input}" -e "${efficiency}" -v ${selections} -u ${unique} -a "qtA" -s "BDT" -t "sign" -f -p 2>&1 | tee ${log}

popd >> /dev/null
