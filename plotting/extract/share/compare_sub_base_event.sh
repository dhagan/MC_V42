executable=${ANA_IP}/plotting/extract/build/extr
selections=${LIB_PATH}/share/hf_bounds.txt
unique="base"
output_unique="event_vs_original"
log=${LOG_PATH}/extract/${unique}.txt
variable="qtA"
base_sign_input="${OUT_PATH}/subtraction/${unique}/subtracted_${unique}_sign_A-${variable}_S-BDT.root"
comp_sign_input="${OUT_PATH}/event_subtraction/${unique}/subtracted_${unique}_sign_A-${variable}_S-BDT.root"


touch ${log}
mkdir -p ${OUT_PATH}/extract/${output_unique}_sub
pushd ${OUT_PATH}/extract/${output_unique}_sub >> /dev/null

${executable} -i ${base_sign_input}:${comp_sign_input} -v ${selections} -u ${output_unique} -a ${variable} -s "BDT" -c

popd >> /dev/null
