# Exponential background fits

This program is for fitting the dimuon continuum in the jpsi mass spectrum to an exponential function instead of the baseline assumption of a linear contribution.
Parameters extracted for this exponential should be used to calculate the approximate ratio of background in the sidebands to that in the centre of the mass peak.
If the background is linear, the ratio of the two regions is 1. C = I4+I1/I2+I3 = (1+y^3)(y+y^2), with y=exp( -ad ), where d is the interval width and a the extracted parameter.
