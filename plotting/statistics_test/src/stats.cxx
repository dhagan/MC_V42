#include <stats.hxx>

void stat(){

  prep_style();
 
  TTree * source_tree = new TTree( "tree", "tree" );
  double variable_same = 0.0, variable_diff = 0.0;
  double weight = 0;
  int flag = 0;

  source_tree->Branch( "variable_same", &variable_same );
  source_tree->Branch( "variable_diff", &variable_diff );
  source_tree->Branch( "weight", &weight );
  source_tree->Branch( "flag", &flag );

  for ( size_t entry = 0; entry < 11; entry++ ){
    if ( entry < 5 ){
      variable_same = 0.5;
      variable_diff = 0.5;
      weight = 1.0;
      flag = 1.0;
    } else if ( entry < 7 ){
      variable_same = 0.5;
      variable_diff = 0.5;
      weight = -1.0;
      flag = 0.0;
    } else if ( entry < 10 ) {
      variable_same = 1.5;
      variable_diff = 1.5;
      weight = 1.0;
      flag = 1.0;
    } else if ( entry < 11 ){
      variable_same = 1.5;
      variable_diff = 1.4;
      weight = -1.0;
      flag = 0.0;
    }

    source_tree->Fill();
  }
  
  
  TFile * source_file = new TFile( "./source.root", "RECREATE" );
  source_file->cd();
  source_tree->Write( "tree" );

  source_file->Close();
  
  // now replicate input the process
  
  TFile * input_file = new TFile( "./source.root", "READ" );
  TTree * input_tree = (TTree *) input_file->Get( "tree" );

  std::vector< float > combination_style = { 1.0, 0.0, 0.0, 0.0, 1.0, kRed+1, 1.0, 1.0, 0.0, 0.0, 0 };
  std::vector< float > weight_style = { 1.0, 0.0, 0.0, 0.0, 2.0, kBlue+1, 1.0, 1.0, 0.0, 0.0, 0 };

  TH1F * combination_model_histogram_same = new TH1F( "combination_hist_same", "", 2, 0, 2 );
  TH1F * first_model_histogram_same = new TH1F( "first_hist_same", "", 2, 0, 2 );
  TH1F * second_model_histogram_same = new TH1F( "second_hist_same", "", 2, 0, 2 );
  TH1F * weight_model_histogram_same = new TH1F( "weight_hist_same", "", 2, 0, 2 );

  TH1F * combination_model_histogram_diff = new TH1F( "combination_hist_diff", "", 2, 0, 2 );
  TH1F * first_model_histogram_diff = new TH1F( "first_hist_diff", "", 2, 0, 2 );
  TH1F * second_model_histogram_diff = new TH1F( "second_hist_diff", "", 2, 0, 2 );
  TH1F * weight_model_histogram_diff = new TH1F( "weight_hist_diff", "", 2, 0, 2 );


  combination_model_histogram_same->Sumw2( kTRUE );
  first_model_histogram_same->Sumw2( kTRUE );
  second_model_histogram_same->Sumw2( kTRUE ); 
  weight_model_histogram_same->Sumw2( kTRUE );

  input_tree->Draw( "variable_same>>weight_hist_same", "weight", "goff");
  input_tree->Draw( "variable_same>>first_hist_same", "flag==1", "goff");
  input_tree->Draw( "variable_same>>second_hist_same", "flag==0", "goff");

  combination_model_histogram_same->Add( first_model_histogram_same, second_model_histogram_same, 1.0, -1.0 );

  gStyle->SetOptStat( 11111111 );
  TCanvas * stats_canvas = new TCanvas( "stats_canv", "", 200, 200, 3000, 1500 ); 
  stats_canvas->Divide( 2, 1 );

  TPad * stats_pad = (TPad *) stats_canvas->cd( 1 );
  style_hist( combination_model_histogram_same, combination_style );
  style_hist( weight_model_histogram_same, weight_style );
  combination_model_histogram_same->Draw( "HIST E1" );
  weight_model_histogram_same->Draw( "HIST E1 SAMES" );

  add_pad_title( stats_pad, "Weight vs Combination example, bin fills match", false );
  hist_prep_axes( combination_model_histogram_same );
  combination_model_histogram_same->GetYaxis()->SetRangeUser( 0, 15 );
  set_axis_labels( combination_model_histogram_same, "Variable", "Yield/1.0 [var]" );
  add_atlas_decorations( stats_pad, true );
  TLegend * stats_legend_same = below_logo_legend();
  stats_legend_same->SetTextSize( 0.02 );
  stats_legend_same->AddEntry( combination_model_histogram_same, Form( "Hist mode - %s", combination_model_histogram_same->GetName() ), "LP" );
  stats_legend_same->AddEntry( weight_model_histogram_same, Form( "Weight mode - %s", weight_model_histogram_same->GetName() ), "LP" );
  stats_legend_same->Draw();
  TPaveStats * combination_model_stats_same = make_stats( combination_model_histogram_same, true, false );
  TPaveStats * weight_model_stats_same = make_stats( weight_model_histogram_same, true, true );
  combination_model_stats_same->Draw();
  weight_model_stats_same->Draw();

  combination_model_histogram_diff->Sumw2( kTRUE );
  first_model_histogram_diff->Sumw2( kTRUE );
  second_model_histogram_diff->Sumw2( kTRUE ); 
  weight_model_histogram_diff->Sumw2( kTRUE );

  input_tree->Draw( "variable_diff>>weight_hist_diff", "weight", "goff");
  input_tree->Draw( "variable_diff>>first_hist_diff", "flag==1", "goff");
  input_tree->Draw( "variable_diff>>second_hist_diff", "flag==0", "goff");

  combination_model_histogram_diff->Add( first_model_histogram_diff, second_model_histogram_diff, 1.0, -1.0 );


  stats_pad = (TPad *) stats_canvas->cd( 2 );
  style_hist( combination_model_histogram_diff, combination_style );
  style_hist( weight_model_histogram_diff, weight_style );
  combination_model_histogram_diff->Draw( "HIST E1" );
  weight_model_histogram_diff->Draw( "HIST E1 SAMES" );

  add_pad_title( stats_pad, "Weight vs Combination example, bin fills differ", false );
  hist_prep_axes( combination_model_histogram_diff );
  combination_model_histogram_diff->GetYaxis()->SetRangeUser( 0, 15 );
  set_axis_labels( combination_model_histogram_diff, "Variable", "Yield/1.0 [var]" );
  add_atlas_decorations( stats_pad, true );
  TLegend * stats_legend_diff = below_logo_legend();
  stats_legend_diff->SetTextSize( 0.02 );
  stats_legend_diff->AddEntry( combination_model_histogram_diff, Form( "Hist mode - %s", combination_model_histogram_diff->GetName() ), "LP" );
  stats_legend_diff->AddEntry( weight_model_histogram_diff, Form( "Weight mode - %s", weight_model_histogram_diff->GetName() ), "LP" );
  stats_legend_diff->Draw();
  TPaveStats * combination_model_stats_diff = make_stats( combination_model_histogram_diff, true, false );
  TPaveStats * weight_model_stats_diff = make_stats( weight_model_histogram_diff, true, true );
  combination_model_stats_diff->Draw();
  weight_model_stats_diff->Draw();




  stats_canvas->SaveAs( "stats_test.png" );




}
