#include <main.hxx>
#include <stats.hxx>

int help(){
  std::cout << "h     --    help      --  output help" << std::endl;
  return 0;
}

int main (int argc, char * argv[]){

  int option{0}, option_index{0};

  static struct option long_options[] = {
    {"help",        no_argument,              0,      'h'},
    {0,             0,                        0,      0}
  };


  do {
    option = getopt_long( argc, argv, "h", long_options, &option_index );
    switch (option){
      case 'h':
        return help();
        break;
    }
  } while (option != -1);

  stat();

  return 0;

}

