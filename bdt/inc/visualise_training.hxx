#ifndef visualise_hxx
#define visualise_hxx

#include <anna.hxx>

#include <TDirectory.h>

void visualise_training( std::string & train_file, std::string & hyperparams, std::string & discriminating_vars,
                         std::string & spectator_vars, bound_mgr * bounds, std::string & unique );

#endif
