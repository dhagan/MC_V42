#include <string>

void optimise( std::string sign_file_str, std::string bckg_file_str, std::string hpar_str, std::string disc_var_str,
              std::string spec_var_str, std::string unique_str );
