cmake_minimum_required( VERSION 3.14.0 )

## Project details
project( bdt )

## Set sources and lib paths.
set( sources src/main.cxx src/train.cxx src/optimise.cxx src/eval.cxx src/visualise_training.cxx )

## Find root package
find_package( ROOT CONFIG REQUIRED COMPONENTS TMVA )
##target_compile_definitions( ROOT::Core INTERFACE ${ROOT_DEFINITIONS} )

## Prepare executable
add_compile_options( -std=c++17 -Wall -Werror -Wextra -pedantic -O -O2 )
add_executable( bdt ${sources} )
target_include_directories( bdt PUBLIC inc ${CMAKE_SOURCE_DIR}/lib/analysis_utils/inc )

## Link libraries
target_link_libraries( bdt PRIVATE ROOT::Core ${ROOT_LIBRARIES} )
target_link_libraries( bdt PRIVATE analysis_utils  )

## Enable export.
set( CMAKE_EXPORT_COMPILE_COMMANDS on )
