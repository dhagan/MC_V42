# BDT Training and evaluation

## Description
This stage of the analysis takes "reco" files from the tree generation step and trains a boosted decision tree (BDT), follwoing this the BDT is used to give a score to every event in any input file provided, as long as it matches the expected structure of the BDT for analysis. This bdt score is appended to the output tree as a branch called "BDT"

## Usage
Scripts for baseline and numerous systematic variations have been written, these == runs scripts are stored in the ./share directory ==. These provide proper output structure and output organisation.

```
Usage:
./bdt --mode,-m MODE --sign,-s SIGN_PATH --disc_vars,-d DISCRIMINATOR_VARAIABLES --spec_vars,-v SPECTATOR_VARABLES \
      --hyperparams,-h HYPERPARAMETERS --type,-t TYPE --unique,-u UNIQUE [ --bckg,-b BCKG_PATH ] \
      [ --xml_path,-x WEIGHT_XML_PATH ]

Required arguments;
  --mode,-m             Program execution mode takes train, eval, and optimise.
                            "train"     --  Train the BDT on input signal and background file. This requires 
                                            -b option to be specified and a filepath to be supplied.
                            "eval"      --  Evaluate given tree, provide path to tree for processing to
                                            the -s argument.
                            "optimise"  --  Do not use, not complete.
  --sign,-s             Path to input tree, accepts all file types. Provide only "sign" type files 
                        in the "train" mode.
  --disc_vars,-d        Colon separated list of variables used for calculating BDT score.
  --spec_vars,-v        Colon separated list of all other variables that are NOT used for calculating 
                        BDT score.
  --hyperparams,-h      Colon separated list of hyperparameters used for defining construction and
                        and training of the BDT.
  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is 
                        necessary for bookkeeping.

Optional arguements;
  --type,-t             Type of input file, accepts "sign", "bckg", "data", and "bbbg". Must be
                        specified in "eval" mode.
  --bckg,-b             Path for background file, must be specified in the "train" mode.
  --xml_path,-x         Path to the xml file containing the previously completed BDT training.
                        This must be provided for the "eval" mode.
  --help,-?             output help message.
```
