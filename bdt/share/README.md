# BDT Scripts

## List of scripts and purpose.
| Script name       | Function |
| :---              | :---     |
|  base.sh          | This is the baseline script.                               |
|  generic.sh       | Generic script, takes unique as first bash argument.       |
|  wide.sh          | Wider $`q_{T}^{A}`$ processing, 25 bins from -20 to 30 GeV.|
|  jp8.sh           | Full training and eval for  $` P_{T,J/\psi} > 8 GeV `$.    |
|  jp9.sh           | Full training and eval for  $` P_{T,J/\psi} > 9 GeV `$.    |
|  jp10.sh          | Full training and eval for  $` P_{T,J/\psi} > 10 GeV `$.   |
|  jpsi\_phot.sh    | Training for $`P_{T,J/\psi}`$                              |
|  phot\_pt.sh      | Training for $`P_{T,\gamma}`$                              |

## Script Structure

Script variables, these are the baseline
```
unique=base
executable=${ANA_IP}/bdt/build/bdt
log=${LOG_PATH}/bdt/${unique}.txt
data=${OUT_PATH}/trees/gen_${unique}/data/data_${unique}.root:tree
sign=${OUT_PATH}/trees/gen_${unique}/sign/sign_${unique}.root:tree
bckg=${OUT_PATH}/trees/gen_${unique}/bckg/bckg_${unique}.root:tree
bbbg=${OUT_PATH}/trees/gen_${unique}/bbbg/bbbg_${unique}.root:tree
bdtxml=${OUT_PATH}/bdt/${unique}/tmva_dataloader/weights/tmva_classification_BDT.weights.xml
disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
spec="ActIpX:AvgIpX:AbsdY:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0"
hpar="!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5%:PruneMethod=NoPruning"
```

Make log, create directories, push to them
```
touch ${log}
mkdir -p ${OUT_PATH}/bdt/${unique}
mkdir -p ${OUT_PATH}/bdt/${unique}/eval
pushd ${OUT_PATH}/bdt/${unique} >> /dev/null
```


Train the tree.
```
${executable} -s ${sign} -b ${bckg} -d ${disc} -v ${spec} -m "train" -h ${hpar} -u ${unique} 2>&1 | tee ${log}
```

Evaluate trees
```
$executable -s ${data} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "data" 2>&1 | tee -a ${log}
$executable -s ${sign} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "sign" 2>&1 | tee -a ${log}
$executable -s ${bckg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "bckg" 2>&1 | tee -a ${log}
$executable -s ${bbbg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${unique} -t "bbbg" 2>&1 | tee -a ${log}
```

Push out and exit
```
popd >> /dev/null
```
