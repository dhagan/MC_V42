#!/bin/bash

#Script Shell Variables
unique=vars
old_unique=base
storage=run_${unique}
data=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${old_unique}/data/,TreeD
sign=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${old_unique}/sign/,TreeD
bckg=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${old_unique}/bckg/,TreeD
bbbg=/home/atlas/dhagan/analysis/analysisFork/445/TMVATrees/run/gen_${old_unique}/bbbg/,TreeD
bdtxml=/home/atlas/dhagan/analysis/analysisFork/445/TMVA/run_${unique}/dataset_1o2/weights/TMVA_Training.xml

disc="AbsdPhi:AbsdY:AbsPhi:AbsCosTheta:qtA:qtB:qTSquared:Lambda"
spec="ActIpX:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:JPsi_Eta:Phot_Eta:DeltaZ0"
hpar="!H:!V:NTrees=1600:MaxDepth=16:BoostType=AdaBoost:AdaBoostBeta=0.01:SeparationType=GiniIndex:nCuts=100:MinNodeSize=20%:PruneMethod=CostComplexity:PruneStrength=0.40"

mkdir -p ${storage}
cd ${storage}
mkdir -p eval
mkdir -p Evalpngs
mkdir -p dataset_1o2/weights
bdtmapper=../../../tmvacode/BDT_Trainer/source/BDTMapper

$bdtmapper -n ${sign} -v ${bckg} -l ${disc} -s ${spec} -t Train -H ${hpar}
mv dataset_1o2/weights/TMVAClassification_MVACategories_1o2.weights.xml ${bdtxml}

#### original data
##$bdtmapper -n ${data} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
##mv Evaluate.root eval/eval_data.root
##
#### original signal
##$bdtmapper -n ${sign} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
##mv Evaluate.root eval/eval_sign.root
##
#### original pp
##$bdtmapper -n ${bckg} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
##mv Evaluate.root eval/eval_bckg.root
##
#### original bb
##$bdtmapper -n ${bbbg} -l ${disc} -s ${spec} -t Evaluate -H ${hpar} -x ${bdtxml} 
##mv Evaluate.root eval/eval_bbbg.root
