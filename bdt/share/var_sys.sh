#!/bin/bash

aaDPDY_disc="AbsdPhi:AbsdY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qTSquared"
aaDPDY_spec="DPhi:DY:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

faDPDY_disc="DPhi:AbsdY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qTSquared"
faDPDY_spec="AbsdPhi:DY:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

ffDPDY_disc="DPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qTSquared"
ffDPDY_spec="AbsdPhi:AbsdY:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

aaf_disc="AbsdPhi:AbsPhi:costheta:DY:Lambda:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qTSquared"
aaf_spec="AbsdY:DPhi:Phi:AbsCosTheta:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

afa_disc="AbsdPhi:Phi:AbsCosTheta:DY:Lambda:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qTSquared"
afa_spec="AbsdY:DPhi:AbsPhi:costheta:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

faa_disc="DPhi:AbsPhi:AbsCosTheta:DY:Lambda:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qTSquared"
faa_spec="AbsdY:AbsdPhi:Phi:costheta:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

##extd_disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qtL:qtM:qTSquared"
##extd_spec="AbsdY:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"
##
noqt2Disc_disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum"
noqt2Disc_spec="AbsdY:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:qTSquared:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

noL_disc="AbsdPhi:DY:AbsPhi:AbsCosTheta:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum:qtA:qtB:qTSquared"
noL_spec="AbsdY:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:DiMuonPt:PhotonPt:qtL:qtM:Lambda:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB"

hpar="!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5%:PruneMethod=NoPruning"

##extd="${extd_disc}#${extd_spec}&extd"
aaf="${aaf_disc}#${aaf_spec}&aaf"
afa="${afa_disc}#${afa_spec}&afa"
faa="${faa_disc}#${faa_spec}&faa"
noqt2Disc="${noqt2Disc_disc}#${noqt2Disc_spec}&noqt2Disc"
noL="${noL_disc}#${noL_spec}&noL"

aaDPDY="${aaDPDY_disc}#${aaDPDY_spec}&aaDPDY"
afDPDY="${afDPDY_disc}#${afDPDY_spec}&afDPDY"
faDPDY="${faDPDY_disc}#${faDPDY_spec}&faDPDY"
ffDPDY="${ffDPDY_disc}#${ffDPDY_spec}&ffDPDY"

executable=${exec_path}/bdt

input_unique="base"
data=${OUT_PATH}/trees/${input_unique}/data/data_${input_unique}.root:tree
sign=${OUT_PATH}/trees/${input_unique}/sign/sign_${input_unique}.root:tree
bckg=${OUT_PATH}/trees/${input_unique}/bckg/bckg_${input_unique}.root:tree
bbbg=${OUT_PATH}/trees/${input_unique}/bbbg/bbbg_${input_unique}.root:tree

types=( "data" "sign" "bckg" "bbbg" )
vars=( ${aaf} ${afa} ${faa} ${noqt2Disc} ${noL} ${ffDPDY} ${aaDPDY} ${faDPDY} ${noqt2Disc} ${noL} )

for var in ${vars[@]}
do
	disc_spec=${var%&*}
	disc=${disc_spec%#*}
	spec=${disc_spec#*#}
	output_unique="base_${var#*&}"
	bdtxml=${OUT_PATH}/bdt/${output_unique}/tmva_dataloader/weights/tmva_classification_BDT.weights.xml
	
	mkdir -p ${OUT_PATH}/bdt/${output_unique}
	mkdir -p ${OUT_PATH}/bdt/${output_unique}/eval
	pushd ${OUT_PATH}/bdt/${output_unique} >> /dev/null

	${executable} -s ${sign} -b ${bckg} -d ${disc} -v ${spec} -m "train" -h ${hpar} -u ${output_unique}

	${executable} -s ${sign} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "sign" & 
	${executable} -s ${bckg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "bckg" &
	${executable} -s ${bbbg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "bbbg" &
	${executable} -s ${data} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "data"

	popd >> /dev/null
done

