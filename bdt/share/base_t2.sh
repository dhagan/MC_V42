#!/bin/bash

#Script Shell Variables
input_unique="base"
output_unique="base_t2"
executable=${exec_path}/bdt
data=${OUT_PATH}/trees/${input_unique}/data/data_${input_unique}.root:tree
sign=${OUT_PATH}/trees/${input_unique}/sign/sign_${input_unique}.root:tree
bckg=${OUT_PATH}/trees/${input_unique}/bckg/bckg_${input_unique}.root:tree
bbbg=${OUT_PATH}/trees/${input_unique}/bbbg/bbbg_${input_unique}.root:tree
bdtxml=${OUT_PATH}/bdt/${output_unique}/tmva_dataloader/weights/tmva_classification_BDT.weights.xml

disc="AbsdPhi:DY:Lambda:AbsPhi:AbsCosTheta:qTSquared"
spec="ActIpX:AvgIpX:AbsdY:DPhi:costheta:Phi:DiMuonMass:DiMuonTau:Trigger_HLT_2mu4_bJpsimumu_noL2:qtL:qtM:DiMuonPt:PhotonPt:JPsi_Eta:Phot_Eta:DeltaZ0:pu_weight:MuPos_Pt:MuPos_Eta:MuNeg_Pt:MuNeg_Eta:DiLept_DeltaR:DiLept_Rap:qtA:qtB:qxpsi:qypsi:qxgamma:qygamma:qxsum:qysum"
hpar="!H:!V:NTrees=200:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.15:SeparationType=GiniIndex:nCuts=100:MinNodeSize=5%:PruneMethod=NoPruning"

mkdir -p ${OUT_PATH}/bdt/${output_unique}
mkdir -p ${OUT_PATH}/bdt/${output_unique}/eval
mkdir -p ${OUT_PATH}/bdt/${output_unique}/vis
pushd ${OUT_PATH}/bdt/${output_unique} >> /dev/null

## train tree
$executable -s ${sign} -b ${bckg} -d ${disc} -v ${spec} -m "train" -h ${hpar} -u ${output_unique}

## eval samples
##$executable -s ${sign} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "sign" &
##$executable -s ${bckg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "bckg" &
##$executable -s ${bbbg} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "bbbg" &
##$executable -s ${data} -d ${disc} -v ${spec} -m "eval" -h ${hpar} -x ${bdtxml} -u ${output_unique} -t "data" 
##
#### visualise training
##train_root="${OUT_PATH}/bdt/${output_unique}/tmva_train_${output_unique}.root"
##${executable} -s ${train_root} -d ${disc} -v ${spec} -m "vis" -h ${hpar} -u ${output_unique} ##2>&1 | tee ${log}


popd >> /dev/null
