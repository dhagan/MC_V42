#include <main.hxx>
#include <eval.hxx>

void eval( std::string input_file, std::string disc_vars, std::string spec_vars, 
           std::string type, std::string unique, std::string xml_path ){

  
  std::vector<std::string> input_vec;
  split_strings( input_vec, input_file, ":" );

  TFile * input = new TFile( input_vec.at(0).c_str(), "READ" );
  TTree * input_tree = (TTree*) input->Get( input_vec.at(1).c_str() );

  TMVA::Reader * reader = new TMVA::Reader( "!Color:!Verbose" );

  std::map< std::string, Float_t > disc_map; 
  std::map< std::string, Float_t > spec_map; 

  std::vector<std::string> disc_var_vec, spec_var_vec;
  split_strings( disc_var_vec, disc_vars, ":" );
  split_strings( spec_var_vec, spec_vars, ":" );

  for ( std::string & disc : disc_var_vec ){
    disc_map[disc] = 0;
    reader->AddVariable( disc.c_str(),  &(disc_map.find(disc)->second) );
  }

  for ( std::string & spec : spec_var_vec ){
    spec_map[spec] = 0;
    reader->AddSpectator( spec.c_str(), &(spec_map.find(spec)->second) );
  }
  
  TFile * tmva_eval_output = new TFile( Form( "./eval/eval_%s.root", type.c_str() ), "RECREATE" );
  TTree * tmva_output_tree = new TTree( "tree", Form( "%s_%s", type.c_str(), unique.c_str() ) );

  for ( std::string & disc : disc_var_vec ){
    input_tree->SetBranchAddress( disc.c_str(), &(disc_map.find(disc)->second) );
    tmva_output_tree->Branch( disc.c_str(), &(disc_map.find(disc)->second) );
  }
  
  for ( std::string & spec : spec_var_vec ){
    input_tree->SetBranchAddress( spec.c_str(), &(spec_map.find(spec)->second) );
    tmva_output_tree->Branch( spec.c_str(), &(spec_map.find(spec)->second) );
  }

  double bdt_score = 0;
  tmva_output_tree->Branch( "BDT", &bdt_score );

  reader->BookMVA( "BDT", xml_path.c_str() );

  int entries = input_tree->GetEntries();

  for ( int entry = 0; entry <= entries; entry++ ){
    input_tree->GetEntry( entry );
    bdt_score = reader->EvaluateMVA( "BDT" );
    tmva_output_tree->Fill();
  }

  input->Close();
  tmva_eval_output->cd();
  tmva_output_tree->Write();
  tmva_eval_output->Close();

}
