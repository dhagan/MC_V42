
#include <main.hxx>

#include <eval.hxx>
#include <train.hxx>
#include <optimise.hxx>
#include <visualise_training.hxx>

// converting scripts from old version

// no mode equivalent in this 
// nominal -n -> signal -s
// background -v -> bckg -b
// training frac -f -> -f, unused
// number folds -p -> -n
// hyperparams -H -> h
// discriminators -l -> disc -d
// spec_vars -s -> -v
// xml path -x -> -x
// unique_str ? -> -u
// type ? -> -t


int help(){

	std::cout << "Usage:" << std::endl;
	std::cout << "./bdt --mode,-m MODE --sign,-s SIGN_PATH --disc_vars,-d DISCRIMINATOR_VARAIABLES --spec_vars,-v SPECTATOR_VARABLES \\ " << std::endl;
	std::cout << "      --hyperparams,-h HYPERPARAMETERS --type,-t TYPE --unique,-u UNIQUE [ --bckg,-b BCKG_PATH ] \\ " << std::endl;
	std::cout << "      [ --xml_path,-x WEIGHT_XML_PATH ] " << std::endl;
	std::cout << " " << std::endl;
	std::cout << "Required arguments; " << std::endl;
	std::cout << "  --mode,-m             Program execution mode takes train, eval, and optimise. " << std::endl;
	std::cout << "                            \"train\"     --  Train the BDT on input signal and background file. This requires " << std::endl;
	std::cout << "                                            -b option to be specified and a filepath to be supplied. " << std::endl;
	std::cout << "                            \"eval\"      --  Evaluate given tree, provide path to tree for processing to " << std::endl;
	std::cout << "                                            the -s argument. " << std::endl;
	std::cout << "                            \"optimise\"  --  Do not use, not complete. " << std::endl;
	std::cout << "                            \"vis\"  --  Visualise output of training." << std::endl;
	std::cout << "  --sign,-s             Path to input tree, accepts all file types. Provide only \"sign\" type files " << std::endl;
	std::cout << "                        in the \"train\" mode. In the \"vis\" mode, simply provide the path to the dataloader output. " << std::endl;
	std::cout << "  --disc_vars,-d        Colon separated list of variables used for calculating BDT score. " << std::endl;
	std::cout << "  --spec_vars,-v        Colon separated list of all other variables that are NOT used for calculating " << std::endl;
	std::cout << "                        BDT score. " << std::endl;
	std::cout << "  --hyperparams,-h      Colon separated list of hyperparameters used for defining construction and " << std::endl;
	std::cout << "                        and training of the BDT. " << std::endl;
	std::cout << "  --unique,-u           A unique string for this processing. This could be the name of the systematic, etc. This is " << std::endl;
	std::cout << "                        necessary for bookkeeping. " << std::endl;
	std::cout << " " << std::endl;
	std::cout << "Optional arguements; " << std::endl;
	std::cout << "  --type,-t             Type of input file, accepts \"sign\", \"bckg\", \"data\", and \"bbbg\". Must be " << std::endl;
	std::cout << "                        specified in \"eval\" mode. " << std::endl;
  std::cout << "  --bounds,-k           Path to a bounds file for processing, for use in \" vis\" mode only." << std::endl;
	std::cout << "  --bckg,-b             Path for background file, must be specified in the \"train\" mode. " << std::endl;
	std::cout << "  --xml_path,-x         Path to the xml file containing the previously completed BDT training. " << std::endl;
	std::cout << "                        This must be provided for the \"eval\" mode. " << std::endl;
	std::cout << "  --help,-?             output help message. " << std::endl;
  return 0;
}

int main( int argc, char *argv[] ) {

  std::string mode;
  std::string sign_file, bckg_file;
  std::string hyperparams;
  std::string disc_vars, spec_vars;
  std::string xml_path;
  std::string type, unique;
  std::string bounds_path;

  static struct option long_options[] = {
    { "mode",           required_argument,  0,        'm'},
    { "sign",           required_argument,  0,        's'},
    { "bckg",           required_argument,  0,        'b'},
    { "hyperparams",    required_argument,  0,        'h'},
    { "disc_vars ",     required_argument,  0,        'd'},
    { "spec_vars",      required_argument,  0,        'v'},
    { "xml_path",       required_argument,  0,        'x'},
    { "type",           required_argument,  0,        't'},
    { "unique",         required_argument,  0,        'u'},
    { "bounds",         required_argument,  0,        'k'},
    { "help",           no_argument,        0,        '?'},
    { 0,                0,                  0,        0}
  };

  int option_index{0}, option{0};
  do {
    option = getopt_long( argc, argv, "m:s:b:h:d:v:x:t:u:k:?", long_options, &option_index);
    switch ( option ){
      case 'm':
        mode          =   std::string( optarg );
        break;
      case 's':
        sign_file =   std::string( optarg );
        break;
      case 'b': 
        bckg_file =   std::string( optarg );
        break;
      case 'h':
        hyperparams   =   std::string( optarg );
        break;
      case 'd':
        disc_vars  =   std::string( optarg );
        break;
      case 'v':
        spec_vars  =   std::string( optarg );
        break;
      case 'x':
        xml_path      =   std::string( optarg );
        break;
      case 't':
        type      =   std::string( optarg );
        break;
      case 'k':
        bounds_path     = std::string( optarg );
        break;
      case 'u':
        unique    =   std::string( optarg );
        break;
      case '?':
        return help();
        break;
    }
  } while ( option != -1 );


  if ( mode.find("train") != std::string::npos ){
    train( sign_file, bckg_file, hyperparams, disc_vars, spec_vars, unique );
  }

  if ( mode.find("eval") != std::string::npos ){
    eval( sign_file, disc_vars, spec_vars, type, unique, xml_path );
  }
  
  if ( mode.find("optimise") != std::string::npos ){
    optimise( sign_file, bckg_file, hyperparams, disc_vars, spec_vars, unique );
  }
  if ( mode.find("vis") != std::string::npos ){
    bound_mgr * bounds = new bound_mgr();
    bounds->load_bound_mgr( bounds_path );
    visualise_training( sign_file, hyperparams, disc_vars, spec_vars, bounds, unique );
  }
}
