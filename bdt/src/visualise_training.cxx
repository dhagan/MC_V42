#include <visualise_training.hxx>

#include <anna.hxx>
#include <plotting.hxx>

void visualise_training( std::string & train_file, std::string & hyperparams, std::string & discriminating_vars,
                         std::string & spectator_vars, bound_mgr * bounds, std::string & unique ){

  if ( !hyperparams.empty() ){ std::cout << hyperparams << std::endl; }
  if ( !discriminating_vars.empty() ){ std::cout << discriminating_vars<< std::endl; }
  if ( !spectator_vars.empty() ){ std::cout << spectator_vars << std::endl; }
  if ( !unique.empty() ){ std::cout << unique << std::endl; }
  if ( bounds != nullptr ){ std::cout << "bounds valid" << std::endl; }


  std::vector< std::string > discriminator_vector, scatter_hist_names;
  split_strings( discriminator_vector, discriminating_vars, ":" );
  for ( std::string & outer_disc : discriminator_vector ){
    for ( std::string & inner_disc : discriminator_vector ){
      if ( inner_disc.compare( outer_disc ) == 0 ){ continue; }
      scatter_hist_names.push_back( Form( "scat_%s_vs_%s", inner_disc.c_str(), outer_disc.c_str() ) );
    }
  }

  prep_style();
  gStyle->SetPalette( kGreyScale );
  TColor::InvertPalette();


  TFile * training_output = new TFile( train_file.c_str(), "READ");
  TDirectory * dataloader_directory = training_output->GetDirectory( "tmva_dataloader" );

  // Draw Correlations
  TH2F * sign_correlation = (TH2F *) dataloader_directory->Get( "CorrelationMatrixS" );
  TH2F * bckg_correlation = (TH2F *) dataloader_directory->Get( "CorrelationMatrixB" );
  TH2F * correlation_delta = (TH2F *) sign_correlation->Clone();
  TH2F * correlation_ratio = (TH2F *) sign_correlation->Clone();
  correlation_delta->Reset();
  correlation_ratio->Reset();
  correlation_delta->Add( sign_correlation, bckg_correlation, 1.0, -1.0 );
  correlation_ratio->Divide( sign_correlation, bckg_correlation, 1.0, 1.0 );

  TCanvas * correlation_canvas = new TCanvas( "corr_canvas", "", 200, 200, 2000, 2000 );
  correlation_canvas->Divide( 2, 2 );

  TPad * correlation_pad = (TPad *) correlation_canvas->cd( 1 );
  sign_correlation->Draw( "COLZ" );
  
  hist_prep_text( sign_correlation );
  add_pad_title( correlation_pad, "Signal Correlation", false );

  correlation_pad = (TPad *) correlation_canvas->cd( 2 );
  bckg_correlation->Draw( "COLZ" );
  hist_prep_text( bckg_correlation );
  add_pad_title( correlation_pad, "Background Correlation", false );

  correlation_pad = (TPad *) correlation_canvas->cd( 3 );
  correlation_delta->Draw( "COLZ" );
  hist_prep_text( correlation_delta );
  add_pad_title( correlation_pad, "Correlation #Delta, sign-bckg", false );

  correlation_pad = (TPad *) correlation_canvas->cd( 4 );
  correlation_ratio->Draw( "COLZ" );
  hist_prep_text( correlation_ratio );
  add_pad_title( correlation_pad, "Correlation ratio - sign/bckg", false );

  correlation_canvas->SaveAs( "./vis/correlations.png" );

  TDirectory * BDT_upper_directory = dataloader_directory->GetDirectory( "Method_BDT" );
  TDirectory * BDT_lower_directory = BDT_upper_directory->GetDirectory( "BDT" );
  TDirectory * correlation_directory = BDT_lower_directory->GetDirectory( "CorrelationPlots" );

  for ( std::string & hist_name : scatter_hist_names ){

    TH2F * sign_scatter_hist = (TH2F *) correlation_directory->Get( Form( "%s_Signal", hist_name.c_str() ) );
    TH2F * bckg_scatter_hist = (TH2F *) correlation_directory->Get( Form( "%s_Background", hist_name.c_str() ) );
    if ( sign_scatter_hist == nullptr || bckg_scatter_hist == nullptr ){ continue; }
    sign_scatter_hist->RebinX( 10 ); 
    sign_scatter_hist->RebinY( 10 );
    bckg_scatter_hist->RebinX( 10 );
    bckg_scatter_hist->RebinY( 10 );


    TCanvas * scatter_canvas = new TCanvas( Form( "%s", hist_name.c_str() ), "", 200, 200, 2000, 1000 );
    scatter_canvas->Divide( 2, 1 );

    TPad * scatter_pad = (TPad *) scatter_canvas->cd( 1 );
    sign_scatter_hist->Draw( "COLZ" );
    auto sign_ltx = new TLatex();
    sign_ltx->SetTextSize( 0.025 );
    sign_ltx->DrawLatexNDC( 0.54, 0.77, Form("Correlation = %.5f", sign_scatter_hist->GetCorrelationFactor()) );
    sign_ltx->DrawLatexNDC( 0.54, 0.74, Form("Covariance = %.5f", sign_scatter_hist->GetCovariance()) );
    hist_prep_text( sign_scatter_hist );
    add_pad_title( scatter_pad, Form("Signal %s", hist_name.c_str() ), false );

    scatter_pad = (TPad *) scatter_canvas->cd( 2 );
    bckg_scatter_hist->Draw( "COLZ" );
    auto bckg_ltx = new TLatex();
    bckg_ltx->SetTextSize( 0.025 );
    bckg_ltx->DrawLatexNDC( 0.54, 0.77, Form("Correlation = %.2f", bckg_scatter_hist->GetCorrelationFactor()) );
    bckg_ltx->DrawLatexNDC( 0.54, 0.74, Form("Covariance = %.2f", bckg_scatter_hist->GetCovariance()) );
    hist_prep_text( bckg_scatter_hist );
    add_pad_title( scatter_pad, Form("Background %s", hist_name.c_str() ), false );

    scatter_canvas->SaveAs( Form( "./vis/%s.png", hist_name.c_str() ) );

  }

  TCanvas * train_stats_canvas = new TCanvas( "train_stats", "", 200, 200, 2000, 2000 );
  train_stats_canvas->Divide( 2, 2 );

  TH1D * sign_efficiency = (TH1D *) BDT_lower_directory->Get( "MVA_BDT_effS" );
  TH1D * bckg_efficiency = (TH1D *) BDT_lower_directory->Get( "MVA_BDT_effB" );
  TH1D * sign_bckg_efficiency = (TH1D *) BDT_lower_directory->Get( "MVA_BDT_effBvsS" ) ;
  TH1D * sign_bckg_rej_eff = (TH1D *) BDT_lower_directory->Get( "MVA_BDT_rejBvsS" );


  TPad * stats_pad = (TPad *) train_stats_canvas->cd( 1 );
  sign_efficiency->GetXaxis()->SetRangeUser( -1.0, 1.0 );
  sign_efficiency->Draw();
  hist_prep_text( sign_efficiency );
  set_axis_labels( sign_efficiency, "BDT Score", "Efficiency" );
  add_pad_title( stats_pad, "Signal efficiency", false );

  stats_pad = (TPad *) train_stats_canvas->cd( 2 );
  bckg_efficiency->Draw();
  hist_prep_text( bckg_efficiency );
  set_axis_labels( bckg_efficiency, "BDT Score", "Efficiency" );
  bckg_efficiency->GetXaxis()->SetRangeUser( -1.0, 1.0 );
  add_pad_title( stats_pad, "Background efficiency", false );

  stats_pad = (TPad *) train_stats_canvas->cd( 3 );
  sign_bckg_efficiency->Draw();
  hist_prep_text( sign_bckg_efficiency );
  set_axis_labels( sign_bckg_efficiency, "Signal Efficiency", "Background Efficiency" );
  sign_bckg_efficiency->GetYaxis()->SetRangeUser( 0.0, 1.0 );
  add_pad_title( stats_pad, "Signal vs Background efficiency ", false );

  stats_pad = (TPad *) train_stats_canvas->cd( 4 );
  sign_bckg_rej_eff->Draw();
  hist_prep_text( sign_bckg_rej_eff );
  set_axis_labels( sign_bckg_rej_eff, "Signal Efficiency", "Background Rejection" );
  add_pad_title( stats_pad, "Signal efficiency vs Background rejection", false );

  train_stats_canvas->SaveAs( "./vis/training_efficiencies.png");

}
