#include <main.hxx>
#include <optimise.hxx>
#include <plotting.hxx>
#include <process_utils.hxx>

#include <iostream>
#include <fstream>

void optimise( std::string sign_file_str, std::string bckg_file_str, std::string hpar_str, std::string disc_var_str,
              std::string spec_var_str, std::string unique_str ){


  std::vector<std::string> sign_vec, bckg_vec;
  split_strings( sign_vec, sign_file_str, ":" );
  split_strings( bckg_vec, bckg_file_str, ":" );

  std::string hyperparameters{ hpar_str };
  std::string unique{ unique_str };

  std::vector<std::string> disc_vars, spec_vars;
  split_strings( disc_vars, disc_var_str, ":" );
  split_strings( spec_vars, spec_var_str, ":" );


  std::cout << "load input trees" << std::endl;
  TFile * sign_file = new TFile( sign_vec.at(0).c_str(), "READ" );
  TFile * bckg_file = new TFile( bckg_vec.at(0).c_str(), "READ" );
  TTree * sign_tree = (TTree*) sign_file->Get( sign_vec.at(1).c_str() );
  TTree * bckg_tree = (TTree*) bckg_file->Get( bckg_vec.at(1).c_str() );

  
  std::cout << "prepare output tree" << std::endl;
  TFile * tmva_train_output = new TFile( Form( "./opt_%s.root", unique.c_str() ), "RECREATE" );

  std::cout << "create factory" << std::endl;
  TMVA::Factory * tmva_factory = new TMVA::Factory( Form( "factory_%s", unique.c_str() ), tmva_train_output, "!V:!Silent:Color:AnalysisType=Classification");

  std::cout << "create dataloader" << std::endl;
  TMVA::DataLoader * tmva_dataloader = new TMVA::DataLoader( Form( "loader_%s", unique.c_str() ) );
  
  std::cout << "assigning trees to loader" << std::endl;
  tmva_dataloader->AddTree( sign_tree, "Signal", 1.0, "(EventNumber%2)==0", TMVA::Types::kTraining);
  tmva_dataloader->AddTree( sign_tree, "Signal", 1.0, "(EventNumber%2)==1", TMVA::Types::kTesting);
  tmva_dataloader->AddTree( bckg_tree, "Background", 1.0, "(EventNumber%2)==0", TMVA::Types::kTraining);
  tmva_dataloader->AddTree( bckg_tree, "Background", 1.0, "(EventNumber%2)==1", TMVA::Types::kTesting);
  
  std::cout << "assigning discriminating and spectator variables" << std::endl;
  for ( std::string & disc : disc_vars ){ tmva_dataloader->AddVariable( disc.c_str(), 'F' ); }
  for ( std::string & spec : spec_vars ){ tmva_dataloader->AddSpectator( spec.c_str(), 'F' ); }


  std::cout << "prep trees" << std::endl;
  tmva_dataloader->PrepareTrainingAndTestTree( "", "NormMode=EqualNumEvents:!V" );

  std::cout << "ready bdt" << std::endl;
  tmva_factory->BookMethod( tmva_dataloader, TMVA::Types::kBDT, unique.c_str(), hyperparameters.c_str() );

  tmva_factory->TrainAllMethods();
  tmva_factory->TestAllMethods();
  tmva_factory->EvaluateAllMethods();

  //SetAtlasStyle();
  gROOT->SetBatch(true);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  gStyle->SetEndErrorSize(15);

  gStyle->SetPadTopMargin(0.19);
  gStyle->SetPadRightMargin(0.16);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  TGraph * roc_curve = tmva_factory->GetROCCurve( tmva_dataloader, unique.c_str() );
  double roc_integral = tmva_factory->GetROCIntegral( tmva_dataloader, unique.c_str() );

  std::string filename{"/home/atlas/dhagan/analysis/bdt_opt/run/roc_integrals.txt"};
  std::ofstream roc_file;
  roc_file.open( filename, std::ios_base::app );
  roc_file << unique << "#" << roc_integral << std::endl;

  TFile * roc_graphs = new TFile( "/home/atlas/dhagan/analysis/bdt_opt/run/roc_curves.root" , "UPDATE" );
  roc_graphs->cd();
  roc_curve->Write( Form( "roc_curve_%s", unique.c_str() ) );
  roc_graphs->Close();
  
  TCanvas * roc_canv = new TCanvas( "roc_canv", "roc_canv", 100, 100, 1000, 1000 );
  roc_canv->Divide(1,1);
  roc_canv->cd(1);
  roc_curve->Draw();
  roc_canv->SaveAs( Form( "./%s_roc_curve.png", unique.c_str() ) );


}
