yaml="$(pwd)/pull.yaml"
mkdir -p ${OUT_PATH}/lh_summary/pull/
pushd ${OUT_PATH}/lh_summary/pull/ >> /dev/null
lh_summary -i ${yaml}
popd >> /dev/null
