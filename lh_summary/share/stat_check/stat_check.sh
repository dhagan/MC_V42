##yaml="${ANA_IP}/likelihood_fit/share/systematic/systematics.yaml"
yaml="$(pwd)/stat_check.yaml"

mkdir -p ${OUT_PATH}/lh_summary/stat_check/
pushd ${OUT_PATH}/lh_summary/stat_check/ >> /dev/null
lh_summary -i ${yaml}
popd >> /dev/null
