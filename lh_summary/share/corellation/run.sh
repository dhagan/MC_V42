#!/bin/bash

yaml="$(pwd)/cor.yaml"

mkdir -p ${OUT_PATH}/lh_summary/${unique}/correlation
pushd ${OUT_PATH}/lh_summary/${unique}/correlation >> /dev/null
lh_summary -i ${yaml}
popd >> /dev/null
