yaml="$(pwd)/compare.yaml"
mkdir -p ${OUT_PATH}/lh_summary/compare/
pushd ${OUT_PATH}/lh_summary/compare/ >> /dev/null
lh_summary -i ${yaml}
popd >> /dev/null
