##yaml="${ANA_IP}/likelihood_fit/share/systematic/systematics.yaml"
yaml="$(pwd)/systematics.yaml"

mkdir -p ${OUT_PATH}/lh_summary/systematics/
pushd ${OUT_PATH}/lh_summary/systematics/ >> /dev/null
lh_summary -i ${yaml}
popd >> /dev/null
