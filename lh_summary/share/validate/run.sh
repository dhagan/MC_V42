#!/bin/bash

yaml="$(pwd)/val.yaml"

mkdir -p ${OUT_PATH}/lh_summary/${unique}/validate
pushd ${OUT_PATH}/lh_summary/${unique}/validate >> /dev/null
lh_summary -i ${yaml}
popd >> /dev/null
