#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef exrat_hxx 
#define exrat_hxx

void exrat( YAML::Node & run_node, variable_set & variables );

#endif
