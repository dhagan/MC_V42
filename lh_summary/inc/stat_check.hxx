#include <funcs.hxx>
#include <yaml-cpp/yaml.h>

#ifndef statchk_hxx 
#define statchk_hxx

void stat_check( YAML::Node & run_node, variable_set & variables );

#endif
