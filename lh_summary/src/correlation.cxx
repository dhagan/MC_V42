#include <correlation.hxx>
#include <sys/stat.h>
#include <cmath>

double get_NF_corr( const std::string & fit_path ){
  
  YAML::Node table_result = YAML::LoadFile( fit_path + "CorrelationMatrix.yaml" );
  YAML::Node matrix = table_result[1]["correlation_rows"];
  int idx = matrix[ matrix.size()-1 ].size();
  return matrix[ matrix.size()-1 ][idx-2].as<double>();

}


void corr_table( std::vector<double> & correlations, variable_set & variables ){

  std::vector<std::string > ana_names = variables.analysis_bound.get_series_names();

  std::string table_layout = "|c|", bin_names = "bin ", corr_str ="correlation";
  table_layout.reserve( 30 ), bin_names.reserve( 200 ), correlations.reserve( 200 );
  for ( size_t bin = 0; bin < ana_names.size(); bin++ ){
    table_layout += " r ";
    bin_names += "&" + ana_names[bin].substr(0, ana_names[bin].find( '-' ) );  
  }
  table_layout += "|}\n \\hline\n";
  bin_names += "\\\\ \\hline\n";
  

  std::ofstream corr_table( "table_corr.tex" );
  corr_table << "\\documentclass[landscape]{article}\n";
  corr_table << "\\usepackage[paperheight=4in,paperwidth=10in]{geometry}\n";
  corr_table << "\\begin{document}";
  corr_table << std::fixed << std::setprecision( 2 );
  corr_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{";
  corr_table << table_layout;
  corr_table << bin_names;
  corr_table << corr_str;
  for ( size_t bin = 0; bin < ana_names.size(); bin++ ){ corr_table << "&" << correlations[bin]; }
  corr_table << "\\\\ \\hline\n";
  corr_table << "\\end{tabular}\n\\caption{Correlations}\n\\end{table}\n\\end{document}";
  corr_table.close();

}






void correlation( YAML::Node run_node, variable_set & variables ){

  std::string unique = run_node["unique"].as<std::string>();
  std::string dir_name = "./" + unique;
  mkdir( dir_name.c_str(), 744 );
  chdir( dir_name.c_str() );
  
  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();
  std::vector< double > correlations = {};

  YAML::Emitter emitter;

  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";
    correlations.push_back( get_NF_corr( trex_qta_path ) );

  }

  corr_table( correlations, variables );

}
