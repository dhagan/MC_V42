#include <compare.hxx>


void compare( YAML::Node run_node, variable_set & variables ){

  prep_style();

  const std::string base_unique = run_node[ "base_unique" ].as<std::string>();
  const std::string diff_unique = run_node[ "diff_unique" ].as<std::string>();
  const std::string compare_name = run_node[ "compare_name" ].as<std::string>();
  const std::string base_name = run_node[ "base_name" ].as<std::string>();
  const std::string diff_name = run_node[ "diff_name" ].as<std::string>();
  const std::string ratio_name = diff_name + "/" + base_name;

  std::cout << 15 << std::endl;
  auto[ base_total, base_extracted ] = get_fit( base_unique, variables );
  std::cout << 18 << std::endl;
  auto[ diff_total, diff_extracted ] = get_fit( diff_unique, variables );
  std::cout << 18 << std::endl;

  delete base_total;
  delete diff_total;

  TH1F * base_sign = base_extracted.sign_hist;
  TH1F * diff_sign = diff_extracted.sign_hist;
  TH1F * ratio_sign = static_cast<TH1F *>( base_sign->Clone() );
  ratio_sign->Reset();
  ratio_sign->Divide( diff_sign, base_sign, 1.0, 1.0 );

  base_sign->SetLineColor( kBlack );
  base_sign->SetLineStyle( 1 );
  diff_sign->SetLineColor( kBlack );
  diff_sign->SetLineStyle( 2 );
  base_sign->SetName( base_name.c_str() );
  diff_sign->SetName( diff_name.c_str() );


  TCanvas * compare_canvas = new TCanvas( "compare", "", 200, 200, 2000, 1000 );
  gStyle->SetOptStat( "krinmes" );
  compare_canvas->Divide( 2, 1 );

  TPad * active_pad = static_cast<TPad *>( compare_canvas->cd( 1 ) );
  base_sign->Draw( "HIST E1" );
  diff_sign->Draw( "HIST E1 SAMES" );
  hist_prep_axes( base_sign );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( base_sign, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "", true );
  base_sign->GetYaxis()->SetRangeUser( 0, base_sign->GetMaximum()*1.2 );
  TLegend * compare_legend = below_logo_legend();
  compare_legend->AddEntry( base_sign, base_name.c_str() );
  compare_legend->AddEntry( diff_sign, diff_name.c_str() );
  compare_legend->Draw( "SAME" );
  TPaveStats * base_stats = make_stats( base_sign, true, true );
  base_stats->Draw( "SAME" );
  TPaveStats * diff_stats = make_stats( diff_sign, true, false );
  diff_stats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( compare_canvas->cd( 2 ) );
  ratio_sign->Draw( "HIST E1" );
  hist_prep_axes( ratio_sign );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( ratio_sign, "q_{T}^{A}", ratio_name.c_str() );
  add_pad_title( active_pad, "Ratio", true );
  ratio_sign->GetYaxis()->SetRangeUser( 0, 2 );
  TPaveStats * ratio_stats = make_stats( ratio_sign );
  ratio_stats->Draw( "SAME" );

  const std::string file_name = "compare_" + base_unique + "_vs_" + diff_unique + ".png";
  compare_canvas->SaveAs( file_name.c_str() );

  base_extracted.erase();
  diff_extracted.erase();
  delete ratio_sign;
  delete compare_canvas;

}
