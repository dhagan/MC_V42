#include <stat_check.hxx>
#include <sys/stat.h>


TH1F * get_input_hist( std::string & unique, int bin = 7 ){

  std::string file_location  = std::string(std::getenv( "OUT_PATH" ) ) 
                             + "/trex/" + unique + "/hists/data_"
                             + unique + "_qtA" + std::to_string( bin ) 
                             +  "-15.root";
  TFile * input_file = new TFile( file_location.c_str(), "READ" );
  input_file->ls();
  TH1F * histogram = static_cast<TH1F *>(input_file->Get( "sr" ) );
  //std::cout << histogram->GetName() << std::endl;
  //input_file->Close();
  return histogram;

}

TH1F * get_trex_hist( std::string & unique, int bin = 7 ){

  std::string file_location  = std::string( std::getenv( "OUT_PATH" ) ) 
                             + "/trex/" + unique + "/qtA" 
                             + std::to_string( bin ) + "-15/" + unique
                             + "/Histograms/" + unique + "_histos.root";
  TFile * input_file = new TFile( file_location.c_str(), "READ" );
  TH1F * histogram = static_cast< TH1F *>( input_file->Get( "Analysis/data/nominal/Analysis_data" ));
  //std::cout << histogram->GetName() << std::endl;
  //input_file->Close();
  return histogram;
}

void stat_check( YAML::Node & run_node, variable_set & variables ){

  mkdir( "./stat_check", 744 );
  chdir( "./stat_check" );

  std::cout << variables.analysis_variable << std::endl;

  std::string x_string = "BDT/0.04 [score]";
  std::string y_string = "Entries/0.04 [score]";

  std::string unique = run_node["unique"].as<std::string>();
  TH1F * input_hist = get_input_hist( unique, 7 );
  TH1F * trex_hist = get_trex_hist( unique, 7 );
  input_hist->SetLineColor(kRed + 1);
  trex_hist->SetLineColor(kBlue+ 1);
  trex_hist->SetLineStyle( 2 );

  std::cout << input_hist->Integral() << std::endl; 
  std::cout << trex_hist->Integral() << std::endl; 

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast< TPad *>( canv.cd( 1 ) );
  input_hist->Draw( "E1" );
  trex_hist->Draw( "SAME E1" );
  add_atlas_decorations( active_pad, false, false );
  add_internal( active_pad );
  set_axis_labels( input_hist, x_string.c_str(), y_string.c_str() );
  hist_prep_axes( input_hist, true );
  //TPaveStats * stats = make_stats( input_hist );
  //stats->Draw( "SAME" );
  input_hist->GetYaxis()->SetRangeUser( 0, 1050 );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( input_hist, "Input", "LP" );
  legend->AddEntry( input_hist, "TREx", "LP" );
  legend->Draw();
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  std::string output_filename = "mass_vgt_fit_" + std::string( input_hist->GetName() ) + ".pdf";
  canv.SaveAs( output_filename.c_str() );
  canv.Clear();

  chdir( ".." );



}