#include <pulls.hxx>
#include <TGraphErrors.h>
#include <sys/stat.h>

void pulls( YAML::Node run_node, variable_set & variables ){

  prep_style();

  std::vector< std::string > ana_cut_names  = variables.analysis_bound.get_series_names();

  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
    
  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";

  std::string directory = "./" + unique;
  mkdir( directory.c_str(), 744 );
  chdir( directory.c_str() );
  

  std::vector< double > photon_sf_value;
  std::vector< double > photon_sf_error;
  std::vector< double > muon_sf_value;
  std::vector< double > muon_sf_error;
  std::vector< double > muon_reco_value;
  std::vector< double > muon_reco_error;
  std::vector< double > photon_weight_value;
  std::vector< double > photon_weight_error;
  std::vector< double > jpsi_weight_value;
  std::vector< double > jpsi_weight_error;
  std::vector< double > dphi_weight_value;
  std::vector< double > dphi_weight_error;
  std::vector< double > absdphi_weight_value;
  std::vector< double > absdphi_weight_error;
  std::vector< double > dy_weight_value;
  std::vector< double > dy_weight_error;
  std::vector< double > qta_bin;
  std::vector< double > ex;
  std::vector< double > bin_edge = {0.5};

  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){
    qta_bin.push_back( ana_idx + 1 );
    ex.push_back( 0 );
    bin_edge.push_back( bin_edge[ana_idx] + 1 );
  }
  bin_edge.push_back( bin_edge[ bin_edge.size()] + 1 );

  std::vector< double > shade_edge = { 0.45, 0.55, 15.45, 15.55 };
  TH1F * error_shade = new TH1F( "es", "", 3, &shade_edge[0] );
  TH1F * error_shade_upper = new TH1F( "esu", "", 3, &shade_edge[0] );
  TH1F * error_shade_lower = new TH1F( "esl", "", 3, &shade_edge[0] );
  for ( size_t ana_idx = 1; ana_idx <= 3; ana_idx++ ){
    error_shade->SetBinContent( ana_idx, 0 );
    error_shade_upper->SetBinContent( ana_idx, 1.5 );
    error_shade_lower->SetBinContent( ana_idx, -1.5 );
    error_shade->SetBinError( ana_idx, 1 );
    error_shade_upper->SetBinError( ana_idx, 0.5 );
    error_shade_lower->SetBinError( ana_idx, 0.5 );
  }



  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_cut_names[ ana_idx ];
    std::string trex_qta_path = trex_path + qta_name + "/" + unique + "/";

    std::string trex_roofit_out = trex_qta_path + "Fits/" + unique + ".root";
    TFile * roofit_out = new TFile( trex_roofit_out.c_str(), "READ" );
    //RooFitResult * rfr = (RooFitResult *) roofit_out->Get( "nll_simPdf_newasimovData_with_constr" );
    RooFitResult * rfr = (RooFitResult *) roofit_out->Get( "RooEvaluatorWrapper" );

    RooRealVar * photon_sf = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_photon_sf" );
    RooRealVar * muon_sf = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_msf" );
    RooRealVar * muon_reco = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_reconstruction_mr" );
    RooRealVar * photon_weight = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_photon_weight" );
    RooRealVar * jpsi_weight = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_jpsi_weight" );
    RooRealVar * dphi_weight = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_dphi_weight" );
    RooRealVar * absdphi_weight = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_absdPhi_weight" );
    RooRealVar * dy_weight = (RooRealVar * ) rfr->floatParsFinal().find( "alpha_dy_weight" );


    if ( photon_sf != nullptr ){
      photon_sf_value.push_back( photon_sf->getVal()   );
      photon_sf_error.push_back( photon_sf->getError() );
    } else {
      photon_sf_value.push_back( 0.0 );
      photon_sf_error.push_back( 0.0 );
    }
    muon_sf_value.push_back(   muon_sf->getVal()   );
    muon_sf_error.push_back(   muon_sf->getError() );
    if ( muon_reco != nullptr ){
      muon_reco_value.push_back( muon_reco->getVal()   );
      muon_reco_error.push_back( muon_reco->getError() );
    } else {
      muon_reco_value.push_back( 0.0 );
      muon_reco_error.push_back( 0.0 );
    }
    if ( photon_weight != nullptr ){
      photon_weight_value.push_back( photon_weight->getVal()   );
      photon_weight_error.push_back( photon_weight->getError() );
    } else {
      photon_weight_value.push_back( 0.0 );
      photon_weight_error.push_back( 0.0 );
    }
    if ( jpsi_weight != nullptr ){
      jpsi_weight_value.push_back( jpsi_weight->getVal()   );
      jpsi_weight_error.push_back( jpsi_weight->getError() );
    } else {
      jpsi_weight_value.push_back( 0.0 );
      jpsi_weight_error.push_back( 0.0 );
    }
    if ( dphi_weight != nullptr ){
      dphi_weight_value.push_back( dphi_weight->getVal()   );
      dphi_weight_error.push_back( dphi_weight->getError() );
    } else {
      dphi_weight_value.push_back( 0.0 );
      dphi_weight_error.push_back( 0.0 );
    }
    if ( absdphi_weight != nullptr ){
      absdphi_weight_value.push_back( absdphi_weight->getVal()   );
      absdphi_weight_error.push_back( absdphi_weight->getError() );
    } else {
      absdphi_weight_value.push_back( 0.0 );
      absdphi_weight_error.push_back( 0.0 );
    }
    if ( dy_weight != nullptr ){
      dy_weight_value.push_back( dy_weight->getVal()   );
      dy_weight_error.push_back( dy_weight->getError() );
    } else {
      dy_weight_value.push_back( 0.0 );
      dy_weight_error.push_back( 0.0 );
    }

  }
  error_shade->SetBinContent( max_bins, 0 );
  error_shade->SetBinError( max_bins, 1 );
  error_shade->SetBinContent( max_bins+1, 0 );
  error_shade->SetBinError( max_bins+1, 1 );

  TGraphErrors photon_sf_pull( max_bins, &qta_bin[0], &photon_sf_value[0], &ex[0], & photon_sf_error[0] );
  TGraphErrors muon_sf_pull( max_bins, &qta_bin[0], &muon_sf_value[0], &ex[0], & muon_sf_error[0] );
  TGraphErrors muon_reco_pull( max_bins, &qta_bin[0], &muon_reco_value[0], &ex[0], & muon_reco_error[0] );
  TGraphErrors photon_weight_pull( max_bins, &qta_bin[0], &photon_weight_value[0], &ex[0], & photon_weight_error[0] );
  TGraphErrors jpsi_weight_pull( max_bins, &qta_bin[0], &jpsi_weight_value[0], &ex[0], & jpsi_weight_error[0] );
  TGraphErrors dphi_weight_pull( max_bins, &qta_bin[0], &dphi_weight_value[0], &ex[0], & dphi_weight_error[0] );
  TGraphErrors absdphi_weight_pull( max_bins, &qta_bin[0], &absdphi_weight_value[0], &ex[0], & absdphi_weight_error[0] );
  TGraphErrors dy_weight_pull( max_bins, &qta_bin[0], &dy_weight_value[0], &ex[0], & dy_weight_error[0] );

  photon_sf_pull.SetMarkerStyle( 20 );
  muon_sf_pull.SetMarkerStyle( 20 );
  muon_reco_pull.SetMarkerStyle( 20 );
  photon_weight_pull.SetMarkerStyle( 20 );
  jpsi_weight_pull.SetMarkerStyle( 20 );
  dphi_weight_pull.SetMarkerStyle( 20 );
  absdphi_weight_pull.SetMarkerStyle( 20 );
  dy_weight_pull.SetMarkerStyle( 20 );

  error_shade->SetFillColorAlpha( kGreen, 0.8 );
  error_shade->SetMarkerStyle( 0 );
  error_shade_upper->SetFillColorAlpha( kYellow, 0.8 );
  error_shade_upper->SetMarkerStyle( 0 );
  error_shade_lower->SetFillColorAlpha( kYellow, 0.8 );
  error_shade_lower->SetMarkerStyle( 0 );


  TCanvas canv( "canv", "", 2000, 1000 );


  canv.Divide( 1, 1 );
  TPad * active_pad = static_cast<TPad*>( canv.cd( 1 ) );
  photon_sf_pull.Draw( "AP" );
  photon_sf_pull.GetYaxis()->SetRangeUser( -3, 3 );
  photon_sf_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  photon_sf_pull.Draw( "P SAME" );
  photon_sf_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  photon_sf_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "Photon SF", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "PhotonSFPull.pdf" );
  canv.Clear();


  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad*>( canv.cd( 2 ) );
  muon_sf_pull.Draw( "AP" );
  muon_sf_pull.GetYaxis()->SetRangeUser( -3, 3 );
  muon_sf_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  muon_sf_pull.Draw( "P SAME" );
  muon_sf_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  muon_sf_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "Muon SF", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "MuonSFPull.pdf" );
  canv.Clear();



  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad*>(canv.cd( 3 ) );
  muon_reco_pull.Draw( "AP" );
  muon_reco_pull.GetYaxis()->SetRangeUser( -3, 3 );
  muon_reco_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  muon_reco_pull.Draw( "P SAME" );
  muon_reco_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  muon_reco_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "Muon reco", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "MuonRecoPull.pdf" );
  canv.Clear();



  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad*>( canv.cd( 4 ) );
  photon_weight_pull.Draw( "AP" );
  photon_weight_pull.GetYaxis()->SetRangeUser( -3, 3 );
  photon_weight_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  photon_weight_pull.Draw( "P SAME" );
  photon_weight_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  photon_weight_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "Photon Weight", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "PhotonWeightPull.pdf" );
  canv.Clear();

  

  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad*>( canv.cd( 5 ) );
  jpsi_weight_pull.Draw( "AP" );
  jpsi_weight_pull.GetYaxis()->SetRangeUser( -3, 3 );
  jpsi_weight_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  jpsi_weight_pull.Draw( "P SAME" );
  jpsi_weight_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  jpsi_weight_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "J/#psi Weight", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "JpsiPtPull.pdf" );
  canv.Clear();


  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad*>( canv.cd( 6 ) );
  dphi_weight_pull.Draw( "AP" );
  dphi_weight_pull.GetYaxis()->SetRangeUser( -3, 3 );
  dphi_weight_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  dphi_weight_pull.Draw( "P SAME" );
  dphi_weight_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  dphi_weight_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "#Delta#phi Weight", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "DeltaPhiPull.pdf" );
  canv.Clear();


  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad*>(canv.cd( 7 ) );
  dy_weight_pull.Draw( "AP" );
  dy_weight_pull.GetYaxis()->SetRangeUser( -3, 3 );
  dy_weight_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  dy_weight_pull.Draw( "P SAME" );
  dy_weight_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  dy_weight_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "#Delta Y Weight", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "DeltaYPull.pdf" );
  canv.Clear();


  canv.Divide( 1, 1 );
  active_pad = static_cast<TPad*>(canv.cd( 8 ) );
  absdphi_weight_pull.Draw( "AP" );
  absdphi_weight_pull.GetYaxis()->SetRangeUser( -3, 3 );
  absdphi_weight_pull.GetXaxis()->SetRangeUser( 0, 16 );
  error_shade->Draw( "SAME E3" );
  error_shade_upper->Draw( "SAME E3" );
  error_shade_lower->Draw( "SAME E3" );
  absdphi_weight_pull.Draw( "P SAME" );
  absdphi_weight_pull.GetXaxis()->SetTitle( "q_{T}^{A} bin" );
  absdphi_weight_pull.GetYaxis()->SetTitle( "(#hat{#theta}-#theta_{0})/#Delta#theta" );
  add_pad_title( active_pad, "Abs(#Delta#phi) Weight", false );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  canv.SaveAs( "AbsDeltaYPull.pdf" );
  canv.Clear();

  chdir( ".." );

}