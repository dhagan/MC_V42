#include <full_result.hxx>
#include <regex>
#include <systematic.hxx>
#include <tuple>

// i know what's happening here, things are being translated because muon_sf is being droppedd
// this needs to be done with some form of thingy recognition
//double rel_effect_systxt( const std::string & line ){

void make_table( TH1F * stat, TH1F * stat_sys_in, TH1F * all, variable_set & variables ){

  std::vector<std::string > ana_names = variables.analysis_bound.get_series_names();

  std::vector<double > cntr_val( stat->GetNbinsX(), 0.0);
  std::vector<double > stat_err( stat->GetNbinsX(), 0.0);
  std::vector<double > syst_in_err( stat->GetNbinsX(), 0.0);
  std::vector<double > syst_ex_err( stat->GetNbinsX(), 0.0);
  std::vector<double > total_err( stat->GetNbinsX(), 0.0);
  std::vector<double > stat_err_rel( stat->GetNbinsX(), 0.0);
  std::vector<double > syst_in_err_rel( stat->GetNbinsX(), 0.0);
  std::vector<double > syst_ex_err_rel( stat->GetNbinsX(), 0.0);
  std::vector<double > total_err_rel( stat->GetNbinsX(), 0.0);

  for ( int bin_idx = 1, vec_idx = 0; bin_idx <= stat->GetNbinsX(); bin_idx++, vec_idx++ ){

    cntr_val[vec_idx] = stat_sys_in->GetBinContent( bin_idx );
    double se = stat->GetBinError( bin_idx );
    double be = stat_sys_in->GetBinError( bin_idx );
    double te = all->GetBinError( bin_idx );
    stat_err[vec_idx] = se;
    syst_ex_err[vec_idx] = std::sqrt( te*te - be*be );
    syst_in_err[vec_idx] = std::sqrt( be*be - se*se );
  
    stat_err_rel[vec_idx] = se/cntr_val[vec_idx];
    syst_ex_err_rel[vec_idx] = std::sqrt( te*te - be*be )/cntr_val[vec_idx];
    syst_in_err_rel[vec_idx] = std::sqrt( be*be - se*se )/cntr_val[vec_idx];
    total_err_rel[vec_idx] = all->GetBinError( bin_idx )/cntr_val[vec_idx];
    total_err[vec_idx] = all->GetBinError( bin_idx );
  }

  std::ofstream perc_table( "table_perc.tex" );
  perc_table << "\\documentclass{article} \n\\begin{document}";
  perc_table << std::fixed << std::setprecision( 2 );
  perc_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{| c | r r r r r |} \n \\hline\n";
  perc_table << "Bin & Yield & statistical(\\%) & systematic(\\%) & external(\\%) & total(\\%) \\\\ \\hline\n";
  for ( int vec_idx = 0; vec_idx < stat->GetNbinsX(); vec_idx++ ){
    perc_table << ana_names[vec_idx] << " & " << cntr_val[vec_idx] << " & " << stat_err_rel[vec_idx]*100.0 << " & ";
    perc_table << syst_in_err_rel[vec_idx]*100.0 << " & " << syst_ex_err_rel[vec_idx]*100.0 << " & ";
    perc_table << total_err_rel[vec_idx]*100.0 << "\\\\ \\hline\n";
  }
  perc_table << "\\end{tabular}\n\\caption{Percentage Errors}\n\\end{table}\n\\end{document}";
  perc_table.close();

  std::ofstream rel_table( "table_rel.tex" );
  rel_table << "\\documentclass{article} \n\\begin{document}";
  rel_table << std::fixed << std::setprecision( 2 );
  rel_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{| c | r r r r r |} \n \\hline\n";
  rel_table << "Bin & Yield & statistical & systematic & external & total \\\\ \\hline\n";
  for ( int vec_idx = 0; vec_idx < stat->GetNbinsX(); vec_idx++ ){
    rel_table << ana_names[vec_idx] << " & " << cntr_val[vec_idx] << " & " << stat_err_rel[vec_idx] << " & ";
    rel_table << syst_in_err_rel[vec_idx] << " & " << syst_ex_err_rel[vec_idx] << " & ";
    rel_table << total_err_rel[vec_idx] << "\\\\ \\hline\n";
  }
  rel_table << "\\end{tabular}\n\\caption{Relative Errors}\n\\end{table}\n\\end{document}";
  rel_table.close();

  std::ofstream abs_table( "table_abs.tex" );
  abs_table << "\\documentclass{article} \n\\begin{document}";
  abs_table << std::fixed << std::setprecision( 0 );
  abs_table << "\\begin{table}[h!] \n\\centering \\begin{tabular}{| c | r | r r r | r | r |} \n \\hline\n";
  abs_table << "Bin & Yield & statistical & systematic & external & total & total (\\%) \\\\ \\hline\n";
  for ( int vec_idx = 0; vec_idx < stat->GetNbinsX(); vec_idx++ ){
    abs_table << ana_names[vec_idx].substr(0, ana_names[vec_idx].find( '-' ) ) << " & " << cntr_val[vec_idx] << " & " << stat_err[vec_idx] << " & ";
    abs_table << syst_in_err[vec_idx] << " & " << syst_ex_err[vec_idx] << " & ";
    abs_table << total_err[vec_idx] << "&" << total_err_rel[vec_idx]*100.0 << "\\\\ \\hline\n";
  }
  abs_table << "\\end{tabular}\n\\caption{Absolute Errors}\n\\end{table}\n\\end{document}";
  abs_table.close();

}


void full_result( YAML::Node run_node, variable_set & variables ){

  prep_style();  

  /* what do we want?
   * We want to have the baseline
   * the nosys
   * the relative error of systematics
  */ 

  // baseline with error  
  std::string baseline_unique = run_node[ "baseline_unique" ].as<std::string>();
  std::string statonly_unique = run_node[ "statonly_unique" ].as<std::string>();
  std::string syststat_unique = run_node[ "syststat_unique" ].as<std::string>();
  std::string sf_unique, sf_filepath;
  if ( run_node[ "input_unique" ] ){ 
      sf_unique = run_node[ "input_unique" ].as< std::string >();
      sf_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
        + "/scalefactors/" 
        + sf_unique;
  }
  std::string eff_unique, eff_filepath;
  if ( run_node[ "efficiency_unique" ] ){ 
      eff_unique = run_node[ "efficiency_unique" ].as< std::string >();
      eff_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
        + "/trees/" + eff_unique
        + "/efficiency/sign_efficiencies_" 
        + eff_unique + ".root";
  }


  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( baseline_unique );
  fileset->load_subtraction_fileset( sf_filepath, sf_unique, true );
  fileset->load_efficiency_fileset( eff_filepath, eff_unique );

  std::vector< TH1F * > sys_errors = {};
  std::vector< std::string > systematic_groups = run_node[ "systematic_uniques" ].as< std::vector<std::string>>();
  std::transform( systematic_groups.begin(), systematic_groups.end(), std::back_inserter( sys_errors ),
    [ &sys_errors, &run_node, &syststat_unique, &variables]( const std::string & group_name ){
      return systematic( syststat_unique, group_name, 
                         run_node[group_name].as<std::vector<std::string>>(), 
                         variables, true );
    }
  );

  auto[ baseline_total, baseline_extracted ] = get_result( baseline_unique, variables );
  auto[ statonly_total, statonly_extracted ] = get_result( statonly_unique, variables );
  auto[ syststat_total, syststat_extracted ] = get_result( syststat_unique, variables );


  TH1F * statonly_sign = statonly_extracted.sign_hist;
  TH1F * baseline_sign = baseline_extracted.sign_hist;
  TH1F * syststat_sign = syststat_extracted.sign_hist;
 
  //int vec_idx = 0;
  for ( int bin_idx = 1, vec_idx = 0; bin_idx <= syststat_sign->GetNbinsX(); bin_idx++, vec_idx++ ){
    
    double be = baseline_sign->GetBinError( bin_idx );
    double ex_sys = 0;
    for ( TH1F * sys_err : sys_errors ){
      ex_sys += sys_err->GetBinContent( bin_idx )*sys_err->GetBinContent( bin_idx );
    }
    double total_bin_error = std::sqrt( be*be + ex_sys );
    syststat_sign->SetBinError( bin_idx, total_bin_error );
 
  }

  make_table( statonly_sign, baseline_sign, syststat_sign, variables );

  statonly_sign->SetLineColorAlpha( kRed+1, 1.0 );
  baseline_sign->SetLineColorAlpha( kRed+1, 1.0 );
  syststat_sign->SetLineColorAlpha( kRed+2, 1.0 );
  statonly_sign->SetLineStyle( 2.0 );
  baseline_sign->SetLineStyle( 1.0 );
  syststat_sign->SetLineStyle( 1 );


  TH1F * efficiency = fileset->get_efficiency( variables, "qtB" );
  TCanvas * effcanv = new TCanvas( "effc", "", 200, 200, 1000, 1000  );
  effcanv->Divide( 1, 1 );
  effcanv->cd( 1 );
  efficiency->GetYaxis()->SetRangeUser( 0, 1.0 );
  efficiency->Draw( "HIST E1" );
  effcanv->SaveAs( "effc.png" );



  TH1F * lastplot_sign = static_cast<TH1F*>( syststat_sign->Clone("final") );
  lastplot_sign->Divide( syststat_sign, efficiency, 1.0, 1.0 );
  TF1 * dg = prep_dg();
  dg->SetRange( -10, 20 );
  align_dg( dg, lastplot_sign, false );
  dg->SetLineColor( kBlack );
  dg->SetNpx( 1000 );

  //TF1 * sg = prep_sg();
  //sg->SetRange( -10, 20);
  //align_sg( sg, lastplot_sign );
  //sg->SetLineColor( kGreen );
  //sg->SetNpx( 1000 );

  TCanvas * summary_canvas = new TCanvas( "summary", "", 200, 200, 2000, 1000 );
  summary_canvas->Divide( 2, 1 );

  gStyle->SetOptStat( "krimes" );
  TPad * active_pad = static_cast<TPad *>( summary_canvas->cd( 1 ) );
  baseline_sign->Draw( "HIST E1" );
  baseline_sign->SetStats( false );
  statonly_sign->Draw( "HIST E1 SAME" );
  statonly_sign->SetStats( false );
  syststat_sign->SetStats( true );
  syststat_sign->Draw( "HIST E1 SAMES" );
  hist_prep_axes( baseline_sign );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( baseline_sign, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Nominal, stat+sys error", true );
  baseline_sign->GetYaxis()->SetRangeUser( 0, baseline_sign->GetMaximum()*1.2 );
  TLegend * pc_legend = below_logo_legend();
  pc_legend->AddEntry( statonly_sign, "stat" );
  pc_legend->AddEntry( baseline_sign, "stat+sys" );
  pc_legend->AddEntry( syststat_sign, "stat+sys+sys_ex" );
  pc_legend->Draw( "SAME" );
  TPaveStats * syststats = make_stats( syststat_sign );
  syststats->Draw( "SAME" );

  active_pad = static_cast<TPad *>( summary_canvas->cd( 2 ) );
  lastplot_sign->Draw( "HIST E1" );
  lastplot_sign->SetStats( true );
  lastplot_sign->Fit( dg, "MS", "", -10, 20 );
  dg->Draw( "SAME" );
  hist_prep_axes( lastplot_sign );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( lastplot_sign, "q_{T}^{A}", "Yield" );
  add_pad_title( active_pad, "Total, efficiency corrected", true );  summary_canvas->SaveAs( "Extraction.png" );
  lastplot_sign->GetYaxis()->SetRangeUser( 0, lastplot_sign->GetMaximum()*1.2 );
  TLegend * eff_legend = below_logo_legend();
  eff_legend->AddEntry( lastplot_sign, "eff comp" );
  eff_legend->Draw( "SAME" );                                     
  TPaveStats * stats = make_stats( lastplot_sign );
  stats->Draw( "SAME" );

  summary_canvas->SaveAs( "Extraction.png" );
  summary_canvas->SaveAs( "Extraction.pdf" );


}

  //bound & ana_bound = variables.analysis_bound;                                          
  //TH1F * mu_calc = ana_bound.get_hist();                                                 
  //// parse yield effects                                                                 
  //std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + base_unique + "/";
  //TH1F * photon_sf_signal = variables.analysis_bound.get_hist( "psfs_" + base_unique );  
  //TH1F * muon_sf_signal = variables.analysis_bound.get_hist( "msrs_" + base_unique );    
  //TH1F * muon_reco_signal = variables.analysis_bound.get_hist( "mres_" + base_unique );  
  //TH1F * photon_sf_control = variables.analysis_bound.get_hist( "psfc_" + base_unique ); 
  //TH1F * muon_sf_control = variables.analysis_bound.get_hist( "msrc_" + base_unique );   
  //TH1F * muon_reco_control = variables.analysis_bound.get_hist( "mrec_" + base_unique ); 

  //// Needs fleshed out just a little
  //double sign_yield = sign_extracted->Integral();
  //double data_yield = baseline_extracted.data_hist->Integral();
  //double mu = ( data_yield != 0 ) ? sign_yield/data_yield : 0.0;
  //mu_calc->SetBinContent( ana_idx+1, mu );
  
  //const std::string & qta_name = ana_series_names[ ana_idx ];
  //std::string trex_qta_path = trex_path + qta_name + "/" + base_unique + "/";
  //std::string plots_path = trex_qta_path + "Plots/";
  //std::string table_path = trex_qta_path + "Tables/";

  //std::vector<double> signal_effects = get_effects( table_path + "/SIGNAL_syst_postFit.txt" );
  //std::vector<double> control_effects = get_effects(table_path + "/CONTROL_syst_postFit.txt" );

  //photon_sf_signal->SetBinContent( ana_idx+1,   signal_effects.at( 0 ) );
  //muon_sf_signal->SetBinContent( ana_idx+1,     signal_effects.at( 1 ) );
  //muon_reco_signal->SetBinContent( ana_idx+1,   signal_effects.at( 2 ) );
  //photon_sf_control->SetBinContent( ana_idx+1,  control_effects.at( 0 ) );
  //muon_sf_control->SetBinContent( ana_idx+1,    control_effects.at( 1 ) );
  //muon_reco_control->SetBinContent( ana_idx+1,  control_effects.at( 2 ) );

  //photon_sf_signal->SetBinError( ana_idx+1, 0 );
  //muon_sf_signal->SetBinError( ana_idx+1, 0 );
  //muon_reco_signal->SetBinError( ana_idx+1, 0 );
  //photon_sf_control->SetBinError( ana_idx+1, 0 );
  //muon_sf_control->SetBinError( ana_idx+1, 0 );
  //muon_reco_control->SetBinError( ana_idx+1, 0 );


//TH1F * sign_extracted_nosys_copy = static_cast<TH1F *>( sign_extracted_nosys->Clone() );


  //TCanvas * rel_canv = new TCanvas( "", "", 200, 200, 4000, 2000 );
  //rel_canv->Divide( 4, 2 );

  //TPad * active_pad = static_cast<TPad *>( rel_canv->cd( 1 ) );
  //sign_extracted->Draw( "HIST E1" );
  //sign_extracted_nosys_copy->Draw( "HIST E1 SAME" );
  //sign_extracted_nosys_copy->SetLineColorAlpha( kRed+1, 1.0 );
  //sign_extracted_nosys_copy->SetLineStyle( 2 );
  //hist_prep_axes( sign_extracted );
  //set_axis_labels( sign_extracted, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "Extracted" );

  //active_pad = static_cast<TPad *>( rel_canv->cd( 2 ) );
  //photon_sf_signal->Draw( "HIST E1" );
  //hist_prep_axes( photon_sf_signal );
  //set_axis_labels( photon_sf_signal, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "photon sf signal" );

  //active_pad = static_cast<TPad *>( rel_canv->cd( 3 ) );
  //muon_sf_signal->Draw( "HIST E1" );
  //hist_prep_axes( muon_sf_signal );
  //set_axis_labels( muon_sf_signal, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "muon sf signal" );

  //active_pad = static_cast<TPad *>( rel_canv->cd( 4 ) );
  //muon_reco_signal->Draw( "HIST E1" );
  //hist_prep_axes( muon_reco_signal );
  //set_axis_labels( muon_reco_signal, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "muon reco signal" );

  //active_pad = static_cast<TPad *>( rel_canv->cd( 5 ) );
  //sign_extracted_nosys->Draw( "HIST E1" );
  //hist_prep_axes( sign_extracted_nosys );
  //set_axis_labels( sign_extracted_nosys, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "nosys" );

  //active_pad = static_cast<TPad *>( rel_canv->cd( 6 ) );
  //photon_sf_control->Draw( "HIST E1" );
  //hist_prep_axes( photon_sf_control );
  //set_axis_labels( photon_sf_control, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "photon sf control" );

  //active_pad = static_cast<TPad *>( rel_canv->cd( 7 ) );
  //muon_sf_control->Draw( "HIST E1" );
  //hist_prep_axes( muon_sf_control );
  //set_axis_labels( muon_sf_control, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "muon sf control" );

  //active_pad = static_cast<TPad *>( rel_canv->cd( 8 ) );
  //muon_reco_control->Draw( "HIST E1" );
  //hist_prep_axes( muon_reco_control );
  //set_axis_labels( muon_reco_control, "q_{T}^{A}", "Relative Effect" );
  //add_pad_title( active_pad, "muon reco control" );

  //rel_canv->SaveAs( "relatives.png" );
