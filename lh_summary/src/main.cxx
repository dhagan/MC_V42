#include <main.hxx>
#include <funcs.hxx>
#include <systematic.hxx>
#include <full_result.hxx>
#include <validation.hxx>
#include <compare.hxx>
#include <pulls.hxx>
#include <exrat.hxx>
#include <correlation.hxx>
#include <stat_check.hxx>

#include <yaml-cpp/yaml.h>

int help(){
  std::cout << " Usage:" << std::endl;
  std::cout << " ./gen --input,-i INPUT_PATH" << std::endl;
  std::cout << " " << std::endl;
  return 0;
}
int main( int argc, char * argv[] ){

  prep_style();
  //gStyle->SetPalette( kGreyScale );
  //TColor::InvertPalette();

  int option{0}, option_index{0};

  static struct option long_options[] = {
      { "help",        no_argument,              0,      'h'},
      { "input",       required_argument,        0,      'i'},
      {0,             0,                        0,      0}
    };

  std::string input = "";
  do {
    option = getopt_long(argc, argv, "i:ph", long_options,&option_index);
    switch (option){
      case 'h':
        return help();
      case 'i': 
        input         = std::string( optarg );
        break;
    }
  } while (option != -1);

  if ( input.empty() ){
    std::cout << "No input, exiting..." << std::endl;
    return 0;
  }

  YAML::Node config = YAML::LoadFile( input );
  std::vector< std::string > run_list = config["general"]["runs"].as< std::vector<std::string > >();

  for ( std::string & run : run_list ){

    YAML::Node run_node = config[ run ];

    std::vector< std::string > modes = run_node[ "modes" ].as< std::vector< std::string > >();
    if ( modes.size() == 0 ){ std::cout << "No modes provided, exiting..." << std::endl; }

    std::string avar = "qtA", svar = "BDT", cuts = "";
    std::string range = "", bounds_path = "";
    if ( run_node[ "avar" ] ){ avar = run_node["avar"].as< std::string >(); }
    if ( run_node[ "svar" ] ){ svar = run_node["svar"].as< std::string >(); }
    if ( run_node[ "ranges" ] ){ range = run_node["ranges"].as< std::string >(); }
    if ( run_node[ "bounds_path" ] ){ range = std::string( std::getenv( "LIB_PATH" ) ) 
                                              + "/share/" 
                                              + run_node["bounds_path"].as< std::string >(); }

    variable_set variables = variable_set( bounds_path, range, avar, svar );
    variables.set_extra_bounds( cuts );

    for ( std::string & mode : modes ){

      if ( mode.find( "systematic" ) != std::string::npos ){
        systematic( run_node, variables );
        continue;
      }
      if ( mode.find( "validation" ) != std::string::npos ){
        validation( run_node, variables );
        continue;
      }
      if ( mode.find( "qdv" ) != std::string::npos ){
        qdv( run_node );
        continue;
      }
      if ( mode.find( "full" ) != std::string::npos ){ 
        full_result( run_node, variables );
        continue;
      }
      if ( mode.find( "compare" ) != std::string::npos ){ 
        compare( run_node, variables );
        continue;
      }
      if ( mode.find( "pull" ) != std::string::npos ){ 
        pulls( run_node, variables );
        continue;
      }
      if ( mode.find( "exrat" ) != std::string::npos ){ 
        exrat( run_node, variables );
        continue;
      }
      if ( mode.find( "correlation" ) != std::string::npos ){ 
        correlation( run_node, variables );
        continue;
      }
      if ( mode.find( "stat_check" ) != std::string::npos ){ 
        stat_check( run_node, variables );
        continue;
      }
    }
  }
 
  return 0;

}

//else if ( mode.find( "sr" ) != std::string::npos ){
//
//  if ( avar.empty() || svar.empty() ){ parse_filename( input, avar, svar ); }
//  bound_mgr * selections = new bound_mgr();
//  selections->load_bound_mgr( bounds_path );
//  if ( !range.empty() ){ selections->process_bounds_string( range ); }
//  generate_sr( input, avar, svar, cuts, output_unique, selections );  
