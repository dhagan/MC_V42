#include <exrat.hxx>
#include <sys/stat.h>

void exrat( YAML::Node & run_node, variable_set & variables ){

  prep_style();

  bound & ana_bound = variables.analysis_bound;

  std::string num_unique = run_node[ "num_unique" ].as<std::string>();
  std::string den_unique = run_node[ "den_unique" ].as<std::string>();
  std::string eff_unique = run_node[ "eff_unique" ].as<std::string>();

  std::string dir_name = "./" + num_unique + "_" + den_unique;
  mkdir( dir_name.c_str(), 744 );
  chdir( dir_name.c_str() );


  std::string eff_filepath = std::string( std::getenv( "OUT_PATH" ) ) 
                           + "/trees/" + eff_unique
                           + "/efficiency/sign_efficiencies_" 
                           + eff_unique + ".root";
  basic_fileset * fileset = new basic_fileset();
  fileset->set_unique( eff_unique );
  fileset->load_efficiency_fileset( eff_filepath, eff_unique );

  auto[ num_total, num_extr ] = get_result( num_unique, variables );
  auto[ den_total, den_extr ] = get_result( den_unique, variables );

  TH1F * num_sign = num_extr.sign_hist; 
  TH1F * den_sign = den_extr.sign_hist; 
  TH1F * rat_sign = static_cast<TH1F *>( num_sign->Clone() );
  rat_sign->Divide( num_sign, den_sign, 1.0, 1.0 );
  TH1F * dif_sign = static_cast<TH1F *>( num_sign->Clone() );
  dif_sign->Add( num_sign, den_sign, 1.0, -1.0 );

  num_sign->SetLineColor( kRed+1 );
  den_sign->SetLineColor( kRed+1 );
  num_sign->SetLineStyle( 2 );
  den_sign->SetLineStyle( 1 );

  TCanvas * canvas = new TCanvas( "canv", "", 200, 200, 3000, 1000 );
  canvas->Divide( 3, 1 );

  add_pad( num_sign, "Extracted", "Yield", ana_bound );
  den_sign->Draw( "HIST SAME E1" );
  TLegend * comp_leg = below_logo_legend();
  comp_leg->AddEntry( num_sign, num_unique.c_str() );
  comp_leg->AddEntry( den_sign, den_unique.c_str() );
  comp_leg->Draw();
  add_pad( rat_sign, "Ratio", "Ratio", ana_bound );
  add_pad( dif_sign, "Difference", "#Delta", ana_bound );
  dif_sign->GetYaxis()->SetRangeUser( -dif_sign->GetMaximum()*1.3 , dif_sign->GetMaximum()*1.3 );
  canvas->SaveAs( "comparison.png" );

  delete canvas;
  num_extr.erase();
  den_extr.erase();
  delete num_total;
  delete den_total;
  delete rat_sign;
  delete dif_sign;

  chdir( ".." );

}
