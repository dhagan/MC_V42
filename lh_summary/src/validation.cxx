#include <validation.hxx>
#include <sstream>
#include <sys/stat.h>

enum val_type{ val_sign, val_bckg, val_data, val_yield };

std::unordered_map< val_type, std::string > val_string = { {val_sign, "Samples"}, {val_bckg, "Samples" }, 
                                                           {val_data, "Data"}, { val_yield, "Total" } };
std::unordered_map< val_type, int > val_idx = { {val_sign, 0}, {val_bckg, 1 }, { val_data, 0 }, { val_yield, 0 } };
std::unordered_map< val_type, int > val_color = { {val_sign, kRed+1}, {val_bckg, kBlue+1 }, { val_data, kBlack }, { val_yield, kGreen+1} };
std::unordered_map< val_type, int > val_style= { {val_sign, 1}, {val_bckg, 1 }, { val_data, 1 }, { val_yield, 2 } };
std::unordered_map< val_type, std::string > val_name = { {val_sign, "sign"}, {val_bckg, "bckg" }, 
                                                           {val_data, "data"}, { val_yield, "fit" } };


std::vector< val > create_vals( YAML::Node & run_node, const std::vector<std::string> & val_vars, variable_set & variables );

void quick_style( TH1F * hist, val_type type );

void fill_val( val & validation, variable_set & variables, const std::string & unique, val_type type );

template <typename T>
std::string tswp(const T a_value, const int n = 6){
  std::ostringstream out;
  out.precision(n);
  out << std::fixed << a_value;
  return std::move(out).str();
}

std::vector< val > create_vals( YAML::Node & run_node, const std::vector<std::string> & val_vars, variable_set & variables ){
  std::vector< val > validations = {};
  std::transform( val_vars.cbegin(), val_vars.cend(), std::back_inserter( validations ),
    [ &variables, &run_node ]( const std::string & variable ){
      return val( variable, run_node, variables );
    }  
  );
  return validations;
}

void quick_style( TH1F * hist, val_type type ){
  hist->SetLineColorAlpha( val_color[type], 1.0 );
  hist->SetLineStyle( val_style[type] );
  hist->SetLineWidth( 1 );
}


void fill_val( val & validation, variable_set & variables, const std::string & unique, val_type type ){

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string config_path =  std::string( std::getenv("ANA_IP") ) + "/lh_summary/share/validate";
  
  size_t min_bins = 0;
  size_t max_bins = variables.analysis_bound.get_bins();
  std::vector< std::string > ana_series_names = variables.analysis_bound.get_series_names();

  for ( size_t ana_idx = min_bins; ana_idx < max_bins; ana_idx++ ){

    const std::string & qta_name = ana_series_names[ ana_idx ];
    std::string val_path = trex_path + qta_name + "/" + unique + "/Plots";
    val_path += "/VALIDATION" + std::to_string( validation.index ) + "_postfit.yaml";
    YAML::Node val_node = YAML::LoadFile( val_path );
    std::vector< double > yields = val_node[val_string[type]][val_idx[type]]["Yield"].as<std::vector<double>>();
    std::vector< double > errors;
    if ( type == val_yield ){ errors = val_node[val_string[type]][val_idx[type]]["UncertaintyUp"].as<std::vector<double>>(); }
    for ( int bin_idx = 0; bin_idx < validation.hist->GetNbinsX(); bin_idx++ ){
      validation.hist->AddBinContent( bin_idx+1, yields[bin_idx] );
      if (type == val_yield ){
        validation.hist->SetBinError( bin_idx+1, validation.hist->GetBinError(bin_idx+1) + (errors[bin_idx]*errors[bin_idx]) );
      }
    }
  }
  for ( int bin_idx = 0; bin_idx < validation.hist->GetNbinsX(); bin_idx++ ){
    validation.hist->SetBinError( bin_idx+1, std::sqrt( validation.hist->GetBinError(bin_idx+1) ) );
  }
}

void plot_validation( val & validation, val_type type ){
  TH1F * val_hist = validation.hist;
  TCanvas * val_canv = new TCanvas( "val_canv", "", 200,200, 1000, 1000 );
  val_canv->Divide( 1, 1 );
  TPad * active_pad = static_cast<TPad *>( val_canv->cd(1) );
  val_hist->Draw( "HIST E1");
  hist_prep_axes( val_hist );
  add_atlas_decorations( active_pad, true );
  set_axis_labels( val_hist, val_hist->GetName(), "Yield" );
  add_pad_title( active_pad, val_hist->GetName() );
  TLegend * val_legend = below_logo_legend();
  val_legend->AddEntry( val_hist, "sign", "lep" );
  val_legend->Draw( "SAME" );
  TPaveStats * val_stats = make_stats( val_hist );
  val_stats->Draw( "SAME" );
  std::string output_name = std::string( validation.var_bound.get_name() ) + "_validation_" + val_name[type] + ".png";
  val_canv->SaveAs( output_name.c_str() );
  delete val_canv;
}

void ratio_variations( TH1F * original, const std::string & val_name, double value=1.0 ){
  for ( int bin_idx = 1; bin_idx <= original->GetNbinsX(); bin_idx++ ){
    std::string modified_name = std::string(original->GetName()) + "_" + val_name + "_" + std::to_string( bin_idx );
    TH1F * modified = static_cast<TH1F *>( original->Clone( modified_name.c_str() ) );
    modified->SetBinContent( bin_idx, value*modified->GetBinContent( bin_idx ) );
    modified->Write( modified_name.c_str() );
  }
}

void closure( TH1F * extracted, TH1F * input, bound & val_bound, sample_type stype ){

  gStyle->SetOptStat( "" );
  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  extracted->SetLineStyle( 2 );

  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );

  input->Draw( "HIST E1" ); 
  extracted->Draw( "HIST E1 SAME" ); 
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( input, val_bound.get_ltx(), "");
  hist_prep_axes( input, true );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( input, "Input", "l" );
  legend->AddEntry( extracted, "Validation", "l" );
  legend->Draw( "SAME" );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );
  std::string name = val_bound.get_name() + "_" + type_str[stype] + "_closure.pdf";
  canv.SaveAs( name.c_str() );
  canv.Clear();

  input->SetLineStyle( 2 );
  gStyle->SetOptStat( "mirse" );
}

void closure_yield( TH1F * extracted, TH1F * input, bound & val_bound, sample_type stype ){

  gStyle->SetOptStat( "" );
  TCanvas canv( "canv", "", 200, 200, 2000, 1000 );
  canv.Divide( 2, 1 );

  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );

  input->Draw( "HIST E1" ); 
  extracted->Draw( "HIST E1 SAME" ); 
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( input, val_bound.get_ltx(), "");
  hist_prep_axes( input );
  TLegend * legend = below_logo_legend();
  legend->AddEntry( input, "Subtracted Data", "l" );
  legend->AddEntry( extracted, "Extracted Yield", "l" );
  legend->Draw( "SAME" );
  input->GetYaxis()->SetRangeUser( 100, 10e5 );
  extracted->GetYaxis()->SetRangeUser( 100, 10e5 );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  active_pad->SetLogy( 1 );

  TH1F * ratio = static_cast<TH1F *>( input->Clone() );
  ratio->Reset();
  ratio->Divide( input, extracted, 1.0, 1.0 );

  active_pad = static_cast<TPad *>( canv.cd( 2 ) );
  ratio->Draw( "HIST E1" );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( ratio, val_bound.get_ltx(), "");
  hist_prep_axes( ratio );
  TLegend * legend3 = below_logo_legend();
  legend3->AddEntry( ratio, "Ratio, Data/Yield", "l" );
  legend3->Draw( "SAME" );
  ratio->GetYaxis()->SetRangeUser( 0.0, 2.0 );
  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );


  canv.SetFillStyle( 4000 );
  std::string name = val_bound.get_name() + "_" + type_str[stype] + "_closure_yield.pdf";
  canv.SaveAs( name.c_str() );
  canv.Clear();
  //save_and_clear( active_pad, canv, val_bound.get_name() + "_" + type_str[stype] + "_closure.pdf" );

  TH1F * closure_weights = static_cast<TH1F *>( ratio->Clone() );
  closure_weights->Reset();
  for ( int bin = 1; bin <= ratio->GetNbinsX(); bin++ ){ 
    closure_weights->SetBinContent( bin, ratio->GetBinContent( bin ) );
    closure_weights->SetBinError( bin, ratio->GetBinError( bin ) );
  }
  closure_weights->GetYaxis()->SetRangeUser( 0, 2.0 );
  closure_weights->GetYaxis()->SetRange( 0, 2.0 );

  std::string cwf_name = "./cwf_" + val_bound.get_name() + ".root";
  TFile * closure_weight_file = new TFile( cwf_name.c_str(), "RECREATE" );
  closure_weights->Write( "cw" );
  closure_weight_file->Close();

  gStyle->SetOptStat( "mirse" );
}

#include <sys/stat.h>
void qdv( YAML::Node & run_node ){

  mkdir( "./qdv", 744 );
  chdir( "./qdv" );

  std::string b_unique = run_node[ "before" ].as<std::string>();
  std::string a_unique = run_node[ "after" ].as<std::string>();

  std::string b_path = std::string( std::getenv( "OUT_PATH" ) ) + "/lh_summary/validate/"
    + b_unique + "/cwf_PhotonPt.root";
  std::string a_path = std::string( std::getenv( "OUT_PATH" ) ) + "/lh_summary/validate/"
    + a_unique + "/cwf_PhotonPt.root";

  TFile * a_file = new TFile( a_path.c_str(), "READ" );
  TFile * b_file = new TFile( b_path.c_str(), "READ" );

  TH1F * a_closure = static_cast<TH1F *>( a_file->Get( "cw" ) );
  TH1F * b_closure = static_cast<TH1F *>( b_file->Get( "cw" ) );
  TH1F * flat = static_cast<TH1F *>( b_closure->Clone( "flat" ) );
  flat->Reset();
  for ( int bin = 1; bin <= flat->GetNbinsX(); bin++ ){
    flat->SetBinContent( bin, 1.0 );
  }


  a_closure->SetLineStyle( 2 );
  b_closure->SetLineStyle( 1 );
  a_closure->SetLineColor( kRed+2 );
  b_closure->SetLineColor( kBlue+2 );
  flat->SetLineStyle( 1 );
  flat->SetLineColor( kBlack );

  gStyle->SetOptStat( "" );

  TCanvas canv( "canv", "", 200, 200, 1000, 1000 );
  canv.Divide( 1, 1 );

  TPad * active_pad = static_cast<TPad *>( canv.cd( 1 ) );
  a_closure->Draw( "HIST E1" );
  b_closure->Draw( "HIST E1 SAME");
  flat->Draw( "HIST SAME" );
  add_atlas_decorations( active_pad, false );
  add_internal( active_pad );
  set_axis_labels( a_closure, "p_{T}(#gamma)", "Closure weight");
  hist_prep_axes( a_closure );
  TLegend * legend3 = below_logo_legend();
  legend3->AddEntry( b_closure, "Weight, before", "l" );
  legend3->AddEntry( a_closure, "Weight, after", "l" );
  legend3->Draw( "SAME" );
  a_closure->GetYaxis()->SetRangeUser( 0.0, 2.0 );

  active_pad->SetBottomMargin( 0.1 );
  active_pad->SetFillStyle( 4000 );
  canv.SetFillStyle( 4000 );

  std::string qdv_name = "qdv_" + b_unique + "_to_" + a_unique + ".pdf";

  canv.SaveAs( qdv_name.c_str() );
  gStyle->SetOptStat( "mirse" );

  chdir( ".." );

}

void validation( YAML::Node & run_node, variable_set & variables ){

  std::string unique = run_node[ "unique" ].as<std::string>();
  std::string output_dir = "./" + unique;
  if ( access( output_dir.c_str(), 744 ) != 0 ){
    mkdir( output_dir.c_str(), 744 );
  }
  if ( chdir( output_dir.c_str() ) == -1 ){
    std::cout << errno << std::endl;
  };

  std::string data_scale = "1.0";
  if ( run_node[ "data_scale" ] ){
    data_scale = run_node["data_scale"].as<std::string>();
  }

  std::string forced_region = "1.0";
  if ( run_node["force_region"] ){
    region forced = region_convert( run_node["force_region"].as<std::string>() );
    forced_region = region_cuts[ forced ];
  }

  std::string trex_path = std::getenv( "OUT_PATH" ) + std::string( "/trex/" ) + unique + "/";
  std::string config_path =  std::string( std::getenv("ANA_IP") ) + "/lh_summary/share/validate";
  std::vector< std::string > val_vars = run_node[ "validation_variables" ].as<std::vector<std::string>>();

  std::string data_weight   = "(subtraction_weight*" + data_scale + ")"
                              "*((Lambda>=25.00000)&&(Lambda<=200.00000)&&(abs(qtB)>=0.00000)&&(abs(qtB)<=6.00000)&&("+forced_region+"))";
  std::string mc_weight     = "(subtraction_weight*photon_sf*muon_sf*pu_weight)"
                              "*((Lambda>=25.00000)&&(Lambda<=200.00000)&&(abs(qtB)>=0.00000)&&(abs(qtB)<=6.00000)&&("+forced_region+"))";

  std::string fs_unique = run_node[ "fs_unique" ].as<std::string>();
  std::string fs_path = std::string( std::getenv( "OUT_PATH" ) ) + "/scalefactors/" + fs_unique;
  basic_fileset fileset( unique ); 
  fileset.load_subtraction_fileset( fs_path, fs_unique, true );

  std::vector< val > sign_vals  = create_vals( run_node, val_vars, variables );
  std::vector< val > bckg_vals  = create_vals( run_node, val_vars, variables );
  std::vector< val > data_vals  = create_vals( run_node, val_vars, variables );
  std::vector< val > yield_vals = create_vals( run_node, val_vars, variables );
  std::vector< val > sign_mcs   = create_vals( run_node, val_vars, variables );
  std::vector< val > bckg_mcs   = create_vals( run_node, val_vars, variables );
  std::vector< val > data_mcs   = create_vals( run_node, val_vars, variables );

  for ( val & mc : sign_mcs ){
    std::string draw_expression = mc.var_bound.get_var() + ">>" + mc.hist->GetName();
    fileset.sign_tree->Draw( draw_expression.c_str(), mc_weight.c_str(), "e goff" ); 
  }
  for ( val & mc : bckg_mcs ){
    std::string draw_expression = mc.var_bound.get_var() + ">>" + mc.hist->GetName();
    fileset.bckg_tree->Draw( draw_expression.c_str(), mc_weight.c_str(), "e goff" ); 
  }
  for ( val & data : data_mcs ){
    std::string draw_expression = data.var_bound.get_var() + ">>" + data.hist->GetName();
    fileset.data_tree->Draw( draw_expression.c_str(), data_weight.c_str(), "e goff" ); 
  }

  std::for_each( sign_vals.begin(), sign_vals.end(), [ &variables, &unique ]( val & validation ){ fill_val( validation, variables, unique, val_sign ); } );
  std::for_each( bckg_vals.begin(), bckg_vals.end(), [ &variables, &unique ]( val & validation ){ fill_val( validation, variables, unique, val_bckg ); } );
  std::for_each( data_vals.begin(), data_vals.end(), [ &variables, &unique ]( val & validation ){ fill_val( validation, variables, unique, val_data ); } );
  std::for_each( yield_vals.begin(), yield_vals.end(), [ &variables, &unique ]( val & validation ){ fill_val( validation, variables, unique, val_yield ); } );

  prep_style();
  gStyle->SetOptStat( "rmien" );
  std::for_each( sign_vals.begin(), sign_vals.end(), []( val & sval ){ plot_validation( sval, val_sign ); } );
  std::for_each( bckg_vals.begin(), bckg_vals.end(), []( val & sval ){ plot_validation( sval, val_bckg ); } );
  std::for_each( data_vals.begin(), data_vals.end(), []( val & sval ){ plot_validation( sval, val_data ); } );
  std::for_each( yield_vals.begin(), yield_vals.end(), []( val & sval ){ plot_validation( sval, val_yield ); } );
  std::for_each( sign_vals.begin(), sign_vals.end(), []( val & sval ){   quick_style( sval.hist, val_sign ); } );
  std::for_each( bckg_vals.begin(), bckg_vals.end(), []( val & sval ){   quick_style( sval.hist, val_bckg ); } );
  std::for_each( data_vals.begin(), data_vals.end(), []( val & sval ){   quick_style( sval.hist, val_data ); } );
  std::for_each( yield_vals.begin(), yield_vals.end(), []( val & sval ){ quick_style( sval.hist, val_yield ); } );
  std::for_each( sign_mcs.begin(), sign_mcs.end(), []( val & sval ){   quick_style( sval.hist, val_sign ); } );
  std::for_each( bckg_mcs.begin(), bckg_mcs.end(), []( val & sval ){   quick_style( sval.hist, val_bckg ); } );
  std::for_each( data_mcs.begin(), data_mcs.end(), []( val & sval ){   quick_style( sval.hist, val_data ); } );

  TFile * ratios_store = new TFile( "./ratios.root", "RECREATE" );

  gStyle->SetOptStat( "mirse" );
  for ( size_t val_idx = 0; val_idx < sign_vals.size(); val_idx++ ){

    bound & val_bound = sign_vals[val_idx].var_bound;
    std::string val_name = val_bound.get_name();
    TCanvas * val_canv = new TCanvas( val_name.c_str(), "", 200, 200, 2000, 3000 );
    val_canv->Divide( 2, 3 );

    TPad * active_pad = static_cast<TPad *>( val_canv->cd( 1 ) );
    yield_vals[val_idx].hist->Draw( "HIST" );
    sign_vals[val_idx].hist->Draw( "HIST SAME" );
    bckg_vals[val_idx].hist->Draw( "HIST SAME" );
    data_vals[val_idx].hist->Draw( "HIST SAME" );
    hist_prep_axes( yield_vals[val_idx].hist, true );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( yield_vals[val_idx].hist, val_bound.get_ltx(), "Yield" );
    add_pad_title( active_pad, "Extracted Validation" );
    TLegend * val_ex_legend = below_logo_legend();
    val_ex_legend->AddEntry( yield_vals[val_idx].hist, "Fit", "l" );
    val_ex_legend->AddEntry( sign_vals[val_idx].hist, "sign", "l" );
    val_ex_legend->AddEntry( bckg_vals[val_idx].hist, "bckg", "l" );
    val_ex_legend->AddEntry( data_vals[val_idx].hist, "data", "l" );
    val_ex_legend->Draw( "SAME" );
    TPaveStats * data_ex_stat = make_stats( yield_vals[val_idx].hist );
    data_ex_stat->Draw( "SAME" );

    TH1F * data_mc = static_cast<TH1F *>( (data_mcs[val_idx].hist)->Clone() ) ;
    TH1F * sign_mc = static_cast<TH1F *>( (sign_mcs[val_idx].hist)->Clone() ) ;
    TH1F * bckg_mc = static_cast<TH1F *>( (bckg_mcs[val_idx].hist)->Clone() ) ;

    active_pad = static_cast<TPad *>( val_canv->cd( 2 ) );
    data_mc->Draw( "HIST" );
    sign_mc->Draw( "HIST SAME" );
    bckg_mc->Draw( "HIST SAME" );
    hist_prep_axes( data_mc, true );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( data_mc, val_bound.get_ltx(), "Yield" );
    add_pad_title( active_pad, "Input Validation" );
    TLegend * val_mc_legend = below_logo_legend();
    val_mc_legend->AddEntry( data_mc, "data", "l" );
    val_mc_legend->AddEntry( sign_mc, "sign", "l" );
    val_mc_legend->AddEntry( bckg_mc, "bckg", "l" );
    val_mc_legend->Draw( "SAME" );
    TPaveStats * data_mc_stat = make_stats( data_mc );
    data_mc_stat->Draw( "SAME" );

    TH1F * sign_mc_norm   = static_cast<TH1F *>( (sign_mcs[val_idx].hist)->Clone() ) ;
    TH1F * bckg_mc_norm   = static_cast<TH1F *>( (bckg_mcs[val_idx].hist)->Clone() ) ;
    TH1F * sign_val_norm  = static_cast<TH1F *>( (sign_vals[val_idx].hist)->Clone() ) ;
    TH1F * bckg_val_norm  = static_cast<TH1F *>( (bckg_vals[val_idx].hist)->Clone() ) ;

    TH1F * yield_val_norm = static_cast<TH1F *>( (yield_vals[val_idx].hist)->Clone() );
    TH1F * data_val_norm  = static_cast<TH1F *>( (data_mcs[val_idx].hist)->Clone() ) ;

    //TH1F * data_mc_norm  = static_cast<TH1F *>( (data_mcs[val_idx].hist)->Clone() ) ;
    

    for ( int bin_idx = 1; bin_idx <= sign_mc_norm->GetNbinsX(); bin_idx++ ){
      sign_mc_norm->SetBinContent(  bin_idx, sign_mc_norm->GetBinContent(bin_idx)/sign_mc_norm->GetBinWidth( bin_idx ) );
      bckg_mc_norm->SetBinContent(  bin_idx, bckg_mc_norm->GetBinContent(bin_idx)/bckg_mc_norm->GetBinWidth( bin_idx ) );
      sign_val_norm->SetBinContent( bin_idx, sign_val_norm->GetBinContent(bin_idx)/sign_val_norm->GetBinWidth( bin_idx ) );
      bckg_val_norm->SetBinContent( bin_idx, bckg_val_norm->GetBinContent(bin_idx)/bckg_val_norm->GetBinWidth( bin_idx ) );
      data_val_norm->SetBinContent( bin_idx, data_val_norm->GetBinContent(bin_idx)/data_val_norm->GetBinWidth( bin_idx ) );
      yield_val_norm->SetBinContent( bin_idx, yield_val_norm->GetBinContent(bin_idx)/yield_val_norm->GetBinWidth( bin_idx ) );
    }

    sign_mc_norm->Scale( 1.0/sign_mc_norm->Integral() ); 
    bckg_mc_norm->Scale( 1.0/bckg_mc_norm->Integral() ); 
    sign_val_norm->Scale( 1.0/sign_val_norm->Integral() );
    bckg_val_norm->Scale( 1.0/bckg_val_norm->Integral() );

    closure( sign_val_norm, sign_mc_norm, val_bound, sample_type::sign );
    closure( bckg_val_norm, bckg_mc_norm, val_bound, sample_type::bckg );
    closure_yield( yield_val_norm, data_val_norm, val_bound, sample_type::data );


    active_pad = static_cast<TPad *>( val_canv->cd( 3 ) );
    sign_mc_norm->Draw( "HIST" );
    sign_val_norm->Draw( "HIST SAMES" );
    sign_val_norm->SetLineStyle( 2 );
    hist_prep_axes( sign_mc_norm, true );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( sign_mc_norm, val_bound.get_ltx(), "Density" );
    add_pad_title( active_pad, "Normalised signals" );
    TLegend * val_snrm_legend = below_logo_legend();
    val_snrm_legend->AddEntry( sign_mc_norm, "mc", "l" );
    val_snrm_legend->AddEntry( sign_val_norm, "ex", "l" );
    val_snrm_legend->Draw( "SAME" );
    TPaveStats * sign_mc_stats = make_stats( sign_mc_norm, true, false );
    TPaveStats * sign_ex_stats = make_stats( sign_val_norm, true, true );
    sign_mc_stats->Draw( "SAME" );
    sign_ex_stats->Draw( "SAME" );

    active_pad = static_cast<TPad *>( val_canv->cd( 4 ) );
    bckg_mc_norm->Draw( "HIST" );
    bckg_val_norm->Draw( "HIST SAMES" );
    bckg_val_norm->SetLineStyle( 2 );
    hist_prep_axes( bckg_mc_norm, true );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( bckg_mc_norm, val_bound.get_ltx(), "Density" );
    add_pad_title( active_pad, "Normalised backgrounds" );
    TLegend * val_bnrm_legend = below_logo_legend();
    val_bnrm_legend->AddEntry( bckg_mc_norm, "mc", "l" );
    val_bnrm_legend->AddEntry( bckg_val_norm, "ex", "l" );
    val_bnrm_legend->Draw( "SAME" );
    TPaveStats * bckg_mc_stats = make_stats( bckg_mc_norm, true, false );
    TPaveStats * bckg_ex_stats = make_stats( bckg_val_norm, true, true );
    bckg_mc_stats->Draw( "SAME" );
    bckg_ex_stats->Draw( "SAME" );


    TH1F * sign_mc_norm_log = static_cast<TH1F *>( sign_mc_norm->Clone() );
    TH1F * bckg_mc_norm_log = static_cast<TH1F *>( bckg_mc_norm->Clone() );

    active_pad = static_cast<TPad *>( val_canv->cd( 5 ) );
    sign_mc_norm_log->SetStats( false );
    sign_mc_norm_log->Draw( "HIST" );
    sign_val_norm->Draw( "HIST SAME" );
    sign_val_norm->SetLineStyle( 2 );
    hist_prep_axes( sign_mc_norm_log );
    sign_mc_norm_log->GetYaxis()->SetRangeUser( 1e-4, sign_mc_norm_log->GetMaximum()*100 );
    active_pad->SetLogy();
    add_atlas_decorations( active_pad, true );
    set_axis_labels( sign_mc_norm_log, val_bound.get_ltx(), "Density" );
    add_pad_title( active_pad, "Log Normalised signals" );
    TLegend * val_log_snrm_legend = below_logo_legend();
    val_log_snrm_legend->AddEntry( sign_mc_norm_log, "mc", "l" );
    val_log_snrm_legend->AddEntry( sign_val_norm, "ex", "l" );
    val_log_snrm_legend->Draw( "SAME" );

    active_pad = static_cast<TPad *>( val_canv->cd( 6 ) );
    bckg_mc_norm_log->SetStats( false );
    bckg_mc_norm_log->Draw( "HIST" );
    bckg_val_norm->Draw( "HIST SAME" );
    bckg_val_norm->SetLineStyle( 2 );
    hist_prep_axes( bckg_mc_norm_log );
    bckg_mc_norm_log->GetYaxis()->SetRangeUser( 1e-4, bckg_mc_norm_log->GetMaximum()*100 );
    active_pad->SetLogy();
    add_atlas_decorations( active_pad, true );
    set_axis_labels( bckg_mc_norm_log, val_bound.get_ltx(), "Density" );
    add_pad_title( active_pad, "Log Normalised backgrounds" );
    TLegend * val_log_bnrm_legend = below_logo_legend();
    val_log_bnrm_legend->AddEntry( bckg_mc_norm_log, "mc", "l" );
    val_log_bnrm_legend->AddEntry( bckg_val_norm, "ex", "l" );
    val_log_bnrm_legend->Draw( "SAME" );

    std::string canv_name = "./full_" + val_name + ".png" ;
    val_canv->SaveAs( canv_name.c_str() );


    TH1F * sign_ratio = static_cast<TH1F *>( sign_val_norm->Clone() ); 
    TH1F * bckg_ratio = static_cast<TH1F *>( bckg_val_norm->Clone() ); 
    sign_ratio->Reset();
    bckg_ratio->Reset();
    sign_ratio->Divide( sign_val_norm, sign_mc_norm, 1.0, 1.0 );
    bckg_ratio->Divide( bckg_val_norm, bckg_mc_norm, 1.0, 1.0 );

    TCanvas * ratio_canvas = new TCanvas( "rc", "", 200, 200, 2000, 1000 );
    ratio_canvas->Divide( 2, 1 );

    active_pad = static_cast<TPad *>( ratio_canvas->cd( 1 ) );
    sign_ratio->Draw( "HIST" );
    sign_ratio->SetLineStyle( 1 );
    hist_prep_axes( sign_ratio, true );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( sign_ratio, val_bound.get_ltx(), "Density" );
    add_pad_title( active_pad, "Normalised signals" );
    TLegend * sign_ratio_legend = below_logo_legend();
    sign_ratio_legend->AddEntry( sign_ratio, "ratio", "l" );
    sign_ratio_legend->Draw( "SAME" );

    active_pad = static_cast<TPad *>( ratio_canvas->cd( 2 ) );
    bckg_ratio->Draw( "HIST" );
    bckg_ratio->SetLineStyle( 1 );
    hist_prep_axes( bckg_ratio, true );
    add_atlas_decorations( active_pad, true );
    set_axis_labels( bckg_ratio, val_bound.get_ltx(), "Density" );
    add_pad_title( active_pad, "Normalised bckgals" );
    TLegend * bckg_ratio_legend = below_logo_legend();
    bckg_ratio_legend->AddEntry( bckg_ratio, "ratio", "l" );
    bckg_ratio_legend->Draw( "SAME" );

    std::string ratio_canv_name = "./ratio_" + val_name + ".png" ;
    ratio_canvas->SaveAs( ratio_canv_name.c_str() );
    delete ratio_canvas;
    

    ratios_store->cd();
    bckg_ratio->SetName( val_name.c_str() );
    bckg_ratio->Write( val_name.c_str() );
    TH1F * flat = static_cast< TH1F *>( bckg_ratio->Clone() );
    flat->Reset();
    for ( int bin_idx = 1; bin_idx <= flat->GetNbinsX(); bin_idx++ ){ flat->SetBinContent( bin_idx, 1.0 ); }
    ratio_variations( flat, "flat_up", 1.05 );
    ratio_variations( bckg_ratio, "bckg_up", 1.1 );

    delete val_canv; 
    delete sign_ratio;
    delete flat;
    delete bckg_ratio;

   


  }

  std::vector< double > photon_pt_edges = { 5, 6, 7, 8, 9, 10, 25 };
  TH1F * cw_photonpt = new TH1F( "PhotonPt", "", photon_pt_edges.size()-1, &photon_pt_edges[0] );
  for ( int bin = 1; bin <= cw_photonpt->GetNbinsX(); bin++ ){cw_photonpt->SetBinContent( bin, 1.0 ); }
  ratio_variations( cw_photonpt, "corr_up", 1.01 );

  ratios_store->Close();

  chdir( ".." );

}
